[![License](https://img.shields.io/badge/license-MIT-blue)](LICENSE)
[![Pipeline Status](https://git.science.uu.nl/datastrophe/frontend/badges/main/pipeline.svg)](https://git.science.uu.nl/datastrophe/frontend/-/commits/main)
[![Coverage](https://git.science.uu.nl/datastrophe/frontend/badges/main/coverage.svg)](https://git.science.uu.nl/datastrophe/frontend/-/commits/main)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<div align='center'>
<img src="https://git.science.uu.nl/datastrophe/frontend/-/raw/develop/src/presentation/view/navbar/logogp.png" align="center" width="150" alt="Project icon">
</div>

## Frontend

This is the frontend React app for the GraphPolaris dashboard. It features interactive database query building and result visualisations.

### Installation

To install the web app you need to have `yarn` installed. This can be done easily with
`npm install --global yarn`

Install dependencies and start:

```
// Clone the repository and install all necessary dependencies
yarn

// Start the web app
yarn start
```

### Testing

To run the tests execute `yarn test`.
To see coverages run `yarn test --coverage --watchAll`.

### File structure

```
├───data
│   ├───drivers
│   └───mock-data
├───domain
│   ├───entity
│   ├───repository-interfaces
│   └───usecases
└───presentation
    ├───view
    └───view-model
```

There are three main folders, `data`, `domain` and `presentation`, in the `src` folder.

The `data` folders contains files for requesting and receiving data from the backend (drivers), and mock data used for testing.

The `domain` folder holds three folders:

- `entity` contains models, structures and type definitions used by view models.
- `repository-interfaces` contain interfaces for making calls to the backend.
- `usecases` contains usecases used by view models.

The `presentation` folder contains two folders:

- `view`, the React components for rendering the views are defined here.
- `view-model`, the view-models use by the corresponding views are defined here.

### Contributing

Please use VSCode with the [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) and [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) extensions installed.
