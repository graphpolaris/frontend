/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { act } from 'react-dom/test-utils';
import React from 'react';
import App from './App';
import ReactDOM from 'react-dom';

let container: any;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
  global.ResizeObserver = require('resize-observer-polyfill');
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

describe('App', () => {
  it('Should reach the end of the file', () => {
    const logSpy = jest.spyOn(console, 'log');
    act(() => {
      ReactDOM.render(<App />, container);
    });
    expect(logSpy).toBeCalledWith('Everything succesfully started');
  });
});
