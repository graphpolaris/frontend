/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import React from 'react';
import './App.css';
import './Fonts.css';
import Panels from './presentation/view/panels/PanelsComponent';
import graph from './data/mock-data/node-link/Miserables';
import QueryBuilderViewModelImpl from './presentation/view-model/query-builder/QueryBuilderViewModelImpl';
import BackendMessenger from './data/drivers/backend-messenger/BackendMessenger';
import WebSocketHandler from './data/drivers/backend-message-receiver/WebSocketHandler';
import GraphUseCase from './domain/usecases/graph-schema/GraphUseCase';
import PanelsViewModelImpl from './presentation/view-model/panels/PanelsViewModelImpl';
import ResultNodeLinkParserUseCase from './domain/usecases/node-link/ResultNodeLinkParserUseCase';
import NodeLinkViewModelImpl from './presentation/view-model/result-visualisations/node-link/NodeLinkViewModelImpl';
import SchemaViewModelImpl from './presentation/view-model/graph-schema/SchemaViewModelImpl';
import DragElementUseCase from './domain/usecases/query-builder/DragElementUseCase';
import SetElementsUseCase from './domain/usecases/query-builder/SetElementsUseCase';
import LoginUseCase from './domain/usecases/authorize/LoginUseCase';
import AuthorizeApi from './data/drivers/authorize/AuthorizeApi';
import AuthHolder from './domain/entity/auth/models/AuthHolder';
import VisualTypeHolder from './domain/entity/VisualTypeHolders/VisualTypeHolder';
import NodeUseCase from './domain/usecases/graph-schema/NodeUseCase';
import EdgeUseCase from './domain/usecases/graph-schema/EdgeUseCase';
import DrawOrderUseCase from './domain/usecases/graph-schema/DrawOrderUseCase';
import SemanticSubstratesViewModelImpl from './presentation/view-model/result-visualisations/semantic-substrates/SemanticSubstratesViewModelImpl';
import BackendMessageReceiver from './domain/repository-interfaces/BackendMessageReceiver';

import PaohvisViewModelImpl from './presentation/view-model/result-visualisations/paohvis/PaohvisViewModelImpl';
import RawJSONViewModelImpl from './presentation/view-model/result-visualisations/raw-json/RawJSONViewModelImpl';
import HivePlotsVisViewModelImpl from './presentation/view-model/result-visualisations/hiveplots/HivePlotsViewModelImpl';
import NavbarViewModelImpl from './presentation/view-model/navbar/NavbarViewModelImpl';
import PanelVisualisationViewModelImpl from './presentation/view-model/panel-visualisation/PanelVisualisationViewModelImpl';
import DatabaseHolder from './domain/entity/navbar/DatabaseHolder';
import RequestSchemaUseCase from './domain/usecases/request-schema/RequestSchemaUseCase';
import DatabaseApi from './data/drivers/database/databaseAPI';
import QueryStatusListViewModelImpl from './presentation/view-model/query-status-list/QueryStatusListViewModelImpl';
import FunctionsMenuViewModelImpl from './presentation/view-model/query-builder/functions-menu/FunctionsMenuViewModelImpl';
import QueryApi from './data/drivers/query/QueryApi';
import { ColourPalettes } from './domain/entity/customization/colours';
import { LocalStorage } from './data/drivers/LocalStorage';
import GetElementsUseCase from './domain/usecases/query-builder/GetElementsUseCase';
import ErrorsViewModelImpl from './presentation/view-model/errors/ErrorsViewModelImpl';
function App(): JSX.Element {
  const domain = 'datastrophe.science.uu.nl';

  // data layer
  const backendMessenger = new BackendMessenger(domain);
  const graphUseCase = new GraphUseCase();
  const resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  const backendMessageReceiver: BackendMessageReceiver = new WebSocketHandler(domain);

  const queryApi = new QueryApi(backendMessenger);
  const authorizeApi = new AuthorizeApi(backendMessenger);
  const databaseApi = new DatabaseApi(backendMessenger);

  // domain layer
  const drawOrderUseCase = new DrawOrderUseCase();
  const nodeUseCase = new NodeUseCase();
  const edgeUseCase = new EdgeUseCase();
  const requestSchemaUseCase = new RequestSchemaUseCase(backendMessenger);

  const authHolder = new AuthHolder();
  const visualTypeHolder = new VisualTypeHolder();
  const databaseHolder = new DatabaseHolder();

  const loginUseCase = new LoginUseCase(
    authorizeApi,
    databaseApi,
    authHolder,
    databaseHolder,
    backendMessageReceiver,
    requestSchemaUseCase,
  );
  loginUseCase.handleCallbackLogin();
  loginUseCase.tryAutoLoginWithClientIDCookie();

  // view layer
  const queryBuilderViewModel = new QueryBuilderViewModelImpl(queryApi, databaseHolder);

  const schemaViewModel = new SchemaViewModelImpl(
    drawOrderUseCase,
    nodeUseCase,
    edgeUseCase,
    graphUseCase,
    queryBuilderViewModel.addAttribute,
  );

  const rawJSONViewModel = new RawJSONViewModelImpl();
  const nodeLinkViewModel = new NodeLinkViewModelImpl(
    resultNodeLinkParserUseCase,
    graph,
    window.innerWidth,
    window.innerHeight,
    ColourPalettes[LocalStorage.instance().cache.currentColours.key],
  );

  const semanticSubstratesViewModel = new SemanticSubstratesViewModelImpl();
  const paohvisViewModel = new PaohvisViewModelImpl();
  const hivePlotsViewModel = new HivePlotsVisViewModelImpl();

  const navbarViewModel = new NavbarViewModelImpl(
    loginUseCase,
    requestSchemaUseCase,
    databaseApi,
    authHolder,
    databaseHolder,
  );

  const panelVisualisationViewModel = new PanelVisualisationViewModelImpl(
    visualTypeHolder,
    rawJSONViewModel,
    nodeLinkViewModel,
    semanticSubstratesViewModel,
    paohvisViewModel,
    hivePlotsViewModel,
  );

  const queryStatusListViewModel = new QueryStatusListViewModelImpl(
    queryApi,
    databaseHolder,
    queryBuilderViewModel,
  );
  const functionsMenuViewModel = new FunctionsMenuViewModelImpl();

  const panelViewModel = new PanelsViewModelImpl(
    queryBuilderViewModel,
    queryStatusListViewModel,
    schemaViewModel,
    navbarViewModel,
    panelVisualisationViewModel,
    functionsMenuViewModel,
  );
  console.log('Everything succesfully started');
  return (
    <div style={{ height: '100%', overflow: 'hidden' }}>
      <Panels panelViewModel={panelViewModel} />
    </div>
  );
}

export default App;
