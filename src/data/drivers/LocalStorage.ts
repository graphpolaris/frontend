/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import LocalStorageRepository from '../../domain/repository-interfaces/LocalStorageRepository';

export class LocalStorage {
  private static singletonInstance: LocalStorageRepository;

  /** Get the singleton instance of the ConfigurationStorage. */
  public static instance(): LocalStorageRepository {
    if (!this.singletonInstance) this.singletonInstance = new LocalStorageRepository();
    return this.singletonInstance;
  }
}
