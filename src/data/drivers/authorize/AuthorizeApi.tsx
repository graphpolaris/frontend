/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Http } from '@material-ui/icons';
import AuthorizationResult from '../../../domain/entity/auth/structures/AuthorizationResult';
import AuthorizeRepository from '../../../domain/repository-interfaces/AuthorizeRepository';
import BackendMessengerRepository from '../../../domain/repository-interfaces/BackendMessengerRepository';

/** Wrapper for authorization requests to the backend. */
export default class AuthorizeApi implements AuthorizeRepository {
  private backendMessenger: BackendMessengerRepository;

  /** @param backendMessenger - The message repository implemention to use for sending the requests. */
  constructor(backendMessenger: BackendMessengerRepository) {
    this.backendMessenger = backendMessenger;
  }

  /**
   * Currently only requests a new ClientID, and requests a new socketID.
   * @returns A promise with the client and session ID in the authorization result.
   * @todo Add proper login and error message handling.
   */
  public async login(): Promise<AuthorizationResult> {
    const clientID = await this.RequestNewClientIDCookie();
    const { sessionID } = await this.RequestNewSocketIDCookie();

    return new Promise((resolve) => {
      resolve({
        clientID: clientID,
        sessionID: sessionID,
      });
    });
  }

  public async loginWithCookie(): Promise<AuthorizationResult> {
    return this.RequestNewSocketIDCookie();
  }

  /**
   * Request a new client and session id from the backend.
   * These will be set in cookie to be used for requesting data.
   * @returns A promise with the clientID.
   */
  private RequestNewClientIDCookie(): Promise<string> {
    let loc;
    try {
      loc = 'user/' + window.location.href.split('/')[4]; //get the callback?infoinfoinfo from datastrophe.science.uu.nl/develop/callback?infoinfoinfo
    } catch {
      loc = 'user/callback';
      console.log('no callback found, please use the proper callback url');
      //todo: redirect here to /user/create, for now it just makes sure the test doesnt mind that jsdom cant change location
    }

    console.log('gonna make the client request');
    console.log(loc);
    return this.backendMessenger.SendRequest(loc).then((response) => {
      // Return the response clientID
      return response.json().then((data: any) => data.clientID);
    });
  }

  /**
   * Request a new session id from the backend, will only work if the user has a clientID cookie set.
   * @returns A promise with the client and session ID.
   */
  private RequestNewSocketIDCookie(): Promise<{ clientID: string; sessionID: string }> {
    console.log('gonna make the session request');
    return this.backendMessenger.SendRequest('user/session/').then((response) => {
      // Return the response sessionID
      return response.json().then((data: any) => {
        return { clientID: data.clientID, sessionID: data.sessionID };
      });
    });
  }
}
