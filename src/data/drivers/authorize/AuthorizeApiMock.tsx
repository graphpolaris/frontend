/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock implementations.
 * See testing plan for more details.*/

import AuthorizationResult from '../../../domain/entity/auth/structures/AuthorizationResult';
import AuthorizeRepository from '../../../domain/repository-interfaces/AuthorizeRepository';

export default class AuthorizeApiMock implements AuthorizeRepository {
  private clientID: string;
  private sessionID: string;

  constructor(clientID: string, sessionID: string) {
    this.clientID = clientID;
    this.sessionID = sessionID;
  }
  /** replaces the original login methode */
  public async login(): Promise<AuthorizationResult> {
    return {
      clientID: this.clientID,
      sessionID: this.sessionID,
    };
  }

  /** replaces the original loginWithCookie methode*/
  public async loginWithCookie(): Promise<AuthorizationResult> {
    return {
      clientID: this.clientID,
      sessionID: this.sessionID,
    };
  }
}
