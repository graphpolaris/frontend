/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mocks.
 * See testing plan for more details.*/
import BackendMessageReceiver from '../../../domain/repository-interfaces/BackendMessageReceiver';

/** A mock for the backend message receiver used for testing purposes. */
export default class BackendMessageReceiverMock implements BackendMessageReceiver {
  private connected = false;

  public connect(): void {
    this.connected = true;
  }
  public close(): void {
    this.connected = false;
  }

  public onMessage(msg: string): void {}

  public isConnected(): boolean {
    return this.connected;
  }
}
