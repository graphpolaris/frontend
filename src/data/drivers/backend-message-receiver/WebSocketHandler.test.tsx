/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import WebSocketHandler from './WebSocketHandler';
import Broker from '../../../domain/entity/broker/broker';
describe('WebSocketHandler', () => {
  let websockethandler = new WebSocketHandler('placeholderName');

  it('Should connect', () => {
    websockethandler.connect(websockethandler.close);
    expect(websockethandler.isConnected()).toEqual(true);
  });

  it('Should disconnect', () => {
    websockethandler.connect(websockethandler.close);
    websockethandler.close();
    expect(websockethandler.isConnected()).toEqual(false);
  });
});
