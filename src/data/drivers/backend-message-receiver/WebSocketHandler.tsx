/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import GraphUseCase from '../../../domain/usecases/graph-schema/GraphUseCase';
import BackendMessageReceiver from '../../../domain/repository-interfaces/BackendMessageReceiver';
import Broker from '../../../domain/entity/broker/broker';
import ErrorsViewModelImpl from '../../../presentation/view-model/errors/ErrorsViewModelImpl';

/** The websockethandler creates a websocket and wait for messages send to the socket. */
export default class WebSocketHandler implements BackendMessageReceiver {
  private webSocket: WebSocket | undefined;
  private url: string;
  private connected: boolean;

  /** @param domain The domain to make the websocket connection with. */
  public constructor(domain: string) {
    this.url = 'wss://' + domain + '/socket/';
    this.connected = false;
  }

  /**
   * Create a websocket to the given URL.
   * @param {string} URL is the URL to which the websocket connection is opened.
   */
  public connect(onOpen: () => void): void {
    // If there already is already a current websocket connection, close it first.
    if (this.webSocket) this.close();

    this.webSocket = new WebSocket(this.url);
    this.webSocket.onopen = () => onOpen();
    this.webSocket.onmessage = this.onWebSocketMessage;
    this.webSocket.onerror = this.onError;
    this.webSocket.onclose = this.onClose;

    this.connected = true;
  }

  /** Closes the current websocket connection. */
  public close = (): void => {
    if (this.webSocket) this.webSocket.close();
    this.connected = false;
  };

  /** @returns A boolean which indicates if there currently is a socket connection. */
  public isConnected = (): boolean => {
    return this.connected;
  };

  /**
   * Websocket connection close event handler.
   * @param {any} event Contains the event data.
   */
  private onClose(event: any): void {
    console.log(event.data);
  }

  /**
   * Websocket connection message event handler. Called if a new message is received through the socket.
   * @param {any} event Contains the event data.
   */
  public onWebSocketMessage = (event: MessageEvent<any>) => {
    let data = JSON.parse(event.data);
    Broker.instance().publish(data.value, data.type);
  };

  /**
   * Websocket connection error event handler.
   * @param {any} event contains the event data.
   */
  private onError(event: any): void {
    console.log(event);
  }
}
