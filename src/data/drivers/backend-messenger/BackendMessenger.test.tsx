/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BackendMessenger from './BackendMessenger';
describe('BackendMessenger', () => {
  let placeholderDomain = 'placeholderDomain';
  let backendMessenger = new BackendMessenger(placeholderDomain);

  it('should create the the correct url ', () => {
    expect(backendMessenger['url']).toEqual('https://' + placeholderDomain + '/');
  });
});
