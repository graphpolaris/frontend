/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BackendMessengerRepository from '../../../domain/repository-interfaces/BackendMessengerRepository';

/** Sends messages and requests to the backend. */
export default class BackendMessenger implements BackendMessengerRepository {
  private url: string;

  /** @param domain The domain to send the messages towards. */
  constructor(domain: string) {
    this.url = 'https://' + domain + '/';
  }

  /**
   * Sends a fetch request to the Datastrophe domain.
   * @param {string} body The request body you want to send. Should be stringified JSON.
   * @param {string} requestURL The URL you want to perform this request to, appended to 'datastrophe.science.uu.nl'.
   * @param {string} requestMethod The method of your request. Most used are: POST, GET.
   * @returns {Promise<void>} A promise which is resolved once a response with status 200 has been received.
   */
  public SendMessage(body: string, requestURL: string, requestMethod: string): Promise<Response> {
    // Construct the URL we will request from
    const req = this.url + requestURL;

    return fetch(req, {
      method: requestMethod,
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      // Pass this parameter to always send authorization cookies, even on localhost
      credentials: 'include',
      // Set the body of the request, if this is not a POST request, body is set to undefined
      body: requestMethod == 'POST' ? body : undefined,
      // Wait for the fetch promise to resolve
    });
  }

  /**
   * Sendrequest sends a GET request to the backend.
   * @param requestURL The URL you want to perform this request to, appended to 'datastrophe.science.uu.nl'.
   * @returns {Promise<void>} A promise which is resolved once a response with status 200 has been received.
   */
  public SendRequest(requestURL: string): Promise<Response> {
    // Construct the URL we will request from
    const req = this.url + requestURL;
    console.log('backendmessager: ' + req);
    return fetch(req, {
      method: 'GET',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      // Pass this parameter to always send authorization cookies, even on localhost
      credentials: 'include',
    });
  }
}
