/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock implementations.
 * See testing plan for more details.*/

import BackendMessengerRepository from '../../../domain/repository-interfaces/BackendMessengerRepository';

/** Mock repository used for testing. */
export default class BackendMessengerMock implements BackendMessengerRepository {
  /** A mock version of the sendMessage function sends a fake message. */
  private url: string;

  /** @param domain The domain to send the messages towards. */
  constructor(domain: string) {
    this.url = 'https://' + domain + '/';
  }
  public SendMessage(body: string, requestURL: string, method: string): Promise<Response> {
    switch (requestURL) {
      case 'query/execute/':
        return new Promise((resolve, reject) => {
          if (body == 'validjsonquery') resolve(this.createResponse({ queryID: 'testID' }));
          else if (body == 'no200response') resolve(new Response(undefined, { status: 404 }));
          else if (body == 'notvalidjsonquery') reject('invalid body');
          else resolve(this.createResponse({ ok: 'ok' }));
        });
      case 'query/retrieve-cached/':
        return new Promise((resolve, reject) => {
          const b = JSON.parse(body);
          if ('queryID' in b) resolve(this.createResponse({}));
          else reject('invalid body');
        });
      default:
        return new Promise((resolve) => {
          resolve(this.createResponse({ ok: 'ok' }));
        });
    }
  }

  /** A mock version of the sendRequest function sends a fake request. */
  public SendRequest(requestURL: string): Promise<Response> {
    switch (requestURL) {
      case 'user/callback':
        return new Promise((resolve) => {
          const result = {
            clientID: 'mock-clientID',
          };
          resolve(this.createResponse(result));
        });

      case 'user/session/':
        return new Promise((resolve) => {
          const result = {
            clientID: 'mock-clientID',
            sessionID: 'mock-sessionID',
          };
          resolve(this.createResponse(result));
        });
      case 'user/databases/':
        return new Promise((resolve) => {
          resolve(this.createResponse({ databases: ['testdb'] }));
        });
      default:
        return new Promise((resolve) => {
          resolve(new Response(undefined, { status: 404 }));
        });
    }
  }

  /** A mock response created to test the backend messenger. */
  private createResponse(body: any): Response {
    const meta = {
      'Content-Type': 'application/json',
    };
    const headers = new Headers(meta);
    const responseInit = { status: 200, headers: headers };

    const response = new Response(JSON.stringify(body), responseInit);

    return response;
  }
}
