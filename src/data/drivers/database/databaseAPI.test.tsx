/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import BackendMessengerMock from '../backend-messenger/BackendMessengerMock';
import DatabaseApi from './databaseAPI';
import { Database } from '../../../domain/repository-interfaces/DatabaseRepository';

describe('databaseAPI', () => {
  let BackendMessenger;
  let DatabaseAPI: DatabaseApi;
  const placeholderDomain = 'placeholderDomain ';

  beforeEach(() => {
    BackendMessenger = new BackendMessengerMock(placeholderDomain);
    DatabaseAPI = new DatabaseApi(BackendMessenger);
  });

  it('should send a message to the backend messenger on adding a database.', () => {
    //ph means placeholder
    const database: Database = {
      username: 'phUsername',
      password: 'phPassword',
      hostname: 'phHostname',
      port: 123,
      databaseName: 'testdb',
      internalDatabaseName: 'phinternalDatabaseName',
      databaseType: 'phdatabaseType',
    };
    let promise = DatabaseAPI.addDatabase(database);
    promise
      .then((resp) => expect(resp.ok).toBe(true))
      .catch(() => fail("Database wasn't added correctly"));
  });
});
