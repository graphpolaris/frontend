/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BackendMessengerRepository from '../../../domain/repository-interfaces/BackendMessengerRepository';
import DatabaseRepository, {
  Database,
} from '../../../domain/repository-interfaces/DatabaseRepository';

export default class DatabaseApi implements DatabaseRepository {
  private backendMessenger: BackendMessengerRepository;

  /** @param backendMessenger A BackendMessengerRepository implementation. */
  constructor(backendMessenger: BackendMessengerRepository) {
    this.backendMessenger = backendMessenger;
  }

  /** Call this when you wish to add a database and send it to backend
   * @param database The database you wish to add.
   */
  public addDatabase(database: Database): Promise<Response> {
    const body = JSON.stringify({
      databaseName: database.databaseName,
      databaseType: database.databaseType,
      internalDatabaseName: database.internalDatabaseName,
      databaseUrl: database.hostname,
      databasePort: database.port,
      databaseUsername: database.username,
      databasePassword: database.password,
    });
    return this.backendMessenger.SendMessage(body, 'user/add-database/', 'POST');
  }
  /** Gets the databases from backend */
  public fetchUserDatabases(): Promise<string[]> {
    return this.backendMessenger.SendRequest('user/databases/').then((res) => {
      return res.json().then((vals) => vals.databases);
    });
  }
}
