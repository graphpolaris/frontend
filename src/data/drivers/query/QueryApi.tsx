import BackendMessengerRepository from '../../../domain/repository-interfaces/BackendMessengerRepository';
import QueryRepository, { Query } from '../../../domain/repository-interfaces/QueryRepository';

/**
 * Implementation of the QueryRepository.
 * Contains function for sending a query to the database and retrieving a cached query.
 */
export default class QueryApi implements QueryRepository {
  backendMessenger: BackendMessengerRepository;

  /** @param backendMessenger {BackendMessengerRepository} A BackendMessengerRepository implementation. */
  constructor(backendMessenger: BackendMessengerRepository) {
    this.backendMessenger = backendMessenger;
  }

  async sendQuery(query: Query): Promise<string> {
    const body = JSON.stringify(query);
    return this.backendMessenger.SendMessage(body, 'query/execute/', 'POST').then((response) => {
      return response.json().then((obj) => obj.queryID);
    });
  }

  async retrieveCachedQuery(queryID: string): Promise<Response> {
    return this.backendMessenger.SendMessage(
      JSON.stringify({ queryID }),
      'query/retrieve-cached/',
      'POST',
    );
  }
}
