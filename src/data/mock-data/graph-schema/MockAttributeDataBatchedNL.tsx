export const mockAttributeDataNLNode1 = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'woonplaats',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'anc',
      type: 'Numerical',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'naam',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [
        'VVD',
        'PVV',
        'SP',
        'CDA',
        'PvdA',
        'DENK',
        'FVD',
        'D66',
        'CU',
        'SGP',
        'GL',
        'Volt',
        'JA21',
        'Groep Van Haga',
        'PvdD',
        'Fractie Den Haan',
        'BBB',
        'BIJ1',
      ],
    },
    {
      name: 'img',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'leeftijd',
      type: 'Numerical',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLNode1NoValidObjectType = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'woonplaats',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'anc',
      type: 'Numerical',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLNode1AttributeArrayIsNoArray = {
  id: 'kamerleden',
  attributes: {
    name: 'woonplaats',
    type: 'Other',
    nullAmount: 0,
    uniqueCategoricalValues: [],
  },
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLUniqueCategoricalValuesIsNoArray = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: 'VVD',
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLUniqueCategoricalValuesLengthIsZero = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLCategoricalLength0 = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLDifferentTypes1 = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: ['VVD', 6],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLDifferentTypes2 = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [6, false],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLDifferentTypes3 = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [false, 'VVD'],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLInvalidType = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [{ name: 'VVD' }],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLInvalidLengthNumerical = {
  id: 'kamerleden',
  attributes: [
    {
      name: 'partij',
      type: 'Numerical',
      nullAmount: 0,
      uniqueCategoricalValues: [6],
    },
  ],
  length: 149,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLNode2 = {
  id: 'commissies',
  attributes: [
    {
      name: 'naam',
      type: 'Other',
      nullAmount: 1,
      uniqueCategoricalValues: [],
    },
  ],
  length: 38,
  connectedRatio: 0.9736842105263158,
  summedNullAmount: 1,
};

export const mockAttributeDataNLNode2IncorrectId = {
  id: 'commissions',
  attributes: [
    {
      name: 'naam',
      type: 'Other',
      nullAmount: 1,
      uniqueCategoricalValues: [],
    },
  ],
  length: 38,
  connectedRatio: 0.9736842105263158,
  summedNullAmount: 1,
};

export const mockAttributeDataNLNode3 = {
  id: 'moties',
  attributes: [
    {
      name: 'indiener',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'info',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'datum',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
  ],
  length: 6065,
  connectedRatio: 0.7899422918384171,
  summedNullAmount: 0,
};

export const mockAttributeDataNLNode4 = {
  id: 'partijen',
  attributes: [
    {
      name: 'zetels',
      type: 'Numerical',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'voorzitter',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'voorzitterId',
      type: 'Numerical',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
    {
      name: 'naam',
      type: 'Other',
      nullAmount: 0,
      uniqueCategoricalValues: [],
    },
  ],
  length: 18,
  connectedRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLEdge1 = {
  id: 'onderdeel_van',
  attributes: [],
  length: 1164,
  fromRatio: 0.9060402684563759,
  toRatio: 0.9736842105263158,
  summedNullAmount: 0,
};

export const mockAttributeDataNLEdge2 = {
  id: 'lid_van',
  attributes: [
    {
      name: 'isVoorzitter',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [false, true],
    },
  ],
  length: 149,
  fromRatio: 1,
  toRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLEdge2IncorrectId = {
  id: 'lidvan',
  attributes: [
    {
      name: 'isVoorzitter',
      type: 'Categorical',
      nullAmount: 0,
      uniqueCategoricalValues: [false, true],
    },
  ],
  length: 149,
  fromRatio: 1,
  toRatio: 1,
  summedNullAmount: 0,
};

export const mockAttributeDataNLEdge3 = {
  id: 'dient_in',
  attributes: [],
  length: 7444,
  fromRatio: 0.959731543624161,
  toRatio: 0.7899422918384171,
  summedNullAmount: 0,
};
