/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/
import * as InputTypes from '../../../domain/entity/graph-schema/structures/InputDataTypes';

/** Mock data to create an aeroplane database. */
const nodes: InputTypes.Node[] = [
  {
    name: 'Thijs',
    attributes: [],
  },
  {
    name: 'Airport',
    attributes: [
      { name: 'city', type: 'string' },
      { name: 'vip', type: 'bool' },
      { name: 'state', type: 'string' },
    ],
  },
  {
    name: 'Airport2',
    attributes: [
      { name: 'city', type: 'string' },
      { name: 'vip', type: 'bool' },
      { name: 'state', type: 'string' },
    ],
  },
  {
    name: 'Plane',
    attributes: [
      { name: 'type', type: 'string' },
      { name: 'maxFuelCapacity', type: 'int' },
    ],
  },
  { name: 'Staff', attributes: [] },
  { name: 'Unconnected', attributes: [] },
];

/** Mocked edges */
const edges: InputTypes.Edge[] = [
  {
    name: 'Airport2:Airport',
    from: 'Airport2',
    to: 'Airport',
    collection: 'flights',
    attributes: [
      { name: 'arrivalTime', type: 'int' },
      { name: 'departureTime', type: 'int' },
    ],
  },
  {
    name: 'Airport:Staff',
    from: 'Airport',
    to: 'Staff',
    collection: 'flights',
    attributes: [{ name: 'salary', type: 'int' }],
  },
  { name: 'Plane:Airport', from: 'Plane', to: 'Airport', collection: 'flights', attributes: [] },
  {
    name: 'Airport:Thijs',
    from: 'Airport',
    to: 'Thijs',
    collection: 'flights',
    attributes: [{ name: 'hallo', type: 'string' }],
  },
  {
    name: 'Thijs:Airport',
    from: 'Thijs',
    to: 'Airport',
    collection: 'flights',
    attributes: [{ name: 'hallo', type: 'string' }],
  },
  {
    name: 'Staff:Plane',
    from: 'Staff',
    to: 'Plane',
    collection: 'flights',
    attributes: [{ name: 'hallo', type: 'string' }],
  },
  {
    name: 'Staff:Airport2',
    from: 'Staff',
    to: 'Airport2',
    collection: 'flights',
    attributes: [{ name: 'hallo', type: 'string' }],
  },
  {
    name: 'Airport2:Plane',
    from: 'Airport2',
    to: 'Plane',
    collection: 'flights',
    attributes: [{ name: 'hallo', type: 'string' }],
  },

  {
    name: 'Airport:Airport',
    from: 'Airport',
    to: 'Airport',
    collection: 'flights',
    attributes: [{ name: 'test', type: 'string' }],
  },
];

export const schema: InputTypes.Schema = {
  nodes: nodes,
  edges: edges,
};

export const schema2: InputTypes.Schema = {
  nodes: nodes,
  edges: [],
};
