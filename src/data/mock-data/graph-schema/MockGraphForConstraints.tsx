/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

import * as InputTypes from '../../../domain/entity/graph-schema/structures/InputDataTypes';

/** Mock data to create an aeroplane database. */
const nodes: InputTypes.Node[] = [
  {
    name: 'airports',
    attributes: [
      { name: 'city', type: 'string' },
      { name: 'vip', type: 'bool' },
      { name: 'state', type: 'string' },
      { name: 'id', type: 'string' },
    ],
  },
];

const edges: InputTypes.Edge[] = [
  {
    name: 'airports:airports',
    to: 'airports',
    from: 'airports',
    collection: 'flights',
    attributes: [
      { name: 'Day', type: 'int' },
      { name: 'id', type: 'string' },
    ],
  },
];

export const mockSchema: InputTypes.Schema = {
  nodes: nodes,
  edges: edges,
};
