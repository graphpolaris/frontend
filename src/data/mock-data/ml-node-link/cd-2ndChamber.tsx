export default {
  edges: [
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/148',
      id: 'member_of/148',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/147',
      id: 'member_of/147',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/146',
      id: 'member_of/146',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/145',
      id: 'member_of/145',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/143',
      id: 'member_of/143',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/142',
      id: 'member_of/142',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/139',
      id: 'member_of/139',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/138',
      id: 'member_of/138',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/137',
      id: 'member_of/137',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/136',
      id: 'member_of/136',
      to: 'parties/PvdD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/134',
      id: 'member_of/134',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/133',
      id: 'member_of/133',
      to: 'parties/PvdD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/131',
      id: 'member_of/131',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/129',
      id: 'member_of/129',
      to: 'parties/PvdD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/128',
      id: 'member_of/128',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/127',
      id: 'member_of/127',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/125',
      id: 'member_of/125',
      to: 'parties/SGP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/124',
      id: 'member_of/124',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/123',
      id: 'member_of/123',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/122',
      id: 'member_of/122',
      to: 'parties/GroepVanHaga',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/121',
      id: 'member_of/121',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/141',
      id: 'member_of/141',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/120',
      id: 'member_of/120',
      to: 'parties/BIJ1',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/119',
      id: 'member_of/119',
      to: 'parties/CU',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/118',
      id: 'member_of/118',
      to: 'parties/CU',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/117',
      id: 'member_of/117',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/113',
      id: 'member_of/113',
      to: 'parties/PvdD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/112',
      id: 'member_of/112',
      to: 'parties/JA21',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/111',
      id: 'member_of/111',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/110',
      id: 'member_of/110',
      to: 'parties/BBB',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/109',
      id: 'member_of/109',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/108',
      id: 'member_of/108',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/107',
      id: 'member_of/107',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/105',
      id: 'member_of/105',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/104',
      id: 'member_of/104',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/103',
      id: 'member_of/103',
      to: 'parties/PvdD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/102',
      id: 'member_of/102',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/101',
      id: 'member_of/101',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/100',
      id: 'member_of/100',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/99',
      id: 'member_of/99',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/98',
      id: 'member_of/98',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/97',
      id: 'member_of/97',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/96',
      id: 'member_of/96',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/132',
      id: 'member_of/132',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/95',
      id: 'member_of/95',
      to: 'parties/FVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/94',
      id: 'member_of/94',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/116',
      id: 'member_of/116',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/93',
      id: 'member_of/93',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/92',
      id: 'member_of/92',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/91',
      id: 'member_of/91',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/90',
      id: 'member_of/90',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/89',
      id: 'member_of/89',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/87',
      id: 'member_of/87',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/86',
      id: 'member_of/86',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/85',
      id: 'member_of/85',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/84',
      id: 'member_of/84',
      to: 'parties/DENK',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/130',
      id: 'member_of/130',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/83',
      id: 'member_of/83',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/82',
      id: 'member_of/82',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/81',
      id: 'member_of/81',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/80',
      id: 'member_of/80',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/79',
      id: 'member_of/79',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/78',
      id: 'member_of/78',
      to: 'parties/Volt',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/77',
      id: 'member_of/77',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/76',
      id: 'member_of/76',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/75',
      id: 'member_of/75',
      to: 'parties/FVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/74',
      id: 'member_of/74',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/73',
      id: 'member_of/73',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/72',
      id: 'member_of/72',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/71',
      id: 'member_of/71',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/70',
      id: 'member_of/70',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/68',
      id: 'member_of/68',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/67',
      id: 'member_of/67',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/66',
      id: 'member_of/66',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/65',
      id: 'member_of/65',
      to: 'parties/FVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/64',
      id: 'member_of/64',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/144',
      id: 'member_of/144',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/63',
      id: 'member_of/63',
      to: 'parties/FVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/62',
      id: 'member_of/62',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/61',
      id: 'member_of/61',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/60',
      id: 'member_of/60',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/59',
      id: 'member_of/59',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/140',
      id: 'member_of/140',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/57',
      id: 'member_of/57',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/135',
      id: 'member_of/135',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/56',
      id: 'member_of/56',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/55',
      id: 'member_of/55',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/54',
      id: 'member_of/54',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/53',
      id: 'member_of/53',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/52',
      id: 'member_of/52',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/51',
      id: 'member_of/51',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/50',
      id: 'member_of/50',
      to: 'parties/GroepVanHaga',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/49',
      id: 'member_of/49',
      to: 'parties/FractieDenHaan',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/48',
      id: 'member_of/48',
      to: 'parties/Volt',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/47',
      id: 'member_of/47',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/46',
      id: 'member_of/46',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/45',
      id: 'member_of/45',
      to: 'parties/CU',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/44',
      id: 'member_of/44',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/106',
      id: 'member_of/106',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/43',
      id: 'member_of/43',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/42',
      id: 'member_of/42',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/41',
      id: 'member_of/41',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/40',
      id: 'member_of/40',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/39',
      id: 'member_of/39',
      to: 'parties/PvdD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/38',
      id: 'member_of/38',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/37',
      id: 'member_of/37',
      to: 'parties/JA21',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/36',
      id: 'member_of/36',
      to: 'parties/GroepVanHaga',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/35',
      id: 'member_of/35',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/34',
      id: 'member_of/34',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/33',
      id: 'member_of/33',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/32',
      id: 'member_of/32',
      to: 'parties/JA21',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/88',
      id: 'member_of/88',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/31',
      id: 'member_of/31',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/30',
      id: 'member_of/30',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/29',
      id: 'member_of/29',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/28',
      id: 'member_of/28',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/26',
      id: 'member_of/26',
      to: 'parties/CU',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/69',
      id: 'member_of/69',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/25',
      id: 'member_of/25',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/24',
      id: 'member_of/24',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/23',
      id: 'member_of/23',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/22',
      id: 'member_of/22',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/21',
      id: 'member_of/21',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/19',
      id: 'member_of/19',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/18',
      id: 'member_of/18',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/17',
      id: 'member_of/17',
      to: 'parties/SGP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/16',
      id: 'member_of/16',
      to: 'parties/CU',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/15',
      id: 'member_of/15',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/14',
      id: 'member_of/14',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/58',
      id: 'member_of/58',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/13',
      id: 'member_of/13',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/12',
      id: 'member_of/12',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/27',
      id: 'member_of/27',
      to: 'parties/Volt',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/11',
      id: 'member_of/11',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/10',
      id: 'member_of/10',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/115',
      id: 'member_of/115',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/9',
      id: 'member_of/9',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/20',
      id: 'member_of/20',
      to: 'parties/GL',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/8',
      id: 'member_of/8',
      to: 'parties/FVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/7',
      id: 'member_of/7',
      to: 'parties/DENK',
    },
    {
      attributes: {
        isChairman: true,
      },
      from: 'parliament/6',
      id: 'member_of/6',
      to: 'parties/DENK',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/5',
      id: 'member_of/5',
      to: 'parties/VVD',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/4',
      id: 'member_of/4',
      to: 'parties/PvdA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/126',
      id: 'member_of/126',
      to: 'parties/SGP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/3',
      id: 'member_of/3',
      to: 'parties/CDA',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/114',
      id: 'member_of/114',
      to: 'parties/D66',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/2',
      id: 'member_of/2',
      to: 'parties/SP',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/1',
      id: 'member_of/1',
      to: 'parties/PVV',
    },
    {
      attributes: {
        isChairman: false,
      },
      from: 'parliament/0',
      id: 'member_of/0',
      to: 'parties/VVD',
    },
  ],
  nodes: [
    {
      attributes: {
        age: 56,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/45ef5aae-a6d0-4bad-861e-dd5b883a9665.jpg?itok=h5aEZJVM',
        name: 'Jorien Wuite',
        party: 'D66',
        residence: 'Voorburg',
        seniority: 57,
      },
      id: 'parliament/147',
      mldata: 0,
    },
    {
      attributes: {
        age: 57,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/6d33b9bc-7d3a-4a60-b2d7-c2b70cd11ab1.jpg?itok=kKj5CrLe',
        name: 'Geert Wilders',
        party: 'PVV',
        residence: '',
        seniority: 8247,
      },
      id: 'parliament/144',
      mldata: 1,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/7d18b6e3-de0c-4aea-bef8-242bbc38e936.jpg?itok=zebZoGjq',
        name: 'Jeroen van Wijngaarden',
        party: 'VVD',
        residence: 'Amsterdam',
        seniority: 1823,
      },
      id: 'parliament/143',
      mldata: 2,
    },
    {
      attributes: {
        age: 39,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/c7822b58-103f-4612-87ef-648be97192c6.jpg?itok=jDwKCC26',
        name: 'Lisa Westerveld',
        party: 'GL',
        residence: 'Nijmegen',
        seniority: 1526,
      },
      id: 'parliament/140',
      mldata: 3,
    },
    {
      attributes: {
        age: 53,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/0d70272a-5caf-4f32-aecb-76277e55999f.jpg?itok=7-i63dPN',
        name: 'Lucille Werner',
        party: 'CDA',
        residence: 'Nederhorst den Berg',
        seniority: 57,
      },
      id: 'parliament/139',
      mldata: 4,
    },
    {
      attributes: {
        age: 44,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/ef8d31db-0876-4998-b0d9-056643260d46.jpg?itok=CQymqLJt',
        name: 'Danai van Weerdenburg',
        party: 'PVV',
        residence: 'Amstelveen',
        seniority: 1526,
      },
      id: 'parliament/137',
      mldata: 1,
    },
    {
      attributes: {
        age: 55,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/d07ef52c-cc10-4445-a3a1-dfe50371f110.jpg?itok=HNVyVKGr',
        name: 'Frank Wassenberg',
        party: 'PvdD',
        residence: 'Geleen',
        seniority: 1862,
      },
      id: 'parliament/136',
      mldata: 5,
    },
    {
      attributes: {
        age: 57,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/19c8d0fb-66ca-4b45-be82-057cb36e1e61.jpg?itok=Bkq8gaTU',
        name: 'Hans Vijlbrief',
        party: 'D66',
        residence: 'Woubrugge',
        seniority: 57,
      },
      id: 'parliament/134',
      mldata: 0,
    },
    {
      attributes: {
        age: 37,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/980a8ae3-b1c7-4912-ab9e-be3e6fd20d36.jpg?itok=GcOX1VP9',
        name: 'Leonie Vestering',
        party: 'PvdD',
        residence: 'Almere',
        seniority: 57,
      },
      id: 'parliament/133',
      mldata: 5,
    },
    {
      attributes: {
        age: 36,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/b625d2bf-da74-40a2-b013-2069a09dcb6c.jpg?itok=zMgno8bi',
        name: 'Peter Valstar',
        party: 'VVD',
        residence: "'s-Gravenzande",
        seniority: 57,
      },
      id: 'parliament/132',
      mldata: 2,
    },
    {
      attributes: {
        age: 49,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/97ffe642-648f-4dab-a0e8-0720e96e3e35.jpg?itok=eDllv9dS',
        name: 'Judith Tielen',
        party: 'VVD',
        residence: 'Utrecht',
        seniority: 1304,
      },
      id: 'parliament/131',
      mldata: 2,
    },
    {
      attributes: {
        age: 47,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/2aeb5ca9-f64f-4ec6-a0ce-83223bff4772.jpg?itok=mS_-AfYP',
        name: 'Joris Thijssen',
        party: 'PvdA',
        residence: 'Muiderberg',
        seniority: 57,
      },
      id: 'parliament/130',
      mldata: 6,
    },
    {
      attributes: {
        age: 35,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/7564c685-2691-4796-9159-ebd5df4fdc8d.jpg?itok=V5oHsfsj',
        name: 'Christine Teunissen',
        party: 'PvdD',
        residence: "'s-Gravenhage",
        seniority: 169,
      },
      id: 'parliament/129',
      mldata: 5,
    },
    {
      attributes: {
        age: 46,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/2538ca6a-8254-4c7e-af71-a20db82afd30.jpg?itok=GvwO8NYO',
        name: 'Ockje Tellegen',
        party: 'VVD',
        residence: "'s-Gravenhage",
        seniority: 3171,
      },
      id: 'parliament/128',
      mldata: 2,
    },
    {
      attributes: {
        age: 44,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/ce400601-651e-4ac9-a43e-ed8d4817b7fd.jpg?itok=wsISYKCx',
        name: 'Pim van Strien',
        party: 'VVD',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/127',
      mldata: 2,
    },
    {
      attributes: {
        age: 52,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/1b59d69d-89a1-42c3-b5bf-52f38c39d88d.jpg?itok=o4nroqLI',
        name: 'Kees van der Staaij',
        party: 'SGP',
        residence: 'Benthuizen',
        seniority: 8409,
      },
      id: 'parliament/125',
      mldata: 7,
    },
    {
      attributes: {
        age: 55,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/b50b3f0b-afc8-46e5-bfda-cb60e2935a5a.jpg?itok=7R0cTE10',
        name: 'Bart Snels',
        party: 'GL',
        residence: 'Utrecht',
        seniority: 1526,
      },
      id: 'parliament/124',
      mldata: 3,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/da9f33b0-80ca-49eb-a1ec-def3b46538d9.jpg?itok=nL6HIBWH',
        name: 'Joost Sneller',
        party: 'D66',
        residence: "'s-Gravenhage",
        seniority: 1304,
      },
      id: 'parliament/123',
      mldata: 0,
    },
    {
      attributes: {
        age: 36,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/6a54c095-9872-4322-81be-114b74233d40.jpg?itok=uf9jc1Gk',
        name: 'Hanneke van der Werf',
        party: 'D66',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/138',
      mldata: 0,
    },
    {
      attributes: {
        age: 60,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/eb79af3c-a66d-4374-b9e7-34d7f3e92dc7.jpg?itok=wMZmTmkc',
        name: 'Hans Smolders',
        party: 'Groep Van Haga',
        residence: 'Tilburg',
        seniority: 309,
      },
      id: 'parliament/122',
      mldata: 8,
    },
    {
      attributes: {
        age: 50,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/0ae6735e-4b1b-4302-a20e-bbe5b72155c3.jpg?itok=C1-541cc',
        name: 'Sylvana Simons',
        party: 'BIJ1',
        residence: 'Duivendrecht',
        seniority: 57,
      },
      id: 'parliament/120',
      mldata: 9,
    },
    {
      attributes: {
        age: 51,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/1bdd5fcc-6231-4781-a4b6-413ae942af01.jpg?itok=PDvY8Xiy',
        name: 'Gert-Jan Segers',
        party: 'CU',
        residence: 'Hoogland',
        seniority: 3171,
      },
      id: 'parliament/119',
      mldata: 10,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/3b8c5482-7bdb-414b-97c7-ca34fb10da6f.jpg?itok=hDNG3GK1',
        name: 'Carola Schouten',
        party: 'CU',
        residence: 'Rotterdam',
        seniority: 2410,
      },
      id: 'parliament/118',
      mldata: 10,
    },
    {
      attributes: {
        age: 54,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8b3664bd-77e4-468b-af96-f3f4ec27fcce.jpg?itok=ofrc9cnP',
        name: 'Mark Rutte',
        party: 'VVD',
        residence: "'s-Gravenhage",
        seniority: 2004,
      },
      id: 'parliament/117',
      mldata: 2,
    },
    {
      attributes: {
        age: 68,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/c60c655f-40ba-47b0-8e93-a2c8d1ff4743.jpg?itok=EBAonTBo',
        name: 'Raymond de Roon',
        party: 'PVV',
        residence: 'Aardenburg',
        seniority: 5292,
      },
      id: 'parliament/116',
      mldata: 1,
    },
    {
      attributes: {
        age: 32,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/4bf69aa9-29a7-44db-b7ee-8bfa40e343dd.jpg?itok=-BKeAg8F',
        name: 'Queeny Rajkowski',
        party: 'VVD',
        residence: 'Utrecht',
        seniority: 57,
      },
      id: 'parliament/115',
      mldata: 2,
    },
    {
      attributes: {
        age: 59,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/5d54939f-2da7-4ca6-b159-4ef344336b21.jpg?itok=Zy7RnTQ2',
        name: 'Lammert van Raan',
        party: 'PvdD',
        residence: 'Amsterdam',
        seniority: 1526,
      },
      id: 'parliament/113',
      mldata: 5,
    },
    {
      attributes: {
        age: 30,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/9f0c39d6-4a57-4849-9215-aa1147c7e0f6.jpg?itok=1JWOuIjk',
        name: 'Nicki Pouw-Verweij',
        party: 'JA21',
        residence: 'Maarssen',
        seniority: 57,
      },
      id: 'parliament/112',
      mldata: 11,
    },
    {
      attributes: {
        chairman: 'Caroline van der Plas',
        chairmanId: 110,
        name: 'BoerBurgerBeweging',
        seats: 1,
      },
      id: 'parties/BBB',
      mldata: 12,
    },
    {
      attributes: {
        age: 53,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/b41b2403-9f06-4d55-adc5-b0777373ca7e.jpg?itok=rmudNELC',
        name: 'Caroline van der Plas',
        party: 'BBB',
        residence: 'Deventer',
        seniority: 57,
      },
      id: 'parliament/110',
      mldata: 12,
    },
    {
      attributes: {
        age: 42,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/d89044ef-b7df-4a15-be10-d342fe381664.jpg?itok=gmiV1adi',
        name: 'Wieke Paulusma',
        party: 'D66',
        residence: 'Groningen',
        seniority: 42,
      },
      id: 'parliament/107',
      mldata: 0,
    },
    {
      attributes: {
        chairman: 'Sylvana Simons',
        chairmanId: 120,
        name: 'BIJ1',
        seats: 1,
      },
      id: 'parties/BIJ1',
      mldata: 9,
    },
    {
      attributes: {
        age: 37,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/2690bccc-5463-459a-b1df-cb93e222b348.jpg?itok=UZOQRvuv',
        name: 'Jan Paternotte',
        party: 'D66',
        residence: 'Leiden',
        seniority: 1526,
      },
      id: 'parliament/105',
      mldata: 0,
    },
    {
      attributes: {
        age: 58,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/16be6093-7884-4d00-ae31-70dc7062f57a.jpg?itok=S9UMNjs4',
        name: 'Lilianne Ploumen',
        party: 'PvdA',
        residence: 'Amsterdam',
        seniority: 1526,
      },
      id: 'parliament/111',
      mldata: 6,
    },
    {
      attributes: {
        age: 44,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/1c16919b-5c3f-486c-9cc7-bdd00e29e9a4.jpg?itok=FJdVr82u',
        name: 'Esther Ouwehand',
        party: 'PvdD',
        residence: 'Leiden',
        seniority: 4956,
      },
      id: 'parliament/103',
      mldata: 5,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/2b223ce1-e251-462b-a5fc-971c92bbf37c.jpg?itok=MD8qI-l1',
        name: 'Daan de Neef',
        party: 'VVD',
        residence: 'Breda',
        seniority: 57,
      },
      id: 'parliament/100',
      mldata: 2,
    },
    {
      attributes: {
        age: 59,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/48d02bab-281c-4bd0-8a50-26bf90a38cc0.jpg?itok=58n_mvgQ',
        name: 'Edgar Mulder',
        party: 'PVV',
        residence: 'Zwolle',
        seniority: 1526,
      },
      id: 'parliament/99',
      mldata: 1,
    },
    {
      attributes: {
        age: 47,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/ee32e8d2-aa15-4603-901e-2cc3c72e9ead.jpg?itok=ZbbSykjQ',
        name: 'Agnes Mulder',
        party: 'CDA',
        residence: 'Assen',
        seniority: 3171,
      },
      id: 'parliament/98',
      mldata: 4,
    },
    {
      attributes: {
        age: 33,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/cd02edbc-106e-46d1-8d70-6594741356ea.jpg?itok=Q9q13ntf',
        name: 'Fahid Minhas',
        party: 'VVD',
        residence: 'Schiedam',
        seniority: 57,
      },
      id: 'parliament/97',
      mldata: 2,
    },
    {
      attributes: {
        age: 32,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/557f8bd5-9f66-4bed-9b5f-86746bb0600a.jpg?itok=zYjIf7tK',
        name: 'Gideon van Meijeren',
        party: 'FVD',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/95',
      mldata: 13,
    },
    {
      attributes: {
        age: 54,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/0e34f732-337a-4339-961f-8c18d68e714d.jpg?itok=FTBMCT9a',
        name: 'Marille Paul',
        party: 'VVD',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/106',
      mldata: 2,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/754f2d87-8990-4b77-8181-54d8df8fa63a.jpg?itok=sRQ4AOwO',
        name: 'Gidi Markuszower',
        party: 'PVV',
        residence: 'Amstelveen',
        seniority: 1526,
      },
      id: 'parliament/93',
      mldata: 1,
    },
    {
      attributes: {
        age: 35,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/197b435e-29b1-4b69-8c6a-b38bc44ffce3.jpg?itok=37gP59C5',
        name: 'Lilian Marijnissen',
        party: 'SP',
        residence: 'Oss',
        seniority: 1526,
      },
      id: 'parliament/92',
      mldata: 14,
    },
    {
      attributes: {
        age: 39,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/bf030048-0434-42f1-bbed-8b0c04014db4.jpg?itok=G35Rmw4M',
        name: 'Sjoerd Sjoerdsma',
        party: 'D66',
        residence: "'s-Gravenhage",
        seniority: 3171,
      },
      id: 'parliament/121',
      mldata: 0,
    },
    {
      attributes: {
        age: 34,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/e5f316f0-e4e7-4cf0-a064-ed9321737487.jpg?itok=hJohvkxD',
        name: 'Vicky Maeijer',
        party: 'PVV',
        residence: 'Krimpen aan den IJssel',
        seniority: 1414,
      },
      id: 'parliament/91',
      mldata: 1,
    },
    {
      attributes: {
        age: 45,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8ba16b4b-5dc1-441e-9229-8bb63c903ce1.jpg?itok=Os2qNhNc',
        name: 'Ren Peters',
        party: 'CDA',
        residence: 'Oss',
        seniority: 1526,
      },
      id: 'parliament/108',
      mldata: 4,
    },
    {
      attributes: {
        age: 31,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/66b2be3d-10f8-46a4-90c6-b86005df0a50.jpg?itok=obcIgEe-',
        name: 'Senna Maatoug',
        party: 'GL',
        residence: 'Leiden',
        seniority: 57,
      },
      id: 'parliament/89',
      mldata: 3,
    },
    {
      attributes: {
        age: 42,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/e6cc253d-14cb-437b-be88-4966f5fcb1f5.jpg?itok=szjd01HY',
        name: 'Renske Leijten',
        party: 'SP',
        residence: 'Haarlem',
        seniority: 5180,
      },
      id: 'parliament/88',
      mldata: 14,
    },
    {
      attributes: {
        age: 56,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/75a44c3a-5914-4c8e-9ca4-d987853f5844.jpg?itok=kA8ZaoNf',
        name: 'Tom van der Lee',
        party: 'GL',
        residence: 'Amsterdam',
        seniority: 1526,
      },
      id: 'parliament/87',
      mldata: 3,
    },
    {
      attributes: {
        age: 41,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/fb291eb2-962a-482f-971a-0b62673f9a86.jpg?itok=2J7OszgV',
        name: 'Jeanet van der Laan',
        party: 'D66',
        residence: 'Lisse',
        seniority: 57,
      },
      id: 'parliament/86',
      mldata: 0,
    },
    {
      attributes: {
        age: 36,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/56a86879-60e4-4cdb-b5f1-b66d6d090798.jpg?itok=j-6SlXLZ',
        name: 'Peter Kwint',
        party: 'SP',
        residence: 'Amsterdam',
        seniority: 1526,
      },
      id: 'parliament/85',
      mldata: 14,
    },
    {
      attributes: {
        age: 39,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/16596afa-9e56-4d0a-a715-f40592825937.jpg?itok=xVPho7Yp',
        name: 'Tunahan Kuzu',
        party: 'DENK',
        residence: 'Rotterdam',
        seniority: 3171,
      },
      id: 'parliament/84',
      mldata: 15,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/4c3e51e6-9f1e-49cc-a919-f74a8c88343e.jpg?itok=IkU-tMGy',
        name: 'Attje Kuiken',
        party: 'PvdA',
        residence: 'Breda',
        seniority: 5180,
      },
      id: 'parliament/83',
      mldata: 6,
    },
    {
      attributes: {
        age: 34,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/88bf9f29-aa91-4ddd-ad2d-b73dfe6706ef.jpg?itok=qH6hnVtR',
        name: 'Anne Kuik',
        party: 'CDA',
        residence: 'Groningen',
        seniority: 1526,
      },
      id: 'parliament/82',
      mldata: 4,
    },
    {
      attributes: {
        age: 28,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8767e9de-869e-4e8a-940f-1a63d15eab3c.jpg?itok=tdoE8yQX',
        name: 'Daan de Kort',
        party: 'VVD',
        residence: 'Veldhoven',
        seniority: 57,
      },
      id: 'parliament/81',
      mldata: 2,
    },
    {
      attributes: {
        age: 39,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/88e0734b-8c03-4daa-8ec5-055e36c07030.jpg?itok=_ZteUeWk',
        name: 'Daniel Koerhuis',
        party: 'VVD',
        residence: 'Raalte',
        seniority: 1526,
      },
      id: 'parliament/79',
      mldata: 2,
    },
    {
      attributes: {
        age: 32,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/d34e1efc-fbba-4ad8-823a-61bc3f25d580.jpg?itok=WXFGv3_8',
        name: 'Marieke Koekkoek',
        party: 'Volt',
        residence: 'Utrecht',
        seniority: 57,
      },
      id: 'parliament/78',
      mldata: 16,
    },
    {
      attributes: {
        age: 49,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8dfa0156-0834-4c00-87e8-c4e9919f781f.jpg?itok=G8DwmYfZ',
        name: 'Raymond Knops',
        party: 'CDA',
        residence: 'Hegelsom',
        seniority: 4282,
      },
      id: 'parliament/77',
      mldata: 4,
    },
    {
      attributes: {
        age: 35,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/0d036fcf-a456-4798-9b76-3f44eb74a451.jpg?itok=g-X9uPV-',
        name: 'Jesse Klaver',
        party: 'GL',
        residence: "'s-Gravenhage",
        seniority: 3997,
      },
      id: 'parliament/76',
      mldata: 3,
    },
    {
      attributes: {
        age: 37,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/0137f25d-849f-428a-b69b-822301b2274e.jpg?itok=w8fa-kV7',
        name: 'Simone Kerseboom',
        party: 'FVD',
        residence: 'Maastricht',
        seniority: 57,
      },
      id: 'parliament/75',
      mldata: 13,
    },
    {
      attributes: {
        age: 37,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/196d0048-0369-4786-ba81-bda9da8c3636.jpg?itok=OeUuyimA',
        name: 'Bart van Kent',
        party: 'SP',
        residence: "'s-Gravenhage",
        seniority: 1526,
      },
      id: 'parliament/74',
      mldata: 14,
    },
    {
      attributes: {
        age: 52,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8c5a8186-8f46-4bca-956d-06d4fa521635.jpg?itok=dIksPa3Q',
        name: 'Mona Keijzer',
        party: 'CDA',
        residence: 'Edam',
        seniority: 1919,
      },
      id: 'parliament/73',
      mldata: 4,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/9e79de98-e914-4dc8-8dc7-6d7cb09b93d7.jpg?itok=S7MgISCL',
        name: 'Barbara Kathmann',
        party: 'PvdA',
        residence: 'Rotterdam',
        seniority: 57,
      },
      id: 'parliament/72',
      mldata: 6,
    },
    {
      attributes: {
        age: 37,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8349fff0-99b3-449a-bd4d-932ae95b29da.jpg?itok=xYICmuaX',
        name: 'Hlya Kat',
        party: 'D66',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/71',
      mldata: 0,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/f23be4bd-7b2b-45dd-be84-4f76f11b624a.jpg?itok=DsfJ-8o0',
        name: 'Roelien Kamminga',
        party: 'VVD',
        residence: 'Groningen',
        seniority: 57,
      },
      id: 'parliament/70',
      mldata: 2,
    },
    {
      attributes: {
        age: 59,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/64457f28-37b6-4948-860a-f28bc62b99ae.jpg?itok=Q2hiR0vj',
        name: 'Sigrid Kaag',
        party: 'D66',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/69',
      mldata: 0,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/92fe14f9-9abe-4759-b311-8b635302c2f2.jpg?itok=h4S-TKy9',
        name: 'Lon de Jong',
        party: 'PVV',
        residence: "'s-Gravenhage",
        seniority: 2352,
      },
      id: 'parliament/67',
      mldata: 1,
    },
    {
      attributes: {
        age: 34,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/49be3576-cea3-46c0-87eb-89beb108248d.jpg?itok=VfqikY8P',
        name: 'Rob Jetten',
        party: 'D66',
        residence: 'Ubbergen',
        seniority: 1527,
      },
      id: 'parliament/66',
      mldata: 0,
    },
    {
      attributes: {
        age: 28,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/359e90b6-6f1f-47e9-a46b-d0ff54836077.jpg?itok=-JPcY_Gs',
        name: 'Freek Jansen',
        party: 'FVD',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/65',
      mldata: 13,
    },
    {
      attributes: {
        age: 49,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/7954eb2c-1bf5-42af-80dc-85f29b367db0.jpg?itok=9ucjAERV',
        name: 'Folkert Idsinga',
        party: 'VVD',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/64',
      mldata: 2,
    },
    {
      attributes: {
        age: 23,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/e70c3ee3-cca2-4521-abea-b124dcec0952.jpg?itok=uYC9dgfP',
        name: 'Habtamu de Hoop',
        party: 'PvdA',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/62',
      mldata: 6,
    },
    {
      attributes: {
        age: 45,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/bf8eb79e-5034-4882-a4c5-413baba477d5.jpg?itok=Tz1DdDDy',
        name: 'Wopke Hoekstra',
        party: 'CDA',
        residence: 'Bussum',
        seniority: 57,
      },
      id: 'parliament/61',
      mldata: 4,
    },
    {
      attributes: {
        age: 42,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a437e923-725f-415e-a01d-5588a7af933e.jpg?itok=eLyHy8KA',
        name: 'Kati Piri',
        party: 'PvdA',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/109',
      mldata: 6,
    },
    {
      attributes: {
        age: 44,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/973da897-66c7-4010-a98d-505e0e97a60c.jpg?itok=HLNJtdsA',
        name: 'Ingrid Michon-Derkzen',
        party: 'VVD',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/96',
      mldata: 2,
    },
    {
      attributes: {
        age: 53,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8509e44d-0f9b-413a-a2cd-f64d656c3955.jpg?itok=Iy2_G4Jx',
        name: 'Jacqueline van den Hil',
        party: 'VVD',
        residence: 'Goes',
        seniority: 57,
      },
      id: 'parliament/60',
      mldata: 2,
    },
    {
      attributes: {
        age: 40,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/b8663b71-f92b-4d2b-a06b-f78dba168d98.jpg?itok=UvSjs0bK',
        name: 'Sophie Hermans',
        party: 'VVD',
        residence: 'Amsterdam',
        seniority: 1526,
      },
      id: 'parliament/58',
      mldata: 2,
    },
    {
      attributes: {
        age: 48,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/d1bb566e-a111-43b7-8856-273ebac1f753.jpg?itok=-k-RCynx',
        name: 'Steven van Weyenberg',
        party: 'D66',
        residence: "'s-Gravenhage",
        seniority: 3171,
      },
      id: 'parliament/141',
      mldata: 0,
    },
    {
      attributes: {
        age: 41,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/356e5fc9-831f-4d9c-be2f-fd35a99e865b.jpg?itok=J3UCVuiO',
        name: 'Hilde Palland',
        party: 'CDA',
        residence: 'Kampen',
        seniority: 729,
      },
      id: 'parliament/104',
      mldata: 4,
    },
    {
      attributes: {
        age: 47,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/74b44968-d1f2-482a-a365-1fbb4cbda7f9.jpg?itok=JxDjWGRZ',
        name: 'Lilian Helder',
        party: 'PVV',
        residence: 'Venlo',
        seniority: 3997,
      },
      id: 'parliament/57',
      mldata: 1,
    },
    {
      attributes: {
        age: 40,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a3b17348-54cf-4bca-82d4-e62bd46c2fbc.jpg?itok=MF1OOpeK',
        name: 'Eelco Heinen',
        party: 'VVD',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/56',
      mldata: 2,
    },
    {
      attributes: {
        age: 36,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/77c541d2-49e3-483f-869d-134df420c5ea.jpg?itok=Hju09emt',
        name: 'Alexander Kops',
        party: 'PVV',
        residence: 'Overasselt',
        seniority: 1526,
      },
      id: 'parliament/80',
      mldata: 1,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/cf4c4d24-798d-4bbe-bf06-23ca674f0d28.jpg?itok=7XPnMkfR',
        name: 'Pieter Heerma',
        party: 'CDA',
        residence: 'Purmerend',
        seniority: 3171,
      },
      id: 'parliament/55',
      mldata: 4,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/522d59a0-10ba-488f-a27a-a1dda7581900.jpg?itok=5b_3G5nE',
        name: 'Rudmer Heerema',
        party: 'VVD',
        residence: 'Alkmaar',
        seniority: 2601,
      },
      id: 'parliament/54',
      mldata: 2,
    },
    {
      attributes: {
        age: 37,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/666f31ca-041f-466d-9e78-804317921d69.jpg?itok=G3ozh6C3',
        name: 'Romke de Jong',
        party: 'D66',
        residence: 'Gorredijk',
        seniority: 57,
      },
      id: 'parliament/68',
      mldata: 0,
    },
    {
      attributes: {
        age: 39,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/03d66e4a-8698-44fb-9c29-625630da268c.jpg?itok=hZ9q6Llc',
        name: 'Alexander Hammelburg',
        party: 'D66',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/52',
      mldata: 0,
    },
    {
      attributes: {
        age: 42,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/581d8f64-6060-4cf0-b281-a46ad978cf0d.jpg?itok=hdjgBh_P',
        name: "Bas van 't Wout",
        party: 'VVD',
        residence: 'Hoeven',
        seniority: 2899,
      },
      id: 'parliament/146',
      mldata: 2,
    },
    {
      attributes: {
        age: 34,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/425ddb12-0b71-44b6-9875-70b075dc99f7.jpg?itok=txVxBI0l',
        name: 'Kiki Hagen',
        party: 'D66',
        residence: 'Mijdrecht',
        seniority: 57,
      },
      id: 'parliament/51',
      mldata: 0,
    },
    {
      attributes: {
        age: 54,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/ab15ce32-a527-4526-a5b2-0f2cdb92feeb.jpg?itok=rPcdqouS',
        name: 'Wybren van Haga',
        party: 'Groep Van Haga',
        residence: 'Haarlem',
        seniority: 1305,
      },
      id: 'parliament/50',
      mldata: 8,
    },
    {
      attributes: {
        chairman: 'Liane den Haan',
        chairmanId: 49,
        name: 'Fractie Den Haan',
        seats: 1,
      },
      id: 'parties/FractieDenHaan',
      mldata: 17,
    },
    {
      attributes: {
        age: 53,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/3e48d4c9-0d66-4b50-9f9c-969b5e314a42.jpg?itok=0wlX1bse',
        name: 'Liane den Haan',
        party: 'Fractie Den Haan',
        residence: 'Woerden',
        seniority: 58,
      },
      id: 'parliament/49',
      mldata: 17,
    },
    {
      attributes: {
        age: 65,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/17f26a48-9f54-42a1-9890-c6da761a9a2f.jpg?itok=BA8JoO5G',
        name: 'Paul van Meenen',
        party: 'D66',
        residence: 'Leiden',
        seniority: 3171,
      },
      id: 'parliament/94',
      mldata: 0,
    },
    {
      attributes: {
        age: 53,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/762e0f40-f100-4896-85a2-b1d8818fae41.jpg?itok=YM5w7UBV',
        name: 'Tjeerd de Groot',
        party: 'D66',
        residence: 'Haarlem',
        seniority: 1525,
      },
      id: 'parliament/47',
      mldata: 0,
    },
    {
      attributes: {
        age: 41,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a0b48551-42b7-4c55-9556-6b997d978ff2.jpg?itok=-A3mAzHe',
        name: 'Peter de Groot',
        party: 'VVD',
        residence: 'Harderwijk',
        seniority: 57,
      },
      id: 'parliament/46',
      mldata: 2,
    },
    {
      attributes: {
        age: 41,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/d38ca3cf-9803-48dc-8592-c67c4430168e.jpg?itok=ThZ4_VyE',
        name: 'Pieter Grinwis',
        party: 'CU',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/45',
      mldata: 10,
    },
    {
      attributes: {
        age: 35,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/c3b1dca8-2cb4-4025-9527-36163a098c1f.jpg?itok=3BnXwQgy',
        name: 'Dennis Wiersma',
        party: 'VVD',
        residence: 'De Bilt',
        seniority: 1526,
      },
      id: 'parliament/142',
      mldata: 2,
    },
    {
      attributes: {
        age: 54,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/819764fa-3a17-467b-8327-30899ed083b6.jpg?itok=WwLyP7W7',
        name: 'Dion Graus',
        party: 'PVV',
        residence: 'Heerlen',
        seniority: 5292,
      },
      id: 'parliament/44',
      mldata: 1,
    },
    {
      attributes: {
        age: 52,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/7c43c8c2-1958-496c-8060-b0f3bf652992.jpg?itok=BmeZkJ4o',
        name: 'Barry Madlener',
        party: 'PVV',
        residence: 'Rockanje',
        seniority: 4128,
      },
      id: 'parliament/90',
      mldata: 1,
    },
    {
      attributes: {
        age: 51,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/75c8786e-c5c2-4d12-95ff-4b804234f062.jpg?itok=oyzQt1wF',
        name: 'Machiel de Graaf',
        party: 'PVV',
        residence: "'s-Gravenhage",
        seniority: 3171,
      },
      id: 'parliament/43',
      mldata: 1,
    },
    {
      attributes: {
        age: 48,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/49e7fc50-90f5-4476-9275-c31f88864082.jpg?itok=n7ZLWx8e',
        name: 'Lisa van Ginneken',
        party: 'D66',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/42',
      mldata: 0,
    },
    {
      attributes: {
        age: 41,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/64f39510-b420-4b01-8b5e-2b2f2b0aad91.jpg?itok=P-qrjpbk',
        name: 'Pepijn van Houwelingen',
        party: 'FVD',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/63',
      mldata: 13,
    },
    {
      attributes: {
        age: 50,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/10870c31-0253-4fa0-a2bc-1a6d3c553028.jpg?itok=MJad5pMA',
        name: 'Jaco Geurts',
        party: 'CDA',
        residence: 'Voorthuizen',
        seniority: 3171,
      },
      id: 'parliament/41',
      mldata: 4,
    },
    {
      attributes: {
        age: 49,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/84e7ec53-7bec-491e-bec7-19b6b1d359b1.jpg?itok=cb9yTS_u',
        name: 'Sietse Fritsma',
        party: 'PVV',
        residence: "'s-Gravenhage",
        seniority: 4987,
      },
      id: 'parliament/40',
      mldata: 1,
    },
    {
      attributes: {
        chairman: 'Esther Ouwehand',
        chairmanId: 103,
        name: 'Partij voor de Dieren',
        seats: 6,
      },
      id: 'parties/PvdD',
      mldata: 5,
    },
    {
      attributes: {
        age: 35,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a8e4581d-5186-4367-95c5-0a754c60b276.jpg?itok=aTE5WY1R',
        name: 'Eva van Esch',
        party: 'PvdD',
        residence: 'Utrecht',
        seniority: 596,
      },
      id: 'parliament/39',
      mldata: 5,
    },
    {
      attributes: {
        age: 30,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/9c8bc5bc-8d13-4c11-be37-57a43a1d734b.jpg?itok=TqEaAJCz',
        name: 'Silvio Erkens',
        party: 'VVD',
        residence: 'Kerkrade',
        seniority: 57,
      },
      id: 'parliament/38',
      mldata: 2,
    },
    {
      attributes: {
        age: 62,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/6e4a8d23-9d3b-4565-a3ab-086eb51d6c08.jpg?itok=RSKd0BLe',
        name: 'Derk Jan  Eppink',
        party: 'JA21',
        residence: "'s-Gravenhage",
        seniority: 57,
      },
      id: 'parliament/37',
      mldata: 11,
    },
    {
      attributes: {
        chairman: 'Wybren van Haga',
        chairmanId: 50,
        name: 'Groep Van Haga',
        seats: 3,
      },
      id: 'parties/GroepVanHaga',
      mldata: 8,
    },
    {
      attributes: {
        age: 55,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/d59d1492-57b6-482b-88b6-b2b77c45c33d.jpg?itok=g4RdRfw2',
        name: 'Olaf Ephraim',
        party: 'Groep Van Haga',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/36',
      mldata: 8,
    },
    {
      attributes: {
        age: 32,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/ea21d788-7cfe-41ff-b22f-0664397139af.jpg?itok=4G8NOaak',
        name: 'Ulysse Ellian',
        party: 'VVD',
        residence: 'Almere',
        seniority: 57,
      },
      id: 'parliament/35',
      mldata: 2,
    },
    {
      attributes: {
        age: 45,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/551f9a19-738c-4298-afe1-5ca7959ab74b.jpg?itok=24RXpYrd',
        name: 'Corinne Ellemeet',
        party: 'GL',
        residence: 'Abcoude',
        seniority: 1637,
      },
      id: 'parliament/34',
      mldata: 3,
    },
    {
      attributes: {
        age: 41,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/c4f7c468-721e-4953-97f2-bff05fe570c4.jpg?itok=MmXfOgJx',
        name: 'Zohair El Yassini',
        party: 'VVD',
        residence: 'Utrecht',
        seniority: 1526,
      },
      id: 'parliament/33',
      mldata: 2,
    },
    {
      attributes: {
        chairman: 'Joost Eerdmans',
        chairmanId: 32,
        name: 'JA21',
        seats: 3,
      },
      id: 'parties/JA21',
      mldata: 11,
    },
    {
      attributes: {
        age: 50,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/c6e26bd3-724f-45d3-93af-ef431e6fcd42.jpg?itok=k6ziMwhD',
        name: 'Joost Eerdmans',
        party: 'JA21',
        residence: 'Rotterdam',
        seniority: 1709,
      },
      id: 'parliament/32',
      mldata: 11,
    },
    {
      attributes: {
        age: 50,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/2b533923-83a2-4ac6-b497-934a549b50ab.jpg?itok=-vG3IC_G',
        name: 'Jasper van Dijk',
        party: 'SP',
        residence: 'Utrecht',
        seniority: 5292,
      },
      id: 'parliament/31',
      mldata: 14,
    },
    {
      attributes: {
        age: 46,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/1ee767b9-67f3-47e5-9e28-045da1c37b05.jpg?itok=mL3_rW1Z',
        name: 'Inge van Dijk',
        party: 'CDA',
        residence: 'Gemert',
        seniority: 57,
      },
      id: 'parliament/30',
      mldata: 4,
    },
    {
      attributes: {
        age: 56,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/b8d002bc-ea6c-4f57-b36e-38415b205e09.jpg?itok=tHQU-Qrn',
        name: 'Aukje de Vries',
        party: 'VVD',
        residence: 'Leeuwarden',
        seniority: 3122,
      },
      id: 'parliament/135',
      mldata: 2,
    },
    {
      attributes: {
        age: 40,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/cabae19e-836c-48c4-903e-7357789f9260.jpg?itok=v6jbBdci',
        name: 'Gijs van Dijk',
        party: 'PvdA',
        residence: 'Den Burg',
        seniority: 1526,
      },
      id: 'parliament/29',
      mldata: 6,
    },
    {
      attributes: {
        age: 57,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a396ae20-6309-456b-a5ae-e4068dea8a67.jpg?itok=TsprNWzX',
        name: 'Tony van Dijck',
        party: 'PVV',
        residence: "'s-Gravenhage",
        seniority: 5292,
      },
      id: 'parliament/28',
      mldata: 1,
    },
    {
      attributes: {
        chairman: 'Laurens Dassen',
        chairmanId: 27,
        name: 'Volt',
        seats: 3,
      },
      id: 'parties/Volt',
      mldata: 16,
    },
    {
      attributes: {
        age: 31,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/7cbb2d95-46b2-4fcb-b129-9505f56e7f07.jpg?itok=QbPgLPWt',
        name: 'Don Ceder',
        party: 'CU',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/26',
      mldata: 10,
    },
    {
      attributes: {
        age: 31,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/bf494e35-c009-4c74-a5c6-3cb2cd26bb51.jpg?itok=2CTfrPhP',
        name: 'Thom van Campen',
        party: 'VVD',
        residence: 'Zwolle',
        seniority: 57,
      },
      id: 'parliament/25',
      mldata: 2,
    },
    {
      attributes: {
        age: 51,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/ccea26dd-dcae-4174-8945-ec5cb568e951.jpg?itok=z7gsBQYd',
        name: 'Laura Bromet',
        party: 'GL',
        residence: 'Monnickendam',
        seniority: 1085,
      },
      id: 'parliament/24',
      mldata: 3,
    },
    {
      attributes: {
        age: 42,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/e85125f3-b7cf-426c-9355-412838893f55.jpg?itok=Qs1_f_yV',
        name: 'Faissal Boulakjar',
        party: 'D66',
        residence: 'Teteringen',
        seniority: 57,
      },
      id: 'parliament/22',
      mldata: 0,
    },
    {
      attributes: {
        age: 51,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/8ee20f38-f3f0-4ff9-b5d9-84557b5ddcb1.jpg?itok=ExYCR1Ti',
        name: 'Hatte van der Woude',
        party: 'VVD',
        residence: 'Delft',
        seniority: 57,
      },
      id: 'parliament/145',
      mldata: 2,
    },
    {
      attributes: {
        age: 45,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/13e26587-88d8-440b-bb93-527a8e845342.jpg?itok=18OPNvjY',
        name: 'Raoul Boucke',
        party: 'D66',
        residence: 'Rotterdam',
        seniority: 57,
      },
      id: 'parliament/21',
      mldata: 0,
    },
    {
      attributes: {
        chairman: 'Jesse Klaver',
        chairmanId: 76,
        name: 'GroenLinks',
        seats: 8,
      },
      id: 'parties/GL',
      mldata: 3,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/9ab41898-332f-4c08-a834-d58b4669c990.jpg?itok=pbUPRriP',
        name: 'Dilan Yeilgz-Zegerius',
        party: 'VVD',
        residence: 'Amsterdam',
        seniority: 1526,
      },
      id: 'parliament/148',
      mldata: 2,
    },
    {
      attributes: {
        age: 46,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/98aa8bde-85b0-49ed-9795-e2eeb206e3b8.jpg?itok=CODn43Bf',
        name: 'Chris Stoffer',
        party: 'SGP',
        residence: 'Elspeet',
        seniority: 1142,
      },
      id: 'parliament/126',
      mldata: 7,
    },
    {
      attributes: {
        age: 27,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/dab4cf29-4843-4261-b09b-973fe98dbf65.jpg?itok=szVeNy9I',
        name: 'Kauthar Bouchallikh',
        party: 'GL',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/20',
      mldata: 3,
    },
    {
      attributes: {
        age: 32,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/1e8aa099-9c94-432d-bff7-ac9fe20ff3ff.jpg?itok=lwMs1157',
        name: 'Derk Boswijk',
        party: 'CDA',
        residence: 'Kockengen',
        seniority: 57,
      },
      id: 'parliament/19',
      mldata: 4,
    },
    {
      attributes: {
        age: 56,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/034829b1-019f-4ee2-8961-83e82ac2eaa3.jpg?itok=bvOgE5s3',
        name: 'Martin Bosma',
        party: 'PVV',
        residence: 'Amsterdam',
        seniority: 5292,
      },
      id: 'parliament/18',
      mldata: 1,
    },
    {
      attributes: {
        chairman: 'Kees van der Staaij',
        chairmanId: 125,
        name: 'Staatkundig Gereformeerde Partij',
        seats: 3,
      },
      id: 'parties/SGP',
      mldata: 7,
    },
    {
      attributes: {
        age: 64,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/9b13c8c8-eba4-411e-b095-858e352d3178.jpg?itok=qcZcaIXc',
        name: 'Roelof Bisschop',
        party: 'SGP',
        residence: 'Veenendaal',
        seniority: 3171,
      },
      id: 'parliament/17',
      mldata: 7,
    },
    {
      attributes: {
        age: 35,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a68468b6-51ea-440c-9387-b44ba10ad8b6.jpg?itok=UYMugaP_',
        name: 'Laurens Dassen',
        party: 'Volt',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/27',
      mldata: 16,
    },
    {
      attributes: {
        chairman: 'Gert-Jan Segers',
        chairmanId: 119,
        name: 'ChristenUnie',
        seats: 5,
      },
      id: 'parties/CU',
      mldata: 10,
    },
    {
      attributes: {
        age: 30,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/6fc5878e-d76a-4e9d-a4e3-e5ef581bb2ff.jpg?itok=qf_oudwu',
        name: 'Rens Raemakers',
        party: 'D66',
        residence: 'Neer',
        seniority: 1414,
      },
      id: 'parliament/114',
      mldata: 0,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/b53f74bf-e57c-4e01-8d65-de2f319f44db.jpg?itok=5kdTqzlc',
        name: 'Mirjam Bikker',
        party: 'CU',
        residence: 'Gouda',
        seniority: 57,
      },
      id: 'parliament/16',
      mldata: 10,
    },
    {
      attributes: {
        age: 49,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a04d4b85-56f0-448f-b5e8-28b5340af204.jpg?itok=KiulCSRP',
        name: 'Marijke van Beukering-Huijbregts',
        party: 'D66',
        residence: 'IJsselstein',
        seniority: 463,
      },
      id: 'parliament/15',
      mldata: 0,
    },
    {
      attributes: {
        age: 49,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/aa0272f6-6e3c-41f7-b040-b774b30d2196.jpg?itok=IQu5H4Bx',
        name: 'Vera Bergkamp',
        party: 'D66',
        residence: 'Amsterdam',
        seniority: 3171,
      },
      id: 'parliament/14',
      mldata: 0,
    },
    {
      attributes: {
        age: 62,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/c0d8ed4a-ec11-48e2-a468-6cd93327305b.jpg?itok=1hk9sTXT',
        name: 'Joba van den Berg',
        party: 'CDA',
        residence: 'Utrecht',
        seniority: 1497,
      },
      id: 'parliament/13',
      mldata: 4,
    },
    {
      attributes: {
        chairman: 'Rob Jetten',
        chairmanId: 66,
        name: 'Democraten 66',
        seats: 24,
      },
      id: 'parties/D66',
      mldata: 0,
    },
    {
      attributes: {
        age: 42,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/29124ef6-e1d5-4138-bf01-8433afde7269.jpg?itok=qw0TE4jl',
        name: 'Salima Belhaj',
        party: 'D66',
        residence: 'Rotterdam',
        seniority: 1948,
      },
      id: 'parliament/12',
      mldata: 0,
    },
    {
      attributes: {
        age: 69,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/b9cfefa1-1e5e-4fd7-a9d5-458e4c3a20db.jpg?itok=hMEE5GW1',
        name: 'Harm Beertema',
        party: 'PVV',
        residence: 'Voorburg',
        seniority: 3997,
      },
      id: 'parliament/11',
      mldata: 1,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/0ea8e29d-3bfe-4982-8594-9350b527d553.jpg?itok=84_wILdo',
        name: 'Sandra Beckerman',
        party: 'SP',
        residence: 'Groningen',
        seniority: 1526,
      },
      id: 'parliament/10',
      mldata: 14,
    },
    {
      attributes: {
        age: 35,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/a962ffcb-a300-45e1-8850-70c7173c233e.jpg?itok=0f-c2lGz',
        name: 'Bente Becker',
        party: 'VVD',
        residence: "'s-Gravenhage",
        seniority: 1414,
      },
      id: 'parliament/9',
      mldata: 2,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/fdc445bd-f8fe-4cfc-b387-7fd66a181674.jpg?itok=KE1zGYV2',
        name: 'Michiel van Nispen',
        party: 'SP',
        residence: 'Breda',
        seniority: 2612,
      },
      id: 'parliament/102',
      mldata: 14,
    },
    {
      attributes: {
        age: 43,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/f31cabe4-e1f4-46db-88a9-b46ba44dded4.jpg?itok=FfoXTIug',
        name: 'Nilfer  Gndoan',
        party: 'Volt',
        residence: 'Amsterdam',
        seniority: 57,
      },
      id: 'parliament/48',
      mldata: 16,
    },
    {
      attributes: {
        chairman: 'Thierry Baudet',
        chairmanId: 8,
        name: 'Forum voor Democratie',
        seats: 5,
      },
      id: 'parties/FVD',
      mldata: 13,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/016247cb-6d9f-408a-9581-ae3769016076.jpg?itok=DLaW69dQ',
        name: 'Maarten Hijink',
        party: 'SP',
        residence: 'Amersfoort',
        seniority: 1526,
      },
      id: 'parliament/59',
      mldata: 14,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/bb3a9cf3-6f64-4092-85a1-4318244ee603.jpg?itok=3GO5pzB9',
        name: 'Thierry Baudet',
        party: 'FVD',
        residence: 'Amsterdam',
        seniority: 1526,
      },
      id: 'parliament/8',
      mldata: 13,
    },
    {
      attributes: {
        age: 34,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/e5c4753f-d2d8-4a4a-ae9d-62118892a731.jpg?itok=iGnPsRMX',
        name: 'Ruben Brekelmans',
        party: 'VVD',
        residence: 'Oisterwijk',
        seniority: 57,
      },
      id: 'parliament/23',
      mldata: 2,
    },
    {
      attributes: {
        age: 29,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/859e1824-cc60-4304-9293-dca932ebbed2.jpg?itok=WSmCeVtj',
        name: 'Stephan van Baarle',
        party: 'DENK',
        residence: 'Rotterdam',
        seniority: 57,
      },
      id: 'parliament/7',
      mldata: 15,
    },
    {
      attributes: {
        chairman: 'Farid Azarkan',
        chairmanId: 6,
        name: 'DENK',
        seats: 3,
      },
      id: 'parties/DENK',
      mldata: 15,
    },
    {
      attributes: {
        age: 49,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/09155809-0b6e-4f9f-a75b-026aa4b35588.jpg?itok=oVDwOcW4',
        name: 'Farid Azarkan',
        party: 'DENK',
        residence: 'Culemborg',
        seniority: 1526,
      },
      id: 'parliament/6',
      mldata: 15,
    },
    {
      attributes: {
        age: 46,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/7f3c60c7-b846-4f3a-a9f4-a18abf37bb3a.jpg?itok=tHWW3DRc',
        name: 'Tamara van Ark',
        party: 'VVD',
        residence: 'Nieuwerkerk aan den IJssel',
        seniority: 2745,
      },
      id: 'parliament/5',
      mldata: 2,
    },
    {
      attributes: {
        chairman: 'Lilianne Ploumen',
        chairmanId: 111,
        name: 'Partij van de Arbeid',
        seats: 9,
      },
      id: 'parties/PvdA',
      mldata: 6,
    },
    {
      attributes: {
        age: 60,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/ec571f94-11b8-47d7-bb47-fd0d2d7c7f07.jpg?itok=NySCDfzP',
        name: 'Khadija Arib',
        party: 'PvdA',
        residence: 'Amsterdam',
        seniority: 8318,
      },
      id: 'parliament/4',
      mldata: 6,
    },
    {
      attributes: {
        age: 52,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/5a2cb9b8-4a4c-4860-8793-59da092434b6.jpg?itok=IqOn-lRI',
        name: 'Mark Harbers',
        party: 'VVD',
        residence: 'Rotterdam',
        seniority: 3602,
      },
      id: 'parliament/53',
      mldata: 2,
    },
    {
      attributes: {
        chairman: 'Wopke Hoekstra',
        chairmanId: 61,
        name: 'Christen-Democratisch Appl',
        seats: 15,
      },
      id: 'parties/CDA',
      mldata: 4,
    },
    {
      attributes: {
        age: 50,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/1e4f3007-3e72-4135-80e0-238966189fd4.jpg?itok=9sVkmMbI',
        name: 'Mustafa Amhaouch',
        party: 'CDA',
        residence: 'Panningen',
        seniority: 1962,
      },
      id: 'parliament/3',
      mldata: 4,
    },
    {
      attributes: {
        chairman: 'Lilian Marijnissen',
        chairmanId: 92,
        name: 'Socialistische Partij',
        seats: 9,
      },
      id: 'parties/SP',
      mldata: 14,
    },
    {
      attributes: {
        age: 32,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/bb6801d4-f671-4ade-aa86-29aa89373dd9.jpg?itok=KTSPZU51',
        name: 'Mahir Alkaya',
        party: 'SP',
        residence: 'Amsterdam',
        seniority: 1225,
      },
      id: 'parliament/2',
      mldata: 14,
    },
    {
      attributes: {
        age: 38,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/44081df1-828e-4d79-bd27-261be38f66f1.jpg?itok=qP7EAsSN',
        name: 'Henk Nijboer',
        party: 'PvdA',
        residence: 'Groningen',
        seniority: 3171,
      },
      id: 'parliament/101',
      mldata: 6,
    },
    {
      attributes: {
        chairman: 'Geert Wilders',
        chairmanId: 144,
        name: 'Partij voor de Vrijheid',
        seats: 17,
      },
      id: 'parties/PVV',
      mldata: 1,
    },
    {
      attributes: {
        age: 44,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/1c889902-6ede-427c-8682-000f683fffaa.jpg?itok=LJy2dsN1',
        name: 'Fleur Agema',
        party: 'PVV',
        residence: "'s-Gravenhage",
        seniority: 5180,
      },
      id: 'parliament/1',
      mldata: 1,
    },
    {
      attributes: {
        chairman: 'Mark Rutte',
        chairmanId: 117,
        name: 'Volkspartij voor Vrijheid en Democratie',
        seats: 34,
      },
      id: 'parties/VVD',
      mldata: 2,
    },
    {
      attributes: {
        age: 31,
        img: 'https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/397c857a-fda0-414d-8fdc-8288cd3284aa.jpg?itok=55l5zRvr',
        name: 'Thierry Aartsen',
        party: 'VVD',
        residence: 'Breda',
        seniority: 987,
      },
      id: 'parliament/0',
      mldata: 2,
    },
  ],
};
