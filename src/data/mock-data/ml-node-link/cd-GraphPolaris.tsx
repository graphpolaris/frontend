/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
export default {
  edges: [],
  nodes: [
    {
      attributes: {
        name: 'Peter',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/1',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Tinus',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/2',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Pascalle',
        sex: 'Female',
        team: 'GravenVanPolaris',
      },
      id: 'gp/3',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Wesley',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/4',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Wietse',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/5',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Wouter',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/6',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Douwe',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/7',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Aischa',
        sex: 'Female',
        team: 'GravenVanPolaris',
      },
      id: 'gp/8',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Sander',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/9',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Joes',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/10',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Kieran',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/11',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Nyno',
        sex: 'Male',
        team: 'GravenVanPolaris',
      },
      id: 'gp/12',
      mldata: 1,
    },
    {
      attributes: {
        name: 'Emma',
        sex: 'Female',
        team: 'GravenVanPolaris',
      },
      id: 'gp/13',
      mldata: 1,
    },
    {
      attributes: {
        name: 'Michael',
        sex: 'Male',
        team: 'GraphPolaris',
      },
      id: 'gpt/14',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Thijs',
        sex: 'Male',
        team: 'GraphPolaris',
      },
      id: 'gpt/15',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Sivan',
        sex: 'Male',
        team: 'GraphPolaris',
      },
      id: 'gp/16',
      mldata: 0,
    },
    {
      attributes: {
        name: 'Joris',
        sex: 'Male',
        team: 'GraphPolaris',
      },
      id: 'gp/17',
      mldata: 0,
    },
  ],
  linkPrediction: false,
  shortestPath: false,
  communityDetection: true,
  numberOfMlClusters: 2,
};
