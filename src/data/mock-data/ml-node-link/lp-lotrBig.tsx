/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

// Mock data with link prediction machine learning data
export default {
  edges: [
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/169',
      id: 'has_relation/2922345',
      to: 'character/167',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/169',
      id: 'has_relation/2921913',
      to: 'character/156',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/168',
      id: 'has_relation/2922665',
      to: 'character/169',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/168',
      id: 'has_relation/2922344',
      to: 'character/167',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/168',
      id: 'has_relation/2921912',
      to: 'character/156',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/159',
      id: 'has_relation/2922656',
      to: 'character/101',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/159',
      id: 'has_relation/2922655',
      to: 'character/158',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/159',
      id: 'has_relation/2922341',
      to: 'character/112',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/159',
      id: 'has_relation/2921906',
      to: 'character/109',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/158',
      id: 'has_relation/2922653',
      to: 'character/113',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/158',
      id: 'has_relation/2922508',
      to: 'character/163',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/158',
      id: 'has_relation/2922340',
      to: 'character/112',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/158',
      id: 'has_relation/2921905',
      to: 'character/109',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/157',
      id: 'has_relation/2922507',
      to: 'character/166',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/157',
      id: 'has_relation/2922339',
      to: 'character/111',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/157',
      id: 'has_relation/2921904',
      to: 'character/110',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/156',
      id: 'has_relation/2922651',
      to: 'character/157',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/156',
      id: 'has_relation/2922506',
      to: 'character/167',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/156',
      id: 'has_relation/2922338',
      to: 'character/111',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/156',
      id: 'has_relation/2921903',
      to: 'character/110',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/155',
      id: 'has_relation/2922650',
      to: 'character/157',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/155',
      id: 'has_relation/2922649',
      to: 'character/156',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/155',
      id: 'has_relation/2922337',
      to: 'character/111',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/155',
      id: 'has_relation/2921902',
      to: 'character/110',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/127',
      id: 'has_relation/2922320',
      to: 'character/106',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/127',
      id: 'has_relation/2921882',
      to: 'character/105',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/116',
      id: 'has_relation/2922496',
      to: 'character/117',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/116',
      id: 'has_relation/2922310',
      to: 'character/115',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/116',
      id: 'has_relation/2921872',
      to: 'character/113',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/113',
      id: 'has_relation/2922495',
      to: 'character/115',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/113',
      id: 'has_relation/2922308',
      to: 'character/112',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/113',
      id: 'has_relation/2921870',
      to: 'character/109',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/110',
      id: 'has_relation/2922494',
      to: 'character/111',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/109',
      id: 'has_relation/2922614',
      to: 'character/157',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/109',
      id: 'has_relation/2922613',
      to: 'character/156',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/109',
      id: 'has_relation/2922612',
      to: 'character/155',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/109',
      id: 'has_relation/2922493',
      to: 'character/112',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/109',
      id: 'has_relation/2922307',
      to: 'character/111',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/109',
      id: 'has_relation/2921869',
      to: 'character/110',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/108',
      id: 'has_relation/2922611',
      to: 'character/109',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/108',
      id: 'has_relation/2922609',
      to: 'character/155',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/108',
      id: 'has_relation/2922608',
      to: 'character/156',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/108',
      id: 'has_relation/2922306',
      to: 'character/111',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/108',
      id: 'has_relation/2921868',
      to: 'character/110',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/106',
      id: 'has_relation/2921867',
      to: 'character/151',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/105',
      id: 'has_relation/2922491',
      to: 'character/106',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/159',
      id: 'has_relation/2922654',
      to: 'character/113',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/105',
      id: 'has_relation/2922305',
      to: 'character/107',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/108',
      id: 'has_relation/2922610',
      to: 'character/157',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/105',
      id: 'has_relation/2921866',
      to: 'character/108',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/158',
      id: 'has_relation/2922652',
      to: 'character/101',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/104',
      id: 'has_relation/2922607',
      to: 'character/127',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/104',
      id: 'has_relation/2922304',
      to: 'character/106',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/104',
      id: 'has_relation/2921865',
      to: 'character/105',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/101',
      id: 'has_relation/2922601',
      to: 'character/113',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/101',
      id: 'has_relation/2922489',
      to: 'character/114',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_relative',
      },
      from: 'character/101',
      id: 'has_relation/2922440',
      to: 'character/105',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/101',
      id: 'has_relation/2922301',
      to: 'character/112',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/101',
      id: 'has_relation/2921862',
      to: 'character/109',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/97',
      id: 'has_relation/2922300',
      to: 'character/117',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/97',
      id: 'has_relation/2921861',
      to: 'character/116',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/95',
      id: 'has_relation/2922594',
      to: 'character/104',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/95',
      id: 'has_relation/2922593',
      to: 'character/127',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/95',
      id: 'has_relation/2922488',
      to: 'character/96',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_relative',
      },
      from: 'character/95',
      id: 'has_relation/2922439',
      to: 'character/78',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/95',
      id: 'has_relation/2922298',
      to: 'character/106',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/95',
      id: 'has_relation/2921859',
      to: 'character/105',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/78',
      id: 'has_relation/2922290',
      to: 'character/114',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/78',
      id: 'has_relation/2921848',
      to: 'character/101',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_relative',
      },
      from: 'character/77',
      id: 'has_relation/2922438',
      to: 'character/78',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/77',
      id: 'has_relation/2922289',
      to: 'character/96',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/77',
      id: 'has_relation/2921847',
      to: 'character/95',
    },
  ],
  nodes: [
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q23817042',
        name: 'Polo Baggins',
        url: 'http://www.wikidata.org/entity/Q23817042',
      },
      id: 'character/169',
      key: '169',
      rev: '_dKafMaC--L',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q23817039',
        name: 'Rosa Baggins',
        url: 'http://www.wikidata.org/entity/Q23817039',
      },
      id: 'character/168',
      key: '168',
      rev: '_dKafMaC--K',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q23817019',
        name: 'Rudigar Bolger',
        url: 'http://www.wikidata.org/entity/Q23817019',
      },
      id: 'character/163',
      key: '163',
      rev: '_dKafMaC--F',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q23816916',
        name: 'Belba Baggins',
        url: 'http://www.wikidata.org/entity/Q23816916',
      },
      id: 'character/158',
      key: '158',
      rev: '_dKafMaC--A',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q23817034',
        name: 'Togo Goodbody',
        url: 'http://www.wikidata.org/entity/Q23817034',
      },
      id: 'character/166',
      key: '166',
      rev: '_dKafMaC--I',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q23817037',
        name: 'Mimosa Bunce',
        url: 'http://www.wikidata.org/entity/Q23817037',
      },
      id: 'character/167',
      key: '167',
      rev: '_dKafMaC--J',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285183',
        name: 'Camellia Sackville',
        url: 'http://www.wikidata.org/entity/Q15285183',
      },
      id: 'character/115',
      key: '115',
      rev: '_dKafMa---l',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q23816906',
        name: 'Lily Baggins',
        url: 'http://www.wikidata.org/entity/Q23816906',
      },
      id: 'character/157',
      key: '157',
      rev: '_dKafMaC--_',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q23816862',
        name: 'Pansy Baggins',
        url: 'http://www.wikidata.org/entity/Q23816862',
      },
      id: 'character/155',
      key: '155',
      rev: '_dKafMa--_N',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q23816891',
        name: 'Ponto Baggins I',
        url: 'http://www.wikidata.org/entity/Q23816891',
      },
      id: 'character/156',
      key: '156',
      rev: '_dKafMaC---',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285152',
        name: 'Berylla Baggins',
        url: 'http://www.wikidata.org/entity/Q15285152',
      },
      id: 'character/111',
      key: '111',
      rev: '_dKafMa---h',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q15285150',
        name: 'Balbo Baggins',
        url: 'http://www.wikidata.org/entity/Q15285150',
      },
      id: 'character/110',
      key: '110',
      rev: '_dKafMa---g',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q23811292',
        name: 'Adalgar Bolger',
        url: 'http://www.wikidata.org/entity/Q23811292',
      },
      id: 'character/151',
      key: '151',
      rev: '_dKafMa--_J',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285132',
        name: 'Tanta Hornblower',
        url: 'http://www.wikidata.org/entity/Q15285132',
      },
      id: 'character/107',
      key: '107',
      rev: '_dKafMa---d',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q15285136',
        name: 'Largo Baggins',
        url: 'http://www.wikidata.org/entity/Q15285136',
      },
      id: 'character/108',
      key: '108',
      rev: '_dKafMa---e',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q15285165',
        name: 'Longo Baggins',
        url: 'http://www.wikidata.org/entity/Q15285165',
      },
      id: 'character/113',
      key: '113',
      rev: '_dKafMa---j',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285160',
        name: 'Laura Grubb',
        url: 'http://www.wikidata.org/entity/Q15285160',
      },
      id: 'character/112',
      key: '112',
      rev: '_dKafMa---i',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q15285144',
        name: 'Mungo Baggins',
        url: 'http://www.wikidata.org/entity/Q15285144',
      },
      id: 'character/109',
      key: '109',
      rev: '_dKafMa---f',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285193',
        name: 'Lobelia Bracegirdle',
        url: 'http://www.wikidata.org/entity/Q15285193',
      },
      id: 'character/117',
      key: '117',
      rev: '_dKafMa---n',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q15285187',
        name: 'Otto Sackville-Baggins',
        url: 'http://www.wikidata.org/entity/Q15285187',
      },
      id: 'character/116',
      key: '116',
      rev: '_dKafMa---m',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q7486999',
        name: 'Lotho Sackville-Baggins',
        url: 'http://www.wikidata.org/entity/Q7486999',
      },
      id: 'character/97',
      key: '97',
      rev: '_dKafMa---T',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285113',
        name: 'Dora Baggins',
        url: 'http://www.wikidata.org/entity/Q15285113',
      },
      id: 'character/104',
      key: '104',
      rev: '_dKafMa---a',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q15285306',
        name: 'Dudo Baggins',
        url: 'http://www.wikidata.org/entity/Q15285306',
      },
      id: 'character/127',
      key: '127',
      rev: '_dKafMa---x',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q15285118',
        name: 'Fosco Baggins',
        url: 'http://www.wikidata.org/entity/Q15285118',
      },
      id: 'character/105',
      key: '105',
      rev: '_dKafMa---b',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285171',
        name: 'Belladonna Took',
        url: 'http://www.wikidata.org/entity/Q15285171',
      },
      id: 'character/114',
      key: '114',
      rev: '_dKafMa---k',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q12255529',
        name: 'Bungo Baggins',
        url: 'http://www.wikidata.org/entity/Q12255529',
      },
      id: 'character/101',
      key: '101',
      rev: '_dKafMa---X',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q185737',
        name: 'Bilbo Baggins',
        url: 'http://www.wikidata.org/entity/Q185737',
      },
      id: 'character/78',
      key: '78',
      rev: '_dKafMa---A',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q23816918',
        name: 'Bingo Baggins',
        url: 'http://www.wikidata.org/entity/Q23816918',
      },
      id: 'character/159',
      key: '159',
      rev: '_dKafMaC--B',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q15285123',
        name: 'Ruby Baggins',
        url: 'http://www.wikidata.org/entity/Q15285123',
      },
      id: 'character/106',
      key: '106',
      rev: '_dKafMa---c',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q7486642',
        name: 'Primula Brandybuck',
        url: 'http://www.wikidata.org/entity/Q7486642',
      },
      id: 'character/96',
      key: '96',
      rev: '_dKafMa---S',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q7486626',
        name: 'Drogo Baggins',
        url: 'http://www.wikidata.org/entity/Q7486626',
      },
      id: 'character/95',
      key: '95',
      rev: '_dKafMa---R',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q177329',
        name: 'Frodo Baggins',
        url: 'http://www.wikidata.org/entity/Q177329',
      },
      id: 'character/77',
      key: '77',
      rev: '_dKafMa---_',
    },
  ],
  mlEdges: [
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/0',
      to: 'character/101',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/1',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/2',
      to: 'character/114',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/3',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/4',
      to: 'character/104',
    },
    {
      attributes: {
        jaccard_coefficient: 0.3333333333333333,
      },
      from: 'character/117',
      id: 'link_prediction_relation/5',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/6',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/7',
      to: 'character/111',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/8',
      to: 'character/155',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/9',
      to: 'character/157',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/10',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/11',
      to: 'character/108',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/12',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/117',
      id: 'link_prediction_relation/13',
      to: 'character/113',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/14',
      to: 'character/112',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/15',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/16',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/17',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/18',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/19',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/20',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/21',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/22',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/23',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/24',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/25',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/26',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/27',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/117',
      id: 'link_prediction_relation/28',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/101',
      id: 'link_prediction_relation/29',
      to: 'character/104',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/101',
      id: 'link_prediction_relation/30',
      to: 'character/111',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07142857142857142,
      },
      from: 'character/101',
      id: 'link_prediction_relation/31',
      to: 'character/157',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/101',
      id: 'link_prediction_relation/32',
      to: 'character/155',
    },
    {
      attributes: {
        jaccard_coefficient: 0.15384615384615385,
      },
      from: 'character/101',
      id: 'link_prediction_relation/33',
      to: 'character/108',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/101',
      id: 'link_prediction_relation/34',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/101',
      id: 'link_prediction_relation/35',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0625,
      },
      from: 'character/101',
      id: 'link_prediction_relation/36',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/101',
      id: 'link_prediction_relation/37',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/101',
      id: 'link_prediction_relation/38',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/101',
      id: 'link_prediction_relation/39',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/101',
      id: 'link_prediction_relation/40',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/101',
      id: 'link_prediction_relation/41',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/101',
      id: 'link_prediction_relation/42',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/101',
      id: 'link_prediction_relation/43',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/101',
      id: 'link_prediction_relation/44',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/101',
      id: 'link_prediction_relation/45',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/101',
      id: 'link_prediction_relation/46',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/101',
      id: 'link_prediction_relation/47',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/101',
      id: 'link_prediction_relation/48',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/101',
      id: 'link_prediction_relation/49',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.15384615384615385,
      },
      from: 'character/101',
      id: 'link_prediction_relation/50',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/51',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/52',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/53',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/54',
      to: 'character/104',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/55',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/114',
      id: 'link_prediction_relation/56',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/57',
      to: 'character/111',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/58',
      to: 'character/155',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/59',
      to: 'character/157',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/60',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/61',
      to: 'character/108',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/114',
      id: 'link_prediction_relation/62',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/114',
      id: 'link_prediction_relation/63',
      to: 'character/113',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/114',
      id: 'link_prediction_relation/64',
      to: 'character/112',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/65',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/66',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/67',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.25,
      },
      from: 'character/114',
      id: 'link_prediction_relation/68',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/69',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/70',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/71',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/72',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/114',
      id: 'link_prediction_relation/73',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/114',
      id: 'link_prediction_relation/74',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/75',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/76',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/114',
      id: 'link_prediction_relation/77',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/114',
      id: 'link_prediction_relation/78',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/79',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.2,
      },
      from: 'character/104',
      id: 'link_prediction_relation/80',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/81',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/82',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/83',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/84',
      to: 'character/111',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/85',
      to: 'character/155',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/86',
      to: 'character/157',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/87',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/104',
      id: 'link_prediction_relation/88',
      to: 'character/108',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/89',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/90',
      to: 'character/113',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/91',
      to: 'character/112',
    },
    {
      attributes: {
        jaccard_coefficient: 0.25,
      },
      from: 'character/104',
      id: 'link_prediction_relation/92',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/93',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/94',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/104',
      id: 'link_prediction_relation/95',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/96',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/97',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/98',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/99',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/104',
      id: 'link_prediction_relation/100',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.25,
      },
      from: 'character/104',
      id: 'link_prediction_relation/101',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/104',
      id: 'link_prediction_relation/102',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/111',
      id: 'link_prediction_relation/103',
      to: 'character/113',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/111',
      id: 'link_prediction_relation/104',
      to: 'character/112',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/105',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/111',
      id: 'link_prediction_relation/106',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/111',
      id: 'link_prediction_relation/107',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/108',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/109',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/111',
      id: 'link_prediction_relation/110',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/111',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/112',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/113',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/114',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/111',
      id: 'link_prediction_relation/115',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/116',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/111',
      id: 'link_prediction_relation/117',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/118',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/119',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/111',
      id: 'link_prediction_relation/120',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/111',
      id: 'link_prediction_relation/121',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/122',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/111',
      id: 'link_prediction_relation/123',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/155',
      id: 'link_prediction_relation/124',
      to: 'character/113',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/155',
      id: 'link_prediction_relation/125',
      to: 'character/112',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/126',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/155',
      id: 'link_prediction_relation/127',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/155',
      id: 'link_prediction_relation/128',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/129',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/130',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/155',
      id: 'link_prediction_relation/131',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/132',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/133',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/134',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/135',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/155',
      id: 'link_prediction_relation/136',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/137',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/155',
      id: 'link_prediction_relation/138',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/139',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/140',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/155',
      id: 'link_prediction_relation/141',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/155',
      id: 'link_prediction_relation/142',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/143',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/155',
      id: 'link_prediction_relation/144',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/157',
      id: 'link_prediction_relation/145',
      to: 'character/113',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/157',
      id: 'link_prediction_relation/146',
      to: 'character/112',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/147',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/157',
      id: 'link_prediction_relation/148',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/149',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/150',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/157',
      id: 'link_prediction_relation/151',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/152',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/153',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/154',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/155',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/157',
      id: 'link_prediction_relation/156',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/157',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/157',
      id: 'link_prediction_relation/158',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/159',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/160',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/157',
      id: 'link_prediction_relation/161',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/157',
      id: 'link_prediction_relation/162',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/163',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/157',
      id: 'link_prediction_relation/164',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/108',
      id: 'link_prediction_relation/165',
      to: 'character/113',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/108',
      id: 'link_prediction_relation/166',
      to: 'character/112',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/167',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/108',
      id: 'link_prediction_relation/168',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/108',
      id: 'link_prediction_relation/169',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/170',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/171',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/108',
      id: 'link_prediction_relation/172',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/173',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/174',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/175',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/176',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/108',
      id: 'link_prediction_relation/177',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/108',
      id: 'link_prediction_relation/178',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/108',
      id: 'link_prediction_relation/179',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/108',
      id: 'link_prediction_relation/180',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/108',
      id: 'link_prediction_relation/181',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/108',
      id: 'link_prediction_relation/182',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/108',
      id: 'link_prediction_relation/183',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/108',
      id: 'link_prediction_relation/184',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/185',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/186',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/113',
      id: 'link_prediction_relation/187',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/188',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/113',
      id: 'link_prediction_relation/189',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/190',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/191',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/192',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/113',
      id: 'link_prediction_relation/193',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.06666666666666667,
      },
      from: 'character/113',
      id: 'link_prediction_relation/194',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/195',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/113',
      id: 'link_prediction_relation/196',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/113',
      id: 'link_prediction_relation/197',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/198',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/199',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/200',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/113',
      id: 'link_prediction_relation/201',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/202',
      to: 'character/77',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/203',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/112',
      id: 'link_prediction_relation/204',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/112',
      id: 'link_prediction_relation/205',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/206',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/207',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/112',
      id: 'link_prediction_relation/208',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/209',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/112',
      id: 'link_prediction_relation/210',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/112',
      id: 'link_prediction_relation/211',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.2,
      },
      from: 'character/112',
      id: 'link_prediction_relation/212',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/213',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/214',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/215',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/216',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/217',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/112',
      id: 'link_prediction_relation/218',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/219',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/112',
      id: 'link_prediction_relation/220',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/221',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/222',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/223',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/224',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/225',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/226',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/227',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/228',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/229',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/77',
      id: 'link_prediction_relation/230',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/231',
      to: 'character/166',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/232',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/233',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/77',
      id: 'link_prediction_relation/234',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/235',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/77',
      id: 'link_prediction_relation/236',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/237',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/77',
      id: 'link_prediction_relation/238',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/166',
      id: 'link_prediction_relation/239',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/240',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/241',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/242',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/166',
      id: 'link_prediction_relation/243',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/244',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/245',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/246',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/247',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/248',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/249',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/166',
      id: 'link_prediction_relation/250',
      to: 'character/156',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/251',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/252',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/253',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/254',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/255',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/256',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/257',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/166',
      id: 'link_prediction_relation/258',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/259',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/260',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/261',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/262',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/263',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/264',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/265',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.06666666666666667,
      },
      from: 'character/156',
      id: 'link_prediction_relation/266',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/156',
      id: 'link_prediction_relation/267',
      to: 'character/159',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/268',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/269',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/270',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/156',
      id: 'link_prediction_relation/271',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07142857142857142,
      },
      from: 'character/156',
      id: 'link_prediction_relation/272',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/273',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/274',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/275',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1,
      },
      from: 'character/159',
      id: 'link_prediction_relation/276',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/277',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/278',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/159',
      id: 'link_prediction_relation/279',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.2,
      },
      from: 'character/159',
      id: 'link_prediction_relation/280',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/159',
      id: 'link_prediction_relation/281',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/282',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/159',
      id: 'link_prediction_relation/283',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/159',
      id: 'link_prediction_relation/284',
      to: 'character/78',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/285',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/286',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/287',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/159',
      id: 'link_prediction_relation/288',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/289',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/290',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/291',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/292',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/78',
      id: 'link_prediction_relation/293',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.5,
      },
      from: 'character/78',
      id: 'link_prediction_relation/294',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/295',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/296',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/297',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/78',
      id: 'link_prediction_relation/298',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/299',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.2222222222222222,
      },
      from: 'character/78',
      id: 'link_prediction_relation/300',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07142857142857142,
      },
      from: 'character/78',
      id: 'link_prediction_relation/301',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/78',
      id: 'link_prediction_relation/302',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/303',
      to: 'character/151',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/78',
      id: 'link_prediction_relation/304',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/305',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/306',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/307',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/308',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/309',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/310',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/311',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/312',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/313',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.25,
      },
      from: 'character/151',
      id: 'link_prediction_relation/314',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/315',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/151',
      id: 'link_prediction_relation/316',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/317',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/151',
      id: 'link_prediction_relation/318',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/151',
      id: 'link_prediction_relation/319',
      to: 'character/167',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/320',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/321',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/167',
      id: 'link_prediction_relation/322',
      to: 'character/110',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/323',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/324',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/325',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/326',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/327',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/167',
      id: 'link_prediction_relation/328',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/329',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/330',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/331',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/167',
      id: 'link_prediction_relation/332',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/333',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/110',
      id: 'link_prediction_relation/334',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/335',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/336',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/337',
      to: 'character/96',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/338',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/339',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/110',
      id: 'link_prediction_relation/340',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/110',
      id: 'link_prediction_relation/341',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/342',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/343',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/110',
      id: 'link_prediction_relation/344',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/110',
      id: 'link_prediction_relation/345',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/346',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/347',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/348',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.2,
      },
      from: 'character/96',
      id: 'link_prediction_relation/349',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/350',
      to: 'character/116',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/351',
      to: 'character/115',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/352',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/96',
      id: 'link_prediction_relation/353',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/354',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.16666666666666666,
      },
      from: 'character/96',
      id: 'link_prediction_relation/355',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/356',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/96',
      id: 'link_prediction_relation/357',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/358',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/359',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/360',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/361',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/362',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07142857142857142,
      },
      from: 'character/116',
      id: 'link_prediction_relation/363',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/364',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/365',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/116',
      id: 'link_prediction_relation/366',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.1111111111111111,
      },
      from: 'character/116',
      id: 'link_prediction_relation/367',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/368',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/369',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.3333333333333333,
      },
      from: 'character/115',
      id: 'link_prediction_relation/370',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/371',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/372',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/373',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/115',
      id: 'link_prediction_relation/374',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/375',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/376',
      to: 'character/163',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/115',
      id: 'link_prediction_relation/377',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/115',
      id: 'link_prediction_relation/378',
      to: 'character/158',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/379',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/380',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/381',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/382',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/383',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/384',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/385',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.09090909090909091,
      },
      from: 'character/163',
      id: 'link_prediction_relation/386',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/163',
      id: 'link_prediction_relation/387',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/158',
      id: 'link_prediction_relation/388',
      to: 'character/107',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/158',
      id: 'link_prediction_relation/389',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/158',
      id: 'link_prediction_relation/390',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/158',
      id: 'link_prediction_relation/391',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/158',
      id: 'link_prediction_relation/392',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.08333333333333333,
      },
      from: 'character/158',
      id: 'link_prediction_relation/393',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/158',
      id: 'link_prediction_relation/394',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/158',
      id: 'link_prediction_relation/395',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/107',
      id: 'link_prediction_relation/396',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/107',
      id: 'link_prediction_relation/397',
      to: 'character/169',
    },
    {
      attributes: {
        jaccard_coefficient: 0.25,
      },
      from: 'character/107',
      id: 'link_prediction_relation/398',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/107',
      id: 'link_prediction_relation/399',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.2,
      },
      from: 'character/107',
      id: 'link_prediction_relation/400',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/107',
      id: 'link_prediction_relation/401',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.14285714285714285,
      },
      from: 'character/107',
      id: 'link_prediction_relation/402',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/169',
      id: 'link_prediction_relation/403',
      to: 'character/97',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/169',
      id: 'link_prediction_relation/404',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/169',
      id: 'link_prediction_relation/405',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/169',
      id: 'link_prediction_relation/406',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/169',
      id: 'link_prediction_relation/407',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/169',
      id: 'link_prediction_relation/408',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/97',
      id: 'link_prediction_relation/409',
      to: 'character/127',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/97',
      id: 'link_prediction_relation/410',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/97',
      id: 'link_prediction_relation/411',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/97',
      id: 'link_prediction_relation/412',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/97',
      id: 'link_prediction_relation/413',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/97',
      id: 'link_prediction_relation/414',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/127',
      id: 'link_prediction_relation/415',
      to: 'character/168',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/127',
      id: 'link_prediction_relation/416',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/168',
      id: 'link_prediction_relation/417',
      to: 'character/106',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/168',
      id: 'link_prediction_relation/418',
      to: 'character/105',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/168',
      id: 'link_prediction_relation/419',
      to: 'character/95',
    },
    {
      attributes: {
        jaccard_coefficient: 0.07692307692307693,
      },
      from: 'character/168',
      id: 'link_prediction_relation/420',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.125,
      },
      from: 'character/105',
      id: 'link_prediction_relation/421',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/106',
      id: 'link_prediction_relation/422',
      to: 'character/109',
    },
    {
      attributes: {
        jaccard_coefficient: 0.0,
      },
      from: 'character/109',
      id: 'link_prediction_relation/423',
      to: 'character/95',
    },
  ],
};
