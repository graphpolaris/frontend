/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

// Mock data with link prediction machine learning data
export default {
  edges: [
    {
      attributes: {
        _exists: 'true',
        _tag: 'spouse',
      },
      from: 'character/79',
      id: 'has_relation/2922483',
      to: 'character/94',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/79',
      id: 'has_relation/2922291',
      to: 'character/70',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/79',
      id: 'has_relation/2921849',
      to: 'character/85',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/71',
      id: 'has_relation/2922585',
      to: 'character/79',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/71',
      id: 'has_relation/2922584',
      to: 'character/69',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/71',
      id: 'has_relation/2922287',
      to: 'character/70',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/71',
      id: 'has_relation/2921845',
      to: 'character/85',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'sibling',
      },
      from: 'character/69',
      id: 'has_relation/2922583',
      to: 'character/79',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_mother',
      },
      from: 'character/69',
      id: 'has_relation/2922286',
      to: 'character/70',
    },
    {
      attributes: {
        _exists: 'true',
        _tag: 'has_father',
      },
      from: 'character/69',
      id: 'has_relation/2921844',
      to: 'character/85',
    },
  ],
  nodes: [
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q7476340',
        name: 'Rose Cotton',
        url: 'http://www.wikidata.org/entity/Q7476340',
      },
      id: 'character/94',
      key: '94',
      rev: '_dKafMa---Q',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q53766582',
        name: 'Hamson Gamgee',
        url: 'http://www.wikidata.org/entity/Q53766582',
      },
      id: 'character/71',
      key: '71',
      rev: '_dKafMZ6--q',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q219473',
        name: 'Samwise Gamgee',
        position: 'Mayor of the Shire',
        url: 'http://www.wikidata.org/entity/Q219473',
      },
      id: 'character/79',
      key: '79',
      rev: '_dKafMa---B',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q53766306',
        name: 'Bell Goodchild',
        url: 'http://www.wikidata.org/entity/Q53766306',
      },
      id: 'character/70',
      key: '70',
      rev: '_dKafMZ6--p',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'male',
        id: 'Q2630368',
        name: 'Hamfast Gamgee',
        url: 'http://www.wikidata.org/entity/Q2630368',
      },
      id: 'character/85',
      key: '85',
      rev: '_dKafMa---H',
    },
    {
      attributes: {
        familyComponent: 0,
        gender: 'female',
        id: 'Q53765874',
        name: 'Marigold Gamgee',
        url: 'http://www.wikidata.org/entity/Q53765874',
      },
      id: 'character/69',
      key: '69',
      rev: '_dKafMZ6--o',
    },
  ],
  mlEdges: [
    {
      attributes: {
        jaccard_coefficient: 0.25,
      },
      from: 'character/71',
      id: 'link_prediction_relation/0',
      to: 'character/94',
    },
    {
      attributes: {
        jaccard_coefficient: 0.3333333333333333,
      },
      from: 'character/70',
      id: 'link_prediction_relation/1',
      to: 'character/94',
    },
    {
      attributes: {
        jaccard_coefficient: 1.0,
      },
      from: 'character/70',
      id: 'link_prediction_relation/2',
      to: 'character/85',
    },
    {
      attributes: {
        jaccard_coefficient: 0.3333333333333333,
      },
      from: 'character/85',
      id: 'link_prediction_relation/3',
      to: 'character/94',
    },
    {
      attributes: {
        jaccard_coefficient: 0.25,
      },
      from: 'character/69',
      id: 'link_prediction_relation/4',
      to: 'character/94',
    },
  ],
};
