/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { NodeLinkResultType } from '../../../domain/entity/query-result/structures/NodeLinkResultType';

const animalQueryResult: NodeLinkResultType = {
  nodes: [
    { id: 'plant/grass', attributes: { color: 'green' } },
    { id: 'plant/carrot', attributes: { color: 'orange' } },
    { id: 'animal/rabbit', attributes: { diet: 'herbivore', type: 'rodent' } },
    { id: 'animal/wolf', attributes: { diet: 'carnivore', type: 'canine' } },
    { id: 'animal/hamster', attributes: { diet: 'omnivore', type: 'rodent' } },
    { id: 'animal/cricket', attributes: { diet: 'omnivore', type: 'insect' } },
  ],
  edges: [
    {
      from: 'animal/rabbit',
      to: 'plant/grass',
      id: 'eats/1',
      attributes: { isMainDiet: true },
    },
    {
      from: 'animal/rabbit',
      to: 'plant/carrot',
      id: 'eats/2',
      attributes: { isMainDiet: false },
    },
    {
      from: 'animal/hamster',
      to: 'plant/carrot',
      id: 'eats/3',
      attributes: { isMainDiet: false },
    },
    {
      from: 'animal/hamster',
      to: 'plant/grass',
      id: 'eats/4',
      attributes: { isMainDiet: false },
    },
    {
      from: 'animal/hamster',
      to: 'animal/cricket',
      id: 'eats/5',
      attributes: { isMainDiet: false },
    },
    {
      from: 'animal/cricket',
      to: 'plant/grass',
      id: 'eats/6',
      attributes: { isMainDiet: true },
    },
    {
      from: 'animal/cricket',
      to: 'animal/cricket',
      id: 'eats/7',
      attributes: { isMainDiet: false },
    },
    {
      from: 'animal/wolf',
      to: 'animal/rabbit',
      id: 'eats/8',
      attributes: { isMainDiet: true },
    },
    {
      from: 'animal/wolf',
      to: 'animal/hamster',
      id: 'eats/9',
      attributes: { isMainDiet: false },
    },
  ],
};

export const animalMocks = {
  queryResult: animalQueryResult,
  oneNodeFilter: [
    { targetGroup: 'animal', attributeName: 'diet', value: 'omnivore', predicateName: '!=' },
  ],
  oneNodeFilterOutput: {
    nodes: [
      { id: 'plant/grass', attributes: { color: 'green' } },
      { id: 'plant/carrot', attributes: { color: 'orange' } },
      { id: 'animal/rabbit', attributes: { diet: 'herbivore', type: 'rodent' } },
      { id: 'animal/wolf', attributes: { diet: 'carnivore', type: 'canine' } },
    ],
    edges: [
      {
        from: 'animal/rabbit',
        to: 'plant/grass',
        id: 'eats/1',
        attributes: { isMainDiet: true },
      },
      {
        from: 'animal/rabbit',
        to: 'plant/carrot',
        id: 'eats/2',
        attributes: { isMainDiet: false },
      },
      {
        from: 'animal/wolf',
        to: 'animal/rabbit',
        id: 'eats/8',
        attributes: { isMainDiet: true },
      },
    ],
  },
  twoNodeTypesFilter: [
    { targetGroup: 'animal', attributeName: 'diet', value: 'omnivore', predicateName: '==' },
    { targetGroup: 'plant', attributeName: 'color', value: 'orang', predicateName: 'excludes' },
  ],
  twoNodeTypesFilterOutput: {
    nodes: [
      { id: 'plant/grass', attributes: { color: 'green' } },
      { id: 'animal/hamster', attributes: { diet: 'omnivore', type: 'rodent' } },
      { id: 'animal/cricket', attributes: { diet: 'omnivore', type: 'insect' } },
    ],
    edges: [
      {
        from: 'animal/hamster',
        to: 'plant/grass',
        id: 'eats/4',
        attributes: { isMainDiet: false },
      },

      {
        from: 'animal/hamster',
        to: 'animal/cricket',
        id: 'eats/5',
        attributes: { isMainDiet: false },
      },
      {
        from: 'animal/cricket',
        to: 'plant/grass',
        id: 'eats/6',
        attributes: { isMainDiet: true },
      },
      {
        from: 'animal/cricket',
        to: 'animal/cricket',
        id: 'eats/7',
        attributes: { isMainDiet: false },
      },
    ],
  },
  oneEdgeFilter: [
    { targetGroup: 'eats', attributeName: 'isMainDiet', value: true, predicateName: '==' },
  ],
  oneEdgeFilterOutput: {
    nodes: [
      { id: 'plant/grass', attributes: { color: 'green' } },
      { id: 'animal/rabbit', attributes: { diet: 'herbivore', type: 'rodent' } },
      { id: 'animal/wolf', attributes: { diet: 'carnivore', type: 'canine' } },
      { id: 'animal/cricket', attributes: { diet: 'omnivore', type: 'insect' } },
    ],
    edges: [
      {
        from: 'animal/rabbit',
        to: 'plant/grass',
        id: 'eats/1',
        attributes: { isMainDiet: true },
      },
      {
        from: 'animal/cricket',
        to: 'plant/grass',
        id: 'eats/6',
        attributes: { isMainDiet: true },
      },
      {
        from: 'animal/wolf',
        to: 'animal/rabbit',
        id: 'eats/8',
        attributes: { isMainDiet: true },
      },
    ],
  },
  oneNodeAndEdgeFilterOutput: {
    nodes: [
      { id: 'plant/grass', attributes: { color: 'green' } },
      { id: 'animal/rabbit', attributes: { diet: 'herbivore', type: 'rodent' } },
      { id: 'animal/wolf', attributes: { diet: 'carnivore', type: 'canine' } },
    ],
    edges: [
      {
        from: 'animal/rabbit',
        to: 'plant/grass',
        id: 'eats/1',
        attributes: { isMainDiet: true },
      },
      {
        from: 'animal/wolf',
        to: 'animal/rabbit',
        id: 'eats/8',
        attributes: { isMainDiet: true },
      },
    ],
  },
};

export const sameAttributes = {
  queryResult: {
    nodes: [
      { id: 'person/Bob', attributes: { name: 'Bob Bobson' } },
      { id: 'pet/dog1', attributes: { name: 'Hotdog' } },
      { id: 'person/Bobby', attributes: { name: 'Bobby Bobson' } },
      { id: 'pet/cat1', attributes: { name: 'Catdog' } },
      { id: 'person/filter', attributes: { name: 'Should be filtered out' } },
      { id: 'pet/filter', attributes: { name: 'Should be filtered out' } },
    ],
    edges: [
      {
        from: 'person/Bob',
        to: 'pet/dog1',
        id: 'named/1',
        attributes: { name: 'Hotdog' },
      },
      {
        from: 'person/Bobby',
        to: 'pet/cat1',
        id: 'named/2',
        attributes: { name: 'Catdog' },
      },
      {
        from: 'person/filter',
        to: 'pet/filter',
        id: 'named/3',
        attributes: { name: 'Should be filtered out' },
      },
    ],
  },
  nodeFilter1: {
    targetGroup: 'person',
    attributeName: 'name',
    value: 'Bob',
    predicateName: 'contains',
  },
  nodeFilter2: {
    targetGroup: 'pet',
    attributeName: 'name',
    value: 'dog',
    predicateName: 'contains',
  },
  edgeFilters: [
    { targetGroup: 'named', attributeName: 'name', value: 'dog', predicateName: 'contains' },
  ],
  filteredOutput: {
    nodes: [
      { id: 'person/Bob', attributes: { name: 'Bob Bobson' } },
      { id: 'pet/dog1', attributes: { name: 'Hotdog' } },
      { id: 'person/Bobby', attributes: { name: 'Bobby Bobson' } },
      { id: 'pet/cat1', attributes: { name: 'Catdog' } },
    ],
    edges: [
      {
        from: 'person/Bob',
        to: 'pet/dog1',
        id: 'named/1',
        attributes: { name: 'Hotdog' },
      },
      {
        from: 'person/Bobby',
        to: 'pet/cat1',
        id: 'named/2',
        attributes: { name: 'Catdog' },
      },
    ],
  },
};

export const allAttributeTypes = {
  input: {
    nodes: [
      { id: 'test/a', attributes: { string: 'ab', number: -1, bool: false } },
      { id: 'test/b', attributes: { string: 'bc', number: 0, bool: false } },
      { id: 'test/c', attributes: { string: 'c', number: 1, bool: true } },
      { id: 'node/connected to the others', attributes: { string: 'c', number: 1, bool: true } },
    ],
    edges: [
      {
        from: 'test/a',
        to: 'test/b',
        id: 'edge/1',
        attributes: { string: 'ab', number: -1, bool: true },
      },
      {
        from: 'test/b',
        to: 'test/c',
        id: 'edge/2',
        attributes: { string: 'bc', number: 1, bool: false },
      },
      {
        from: 'test/a',
        to: 'node/connected to the others',
        id: 'edge/3',
        attributes: { string: 'ab', number: -1, bool: true },
      },
      {
        from: 'test/b',
        to: 'node/connected to the others',
        id: 'edge/4',
        attributes: { string: 'bc', number: 1, bool: false },
      },
      {
        from: 'test/c',
        to: 'node/connected to the others',
        id: 'edge/5',
        attributes: { string: 'bc', number: 1, bool: false },
      },
    ],
  },
  nodeFilters1: [
    {
      targetGroup: 'test',
      attributeName: 'number',
      value: -1,
      predicateName: '==',
    },
    {
      targetGroup: 'test',
      attributeName: 'number',
      value: 0,
      predicateName: '<',
    },
  ],
  output1: {
    nodes: [
      { id: 'test/a', attributes: { string: 'ab', number: -1, bool: false } },
      { id: 'node/connected to the others', attributes: { string: 'c', number: 1, bool: true } },
    ],
    edges: [
      {
        from: 'test/a',
        to: 'node/connected to the others',
        id: 'edge/3',
        attributes: { string: 'ab', number: -1, bool: true },
      },
    ],
  },
  nodeFilters2: [
    {
      targetGroup: 'test',
      attributeName: 'number',
      value: 0,
      predicateName: '<=',
    },
    {
      targetGroup: 'test',
      attributeName: 'bool',
      value: false,
      predicateName: '==',
    },
  ],
  output2: {
    nodes: [
      { id: 'test/a', attributes: { string: 'ab', number: -1, bool: false } },
      { id: 'test/b', attributes: { string: 'bc', number: 0, bool: false } },
      { id: 'node/connected to the others', attributes: { string: 'c', number: 1, bool: true } },
    ],
    edges: [
      {
        from: 'test/a',
        to: 'test/b',
        id: 'edge/1',
        attributes: { string: 'ab', number: -1, bool: true },
      },
      {
        from: 'test/a',
        to: 'node/connected to the others',
        id: 'edge/3',
        attributes: { string: 'ab', number: -1, bool: true },
      },
      {
        from: 'test/b',
        to: 'node/connected to the others',
        id: 'edge/4',
        attributes: { string: 'bc', number: 1, bool: false },
      },
    ],
  },
  nodeFilters3: [
    {
      targetGroup: 'test',
      attributeName: 'number',
      value: 0,
      predicateName: '>',
    },
    {
      targetGroup: 'test',
      attributeName: 'bool',
      value: false,
      predicateName: '!=',
    },
  ],
  output3: {
    nodes: [
      { id: 'test/c', attributes: { string: 'c', number: 1, bool: true } },
      { id: 'node/connected to the others', attributes: { string: 'c', number: 1, bool: true } },
    ],
    edges: [
      {
        from: 'test/c',
        to: 'node/connected to the others',
        id: 'edge/5',
        attributes: { string: 'bc', number: 1, bool: false },
      },
    ],
  },
  nodeFilters4: [
    {
      targetGroup: 'test',
      attributeName: 'number',
      value: 0,
      predicateName: '>=',
    },
    {
      targetGroup: 'test',
      attributeName: 'number',
      value: -1,
      predicateName: '!=',
    },
  ],
  output4: {
    nodes: [
      { id: 'test/b', attributes: { string: 'bc', number: 0, bool: false } },
      { id: 'test/c', attributes: { string: 'c', number: 1, bool: true } },
      { id: 'node/connected to the others', attributes: { string: 'c', number: 1, bool: true } },
    ],
    edges: [
      {
        from: 'test/b',
        to: 'test/c',
        id: 'edge/2',
        attributes: { string: 'bc', number: 1, bool: false },
      },
      {
        from: 'test/b',
        to: 'node/connected to the others',
        id: 'edge/4',
        attributes: { string: 'bc', number: 1, bool: false },
      },
      {
        from: 'test/c',
        to: 'node/connected to the others',
        id: 'edge/5',
        attributes: { string: 'bc', number: 1, bool: false },
      },
    ],
  },
  unexpectedPredicates: [
    {
      targetGroup: 'test',
      attributeName: 'string',
      value: 'aaaa',
      predicateName: '>=',
    },
    {
      targetGroup: 'test',
      attributeName: 'number',
      value: 0,
      predicateName: 'contains',
    },
    {
      targetGroup: 'test',
      attributeName: 'bool',
      value: true,
      predicateName: '<',
    },
    {
      targetGroup: 'test',
      attributeName: 'bool',
      value: false,
      predicateName: 'excludes',
    },
  ],
  unsupportedTypeFilter: [
    {
      targetGroup: 'test',
      attributeName: 'string',
      value: { thisShouldThrowAnError: true },
      predicateName: '==',
    },
  ],
  targetGroupDoesNotExistFilter: [
    {
      targetGroup: 'hello',
      attributeName: 'number',
      value: 0,
      predicateName: '>=',
    },
  ],
  attributeDoesNotExistOnNodeFilter: [
    {
      targetGroup: 'test',
      attributeName: 'hello',
      value: 0,
      predicateName: '>=',
    },
  ],
  attributeDoesNotExistOnEdgeFilter: [
    {
      targetGroup: 'edge',
      attributeName: 'hello',
      value: 0,
      predicateName: '>=',
    },
  ],
};

export const caseSensitive = {
  input: {
    nodes: [{ id: 'test/a', attributes: { string: 'AB' } }],
    edges: [
      {
        from: 'test/a',
        to: 'test/a',
        id: 'edge/1',
        attributes: { string: 'AB' },
      },
    ],
  },
  succeedingNodeFilters: [
    {
      targetGroup: 'test',
      attributeName: 'string',
      value: 'AB',
      predicateName: '==',
    },
  ],
  failingNodeFilters: [
    {
      targetGroup: 'test',
      attributeName: 'string',
      value: 'ab',
      predicateName: '==',
    },
  ],
};
