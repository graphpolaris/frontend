/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

export const animalData = {
  edges: [
    {
      name: 'animal:plant',
      to: 'plant',
      from: 'animal',
      collection: 'eatPlant',
      attributes: [{ name: 'isMainDiet', type: 'bool' }],
    },
    {
      name: 'animal:animal',
      to: 'animal',
      from: 'animal',
      collection: 'eatAnimal',
      attributes: [{ name: 'isMainDiet', type: 'bool' }],
    },
  ],
  nodes: [
    { name: 'plant', attributes: [{ name: 'color', type: 'string' }] },
    {
      name: 'animal',
      attributes: [
        { name: 'diet', type: 'string' },
        { name: 'type', type: 'string' },
      ],
    },
  ],
};

export const parliamentData = {
  edges: [
    {
      name: 'parliament:resolutions',
      to: 'resolutions',
      from: 'parliament',
      collection: 'submits',
      attributes: [],
    },
    {
      name: 'parliament:resolutions',
      to: 'resolutions',
      from: 'parliament',
      collection: 'submits',
      attributes: [],
    },
    {
      name: 'parliament:commissions',
      to: 'commissions',
      from: 'parliament',
      collection: 'part_of',
      attributes: [],
    },
    {
      name: 'parliament:parties',
      to: 'parties',
      from: 'parliament',
      collection: 'member_of',
      attributes: [
        {
          name: 'isChairman',
          type: 'bool',
        },
      ],
    },
  ],
  nodes: [
    {
      name: 'commissions',
      attributes: [{ name: 'name', type: 'string' }],
    },
    {
      name: 'resolutions',
      attributes: [
        { name: 'submitter', type: 'string' },
        { name: 'info', type: 'string' },
        { name: 'date', type: 'string' },
      ],
    },
    {
      name: 'parliament',
      attributes: [
        { name: 'age', type: 'int' },
        { name: 'img', type: 'string' },
        { name: 'party', type: 'string' },
        { name: 'residence', type: 'string' },
        { name: 'seniority', type: 'int' },
        { name: 'name', type: 'string' },
      ],
    },
    {
      name: 'parties',
      attributes: [
        { name: 'chairmanId', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'seats', type: 'int' },
        { name: 'chairman', type: 'string' },
      ],
    },
  ],
};

export const parliamentData2 = {
  edges: [
    {
      name: 'parliament:resolutions',
      to: 'resolutions',
      from: 'parliament',
      collection: 'submits',
      attributes: [
        {
          name: 'date',
          type: 'string',
        },
        {
          name: 'floatNumber',
          type: 'float',
        },
      ],
    },
    {
      name: 'parliament:resolutions',
      to: 'resolutions',
      from: 'parliament',
      collection: 'submits',
      attributes: [
        {
          name: 'date',
          type: 'string',
        },
        {
          name: 'floatNumber',
          type: 'float',
        },
      ],
    },
    {
      name: 'parliament:commissions',
      to: 'commissions',
      from: 'parliament',
      collection: 'part_of',
      attributes: [],
    },
    {
      name: 'parliament:parties',
      to: 'parties',
      from: 'parliament',
      collection: 'member_of',
      attributes: [
        {
          name: 'isChairman',
          type: 'bool',
        },
        {
          name: 'amountOfTimes',
          type: 'int',
        },
      ],
    },
  ],
  nodes: [
    {
      name: 'commissions',
      attributes: [{ name: 'name', type: 'string' }],
    },
    {
      name: 'resolutions',
      attributes: [
        { name: 'submitter', type: 'string' },
        { name: 'info', type: 'string' },
        { name: 'date', type: 'string' },
      ],
    },
    {
      name: 'parliament',
      attributes: [
        { name: 'age', type: 'int' },
        { name: 'img', type: 'string' },
        { name: 'party', type: 'string' },
        { name: 'residence', type: 'string' },
        { name: 'seniority', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'weight', type: 'float' },
        { name: 'isChairman', type: 'bool' },
      ],
    },
    {
      name: 'parties',
      attributes: [
        { name: 'chairmanId', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'seats', type: 'int' },
        { name: 'chairman', type: 'string' },
      ],
    },
  ],
};

export const flights = {
  edges: [
    {
      name: 'airports:airports',
      to: 'airports',
      from: 'airports',
      collection: 'flights',
      attributes: [
        {
          name: 'ArrTime',
          type: 'int',
        },
        {
          name: 'ArrTimeUTC',
          type: 'string',
        },
        {
          name: 'Day',
          type: 'int',
        },
        {
          name: 'DayOfWeek',
          type: 'int',
        },
        {
          name: 'DepTime',
          type: 'int',
        },
        {
          name: 'DepTimeUTC',
          type: 'string',
        },
        {
          name: 'Distance',
          type: 'int',
        },
        {
          name: 'FlightNum',
          type: 'int',
        },
        {
          name: 'Month',
          type: 'int',
        },
        {
          name: 'TailNum',
          type: 'string',
        },
        {
          name: 'UniqueCarrier',
          type: 'string',
        },
        {
          name: 'Year',
          type: 'int',
        },
      ],
    },
  ],
  nodes: [
    {
      name: 'airports',
      attributes: [
        { name: 'city', type: 'string' },
        { name: 'country', type: 'string' },
        { name: 'lat', type: 'float' },
        { name: 'long', type: 'float' },
        { name: 'name', type: 'string' },
        { name: 'state', type: 'string' },
        { name: 'vip', type: 'bool' },
      ],
    },
  ],
};
