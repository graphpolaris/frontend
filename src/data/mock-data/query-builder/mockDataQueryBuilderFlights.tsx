/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

import { Edge } from 'react-flow-renderer';
import { FunctionTypes } from '../../../domain/entity/query-builder/structures/FunctionTypes';
import { AnyNode } from '../../../domain/entity/query-builder/structures/Nodes';

/** Mock elements used for testing the query builder. */
/** DO NOT CHANGE THE NODES AS IT MIGHT BREAK TESTCASES IF YOU NEED NEW NODES JUST ADD THEM ADD NODES ON THE BOTTOM*/
export const mockElementsGroupBy: {
  nodes: AnyNode[];
  faultynodes: AnyNode[];
  links: Edge[];
} = {
  nodes: [
    {
      id: '26',
      type: 'relation',
      data: {
        name: 'member_of',
        collection: 'member_of',
        depth: { min: 1, max: 1 },
        fadeIn: false,
      },
      position: { x: -400, y: 110 },
    },
    {
      id: '27',
      type: 'entity',
      data: {
        name: 'parliament',
        fadeIn: false,
      },
      position: { x: 10, y: -50 },
    },
    {
      id: '28',
      type: 'entity',
      data: {
        name: 'parties',
        fadeIn: false,
      },
      position: { x: 100, y: -50 },
    },
    {
      id: `29`,
      type: 'function',
      data: {
        functionType: FunctionTypes.GroupBy,
        args: {
          group: { displayName: 'group', connectable: true, value: 'age', visible: true },
          by: { displayName: 'by', connectable: true, value: '_id', visible: true },
          relation: {
            displayName: 'relation',
            connectable: true,
            value: undefined,
            visible: false,
          },
          modifier: {
            displayName: 'modifier',
            connectable: false,
            value: 'AVG',
            visible: true,
          },
          constraints: {
            displayName: 'constraints',
            connectable: true,
            value: undefined,
            visible: true,
          },
        },
        fadeIn: false,
      },
      position: { x: 69, y: 420 },
    },
    {
      id: '40',
      type: 'attribute',
      data: {
        attribute: 'age',
        value: '45',
        dataType: 'int',
        matchType: 'GT',
        fadeIn: false,
      },
      position: { x: 10, y: 330 },
    },
  ],

  faultynodes: [],

  links: [],
};
