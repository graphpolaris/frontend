/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

import { Edge } from 'react-flow-renderer';
import { FunctionTypes } from '../../../domain/entity/query-builder/structures/FunctionTypes';
import { AnyNode } from '../../../domain/entity/query-builder/structures/Nodes';

/** Mock elements used for testing the query builder. */
/** DO NOT CHANGE THE NODES AS IT MIGHT BREAK TESTCASES IF YOU NEED NEW NODES JUST ADD THEM ADD NODES ON THE BOTTOM*/
export const mockElements: {
  nodes: AnyNode[];
  faultynodes: AnyNode[];
  links: Edge[];
} = {
  nodes: [
    {
      id: '1',
      type: 'entity',
      data: {
        name: 'airports',
        fadeIn: false,
      },
      position: { x: 10, y: -50 },
    },
    {
      id: '2',
      type: 'entity',
      data: {
        name: 'airports',
        fadeIn: false,
      },
      position: { x: 10, y: -20 },
    },
    {
      id: '3',
      type: 'entity',
      data: {
        name: 'airports',
        fadeIn: false,
      },
      position: { x: 10, y: 10 },
    },
    {
      id: '4',
      type: 'entity',
      data: {
        name: 'airports',
        fadeIn: false,
      },
      position: { x: 10, y: 40 },
    },

    {
      id: '5',
      type: 'relation',

      data: {
        collection: 'flights',
        name: 'flights',
        depth: { min: 1, max: 1 },
        fadeIn: false,
      },
      position: { x: 10, y: 80 },
    },
    {
      id: '6',
      type: 'relation',
      data: {
        name: 'flights',
        collection: 'flights',
        depth: { min: 1, max: 1 },
        fadeIn: false,
      },
      position: { x: -400, y: 110 },
    },

    {
      id: '7',
      type: 'attribute',
      data: {
        attribute: 'city',
        value: 'New York',
        dataType: 'string',
        matchType: 'exact',
        fadeIn: false,
      },
      position: { x: 10, y: 150 },
    },
    {
      id: '8',
      type: 'attribute',
      data: {
        attribute: 'city',
        value: 'San Francisco',
        dataType: 'string',
        matchType: 'exact',
        fadeIn: false,
      },
      position: { x: 10, y: 180 },
    },
    {
      id: '9',
      type: 'attribute',
      data: {
        attribute: 'state',
        value: 'HI',
        dataType: 'string',
        matchType: 'exact',
        fadeIn: false,
      },
      position: { x: 10, y: 210 },
    },

    {
      id: '10',
      type: 'attribute',
      data: {
        attribute: 'city',
        value: 'Greenville',
        dataType: 'string',
        matchType: 'exact',
        fadeIn: false,
      },
      position: { x: 10, y: 240 },
    },
    {
      id: '11',
      type: 'attribute',
      data: {
        attribute: 'vip',
        value: 'true',
        dataType: 'bool',
        matchType: 'EQ',
        fadeIn: false,
      },
      position: { x: 10, y: 270 },
    },
    {
      id: '12',
      type: 'attribute',
      data: {
        attribute: 'Day',
        value: '15',
        dataType: 'int',
        matchType: 'EQ',
        fadeIn: false,
      },
      position: { x: 10, y: 200 },
    },
    {
      id: '13',
      type: 'attribute',
      data: {
        attribute: 'Day',
        value: '8',
        dataType: 'int',
        matchType: 'EQ',
        fadeIn: false,
      },
      position: { x: 10, y: 330 },
    },
    {
      id: '14',
      type: 'attribute',
      data: {
        attribute: 'id',
        value: 'airports/KOA',
        dataType: 'string',
        matchType: 'exact',
        fadeIn: false,
      },
      position: { x: 10, y: 360 },
    },

    {
      id: '15',
      type: 'faultytype',
      data: {
        attribute: 'id',
        value: 'airports/KOA',
        dataType: 'string',
        matchType: 'exact',
        fadeIn: false,
      },
      position: { x: 10, y: 360 },
    },
    {
      id: `16`,
      type: 'function',
      data: {
        functionType: FunctionTypes.GroupBy,
        args: {
          group: { displayName: 'group', connectable: true, value: 'Day', visible: true },
          by: { displayName: 'by', connectable: true, value: 'city', visible: true },
          relation: {
            displayName: 'relation',
            connectable: true,
            value: undefined,
            visible: false,
          },
          modifier: {
            displayName: 'modifier',
            connectable: false,
            value: 'AVG',
            visible: true,
          },
        },
        fadeIn: false,
      },
      position: { x: 10, y: 390 },
    },
    {
      id: '17',
      type: 'entity',
      data: {
        name: 'airports',
        fadeIn: false,
      },
      position: { x: 300, y: -50 },
    },
    {
      id: `18`,
      type: 'function',
      data: {
        functionType: FunctionTypes.communityDetection,
        args: {
          CommunityDetection: {
            displayName: 'CommunityDetection',
            connectable: false,
            value: undefined,
            visible: true,
          },
        },
        fadeIn: false,
      },
      position: { x: 0, y: 0 },
    },
    {
      id: `19`,
      type: 'function',
      data: {
        functionType: FunctionTypes.link,
        args: {
          linkprediction: {
            displayName: 'linkprediction',
            connectable: false,
            value: undefined,
            visible: true,
          },
        },
        fadeIn: false,
      },
      position: { x: 0, y: 0 },
    },
    {
      id: `20`,
      type: 'function',
      data: {
        functionType: FunctionTypes.centrality,
        args: {
          centrality: {
            displayName: 'centrality',
            connectable: false,
            value: undefined,
            visible: true,
          },
        },
        fadeIn: false,
      },
      position: { x: 0, y: 0 },
    },
    {
      id: `21`,
      type: 'function',
      data: {
        functionType: FunctionTypes.shortestPath,
        args: {
          centrality: {
            displayName: 'shortestPath',
            connectable: false,
            value: undefined,
            visible: true,
          },
        },
        fadeIn: false,
      },
      position: { x: 0, y: 0 },
    },
  ],

  links: [],

  faultynodes: [
    {
      id: '69',
      type: 'attribute',
      data: {
        attribute: 'test',
        value: '15',
        dataType: 'int',
        matchType: 'EQ',
        fadeIn: false,
      },
      position: { x: 10, y: 300 },
    },
  ],
};
