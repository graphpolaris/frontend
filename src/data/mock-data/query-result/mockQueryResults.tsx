/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

/** Mock elements used for testing the query results. */
export default {
  newYorkResponse: `{"type":"query_result","values":{"edges":null,"nodes":[{"id":"airports/LGA","_key":"LGA","_rev":"_cPz8242-AK","attributes":{"city":"New York","country":"USA","lat":40.77724306,"long":-73.87260917,"name":"LaGuardia","state":"NY","vip":false}},{"id":"airports/JRB","_key":"JRB","_rev":"_cPz824y--S","attributes":{"city":"New York","country":"USA","lat":40.70121361,"long":-74.00902833,"name":"Downtown Manhattan/Wall St. Heliport","state":"NY","vip":false}},{"id":"airports/JRA","_key":"JRA","_rev":"_cPz824y--Q","attributes":{"city":"New York","country":"USA","lat":40.75454583,"long":-74.00708389,"name":"Port Authority-W 30th St Midtown Heliport","state":"NY","vip":false}},{"id":"airports/JFK","_key":"JFK","_rev":"_cPz824u-Ai","attributes":{"city":"New York","country":"USA","lat":40.63975111,"long":-73.77892556,"name":"John F Kennedy Intl","state":"NY","vip":true}},{"id":"airports/6N7","_key":"6N7","_rev":"_cPz8232--u","attributes":{"city":"New York","country":"USA","lat":40.73399083,"long":-73.97291639,"name":"New York Skyports Inc. SPB","state":"NY","vip":false}},{"id":"airports/6N5","_key":"6N5","_rev":"_cPz8232--s","attributes":{"city":"New York","country":"USA","lat":40.74260167,"long":-73.97208306,"name":"E 34th St Heliport","state":"NY","vip":false}}]}}`,
  newYorkParsedResponse: {
    nodes: [
      {
        id: 'airports/LGA',
        type: 1,
        displayInfo: 'LaGuardia',
        attributes: {
          city: 'New York',
          country: 'USA',
          lat: 40.77724306,
          long: -73.87260917,
          name: 'LaGuardia',
          state: 'NY',
          vip: false,
        },
      },
      {
        id: 'airports/JRB',
        type: 1,
        displayInfo: 'Downtown Manhattan/Wall St. Heliport',
        attributes: {
          city: 'New York',
          country: 'USA',
          lat: 40.70121361,
          long: -74.00902833,
          name: 'Downtown Manhattan/Wall St. Heliport',
          state: 'NY',
          vip: false,
        },
      },
      {
        id: 'airports/JRA',
        type: 1,
        displayInfo: 'Port Authority-W 30th St Midtown Heliport',
        attributes: {
          city: 'New York',
          country: 'USA',
          lat: 40.75454583,
          long: -74.00708389,
          name: 'Port Authority-W 30th St Midtown Heliport',
          state: 'NY',
          vip: false,
        },
      },
      {
        id: 'airports/JFK',
        type: 1,
        displayInfo: 'John F Kennedy Intl',
        attributes: {
          city: 'New York',
          country: 'USA',
          lat: 40.63975111,
          long: -73.77892556,
          name: 'John F Kennedy Intl',
          state: 'NY',
          vip: true,
        },
      },
      {
        id: 'airports/6N7',
        type: 1,
        displayInfo: 'New York Skyports Inc. SPB',
        attributes: {
          city: 'New York',
          country: 'USA',
          lat: 40.73399083,
          long: -73.97291639,
          name: 'New York Skyports Inc. SPB',
          state: 'NY',
          vip: false,
        },
      },
      {
        id: 'airports/6N5',
        type: 1,
        displayInfo: 'E 34th St Heliport',
        attributes: {
          city: 'New York',
          country: 'USA',
          lat: 40.74260167,
          long: -73.97208306,
          name: 'E 34th St Heliport',
          state: 'NY',
          vip: false,
        },
      },
    ],
    links: [],
    linkPrediction: false,
    communityDetection: false,
    shortestPath: false,
  },
  newYorkToSanFranciscoResponse: `{"type":"query_result","values":{"edges":[{"from":"airports/JFK","id":"flights/286552","_key":"286552","_rev":"_cP0-dsi--w","to":"airports/SFO","attributes":{"ArrTime":2332,"ArrTimeUTC":"2008-01-16T07:32:00.000Z","Day":15,"DayOfWeek":2,"DepTime":2006,"DepTimeUTC":"2008-01-16T01:06:00.000Z","Distance":2586,"FlightNum":649,"Month":1,"TailNum":"N597JB","UniqueCarrier":"B6","Year":2008}},{"from":"airports/JFK","id":"flights/286394","_key":"286394","_rev":"_cP0-dsK--e","to":"airports/SFO","attributes":{"ArrTime":2330,"ArrTimeUTC":"2008-01-16T07:30:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1958,"DepTimeUTC":"2008-01-16T00:58:00.000Z","Distance":2586,"FlightNum":19,"Month":1,"TailNum":"N537UA","UniqueCarrier":"UA","Year":2008}},{"from":"airports/JFK","id":"flights/283386","_key":"283386","_rev":"_cP0-dkK--h","to":"airports/SFO","attributes":{"ArrTime":2215,"ArrTimeUTC":"2008-01-16T06:15:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1849,"DepTimeUTC":"2008-01-15T23:49:00.000Z","Distance":2586,"FlightNum":73,"Month":1,"TailNum":"N3759","UniqueCarrier":"DL","Year":2008}},{"from":"airports/JFK","id":"flights/282498","_key":"282498","_rev":"_cP0-di---g","to":"airports/SFO","attributes":{"ArrTime":2358,"ArrTimeUTC":"2008-01-16T07:58:00.000Z","Day":15,"DayOfWeek":2,"DepTime":2052,"DepTimeUTC":"2008-01-16T01:52:00.000Z","Distance":2586,"FlightNum":9,"Month":1,"TailNum":"N554UA","UniqueCarrier":"UA","Year":2008}},{"from":"airports/JFK","id":"flights/282321","_key":"282321","_rev":"_cP0-dhi--f","to":"airports/SFO","attributes":{"ArrTime":2132,"ArrTimeUTC":"2008-01-16T05:32:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1825,"DepTimeUTC":"2008-01-15T23:25:00.000Z","Distance":2586,"FlightNum":647,"Month":1,"TailNum":"N651JB","UniqueCarrier":"B6","Year":2008}},{"from":"airports/JFK","id":"flights/281151","_key":"281151","_rev":"_cP0-deq--Y","to":"airports/SFO","attributes":{"ArrTime":2108,"ArrTimeUTC":"2008-01-16T05:08:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1746,"DepTimeUTC":"2008-01-15T22:46:00.000Z","Distance":2586,"FlightNum":17,"Month":1,"TailNum":"N560UA","UniqueCarrier":"UA","Year":2008}},{"from":"airports/JFK","id":"flights/281077","_key":"281077","_rev":"_cP0-dee--c","to":"airports/SFO","attributes":{"ArrTime":2119,"ArrTimeUTC":"2008-01-16T05:19:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1742,"DepTimeUTC":"2008-01-15T22:42:00.000Z","Distance":2586,"FlightNum":177,"Month":1,"TailNum":"N321AA","UniqueCarrier":"AA","Year":2008}},{"from":"airports/JFK","id":"flights/279990","_key":"279990","_rev":"_cP0-db6--C","to":"airports/SFO","attributes":{"ArrTime":1423,"ArrTimeUTC":"2008-01-15T22:23:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1114,"DepTimeUTC":"2008-01-15T16:14:00.000Z","Distance":2586,"FlightNum":11,"Month":1,"TailNum":"N555UA","UniqueCarrier":"UA","Year":2008}},{"from":"airports/JFK","id":"flights/279206","_key":"279206","_rev":"_cP0-da---Q","to":"airports/SFO","attributes":{"ArrTime":1406,"ArrTimeUTC":"2008-01-15T22:06:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1054,"DepTimeUTC":"2008-01-15T15:54:00.000Z","Distance":2586,"FlightNum":15,"Month":1,"TailNum":"N356AA","UniqueCarrier":"AA","Year":2008}},{"from":"airports/JFK","id":"flights/278458","_key":"278458","_rev":"_cP0-dYO--K","to":"airports/SFO","attributes":{"ArrTime":2003,"ArrTimeUTC":"2008-01-16T04:03:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1624,"DepTimeUTC":"2008-01-15T21:24:00.000Z","Distance":2586,"FlightNum":151,"Month":1,"TailNum":"N3744D","UniqueCarrier":"DL","Year":2008}},{"from":"airports/JFK","id":"flights/276601","_key":"276601","_rev":"_cP0-dTu--I","to":"airports/SFO","attributes":{"ArrTime":1909,"ArrTimeUTC":"2008-01-16T03:09:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1535,"DepTimeUTC":"2008-01-15T20:35:00.000Z","Distance":2586,"FlightNum":15,"Month":1,"TailNum":"N510UA","UniqueCarrier":"UA","Year":2008}},{"from":"airports/JFK","id":"flights/276139","_key":"276139","_rev":"_cP0-dSq--e","to":"airports/SFO","attributes":{"ArrTime":1815,"ArrTimeUTC":"2008-01-16T02:15:00.000Z","Day":15,"DayOfWeek":2,"DepTime":1524,"DepTimeUTC":"2008-01-15T20:24:00.000Z","Distance":2586,"FlightNum":85,"Month":1,"TailNum":"N373AA","UniqueCarrier":"AA","Year":2008}},{"from":"airports/JFK","id":"flights/273773","_key":"273773","_rev":"_cP0-dNO--I","to":"airports/SFO","attributes":{"ArrTime":1156,"ArrTimeUTC":"2008-01-15T19:56:00.000Z","Day":15,"DayOfWeek":2,"DepTime":840,"DepTimeUTC":"2008-01-15T13:40:00.000Z","Distance":2586,"FlightNum":2979,"Month":1,"TailNum":"N387AA","UniqueCarrier":"AA","Year":2008}},{"from":"airports/JFK","id":"flights/273538","_key":"273538","_rev":"_cP0-dMu--A","to":"airports/SFO","attributes":{"ArrTime":1131,"ArrTimeUTC":"2008-01-15T19:31:00.000Z","Day":15,"DayOfWeek":2,"DepTime":831,"DepTimeUTC":"2008-01-15T13:31:00.000Z","Distance":2586,"FlightNum":893,"Month":1,"TailNum":"N505UA","UniqueCarrier":"UA","Year":2008}},{"from":"airports/JFK","id":"flights/272322","_key":"272322","_rev":"_cP0-dJ6--C","to":"airports/SFO","attributes":{"ArrTime":1118,"ArrTimeUTC":"2008-01-15T19:18:00.000Z","Day":15,"DayOfWeek":2,"DepTime":757,"DepTimeUTC":"2008-01-15T12:57:00.000Z","Distance":2586,"FlightNum":641,"Month":1,"TailNum":"N528JB","UniqueCarrier":"B6","Year":2008}},{"from":"airports/JFK","id":"flights/271860","_key":"271860","_rev":"_cP0-dI2--V","to":"airports/SFO","attributes":{"ArrTime":1133,"ArrTimeUTC":"2008-01-15T19:33:00.000Z","Day":15,"DayOfWeek":2,"DepTime":743,"DepTimeUTC":"2008-01-15T12:43:00.000Z","Distance":2586,"FlightNum":877,"Month":1,"TailNum":"N502UA","UniqueCarrier":"UA","Year":2008}},{"from":"airports/JFK","id":"flights/270140","_key":"270140","_rev":"_cP0-dEy--K","to":"airports/SFO","attributes":{"ArrTime":1042,"ArrTimeUTC":"2008-01-15T18:42:00.000Z","Day":15,"DayOfWeek":2,"DepTime":650,"DepTimeUTC":"2008-01-15T11:50:00.000Z","Distance":2586,"FlightNum":59,"Month":1,"TailNum":"N399AA","UniqueCarrier":"AA","Year":2008}},{"from":"airports/JFK","id":"flights/269347","_key":"269347","_rev":"_cP0-dC2--j","to":"airports/SFO","attributes":{"ArrTime":913,"ArrTimeUTC":"2008-01-15T17:13:00.000Z","Day":15,"DayOfWeek":2,"DepTime":606,"DepTimeUTC":"2008-01-15T11:06:00.000Z","Distance":2586,"FlightNum":5,"Month":1,"TailNum":"N537UA","UniqueCarrier":"UA","Year":2008}}],"nodes":[{"id":"airports/SFO","_key":"SFO","_rev":"_cPz825i-_J","attributes":{"city":"San Francisco","country":"USA","lat":37.61900194,"long":-122.3748433,"name":"San Francisco International","state":"CA","vip":true}},{"id":"airports/JFK","_key":"JFK","_rev":"_cPz824u-Ai","attributes":{"city":"New York","country":"USA","lat":40.63975111,"long":-73.77892556,"name":"John F Kennedy Intl","state":"NY","vip":true}}]}}`,
  newYorkToSanFranciscoExpectedNodes: [
    {
      type: 1,
      id: 'airports/SFO',
      displayInfo: 'San Francisco International',
      attributes: {
        city: 'San Francisco',
        country: 'USA',
        lat: 37.61900194,
        long: -122.3748433,
        name: 'San Francisco International',
        state: 'CA',
        vip: true,
      },
    },
    {
      type: 1,
      id: 'airports/JFK',
      displayInfo: 'John F Kennedy Intl',
      attributes: {
        city: 'New York',
        country: 'USA',
        lat: 40.63975111,
        long: -73.77892556,
        name: 'John F Kennedy Intl',
        state: 'NY',
        vip: true,
      },
    },
  ],
  newYorkToSanFranciscoExpectedLinks: [
    { source: 'airports/JFK', target: 'airports/SFO', value: 18 },
  ],
  badQueryResponse: `{"bad":"response"}`,
};
