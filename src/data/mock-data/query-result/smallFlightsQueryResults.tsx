/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

export default {
  edges: [
    {
      from: 'airports/JFK',
      to: 'airports/SFO',
      id: 'flights/1',
      value: 18,
      attributes: {
        Year: 2008,
        Month: 1,
        Day: 9,
        DayOfWeek: 3,
        DepTime: 2023,
        ArrTime: 2050,
        DepTimeUTC: '2008-01-10T03:23:00.000Z',
        ArrTimeUTC: '2008-01-10T04:50:00.000Z',
        UniqueCarrier: 'WN',
        FlightNum: 1493,
        TailNum: 'N694SW',
        Distance: 487,
      },
    },
    {
      id: 'flights/2',
      from: 'airports/SFO',
      to: 'airports/JFK',
      value: 18,
      attributes: {
        Year: 2008,
        Month: 1,
        Day: 9,
        DayOfWeek: 3,
        DepTime: 2023,
        ArrTime: 2050,
        DepTimeUTC: '2008-01-10T03:23:00.000Z',
        ArrTimeUTC: '2008-01-10T04:50:00.000Z',
        UniqueCarrier: 'NW',
        FlightNum: 1493,
        TailNum: 'N694SW',
        Distance: 200,
      },
    },
  ],
  nodes: [
    {
      group: 0,
      id: 'airports/SFO',
      displayInfo: 'San Francisco International',
      attributes: {
        city: 'San Francisco',
        country: 'USA',
        lat: 37.61900194,
        long: -122.3748433,
        name: 'San Francisco International',
        state: 'CA',
        vip: true,
      },
    },
    {
      group: 0,
      id: 'airports/JFK',
      displayInfo: 'John F Kennedy Intl',
      attributes: {
        city: 'New York',
        country: 'USA',
        lat: 40.63975111,
        long: -73.77892556,
        name: 'John F Kennedy Intl',
        state: 'NY',
        vip: true,
      },
    },
  ],
};
