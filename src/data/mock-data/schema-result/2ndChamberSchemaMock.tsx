/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

/** Mock elements used for testing the schema result. */
export default {
  nodes: [
    {
      name: 'commissies',
      attributes: [
        {
          name: 'naam',
          type: 'string',
        },
      ],
    },
    {
      name: 'kamerleden',
      attributes: [
        {
          name: 'anc',
          type: 'int',
        },
        {
          name: 'img',
          type: 'string',
        },
        {
          name: 'leeftijd',
          type: 'int',
        },
        {
          name: 'naam',
          type: 'string',
        },
        {
          name: 'partij',
          type: 'string',
        },
        {
          name: 'woonplaats',
          type: 'string',
        },
      ],
    },
    {
      name: 'moties',
      attributes: [
        {
          name: 'datum',
          type: 'string',
        },
        {
          name: 'indiener',
          type: 'string',
        },
        {
          name: 'info',
          type: 'string',
        },
      ],
    },
    {
      name: 'partijen',
      attributes: [
        {
          name: 'naam',
          type: 'string',
        },
        {
          name: 'voorzitter',
          type: 'string',
        },
        {
          name: 'voorzitterId',
          type: 'int',
        },
        {
          name: 'zetels',
          type: 'int',
        },
      ],
    },
  ],
  edges: [
    {
      name: 'moties:kamerleden',
      collection: 'dient_in',
      from: 'moties',
      to: 'kamerleden',
      attributes: [],
    },
    {
      name: 'partijen:kamerleden',
      collection: 'lid_van',
      from: 'partijen',
      to: 'kamerleden',
      attributes: [
        {
          name: 'isVoorzitter',
          type: 'bool',
        },
      ],
    },
    {
      name: 'kamerleden:commissies',
      collection: 'onderdeel_van',
      from: 'kamerleden',
      to: 'commissies',
      attributes: [],
    },
  ],
};
