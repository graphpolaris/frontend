/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock data.
 * See testing plan for more details.*/

export default {
  nodes: [
    {
      name: 'airports',
      attributes: [
        { name: 'state', type: 'string' },
        { name: 'country', type: 'string' },
        { name: 'lat', type: 'float' },
        { name: 'vip', type: 'bool' },
        { name: 'name', type: 'string' },
        { name: 'city', type: 'string' },
        { name: 'long', type: 'float' },
      ],
    },
  ],
  edges: [
    {
      name: 'flights',
      collection: 'flights',
      from: 'airports',
      to: 'airports',
      attributes: [
        { name: 'Distance', type: 'int' },
        { name: 'ArrTimeUTC', type: 'string' },
        { name: 'FlightNum', type: 'int' },
        { name: 'DepTimeUTC', type: 'string' },
        { name: 'ArrTime', type: 'int' },
        { name: 'Day', type: 'int' },
        { name: 'UniqueCarrier', type: 'string' },
        { name: 'TailNum', type: 'string' },
        { name: 'Year', type: 'int' },
        { name: 'Month', type: 'int' },
        { name: 'DepTime', type: 'int' },
        { name: 'DayOfWeek', type: 'int' },
      ],
    },
  ],
};
