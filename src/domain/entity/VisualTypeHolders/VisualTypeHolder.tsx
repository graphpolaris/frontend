/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from '../broker/broker';
import BrokerListener from '../broker/BrokerListenerInterface';
import { isNodeLinkResult } from '../query-result/structures/NodeLinkResultType';
import VisualTypeListener from './VisualTypeListener';

export type NodeLinkVisualTypes =
  | 'node-link'
  | 'semantic-substrates'
  | 'json'
  | 'hiveplots'
  | 'paohvis';
export type PossibleVisualTypes = NodeLinkVisualTypes;

/** VisualTypeHolder is an observer class for notifying listeners when the type of visualisation changes. */
export default class VisualTypeHolder implements BrokerListener {
  private visualTypeListeners: VisualTypeListener[];
  private visualType: PossibleVisualTypes;
  private previousNodeLinkVisual: NodeLinkVisualTypes;

  public constructor() {
    this.visualTypeListeners = [];
    this.visualType = 'node-link';
    // Remember the last kind of visualisation for a node-link result
    this.previousNodeLinkVisual = 'node-link';

    // Listen to the broker for incoming query results
    // Change the visualisation type depending on the query result format
    Broker.instance().subscribe(this, 'query_result');
  }

  /**
   * Changes the visualisation type depending on the query result
   * @param message The data from a query result.
   */
  consumeMessageFromBackend(message: unknown): void {
    if (isNodeLinkResult(message)) {
      this.changeVisualType(this.previousNodeLinkVisual);
    } else this.changeVisualType('json');
  }

  /**
   * Function changes the visualType to the new visualType.
   * Afterwards notifies listeners about changes.
   * @param visualType - This is the new visualType.
   */
  public changeVisualType(visualType: PossibleVisualTypes): void {
    if (visualType != this.visualType) {
      if (
        visualType == 'node-link' ||
        visualType == 'semantic-substrates' ||
        /* By leaving out JSON when you send a query and it is a nodelinkresult type it will change to to a nodelinkresulttype again
        visualType == 'json' ||*/
        visualType == 'paohvis' ||
        visualType == 'hiveplots'
      )
        this.previousNodeLinkVisual = visualType;
      this.visualType = visualType;
      this.notifyListeners();
    }
  }

  /**
   * This returns the current visualType.
   * @returns {string} The name of the visualisation.
   */
  public getVisualType(): string {
    return this.visualType;
  }

  /**
   * Adds a listener to the list of VisualType listeners.
   * @param visualTypeListener A listener to the VisualTypeHolder observer.
   */
  public addVisualTypeListener(visualTypeListener: VisualTypeListener): void {
    this.visualTypeListeners.push(visualTypeListener);
  }

  /**
   * Removes a listener from the list of VisualType listeners.
   * @param visualTypeListener A listener to the VisualTypeHolder observer.
   */
  public removeVisualTypeListener(visualTypeListener: VisualTypeListener): void {
    this.visualTypeListeners.splice(this.visualTypeListeners.indexOf(visualTypeListener), 1);
  }

  /** Notifies the listeners that the visualisation has changed.  */
  private notifyListeners(): void {
    this.visualTypeListeners.forEach((listener) => listener.onVisualTypeChanged());
  }
}
