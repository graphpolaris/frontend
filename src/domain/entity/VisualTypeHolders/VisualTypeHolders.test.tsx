/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import VisualTypeHolder from './VisualTypeHolder';
import VisualisationListenerMock from './VisualisationListenerMock';
describe('visualTypeHolder', () => {
  let visualTypeHolder: VisualTypeHolder;
  let visualisationListenerMock: VisualisationListenerMock;
  beforeEach(() => {
    visualTypeHolder = new VisualTypeHolder();
    visualisationListenerMock = new VisualisationListenerMock();
  });

  it('Handle the consumefromBackend correctly1', () => {
    const data: any = {
      nodes: [],
      edges: [],
    };
    visualTypeHolder.changeVisualType('paohvis');
    expect(visualTypeHolder.getVisualType()).toEqual('paohvis');
    visualTypeHolder.consumeMessageFromBackend(data);
    expect(visualTypeHolder.getVisualType()).toEqual('paohvis');
  });
  it('Handle the consumefromBackend correctly2', () => {
    visualTypeHolder.changeVisualType('paohvis');
    visualTypeHolder.consumeMessageFromBackend('corruptedData');
    expect(visualTypeHolder.getVisualType()).toEqual('json');
  });
  it('Should notify the listeners', () => {
    const logSpy = jest.spyOn(console, 'log');
    visualTypeHolder.addVisualTypeListener(visualisationListenerMock);
    visualTypeHolder.changeVisualType('json');
    expect(logSpy).toBeCalledWith(visualisationListenerMock.workingMessage);
  });
  it('Removing an listener shouldnot notify it anymore ', () => {
    const logSpy = jest.spyOn(console, 'log');
    visualTypeHolder.addVisualTypeListener(visualisationListenerMock);
    visualTypeHolder.removeVisualTypeListener(visualisationListenerMock);
    visualTypeHolder.changeVisualType('json');
    expect(logSpy).not.toBeCalledWith(visualisationListenerMock.workingMessage);
  });
});
