/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** Interface for a listener of the VisualTypeHolder observer */
export default interface VisualisationListener {
  onVisualTypeChanged(): void;
}
