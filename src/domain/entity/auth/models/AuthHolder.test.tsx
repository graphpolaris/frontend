/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import AuthHolder from './AuthHolder';
import AuthListnerMock from './AuthListnerMock';
import AuthListernerMock from './AuthListnerMock';

describe('AuthHolder', () => {
  let authHolder: AuthHolder;
  let AuthListernerMock: AuthListnerMock;

  beforeEach(() => {
    authHolder = new AuthHolder();
    AuthListernerMock = new AuthListnerMock();
  });

  it('Should add a listner', () => {
    authHolder.addAuthListener(AuthListernerMock);
    expect(authHolder['authListeners']).toEqual([AuthListernerMock]);
  });

  it('Should remove a listner', () => {
    authHolder.addAuthListener(AuthListernerMock);
    expect(authHolder['authListeners']).toEqual([AuthListernerMock]);
    authHolder.removeAuthListener(AuthListernerMock);
    expect(authHolder['authListeners']).toEqual([]);
  });
  it('Should get the databases names or show the correct error', () => {
    const listDatabaseNames = ['testDatabase'];
    authHolder.setDatabaseNames(listDatabaseNames);
    expect(() => authHolder.getDatabaseNames()).toThrowError('User is not authorized');
    authHolder.onSignedIn('', '');
    expect(authHolder.getDatabaseNames()).toEqual(listDatabaseNames);
  });
  it('Should get the client id / session ID', () => {
    const signInName = 'testID';
    expect(() => authHolder.getClientID()).toThrowError('User is not authorized');
    expect(() => authHolder.getSessionID()).toThrowError('User is not authorized');
    authHolder.onSignedIn(signInName, signInName);
    expect(authHolder.getClientID()).toEqual(signInName);
    expect(authHolder.getSessionID()).toEqual(signInName);
  });
});
