/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import AuthListener from './AuthListener';

/** AuthHolder is an observer class for notifying listeners when the authorization status changes. */
export default class AuthHolder {
  private authListeners: AuthListener[];
  private isAuthorized: boolean;
  private clientID: string;
  private sessionID: string;
  private databaseNames: string[];
  private userNotAuthorisedError = new Error('User is not authorized');
  public constructor() {
    this.isAuthorized = false;
    this.authListeners = [];
    this.clientID = '';
    this.sessionID = '';
    this.databaseNames = [];
  }

  /**
   * Called when the user is succesfully authorized.
   * @param clientID The clientID of the just signed in user.
   * @param sessionID The sessionID of the user.
   */
  public onSignedIn(clientID: string, sessionID: string): void {
    this.isAuthorized = true;
    this.clientID = clientID;
    this.sessionID = sessionID;
    this.notifyListeners();
  }

  /** Called when the user signs out. Resets all values to default. */
  public onSignedOut(): void {
    this.isAuthorized = false;
    this.clientID = '';
    this.sessionID = '';
    this.databaseNames = [];
    this.notifyListeners();
  }

  /**
   * Sets the connected database of the user.
   * @param databaseNames The database names to set.
   */
  public setDatabaseNames(databaseNames: string[]): void {
    this.databaseNames = databaseNames;
    this.notifyListeners();
  }

  /**
   * Checks if a user is authorized.
   * @returns {boolean} A boolean that is true if the user is authorized.
   */
  public isUserAuthorized(): boolean {
    return this.isAuthorized;
  }

  /**
   * Gets the id of the client.
   * @throws {Error} If user is not authorized.
   * @returns {string} The clientID if the user is signed in.
   */
  public getClientID(): string {
    if (!this.isAuthorized) throw this.userNotAuthorisedError;
    return this.clientID;
  }

  /**
   * Gets the sessionID of the user.
   * @throws {Error} If user is not authorized.
   * @returns {string} The SessionID if the user is signed in.
   */
  public getSessionID(): string {
    if (!this.isAuthorized) throw this.userNotAuthorisedError;
    return this.sessionID;
  }

  /**
   * Gets the name of the database.
   * @throws {Error} If user is not authorized.
   * @returns {string[]} The database names if the user is signed in.
   */
  public getDatabaseNames(): string[] {
    if (!this.isAuthorized) throw this.userNotAuthorisedError;
    return this.databaseNames;
  }

  /**
   * Adds a listener to the observer.
   * @param authListener The listener we want to add.
   */
  public addAuthListener(authListener: AuthListener): void {
    this.authListeners.push(authListener);
  }

  /**
   * Removes a listener from the list of listeners.
   * @param authListener The listener we want to remove.
   */
  public removeAuthListener(authListener: AuthListener): void {
    this.authListeners.splice(this.authListeners.indexOf(authListener), 1);
  }

  /** Notifies all the listeners that the authentication changed. */
  private notifyListeners(): void {
    this.authListeners.forEach((listener) => listener.onAuthChanged());
  }
}
