/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test mock implementations.
 * See testing plan for more details.*/
// Made with the intent of testing the AuthListner.
import AuthListener from './AuthListener';

export default class AuthListnerMock implements AuthListener {
  onAuthChanged(): void {
    return;
  }
}
