/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
/** Interface that holds the result of an authentification. */
export default interface AuthorizationResult {
  clientID: string;
  sessionID: string;
}
