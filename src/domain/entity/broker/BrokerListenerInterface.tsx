/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
export default interface BrokerListener {
  /**
   * The callback method for consuming a message from the backend.
   * This method will be called by the broker to notify listeners of a new message from the backend.
   * @param message The message that will be consumed.
   * @param message The object that will be consumed.
   */
  consumeMessageFromBackend(message: unknown, routingKey: string): void;
}
