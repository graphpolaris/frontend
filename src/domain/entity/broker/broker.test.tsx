/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from './broker';
import BrokerListener from './BrokerListenerInterface';

/** Testsuite for the testing of the broker and the broker listeners */
describe('broker', () => {
  it('should correctly notify a listener with the correct routingkey', () => {
    const consumeFuncMock = jest.fn();
    const brokerListener: BrokerListener = {
      consumeMessageFromBackend: consumeFuncMock,
    };

    // Subscribe the mock listener
    Broker.instance().subscribe(brokerListener, 'testkey');
    Broker.instance().publish({ test: 'hoi' }, 'testkey');
    expect(consumeFuncMock.mock.calls[0][0]).toEqual({ test: 'hoi' });

    // Test if it notifies our listener with a different routingkey
    Broker.instance().publish({ test: 'hoi' }, 'differentKey');
    expect(consumeFuncMock).toBeCalledTimes(1);

    // When unsubscribed we shouldn't get notified
    Broker.instance().unSubscribe(brokerListener, 'testkey');
    Broker.instance().publish({ test: 'hoi' }, 'testkey');
    expect(consumeFuncMock).toBeCalledTimes(1);
  });

  it('should not throw when unsubscribing an already unsubbed listener', () => {
    const consumeFuncMock = jest.fn();
    const brokerListener: BrokerListener = {
      consumeMessageFromBackend: consumeFuncMock,
    };

    // Test unsubscribing a listener to a routing key it was not subscribed to.
    expect(() => Broker.instance().unSubscribe(brokerListener, 'differentkey')).not.toThrow();
  });

  it('should only subscribe a listener once to the same routing key', () => {
    const consumeFuncMock = jest.fn();
    const brokerListener: BrokerListener = {
      consumeMessageFromBackend: consumeFuncMock,
    };
    // Subscribe the mock listener twice.
    Broker.instance().subscribe(brokerListener, 'testkey', false);
    Broker.instance().subscribe(brokerListener, 'testkey', false);
    Broker.instance().publish({ test: 'hahaha' }, 'testkey');

    expect(consumeFuncMock).toBeCalledTimes(1);

    Broker.instance().unSubscribe(brokerListener, 'testkey');
  });

  it('should console log when there are no listeners to publish to', () => {
    const mockConsoleLog = jest.fn();
    console.log = mockConsoleLog;

    Broker.instance().publish({ test: 'hoi' }, 'testkey');

    expect(mockConsoleLog).toBeCalledWith(
      'no listeners for message with routing key %ctestkey',
      'font-weight:bold; color: blue; background-color: white;',
      { test: 'hoi' },
    );
  });

  it('should notify when there is a previous message available after subscribing', () => {
    const consumeFuncMock = jest.fn();
    const brokerListener: BrokerListener = {
      consumeMessageFromBackend: consumeFuncMock,
    };

    Broker.instance().publish({ test: '⚽' }, 'testkey');

    // Subscribe the mock listener twice.
    Broker.instance().subscribe(brokerListener, 'testkey', true);

    expect(consumeFuncMock.mock.calls[0][0]).toEqual({ test: '⚽' });
  });
});
