/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BrokerListener from './BrokerListenerInterface';

/**
 * A broker that handles incoming messages from the backend.
 * It works with routingkeys, a listener can subscribe to messages from the backend with a specific routingkey.
 * Possible routingkeys:
 * - query_result:              Contains an object with nodes and edges or a numerical result.
 * - query_translation_result:  Contains the query translated to the database language.
 * - schema_result:             Contains the schema of the users database.
 * - query_status_update:       Contains an update to if a query is being executed.
 * - query_database_error:      Contains the error received from the database.
 * - query_sent:                Contains the message that a query has been send.
 * - gsa_node_result:           Contains a node that has the data for the graph-schema-analytics
 * - gsa_edge_result:           Contains a edge that has the data for the graph-schema-analytics
 */
export default class Broker {
  private static singletonInstance: Broker;
  private listeners: Record<string, BrokerListener[]> = {};

  /** mostRecentMessages is a dictionary with <routingkey, messageObject>. It stores the most recent message for that routingkey. */
  private mostRecentMessages: Record<string, unknown> = {};

  /** Get the singleton instance of the Broker. */
  public static instance(): Broker {
    if (!this.singletonInstance) this.singletonInstance = new Broker();
    return this.singletonInstance;
  }

  /**
   * Notify all listeners which are subscribed with the specified routingkey.
   * @param {unknown} jsonObject An json object with unknown type.
   * @param {string} routingKey The routing to publish the message to.
   */
  public publish(jsonObject: unknown, routingKey: string): void {
    this.mostRecentMessages[routingKey] = jsonObject;

    if (this.listeners[routingKey] && this.listeners[routingKey].length != 0)
      this.listeners[routingKey].forEach((listener) =>
        listener.consumeMessageFromBackend(jsonObject, routingKey),
      );
    // If there are no listeners, log the message
    else
      console.log(
        `no listeners for message with routing key %c${routingKey}`,
        'font-weight:bold; color: blue; background-color: white;',
        jsonObject,
      );
  }

  /**
   * Subscribe a listener to messages with the specified routingKey.
   * @param {BrokerListener} newListener The listener to subscribe.
   * @param {string} routingKey The routingkey to subscribe to.
   * @param {boolean} consumeMostRecentMessage If true and there is a message for this routingkey available, notify the new listener. Default true.
   */
  public subscribe(
    newListener: BrokerListener,
    routingKey: string,
    consumeMostRecentMessage = true,
  ): void {
    if (!this.listeners[routingKey]) this.listeners[routingKey] = [];

    // Don't add a listener twice
    if (!this.listeners[routingKey].includes(newListener)) {
      this.listeners[routingKey].push(newListener);

      // Consume the most recent message
      if (consumeMostRecentMessage && routingKey in this.mostRecentMessages)
        newListener.consumeMessageFromBackend(this.mostRecentMessages[routingKey], routingKey);
    }
  }

  /**
   * Unsubscribes a listener from messages with specified routingkey.
   * @param {BrokerListener} newListener The listener to unsubscribe.
   * @param {string} routingKey The routing key to unsubscribe from
   */
  public unSubscribe(listener: BrokerListener, routingKey: string): void {
    if (this.listeners[routingKey]) {
      let i = this.listeners[routingKey].indexOf(listener);
      if (i > -1) this.listeners[routingKey].splice(i, 1);
    }
  }
}
