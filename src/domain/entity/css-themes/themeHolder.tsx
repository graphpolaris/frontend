/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
export const SchemaThemeHolder = {
  fontFamily: 'monospace',
  fontSize: 11,
  entity: {
    baseColor: '#e9e9e9',
    secondColor: '#ffac57',
    qualityColor: '#ff9727',
    textColor: 'black',
    height: 20,
    width: 165,
  },
  relation: {
    baseColor: '#e9e9e9',
    secondColor: '#3bc4ff',
    qualityColor: '#02aaf2',
    textColor: 'black',
    height: 20,
    width: 145,
  },

  attribute: {
    baseColor: '#E2D9D3',
    secondColor: '#ECB880',
    textColor: '#181520',
    height: 17,
  },
};

export const QueryBuilderThemeHolder = {
  fontFamily: 'monospace',
  fontSize: 11,
  entity: {
    baseColor: '#e9e9e9',
    highlightColor: '#C48546',
    secondColor: '#ECB880',
    textColor: 'black',
    height: 20,
    width: 165,
  },
  relation: {
    baseColor: '#1fa2a2', //'#25b7a3',
    secondColor: '#82d9d9',
    textColor: 'black',
    width: 240,
    height: 20,
    handle: {
      baseColor: '#166262',
      secondColor: '#7aecd8',
      width: 5,
    },
  },
  function: {
    baseColor: '#8c75c9', //'#25b7a3',
    secondColor: '#a18dd7',
    textColor: 'black',
    height: 20,
    handle: {
      baseColor: '#6d55ad',
      secondColor: '#a18dd7',
      width: 5,
    },
  },
};
