/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
export const ColourPalettes: { [key: string]: any } = {
  availablePalettes: ['default', 'dark'],

  // default
  default: {
    nodes: [
      '181520', // black
      'd49350', // orange
      '1e9797', // blue
      'd56a50', // red
      '800000',
      'fabed4',
      '808000',
      'ffe119',
      'bfef45',
      '3cb44b',
      '42d4f4',
      '000075',
      '4363d8',
      '911eb4',
      'dcbeff',
      'f032e6',
      'a9a9a9',
      '2d7c0b',
      '00ff00',
      '0000ff',
    ],
    nodesBase: ['e9e9e9'],
    elements: {
      entityBase: ['ffac57', 'ECB880', 'a36a30'], // normal, lighter, darker
      entitySecond: ['ff9727'],
      relation: ['1FA2A2', '82d9d9', '166262'],
      relationBase: ['3bc4ff', '82d9d9', '166262'],
      relationSecond: ['02aaf2'],
      attribute: ['bfb6af', 'f8f2ee', 'e2d9d3'],
      function: ['8c75c9', 'ECB880', 'a36a30'],
    },
    visEdge: '999999',
    nodeHighlightedEdge: '30A530',
    background: 'fffdfa',
    visBackground: 'fffdfa',
    attr: 'e2d9d3',
    entry: 'd49350',
    relation: '1e9797',
    graphEdge: '181520',
    builderEdge: '000000',
    text: '000000',
    menuText: '000000',
    elementText: '000000',
    dragger: 'dddddd',
    logo: 'black',
  },

  // dark
  dark: {
    nodes: ['eaeaea', '5358ed', '4eedba', 'b665ed', 'bd73ef', 'c380f0', 'c88cf1'],
    nodesBase: ['e9e9e9'],
    elements: {
      entityBase: ['ffac57', 'ECB880', 'a36a30'], // normal, lighter, darker
      entitySecond: ['ff9727'],
      relationBase: ['3bc4ff', '82d9d9', '166262'],
      relationSecond: ['02aaf2'],
      relation: ['4eedba', 'DBF9EF', '36A580'],
      attribute: ['D7D7D7', 'f8f2ee', 'e2d9d3'],
      function: ['B665ED', 'CDA4EA', '8048A8'],
    },
    visEdge: '999999',
    nodeHighlightedEdge: '30A530',
    background: '171721',
    visBackground: '292935',
    attr: 'e2d9d3',
    graphEdge: 'eaeaea',
    builderEdge: 'ffffff',
    text: 'ff00ff',
    menuText: 'ffffff',
    elementText: '000000',
    dragger: '171721',
    logo: 'white',
  },
};
