/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** Interface that describes functions needed to export a visualisation. */
export default interface Exportable {
  /** Exports the visualisation to PDF. */
  exportToPDF(): void;
  /** Exports the visualisation to PNG. */
  exportToPNG(): void;
}
