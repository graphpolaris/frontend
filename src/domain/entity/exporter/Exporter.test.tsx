/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Exportable from './Exportable';
import Exporter from './Exporter';
/** Testsuite to test the visualisation exporter */
describe('Exporter', () => {
  it('should call the export function of a visualisation', () => {
    const mockExportPNG = jest.fn();
    const mockExportPDF = jest.fn();
    const exportableVis: Exportable = {
      exportToPDF: mockExportPDF,
      exportToPNG: mockExportPNG,
    };

    Exporter.instance().setCurrentResultVisualisation(exportableVis);

    expect(mockExportPDF).toHaveBeenCalledTimes(0);
    Exporter.instance().exportCurrentVisualisationToPDF();
    expect(mockExportPDF).toHaveBeenCalledTimes(1);

    expect(mockExportPNG).toHaveBeenCalledTimes(0);
    Exporter.instance().exportCurrentVisualisationToPNG();
    expect(mockExportPNG).toHaveBeenCalledTimes(1);

    Exporter.instance().unSetCurrentResultVisualisation();
    Exporter.instance().exportCurrentVisualisationToPDF();
    expect(mockExportPDF).toHaveBeenCalledTimes(1);
  });
});
