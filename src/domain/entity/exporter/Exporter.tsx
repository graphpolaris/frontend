/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Exportable from './Exportable';

/**
 * Singleton class for exporting files.
 * Contains functionality for exporting the current displayed result visualisation.
 */
export default class Exporter {
  private static singletonInstance: Exporter;
  currentResultVisualisation?: Exportable;

  /** Get the singleton instance of the Broker. */
  public static instance(): Exporter {
    if (!this.singletonInstance) {
      this.singletonInstance = new Exporter();
    }

    return this.singletonInstance;
  }

  /**
   * Sets the current result visualisation, meaning that this is the currently displayed visualisation.
   * @param {Exportable} resultVisualisation The exportable result visualisation to set.
   */
  setCurrentResultVisualisation(resultVisualisation: Exportable): void {
    this.currentResultVisualisation = resultVisualisation;
  }

  /** Sets the currently result visualisation to undefined, meaning that there is not visualisation displayed. */
  unSetCurrentResultVisualisation(): void {
    this.currentResultVisualisation = undefined;
  }

  /** Calls the exportToPDF() function of the current visualisation if there is one. */
  exportCurrentVisualisationToPDF(): void {
    if (this.currentResultVisualisation) this.currentResultVisualisation.exportToPDF();
  }

  /** Calls the exportToPNG() function of the current visualisation if there is one. */
  exportCurrentVisualisationToPNG(): void {
    if (this.currentResultVisualisation) this.currentResultVisualisation.exportToPNG();
  }
}
