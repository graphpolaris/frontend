/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { isAttributeDataEntity, isAttributeDataRelation } from './AttributeDataType';
import {
  mockAttributeDataNLDifferentTypes1,
  mockAttributeDataNLDifferentTypes2,
  mockAttributeDataNLDifferentTypes3,
  mockAttributeDataNLEdge1,
  mockAttributeDataNLInvalidLengthNumerical,
  mockAttributeDataNLInvalidType,
  mockAttributeDataNLNode1,
  mockAttributeDataNLNode1AttributeArrayIsNoArray,
  mockAttributeDataNLUniqueCategoricalValuesIsNoArray,
  mockAttributeDataNLUniqueCategoricalValuesLengthIsZero,
} from '../../../../data/mock-data/graph-schema/MockAttributeDataBatchedNL';

describe('AttributeDataType', () => {
  it('should return true for a valid attributeData object with an entity', () => {
    expect(isAttributeDataEntity(mockAttributeDataNLNode1)).toEqual(true);
  });

  it('should return true for a valid attributeData object with an relation', () => {
    expect(isAttributeDataRelation(mockAttributeDataNLEdge1)).toEqual(true);
  });

  it('should return false if the attributes array is no array', () => {
    expect(isAttributeDataEntity(mockAttributeDataNLNode1AttributeArrayIsNoArray)).toEqual(false);
  });

  it('should return false if the unique-categorical-values array is no array', () => {
    expect(isAttributeDataEntity(mockAttributeDataNLUniqueCategoricalValuesIsNoArray)).toEqual(
      false,
    );
  });

  it('should return false if the unique-categorical-values array has length 0, while having "Categorical" type', () => {
    expect(isAttributeDataEntity(mockAttributeDataNLUniqueCategoricalValuesLengthIsZero)).toEqual(
      false,
    );
  });

  it('should return false if the unique-categorical-values have different types', () => {
    expect(isAttributeDataEntity(mockAttributeDataNLDifferentTypes1)).toEqual(false);
    expect(isAttributeDataEntity(mockAttributeDataNLDifferentTypes2)).toEqual(false);
    expect(isAttributeDataEntity(mockAttributeDataNLDifferentTypes3)).toEqual(false);
  });

  it('should return false if the unique-categorical-values array has an element with an invalid type', () => {
    expect(isAttributeDataEntity(mockAttributeDataNLInvalidType)).toEqual(false);
  });

  it('should return false if the unique-categorical-values array contains an element while the category is not "Categorical', () => {
    expect(isAttributeDataEntity(mockAttributeDataNLInvalidLengthNumerical)).toEqual(false);
  });
});
