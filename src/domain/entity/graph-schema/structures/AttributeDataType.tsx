/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { AttributeData, AttributeFromAttributeData } from './InputDataTypes';

/**
 * Checks if an object has all the properties of an attributeData type. If true, the object will be casted to attributeDataType
 * @param {any} object The object to check if it is an attributeData object.
 * @returns If true, the object is a of type attributeDataType.
 */
export function isAttributeDataEntity(object: any): object is AttributeData {
  if (
    typeof object === 'object' &&
    'id' in object &&
    object.id != undefined &&
    'length' in object &&
    object.length != undefined &&
    'connectedRatio' in object &&
    object.connectedRatio != undefined &&
    'summedNullAmount' in object &&
    object.summedNullAmount != undefined &&
    'attributes' in object &&
    object.attributes != undefined
  ) {
    // Check the structure of a node.
    return (
      typeof object.id == 'string' &&
      typeof object.length == 'number' &&
      typeof object.connectedRatio == 'number' &&
      typeof object.summedNullAmount == 'number' &&
      isAttributeArray(object.attributes)
    );
  }
  return false;
}

/**
 * Checks if an object has all the properties of an attributeData type. If true, the object will be casted to attributeDataType
 * @param {any} object The object to check if it is an attributeData object.
 * @returns If true, the object is a of type attributeDataType.
 */
export function isAttributeDataRelation(object: any): object is AttributeData {
  if (
    typeof object === 'object' &&
    'id' in object &&
    object.id != undefined &&
    'length' in object &&
    object.length != undefined &&
    'fromRatio' in object &&
    object.fromRatio != undefined &&
    'toRatio' in object &&
    object.toRatio != undefined &&
    'summedNullAmount' in object &&
    object.summedNullAmount != undefined &&
    'attributes' in object &&
    object.attributes != undefined
  ) {
    // Check the structure of a edge.
    return (
      typeof object.id == 'string' &&
      typeof object.length == 'number' &&
      typeof object.fromRatio == 'number' &&
      typeof object.toRatio == 'number' &&
      typeof object.summedNullAmount == 'number' &&
      isAttributeArray(object.attributes)
    );
  }
  return false;
}
/**
 * Checks if an object has the correct structure of an attribute from AttributeData.
 * @param {any} object The object to check.
 * @returns If true, the object has the correct structure of an attribute from AttributeData.
 */
function isAttributeArray(object: any): object is AttributeFromAttributeData[] {
  if (!Array.isArray(object)) return false;

  return object.every(
    (attribute: AttributeFromAttributeData) =>
      typeof attribute.name == 'string' &&
      ['Categorical', 'Numerical', 'Other'].includes(attribute.type) &&
      typeof attribute.nullAmount == 'number' &&
      isCorrectUniqueCategoricalValues(attribute),
  );
}

/**
 * Checks whether uniqueCategoricalValues has the correct values.
 * @param attribute The attribute that contains the array (possibly null) with all unique values of the categories.
 * @returns If true, uniqueCategoricalValues is or null, or has an array with values of the same type.
 */
function isCorrectUniqueCategoricalValues(attribute: any): attribute is AttributeFromAttributeData {
  const uniqueCategoricalValues = attribute.uniqueCategoricalValues;

  // Check if uniqueCategoricalValues is an array.
  if (!Array.isArray(uniqueCategoricalValues)) return false;

  // Check if uniqueCategoricalValues.type is Categorical.
  if (attribute.type === 'Categorical') {
    // If true, it has to have a length bigger than 0.
    if (uniqueCategoricalValues.length == 0) return false;

    // All types in the array has to be the same.
    if (typeof uniqueCategoricalValues[0] == 'string') {
      return uniqueCategoricalValues.every((value: any) => typeof value == 'string');
    } else if (typeof uniqueCategoricalValues[0] == 'number') {
      return uniqueCategoricalValues.every((value: any) => typeof value == 'number');
    } else if (typeof uniqueCategoricalValues[0] == 'boolean') {
      return uniqueCategoricalValues.every((value: any) => typeof value == 'boolean');
    } else return false;
  }
  // If the type is Numerical or Other (the two remaining), check if the length of the array is 0.
  else {
    if (uniqueCategoricalValues.length == 0) return true;
    else return false;
  }
}
