/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { AttributeCategory } from './Types';

/** Schema type, consist of nodes and edges */
export type Schema = {
  edges: Edge[];
  nodes: Node[];
};

/** Attribute type, consist of a name */
export type Attribute = {
  name: string;
  type: 'string' | 'int' | 'bool' | 'float';
};

/** Node type, consist of a name and a list of attributes */
export type Node = {
  name: string;
  attributes: Attribute[];
};

/** Edge type, consist of a name, start point, end point and a list of attributes */
export type Edge = {
  name: string;
  to: string;
  from: string;
  collection: string;
  attributes: Attribute[];
};

/** Type of the attribute-data, which could be either of a node (entity) or an edge (relation) */
export type AttributeData = NodeAttributeData | EdgeAttributeData;

/** Type for a node containing all attribute-data */
export type NodeAttributeData = {
  id: string;
  attributes: AttributeFromAttributeData[];
  length: number;
  connectedRatio: number;
  summedNullAmount: number;
};

/** Type for an edge containing all attribute-data */
export type EdgeAttributeData = {
  id: string;
  attributes: AttributeFromAttributeData[];
  length: number;
  fromRatio: number;
  toRatio: number;
  summedNullAmount: number;
};

/** Type for an attribute of a node or an edge, containing attribute-data */
export type AttributeFromAttributeData = {
  name: string;
  type: AttributeCategory;
  nullAmount: number;
  uniqueCategoricalValues: null | string[] | number[] | boolean[];
};
