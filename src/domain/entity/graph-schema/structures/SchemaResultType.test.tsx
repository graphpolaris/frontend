/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { isSchemaResult } from './SchemaResultType';

/** Testsuite to test the schema result checker */
describe('isSchemaResult', () => {
  it('should return true for a valid schema object', () => {
    let schema: any = {
      nodes: [],
      edges: [],
    };
    expect(isSchemaResult(schema)).toBe(true);

    schema.nodes.push({ name: 'hoi', attributes: [{ name: 'attr', type: 'string' }] });
    schema.edges.push({ name: 'edge', from: '1', to: '2', collection: 'flights', attributes: [] });
    expect(isSchemaResult(schema)).toBe(true);

    schema.TEST = 2;
    expect(isSchemaResult(schema)).toBe(true);
  });

  it('should return false for an invalid schema object', () => {
    let schema: any = {
      nodes: [],
      edges: [],
    };
    expect(isSchemaResult(schema)).toBe(true);

    schema.edges = 2;
    expect(isSchemaResult(schema)).toBe(false);

    schema.edges = [{ name: 'test', from: '2', TO: '2', collection: 'flights', attributes: [] }];
    expect(isSchemaResult(schema)).toBe(false);

    schema.edges = [{ name: 'test', from: '2', to: '2', collection: 'flights', attributes: 2 }];
    expect(isSchemaResult(schema)).toBe(false);

    schema.edges = [
      {
        name: 'test',
        from: '2',
        to: '2',
        collection: 'flights',
        attributes: [
          { name: 'attr', type: 'text' },
          { name: 'attr', type: 'ff at 20' },
        ],
      },
    ];
    expect(isSchemaResult(schema)).toBe(false);

    // It should return false for incomplete schema
    let onlyNodes: any = { nodes: [] };
    let onlyEdges: any = { egdes: [] };
    expect(isSchemaResult(onlyNodes)).toEqual(false);
    expect(isSchemaResult(onlyEdges)).toEqual(false);
  });
});
