/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Attribute, Schema } from './InputDataTypes';

/**
 * Checks if an object has all the properties of a schema result. If true, the object will be casted to SchemaResultType
 * @param {any} object The object to check if it is a SchemaResult object.
 * @returns If true, the object is a of type SchemaResultType.
 */
export function isSchemaResult(object: any): object is Schema {
  if (
    typeof object === 'object' &&
    'nodes' in object &&
    object.nodes != undefined &&
    'edges' in object &&
    object.edges != undefined
  ) {
    if (!Array.isArray(object.nodes) || !Array.isArray(object.edges)) return false;

    // Check the structure of all nodes
    const validNodes = object.nodes.every(
      (node: any) => typeof node.name == 'string' && isAttributeArray(node.attributes),
    );

    // Check the structure of all edges
    const validEdges = object.edges.every(
      (edge: any) =>
        typeof edge.name == 'string' &&
        typeof edge.collection == 'string' &&
        typeof edge.from == 'string' &&
        typeof edge.to == 'string' &&
        isAttributeArray(edge.attributes),
    );
    return validNodes && validEdges;
  }
  return false;
}

/**
 * Checks if an object has the structure of a SchemaAttribute.
 * @param {any} object The object to check.
 * @returns If true, the object has the structure of SchemaAttribute type.
 */
function isAttributeArray(object: any): object is Attribute[] {
  if (!Array.isArray(object)) {
    return false;
  }

  return object.every(
    (attribute) =>
      typeof attribute.name == 'string' &&
      ['string', 'int', 'bool', 'float'].includes(attribute.type),
  );
}
