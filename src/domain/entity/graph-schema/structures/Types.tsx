/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Edge, Node } from 'react-flow-renderer';
import { Attribute } from './InputDataTypes';

/**
 * List of schema elements for react flow
 */
export type SchemaElements = {
  nodes: Node[];
  edges: Edge[];
  selfEdges: Edge[];
};

/**
 * Point that has an x and y coordinate
 */
export interface Point {
  x: number;
  y: number;
}

/**
 * Bounding box described by a top left and bottom right coordinate
 */
export interface BoundingBox {
  topLeft: Point;
  bottomRight: Point;
}

/**
 * Typing for the Node Quality data of an entity.
 * It is used for the Node quality analytics and will be displayed in the corresponding popup.
 */
export interface NodeQualityDataForEntities {
  nodeCount: number;
  attributeNullCount: number;
  notConnectedNodeCount: number;

  isAttributeDataIn: boolean; // is true when the data to display has arrived

  // for user interactions
  onClickCloseButton: () => void;
}

/**
 * Typing for the Node Quality data of a relation.
 * It is used for the Node quality analytics and will be displayed in the corresponding popup.
 */
export interface NodeQualityDataForRelations {
  nodeCount: number;
  attributeNullCount: number;
  // from-entity node --relation--> to-entity node
  fromRatio: number; // the ratio of from-entity nodes to nodes that have this relation
  toRatio: number; // the ratio of to-entity nodes to nodes that have this relation

  isAttributeDataIn: boolean; // is true when the data to display has arrived

  // for user interactions
  onClickCloseButton: () => void;
}

/**
 * Typing for the Node Quality popup of an entity or relation node.
 */
export interface NodeQualityPopupNode extends Node {
  data: NodeQualityDataForEntities | NodeQualityDataForRelations;
  nodeID: string; //ID of the node for which the popup is
}

/**
 * Typing for the attribute analytics popup menu data of an entity or relation.
 */
export interface AttributeAnalyticsData {
  nodeType: NodeType;
  nodeID: string;
  attributes: AttributeWithData[];
  // nullAmount: number;

  isAttributeDataIn: boolean; // is true when the data to display has arrived

  // for user interactions
  onClickCloseButton: () => void;
  onClickPlaceInQueryBuilderButton: (name: string, type: string) => void;
  searchForAttributes: (id: string, searchbarValue: string) => void;
  resetAttributeFilters: (id: string) => void;
  applyAttributeFilters: (
    id: string,
    category: AttributeCategory,
    predicate: string,
    percentage: number,
  ) => void;
}

/** All possible options of categories of an attribute */
export enum AttributeCategory {
  categorical = 'Categorical',
  numerical = 'Numerical',
  other = 'Other',
  undefined = 'undefined',
}

/** All possible options of node-types */
export enum NodeType {
  entity = 'entity',
  relation = 'relation',
}

/**
 * Typing for the attribute analytics popup menu of entity or relation nodes
 */
export interface AttributeAnalyticsPopupMenuNode extends Node {
  nodeID: string; //ID of the node for which the popup is
  data: AttributeAnalyticsData;
}

/** Typing of the attributes which are stored in the popup menu's */
export type AttributeWithData = {
  attribute: Attribute;
  category: AttributeCategory;
  nullAmount: number;
};
