/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { LocalStorage } from '../../../data/drivers/LocalStorage';
import DatabaseListener from './DatabaseListener';

/** DatabaseHolder is an observer class for notifying listeners when the database status changes. */
export default class DatabaseHolder {
  private databaseListeners: DatabaseListener[];
  private database: string;

  public constructor() {
    this.databaseListeners = [];
    this.database = '';
  }

  /**
   * Function changes the Database to the new Database.
   * After changing the database, it notifies listeners about changes.
   * @param database This is the new Database.
   */
  public changeDatabase(database: string): void {
    if (this.database != database) {
      this.database = database;
      LocalStorage.instance().cache.currentDatabaseKey = database;
      LocalStorage.instance().SaveToCache();
      this.notifyListeners();
    }
  }

  /**
   * Gets the current database name.
   * @returns {string} The current database name.
   */
  public getDatabase(): string {
    return this.database;
  }

  /**
   * Adds a listener for a database change.
   * @param databaseListener The DatabaseListener to add.
   */
  public addDatabaseListener(databaseListener: DatabaseListener): void {
    this.databaseListeners.push(databaseListener);
  }

  /**
   * Removes a listener from the list of database listeners.
   * @param databaseListener The DatabaseListener to remove.
   */
  public removeDatabaseListener(databaseListener: DatabaseListener): void {
    this.databaseListeners.splice(this.databaseListeners.indexOf(databaseListener), 1);
  }

  /** Notifies all listeners that the database has changed. */
  private notifyListeners(): void {
    this.databaseListeners.forEach((listener) => listener.onDatabaseChanged());
  }
}
