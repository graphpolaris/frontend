/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** The interface for a listener to the Databaseholder observer. */
export default interface DatabaseListener {
  onDatabaseChanged(): void;
}
