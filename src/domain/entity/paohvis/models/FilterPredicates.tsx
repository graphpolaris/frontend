/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

 type TextPredicate = (a1: string, a2: string) => boolean;
 type NumberPredicate = (a1: number, a2: number) => boolean;
 type BoolPredicate = (a1: boolean, a2: boolean) => boolean;
 
 /** Predicates to be used on attributes that are strings. */
 export const textPredicates: Record<string, TextPredicate> = {
   '==': (a1, a2) => a1 == a2,
   '!=': (a1, a2) => a1 != a2,
   contains: (a1, a2) => a1.includes(a2),
   excludes: (a1, a2) => !a1.includes(a2),
 };
 
 /** Predicates to be used on attributes that are numbers. */
 export const numberPredicates: Record<string, NumberPredicate> = {
   '==': (a1, a2) => a1 == a2,
   '!=': (a1, a2) => a1 != a2,
   '>=': (a1, a2) => a1 >= a2,
   '>': (a1, a2) => a1 > a2,
   '<=': (a1, a2) => a1 <= a2,
   '<': (a1, a2) => a1 < a2,
 };
 
 /** Predicates to be used on attributes that are booleans. */
 export const boolPredicates: Record<string, BoolPredicate> = {
   '==': (a1, a2) => a1 == a2,
   '!=': (a1, a2) => a1 != a2,
 };