/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import PaohvisListener from './PaohvisListener';
import PaohvisHolder from './PaohvisHolder';

describe('PaohvisHolder', () => {
  it('should add an remove listeners correctly', () => {
    const holder: PaohvisHolder = new PaohvisHolder();
    const mockOnTableMade1 = jest.fn();
    const mockOnTableMade2 = jest.fn();
    const l1: PaohvisListener = { onTableMade: mockOnTableMade1 };
    const l2: PaohvisListener = { onTableMade: mockOnTableMade2 };
    holder.addListener(l1);
    holder.addListener(l2);

    expect(mockOnTableMade1).toHaveBeenCalledTimes(0);
    expect(mockOnTableMade2).toHaveBeenCalledTimes(0);
    holder.onTableMade('', '', '');
    expect(mockOnTableMade1).toHaveBeenCalledTimes(1);
    expect(mockOnTableMade2).toHaveBeenCalledTimes(1);

    holder.removeListener(l2);
    holder.onTableMade('', '', '');
    expect(mockOnTableMade1).toHaveBeenCalledTimes(2);
    expect(mockOnTableMade2).toHaveBeenCalledTimes(1);
  });
});
