/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import PaohvisListener from './PaohvisListener';

/** PaohvisHolder is an observer class for notifying listeners when a Paohvis table is made. */
export default class PaohvisHolder {
  private paohvisListeners: PaohvisListener[];
  private entityVertical: string;
  private entityHorizontal: string;
  private relationName: string;

  public constructor() {
    this.paohvisListeners = [];
    this.entityVertical = '';
    this.entityHorizontal = '';
    this.relationName = '';
  }

  /**
   * This is called whenever a Paohvis table is made.
   * @param entityVertical that is on the y-axis of the Paohis table.
   * @param entityHorizontal that is on the x-axis of the Paohis table.
   * @param relationName that is used as the hyperedge of the Paohis table.
   */
  public onTableMade(entityVertical: string, entityHorizontal: string, relationName: string): void {
    this.entityVertical = entityVertical;
    this.entityHorizontal = entityHorizontal;
    this.relationName = relationName;

    this.notifyListeners();
  }

  /**
   * Adds a listener to the observer.
   * @param listener The listener that we want to add.
   */
  public addListener(listener: PaohvisListener): void {
    this.paohvisListeners.push(listener);
  }

  /**
   * Removes a listener from the array of listeners.
   * @param listener The listener that we want to remove.
   */
  public removeListener(listener: PaohvisListener): void {
    this.paohvisListeners.splice(this.paohvisListeners.indexOf(listener), 1);
  }

  /** Notifies to all the listeners that a Paohvis table was made. */
  private notifyListeners(): void {
    this.paohvisListeners.forEach((listener) =>
      listener.onTableMade(this.entityVertical, this.entityHorizontal, this.relationName),
    );
  }
}
