/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** Interface for the listener of the PaohvisHolder observer class. */
export default interface PaohvisListener {
  /**
   * This is called whenever a Paohvis table is made.
   * @param entityVertical that is on the y-axis of the Paohis table.
   * @param entityHorizontal that is on the x-axis of the Paohis table.
   * @param relationName that is used as the hyperedge of the Paohis table.
   */
  onTableMade(entityVertical: string, entityHorizontal: string, relationName: string): void;
}
