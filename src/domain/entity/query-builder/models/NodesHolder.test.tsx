/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Node } from 'react-flow-renderer';
import {
  AttributeNode,
  EntityNode,
  QueryElementTypes,
  RelationNode,
  FunctionNode,
} from '../structures/Nodes';
import { mockElements } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import NodesHolder from './NodesHolder';
import SetElementsUseCase from '../../../usecases/query-builder/SetElementsUseCase';
import GetElementsUseCase from '../../../usecases/query-builder/GetElementsUseCase';

/** Testsuite to test the NodeHolder model */
describe('NodesHolder', () => {
  let nodesHolder: NodesHolder;
  let getElementsUseCase: GetElementsUseCase;
  let setElementsUsecase: SetElementsUseCase;
  let entityElements: Node[];
  let relationElements: Node[];
  let attributeElements: Node[];
  let functionElements: Node[];

  beforeEach(() => {
    // Initialize a nodesHolder
    nodesHolder = new NodesHolder(mockElements.nodes);
    getElementsUseCase = new GetElementsUseCase(nodesHolder);
    setElementsUsecase = new SetElementsUseCase(nodesHolder, getElementsUseCase);
    // Collect all the entity, relation and attributeElements from the start elements
    entityElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Entity);
    relationElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Relation);
    attributeElements = mockElements.nodes.filter(
      (node) => node.type == QueryElementTypes.Attribute,
    );
    functionElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Function);
  });
  it('it should correctly add entity nodes', () => {
    //The plus 1 is needed as ID's start at 1 and not at 0
    expect(Number(nodesHolder.getNewID())).toEqual(
      mockElements.nodes.length + mockElements.links.length + 1,
    );
    setElementsUsecase.addNode(mockElements.faultynodes[0]);
    expect(nodesHolder.getMaxIdInElements().toString()).toEqual(mockElements.faultynodes[0].id);
  });
  it('it should correctly add entity nodes', () => {
    setElementsUsecase.RemoveAllElements();
    const entityNode = entityElements[0];
    setElementsUsecase.addNode(entityNode);
    expect(getElementsUseCase.getAllNodes()).toContain(entityNode);
  });
  it('it should correctly add relation nodes', () => {
    setElementsUsecase.RemoveAllElements();
    const relationNode = relationElements[0];
    setElementsUsecase.addNode(relationNode);
    expect(getElementsUseCase.getAllNodes()).toContain(relationNode);
  });
  it('it should correctly add attribute nodes', () => {
    setElementsUsecase.RemoveAllElements();
    const attributeNode = attributeElements[0];
    setElementsUsecase.addNode(attributeNode);
    expect(getElementsUseCase.getAllNodes()).toContain(attributeNode);
  });
  it('it should correctly add function nodes', () => {
    setElementsUsecase.RemoveAllElements();
    const functionNode = functionElements[0];
    setElementsUsecase.addNode(functionNode);
    expect(getElementsUseCase.getAllNodes()).toContain(functionNode);
  });

  it('should hold the same amount of nodes as the amount of nodes given as input', () => {
    expect(entityElements.length).toEqual(
      Object.keys(
        getElementsUseCase.getAllNodes().filter((node) => node.type == QueryElementTypes.Entity),
      ).length,
    );
    expect(relationElements.length).toEqual(
      Object.keys(
        getElementsUseCase.getAllNodes().filter((node) => node.type == QueryElementTypes.Relation),
      ).length,
    );
    expect(attributeElements.length).toEqual(
      Object.keys(
        getElementsUseCase.getAllNodes().filter((node) => node.type == QueryElementTypes.Attribute),
      ).length,
    );
    expect(functionElements.length).toEqual(
      Object.keys(
        getElementsUseCase.getAllNodes().filter((node) => node.type == QueryElementTypes.Function),
      ).length,
    );
  });

  it('should hold the same amount of nodes as the amount of edges given as input', () => {
    expect(mockElements.links.length).toEqual(getElementsUseCase.getAllEdges().length);
  });

  it('should correctly retrieve a node by ID', () => {
    entityElements.forEach((entityElement) => {
      const entityNode = getElementsUseCase.getNodeByID(entityElement.id);
      expect(entityElement).toEqual(entityNode);
    });
    relationElements.forEach((relationElement) => {
      const relationNode = getElementsUseCase.getNodeByID(relationElement.id);
      expect(relationElement).toEqual(relationNode);
    });
    attributeElements.forEach((attributeElement) => {
      const attributeNode = getElementsUseCase.getNodeByID(attributeElement.id);
      expect(attributeElement).toEqual(attributeNode);
    });
    functionElements.forEach((functionElement) => {
      const functionNode = getElementsUseCase.getNodeByID(functionElement.id);
      expect(functionElement).toEqual(functionNode);
    });

    // Also check if we give undefined for a nonexisting node id
    expect(() =>
      getElementsUseCase.getNodeByID('yesyesyesthisnodewillprobablynotexistttttdt'),
    ).toThrowError('NodeID not found in NodesHolder');
  });

  it('should correctly remove an edge', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const funtionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;

    //Testcase for entity attribute edge
    let edge = undefined;
    setElementsUsecase.connectAttribute(attributeNode, entityNode);
    edge = getElementsUseCase.getConnectedEdges(attributeNode)[0];
    if (edge != undefined) {
      expect(getElementsUseCase.getAllEdges()).toContain(edge);
      setElementsUsecase.RemoveEdge(edge);
      expect(getElementsUseCase.getAllEdges()).not.toContain(edge);
    } else fail('connectElementsUsecase.ConnectElements is broken.');

    //Testcase for relationNode attribute edge
    edge = undefined;
    setElementsUsecase.connectAttribute(attributeNode, relationNode);
    edge = getElementsUseCase.getConnectedEdges(attributeNode)[0];
    if (edge != undefined) {
      expect(getElementsUseCase.getAllEdges()).toContain(edge);
      setElementsUsecase.RemoveEdge(edge);
      expect(getElementsUseCase.getAllEdges()).not.toContain(edge);
    } else fail('connectElementsUsecase.ConnectElements is broken.');

    //Testcase for entityNode relationNode edge
    edge = undefined;
    setElementsUsecase.connectEntityToRelation(entityNode, relationNode, false);
    edge = getElementsUseCase.getConnectedEdges(relationNode)[0];
    if (edge != undefined) {
      expect(getElementsUseCase.getAllEdges()).toContain(edge);
      setElementsUsecase.RemoveEdge(edge);
      expect(getElementsUseCase.getAllEdges()).not.toContain(edge);
    } else fail('connectElementsUsecase.ConnectElements is broken.');

    /** TODO add for functionode edges */
  });

  it('should correctly remove an entity node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUsecase.connectEntityToRelation(entityNode, relationNode, false);
    let edge1 = setElementsUsecase.connectAttribute(attributeNode, entityNode);

    // Remove the entityNode
    setElementsUsecase.RemoveNode(entityNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(entityNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);

    /**TODO add an connection to an function node
     * and also check if the correlating edge is removed
     * when the entitynode is removed.
     */
  });
  it('should correctly remove an relation node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUsecase.connectEntityToRelation(entityNode, relationNode, false);
    let edge1 = setElementsUsecase.connectAttribute(attributeNode, relationNode);

    // Remove the entityNode
    setElementsUsecase.RemoveNode(entityNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(entityNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);

    /**TODO add an connection to an function node
     * and also check if the correlating edge is removed
     * when the entitynode is removed.
     */
  });
  it('should correctly remove an attribute node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const entityNode2 = getElementsUseCase.getNodeByID(entityElements[4].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUsecase.connectEntityToRelation(entityNode, relationNode, false);
    let edge1 = setElementsUsecase.connectAttribute(attributeNode, relationNode);
    let edge2 = setElementsUsecase.connectEntityToRelation(entityNode2, relationNode, true);

    // Remove the relation
    setElementsUsecase.RemoveNode(relationNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(relationNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge2);
  });

  it('should correctly remove an attribute node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const attributeNode2 = getElementsUseCase.getNodeByID(attributeElements[1].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUsecase.connectAttribute(attributeNode, entityNode);
    let edge1 = setElementsUsecase.connectAttribute(attributeNode, relationNode);

    // Remove the attributeNode
    setElementsUsecase.RemoveNode(attributeNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(attributeNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    setElementsUsecase.RemoveNode(attributeNode2);
    expect(getElementsUseCase.getAllNodes()).not.toContain(attributeNode2);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);
  });

  /** TODO add for function node removal */
});
