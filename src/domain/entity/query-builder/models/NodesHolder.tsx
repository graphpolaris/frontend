/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { max } from 'd3-v4';
import { Edge, addEdge, removeElements } from 'react-flow-renderer';
import { Handles, isFunctionHandle } from '../structures/Handles';
import { AnyNode, AnyElement } from '../structures/Nodes';
/**
 * Container for reactflow elements.
 * Note that the actual elements use for the ReactFlow component are set in QueryBuilderComponent, hooked to NodesHolder by reactFlowListeners.
 * Creation, deleting and modification of elements should go through SetElementsUseCase.
 * Reading of elements should go trhough GetElementsUseCase.
 */
export default class NodesHolder {
  private elements: AnyElement[];
  public reactFlowListeners: ((elements: AnyElement[]) => void)[];
  public queryListeners: ((elements: AnyElement[]) => void)[];
  private nextID;

  /**
   * Create a new NodesHolder object from a list of starting elements.
   * When you call this, you should probably ensure useCases and the ReactFlow component are updated to use the new NodesHolder.
   * For this, see updateNodesHolder in QueryBuilderViewModelImplementation and QueryBuilderComponent.
   */
  constructor(elements: AnyElement[]) {
    (this.elements = elements),
      (this.reactFlowListeners = []),
      (this.queryListeners = []),
      (this.nextID = this.getMaxIdInElements() + 1); // The first ID is always 1 (We don't start with ID0)
  }

  /** Obtain the highest node-id currently in use, or 0 if there are no elements */
  public getMaxIdInElements(): number {
    if (this.elements.length == 0) return 0;
    let maxVal = max(this.elements.map((el) => Number(el.id)) as number[]);
    if (!maxVal) throw new Error('Cannot calculate max-ID in nodesHolder elements');
    return maxVal;
  }

  /**
   * Hook to notify ReactFlowComponent of visualizable changes to this.elements
   */
  public addReactFlowListener(fn: (elements: AnyElement[]) => void) {
    this.reactFlowListeners.push(fn);
  }
  public notifyReactFlow() {
    this.reactFlowListeners.forEach((fn) => fn(this.elements));
  }

  /**
   * Hook to notify TranslateQueryUseCase of changes to this.elements that result in a new query to be sent.
   */
  public addQueryListener(fn: () => void) {
    this.queryListeners.push(fn);
  }
  private sendQuery() {
    this.queryListeners.forEach((fn) => fn(this.elements));
  }

  /**
   * Obtain a new nodeID value
   */
  public getNewID() {
    const newID = this.nextID.toString();
    this.nextID = this.nextID + 1;
    return newID;
  }

  /**
   * Insert a node into the list of elements.
   */
  public addNode(node: AnyNode) {
    this.elements = this.elements.concat(node); //NOTE: adding of elements needs to be done via concat. Other methods of list insertion break reactFlow.
    this.notifyReactFlow();
    this.sendQuery();
  }

  /**
   * Insert an edge to elements and ensure nodes are updated accordingly.
   * Makes use of ReactFlow helper function.
   */
  public addEdge(edge: Edge) {
    this.elements = addEdge(edge, this.elements);
    this.notifyReactFlow();
    this.sendQuery();
  }
  /**
   * Remove elements from this.elements, and update previously connected elements accordingly.
   * Makes use of ReactFlow helper function.
   */
  public removeElements(elementsToRemove: AnyElement[]) {
    this.elements = removeElements(elementsToRemove, this.elements);
    this.notifyReactFlow();
    this.sendQuery();
  }

  /**
   * Return all elements.
   * Should only be used by GetElementsUseCase. For other instances, use the usecase
   */
  public getNodesHolderElements(): AnyElement[] {
    return this.elements;
  }
}
