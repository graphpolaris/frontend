/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { FunctionTypes } from './FunctionTypes';

/** Interface for what function descriptions need */
export interface FunctionDescription {
  name: string;
  type: FunctionTypes;
  description: string;
}

/** All availiable functions in the function bar. */
export const AvailableFunctions: Record<string, FunctionDescription> = {
  centrality: {
    name: 'centrality',
    type: FunctionTypes.centrality,
    description: 'W.I.P. Shows the importance of nodes',
  },
  communityDetection: {
    name: 'Community Detection',
    type: FunctionTypes.communityDetection,
    description: 'Group entities connected by a relation based on how interconnected they are.',
  },
  groupBy: {
    name: 'Group By',
    type: FunctionTypes.GroupBy,
    description:
      'W.I.P. Per entity of type A, generate aggregate statistics of an attribute of either alllinks of a relation, or all nodes of anentity of type B connected to the type A entity by a relation.',
  },
  link: {
    name: 'Link Prediction',
    type: FunctionTypes.link,
    description:
      'For each pair of entities from a giventype, determine the overlap betweennodes they are connect to by a given relation.',
  },
  shortestPath: {
    name: 'shortestPath',
    type: FunctionTypes.shortestPath,
    description: 'W.I.P. shortest path. Shows the shortest path between nodes',
  },
};
