/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { AnyNode, FunctionArgs } from './Nodes';

/** What functions exist
 *  Default is for the functions in the function bar that don't exist yet.
 */
export enum FunctionTypes {
  GroupBy = 'groupBy',
  link = 'linkPrediction',
  communityDetection = 'communityDetection',
  centrality = 'centrality',
  shortestPath = 'shortestPath',
  default = 'default',
}

export enum FunctionArgTypes {
  group = 'group',
  by = 'by',
  relation = 'relation',
  modifier = 'modifier',
  constraints = 'constraints',
  result = 'result',
  ID1 = 'ID1',
  ID2 = 'ID2',
}
/** All arguments that groupby pill needs */
export const DefaultGroupByArgs: FunctionArgs = {
  group: { displayName: 'Group', connectable: true, value: '', visible: true },
  by: { displayName: 'By', connectable: true, value: '_id', visible: true },
  relation: { displayName: 'On', connectable: true, value: undefined, visible: true },
  modifier: { displayName: 'Modifier: ', connectable: false, value: '', visible: true },
  constraints: { displayName: 'Constraints: ', connectable: true, value: undefined, visible: true },
  result: { displayName: 'Result: ', connectable: true, value: undefined, visible: true },
};
/** All arguments that linkprediction pill needs */
export const DefaultLinkPredictionArgs: FunctionArgs = {
  linkprediction: {
    //currently the querybuilder shows this name instead of the display name that needs to be changed.
    displayName: 'linkprediction',
    connectable: false,
    value: undefined,
    visible: true,
  },
};

/** All arguments that CommunictyDetection pill needs */
export const DefaultCommunictyDetectionArgs: FunctionArgs = {
  CommunityDetection: {
    displayName: 'CommunityDetection',
    connectable: false,
    value: undefined,
    visible: true,
  },
};

/** All arguments that centrality pill needs */
export const DefaultCentralityArgs: FunctionArgs = {
  centrality: {
    displayName: 'centrality',
    connectable: false,
    value: undefined,
    visible: true,
  },
};

/** All arguments that centrality pill needs */
export const DefaultShortestPathArgs: FunctionArgs = {
  shortestPath: {
    displayName: 'shortestPath',
    connectable: false,
    value: undefined,
    visible: true,
  },
};

// TODO: fix this to somehow make use of the enum
/** Returns the correct arguments depending on the type */
export const DefaultFunctionArgs: { [type: string]: FunctionArgs } = {
  groupBy: DefaultGroupByArgs,
  linkPrediction: DefaultLinkPredictionArgs,
  communityDetection: DefaultCommunictyDetectionArgs,
  centrality: DefaultCentralityArgs,
  shortestPath: DefaultShortestPathArgs,
};
