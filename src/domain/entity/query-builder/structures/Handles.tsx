/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/**
 * Enums for possible values for handles of nodes in the query builder.
 * Possible handles for an entity node.
 */
import { FunctionArgTypes } from './FunctionTypes';
import { AnyNode, QueryElementTypes } from './Nodes';

/** Links need handles to what they are connected to (and which side) */
export enum Handles {
  RelationLeft = 'leftEntityHandle', //target
  RelationRight = 'rightEntityHandle', //target
  ToAttributeHandle = 'attributesHandle', //target
  ToRelation = 'relationsHandle', //source
  OnAttribute = 'onAttributeHandle', //source
  ReceiveFunction = 'receiveFunctionHandle', //target
  FunctionBase = 'functionHandle_', // + name from FunctionTypes args //source
}

/** returns a boolean that check wheter the handle is a function handle */
export function isFunctionHandle(handle: string): boolean {
  return handle.startsWith(Handles.FunctionBase);
}

/**
 *  returns the functionargumenttype
 *  Currently only working for groupby but made that in the future other functiones can use this aswell.
 */
export function functionHandleToType(handle: string): FunctionArgTypes {
  if (isFunctionHandle(handle))
    return handle.slice(Handles.FunctionBase.length) as FunctionArgTypes;
  else throw new Error('Incorrectly trying to assert handle to functionhandle');
}
/** Creates a handle from a functiontype */
export function typeToFunctionHandle(type: FunctionArgTypes): string {
  return Handles.FunctionBase + type;
}

/**
 * Return a list of handles to which a connection can be made by dragging a node nearby
 */
export function nodeToHandlesThatCanReceiveDragconnect(node: AnyNode): string[] {
  switch (node.type) {
    case QueryElementTypes.Entity:
      return [Handles.ToAttributeHandle];
    case QueryElementTypes.Relation:
      return [Handles.RelationLeft, Handles.RelationRight, Handles.ToAttributeHandle];
    case QueryElementTypes.Function:
      return [Handles.ToAttributeHandle];
    case QueryElementTypes.Attribute:
      return [];
    default:
      throw new Error('Unsupported node');
  }
}

/**
 * Return a list of handles from which a connection can be made while dragging the node they are on
 */
export function nodeToHandlesThatCanSendDragconnect(node: AnyNode): string[] {
  switch (node.type) {
    case QueryElementTypes.Entity:
      return [Handles.ToRelation];
    case QueryElementTypes.Relation:
      return [];
    case QueryElementTypes.Function:
      return [];
    case QueryElementTypes.Attribute:
      return [Handles.OnAttribute];
    default:
      throw new Error('Unsupported node');
  }
}
