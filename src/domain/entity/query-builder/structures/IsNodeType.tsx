/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  AnyNode,
  AttributeNode,
  EntityNode,
  FunctionNode,
  QueryElementTypes,
  RelationNode,
} from './Nodes';

/**
 * Checks if a node is an entityNode.
 * @param {AnyNode} node The node that has to checked.
 * @returns True and casts if the node is an EntityNode.
 */
export function isEntityNode(node: AnyNode): node is EntityNode {
  return node.type == QueryElementTypes.Entity;
}

/**
 * Checks if a node is a RelationNode.
 * @param {AnyNode} node The node that has to checked.
 * @returns True and casts if the node is an RelationNode.
 */
export function isRelationNode(node: AnyNode): node is RelationNode {
  return node.type == QueryElementTypes.Relation;
}

/**
 * Checks if a node is an AttributeNode.
 * @param {AnyNode} node The node that has to checked.
 * @returns True and casts if the node is an AttributeNode.
 */
export function isAttributeNode(node: AnyNode): node is AttributeNode {
  return node.type == QueryElementTypes.Attribute;
}

/**
 * Checks if a node is an FunctionNode.
 * @param {AnyNode} node The node that has to checked.
 * @returns True and casts if the node is an FunctionNode.
 */
export function isFunctionNode(node: AnyNode): node is FunctionNode {
  return node.type == QueryElementTypes.Function;
}