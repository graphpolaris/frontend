/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** Interface for x and y position of node */
export interface NodePosition {
  x: number;
  y: number;
}

/** Returns from-position of relation node */
export function RelationPosToFromEntityPos(position: NodePosition): NodePosition {
  return { x: position.x - 60, y: position.y - 40 };
}

/** Returns to-position of relation node */
export function RelationPosToToEntityPos(position: NodePosition): NodePosition {
  return { x: position.x + 400, y: position.y };
}

/** Default position; x=0, y=0 */
export const DefaultPosition = { x: 0, y: 0 };
