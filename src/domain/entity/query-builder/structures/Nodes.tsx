/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Edge as ReactEdge, Node as ReactNode } from 'react-flow-renderer';

/** Enums for the possible types of query elements */
export enum QueryElementTypes {
  Entity = 'entity',
  Relation = 'relation',
  Attribute = 'attribute',
  Function = 'function',
}

/** List of possible query element types */
export const possibleTypes: string[] = [
  QueryElementTypes.Entity,
  QueryElementTypes.Relation,
  QueryElementTypes.Attribute,
  QueryElementTypes.Function,
];

/** All the possible react flow nodes. */
export type AnyNode = EntityNode | RelationNode | AttributeNode | FunctionNode;
export type Edge = ReactEdge<any>;
export type AnyElement = AnyNode | Edge;
export type AnyNodeData = EntityData | RelationData | AttributeData | FunctionData;

export type EntityNode = ReactNode<EntityData>;
export type RelationNode = ReactNode<RelationData>;
export type AttributeNode = ReactNode<AttributeData>;
export type FunctionNode = ReactNode<FunctionData>;

export interface NodeData {
  fadeIn: boolean;
}

/** Interface for the data in an entity node. */
export interface EntityData extends NodeData {
  name: string;
}

/** Interface for the data in an relation node. */
export interface RelationData extends NodeData {
  name: string;
  collection: string;
  depth: { min: number; max: number };
}

/** Interface for the data in an attribute node.
 * Can have multiple constraint datatypes.
 * string MatchTypes: exact/contains/startswith/endswith.
 * int    MatchTypes: GT/LT/EQ.
 * bool   MatchTypes: EQ/NEQ.
 */
export interface AttributeData extends NodeData {
  attribute: string;
  value: string;
  dataType: string;
  matchType: string;
}

export interface FunctionArg {
  displayName: string;
  connectable: boolean; // does this arg have a connectable handle?
  value: string | undefined; // undefined means no text input
  visible: boolean; // for implicit connections
}

export interface FunctionArgs {
  [name: string]: FunctionArg;
}

/** Interface for the data in a function node. */
export interface FunctionData extends NodeData {
  functionType: string;
  args: FunctionArgs;
}

/** Interface for updating the edges. */
export interface updateEdges {
  newEdge: Edge | undefined;
  removeEdge: Edge | undefined;
}
