/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** JSON query format used to send a query to the backend. */
export interface TranslatedJSONQuery {
  return: {
    entities: number[];
    relations: number[];
    groupBys: number[];
  };
  entities: Entity[];
  relations: Relation[];
  groupBys: GroupBy[];
  machineLearning: MachineLearning[];
  limit: number;
}

/** Interface for an entity in the JSON for the query. */
export interface Entity {
  name: string;
  ID: number;
  constraints: Constraint[];
}

/** Interface for an relation in the JSON for the query. */
export interface Relation {
  name: string;
  ID: number;
  fromType: string;
  fromID: number;
  toType: string;
  toID: number;
  depth: { min: number; max: number };
  constraints: Constraint[];
}

/**
 * Constraint datatypes created from the attributes of a relation or entity.
 *
 * string MatchTypes: exact/contains/startswith/endswith.
 * int    MatchTypes: GT/LT/EQ.
 * bool   MatchTypes: EQ/NEQ.
 */
export interface Constraint {
  attribute: string;
  dataType: string;

  matchType: string;
  value: string;
}

/** Interface for a function in the JSON for the query. */
export interface GroupBy {
  ID: number;

  groupType: string;
  groupID: number[];
  groupAttribute: string;

  byType: string;
  byID: number[];
  byAttribute: string;

  appliedModifier: string;
  relationID: number;
  constraints: Constraint[];
}

/** Interface for Machine Learning algorithm */
export interface MachineLearning {
  ID?: number;
  queuename: string;
  parameters: string[];
}
/** Interface for what the JSON needs for link predicition */
export interface LinkPrediction {
  queuename: string;
  parameters: {
    key: string;
    value: string;
  }[];
}
