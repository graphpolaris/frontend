/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { isNodeLinkResult } from './NodeLinkResultType';

/** Testsuite to test the node-link result data checker */
describe('isNodeLinkResult', () => {
  it('should return true for valid nodelink data', () => {
    const data: any = {
      nodes: [],
      edges: [],
    };

    expect(isNodeLinkResult(data)).toBe(true);

    data.nodes = [{ id: '123', attributes: { date: 12000 } }];
    expect(isNodeLinkResult(data)).toBe(true);

    data.edges = [{ from: '10', to: '9' }];
    expect(isNodeLinkResult(data)).toBe(true);

    data.hey = 4;
    expect(isNodeLinkResult(data)).toBe(true);
  });

  it('should return false for valid nodelink data', () => {
    let data: any = {
      nodes: [{ hoi: '10' }],
      edges: [],
    };

    expect(isNodeLinkResult(data)).toBe(false);

    data.nodes = [];
    data.edges = [{ From: '10', to: '9' }];
    expect(isNodeLinkResult(data)).toBe(false);

    data.edges = null;
    expect(isNodeLinkResult(data)).toBe(false);

    data.edges = 1;
    expect(isNodeLinkResult(data)).toBe(false);

    data = {
      nodes: [],
      Edges: [],
    };
    expect(isNodeLinkResult(data)).toBe(false);
  });
});
