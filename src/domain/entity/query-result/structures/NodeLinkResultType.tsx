/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
/** A node link data-type for a query result object from the backend. */
export type NodeLinkResultType = {
  nodes: Node[];
  edges: Link[];
  mlEdges?: Link[];
};

/** Typing for nodes and links in the node-link result. Nodes and links should always have an id and attributes. */
export interface AxisType {
  id: string;
  attributes: Record<string, any>;
  mldata?: Record<string, string[]> | number; // This is shortest path data . This name is needs to be changed together with backend TODO: Change this.
}

/** Typing for a node in the node-link result */
export type Node = AxisType;

/** Typing for a link in the node-link result */
export interface Link extends AxisType {
  from: string;
  to: string;
}

/** Gets the group to which the node/edge belongs */
export function getGroupName(axisType: AxisType): string {
  return axisType.id.split('/')[0];
}

/** Returns true if the given id belongs to the target group. */
export function isNotInGroup(nodeOrEdge: AxisType, targetGroup: string): boolean {
  return getGroupName(nodeOrEdge) != targetGroup;
}

/** Checks if a query result form the backend contains valid NodeLinkResultType data.
 * @param {any} jsonObject The query result object received from the frontend.
 * @returns True and the jsonObject will be casted, false if the jsonObject did not contain all the data fields.
 */
export function isNodeLinkResult(jsonObject: any): jsonObject is NodeLinkResultType {
  if (
    typeof jsonObject === 'object' &&
    jsonObject !== null &&
    'nodes' in jsonObject &&
    'edges' in jsonObject
  ) {
    if (!Array.isArray(jsonObject.nodes) || !Array.isArray(jsonObject.edges)) return false;

    const validNodes = jsonObject.nodes.every((node: any) => 'id' in node && 'attributes' in node);
    const validEdges = jsonObject.edges.every((edge: any) => 'from' in edge && 'to' in edge);

    return validNodes && validEdges;
  } else return false;
}

/** Returns a record with a type of the nodes as key and a number that represents how many times this type is present in the nodeLinkResult as value. */
export function getNodeTypes(nodeLinkResult: NodeLinkResultType): Record<string, number> {
  const types: Record<string, number> = {};

  nodeLinkResult.nodes.forEach((node) => {
    const type = getGroupName(node);
    if (types[type] != undefined) types[type]++;
    else types[type] = 0;
  });

  return types;
}
