/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
/** This file contains the type for a query status update message from the backend.
 * And a function to check if an object is of this type.
 */

/** The query status update message format received from the backend. */
export type QueryStatusUpdateMessage = {
  queryID: string;
  status: string;
};

/**
 * Checks if an object is of type `QueryStatusUpdateMessage`.
 * @param message An object with unknown data type to check against.
 * @returns True and casts the message to a `QueryStatusUpdateMessage` if the message has all the fields.
 */
export function isQueryStatusUpdateMessage(message: any): message is QueryStatusUpdateMessage {
  return (
    typeof message === 'object' &&
    message !== null &&
    'queryID' in message &&
    typeof message.queryID == 'string' &&
    'status' in message &&
    typeof message.status == 'string'
  );
}
