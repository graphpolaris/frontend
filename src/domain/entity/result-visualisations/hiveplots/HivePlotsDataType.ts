//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */
import { id } from '../../../../presentation/view/result-visualisations/hiveplots/HivePlotsComponent';
import { Link } from '../../query-result/structures/NodeLinkResultType';
import { Vector } from '../../utils/Position';

export type HivePlotData = {
  axis: HPAxisData[];
  links: HPLinksFromToAxisData;

  // Used for normalizing the opacity of nodes or links.
  maxNodesAtSamePos: number;
  maxLinksAtSamePos: number; // (max # of links that are exactly the same, e.g. same source and target pos)

  /** The links to highlight for this plot, determined by which nodes the mouse is hovering on.
   * Usage: highLightedNodes[1][20] means that node on axis 1 in bin 20, is being highlighted.
   */
  highLightedNodes: { [key: number]: { [key: number]: boolean } };
};
export type HPAxisData = {
  bins: HPNodeData[];
  idsToValues: Record<id, number>;
  min: number;
  max: number;
  startPos: Vector;
  length: number;
};

/** Values with node id array. Makes binning possible.
 * Map guarantees the keys to be iterated in order of insertion. */
export type HPNodeData = { ids: id[]; pos: Vector };
export type HPLinkData = { fromPos: Vector; toPos: Vector; resultLinks: Link[] };
/** A dictionary that stores links between "from" and "to" axis.
 * These links are then devided per "from bin" and "to bin".
 * `HPLinksFromToAxisData[0][1][20][4]` gives an array of links from axis 0 bin 20, to axis 1 bin 4.
 */
export type HPLinksFromToAxisData = { [key: number]: { [key: number]: HPLinksFromToBinsData } };
export type HPLinksFromToBinsData = { [key: number]: { [key: number]: LinksData } };
export type LinksData = Link[];
