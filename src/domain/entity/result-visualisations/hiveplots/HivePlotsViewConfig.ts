//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */

/** This file contains type declerations for config file of a hiveplots result visualisation. */

/** The possible values to order nodes on their axis. */
export const possibleOrderByValues = ['Degree', 'Closeness', 'Clustering coefficient', 'id'];

// TODO: generate Type entry for this object using schema/query data, which entities are available
/** Possible types to assign nodes by and their corresponding possible values. */
export const possibleAxisAssignments: Record<possibleAssignByValues, string[]> = {
  Connections: ['Target', 'Source', 'Both', 'Other'],
  Type: [],
  // Betweeness: [],
};

export type possibleAssignByValues = 'Connections' | 'Type';

/** The view configurations for hiveplots. */
export type HivePlotsViewConfig = {
  /** The current selected plot in the configuration panel to the right. */
  selectedPlotInConfigPanelIndex: number;

  plots: PlotViewConfig[];
};

/** The configuration fields for one hiveplot. */
export type PlotViewConfig = {
  name: string;
  /** Used for assigning nodes to axis. */
  assignAxisBy: possibleAssignByValues;
  /** An array of axis, where each axis hold a value for assigning nodes. */
  axis: AxisViewConfig[];

  maxNOfBins: number;
};

/** The configuration fields for one axis, used to assign nodes to this axis and order them. */
export type AxisViewConfig = {
  assignmentValue: string;
  orderNodesBy: string;
};
