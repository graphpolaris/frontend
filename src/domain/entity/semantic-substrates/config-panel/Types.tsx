/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import SemanticSubstratesViewModel from '../../../../presentation/view-model/result-visualisations/semantic-substrates/SemanticSubstratesViewModel';
import { NodeLinkResultType } from '../../query-result/structures/NodeLinkResultType';

/* An entity that has an attribute (Either a node with attributes or an edges with attributes)
  For the config-panel of semantic-substrates.*/
export type EntityWithAttributes = {
  name: string;
  attributes: string[];
  group: number;
};

/** Props for this component */
export type FSSConfigPanelProps = {
  graph: NodeLinkResultType;
  // currentColours: any;
  fssViewModel: SemanticSubstratesViewModel;
};
