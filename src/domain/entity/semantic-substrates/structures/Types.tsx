/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Position } from '../../utils/Position';

/** Stores the minimum and maximum of the data. */
export type MinMaxType = {
  min: number;
  max: number;
};

/** The data of a node. */
export type NodeData = {
  text: string;
};

/** The from and to index of a relation. */
export type RelationType = {
  fromIndex: number;
  toIndex: number;
  value?: number;
  colour?: string;
};

/** Used as input for calculating the min max values and scale the position. */
export type PlotInputData = {
  title: string;
  nodes: InputNodeType[];
  width: number;
  height: number;
};

/** The nodes for inputdata for plot generation. */
export interface InputNodeType {
  id: string;
  data: NodeData;
  originalPosition: Position;
  attributes: Record<string, any>;
}

/** The node type for nodes in plots. */
export interface NodeType extends InputNodeType {
  scaledPosition: Position;
}

/** Used for semantic substrate components as input. */
export type PlotType = {
  title: string;
  nodes: NodeType[];
  minmaxXAxis: { min: number; max: number };
  minmaxYAxis: { min: number; max: number };
  selectedAttributeNumerical: string;
  selectedAttributeCatecorigal: string;
  scaleCalculation: (x: number) => number;
  colourCalculation: (x: string) => string;
  width: number;
  height: number;
  yOffset: number;

  //Used for autocompleting the attribute value in a plot title/filter
  possibleTitleAttributeValues: string[];
};

/** The entities with names and attribute parameters from the schema. */
export type EntitiesFromSchema = {
  entityNames: string[];
  attributesPerEntity: Record<string, AttributeNames>;
};

/** The names of attributes per datatype. */
export type AttributeNames = {
  textAttributeNames: string[];
  numberAttributeNames: string[];
  boolAttributeNames: string[];
};

/** The specifications for filtering nodes and scaling a plot. */
export type PlotSpecifications = {
  entity: string;

  labelAttributeType: string;
  labelAttributeValue: string;

  xAxis: AxisLabel;
  yAxis: AxisLabel;

  // If the axisPositioning is attributeType, this will be used to index the attributes of a node
  xAxisAttributeType: string;
  yAxisAttributeType: string;

  width: number;
  height: number;
};

/** The default possible options for an axis label. */
export enum AxisLabel {
  evenlySpaced = 'evenly spaced',
  outboundConnections = '# outbound connections',
  inboundConnections = '# inbound connections',
  byAttribute = 'byAttribute',
}
