/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** An x y position. TODO: use vector instead of Position */
export type Position = {
  x: number;
  y: number;
};

/** A 2D vector. */
export type Vector = {
  x: number;
  y: number;
};

/** Returns the normalized vector. */
export function normalizeVector(vector: Vector): Vector {
  const magnitude = calculateMagnitudeVector(vector);
  return { x: vector.x / magnitude, y: vector.y / magnitude };
}

/** Calculates the magnitude of the vector. */
export function calculateMagnitudeVector(vector: Vector): number {
  return Math.sqrt(vector.x * vector.x + vector.y * vector.y);
}

/** Returns the vector with given magnitude. */
export function setMagnitudeVector(vector: Vector, magnitude: number): Vector {
  const currentMagnitude = calculateMagnitudeVector(vector);
  vector.x = (vector.x / currentMagnitude) * magnitude;
  vector.y = (vector.y / currentMagnitude) * magnitude;
  return vector;
}

/** Returns the addition of the vector with it's normalized self multiplied by the magnitude. */
export function addMagnitudeVector(vector: Vector, magnitude: number): Vector {
  const normalizedVector = normalizeVector(vector);
  return addingVectors(vector, multiplyVectorByFactor(normalizedVector, magnitude));
}

/** Returns vector with the components of the vector multiplied by the multiplication factor. */
export function multiplyVectorByFactor(vector: Vector, multiplicationFactor: number): Vector {
  vector.x *= multiplicationFactor;
  vector.y *= multiplicationFactor;
  return vector;
}

/** Returns the vector which is the ddition of the given vectors. */
export function addingVectors(vectorA: Vector, vectorB: Vector): Vector {
  return { x: vectorA.x + vectorB.x, y: vectorA.y + vectorB.y };
}

/** Returns the dotproduct of two vectors. */
export function dotProductVectors(vectorA: Vector, vectorB: Vector): number {
  return vectorA.x * vectorB.x + vectorA.y * vectorB.y;
}

/** Reutrns the crossproduct of two vectors. */
export function crossProductVectors(vectorA: Vector, vectorB: Vector): number {
  return vectorA.x * vectorB.y - vectorA.y * vectorB.x;
}

/** Returns the vector rotated by an angle. */
export function rotateVectorByAngle(vector: Vector, angle: number): Vector {
  return { x: vector.x * Math.cos(angle) - vector.y * Math.sin(angle), y: vector.x * Math.sin(angle) + vector.y * Math.cos(angle) };
}

/** Returns a copy of the vector. */
export function copyVector(vector: Vector): Vector {
  return { x: vector.x, y: vector.y };
}

/** Returns the x and y position of the vector. */
export function vectorToString(vector: Vector) {
  return vector.x + ' ' + vector.y;
}
