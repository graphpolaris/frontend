/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** Properties of the PanelsViewModel */
export type PanelsViewModelProperties = {
  classes: any;
  navBarHeight: any;
  schemaDrawerHeight: any;
  showMeDrawerHeight: any;
  queryDrawerHeight: any;

  showMeDrawerWidth: any;
  schemaDrawerWidth: any;
  queryDrawerWidth: any;
};
