/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import AuthorizationResult from '../entity/auth/structures/AuthorizationResult';

/** Interface for an authorization api repository. */
export default interface AuthorizeRepository {
  login(): Promise<AuthorizationResult>;

  /** Tries to log in the user, only succeeds if there is a clientID cookie. */
  loginWithCookie(): Promise<AuthorizationResult>;
}
