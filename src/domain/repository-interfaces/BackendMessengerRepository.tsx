/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** Interface for sending messages to the backend. */
export default interface BackendMessengerRepository {
  /** throws {Error} if credentials have not passed. */
  SendMessage(body: string, request: string, method: string): Promise<Response>;

  SendRequest(request: string): Promise<Response>;
}
