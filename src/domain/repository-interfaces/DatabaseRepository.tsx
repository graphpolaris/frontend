/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** Contains interface for adding user databases and fetching the connected databases */
export type Database = {
  // The user specified database, can be anything
  databaseName: string;
  username: string;
  password: string;
  port: number;
  hostname: string;
  // The database for the internal database
  internalDatabaseName: string;
  // The database type vendor, currently only arangodb and neo4j are supported
  databaseType: string;
  // Password for speficied databasehost
};

// Interface for a database api repository
export default interface DatabaseRepository {
  /**
   * Adds a database for the currently logged in user.
   * @param database {Database} The database to add for the user.
   * @returns A promise with a standard Response.
   */
  addDatabase(database: Database): Promise<Response>;

  /**
   * Fetches the currently connected database of the user.
   * @returns The database names.
   */
  fetchUserDatabases(): Promise<string[]>;
}
