/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import LocalStorageRepository from './LocalStorageRepository';
import { defaultCache } from './StorageRepository';

describe('LocalStorageRepository', () => {
  beforeEach(() => {
    localStorageRepository = new LocalStorageRepository();
  });
  let localStorageRepository: LocalStorageRepository;

  /** Testing saving and loading */
  it('Save and load should return the same value', () => {
    localStorageRepository.cache.queryListOpen = true;
    localStorageRepository.cache.currentColours.index = 1;
    localStorageRepository.cache.currentColours.key = 'dark';
    localStorageRepository.SaveToCache();
    localStorageRepository.cache.queryListOpen = false; //To test wheter save actually works.
    localStorageRepository.cache.currentColours.index = 0; //To test wheter save actually works.
    localStorageRepository.cache.currentColours.key = 'default'; //To test wheter save actually works.

    expect(localStorageRepository.LoadCache().queryListOpen).toEqual(true);
    expect(localStorageRepository.LoadCache().currentColours.index).toEqual(1);
    expect(localStorageRepository.LoadCache().currentColours.key).toEqual('dark');
  });

  /** Testing the compare key function */
  it('Should return false arent the same', () => {
    const FakeCache1 = {
      fakeValue1: true,
      fakeValue2: true,
    };
    const FakeCache2 = {
      fakeValue1: true,
    };
    expect(localStorageRepository.compareObjectKeys(FakeCache1, FakeCache2)).toEqual(false);
  });

  it('Should return true are the same', () => {
    const FakeCache1 = {
      fakeValue1: true,
    };
    const FakeCache2 = {
      fakeValue1: true,
    };
    expect(localStorageRepository.compareObjectKeys(FakeCache1, FakeCache2)).toEqual(true);
  });

  it('Should return true when the keys are the same even when nested', () => {
    const FakeCache1 = {
      testValue1: {
        testValue1: true,
      },
    };
    const FakeCache2 = {
      testValue1: {
        testValue1: false,
      },
    };
    expect(localStorageRepository.compareObjectKeys(FakeCache1, FakeCache2)).toEqual(true);
  });
  it('Should return false when the keys arent the same when nested', () => {
    const FakeCache1 = {
      testValue1: {
        testValue1: true,
        testValue2: true,
      },
    };
    const FakeCache2 = {
      testValue1: {
        testValue1: false,
      },
    };
    expect(localStorageRepository.compareObjectKeys(FakeCache1, FakeCache2)).toEqual(false);
  });

  it('Should be the default values when resetting', () => {
    localStorageRepository.cache.queryListOpen = true;
    localStorageRepository.cache.currentColours.index = 1;
    localStorageRepository.cache.currentColours.key = 'dark';
    localStorageRepository.SaveToCache();
    localStorageRepository.Reset();
    expect(localStorageRepository.cache).toEqual(defaultCache);
  });
});
