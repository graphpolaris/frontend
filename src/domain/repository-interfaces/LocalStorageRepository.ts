/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import StorageRepository from './StorageRepository';
import { Cache, defaultCache } from './StorageRepository';

export default class LocalStorageRepository implements StorageRepository {
  public cache: Cache;

  constructor() {
    this.cache = this.LoadCache();
    this.SaveToCache();
  }

  /** Saving the cache to storage */
  public SaveToCache(): void {
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.cache));
  }

  protected readonly localStorageKey = 'configuration';

  /** Loading cache from storage */
  LoadCache(): Cache {
    // if there is an up to date storage available, use that
    // otherwise, use default configuration

    const currentStorage = localStorage.getItem(this.localStorageKey);
    if (currentStorage !== null) {
      const configInStorage = JSON.parse(currentStorage);
      // Checks up to infinite deep whether the keys are the same and if not return default.
      if (this.compareObjectKeys(defaultCache, configInStorage)) {
        return configInStorage as Cache;
      }
      return defaultCache;
    } else return defaultCache;
  }

  /** Go back to default values. */
  public Reset(): void {
    console.log('reseting to default values');
    this.cache = defaultCache;
    localStorage.clear();
  }

  /** Used for testing wheter all keys in the program are in the savefile */
  compareObjectKeys(valueA: any, valueB: any): boolean {
    const keysA = Object.keys(valueA);
    return keysA.every((key) => {
      if (valueB[key] == undefined) return false;
      if (this.isObject(valueA[key])) {
        return this.compareObjectKeys(valueA[key], valueB[key]);
      }
      return true;
    });
  }
  /** Checks wheter something is an object. */
  isObject(value: any) {
    return typeof value === 'object' && !Array.isArray(value) && value !== null;
  }
}
