/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { TranslatedJSONQuery } from '../entity/query-builder/structures/TranslatedJSONQuery';

/** A type for a query to send to the backend. */
export interface Query extends TranslatedJSONQuery {
  databaseName: string;
}

export default interface QueryRepository {
  /**
   * Sends a query to the database to execute.
   * @param query {Query} The query to execute.
   * @returns A promise with the assigned queryID.
   */
  sendQuery(query: Query): Promise<string>;

  /**
   * Sends a message to the backend to retrieve a cached query
   * @param queryID {string} The id of the query to retrieve.
   */
  retrieveCachedQuery(queryID: string): Promise<Response>;
}
