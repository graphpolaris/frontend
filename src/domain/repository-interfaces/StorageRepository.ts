/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { QueryStatusListItem } from '../../presentation/view-model/query-status-list/QueryStatusListViewModelImpl';
import { AnyNode } from '../entity/query-builder/structures/Nodes';
import { Edge } from 'react-flow-renderer';

/** Cache type */
export type Cache = {
  queryListOpen: boolean;
  queryStatusList: {
    queries: Record<string, QueryStatusListItem>;
    queryIDsOrder: string[];
  };
  functionsMenuOpen: boolean;
  currentDatabaseKey: string;
  currentColours: { key: string; index: number };
  elementsperDatabaseObject: Record<string, (AnyNode | Edge<any>)[]>;
  autoSendQueries: boolean;
  panelWidthHeights: {
    windowinnerHeight: number;
    windowinnerWidth: number;
    navBarHeight: number;
    schemaDrawerHeight: number;
    queryDrawerHeight: number;
    schemaDrawerWidth: number;
    queryDrawerWidth: number;
  };
};

/** Interface for StorageRepository */
export default interface StorageRepository {
  cache: Cache;

  SaveToCache(): void; // Saving the state to storage
  LoadCache(): Cache; // Loading state from storage
  Reset(): void; // Go back to default values.
}

/** Const defaultCache of Cache type */
export const defaultCache: Cache = {
  queryListOpen: false,
  queryStatusList: { queries: {}, queryIDsOrder: [] },
  functionsMenuOpen: false,
  currentDatabaseKey: '',
  currentColours: { key: 'default', index: 0 },
  elementsperDatabaseObject: {},
  autoSendQueries: true,

  /*
   *You can't calculate values using other values here,
   *these default values are calculated in the constructor of PanelViewModelImpl where this is used
   *-10 & 50 are just place holder values.
   */
  panelWidthHeights: {
    windowinnerHeight: -10,
    windowinnerWidth: -10,
    navBarHeight: 50,
    schemaDrawerHeight: 50,
    queryDrawerHeight: 50,
    schemaDrawerWidth: 50,
    queryDrawerWidth: 50,
  },
};
