/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import ParseToUniqueEdges from './ParseToUniqueEdgesUseCase';
import MockQueryResult from '../../data/mock-data/query-result/bigMockQueryResults';

/** Testsuite for the parse to unique edges use case. */
describe('ParseToUniqueEdges', () => {
  it('should only keep unique edges', () => {
    const received = ParseToUniqueEdges.parse(MockQueryResult.edges, false);

    expect(new Set(received).size).toBe(received.length);
  });

  it('should only contain edges from the input', () => {
    const unique = ParseToUniqueEdges.parse(MockQueryResult.edges, false);

    const counts: number[] = new Array(unique.length).fill(0);
    MockQueryResult.edges.forEach((edge) => {
      for (let i = 0; i < unique.length; i++) {
        if (edge.from == unique[i].from && edge.to == unique[i].to) {
          counts[i]++;
          break;
        }
      }
    });

    unique.forEach((edge, i) => {
      expect(edge.count).toBe(counts[i]);
    });
  });
});
