/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Link } from '../entity/query-result/structures/NodeLinkResultType';

export type UniqueEdge = {
  from: string;
  to: string;
  count: number;
  attributes: Record<string, any>;
};

export default class ParseToUniqueEdges {
  /**
   * Parse a message (containing query result) edges to unique edges.
   * @param {Link[]} queryResultEdges Edges from a query result.
   * @param {boolean} isLinkPredictionData True if parsing LinkPredictionData, false otherwise.
   * @returns {UniqueEdge[]} Unique edges with a count property added.
   */
  public static parse(queryResultEdges: Link[], isLinkPredictionData: boolean): UniqueEdge[] {
    // Edges to be returned
    const edges: UniqueEdge[] = [];

    // Collect the edges in map, to only keep unique edges
    // And count the number of same edges
    const edgesMap = new Map<string, number>();
    const attriMap = new Map<string, Record<string, any>>();
    if (queryResultEdges != null) {
      if (!isLinkPredictionData) {
        for (let j = 0; j < queryResultEdges.length; j++) {
          const newLink = queryResultEdges[j].from + ':' + queryResultEdges[j].to;
          edgesMap.set(newLink, (edgesMap.get(newLink) || 0) + 1);
          attriMap.set(newLink, queryResultEdges[j].attributes);
        }

        edgesMap.forEach((count, key) => {
          const fromTo = key.split(':');
          edges.push({
            from: fromTo[0],
            to: fromTo[1],
            count: count,
            attributes: attriMap.get(key) ?? [],
          });
        });
      } else {
        for (let i = 0; i < queryResultEdges.length; i++) {
          edges.push({
            from: queryResultEdges[i].from,
            to: queryResultEdges[i].to,
            count: queryResultEdges[i].attributes.jaccard_coefficient,
            attributes: queryResultEdges[i].attributes,
          });
        }
      }
    }
    return edges;
  }
}
