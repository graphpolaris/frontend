/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import AuthorizeApi from '../../../data/drivers/authorize/AuthorizeApi';
import BackendMessengerMock from '../../../data/drivers/backend-messenger/BackendMessengerMock';
import BackendMessageReceiverMock from '../../../data/drivers/backend-message-receiver/BackendMessageReceiverMock';
import AuthHolder from '../../entity/auth/models/AuthHolder';
import LoginUseCase from './LoginUseCase';
import DatabaseHolder from '../../entity/navbar/DatabaseHolder';
import RequestSchemaUseCase from '../request-schema/RequestSchemaUseCase';
import databaseAPIMock from '../../../data/drivers/database/databaseAPIMock';
import AuthorizeMock from '../../../data/drivers/authorize/AuthorizeApiMock';
import AuthorizationResult from '../../entity/auth/structures/AuthorizationResult';

describe('Login usecase', () => {
  const mockClientID = 'mock-clientID';
  const mockSessionID = 'mock-sessionID';
  const backendMessengerMock = new BackendMessengerMock('datastrophe.science.uu.nl');
  const authRepositoryMock = new AuthorizeMock(mockClientID, mockSessionID);
  const databaseApi = new databaseAPIMock(backendMessengerMock);
  const webSocketHandlerMock = new BackendMessageReceiverMock();
  const requestSchemaUseCase = new RequestSchemaUseCase(backendMessengerMock);
  const databaseHolder = new DatabaseHolder();
  let authHolder: AuthHolder;
  let loginUseCase: LoginUseCase;
  beforeEach(() => {
    authHolder = new AuthHolder();
    loginUseCase = new LoginUseCase(
      authRepositoryMock,
      databaseApi,
      authHolder,
      databaseHolder,
      webSocketHandlerMock,
      requestSchemaUseCase,
    );
  });
  const result: AuthorizationResult = {
    clientID: mockClientID,
    sessionID: mockSessionID,
  };

  it('AfterAuthorized should change the propriate values and get the database connected to the user', () => {
    loginUseCase
      .afterAuthorized(result)
      .then(() => {
        expect(authHolder.isUserAuthorized()).toEqual(true);
        expect(authHolder.getClientID()).toEqual(mockClientID);
        expect(authHolder.getSessionID()).toEqual(mockSessionID);
        expect(authHolder.getDatabaseNames()).toEqual(['testdb']);
        expect(webSocketHandlerMock.isConnected()).toEqual(true);
      })
      .catch((message) => {
        console.warn(message);
        fail();
      });
  });
  it('tryAutoLoginWithClientIDCookie should get the user autherized', () => {
    loginUseCase
      .tryAutoLoginWithClientIDCookie()
      .then(() => {
        expect(authHolder.isUserAuthorized()).toEqual(true);
        expect(authHolder.getClientID()).toEqual(mockClientID);
        expect(authHolder.getSessionID()).toEqual(mockSessionID);
        expect(webSocketHandlerMock.isConnected()).toEqual(true);
      })
      .catch((message) => {
        console.warn(message);
        fail();
      });
  });
  it('loginUser  should get the user autherized', () => {
    loginUseCase
      .loginUser('testcase')
      .then(() => {
        expect(authHolder.isUserAuthorized()).toEqual(true);
        expect(authHolder.getClientID()).toEqual(mockClientID);
        expect(authHolder.getSessionID()).toEqual(mockSessionID);
        expect(webSocketHandlerMock.isConnected()).toEqual(true);
      })
      .catch((message) => {
        console.warn(message);
        fail();
      });
  });
});
