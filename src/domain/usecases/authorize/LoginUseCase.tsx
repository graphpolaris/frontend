/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { LocalStorage } from '../../../data/drivers/LocalStorage';
import AuthHolder from '../../entity/auth/models/AuthHolder';
import AuthorizationResult from '../../entity/auth/structures/AuthorizationResult';
import DatabaseHolder from '../../entity/navbar/DatabaseHolder';
import AuthorizeRepository from '../../repository-interfaces/AuthorizeRepository';
import BackendMessageReceiver from '../../repository-interfaces/BackendMessageReceiver';
import DatabaseRepository from '../../repository-interfaces/DatabaseRepository';
import RequestSchemaUseCase from '../request-schema/RequestSchemaUseCase';

export type LoginType = 'google' | 'github' | 'free' | 'testcase';
/**
 * Login use case has functions for requesting client and session ID.
 * @todo Needs to be changed for proper authorization.
 */
export default class LoginUseCase {
  private authRepository: AuthorizeRepository;
  private databaseRepository: DatabaseRepository;
  private authHolder: AuthHolder;
  private databaseHolder: DatabaseHolder;

  private backendMessageConsumer: BackendMessageReceiver;

  private requestSchemaUseCase: RequestSchemaUseCase;

  private windowObjectReference: Window | null;
  private previousUrl: string | null;

  /**
   * constructor for the login use case.
   * @param authRepository
   * @param authHolder Observer for the authentification.
   * @param databaseHolder Observer for the current database.
   * @param webSocketHandler The websocket that receives messages from the backend.
   * @param requestSchemaUseCase The use case that implements a schema request.
   */
  constructor(
    authRepository: AuthorizeRepository,
    databaseRepository: DatabaseRepository,
    authHolder: AuthHolder,
    databaseHolder: DatabaseHolder,
    webSocketHandler: BackendMessageReceiver,
    requestSchemaUseCase: RequestSchemaUseCase,
  ) {
    this.authRepository = authRepository;
    this.databaseRepository = databaseRepository;
    this.authHolder = authHolder;
    this.databaseHolder = databaseHolder;

    this.backendMessageConsumer = webSocketHandler;

    this.requestSchemaUseCase = requestSchemaUseCase;

    this.windowObjectReference = null;
    this.previousUrl = null;
  }

  /**
   * LoginUsers logs the user in and awaits the result.
   * @returns A Promise that is resolved when the user is authenticated.
   */
  public async loginUser(loginType: LoginType): Promise<void> {
    this.authRepository
      .login()
      .then((authResult) => {
        this.afterAuthorized(authResult);
        return;
      })
      .catch(
        () => /* istanbul ignore next. This is only used during debug runs and is impossible to test*/ {
          console.warn('Local deployment was used');
          switch (loginType) {
            case 'google':
              window.location.replace('/user/create/');
              break;
            case 'github':
              window.location.replace('/user/create_github/');
              break;
            case 'free':
              window.location.replace('https://datastrophe.science.uu.nl/user/create_free/');
              break;
            default:
              window.location.replace('/user/create/');
              break;
          }
        },
      );
  }

  /**
   * Tries to login the client if they have a ID cookie.
   * @returns A Promise that is resolved when the user is authenticated.
   */
  public async tryAutoLoginWithClientIDCookie(): Promise<void> {
    this.authRepository.loginWithCookie().then((result) => {
      this.afterAuthorized(result).then(() => {
        return;
      });
    });
  }

  //if there's a callback in the url, this means we need to request a client and session cookie
  public async handleCallbackLogin(): Promise<void> {
    const loc = window.location.href;
    console.log('checking callback in url... authenticating and redirecting');
    if (loc.includes('/callback')) {
      console.log('callback found');
      this.authRepository
        .login()
        .then((authResult) => {
          this.afterAuthorized(authResult);
        })
        .catch(() => {
          console.log('redirecting....');
          window.location.replace('/develop/');
          return;
        });
    } else {
      console.log('no callback found -handlecallbacklogin');
      return;
    }
  }
  /*
   /**
    * To be called when an authorization was succesful.
    * @param authResult The authorization result after authorization was succesful.
    */
  public async afterAuthorized(authResult: AuthorizationResult): Promise<void> {
    this.authHolder.onSignedIn(authResult.clientID, authResult.sessionID);

    this.databaseRepository.fetchUserDatabases().then((databases) => {
      this.authHolder.setDatabaseNames(databases);
      // If all is well, the databases are retrieved from the user
      // If the user already has connected databases, set the first as the current in use
      if (databases.length > 0)
        this.databaseHolder.changeDatabase(LocalStorage.instance().cache.currentDatabaseKey);

      // Request a new websocket connection
      this.backendMessageConsumer.connect(() => {
        if (databases.length > 0)
          // Request the schema for this database
          this.requestSchemaUseCase.requestSchema(LocalStorage.instance().cache.currentDatabaseKey);
      });
    });
  }
}
