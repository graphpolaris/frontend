/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { EntityWithAttributes } from "../../entity/semantic-substrates/config-panel/Types";
import { Node, Link } from "../../entity/query-result/structures/NodeLinkResultType"; 

 /** The use case that calculates the line positions */
 export default class MakeTypesFromGraphUseCase {
   /**
    * Creates a list of unique node types based on the given list of nodes.
    * @param loadedNodes List of nodes from which the different types are to be fetched.
    * @returns List of unique node types.
    */
  public static makeNodeTypes(loadedNodes: Node[]): EntityWithAttributes[] {
    let nodes: EntityWithAttributes[] = [];
    for (let entry of loadedNodes) {
      let entryType = entry.id.split('/')[0];
      let node = nodes.find((x) => x.name == entryType);
      if (node) {
        //if node is already logged
        //then insert any unlogged attribute names

        //loop over attribute names
        for (let attribute in entry.attributes) {
          if (node.attributes.indexOf(attribute) == -1) {
            // console.log(attr);
            node.attributes.push(attribute); // TODO: test
          }
        }
      } else {
        //if node not in nodes yet
        //push all attributes
        let newattributes = [];
        for (let attr in entry.attributes) {
          newattributes.push(attr);
        }
        nodes.push({ name: entryType, attributes: newattributes, group: 1 });
      }
    }
    
    return nodes;
  }

  /**
   * Creates a list of unique relation types based on the given list of relations.
   * @param loadedRelations List of relations from which the different types are to be fetched.
   * @returns List of unique relation types.
   */
  public static makeRelationTypes(loadedRelations: Link[]): EntityWithAttributes[] {
    let relations: EntityWithAttributes[] = [];
    for (let entry of loadedRelations) {
      let entryType = entry.from.split('/')[0] + entry.to.split('/')[0];
      let relation = relations.find((x) => x.name == entryType);
      if (relation) {
        //if relation is already logged
        //then insert any unlogged attribute names

        //loop over attribute names
        for (let attr in entry.attributes) {
          if (relation.attributes.indexOf(attr) == -1) {
            relation.attributes.push(attr); // TODO: test
          }
        }
      } else {
        //if node not in nodes yet
        //push all attributes
        let newattributes = [];
        for (let attribute in entry.attributes) {
          newattributes.push(attribute);
        }
        relations.push({ name: entryType, attributes: newattributes, group: 1 });
      }
    }

    return relations;    
  }
 }
 