/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { SchemaElements } from '../../entity/graph-schema/structures/Types';
import { FlowElement, getIncomers, getOutgoers, Node } from 'react-flow-renderer';

/**
 * This class is responsible for creating draw order of the nodes in the schema.
 */
export default class DrawOrderUseCase {
  /**
   * This function creates the draw order as follows:
   * Determine the entity-node with the most connections and position it at the top.
   * Take the connections of this node and position the node with the most connections below the current node.
   * Repeat this for all connected nodes.
   * Repeat all steps above until all nodes are covered.
   * As this happens, keep track of the order in which the nodes have been positioned, this is the draw order.
   * @param elements The nodes, edges and attributes of the schema.
   * @return {Node[]} A draw order for the nodes of the schema.
   */
  public createDrawOrder = (elements: SchemaElements): Node[] => {
    let amountOfEdgesPerNode: Map<string, number>;
    let drawOrder: Node[];
    drawOrder = [];
    amountOfEdgesPerNode = new Map<string, number>();

    // Adds all nodes to the dictionary with their respective amount of edges
    elements.edges.forEach((edge) => {
      const source = amountOfEdgesPerNode.get(edge.source) || 0;
      amountOfEdgesPerNode.set(edge.source, source + 1);
      const target = amountOfEdgesPerNode.get(edge.target) || 0;
      amountOfEdgesPerNode.set(edge.target, target + 1);
    });

    // Checks for nodes without edges and adds them to the dictionary
    elements.nodes.forEach((node) => {
      if (!amountOfEdgesPerNode.has(node.id)) amountOfEdgesPerNode.set(node.id, 0);
    });
    // Creates the clusters for all nodes inside the dictionary
    while (amountOfEdgesPerNode.size > 0) {
      this.startCluster(amountOfEdgesPerNode, drawOrder, elements);
    }
    return drawOrder;
  };

  /**
   * Finds the node with the largest amount of edges and uses it as the starting point of a cluster.
   * It removes this first node from the dictionary after pushing it on to the drawOrder array.
   * @param amountOfEdgesPerNode A map that contains the nodes and the amount of connections they have.
   * @param drawOrder The order in which the nodes will be drawn.
   */
  public startCluster(amountOfEdgesPerNode: Map<string, number>, drawOrder: Node[], elements: SchemaElements) {
    const nodeWithMaxAmountOfEdges = [...Array.from(amountOfEdgesPerNode.entries())].reduce((elementOne, elementTwo) => (elementTwo[1] > elementOne[1] ? elementTwo : elementOne))[0];

    elements.nodes.filter((node) => {
      if (node.id == nodeWithMaxAmountOfEdges) {
        drawOrder.push(node);
        amountOfEdgesPerNode.delete(node.id);
        this.createCluster(node, amountOfEdgesPerNode, drawOrder, elements);
        return;
      }
    });
  }

  /**
   * Creates the cluster using the given nodeWithMaxAmountOfEdges, that has the most edges.
   * It then pushes the node with a direct connection to the nodeWithMaxAmountOfEdges and the most edges to the drawOrder.
   * This node is then recursively used as the next nodeWithMaxAmountOfEdges, untill no nodes are left.
   * @param node The current nodeWithMaxAmountOfEdges.
   * @param amountOfEdgesPerNode A map that contains the nodes and the amount of connections they have.
   * @param drawOrder The order in which the nodes will be drawn.
   * @returns {Node[]} The nodes added to the draw order from the created cluster.
   */
  public createCluster(
    node: Node,
    amountOfEdgesPerNode: Map<string, number>,
    drawOrder: Node[],
    elements: SchemaElements,
  ): Node[] {
    let elementList: FlowElement[];
    let nodeWithMaxAmountOfEdges: Node;
    let maxAmountOfEdges: number;
    nodeWithMaxAmountOfEdges = { id: '', position: { x: 0, y: 0 } };
    maxAmountOfEdges = 0;
    elementList = [...elements.nodes, ...elements.edges];

    let cluster = [...getOutgoers(node, elementList), ...getIncomers(node, elementList)];

    cluster = cluster.filter((node) => !drawOrder.includes(node));

    if (cluster.length > 0) {
      cluster.forEach((node) => {
        if (amountOfEdgesPerNode.get(node.id) !== undefined && (amountOfEdgesPerNode.get(node.id) as number) >= maxAmountOfEdges) {
          maxAmountOfEdges = amountOfEdgesPerNode.get(node.id) as number;
          nodeWithMaxAmountOfEdges = node;
        }
      });
      amountOfEdgesPerNode.delete(nodeWithMaxAmountOfEdges.id);
      drawOrder.push(nodeWithMaxAmountOfEdges);
      return this.createCluster(nodeWithMaxAmountOfEdges, amountOfEdgesPerNode, drawOrder, elements);
    } else return drawOrder;
  }
}
