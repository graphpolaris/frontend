/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Edge, Node, getConnectedEdges } from 'react-flow-renderer';
import { Attribute } from '../../entity/graph-schema/structures/InputDataTypes';
import { SchemaElements } from '../../entity/graph-schema/structures/Types';

/** This class is responsible for creating and rendering the edges of the schema */
export default class EdgeUseCase {
  private counterOutgoingEdges: number;
  private counterIncomingEdges: number;
  private distanceCounterNormalEdges = 40;
  private initialOffsetOfNodes = 155 - this.distanceCounterNormalEdges;
  private distanceCounterSelfEdges = 58;
  private setRelationNodePosition:
    | ((
        centerX: number,
        centerY: number,
        id: string,
        from: string,
        to: string,
        attributes: Attribute[],
      ) => void)
    | undefined;

  public constructor() {
    this.counterOutgoingEdges = 1;
    this.counterIncomingEdges = 1;
    this.setRelationNodePosition = undefined;
  }

  /**
   * Use the drawOrder to draw the edges of the nodes starting with the top node.
   * We check all connected edges of a node and create lists of incoming and outgoing edges.
   * These lists are used in renderEdges to draw the edges for the current node.
   * @param drawOrder The order in which the nodes will be drawn.
   * @param setRelationNodePosition The function an edge needs to create its relation node.
   * @returns {Edge[]} The list of edges that will be rendered.
   */
  public positionEdges = (
    drawOrder: Node[],
    elements: SchemaElements,
    setRelationNodePosition: (
      centerX: number,
      centerY: number,
      id: string,
      from: string,
      to: string,
      attributes: Attribute[],
    ) => void,
  ): Edge[] => {
    let incomingEdges: Edge[];
    let outgoingEdges: Edge[];
    let edges: Edge[];
    let used: Edge[];
    let result: Edge[];
    let drawOrderID: string[];
    used = [];
    result = [];

    this.counterOutgoingEdges = 1;
    this.counterIncomingEdges = 1;
    this.setRelationNodePosition = setRelationNodePosition;
    drawOrderID = this.convertDrawOrder(drawOrder);
    for (let i = 0; i < drawOrder.length; i++) {
      incomingEdges = [];
      outgoingEdges = [];
      edges = getConnectedEdges([drawOrder[i]], elements.edges);
      edges.forEach((edge) => {
        if (!used.includes(edge)) {
          if (edge.source == drawOrder[i].id) {
            outgoingEdges.push(edge);
            used.push(edge);
          } else {
            incomingEdges.push(edge);
            used.push(edge);
          }
        }
      });

      result = [
        ...result,
        ...this.renderEdges(
          this.sortEdgeArray(incomingEdges, drawOrderID, true),
          this.sortEdgeArray(outgoingEdges, drawOrderID, false),
          drawOrderID,
          drawOrder,
          i,
        ),
      ];
    }
    if (elements.selfEdges.length > 0)
      return [...result, ...this.renderSelfEdges(elements.selfEdges, drawOrder, drawOrderID)];

    return result;
  };

  /**
   * Goes over the list of edges to position them in the schema.
   * First goes through all incoming edges and then through the outgoing edges
   * @param incomingEdges The incoming edges of the node.
   * @param outgoingEdges The outgoing edges of the node.
   * @param drawOrderID The converted drawOrder that only contains the id of the node.
   * @param drawOrder The order in which the nodes will be drawn.
   * @param currentNode Index of the Node in the drawOrder array.
   * @returns {Edge[]} Draws the edge list for the rendering of the schema.
   */
  public renderEdges = (
    incomingEdges: Edge[],
    outgoingEdges: Edge[],
    drawOrderID: string[],
    drawOrder: Node[],
    currentNode: number,
  ): Edge[] => {
    for (let i = 0; i < incomingEdges.length; i++) {
      incomingEdges = this.renderIncomingEdges(
        incomingEdges,
        drawOrderID,
        drawOrder,
        currentNode,
        i,
      );
    }

    for (let i = 0; i < outgoingEdges.length; i++) {
      outgoingEdges = this.renderOutgoingEdges(
        outgoingEdges,
        drawOrderID,
        drawOrder,
        currentNode,
        i,
      );
    }
    return [...incomingEdges, ...outgoingEdges];
  };

  /**
   * Draw the incoming edges between the nodes in the correct positions for the currentNode.
   * We use the currentNode and draw all the incoming edges of this node.
   * Nodes directly beneath are drawn with an edge from the bottom to the top handle.
   * For edges that go to nodes further away the right handle is used for incoming edges.
   * @param incomingEdges The incoming edges of the node.
   * @param drawOrderID The converted drawOrder that only contains the id of the node.
   * @param drawOrder The order in which the nodes will be drawn.
   * @param currentNode Index of the Node in the drawOrder array.
   * @param i Index of the edge in the edge list.
   * @returns {Edge[]} Draws the edge list for the incoming edges.
   */
  public renderIncomingEdges = (
    incomingEdges: Edge[],
    drawOrderID: string[],
    drawOrder: Node[],
    currentNode: number,
    index: number,
  ): Edge[] => {
    if (incomingEdges[index].source == drawOrderID[currentNode + 1] && index == 0) {
      // Draw nodes that are directly next to eachother with an edge from the top to the bottom
      incomingEdges[0].sourceHandle = 'entitySourceTop';
      incomingEdges[0].targetHandle = 'entityTargetBottom';

      // Add the handle id to the data of the specified node in order to display the correct handles
      drawOrder[drawOrderID.indexOf(incomingEdges[index].source)].data.handles.push(
        'entitySourceTop',
      );
      drawOrder[drawOrderID.indexOf(incomingEdges[index].target)].data.handles.push(
        'entityTargetBottom',
      );

      incomingEdges[index].data.setRelationNodePosition = this.setRelationNodePosition;

      incomingEdges[index].data.d = 0;
    } else {
      // Draw nodes that are not directly next to eachother with an edge from the right handle
      incomingEdges[index].sourceHandle = 'entitySourceRight';
      incomingEdges[index].targetHandle = 'entityTargetRight';

      // Add the handle id to the data of the specified node in order to display the correct handles
      drawOrder[drawOrderID.indexOf(incomingEdges[index].source)].data.handles.push(
        'entitySourceRight',
      );
      drawOrder[drawOrderID.indexOf(incomingEdges[index].target)].data.handles.push(
        'entityTargetRight',
      );

      // Create distance between the links with a counter
      incomingEdges[index].data.d =
        this.counterIncomingEdges * this.distanceCounterNormalEdges + this.initialOffsetOfNodes;

      incomingEdges[index].data.setRelationNodePosition = this.setRelationNodePosition;

      this.counterIncomingEdges++;
    }
    return incomingEdges;
  };

  /**
   * Draw the outgoing edges between the nodes in the correct positions for the currentNode.
   * We use the currentNode and draw all the outgoing edges of this node.
   * Nodes directly beneath are drawn with an edge from the bottom to the top handle.
   * For edges that go to nodes further away the right handle is used for outgoing edges.
   * @param outgoingEdges The outgoing edges of the node.
   * @param drawOrderID The converted drawOrder that only contains the id of the node.
   * @param drawOrder The order in which the nodes will be drawn.
   * @param currentNode Index of the Node in the drawOrder array.
   * @param index Index of the edge in the edge list.
   * @returns {Edge[]} Draws the edge list for the outgoing edges.
   */
  public renderOutgoingEdges = (
    outgoingEdges: Edge[],
    drawOrderID: string[],
    drawOrder: Node[],
    currentNode: number,
    index: number,
  ): Edge[] => {
    if (outgoingEdges[index].target == drawOrderID[currentNode + 1] && index == 0) {
      // Draw nodes that are directly next to eachother with a link from the top to the bottom
      outgoingEdges[0].sourceHandle = 'entitySourceBottom';
      outgoingEdges[0].targetHandle = 'entityTargetTop';

      // Add the handle id to the data of the specified node in order to display the correct handles
      drawOrder[drawOrderID.indexOf(outgoingEdges[index].source)].data.handles.push(
        'entitySourceBottom',
      );
      drawOrder[drawOrderID.indexOf(outgoingEdges[index].target)].data.handles.push(
        'entityTargetTop',
      );

      outgoingEdges[index].data.setRelationNodePosition = this.setRelationNodePosition;

      outgoingEdges[index].data.d = 0;
    } else {
      outgoingEdges[index].sourceHandle = 'entitySourceLeft';
      outgoingEdges[index].targetHandle = 'entityTargetLeft';

      // Add the handle id to the data of the specified node in order to display the correct handle
      drawOrder[drawOrderID.indexOf(outgoingEdges[index].source)].data.handles.push(
        'entitySourceLeft',
      );
      drawOrder[drawOrderID.indexOf(outgoingEdges[index].target)].data.handles.push(
        'entityTargetLeft',
      );

      // Create distance between the links with a counter
      outgoingEdges[index].data.d =
        -this.counterOutgoingEdges * this.distanceCounterNormalEdges - this.initialOffsetOfNodes;

      outgoingEdges[index].data.setRelationNodePosition = this.setRelationNodePosition;

      this.counterOutgoingEdges++;
    }
    return outgoingEdges;
  };

  /**
   * Sort edges depending on if they are incoming or outgoing edges.
   * @param input List of edges to sort.
   * @param drawOrderID Draw order by id to sort the list with.
   * @param source Boolean that contains if the edge is a source or target.
   * @returns {Edge[]} List of sorted edges.
   *
   */
  public sortEdgeArray(input: Edge[], drawOrderID: string[], source: boolean): Edge[] {
    let output: Edge[];
    output = [];
    input.forEach((edge) => {
      if (source) output[drawOrderID.indexOf(edge.source)] = edge;
      else output[drawOrderID.indexOf(edge.target)] = edge;
    });

    output = output.filter(function (element) {
      return element != null;
    });
    return output;
  }

  /**
   * Edges only know the id of the nodes, so we need to convert the nodes in the drawOrder to a list of their id's.
   * This way the edges can reference nodes from this list.
   * @param drawOrder The order in which the nodes will be drawn.
   * @returns {string[]} List of orderd node id's.
   */
  private convertDrawOrder(drawOrder: Node[]): string[] {
    let orderedNodeIDs: string[] = [];
    drawOrder.forEach((node) => {
      orderedNodeIDs.push(node.id);
    });
    return orderedNodeIDs;
  }

  /** This creates the self-edges */
  private renderSelfEdges(selfEdges: Edge[], drawOrder: Node[], drawOrderID: string[]): Edge[] {
    selfEdges.forEach((edge) => {
      edge.sourceHandle = 'entitySourceLeft';
      edge.targetHandle = 'entityTargetRight';

      // Add the handle id to the data of the specified node in order to display the correct handles
      drawOrder[drawOrderID.indexOf(edge.source)].data.handles.push('entitySourceLeft');
      drawOrder[drawOrderID.indexOf(edge.target)].data.handles.push('entityTargetRight');

      // Create distance between the links with a counter
      edge.data.d = this.distanceCounterSelfEdges;

      edge.data.setRelationNodePosition = this.setRelationNodePosition;
    });
    return selfEdges;
  }
}
