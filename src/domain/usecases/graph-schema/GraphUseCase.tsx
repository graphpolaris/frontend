/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { SchemaElements } from '../../entity/graph-schema/structures/Types';
import { ArrowHeadType, Edge, Node } from 'react-flow-renderer';
import { Schema } from '../../entity/graph-schema/structures/InputDataTypes';

/** This class is responsible for creating a graphtype out of a schema with nodes and edges received from the backend. */
export default class GraphUseCase {
  public nodes: Node[];
  public edges: Edge[];
  public selfEdges: Edge[];

  public constructor() {
    this.nodes = [];
    this.edges = [];
    this.selfEdges = [];
  }

  /**
   * Loops over the schema, creates the different nodes and edges and export it as a list of schema elements.
   * @param schema The schema received from the backend.
   * @returns {SchemaElements} A list of nodes, edges, and attributes.
   */
  public createGraphFromInputData(schema: Schema): SchemaElements {
    let originNode: Node<any>;
    this.nodes = [];
    this.edges = [];
    this.selfEdges = [];
    for (let i = 0; i < schema.nodes.length; i++) {
      originNode = this.makeEntity(schema.nodes[i].name, schema.nodes[i].attributes);
    }

    for (let j = 0; j < schema.edges.length; j++) {
      this.makeRelation(schema.edges[j], schema.edges[j].attributes);
    }
    return {
      nodes: this.nodes,
      edges: this.edges,
      selfEdges: this.selfEdges,
    };
  }

  /**
   * Creates a relation-edge and pushes it on the edge list.
   * @param edge  The edge information which will be needed to create a edge.
   * @param attributes The attributes contained by the edge.
   * @returns {EdgeType} The created edge.
   */
  public makeRelation(edge: any, attributes: any) {
    const newEdge: Edge = {
      id: edge.collection,
      source: edge.from,
      target: edge.to,
      type: 'nodeEdge',
      label: edge.name,
      data: {
        attributes: attributes,
        d: '',
        created: false,
        collection: edge.collection,
        edgeCount: 0,
      },
      arrowHeadType: ArrowHeadType.Arrow,
    };

    if (edge.from != edge.to) {
      this.edges.push(newEdge);
    } else {
      newEdge.type = 'selfEdge';
      this.selfEdges.push(newEdge);
    }
  }

  /**
   * Creates a new entity node, add it to the list of nodes and updates the nodesdict.
   * @param id          Node id for the new node.
   * @param attributes  List of attributes contained by the node.
   * @returns {Node}    The created node.
   */
  public makeEntity(id: string, attributes: any): Node {
    const newNode: Node = {
      type: 'entity',
      id: id,
      position: { x: 0, y: 0 },
      data: {
        attributes: attributes,
        handles: [],
        width: 165,
        height: 30,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 0,
      },
    };
    this.nodes.push(newNode);
    return newNode;
  }
}
