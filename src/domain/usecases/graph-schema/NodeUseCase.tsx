/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Node } from 'react-flow-renderer';

/** This class is responsible for creating and rendering the nodes of the schema. */
export default class NodeUseCase {
  /**
   * Renders the schema and sets the position of the nodes to the calculated posisitions.
   * Add the function to toggle attributes.
   * @param nodeList The nodes that need to be rendered.
   * @param startPosition The position of the first node to be drawn.
   * @param toggleNodeQualityPopup A function that shows or hides the node's node quality popup.
   * @param toggleAttributeAnalyticsPopupMenu A function that shows or hides the node's attribute analytics popup menu.
   * @returns {Node[]} The nodes that the schema renders.
   */
  public verticalDistanceBetweenEntityNodes = 150;
  public setEntityNodePosition(
    nodeList: Node[],
    startPosition: { x: number; y: number },
    toggleNodeQualityPopup: (id: string) => void,
    toggleAttributeAnalyticsPopupMenu: (id: string) => void,
  ): Node[] {
    let position: { x: number; y: number };
    position = startPosition;
    nodeList.forEach((node) => {
      node.position = { x: position.x, y: position.y };
      position.y = position.y + this.verticalDistanceBetweenEntityNodes;
      node.data.toggleNodeQualityPopup = toggleNodeQualityPopup;
      node.data.toggleAttributeAnalyticsPopupMenu = toggleAttributeAnalyticsPopupMenu;
    });

    return nodeList;
  }
}
