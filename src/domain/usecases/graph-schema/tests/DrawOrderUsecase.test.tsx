/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Node } from 'react-flow-renderer';
import { schema } from '../../../../data/mock-data/graph-schema/MockGraph';
import DrawOrderUseCase from '../DrawOrderUseCase';
import GraphUseCase from '../GraphUseCase';

/** Testsuite for the draworder use case. */
describe('DrawOrderUseCase', () => {
  beforeEach(() => jest.resetModules());

  it('should start making the draworder', () => {
    const drawOrderUseCase = new DrawOrderUseCase();
    const graphUseCase = new GraphUseCase();
    const schemaElements = graphUseCase.createGraphFromInputData(schema);

    const elements = {
      nodes: schemaElements.nodes,
      edges: schemaElements.edges,
      selfEdges: schemaElements.selfEdges,
    };

    const receiveddraworder = drawOrderUseCase.createDrawOrder(elements);
    const expecteddraworder = [
      {
        type: 'entity',
        id: 'Airport',
        position: { x: 0, y: 0 },
        data: {
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
          ],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },

      {
        type: 'entity',
        id: 'Plane',
        position: { x: 0, y: 0 },
        data: {
          attributes: [
            { name: 'type', type: 'string' },
            { name: 'maxFuelCapacity', type: 'int' },
          ],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Staff',
        position: { x: 0, y: 0 },
        data: {
          attributes: [],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Airport2',
        position: { x: 0, y: 0 },
        data: {
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
          ],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 0, y: 0 },
        data: {
          attributes: [],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Unconnected',
        position: { x: 0, y: 0 },
        data: {
          attributes: [],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
    ];

    expect(expecteddraworder).toEqual(receiveddraworder);
  });

  it('should start making the clusters', () => {
    const drawOrderUseCase = new DrawOrderUseCase();
    const graphUseCase = new GraphUseCase();
    const mockCallback = (drawOrderUseCase.createCluster = jest.fn());
    const drawOrder: Node[] = [];
    const dict = new Map<string, number>();
    dict.set('item 1', 5);
    dict.set('airplane', 2);
    dict.set('Thijs', 8);
    dict.set('item 4', 1);

    const schemaElements = graphUseCase.createGraphFromInputData(schema);
    drawOrderUseCase.startCluster(dict, drawOrder, schemaElements);

    /** check if indeed the highest value is deleted */
    const expecteddict = new Map<string, number>();
    expecteddict.set('item 1', 5);
    expecteddict.set('airplane', 2);
    expecteddict.set('item 4', 1);
    expect(dict).toEqual(expecteddict);

    /** check if the node has been pushed to the draworder list */
    const expecteddrawOrder: Node[] = [
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 0, y: 0 },
        data: {
          attributes: [],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
    ];
    expect(drawOrder).toEqual(expecteddrawOrder);

    /** check if the function only get called once ( so no duplicate nodes) */
    expect(mockCallback.mock.calls.length).toBe(1);
  });

  it('should get a single cluster', () => {
    const drawOrderUseCase = new DrawOrderUseCase();
    const graphUseCase = new GraphUseCase();
    const dict = new Map<string, number>();
    const drawOrder: Node[] = [
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 98.46153846153845, y: 517.4707692307693 },
        data: { attributes: [], nodeCount: 0, width: 165, height: 30 },
      },
    ];
    const node: Node = {
      type: 'entity',
      id: 'Thijs',
      position: { x: 98.46153846153845, y: 517.4707692307693 },
      data: { attributes: [], nodeCount: 0, width: 165, height: 30 },
    };
    dict.set('item 1', 5);
    dict.set('airplane', 2);
    dict.set('item 4', 1);

    const returneddraworder = drawOrderUseCase.createCluster(
      node,
      dict,
      drawOrder,
      graphUseCase.createGraphFromInputData(schema),
    );
    const expecteddraworder = [
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 98.46153846153845, y: 517.4707692307693 },
        data: { attributes: [], nodeCount: 0, width: 165, height: 30 },
      },
      { id: '', position: { x: 0, y: 0 } },
    ];
    expect(returneddraworder).toEqual(expecteddraworder);
  });

  it('should get multiple clusters', () => {
    const drawOrderUseCase = new DrawOrderUseCase();
    const graphUseCase = new GraphUseCase();

    let dict = new Map<string, number>();
    let drawOrder: Node[] = [];

    dict.set('Airport', 5);
    dict.set('Airport2', 3);
    dict.set('Plane', 3);
    dict.set('Staff', 3);
    dict.set('Thijs', 2);

    const schemaElements = graphUseCase.createGraphFromInputData(schema);

    const elements = {
      nodes: schemaElements.nodes,
      edges: schemaElements.edges,
      selfEdges: schemaElements.selfEdges,
    };

    while (dict.size > 0) {
      drawOrderUseCase.startCluster(dict, drawOrder, elements);
    }

    const expecteddraworder = [
      {
        type: 'entity',
        id: 'Airport',
        position: { x: 0, y: 0 },
        data: {
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
          ],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Plane',
        position: { x: 0, y: 0 },
        data: {
          attributes: [
            { name: 'type', type: 'string' },
            { name: 'maxFuelCapacity', type: 'int' },
          ],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Staff',
        position: { x: 0, y: 0 },
        data: {
          attributes: [],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Airport2',
        position: { x: 0, y: 0 },
        data: {
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
          ],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 0, y: 0 },
        data: {
          attributes: [],
          connectedRatio: 0,
          handles: [],
          nodeCount: 0,
          summedNullAmount: 0,
          width: 165,
          height: 30,
        },
      },
    ];
    expect(drawOrder).toEqual(expecteddraworder);
  });

  /**
   * checks if createcluster get called enough times
   */
  it('should call createCluster the correct amount of times', () => {
    const drawOrderUseCase = new DrawOrderUseCase();
    const graphUseCase = new GraphUseCase();
    const mockCallback = (drawOrderUseCase.createCluster = jest.fn());
    const dict = new Map<string, number>();
    const drawOrder: Node[] = [];

    dict.set('Airport', 5);
    dict.set('Airport2', 4);
    dict.set('Plane', 4);
    dict.set('Staff', 3);
    dict.set('Thijs', 1);

    const schemaElements = graphUseCase.createGraphFromInputData(schema);

    const elements = {
      nodes: schemaElements.nodes,
      edges: schemaElements.edges,
      selfEdges: schemaElements.selfEdges,
    };

    while (dict.size > 0) {
      drawOrderUseCase.startCluster(dict, drawOrder, elements);
    }
    expect(mockCallback.mock.calls.length).toBe(5);
  });
});
