/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Edge, ArrowHeadType } from 'react-flow-renderer';
import { schema } from '../../../../data/mock-data/graph-schema/MockGraph';
import { Attribute } from '../../../entity/graph-schema/structures/InputDataTypes';
import EdgeUseCase from '../EdgeUseCase';
import GraphUseCase from '../GraphUseCase';

/** Testsuite for the edge use case. */
describe('EdgeUseCase', () => {
  beforeEach(() => jest.resetModules());

  it('should position the edges', () => {
    const edgeUseCase = new EdgeUseCase();
    const graphUseCase = new GraphUseCase();
    function setRelationNodePosition(
      centerX: number,
      centerY: number,
      id: string,
      from: string,
      to: string,
      attributes: Attribute[],
    ): void {
      //TODO It can't be right htis is empty
    }
    const draworder = [
      {
        type: 'entity',
        id: 'Airport',
        position: { x: 0, y: 0 },
        data: { handles: [] },
      },
      { type: 'entity', id: 'Plane', position: { x: 0, y: 0 }, data: { handles: [] } },
      {
        type: 'entity',
        id: 'Airport2',
        position: { x: 0, y: 0 },
        data: { handles: [] },
      },
      { type: 'entity', id: 'Staff', position: { x: 0, y: 0 }, data: { handles: [] } },
      { type: 'entity', id: 'Thijs', position: { x: 0, y: 0 }, data: { handles: [] } },
    ];

    const returnedEdges = edgeUseCase.positionEdges(
      draworder,
      graphUseCase.createGraphFromInputData(schema),
      setRelationNodePosition,
    );
    const expected: Edge[] = [
      {
        id: 'flights',
        source: 'Plane',
        target: 'Airport',
        type: 'nodeEdge',
        label: 'Plane:Airport',
        data: {
          attributes: [],
          d: 0,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceTop',
        targetHandle: 'entityTargetBottom',
      },
      {
        id: 'flights',
        source: 'Airport2',
        target: 'Airport',
        type: 'nodeEdge',
        label: 'Airport2:Airport',
        data: {
          attributes: [
            { name: 'arrivalTime', type: 'int' },
            { name: 'departureTime', type: 'int' },
          ],
          d: 155,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceRight',
        targetHandle: 'entityTargetRight',
      },
      {
        id: 'flights',
        source: 'Thijs',
        target: 'Airport',
        type: 'nodeEdge',
        label: 'Thijs:Airport',
        data: {
          attributes: [{ name: 'hallo', type: 'string' }],
          d: 195,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceRight',
        targetHandle: 'entityTargetRight',
      },
      {
        id: 'flights',
        source: 'Airport',
        target: 'Staff',
        type: 'nodeEdge',
        label: 'Airport:Staff',
        data: {
          attributes: [{ name: 'salary', type: 'int' }],
          d: -155,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceLeft',
        targetHandle: 'entityTargetLeft',
      },
      {
        id: 'flights',
        source: 'Airport',
        target: 'Thijs',
        type: 'nodeEdge',
        label: 'Airport:Thijs',
        data: {
          attributes: [{ name: 'hallo', type: 'string' }],
          d: -195,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceLeft',
        targetHandle: 'entityTargetLeft',
      },
      {
        id: 'flights',
        source: 'Airport2',
        target: 'Plane',
        type: 'nodeEdge',
        label: 'Airport2:Plane',
        data: {
          attributes: [{ name: 'hallo', type: 'string' }],
          d: 0,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceTop',
        targetHandle: 'entityTargetBottom',
      },
      {
        id: 'flights',
        source: 'Staff',
        target: 'Plane',
        type: 'nodeEdge',
        label: 'Staff:Plane',
        data: {
          attributes: [{ name: 'hallo', type: 'string' }],
          d: 235,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceRight',
        targetHandle: 'entityTargetRight',
      },
      {
        id: 'flights',
        source: 'Staff',
        target: 'Airport2',
        type: 'nodeEdge',
        label: 'Staff:Airport2',
        data: {
          attributes: [{ name: 'hallo', type: 'string' }],
          d: 0,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceTop',
        targetHandle: 'entityTargetBottom',
      },
      {
        id: 'flights',
        source: 'Airport',
        target: 'Airport',
        type: 'selfEdge',
        label: 'Airport:Airport',
        data: {
          attributes: [{ name: 'test', type: 'string' }],
          d: 58,
          edgeCount: 0,
          created: false,
          collection: 'flights',
          setRelationNodePosition: setRelationNodePosition,
        },
        arrowHeadType: ArrowHeadType.Arrow,
        sourceHandle: 'entitySourceLeft',
        targetHandle: 'entityTargetRight',
      },
    ];

    expect(returnedEdges).toEqual(expected);
  });

  it('should render the Edges', () => {
    const edgeUseCase = new EdgeUseCase();

    const edgesin: Edge[] = [
      {
        id: 'Staff:Plane',
        label: 'Staff:Plane',
        type: 'nodeEdge',
        source: 'Staff',
        target: 'Plane',
        arrowHeadType: ArrowHeadType.Arrow,
        data: { d: 0, attributes: [] },
        sourceHandle: 'entitySourceTop',
        targetHandle: 'entityTargetBottom',
      },
      {
        id: 'Airport2:Plane',
        label: 'Airport2:Plane',
        type: 'nodeEdge',
        source: 'Airport2',
        target: 'Plane',
        arrowHeadType: ArrowHeadType.Arrow,
        data: { d: 120, attributes: [] },
        sourceHandle: 'entitySourceRight',
        targetHandle: 'entityTargetRight',
      },
    ];
    const edgesout: Edge[] = [
      {
        id: 'Staff:Airport2',
        label: 'Staff:Airport2',
        type: 'nodeEdge',
        source: 'Staff',
        target: 'Airport2',
        arrowHeadType: ArrowHeadType.Arrow,
        data: { d: 0, attributes: [] },
        sourceHandle: 'entitySourceBottom',
        targetHandle: 'entityTargetTop',
      },
    ];

    const draworder = [
      {
        type: 'entity',
        id: 'Airport2',
        position: { x: 98.46153846153845, y: 477.47076923076924 },
        data: { attributes: [], handles: [] },
      },
      {
        type: 'entity',
        id: 'Plane',
        position: { x: 98.46153846153845, y: 177.47076923076924 },
        data: { attributes: [], handles: [] },
      },
      {
        type: 'entity',
        id: 'Staff',
        position: { x: 98.46153846153845, y: 327.47076923076924 },
        data: { attributes: [], handles: [] },
      },
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 98.46153846153845, y: 627.4707692307693 },
        data: { attributes: [], handles: [] },
      },
    ];
    const draworderID = ['Airport2', 'Plane', 'Staff', 'Thijs'];
    const resultedges = edgeUseCase.renderEdges(edgesin, edgesout, draworderID, draworder, 1);
    const expectededges = [
      {
        id: 'Staff:Plane',
        label: 'Staff:Plane',
        type: 'nodeEdge',
        source: 'Staff',
        sourceHandle: 'entitySourceTop',
        target: 'Plane',
        targetHandle: 'entityTargetBottom',
        arrowHeadType: 'arrow',
        data: { d: 0, attributes: [] },
      },
      {
        id: 'Airport2:Plane',
        label: 'Airport2:Plane',
        source: 'Airport2',
        sourceHandle: 'entitySourceRight',
        target: 'Plane',
        targetHandle: 'entityTargetRight',
        type: 'nodeEdge',
        arrowHeadType: 'arrow',
        data: { d: 155, view: undefined, attributes: [] },
      },
      {
        id: 'Staff:Airport2',
        label: 'Staff:Airport2',
        type: 'nodeEdge',
        source: 'Staff',
        sourceHandle: 'entitySourceLeft',
        target: 'Airport2',
        targetHandle: 'entityTargetLeft',
        arrowHeadType: 'arrow',
        data: { d: -155, view: undefined, attributes: [] },
      },
    ];

    expect(resultedges).toEqual(expectededges);
  });

  it('should sort the incoming edges', () => {
    const edgeUseCase = new EdgeUseCase();
    const incomingedges: Edge[] = [
      {
        id: 'Airport2:Airport',
        label: 'Airport2:Airport',
        type: 'nodeEdge',
        source: 'Airport2',
        target: 'Airport',
        arrowHeadType: ArrowHeadType.Arrow,
        data: {
          d: '',
          attributes: [],
        },
      },
      {
        id: 'Plane:Airport',
        label: 'Plane:Airport',
        type: 'nodeEdge',
        source: 'Plane',
        target: 'Airport',
        arrowHeadType: ArrowHeadType.Arrow,
        data: { d: '', attributes: [] },
      },
    ];
    const result = edgeUseCase.sortEdgeArray(incomingedges, ['Plane', 'Airport2'], true);
    const expected: Edge[] = [
      {
        arrowHeadType: ArrowHeadType.Arrow,
        data: {
          d: '',
          attributes: [],
        },
        id: 'Plane:Airport',
        label: 'Plane:Airport',
        type: 'nodeEdge',
        source: 'Plane',
        target: 'Airport',
      },
      {
        arrowHeadType: ArrowHeadType.Arrow,
        data: {
          d: '',
          attributes: [],
        },
        id: 'Airport2:Airport',
        label: 'Airport2:Airport',
        type: 'nodeEdge',
        source: 'Airport2',
        target: 'Airport',
      },
    ];

    expect(result).toEqual(expected);
    expect(result).not.toContain(null);
    expect(result.length).toBe(2);
  });

  it('should sort the outgoing edges', () => {
    const edgeUseCase = new EdgeUseCase();
    const graphUseCase = new GraphUseCase();
    const outgoingedges: Edge[] = [
      {
        id: 'Airport:Staff',
        label: 'Airport:Staff',
        type: 'nodeEdge',
        source: 'Airport',
        target: 'Staff',
        arrowHeadType: ArrowHeadType.Arrow,
        data: { d: '', attributes: [] },
      },
      {
        id: 'Airport:Thijs',
        label: 'Airport:Thijs',
        type: 'nodeEdge',
        source: 'Airport',
        target: 'Thijs',
        arrowHeadType: ArrowHeadType.Arrow,
        data: { d: '', attributes: [] },
      },
    ];
    const result = edgeUseCase.sortEdgeArray(outgoingedges, ['Thijs', 'Staff'], false);
    const expected: Edge[] = [
      {
        arrowHeadType: ArrowHeadType.Arrow,
        data: {
          d: '',
          attributes: [],
        },
        id: 'Airport:Thijs',
        label: 'Airport:Thijs',
        type: 'nodeEdge',
        source: 'Airport',
        target: 'Thijs',
      },
      {
        arrowHeadType: ArrowHeadType.Arrow,
        data: {
          d: '',
          attributes: [],
        },
        id: 'Airport:Staff',
        label: 'Airport:Staff',
        type: 'nodeEdge',
        source: 'Airport',
        target: 'Staff',
      },
    ];

    expect(result).toEqual(expected);
    expect(result).not.toContain(null);
    expect(result.length).toBe(2);
  });
});
