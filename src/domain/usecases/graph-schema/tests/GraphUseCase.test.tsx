/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { schema } from '../../../../data/mock-data/graph-schema/MockGraph';
import { ArrowHeadType, Edge, Node } from 'react-flow-renderer';
import GraphUseCase from '../GraphUseCase';

describe('GraphUseCase', () => {
  beforeEach(() => jest.resetModules());

  it('should create a graph from input data', () => {
    const graphUseCase = new GraphUseCase();
    const expected = { nodes, edges, selfEdges };
    const receivedschema = graphUseCase.createGraphFromInputData(schema);

    expect(receivedschema).toEqual(expected);
  });

  it('should make edge', () => {
    const graphUseCase = new GraphUseCase();
    graphUseCase.makeEntity(schema.nodes[1].name, schema.nodes[1].attributes);
    graphUseCase.makeEntity(schema.nodes[4].name, schema.nodes[4].attributes);
    graphUseCase.makeRelation(schema.edges[0], schema.edges[0].attributes);
    const expected: Edge = {
      id: 'flights',
      label: 'Airport2:Airport',
      type: 'nodeEdge',
      source: 'Airport2',
      target: 'Airport',
      arrowHeadType: ArrowHeadType.Arrow,
      data: {
        collection: 'flights',
        created: false,
        d: '',
        edgeCount: 0,
        attributes: [
          { name: 'arrivalTime', type: 'int' },
          { name: 'departureTime', type: 'int' },
        ],
      },
    };
    expect(graphUseCase.edges).toEqual([expected]);
  });

  it('should make node', () => {
    const graphUseCase = new GraphUseCase();
    const nodes = graphUseCase.makeEntity('Thijs', []);
    const expected: Node = {
      type: 'entity',
      id: 'Thijs',
      position: { x: 0, y: 0 },
      data: {
        attributes: [],
        connectedRatio: 0,
        handles: [],
        nodeCount: 0,
        summedNullAmount: 0,
        width: 165,
        height: 30,
      },
    };

    expect(nodes).toEqual(expected);
    expect(graphUseCase.nodes).toEqual([expected]);
  });
});

// result nodes
const nodes: Node[] = [
  {
    type: 'entity',
    id: 'Thijs',
    position: { x: 0, y: 0 },
    data: {
      attributes: [],
      connectedRatio: 0,
      handles: [],
      nodeCount: 0,
      summedNullAmount: 0,
      width: 165,
      height: 30,
    },
  },
  {
    type: 'entity',
    id: 'Airport',
    position: { x: 0, y: 0 },
    data: {
      attributes: [
        { name: 'city', type: 'string' },
        { name: 'vip', type: 'bool' },
        { name: 'state', type: 'string' },
      ],
      connectedRatio: 0,
      handles: [],
      nodeCount: 0,
      summedNullAmount: 0,
      width: 165,
      height: 30,
    },
  },
  {
    type: 'entity',
    id: 'Airport2',
    position: { x: 0, y: 0 },
    data: {
      attributes: [
        { name: 'city', type: 'string' },
        { name: 'vip', type: 'bool' },
        { name: 'state', type: 'string' },
      ],
      connectedRatio: 0,
      handles: [],
      nodeCount: 0,
      summedNullAmount: 0,
      width: 165,
      height: 30,
    },
  },
  {
    type: 'entity',
    id: 'Plane',
    position: { x: 0, y: 0 },
    data: {
      attributes: [
        { name: 'type', type: 'string' },
        { name: 'maxFuelCapacity', type: 'int' },
      ],
      connectedRatio: 0,
      handles: [],
      nodeCount: 0,
      summedNullAmount: 0,
      width: 165,
      height: 30,
    },
  },
  {
    type: 'entity',
    id: 'Staff',
    position: { x: 0, y: 0 },
    data: {
      attributes: [],
      connectedRatio: 0,
      handles: [],
      nodeCount: 0,
      summedNullAmount: 0,
      width: 165,
      height: 30,
    },
  },
  {
    type: 'entity',
    id: 'Unconnected',
    position: { x: 0, y: 0 },
    data: {
      attributes: [],
      connectedRatio: 0,
      handles: [],
      nodeCount: 0,
      summedNullAmount: 0,
      width: 165,
      height: 30,
    },
  },
];

// result attributes
const attributes: Node[] = [];

// result edges
const edges: Edge[] = [
  {
    id: 'flights',
    label: 'Airport2:Airport',
    type: 'nodeEdge',
    source: 'Airport2',
    target: 'Airport',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [
        { name: 'arrivalTime', type: 'int' },
        { name: 'departureTime', type: 'int' },
      ],
    },
  },
  {
    id: 'flights',
    label: 'Airport:Staff',
    type: 'nodeEdge',
    source: 'Airport',
    target: 'Staff',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [{ name: 'salary', type: 'int' }],
    },
  },
  {
    id: 'flights',
    label: 'Plane:Airport',
    type: 'nodeEdge',
    source: 'Plane',
    target: 'Airport',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [],
    },
  },
  {
    id: 'flights',
    label: 'Airport:Thijs',
    type: 'nodeEdge',
    source: 'Airport',
    target: 'Thijs',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [{ name: 'hallo', type: 'string' }],
    },
  },
  {
    id: 'flights',
    label: 'Thijs:Airport',
    type: 'nodeEdge',
    source: 'Thijs',
    target: 'Airport',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [{ name: 'hallo', type: 'string' }],
    },
  },
  {
    id: 'flights',
    label: 'Staff:Plane',
    type: 'nodeEdge',
    source: 'Staff',
    target: 'Plane',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [{ name: 'hallo', type: 'string' }],
    },
  },
  {
    id: 'flights',
    label: 'Staff:Airport2',
    type: 'nodeEdge',
    source: 'Staff',
    target: 'Airport2',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [{ name: 'hallo', type: 'string' }],
    },
  },
  {
    id: 'flights',
    label: 'Airport2:Plane',
    type: 'nodeEdge',
    source: 'Airport2',
    target: 'Plane',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      collection: 'flights',
      created: false,
      d: '',
      edgeCount: 0,
      attributes: [{ name: 'hallo', type: 'string' }],
    },
  },
];

const selfEdges: Edge[] = [
  {
    id: 'flights',
    source: 'Airport',
    target: 'Airport',
    type: 'selfEdge',
    label: 'Airport:Airport',
    data: {
      attributes: [{ name: 'test', type: 'string' }],
      d: '',
      edgeCount: 0,
      created: false,
      collection: 'flights',
    },
    arrowHeadType: ArrowHeadType.Arrow,
  },
];
