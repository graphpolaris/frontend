/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Node } from 'react-flow-renderer';
import NodeUseCase from '../NodeUseCase';

/** Testsuite for the node use case */
describe('NodeUseCase', () => {
  beforeEach(() => jest.resetModules());

  it('should set node positions', () => {
    const nodeUseCase = new NodeUseCase();

    const expectednodes: Node[] = [
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 0, y: 0 },
        data: {
          attributes: [],
          toggleNodeQualityPopup: toggleNodeQualityPopup,
          toggleAttributeAnalyticsPopupMenu: toggleAttributeAnalyticsPopupMenu,
        },
      },
      {
        type: 'entity',
        id: 'Airport',
        position: { x: 0, y: nodeUseCase.verticalDistanceBetweenEntityNodes },
        data: {
          attributes: [],
          toggleNodeQualityPopup: toggleNodeQualityPopup,
          toggleAttributeAnalyticsPopupMenu: toggleAttributeAnalyticsPopupMenu,
        },
      },
      {
        type: 'entity',
        id: 'Airport2',
        position: { x: 0, y: nodeUseCase.verticalDistanceBetweenEntityNodes * 2 },
        data: {
          attributes: [],
          toggleNodeQualityPopup: toggleNodeQualityPopup,
          toggleAttributeAnalyticsPopupMenu: toggleAttributeAnalyticsPopupMenu,
        },
      },
      {
        type: 'entity',
        id: 'Plane',
        position: { x: 0, y: nodeUseCase.verticalDistanceBetweenEntityNodes * 3 },
        data: {
          attributes: [],
          toggleNodeQualityPopup: toggleNodeQualityPopup,
          toggleAttributeAnalyticsPopupMenu: toggleAttributeAnalyticsPopupMenu,
        },
      },
      {
        type: 'entity',
        id: 'Staff',
        position: { x: 0, y: nodeUseCase.verticalDistanceBetweenEntityNodes * 4 },
        data: {
          attributes: [],
          toggleNodeQualityPopup: toggleNodeQualityPopup,
          toggleAttributeAnalyticsPopupMenu: toggleAttributeAnalyticsPopupMenu,
        },
      },
    ];

    const draworder: Node[] = [
      {
        type: 'entity',
        id: 'Thijs',
        position: { x: 0, y: 0 },
        data: { attributes: [] },
      },
      {
        type: 'entity',
        id: 'Airport',
        position: { x: 0, y: 0 },
        data: { attributes: [] },
      },
      {
        type: 'entity',
        id: 'Airport2',
        position: { x: 0, y: 0 },
        data: { attributes: [] },
      },
      {
        type: 'entity',
        id: 'Plane',
        position: { x: 0, y: 0 },
        data: { attributes: [] },
      },
      {
        type: 'entity',
        id: 'Staff',
        position: { x: 0, y: 0 },
        data: { attributes: [] },
      },
    ];
    const returneddraworder = nodeUseCase.setEntityNodePosition(
      draworder,
      { x: 0, y: 0 },
      toggleNodeQualityPopup,
      toggleAttributeAnalyticsPopupMenu,
    );

    function toggleNodeQualityPopup(id: string): void {}
    function toggleAttributeAnalyticsPopupMenu(id: string): void {}

    expect(expectednodes).toEqual(returneddraworder);
  });
});
