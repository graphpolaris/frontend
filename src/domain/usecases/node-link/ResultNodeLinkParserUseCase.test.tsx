/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import ResultNodeLinkParserUseCase from './ResultNodeLinkParserUseCase';
import mockQueryResults from '../../../data/mock-data/query-result/mockQueryResults';
import { GraphType } from '../../entity/node-link/structures/Types';
import cd2ndChamber from '../../../data/mock-data/ml-node-link/cd-2ndChamber';
import lpLotrSmall from '../../../data/mock-data/ml-node-link/lp-lotrSmall';
import spLotrBig from '../../../data/mock-data/ml-node-link/sp-lotrBig';
import big2ndChamberQueryResult from '../../../data/mock-data/query-result/big2ndChamberQueryResult';

/** Testsuite to test the result node-link parser */
describe('websocketResultNodeLinkParserUseCase', () => {
  // Reset all modules between tests as state of module could interfere with test
  beforeEach(() => {
    jest.resetModules();
    websocketResultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  });

  let websocketResultNodeLinkParserUseCase: ResultNodeLinkParserUseCase;

  it('should parse a query response containing only nodes', () => {
    // Run the parse method on the new york response
    let mlData: boolean = websocketResultNodeLinkParserUseCase.checkIfMLResult(
      JSON.parse(mockQueryResults.newYorkResponse).values,
    );
    let parsedResult: GraphType = websocketResultNodeLinkParserUseCase.parseQueryResult(
      JSON.parse(mockQueryResults.newYorkResponse).values,
      mlData,
    );

    // Assert that there are 6 nodes
    expect(parsedResult.nodes.length).toBe(6);

    // Assert that there are 0 links
    expect(parsedResult.links.length).toBe(0);

    // Assert that the parsed result is equal to the expected result
    expect(parsedResult).toEqual(mockQueryResults.newYorkParsedResponse);
  });

  it('should parse a query response containing nodes and links', () => {
    // Run the parse method on the new york -> san francisco response
    let mlData: boolean = websocketResultNodeLinkParserUseCase.checkIfMLResult(
      JSON.parse(mockQueryResults.newYorkToSanFranciscoResponse).values,
    );
    let parsedResult: GraphType = websocketResultNodeLinkParserUseCase.parseQueryResult(
      JSON.parse(mockQueryResults.newYorkToSanFranciscoResponse).values,
      mlData,
    );

    // Assert that there are two nodes
    expect(parsedResult.nodes.length).toBe(2);

    // Assert that these nodes are equal
    expect(parsedResult.nodes).toEqual(mockQueryResults.newYorkToSanFranciscoExpectedNodes);
  });
});
describe('Detecting the right kind of data', () => {
  let resultNodeLinkParserUseCase: ResultNodeLinkParserUseCase;
  beforeEach(() => {
    jest.resetModules();
    resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  });
  it('Should detect communityDetection and nothing else', () => {
    let parsedResult: GraphType = resultNodeLinkParserUseCase.parseQueryResult(cd2ndChamber);
    expect(parsedResult.communityDetection).toBe(true);
    expect(parsedResult.linkPrediction).toBe(false);
    expect(parsedResult.shortestPath).toBe(false);
  });
  it('Should detect linkPredictionData and nothing else', () => {
    let parsedResult: GraphType = resultNodeLinkParserUseCase.parseQueryResult(lpLotrSmall);
    expect(parsedResult.communityDetection).toBe(false);
    expect(parsedResult.linkPrediction).toBe(true);
    expect(parsedResult.shortestPath).toBe(false);
  });
  it('Should detect shortestPath and nothing else', () => {
    let parsedResult: GraphType = resultNodeLinkParserUseCase.parseQueryResult(spLotrBig);
    expect(parsedResult.communityDetection).toBe(false);
    expect(parsedResult.linkPrediction).toBe(false);
    expect(parsedResult.shortestPath).toBe(true);
  });
  it('Should detect nothing', () => {
    let parsedResult: GraphType =
      resultNodeLinkParserUseCase.parseQueryResult(big2ndChamberQueryResult);
    expect(parsedResult.communityDetection).toBe(false);
    expect(parsedResult.linkPrediction).toBe(false);
    expect(parsedResult.shortestPath).toBe(false);
  });
});
