/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { cluster } from 'd3-v6';
import { extract } from 'pixi.js';
import DatabaseHolder from '../../entity/navbar/DatabaseHolder';
import { GraphType, LinkType, NodeType } from '../../entity/node-link/structures/Types';
import { NodeLinkResultType } from '../../entity/query-result/structures/NodeLinkResultType';
import ParseToUniqueEdges from '../ParseToUniqueEdgesUseCase';
/** ResultNodeLinkParserUseCase implements methods to parse and translate websocket messages from the backend into a GraphType. */
export default class ResultNodeLinkParserUseCase {
  /**
   * Parse a websocket message containing a query result into a node link GraphType.
   * @param {any} queryResult An incoming query result from the websocket.
   * @returns {GraphType} A node-link graph containing the nodes and links for the diagram.
   */
  public parseQueryResult(queryResult: NodeLinkResultType): GraphType {
    let nodes: NodeType[] = [];
    let typeDict: { [key: string]: number } = {};
    // Counter for the types
    let counter = 1;
    // Entry to keep track of the number of machine learning clusters
    let numberOfMlClusters = 0;

    let communityDetectionInResult = false;
    let shortestPathInResult = false;
    let linkPredictionInResult = false;

    for (let i = 0; i < queryResult.nodes.length; i++) {
      // Assigns a group to every entity type for color coding
      let nodeId = queryResult.nodes[i].id + '/';
      let entityType = nodeId.split('/')[0];

      // The preferred text to be shown on top of the node
      let preferredText = nodeId;
      let typeNumber = 1;

      // Check if entity is already seen by the dictionary
      if (entityType in typeDict) typeNumber = typeDict[entityType];
      else {
        typeDict[entityType] = counter;
        typeNumber = counter;
        counter++;
      }

      // Check to see if node has a "naam" attribute and set prefText to it
      if (queryResult.nodes[i].attributes.name != undefined)
        preferredText = queryResult.nodes[i].attributes.name;
      if (queryResult.nodes[i].attributes.label != undefined)
        preferredText = queryResult.nodes[i].attributes.label;
      if (queryResult.nodes[i].attributes.naam != undefined)
        preferredText = queryResult.nodes[i].attributes.naam;

      let data = {
        id: queryResult.nodes[i].id,
        attributes: queryResult.nodes[i].attributes,
        type: typeNumber,
        displayInfo: preferredText,
      };

      let mlExtra = {};
      if (queryResult.nodes[i].mldata && typeof queryResult.nodes[i].mldata != 'number') {
        mlExtra = { shortestPathData: queryResult.nodes[i].mldata as Record<string, string[]> };
        shortestPathInResult = true;
      } else if (typeof queryResult.nodes[i].mldata == 'number') {
        // mldata + 1 so you dont get 0, which is interpreted as 'undefined'
        let numberOfCluster = (queryResult.nodes[i].mldata as number) + 1;
        mlExtra = {
          cluster: numberOfCluster,
          clusterAccoringToMLData: numberOfCluster,
        };
        communityDetectionInResult = true;
        if (numberOfCluster > numberOfMlClusters) {
          numberOfMlClusters = numberOfCluster;
        }
      }

      // Add mlExtra to the node if necessary
      data = { ...data, ...mlExtra };
      nodes.push(data);
    }
    // Filter unique edges and transform to LinkTypes
    // List for all links
    let links: LinkType[] = [];
    // Parse ml edges
    if (queryResult.mlEdges != undefined) {
      let uniqueMLEdges = ParseToUniqueEdges.parse(queryResult.mlEdges, true);
      links = uniqueMLEdges.map((edge) => {
        return { source: edge.from, target: edge.to, value: edge.count, mlEdge: true };
      });
      linkPredictionInResult = true;
    }

    // Parse normal edges
    let uniqueEdges = ParseToUniqueEdges.parse(queryResult.edges, false);
    for (let i = 0; i < uniqueEdges.length; i++) {
      let toAdd: LinkType = {
        source: uniqueEdges[i].from,
        target: uniqueEdges[i].to,
        value: uniqueEdges[i].count,
        mlEdge: false,
      };
      links.push(toAdd);
    }

    //TODO: is this in use?
    const maxCount = links.reduce(
      (previousValue, currentValue) =>
        currentValue.value > previousValue ? currentValue.value : previousValue,
      -1,
    );
    //TODO: is this in use?
    // Scale the value from 0 to 50
    const maxLineWidth = 50;
    if (maxCount > maxLineWidth) {
      links.forEach((link) => {
        link.value = (link.value / maxCount) * maxLineWidth;
        link.value = link.value < 1 ? 1 : link.value;
      });
    }

    // Graph to be returned
    let toBeReturned = {
      nodes: nodes,
      links: links,
      linkPrediction: linkPredictionInResult,
      shortestPath: shortestPathInResult,
      communityDetection: communityDetectionInResult,
    };

    // If query with community detection; add number of clusters to the graph
    let numberOfClusters = {
      numberOfMlClusters: numberOfMlClusters,
    };
    if (communityDetectionInResult) {
      toBeReturned = { ...toBeReturned, ...numberOfClusters };
    }

    return toBeReturned;
  }
}
