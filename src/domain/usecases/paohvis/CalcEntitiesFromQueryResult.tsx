/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  getGroupName,
  NodeLinkResultType,
} from '../../entity/query-result/structures/NodeLinkResultType';

/**
 * This calculates all entities from the query result.
 * @param {NodeLinkResultType} message This is the message with all the information about the entities and relations.
 * @returns {string[]} All names of the entities which are in the query result.
 */
export default function calcEntitiesFromQueryResult(message: NodeLinkResultType): string[] {
  const entityTypesFromQueryResult: string[] = [];
  message.nodes.forEach((node) => {
    const group = getGroupName(node);
    if (!entityTypesFromQueryResult.includes(group)) entityTypesFromQueryResult.push(group);
  });
  return entityTypesFromQueryResult;
}
