/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Schema } from '../../entity/graph-schema/structures/InputDataTypes';
import {
  AttributeNames,
  EntitiesFromSchema,
  RelationsFromSchema,
} from '../../entity/paohvis/structures/Types';

/** Use case for retrieving entity names, relation names and attribute names from a schema result. */
export default class CalcEntityAttrAndRelNamesFromSchemaUseCase {
  /**
   * Takes a schema result and calculates all the entity names, and relation names and attribute names per entity.
   * Used by PAOHvis to show all possible options to choose from when adding a new PAOHvis visualisation or when filtering.
   * @param {SchemaResultType} schemaResult A new schema result from the backend.
   * @returns {EntitiesFromSchema} All entity names, and relation names and attribute names per entity.
   */
  public static calculateAttributesAndRelations(schemaResult: Schema): EntitiesFromSchema {
    const attributesPerEntity: Record<string, AttributeNames> = this.calculateAttributes(
      schemaResult,
    );
    const relationsPerEntity: Record<string, string[]> = this.calculateRelations(schemaResult);

    return {
      entityNames: schemaResult.nodes.map((node) => node.name),
      attributesPerEntity,
      relationsPerEntity,
    };
  }

  /**
   * Takes a schema result and calculates all the attribute names per entity.
   * @param {SchemaResultType} schemaResult A new schema result from the backend.
   * @returns {Record<string, AttributeNames>} All attribute names per entity.
   */
  public static calculateAttributes(schemaResult: Schema): Record<string, AttributeNames> {
    const attributesPerEntity: Record<string, AttributeNames> = {};
    // Go through each entity.
    schemaResult.nodes.forEach((node) => {
      // Extract the attribute names per datatype for each entity.
      const textAttributeNames: string[] = [];
      const boolAttributeNames: string[] = [];
      const numberAttributeNames: string[] = [];
      node.attributes.forEach((attr) => {
        if (attr.type == 'string') textAttributeNames.push(attr.name);
        else if (attr.type == 'int' || attr.type == 'float') numberAttributeNames.push(attr.name);
        else boolAttributeNames.push(attr.name);
      });

      // Create a new object with the arrays with attribute names per datatype.
      attributesPerEntity[node.name] = {
        textAttributeNames,
        boolAttributeNames,
        numberAttributeNames,
      };
    });
    return attributesPerEntity;
  }

  /**
   * Takes a schema result and calculates all the relation names per entity.
   * @param {SchemaResultType} schemaResult A new schema result from the backend.
   * @returns {Record<string, AttributeNames>} All relation (from and to) names per entity.
   */
  public static calculateRelations(schemaResult: Schema): Record<string, string[]> {
    const relationsPerEntity: Record<string, string[]> = {};
    // Go through each relation.
    schemaResult.edges.forEach((edge) => {
      // Extract the from-node-name (collection name) from every relation.
      if (relationsPerEntity[edge.from]) relationsPerEntity[edge.from].push(edge.collection);
      else relationsPerEntity[edge.from] = [edge.collection];
      // Extract the to-node-name (collection name) from every relation.
      if (relationsPerEntity[edge.to]) relationsPerEntity[edge.to].push(edge.collection);
      else relationsPerEntity[edge.to] = [edge.collection];
    });
    return relationsPerEntity;
  }

  /**
   * Takes a schema result and calculates all the relation collection names, and relation names and attribute names per relation.
   * Used by PAOHvis to show all possible options to choose from when adding a new PAOHvis visualisation or when filtering.
   * @param {SchemaResultType} schemaResult A new schema result from the backend.
   * @returns {EntitiesFromSchema} All entity names, and relation names and attribute names per entity.
   */
  public static calculateAttributesFromRelation(schemaResult: Schema): RelationsFromSchema {
    const relationCollections: string[] = [];
    const attributesPerRelation: Record<string, AttributeNames> = {};
    const nameOfCollectionPerRelation: Record<string, string> = {};
    // Go through each relation.
    schemaResult.edges.forEach((edge) => {
      if (!nameOfCollectionPerRelation[edge.collection]) {
        relationCollections.push(edge.collection);
        nameOfCollectionPerRelation[edge.collection] = edge.name;
      }
      // Extract the attribute names per datatype for each relation.
      const textAttributeNames: string[] = [];
      const boolAttributeNames: string[] = [];
      const numberAttributeNames: string[] = [];
      edge.attributes.forEach((attr) => {
        if (attr.type == 'string') textAttributeNames.push(attr.name);
        else if (attr.type == 'int' || attr.type == 'float') numberAttributeNames.push(attr.name);
        else boolAttributeNames.push(attr.name);
      });

      // Create a new object with the arrays with attribute names per datatype.
      attributesPerRelation[edge.collection] = {
        textAttributeNames,
        boolAttributeNames,
        numberAttributeNames,
      };
    });
    return {
      relationCollection: relationCollections,
      relationNames: nameOfCollectionPerRelation,
      attributesPerRelation: attributesPerRelation,
    };
  }
}
