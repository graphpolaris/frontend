/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Node } from '../../entity/query-result/structures/NodeLinkResultType';
import {
  ValueType,
  HyperEdge,
  HyperEdgeRange,
  NodeOrder,
  PaohvisNodeOrder,
} from '../../entity/paohvis/structures/Types';

/**
 * This class is responsible for sorting the data for the ToPaohvisDataParser.
 */
export default class SortUseCase {
  /**
   * Sorts the given HyperEdgeRanges by their labels and sorts the indices for all HyperEdges in the HyperEdgeRanges.
   * @param hyperEdgeRanges that should be sorted.
   * @param type of the HyperEdgeRange labels.
   */
  public static sortHyperEdgeRanges(hyperEdgeRanges: HyperEdgeRange[], type: ValueType): void {
    this.sortHyperEdgeIndices(hyperEdgeRanges);
    this.sortHyperEdgeRangesByLabels(hyperEdgeRanges, type);
  }

  /**
   * Sorts the given HyperEdgeRanges by their labels.
   * They are sorted alphabetically when the labels are strings.
   * They are sorted from lowest to highest when the labels are numbers.
   * @param hyperEdgeRanges that should be sorted.
   * @param type of the HyperEdgeRange labels.
   */
  private static sortHyperEdgeRangesByLabels(
    hyperEdgeRanges: HyperEdgeRange[],
    type: ValueType,
  ): void {
    //sort all hyperedgeranges text
    if (type == ValueType.number) {
      //from lowest to highest
      hyperEdgeRanges.sort((a, b) => compareNumericalLabels(a.rangeText, b.rangeText));
    } else {
      //alphabetical
      hyperEdgeRanges.sort((a, b) => a.rangeText.localeCompare(b.rangeText));
    }
  }

  /**
   * Sorts all indices for each HyperEdge in the HyperEdgeRanges from lowest to highest.
   * @param hyperEdgeRanges  that should be sorted.
   */
  private static sortHyperEdgeIndices(hyperEdgeRanges: HyperEdgeRange[]): void {
    hyperEdgeRanges.forEach((hyperEdgeRange) =>
      hyperEdgeRange.hyperEdges.forEach((hyperEdge) => hyperEdge.indices.sort((n1, n2) => n1 - n2)),
    );
  }

  /**
   * Sort the nodes in the given order.
   * @param nodeOrder is the order in which the nodes should be sorted.
   * @param nodes are the nodes that will be sorted
   * @param hyperEdgeDegree is the dictionary where you can find how many edges connected from the node.
   */
  public static sortNodes(
    nodeOrder: PaohvisNodeOrder,
    nodes: Node[],
    hyperEdgeDegree: Record<string, number>,
  ): void {
    switch (nodeOrder.orderBy) {
      //sort nodes on their degree (# number of hyperedges) (entities with most hyperedges first)
      case NodeOrder.degree:
        //TODO: sort when nodes have the same amount of edges
        nodes.sort((node1, node2) => {
          return hyperEdgeDegree[node2.id] - hyperEdgeDegree[node1.id];
        });
        break;
      //sort nodes on their id alphabetically
      case NodeOrder.alphabetical:
        nodes.sort((node1, node2) => node1.id.localeCompare(node2.id));
        break;
      default:
        throw new Error('This node order does not exist');
    }
    //reverse order
    if (nodeOrder.isReverseOrder) nodes.reverse();
  }

  /**
   * This is used to sort hyperEdges by appearance on y-axis and length of hyperEdges.
   * @param hyperEdgeRanges that should be sorted.
   */
  public static sortHyperEdges(hyperEdgeRanges: HyperEdgeRange[]): void {
    hyperEdgeRanges.forEach((hyperEdgeRange) => {
      hyperEdgeRange.hyperEdges.sort(this.compareByLineLength);
      hyperEdgeRange.hyperEdges.sort(this.compareByAppearanceOnYAxis);
    });
  }

  /**
   * This is used to compare hyperedges by their linelength in the table.
   * Can be used to sort hyperedges from hyperedges with a shorter lineLength to longer lineLength.
   * @param hyperEdge1 first hyperedge to compare
   * @param hyperEdge2 second hyperedge to compare
   * @returns {number} a negative value if the first argument is less than the second argument, zero if they're equal, and a positive value otherwise.
   */
  private static compareByLineLength(hyperEdge1: HyperEdge, hyperEdge2: HyperEdge): number {
    const indices1 = hyperEdge1.indices;
    const lineLength1 = indices1[indices1.length - 1] - indices1[0];

    const indices2 = hyperEdge2.indices;
    const lineLength2 = indices2[indices2.length - 1] - indices2[0];
    return lineLength1 - lineLength2;
  }

  /**
   * This is used to compare hyperedges by their appearance on the y-axis in the table.
   * Can be used to sort hyperedges so that the hyperedges that appear higher on the y-axis should appear earlier on the x-axis.
   * @param hyperEdge1 first hyperedge to compare
   * @param hyperEdge2 second hyperedge to compare
   * @returns {number} a negative value if the first argument is less than the second argument, zero if they're equal, and a positive value otherwise.
   */
  private static compareByAppearanceOnYAxis(hyperEdge1: HyperEdge, hyperEdge2: HyperEdge): number {
    const start1 = hyperEdge1.indices[0];
    const start2 = hyperEdge2.indices[0];
    return start1 - start2;
  }
}

/**
 * This is used to compare numerical labels.
 * Can be used to sort numerical labels from the label with the lowest number to the label with the highest number.
 * @param numericalLabel1 first numerical label to compare
 * @param numericalLabel2 second numerical label to compare
 * @returns {number} a negative value if the first argument is less than the second argument, zero if they're equal, and a positive value otherwise.
 */
function compareNumericalLabels(numericalLabel1: string, numericalLabel2: string): number {
  return parseFloat(numericalLabel1) - parseFloat(numericalLabel2);
}
