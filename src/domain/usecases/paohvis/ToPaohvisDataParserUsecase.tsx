/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  NodeLinkResultType,
  Node,
  Link,
  AxisType,
  getGroupName,
} from '../../entity/query-result/structures/NodeLinkResultType';
import {
  AttributeOrigin,
  ValueType,
  HyperEdge,
  HyperEdgeRange,
  PaohvisAxisInfo,
  PaohvisNodeInfo,
  PaohvisNodeOrder,
  PaohvisData,
  Relation,
  PaohvisFilters,
} from '../../entity/paohvis/structures/Types';
import { getWidthOfText, uniq } from '../../../presentation/util/paohvis/utils';
import style from '../../../presentation/view/result-visualisations/paohvis/PaohvisComponent.module.scss';
import AttributeFilterUsecase, { filterUnusedEdges, getIds } from './AttributesFilterUseCase';
import SortUseCase from './SortUseCase';

type Index = number;

/**
 * This parser is used to parse the incoming query result to the format that's needed to make the Paohvis table.
 */
export default class ToPaohvisDataParserUseCase {
  private queryResult: NodeLinkResultType;
  private xAxisNodeGroup: string;
  private yAxisNodeGroup: string;
  private paohvisFilters: PaohvisFilters;

  public constructor(queryResult: NodeLinkResultType) {
    this.queryResult = queryResult;
    this.xAxisNodeGroup = '';
    this.yAxisNodeGroup = '';
    this.paohvisFilters = { nodeFilters: [], edgeFilters: [] };
  }
  /**
   * Parses query results to the format that's needed to make a Paohvis table.
   * @param axisInfo is the information that's needed to parse everything to the correct axis.
   * @param nodeOrder is the order in which the nodes should be parsed.
   * @returns the information that's needed to make a Paohvis table.
   */
  public parseQueryResult(axisInfo: PaohvisAxisInfo, nodeOrder: PaohvisNodeOrder): PaohvisData {
    this.setAxesNodeGroups(axisInfo);

    const filteredData: NodeLinkResultType = AttributeFilterUsecase.applyFilters(
      this.queryResult,
      this.paohvisFilters,
    );

    // filter unnecessary node groups and relations
    const nodes: Node[] = filteredData.nodes.filter((node) => {
      const nodeType = getGroupName(node);
      return nodeType == this.xAxisNodeGroup || nodeType == this.yAxisNodeGroup;
    });
    const edges: Link[] = filteredData.edges.filter(
      (edge) => getGroupName(edge) == axisInfo.relation.collection,
    );

    // get hyperEdgeDegree
    const hyperEdgeDegree: Record<string, number> =
      ToPaohvisDataParserUseCase.GetHyperEdgeDegreeDict(
        nodes,
        edges,
        axisInfo.isYAxisEntityEqualToRelationFrom,
      );

    //parse nodes
    const rowInfo: PaohvisNodeInfo = ToPaohvisDataParserUseCase.parseNodes(
      nodes,
      hyperEdgeDegree,
      nodeOrder,
      this.yAxisNodeGroup,
      this.xAxisNodeGroup,
    );
    const maxLabelWidth = calcMaxRowLabelWidth(rowInfo.rowLabels);

    //parse hyperEdges
    const filteredEdges = filterUnusedEdges(getIds(nodes), edges);
    const resultHyperEdgeRanges = ToPaohvisDataParserUseCase.parseHyperEdgeRanges(
      nodes,
      filteredEdges,
      axisInfo,
      rowInfo,
    );
    SortUseCase.sortHyperEdges(resultHyperEdgeRanges);

    return {
      rowLabels: rowInfo.rowLabels,
      hyperEdgeRanges: resultHyperEdgeRanges,
      maxRowLabelWidth: maxLabelWidth,
    };
  }

  /**
   * Sets the x-axis and y-axis node groups from the given PaohvisAxisInfo in the parser.
   * @param axisInfo is the new PaohvisAxisInfo that will be used.
   */
  private setAxesNodeGroups(axisInfo: PaohvisAxisInfo): void {
    const relation: Relation = axisInfo.relation;
    if (axisInfo.isYAxisEntityEqualToRelationFrom) {
      this.xAxisNodeGroup = relation.to;
      this.yAxisNodeGroup = relation.from;
    } else {
      this.xAxisNodeGroup = relation.from;
      this.yAxisNodeGroup = relation.to;
    }
  }

  /**
   * This parses the nodes to get the information that's needed to parse the hyperedges.
   * @param nodes are the nodes from the query result.
   * @param hyperEdgeDegree is the dictionary where you can find how many edges connected from the node.
   * @param nodeOrder is the order in which the nodes should be sorted.
   * @param yAxisNodeType is the type of nodes that should be on the y-axis.
   * @param xAxisNodeType is the type of nodes that should be on the x-axis.
   * @returns the information that's needed to parse the hyperedges.
   */
  private static parseNodes(
    nodes: Node[],
    hyperEdgeDegree: Record<string, number>,
    nodeOrder: PaohvisNodeOrder,
    yAxisNodeType: string,
    xAxisNodeType: string,
  ): PaohvisNodeInfo {
    const rowNodes = filterRowNodes(nodes, hyperEdgeDegree, yAxisNodeType);
    SortUseCase.sortNodes(nodeOrder, rowNodes, hyperEdgeDegree);
    const rowLabels = getIds(rowNodes);

    //make dictionary for finding the index of a row
    const yNodesIndexDict: Record<string, number> = {};
    let yNodeIndexCounter = 0;
    for (let i = 0; i < rowNodes.length; i++) {
      yNodesIndexDict[rowNodes[i].id] = yNodeIndexCounter;
      yNodeIndexCounter++;
    }

    const xNodesAttributesDict: Record<string, any> = getXNodesAttributesDict(
      yAxisNodeType,
      xAxisNodeType,
      nodes,
    );
    return {
      rowLabels: rowLabels,
      xNodesAttributesDict: xNodesAttributesDict,
      yNodesIndexDict: yNodesIndexDict,
    };
  }

  /**
   * Makes a dictionary where you can find how many edges are connected to the nodes.
   * @param nodes are the nodes where the edges should be counted for.
   * @param edges should be used to count how many edges are connected to the nodes.
   * @param isEntityRelationFrom is to decide if you need to count the from's or the to's of the edge.
   * @returns a dictionary where you can find how many edges are connected to the nodes.
   */
  private static GetHyperEdgeDegreeDict(
    nodes: Node[],
    edges: Link[],
    isEntityRelationFrom: boolean,
  ): Record<string, number> {
    const hyperEdgeDegreeDict: Record<string, number> = {};

    //initialize dictionary
    nodes.forEach((node) => (hyperEdgeDegreeDict[node.id] = 0));

    //count node appearance frequencies
    edges.forEach((edge) => {
      if (isEntityRelationFrom) hyperEdgeDegreeDict[edge.from]++;
      else hyperEdgeDegreeDict[edge.to]++;
    });

    return hyperEdgeDegreeDict;
  }

  /**
   * Parses the edges to make hyperedge ranges for the Paohvis table.
   * @param nodes the unused nodes should already be filtered out.
   * @param edges the unused edges should already be filtered out.
   * @param axisInfo is the information that's needed to parse the edges to their respective hyperedge range.
   * @param rowInfo is the information about the nodes that's needed to parse the edges to their respective hyperedge range.
   * @returns the hyperedge ranges that will be used by the Paohvis table.
   */
  private static parseHyperEdgeRanges(
    nodes: Node[],
    edges: Link[],
    axisInfo: PaohvisAxisInfo,
    rowInfo: PaohvisNodeInfo,
  ): HyperEdgeRange[] {
    if (nodes.length == 0 || edges.length == 0) return [];

    const resultHyperEdgeRanges: HyperEdgeRange[] = [];

    //is used to find on which index of HyperEdgeRanges the edge should be added
    const attributeRangesIndexDict: Record<string, Index> = {};
    let attributeRangeCounter = 0;

    //is used to find the index to which hyperedge it belongs to (in the array hyperEdgeRanges[i].hyperEdges)
    //dict[hyperEdgeRangeLabel] gives a dict where you can find with dict[edge.to] the index where it belongs to
    const hyperEdgesDict: Record<string, Record<string, Index>> = {};

    const xAxisAttributeType: string = axisInfo.selectedAttribute.name;

    //is used to find the node attributes for the x-axis
    const xNodesAttributesDict: Record<string, any> = rowInfo.xNodesAttributesDict;

    //is used to find the index of nodes in rowLabels
    const yNodesIndexDict: Record<string, number> = rowInfo.yNodesIndexDict;

    let edgeDirection: string;
    let edgeDirectionOpposite: string;
    const isFromOnYAxis = axisInfo.isYAxisEntityEqualToRelationFrom;
    const collection = axisInfo.relation.collection;

    for (let i = 0; i < edges.length; i++) {
      const edge = edges[i];
      ({ edgeDirection, edgeDirectionOpposite } = getEdgeDirections(isFromOnYAxis, edge));

      let edgeIndexInResult: number = yNodesIndexDict[edgeDirection];

      // check if the chosen attribute is an attribute of the edge or the node
      let attribute: any;
      if (axisInfo.selectedAttribute.origin == AttributeOrigin.relation)
        attribute = edge.attributes[xAxisAttributeType];
      else attribute = xNodesAttributesDict[edgeDirectionOpposite][xAxisAttributeType];

      // if no edge attribute was selected, then all edges will be placed in one big hyperEdgeRange
      if (attribute == undefined) attribute = ValueType.noAttribute;

      if (attribute in attributeRangesIndexDict) {
        const rangeIndex = attributeRangesIndexDict[attribute];
        const targetHyperEdges = resultHyperEdgeRanges[rangeIndex].hyperEdges;
        const targetHyperEdgeIndex = hyperEdgesDict[attribute][edgeDirectionOpposite];

        if (targetHyperEdgeIndex != undefined) {
          // hyperedge group already exists so add edge to group
          targetHyperEdges[targetHyperEdgeIndex].indices.push(edgeIndexInResult);
        } else {
          // create new hyperedge group
          hyperEdgesDict[attribute][edgeDirectionOpposite] = targetHyperEdges.length;

          // 'add edge to new hyperedge group'
          targetHyperEdges.push({
            indices: [edgeIndexInResult],
            frequencies: newFrequencies(rowInfo.rowLabels),
            nameToShow: edgeDirectionOpposite,
          });
        }
      } else {
        // create new attribute range
        attributeRangesIndexDict[attribute] = attributeRangeCounter;
        attributeRangeCounter++;

        hyperEdgesDict[attribute] = {};
        hyperEdgesDict[attribute][edgeDirectionOpposite] = 0;

        let label: string;
        if (xAxisAttributeType != ValueType.noAttribute && xAxisAttributeType != '')
          label = attribute.toString();
        else label = 'No attribute was selected';
        const hyperEdge: HyperEdge = {
          indices: [edgeIndexInResult],
          frequencies: newFrequencies(rowInfo.rowLabels),
          nameToShow: edgeDirectionOpposite,
        };
        const hyperEdgeRange: HyperEdgeRange = {
          rangeText: label,
          hyperEdges: [hyperEdge],
        };
        resultHyperEdgeRanges.push(hyperEdgeRange);
      }
    }

    SortUseCase.sortHyperEdgeRanges(resultHyperEdgeRanges, axisInfo.selectedAttribute.type);

    //calc how many duplicate edges are in each hyperedge
    resultHyperEdgeRanges.forEach((hyperEdgeRange) => {
      hyperEdgeRange.hyperEdges.forEach((hyperedge) => {
        hyperedge.indices.forEach((index) => {
          hyperedge.frequencies[index] += 1;
        });
      });
    });

    //filter out duplicate indices from all hyperedges
    resultHyperEdgeRanges.forEach((hyperEdgeRange) => {
      hyperEdgeRange.hyperEdges.forEach((hyperedge) => {
        hyperedge.indices = uniq(hyperedge.indices);
      });
    });

    return resultHyperEdgeRanges;
  }

  /** Sets new PaohvisFilters. */
  public setPaohvisFilters(filters: PaohvisFilters): void {
    this.paohvisFilters = filters;
  }
}

/** Calculates the width that's needed for the rowlabels. */
function calcMaxRowLabelWidth(rowLabels: string[]) {
  const margin = 10;
  let maxLabelWidth = 0;
  rowLabels.forEach((rowLabel) => {
    const textWidth: number = getWidthOfText(
      rowLabel + '   ',
      style.tableFontFamily,
      style.tableFontSize,
      style.tableFontWeight,
    );
    if (textWidth > maxLabelWidth) maxLabelWidth = textWidth;
  });
  return maxLabelWidth + margin;
}

/** Gets a dictionary where you can find the attributes that belong to the nodes on teh x-axis.  */
function getXNodesAttributesDict(yAxisNodeType: string, xAxisNodeType: string, nodes: AxisType[]) {
  const resultXNodesAttributesDict: Record<string, any> = {};
  if (yAxisNodeType == xAxisNodeType)
    nodes.forEach((node) => (resultXNodesAttributesDict[node.id] = node.attributes));
  else
    nodes.forEach((node) => {
      if (getGroupName(node) == xAxisNodeType)
        resultXNodesAttributesDict[node.id] = node.attributes;
    });
  return resultXNodesAttributesDict;
}

/**
 * Gets which direction the edge will be read from. The direction can be read from:
 * 'from => to' or 'to => from'.
 */
function getEdgeDirections(isFromToRelation: boolean, edge: Link) {
  let edgeDirection: string;
  let edgeDirectionOpposite: string;
  if (isFromToRelation) {
    edgeDirection = edge.from;
    edgeDirectionOpposite = edge.to;
  } else {
    edgeDirection = edge.to;
    edgeDirectionOpposite = edge.from;
  }
  return { edgeDirection, edgeDirectionOpposite };
}

/** This makes an array filled with zeroes, which is used to count the frequencies of edges. */
function newFrequencies(rowLabels: string[]): number[] {
  return Array(rowLabels.length).fill(0);
}

/** Filters out nodes that have no edges and nodes that are not the specified type. */
function filterRowNodes(
  nodes: Node[],
  hyperEdgeDegree: Record<string, number>,
  rowNodeType: string,
): Node[] {
  return nodes.filter((node) => hyperEdgeDegree[node.id] > 0 && getGroupName(node) == rowNodeType);
}
