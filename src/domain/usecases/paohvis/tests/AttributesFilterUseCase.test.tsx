/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  animalMocks,
  sameAttributes,
  allAttributeTypes,
  caseSensitive,
} from '../../../../data/mock-data/paohvis/mockFilterData';
import { FilterInfo, PaohvisFilters } from '../../../entity/paohvis/structures/Types';
import { NodeLinkResultType } from '../../../entity/query-result/structures/NodeLinkResultType';
import AttributeFilterUsecase from '../AttributesFilterUseCase';

/** Testsuite for the Paohvis attribute filter use case */
describe('AttributeFilterUsecase, it should filter the attributes of the given nodes/edges with the given predicate and filter out the nodes/edges that have become unused', () => {
  const emptyFilters: PaohvisFilters = { nodeFilters: [], edgeFilters: [] };
  const emptyQueryResult: NodeLinkResultType = { nodes: [], edges: [] };

  const inputAnimals: NodeLinkResultType = animalMocks.queryResult;

  test('filtering with no filters, returns the query result untouched', () =>
    expect(AttributeFilterUsecase.applyFilters(inputAnimals, emptyFilters)).toEqual(inputAnimals));

  test('filters on empty query result, returns an empty query result back', () =>
    expect(
      AttributeFilterUsecase.applyFilters(emptyQueryResult, {
        nodeFilters: animalMocks.oneNodeFilter,
        edgeFilters: animalMocks.oneEdgeFilter,
      }),
    ).toEqual(emptyQueryResult));

  test('filters on 1 type of node', () =>
    expect(
      AttributeFilterUsecase.applyFilters(inputAnimals, {
        nodeFilters: animalMocks.oneNodeFilter,
        edgeFilters: [],
      }),
    ).toEqual(animalMocks.oneNodeFilterOutput));

  test('filters on 2 types of a node', () =>
    expect(
      AttributeFilterUsecase.applyFilters(inputAnimals, {
        nodeFilters: animalMocks.twoNodeTypesFilter,
        edgeFilters: [],
      }),
    ).toEqual(animalMocks.twoNodeTypesFilterOutput));

  test('filters on 1 type of a edge', () =>
    expect(
      AttributeFilterUsecase.applyFilters(inputAnimals, {
        nodeFilters: [],
        edgeFilters: animalMocks.oneEdgeFilter,
      }),
    ).toEqual(animalMocks.oneEdgeFilterOutput));

  test('filters on both nodes and edges', () =>
    expect(
      AttributeFilterUsecase.applyFilters(inputAnimals, {
        nodeFilters: animalMocks.oneNodeFilter,
        edgeFilters: animalMocks.oneEdgeFilter,
      }),
    ).toEqual(animalMocks.oneNodeAndEdgeFilterOutput));

  test('filter on an attribute where all nodes/edges have that attribute, should not filter out the nodes/edges that are not targeted', () => {
    const input: NodeLinkResultType = sameAttributes.queryResult;
    const output: NodeLinkResultType = sameAttributes.filteredOutput;
    const edgeFilters: FilterInfo[] = sameAttributes.edgeFilters;
    const nodeFilter1: FilterInfo = sameAttributes.nodeFilter1;
    const nodeFilter2: FilterInfo = sameAttributes.nodeFilter2;
    const bothNodeFilters: FilterInfo[] = [nodeFilter1, nodeFilter2];

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: [nodeFilter1],
        edgeFilters: [],
      }),
    ).toEqual(output);

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: [nodeFilter2],
        edgeFilters: [],
      }),
    ).toEqual(output);

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: bothNodeFilters,
        edgeFilters: [],
      }),
    ).toEqual(output);

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: [],
        edgeFilters: edgeFilters,
      }),
    ).toEqual(output);

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: bothNodeFilters,
        edgeFilters: edgeFilters,
      }),
    ).toEqual(output);
  });

  test('test that all predicates do what they are supposed to', () => {
    const input = allAttributeTypes.input;
    testNodeFiltersIndividually(allAttributeTypes.nodeFilters1, allAttributeTypes.output1);
    testNodeFiltersIndividually(allAttributeTypes.nodeFilters2, allAttributeTypes.output2);
    testNodeFiltersIndividually(allAttributeTypes.nodeFilters3, allAttributeTypes.output3);
    testNodeFiltersIndividually(allAttributeTypes.nodeFilters4, allAttributeTypes.output4);

    function testNodeFiltersIndividually(nodeFilters: FilterInfo[], output: NodeLinkResultType) {
      nodeFilters.forEach((nodeFilter) => {
        expect(
          AttributeFilterUsecase.applyFilters(input, {
            nodeFilters: [nodeFilter],
            edgeFilters: [],
          }),
        ).toEqual(output);
      });
    }
  });

  test('it should throw an error when given predicates that do not exist on the specific type of that attribute', () => {
    const input: NodeLinkResultType = allAttributeTypes.input;
    const filters: FilterInfo[] = allAttributeTypes.unexpectedPredicates;

    filters.forEach((filter) => {
      expect(() =>
        AttributeFilterUsecase.applyFilters(input, {
          nodeFilters: [filter],
          edgeFilters: [],
        }),
      ).toThrowError('Predicate does not exist');
    });

    filters.forEach((filter) => {
      expect(() =>
        AttributeFilterUsecase.applyFilters(input, {
          nodeFilters: [],
          edgeFilters: [filter],
        }),
      ).toThrowError('Predicate does not exist');
    });
  });

  test('it should throw an error when filtering on an unsupported type', () => {
    const input: NodeLinkResultType = allAttributeTypes.input;

    expect(() =>
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: allAttributeTypes.unsupportedTypeFilter,
        edgeFilters: [],
      }),
    ).toThrowError('Filter on this type is not supported.');

    expect(() =>
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: [],
        edgeFilters: allAttributeTypes.unsupportedTypeFilter,
      }),
    ).toThrowError('Filter on this type is not supported.');
  });

  test('it should return the query result untouched if the filter targetGroup does not exist', () => {
    const input: NodeLinkResultType = allAttributeTypes.input;

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: allAttributeTypes.targetGroupDoesNotExistFilter,
        edgeFilters: [],
      }),
    ).toEqual(input);

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: [],
        edgeFilters: allAttributeTypes.targetGroupDoesNotExistFilter,
      }),
    ).toEqual(input);
  });

  test('it should return an empty query result if the filter attribute does not exist', () => {
    const input: NodeLinkResultType = allAttributeTypes.input;

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: allAttributeTypes.attributeDoesNotExistOnNodeFilter,
        edgeFilters: [],
      }),
    ).toEqual(emptyQueryResult);

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: [],
        edgeFilters: allAttributeTypes.attributeDoesNotExistOnEdgeFilter,
      }),
    ).toEqual(emptyQueryResult);
  });

  //case sensitivity on string predicates
  test('filters on strings are case sensitive', () => {
    const input: NodeLinkResultType = caseSensitive.input;

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: caseSensitive.succeedingNodeFilters,
        edgeFilters: [],
      }),
    ).toEqual(input);

    expect(
      AttributeFilterUsecase.applyFilters(input, {
        nodeFilters: caseSensitive.failingNodeFilters,
        edgeFilters: [],
      }),
    ).toEqual(emptyQueryResult);
  });
});
