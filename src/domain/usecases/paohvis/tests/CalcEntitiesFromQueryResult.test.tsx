/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import calcEntitiesFromQueryResult from '../CalcEntitiesFromQueryResult';
import flights from '../../../../data/mock-data/query-result/smallFlightsQueryResults';
import mocks from '../../../../data/mock-data/paohvis/mockInputData';

describe('calcEntitiesFromQueryResult', () => {
  it('query result with selfedge', () => {
    expect(calcEntitiesFromQueryResult(flights)).toEqual(['airports']);
  });

  it('query chaining with 3 node types', () => {
    expect(calcEntitiesFromQueryResult(mocks.edgesBetween2DifferentNodeTypes)).toContain('person');
    expect(calcEntitiesFromQueryResult(mocks.edgesBetween2DifferentNodeTypes)).toContain('city');
    expect(calcEntitiesFromQueryResult(mocks.edgesBetween2DifferentNodeTypes)).toContain('third');
  });
});
