/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { parliamentData2 } from '../../../../data/mock-data/paohvis/mockSchemaResult';
import { isSchemaResult } from '../../../entity/graph-schema/structures/SchemaResultType';
import CalcEntityAttrAndRelNamesFromSchemaUseCase from '../CalcEntityAttrAndRelNamesFromSchemaUseCase';

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an entitiesFromSchema of the parliamentData2 set with the correct entityNames from calculateAttributesAndRelations', () => {
    if (isSchemaResult(parliamentData2)) {
      const entitiesFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesAndRelations(
        parliamentData2,
      );
      expect(entitiesFromSchema.entityNames).toEqual([
        'commissions',
        'resolutions',
        'parliament',
        'parties',
      ]);
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an attributesPerEntity of the parliamentData2 set with the correct amount of attributes in total from calculateAttributes', () => {
    if (isSchemaResult(parliamentData2)) {
      const attributesPerEntity = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributes(
        parliamentData2,
      );
      for (let i = 0; i < parliamentData2.nodes.length; i++) {
        const name = parliamentData2.nodes[i].name;
        expect(
          attributesPerEntity[name].boolAttributeNames.length +
            attributesPerEntity[name].numberAttributeNames.length +
            attributesPerEntity[name].textAttributeNames.length,
        ).toEqual(parliamentData2.nodes[i].attributes.length);
      }
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an attributesPerEntity (Record<string, string[]>) of the parliamentData2 set with the correct amount of each attribute type from calculateAttributes', () => {
    if (isSchemaResult(parliamentData2)) {
      const attributesPerEntity = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributes(
        parliamentData2,
      );
      for (let i = 0; i < parliamentData2.nodes.length; i++) {
        const name = parliamentData2.nodes[i].name;
        let boolAttributesLength = 0;
        let numberAttributesLength = 0;
        let textAttributesLength = 0;
        parliamentData2.nodes[i].attributes.forEach((attribute) => {
          if (attribute.type == 'bool') boolAttributesLength++;
          else if (attribute.type == 'int' || attribute.type == 'float') numberAttributesLength++;
          else if (attribute.type == 'string') textAttributesLength++;
        });
        expect(attributesPerEntity[name].boolAttributeNames.length).toEqual(boolAttributesLength);
        expect(attributesPerEntity[name].numberAttributeNames.length).toEqual(
          numberAttributesLength,
        );
        expect(attributesPerEntity[name].textAttributeNames.length).toEqual(textAttributesLength);
      }
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an relationsPerEntity (Record<string, string[]>) of the parliamentData2 set with the correct amount of relations of each entity from calculateRelations', () => {
    if (isSchemaResult(parliamentData2)) {
      const relationsPerEntity = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateRelations(
        parliamentData2,
      );
      for (let i = 0; i < parliamentData2.nodes.length; i++) {
        const name = parliamentData2.nodes[i].name;
        let relationCount = 0;
        parliamentData2.edges.forEach((edge) => {
          if (edge.to == name || edge.from == name) relationCount++;
        });
        expect(relationsPerEntity[name].length).toEqual(relationCount);
      }
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an relationsPerEntity (Record<string, string[]>) of the parliamentData2 set with the correct relations of each entity from calculateRelations', () => {
    if (isSchemaResult(parliamentData2)) {
      const relationsPerEntity = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateRelations(
        parliamentData2,
      );
      for (let i = 0; i < parliamentData2.nodes.length; i++) {
        const name = parliamentData2.nodes[i].name;
        parliamentData2.edges.forEach((edge) => {
          if (edge.to == name || edge.from == name)
            expect(relationsPerEntity[name]).toContain(edge.collection);
        });
      }
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an relationsFromSchema of the parliamentData2 set with the correct relationCollections from calculateAttributesFromRelation', () => {
    if (isSchemaResult(parliamentData2)) {
      const relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
        parliamentData2,
      );
      expect(relationsFromSchema.relationCollection).toEqual(['submits', 'part_of', 'member_of']);
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an relationsFromSchema of the parliamentData2 set with the correct relationNames from calculateAttributesFromRelation', () => {
    if (isSchemaResult(parliamentData2)) {
      const relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
        parliamentData2,
      );
      for (let i = 0; i < parliamentData2.edges.length; i++) {
        let edgeNameIsIncluded = false;
        relationsFromSchema.relationCollection.forEach((collection) => {
          if (relationsFromSchema.relationNames[collection] == parliamentData2.edges[i].name)
            edgeNameIsIncluded = true;
        });
        expect(edgeNameIsIncluded).toBe(true);
      }
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an attributesPerRelation of the parliamentData2 set with the correct amount of attributes in total from calculateAttributesFromRelation', () => {
    if (isSchemaResult(parliamentData2)) {
      const relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
        parliamentData2,
      );
      for (let i = 0; i < parliamentData2.edges.length; i++) {
        const name = parliamentData2.edges[i].collection;
        expect(
          relationsFromSchema.attributesPerRelation[name].boolAttributeNames.length +
            relationsFromSchema.attributesPerRelation[name].numberAttributeNames.length +
            relationsFromSchema.attributesPerRelation[name].textAttributeNames.length,
        ).toEqual(parliamentData2.edges[i].attributes.length);
      }
    }
  }));

describe('CalcEntityAttrAndRelNamesFromSchemaUseCase', () =>
  it('should return an attributesPerRelation (Record<string, string[]>) of the parliamentData2 set with the correct amount of each attribute type from calculateAttributesFromRelation', () => {
    if (isSchemaResult(parliamentData2)) {
      const relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
        parliamentData2,
      );
      for (let i = 0; i < parliamentData2.edges.length; i++) {
        const name = parliamentData2.edges[i].collection;
        let boolAttributesLength = 0;
        let numberAttributesLength = 0;
        let textAttributesLength = 0;
        parliamentData2.edges[i].attributes.forEach((attribute) => {
          if (attribute.type == 'bool') boolAttributesLength++;
          else if (attribute.type == 'int' || attribute.type == 'float') numberAttributesLength++;
          else if (attribute.type == 'string') textAttributesLength++;
        });
        expect(relationsFromSchema.attributesPerRelation[name].boolAttributeNames.length).toEqual(
          boolAttributesLength,
        );
        expect(relationsFromSchema.attributesPerRelation[name].numberAttributeNames.length).toEqual(
          numberAttributesLength,
        );
        expect(relationsFromSchema.attributesPerRelation[name].textAttributeNames.length).toEqual(
          textAttributesLength,
        );
      }
    }
  }));
