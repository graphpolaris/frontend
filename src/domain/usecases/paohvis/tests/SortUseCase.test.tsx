/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  HyperEdgeRange,
  NodeOrder,
  PaohvisNodeOrder,
  ValueType,
} from '../../../entity/paohvis/structures/Types';
import { AxisType } from '../../../entity/query-result/structures/NodeLinkResultType';
import SortUseCase from '../SortUseCase';

describe('SortUseCase', () => {
  it('sortHyperEdgeRanges', () => {
    // check for numerical rangeText
    const numberHyperEdgeRanges: HyperEdgeRange[] = [
      {
        rangeText: '2',
        hyperEdges: [
          { indices: [1, 2], frequencies: [], nameToShow: 'hyperedge1' },
          { indices: [1, 3, 2], frequencies: [], nameToShow: 'hyperedge2' },
        ],
      },
      {
        rangeText: '1',
        hyperEdges: [{ indices: [1], frequencies: [], nameToShow: 'name3' }],
      },
      {
        rangeText: '3',
        hyperEdges: [{ indices: [1, 0], frequencies: [], nameToShow: 'name3' }],
      },
    ];
    SortUseCase.sortHyperEdgeRanges(numberHyperEdgeRanges, ValueType.number);
    expect(numberHyperEdgeRanges).toEqual([
      {
        rangeText: '1',
        hyperEdges: [{ indices: [1], frequencies: [], nameToShow: 'name3' }],
      },
      {
        rangeText: '2',
        hyperEdges: [
          { indices: [1, 2], frequencies: [], nameToShow: 'hyperedge1' },
          { indices: [1, 2, 3], frequencies: [], nameToShow: 'hyperedge2' },
        ],
      },
      {
        rangeText: '3',
        hyperEdges: [{ indices: [0, 1], frequencies: [], nameToShow: 'name3' }],
      },
    ]);

    // check for alphabetical rangeText
    const alphabeticalHyperEdgeRanges: HyperEdgeRange[] = [
      {
        rangeText: 'b',
        hyperEdges: [
          { indices: [1, 2], frequencies: [], nameToShow: 'hyperedge1' },
          { indices: [1, 3, 2], frequencies: [], nameToShow: 'hyperedge2' },
        ],
      },
      {
        rangeText: 'a',
        hyperEdges: [{ indices: [1, 0], frequencies: [], nameToShow: 'name3' }],
      },
    ];
    SortUseCase.sortHyperEdgeRanges(alphabeticalHyperEdgeRanges, ValueType.bool);
    expect(alphabeticalHyperEdgeRanges).toEqual([
      {
        rangeText: 'a',
        hyperEdges: [{ indices: [0, 1], frequencies: [], nameToShow: 'name3' }],
      },
      {
        rangeText: 'b',
        hyperEdges: [
          { indices: [1, 2], frequencies: [], nameToShow: 'hyperedge1' },
          { indices: [1, 2, 3], frequencies: [], nameToShow: 'hyperedge2' },
        ],
      },
    ]);
  });
  it('sortNodes', () => {
    const nodes: AxisType[] = [
      { id: 'a', attributes: {} },
      { id: 'c', attributes: {} },
      { id: 'b', attributes: {} },
    ];
    const hyperEdgeDegree: Record<string, number> = { a: 1, b: 3, c: 2 };
    const order1: PaohvisNodeOrder = { orderBy: NodeOrder.degree, isReverseOrder: false };
    const order2: PaohvisNodeOrder = { orderBy: NodeOrder.alphabetical, isReverseOrder: true };

    SortUseCase.sortNodes(order1, nodes, hyperEdgeDegree);
    expect(nodes).toEqual([
      { id: 'b', attributes: {} },
      { id: 'c', attributes: {} },
      { id: 'a', attributes: {} },
    ]);

    SortUseCase.sortNodes(order2, nodes, hyperEdgeDegree);
    expect(nodes).toEqual([
      { id: 'c', attributes: {} },
      { id: 'b', attributes: {} },
      { id: 'a', attributes: {} },
    ]);

    const invalidOrder: PaohvisNodeOrder = { orderBy: 'aaa' as NodeOrder, isReverseOrder: false };
    expect(() => SortUseCase.sortNodes(invalidOrder, [], {})).toThrowError(
      'This node order does not exist',
    );
  });
  it('sortHyperEdges', () => {
    const hyperEdgeRanges: HyperEdgeRange[] = [
      {
        rangeText: 'sortByLineLength',
        hyperEdges: [
          { indices: [1, 2], frequencies: [], nameToShow: 'hyperedge1' },
          { indices: [1], frequencies: [], nameToShow: 'hyperedge2' },
          { indices: [1, 2, 3], frequencies: [], nameToShow: 'hyperedge3' },
        ],
      },
      {
        rangeText: 'sortByAppearanceOnYAxis',
        hyperEdges: [
          { indices: [4], frequencies: [], nameToShow: 'name3' },
          { indices: [3], frequencies: [], nameToShow: 'name2' },
          { indices: [2], frequencies: [], nameToShow: 'name1' },
        ],
      },
    ];

    SortUseCase.sortHyperEdges(hyperEdgeRanges);
    expect(hyperEdgeRanges).toEqual([
      {
        rangeText: 'sortByLineLength',
        hyperEdges: [
          { indices: [1], frequencies: [], nameToShow: 'hyperedge2' },
          { indices: [1, 2], frequencies: [], nameToShow: 'hyperedge1' },
          { indices: [1, 2, 3], frequencies: [], nameToShow: 'hyperedge3' },
        ],
      },
      {
        rangeText: 'sortByAppearanceOnYAxis',
        hyperEdges: [
          { indices: [2], frequencies: [], nameToShow: 'name1' },
          { indices: [3], frequencies: [], nameToShow: 'name2' },
          { indices: [4], frequencies: [], nameToShow: 'name3' },
        ],
      },
    ]);
  });
});
