/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  NodeOrder,
  PaohvisAxisInfo,
  PaohvisNodeOrder,
  PaohvisData,
  ValueType,
  AttributeOrigin,
  PaohvisFilters,
  FilterInfo,
} from '../../../entity/paohvis/structures/Types';
import { NodeLinkResultType } from '../../../entity/query-result/structures/NodeLinkResultType';
import ToPaohvisDataParserUsecase from '../ToPaohvisDataParserUsecase';

import smallQuery from '../../../../data/mock-data/query-result/smallFlightsQueryResults';

describe('ToPaohvisDataParserUseCase', () => {
  const nodeOrder: PaohvisNodeOrder = { orderBy: NodeOrder.degree, isReverseOrder: false };

  const emptyInput: NodeLinkResultType = { nodes: [], edges: [] };
  const emptyOutput: PaohvisData = { rowLabels: [], hyperEdgeRanges: [], maxRowLabelWidth: 10 };

  const mockQueryResult: NodeLinkResultType = {
    edges: [
      {
        from: 'person/Peter',
        to: 'city/Utrecht',
        id: 'livesIn/1',
        attributes: { year: 2000, isHomeTown: true, testString: 'help' },
      },
      {
        from: 'person/MalseMakker',
        to: 'city/Utrecht',
        id: 'livesIn/2',
        attributes: { year: 2005, isHomeTown: false, testString: 'test' },
      },
      {
        from: 'person/Timon',
        to: 'city/Rotterdam',
        id: 'livesIn/3',
        attributes: { year: 2000, isHomeTown: false, testString: 'help' },
      },
      {
        from: 'person/Timon',
        to: 'city/The Hague',
        id: 'livesIn/4',
        attributes: { year: 2005, isHomeTown: true, testString: 'test' },
      },
      {
        from: 'person/Peter',
        to: 'city/The Hague',
        id: 'livesIn/5',
        attributes: { year: 2000, isHomeTown: false, testString: 'help' },
      },
      {
        from: 'third/hello',
        to: 'third/query',
        id: 'chains/12',
        attributes: {},
      },
      {
        from: 'third/query',
        to: 'third/chaining',
        id: 'chains/13',
        attributes: {},
      },
    ],
    nodes: [
      { id: 'person/Peter', attributes: { age: 20 } },
      { id: 'person/Timon', attributes: { age: 21 } },
      { id: 'person/MalseMakker', attributes: { age: 20 } },
      { id: 'city/Utrecht', attributes: { province: 'Utrecht' } },
      { id: 'city/Rotterdam', attributes: { province: 'South Holland' } },
      { id: 'city/The Hague', attributes: { province: 'South Holland' } },
      { id: 'third/hello', attributes: {} },
      { id: 'third/query', attributes: {} },
      { id: 'third/chaining', attributes: {} },
    ],
  };

  it('when given an empty result it should return empty PaohvisSpecs', () => {
    const parser: ToPaohvisDataParserUsecase = new ToPaohvisDataParserUsecase(emptyInput);
    expect(
      parser.parseQueryResult(
        {
          selectedAttribute: {
            name: '',
            type: ValueType.noAttribute,
            origin: AttributeOrigin.noAttribute,
          },
          relation: { collection: '', from: '', to: '' },
          isYAxisEntityEqualToRelationFrom: false,
        },
        nodeOrder,
      ),
    ).toEqual(emptyOutput);
  });

  it('should return correct PaohvisData for selfedge relations', () => {
    let nodeOrderAlpha: PaohvisNodeOrder = {
      orderBy: NodeOrder.alphabetical,
      isReverseOrder: true,
    };

    const axisInfo: PaohvisAxisInfo = {
      selectedAttribute: {
        name: 'vip',
        type: ValueType.bool,
        origin: AttributeOrigin.entity,
      },
      relation: { collection: 'flights', from: 'airports', to: 'airports' },
      isYAxisEntityEqualToRelationFrom: true,
    };
    const parser: ToPaohvisDataParserUsecase = new ToPaohvisDataParserUsecase(smallQuery);
    let resultData = parser.parseQueryResult(axisInfo, nodeOrderAlpha);
    expect(resultData.rowLabels).toEqual(['airports/SFO', 'airports/JFK']);
    expect(resultData.hyperEdgeRanges).toEqual([
      {
        rangeText: 'true',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1, 0],
            nameToShow: 'airports/JFK',
          },
          {
            indices: [1],
            frequencies: [0, 1],
            nameToShow: 'airports/SFO',
          },
        ],
      },
    ]);

    nodeOrderAlpha.isReverseOrder = false;
    resultData = parser.parseQueryResult(axisInfo, nodeOrderAlpha);
    expect(resultData.rowLabels).toEqual(['airports/JFK', 'airports/SFO']);
    expect(resultData.hyperEdgeRanges).toEqual([
      {
        rangeText: 'true',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1, 0],
            nameToShow: 'airports/SFO',
          },
          {
            indices: [1],
            frequencies: [0, 1],
            nameToShow: 'airports/JFK',
          },
        ],
      },
    ]);
  });

  it('should return correct PaohvisData for relations between two different entity types', () => {
    const axisInfo: PaohvisAxisInfo = {
      selectedAttribute: {
        name: 'year',
        type: ValueType.number,
        origin: AttributeOrigin.relation,
      },
      relation: { collection: 'livesIn', from: 'person', to: 'city' },
      isYAxisEntityEqualToRelationFrom: false,
    };
    const parser: ToPaohvisDataParserUsecase = new ToPaohvisDataParserUsecase(mockQueryResult);
    let resultData: PaohvisData = parser.parseQueryResult(axisInfo, nodeOrder);
    expect(resultData.rowLabels).toEqual(['city/Utrecht', 'city/The Hague', 'city/Rotterdam']);
    expect(resultData.hyperEdgeRanges).toEqual([
      {
        rangeText: '2000',
        hyperEdges: [
          {
            indices: [0, 1],
            frequencies: [1, 1, 0],
            nameToShow: 'person/Peter',
          },
          {
            indices: [2],
            frequencies: [0, 0, 1],
            nameToShow: 'person/Timon',
          },
        ],
      },
      {
        rangeText: '2005',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1, 0, 0],
            nameToShow: 'person/MalseMakker',
          },
          {
            indices: [1],
            frequencies: [0, 1, 0],
            nameToShow: 'person/Timon',
          },
        ],
      },
    ]);

    // test with node filters only
    const nodeFilters: FilterInfo[] = [
      {
        targetGroup: 'person',
        attributeName: 'age',
        value: 20,
        predicateName: '<=',
      },
    ];
    parser.setPaohvisFilters({ nodeFilters: nodeFilters, edgeFilters: [] });
    resultData = parser.parseQueryResult(axisInfo, nodeOrder);
    expect(resultData.rowLabels).toEqual(['city/Utrecht', 'city/The Hague']);
    expect(resultData.hyperEdgeRanges).toEqual([
      {
        rangeText: '2000',
        hyperEdges: [
          {
            indices: [0, 1],
            frequencies: [1, 1],
            nameToShow: 'person/Peter',
          },
        ],
      },
      {
        rangeText: '2005',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1, 0],
            nameToShow: 'person/MalseMakker',
          },
        ],
      },
    ]);

    // test with node filters and edge filters
    const edgeFilters: FilterInfo[] = [
      {
        targetGroup: 'livesIn',
        attributeName: 'testString',
        value: 'help',
        predicateName: 'excludes',
      },
    ];
    parser.setPaohvisFilters({ nodeFilters: nodeFilters, edgeFilters: edgeFilters });
    resultData = parser.parseQueryResult(axisInfo, nodeOrder);
    expect(resultData.rowLabels).toEqual(['city/Utrecht']);
    expect(resultData.hyperEdgeRanges).toEqual([
      {
        rangeText: '2005',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1],
            nameToShow: 'person/MalseMakker',
          },
        ],
      },
    ]);

    // test with edge filters only
    parser.setPaohvisFilters({ nodeFilters: [], edgeFilters: edgeFilters });
    resultData = parser.parseQueryResult(axisInfo, nodeOrder);
    expect(resultData.rowLabels).toEqual(['city/Utrecht', 'city/The Hague']);
    expect(resultData.hyperEdgeRanges).toEqual([
      {
        rangeText: '2005',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1, 0],
            nameToShow: 'person/MalseMakker',
          },
          {
            indices: [1],
            frequencies: [0, 1],
            nameToShow: 'person/Timon',
          },
        ],
      },
    ]);
  });
  
  it('parse with no attribute', () => {
    const axisInfo: PaohvisAxisInfo = {
      selectedAttribute: {
        name: '',
        type: ValueType.noAttribute,
        origin: AttributeOrigin.noAttribute,
      },
      relation: { collection: 'livesIn', from: 'person', to: 'city' },
      isYAxisEntityEqualToRelationFrom: true,
    };
    const parser: ToPaohvisDataParserUsecase = new ToPaohvisDataParserUsecase(mockQueryResult);
    let resultData: PaohvisData = parser.parseQueryResult(axisInfo, nodeOrder);
    expect(resultData.rowLabels).toEqual(['person/Peter', 'person/Timon', 'person/MalseMakker']);
    expect(resultData.hyperEdgeRanges).toEqual([
      {
        rangeText: 'No attribute was selected',
        hyperEdges: [
          {
            indices: [0, 1],
            frequencies: [1, 1, 0],
            nameToShow: 'city/The Hague',
          },
          {
            indices: [0, 2],
            frequencies: [1, 0, 1],
            nameToShow: 'city/Utrecht',
          },
          {
            indices: [1],
            frequencies: [0, 1, 0],
            nameToShow: 'city/Rotterdam',
          },
        ],
      },
    ]);
  });
});
