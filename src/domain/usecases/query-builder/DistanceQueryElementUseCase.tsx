/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { removeElements } from 'react-flow-renderer';
import {
  AttributeNode,
  EntityNode,
  QueryElementTypes as t,
  RelationNode,
  AnyNode,
} from '../../entity/query-builder/structures/Nodes';

/** The use case that measures the distance between two query elements */
export default class DistanceQueryElementUseCase {
  public static calcDistancesTo(node: AnyNode, allNodes: AnyNode[]): [number, AnyNode][] {
    const otherNodes = removeElements([node], allNodes) as AnyNode[];
    return otherNodes.map((otherNode) => [this.calcDistance(node, otherNode), otherNode]);
  }

  /**
   * Calculates the distance between two nodes.
   * @param a An entity node.
   * @param b The other entity node.
   * @returns {number} the distance between the 2 nodes.
   */
  public static calcDistance(a: AnyNode, b: AnyNode): number {
    const diffX = a.position.x - b.position.x;
    const diffY = a.position.y - b.position.y;
    return Math.sqrt(diffX * diffX + diffY * diffY);
  }
}
