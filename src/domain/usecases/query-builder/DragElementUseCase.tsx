/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import NodesHolder from '../../entity/query-builder/models/NodesHolder';
import { AnyNode } from '../../entity/query-builder/structures/Nodes';
import {
  Handles,
  nodeToHandlesThatCanReceiveDragconnect,
  nodeToHandlesThatCanSendDragconnect,
} from '../../entity/query-builder/structures/Handles';
import ValidateConnectionUseCase from './ValidateConnectionUseCase';
import DistanceQueryElementUseCase from './DistanceQueryElementUseCase';
import SetElementsUseCase from './SetElementsUseCase';
import GetElementsUseCase from './GetElementsUseCase';

export const closeByDistance = 100;

/** UseCase for handling drag events. It will select a specific usecase depending on the node type. */
export default class DragElementUseCase {
  public dragging = false;
  public nodesHolder: NodesHolder;
  public validateConnectionUseCase: ValidateConnectionUseCase;
  public setElementsUseCase: SetElementsUseCase;
  public getElementsUseCase: GetElementsUseCase;

  public constructor(
    validateConnectionUseCase: ValidateConnectionUseCase,
    setElementsUseCase: SetElementsUseCase,
    getElementsUseCase: GetElementsUseCase,
    nodesHolder: NodesHolder,
  ) {
    this.nodesHolder = nodesHolder;
    this.validateConnectionUseCase = validateConnectionUseCase;
    this.setElementsUseCase = setElementsUseCase;
    this.getElementsUseCase = getElementsUseCase;
  }

  /**
   * Used in reactFlow component (and therefore called via QueryBuilderViewModel implementation). Doesn't do anything as of now.
   * Should probably display a visual of what node might be connected to.
   * @param thisNode The ReactFlow node that is being dragged.
   */
  public OnDrag(thisNode: AnyNode) {
    return;
  }

  /**
   * Not currently in use, but kept here as empty function to enable use of it in test cases.
   * Should events ever be implemented on drag start, these test cases will be adapted to it straight away.
   */
  public OnDragStart(thisNode: AnyNode) {
    return;
  }

  /**
   * Events that happen when a node-drag ends
   * @param node The node being dragged.
   */
  public OnDragStop(node: AnyNode): void {
    this.connectToClosest(node);
  }

  /**
   * Connects node to the closest node to which it can connect, if any exist. Uses threshold for distance.
   */
  public connectToClosest(node: AnyNode): void {
    const sortedPossibleNodes = this.findConnectionOptionsAndProximity(node);
    if (sortedPossibleNodes.length == 0) return;
    const [_, sourceNode, sourceHandle, targetNode, targetHandle] = sortedPossibleNodes[0];
    this.setElementsUseCase.createEdge(sourceNode, sourceHandle, targetNode, targetHandle);
    return;
  }

  /**
   * Return a list of distance, sourceNode and -handle and targetNode and -handle tuples, sorted on distance.
   * The list contains all source-target pairs of the input node and nodes within a threshold distance.
   */
  public findConnectionOptionsAndProximity(
    thisNode: AnyNode,
  ): [number, AnyNode, string, AnyNode, string][] {
    //distances from thisNode to all other nodes
    const distances: [number, AnyNode][] = DistanceQueryElementUseCase.calcDistancesTo(
      thisNode,
      this.getElementsUseCase.getAllNodes(),
    );

    //filter out nodes too far away
    const closeBy = distances.filter(([d, _]) => d < closeByDistance);

    //find pairs of respectively sources and targets that are connectable
    //where either sourceNode or targetNode is thisNode
    const possibleConnections: [number, AnyNode, string, AnyNode, string][] = [];
    // prettier-ignore
    closeBy.forEach(([d, otherNode]) =>
      nodeToHandlesThatCanSendDragconnect(thisNode).forEach((thisHandle) => {
        nodeToHandlesThatCanReceiveDragconnect(otherNode).forEach((otherHandle) => {
          if (this.validateConnectionUseCase.validateConnection(thisNode, thisHandle, otherNode, otherHandle))
            possibleConnections.push([d, thisNode, thisHandle, otherNode, otherHandle]);
          if (this.validateConnectionUseCase.validateConnection(otherNode, otherHandle, thisNode, thisHandle))
            possibleConnections.push([d, otherNode, otherHandle, thisNode, thisHandle]);
        });
      }),
    );

    if (possibleConnections.length == 0) return [];
    const sortedPossibleConnections: [
      number, //distance between nodes
      AnyNode, //sourceNode
      string, //sourceHandle
      AnyNode, //targetNode
      string, //targetHandle
    ][] = possibleConnections.sort((a, b) => {
      return a[0] > b[0] ? 1 : -1;
    });
    return sortedPossibleConnections;
  }
}
