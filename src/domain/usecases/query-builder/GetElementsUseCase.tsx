/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import NodesHolder from '../../entity/query-builder/models/NodesHolder';
import {
  AnyNode,
  AnyElement,
  AttributeNode,
  Edge,
  EntityNode,
  FunctionNode,
  QueryElementTypes,
  RelationNode,
} from '../../entity/query-builder/structures/Nodes';
import {
  Handles,
  isFunctionHandle,
  typeToFunctionHandle,
} from '../../entity/query-builder/structures/Handles';
import { isEdge, isNode, getConnectedEdges, getIncomers, getOutgoers } from 'react-flow-renderer';
import { FunctionArgTypes } from '../../entity/query-builder/structures/FunctionTypes';

/* Usecase with helper functions for reading NodesHolder elements */
export default class GetElementsUseCase {
  nodesHolder: NodesHolder;

  constructor(nodesHolder: NodesHolder) {
    this.nodesHolder = nodesHolder;
  }

  /**
   * Returns all elements
   */
  public getAllElements(): AnyElement[] {
    return this.nodesHolder.getNodesHolderElements();
  }

  /**
   * Returns all edges
   */
  public getAllEdges(): Edge[] {
    return this.getAllElements().filter((edge) => isEdge(edge)) as Edge[];
  }

  /**
   * Returns all nodes
   */
  public getAllNodes(): AnyNode[] {
    return this.getAllElements().filter((edge) => isNode(edge)) as AnyNode[];
  }

  /**
   * Returns all nodes of a given type.
   * Multiple functions are implemented to make use of type assertions.
   */
  private getAllNodesOfType(type: QueryElementTypes): AnyNode[] {
    return this.getAllNodes().filter((node) => {
      if (node.type) {
        return node.type == type;
      } else return false;
    });
  }
  /** returns all entity nodes in the query builder */
  public getEntityNodes() {
    return this.getAllNodesOfType(QueryElementTypes.Entity) as EntityNode[];
  }
  /** returns all relation nodes in the query builder */
  public getRelationNodes() {
    return this.getAllNodesOfType(QueryElementTypes.Relation) as RelationNode[];
  }
  /** returns all function nodes in the query builder */
  public getFunctionNodes() {
    return this.getAllNodesOfType(QueryElementTypes.Function) as FunctionNode[];
  }
  /** returns all attribute nodes in the query builder */
  public getAttributeNodes() {
    return this.getAllNodesOfType(QueryElementTypes.Attribute) as AttributeNode[];
  }

  /**
   * Returns node with given ID
   * Note that the node is returned as AnyNode, and might need to be asserted as a node of a specific type
   */
  public getNodeByID(ID: string): AnyNode {
    const node = this.getAllNodes().find((node) => node.id == ID);
    if (!node) throw new Error('NodeID not found in NodesHolder');
    return node;
  }

  /**
   * Returns all edges with the specified node as target or source
   */
  public getConnectedEdges(node: AnyNode): Edge[] {
    return getConnectedEdges([node], this.getAllEdges());
  }

  /**
   * Returns all edges with the specified node as target
   */
  public getInEdges(node: AnyNode): Edge[] {
    return this.getConnectedEdges(node).filter((edge) => edge.target == node.id);
  }
  /**
   * Returns all edges with the specified node as source
   */
  public getOutEdges(node: AnyNode): Edge[] {
    return this.getConnectedEdges(node).filter((edge) => edge.source == node.id);
  }
  /**
   * Returns all nodes connected to specified node, where the specified node is the target
   */
  public getInNodes(node: AnyNode): AnyNode[] {
    return getIncomers(node, this.getAllElements());
  }
  /**
   * Returns all nodes connected to specified node, where the specified node is the source
   */
  public getOutNodes(node: AnyNode): AnyNode[] {
    return getOutgoers(node, this.getAllElements());
  }

  /**
   * Returns all edges between specified source and target node
   */
  public getEdgesBetween(source: AnyNode, target: AnyNode): Edge[] {
    return this.getAllEdges().filter(
      (edge) => edge.source == source.id && edge.target == target.id,
    );
  }

  /** Checks wheter the edge exists between 2 nodes */
  public doesEdgeExist(
    source: AnyNode,
    sourceHandle: string,
    target: AnyNode,
    targetHandle: string,
  ) {
    const edgeBetween: Edge | undefined = this.getEdgesBetween(source, target).find(
      (edge) => edge.sourceHandle == sourceHandle && edge.targetHandle == targetHandle,
    );
    return typeof edgeBetween != 'undefined';
  }
  /**
   * Returns the node and edge connected to the specified handle of specified node if such a connection exists
   * Note that types need to be asserted
   */
  public getNodeEdgeByHandle(node: AnyNode, handle: string): [AnyNode, Edge] | undefined {
    // prettier-ignore
    switch (handle) {
      case Handles.ToAttributeHandle:
        return this.getNodeEdgeByInHandle(node, handle);
      case Handles.RelationLeft:
        return this.getNodeEdgeByInHandle(node, handle);
      case Handles.RelationRight:
        return this.getNodeEdgeByInHandle(node, handle);
      case Handles.OnAttribute:
        return this.getNodeEdgeByOutHandle(node, handle);
      case Handles.ToRelation:
        return this.getNodeEdgeByOutHandle(node, handle);
      case Handles.FunctionBase:
          return this.getNodeEdgeByOutHandle(node, handle);
      case typeToFunctionHandle(FunctionArgTypes.constraints):
          return this.getNodeEdgeByInHandle(node, handle);
      case Handles.ReceiveFunction:
        return this.getNodeEdgeByInHandle(node, handle);
      default:
        if (isFunctionHandle(handle)) return this.getNodeEdgeByOutHandle(node, handle);
        throw new Error('getNodeEdgeByHandle: unsupported handle');
    }
  }

  /** This is for the multiple connections you can make to the group by. */
  public getNodeEdgesByHandle(node: AnyNode, handle: string): [AnyNode, Edge][] | undefined {
    if (isFunctionHandle(handle)) return this.getNodeEdgesByOutHandle(node, handle);
    else {
      this.getNodeEdgeByHandle(node, handle);
    }
  }

  /**
   * Returns the edge connected to the specified handle of specified node if such a connection exists
   * Note that types need to be asserted
   */
  public getEdgeByHandle(node: AnyNode, handle: string): Edge | undefined {
    const res = this.getNodeEdgeByHandle(node, handle);
    return res ? res[1] : undefined;
  }
  /**
   * Returns the node connected to the specified handle of specified node if such a connection exists.
   * Note that types need to be asserted.
   */
  public getNodeByHandle(node: AnyNode, handle: string): AnyNode | undefined {
    const res = this.getNodeEdgeByHandle(node, handle);
    return res ? res[0] : undefined;
  }

  /**
   * Returns the nodes connected to the specified handle of specified node if such a connection exists.
   * Note that types need to be asserted.
   * @param node The node that the other node should be connected to.
   * @param handle The handle value to look for
   * @returns a list of nodes
   */
  public getNodesByHandle(node: AnyNode, handle: string): AnyNode[] | undefined {
    const results = this.getNodeEdgesByHandle(node, handle);
    return results
      ? results.map((result) => {
          return result[0];
        })
      : undefined;
  }

  /**
   * Returns whether a node is connected to the specified handle of specified node
   */
  public isHandleConnected(node: AnyNode, handle: string): boolean {
    return !(typeof this.getNodeByHandle(node, handle) == 'undefined');
  }

  /**
   * Helper function that don't assert correctness of specified handle, and should not not be called directly.
   */
  private getNodeEdgeByInHandle(node: AnyNode, handle: string): [AnyNode, Edge] | undefined {
    const edge = this.getAllEdges().find(
      (edge_) => edge_.target == node.id && edge_.targetHandle == handle,
    );
    return edge ? [this.getNodeByID(edge.source), edge] : undefined;
  }
  /**
   * Helper function that don't assert correctness of specified handle, and should not not be called directly.
   */
  private getNodeEdgeByOutHandle(node: AnyNode, handle: string): [AnyNode, Edge] | undefined {
    const edge = this.getAllEdges().find(
      (edge_) => edge_.source == node.id && edge_.sourceHandle == handle,
    );
    return edge ? [this.getNodeByID(edge.target), edge] : undefined;
  }

  /**
   * Helper function that don't assert correctness of specified handle, and should not not be called directly.
   * @param node The source node.
   * @param handle The source handle to look for.
   * @returns All edges that have this handle and source node.
   */
  private getNodeEdgesByOutHandle(node: AnyNode, handle: string): [AnyNode, Edge][] | undefined {
    const edges = this.getAllEdges().filter(
      (edge_) => edge_.source == node.id && edge_.sourceHandle == handle,
    );
    const listOfEdgesWithTargetNode = edges.map((edge) => {
      return [this.getNodeByID(edge.target), edge] as [AnyNode, Edge];
    });
    return edges ? listOfEdgesWithTargetNode : undefined;
  }

  /**
   * Returns all relations connected to specified entity
   */
  public getRelationsConnectedToNode(node: EntityNode | FunctionNode): RelationNode[] {
    return this.getOutNodes(node).filter(
      (connectedNode) => connectedNode.type == QueryElementTypes.Relation,
    ) as RelationNode[];
  }

  /**
   * Returns a pair of nodes connected to a relation. Both values may be a EntityNode, FunctionNode or undefined
   */
  public getNodesConnectedToRelation(
    node: RelationNode,
  ): [leftEntity?: EntityNode | FunctionNode, rightEntity?: EntityNode | FunctionNode] {
    return [
      this.getNodeByHandle(node, Handles.RelationLeft) as EntityNode | FunctionNode | undefined,
      this.getNodeByHandle(node, Handles.RelationRight) as EntityNode | FunctionNode | undefined,
    ];
  }

  /**
   * Returns all AttributeNode either directly connected to specified node, or indirectly via other attributes.
   */
  public getAllConstraints(node: AnyNode): AttributeNode[] {
    let handleToLookFor = Handles.ToAttributeHandle as string;
    if (node.type == QueryElementTypes.Function) {
      handleToLookFor = typeToFunctionHandle(FunctionArgTypes.constraints);
    }

    let attributes = [];
    let currentAttribute: AttributeNode | undefined = this.getNodeByHandle(
      node,
      handleToLookFor,
    ) as AttributeNode | undefined;
    // iterate down connected attributes
    while (currentAttribute) {
      attributes.push(currentAttribute);
      currentAttribute = this.getNodeByHandle(currentAttribute, handleToLookFor) as
        | AttributeNode
        | undefined;
    }
    return attributes;
  }
}
