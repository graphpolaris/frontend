/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { Attribute, Edge, Node, Schema } from '../../entity/graph-schema/structures/InputDataTypes';

/**
 * The ParseConstraintsUseCase parses the constraints for the ValidateConnectionUseCase using the given schema.
 * It can create an attributeDictionary, fromDictionary and a toDictionary.
 */
export default class ParseConstraintsUseCase {
  /**
   * Creates the attribute dictionary containing a key, value pair of nodes and their attributes.
   * The schema is used to create a list of all the attributes for every relation or entity node.
   * @param schema The schema data received from the backend.
   * @returns {Map<string, Attribute[]>} The dictionary containing entity and relation nodes and their respective attributes.
   */
  public createAttributeDict(schema: Schema): Map<string, Attribute[]> {
    let attributeDictionary: Map<string, Attribute[]> = new Map<string, Attribute[]>();
    schema.nodes.forEach((node: Node) => attributeDictionary.set(node.name, node.attributes));
    schema.edges.forEach((edge: Edge) => attributeDictionary.set(edge.collection, edge.attributes));
    return attributeDictionary;
  }

  /**
   * Creates the dictionary containing for all relations their respecting 'from' entity nodes.
   * A 'from' entity node is an entity that is positioned on the left side of the relation.
   * For example in | Person -[drives]-> Car | the person is the 'from' entity node.
   * @param schema The schema data received from the backend.
   * @returns {Map<string, string>} The dictionary containing relation nodes and their 'from' entities.
   */
  public createFromDict(schema: Schema): Map<string, string> {
    let fromDict: Map<string, string> = new Map<string, string>();
    schema.edges.forEach((edge: Edge) => fromDict.set(edge.collection, edge.from));
    return fromDict;
  }

  /**
   * Creates the dictionary containing for all relations their respecting 'to' entity nodes.
   * A 'to' entity node is an entity that is positioned on the left side of the relation.
   * For example in | Person -[drives]-> Car | the car is the 'to' entity node.
   * @param schema The schema data received from the backend.
   * @returns {Map<string, string>} The dictionary containing relation nodes and their 'to' entities.
   */
  public createToDict(schema: Schema): Map<string, string> {
    let toDict: Map<string, string> = new Map<string, string>();
    schema.edges.forEach((edge: Edge) => toDict.set(edge.collection, edge.to));
    return toDict;
  }
}
