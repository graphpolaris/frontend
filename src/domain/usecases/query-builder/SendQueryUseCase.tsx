/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BackendMessengerRepository from '../../repository-interfaces/BackendMessengerRepository';

/** UseCase for sending a translated JOSN query string. */
export default class SendQueryUseCase {
  private backendMessenger: BackendMessengerRepository;
  public constructor(backendMessenger: BackendMessengerRepository) {
    this.backendMessenger = backendMessenger;
  }

  /**
   * Sends a translated JSON string query to the backend.
   * @param body The translated JSON string query to send.
   * @throws {Error} If credentials are not valid or have not passed
   * @returns True if the query was successfully send.
   */
  public async SendQuery(body: string): Promise<boolean> {
    return this.backendMessenger
      .SendMessage(body, 'query/execute/', 'POST')
      .then((response) => {
        if (response.status != 200) throw 'Request to execute query unsuccessful!';
        else return true;
      })
      .catch((error) => {
        throw error;
      });
  }
}
