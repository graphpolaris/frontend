/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  AttributeNode,
  EntityNode,
  QueryElementTypes as t,
  RelationNode,
  FunctionNode,
  AnyNode,
  AnyNodeData,
  Edge,
  QueryElementTypes,
  AnyElement,
  FunctionData,
} from '../../entity/query-builder/structures/Nodes';
import NodesHolder from '../../entity/query-builder/models/NodesHolder';
import {
  Handles,
  isFunctionHandle,
  typeToFunctionHandle,
} from '../../entity/query-builder/structures/Handles';
import {
  FunctionTypes,
  DefaultFunctionArgs,
  FunctionArgTypes,
} from '../../entity/query-builder/structures/FunctionTypes';
import { NodePosition } from '../../entity/query-builder/structures/NodePosition';
import GetElementsUseCase from './GetElementsUseCase';
import { Node } from 'react-flow-renderer';

/* UseCase for modifying NodesHolder via the insertion, modification and deletion of elements */
export default class SetElementsUseCase {
  nodesHolder: NodesHolder;
  getElementsUseCase: GetElementsUseCase;

  constructor(nodesHolder: NodesHolder, getElementsUseCase: GetElementsUseCase) {
    this.nodesHolder = nodesHolder;
    this.getElementsUseCase = getElementsUseCase;
  }

  /**
   * Create and insert an edge between specified nodes with specified handles
   * Does not check whether the connection is ledgitimate
   */
  public createEdge(source: AnyNode, sourceHandle: string, target: AnyNode, targetHandle: string) {
    const newEdge = {
      id: this.nodesHolder.getNewID(),
      source: source.id,
      sourceHandle: sourceHandle,
      target: target.id,
      targetHandle: targetHandle,
      animated: false,
    };
    this.nodesHolder.addEdge(newEdge);
    if (isFunctionHandle(sourceHandle)) {
      this.testFunctionCorrectness(source as FunctionNode);
    }
    return;
  }

  /**
   * Connect an attributeNode to a node
   * Does not check whether the connection is ledgitimate
   */
  public connectAttribute(attribute: AttributeNode, node: AnyNode) {
    this.createEdge(attribute, Handles.OnAttribute, node, Handles.ToAttributeHandle);
  }

  /**
   * Create an edge between specified entity and relation. rightSide specifies which side of the relation the entity is to be connected to.
   * Does not check whether the connection is ledgitimate
   */
  public connectEntityToRelation(
    entity: EntityNode,
    relation: RelationNode,
    rightSide: boolean,
  ): void {
    if (
      this.getElementsUseCase.doesEdgeExist(
        entity,
        Handles.ToRelation,
        relation,
        rightSide ? Handles.RelationRight : Handles.RelationLeft,
      )
    )
      return;
    this.createEdge(
      this.getElementsUseCase.getNodeByID(entity.id),
      Handles.ToRelation,
      this.getElementsUseCase.getNodeByID(relation.id),
      rightSide ? Handles.RelationRight : Handles.RelationLeft,
    );
  }

  /**
   * Create an entity, relation or attribute.
   * @param type Should be QueryElementTypes.Entity, QueryElementTypes.Relation or QueryElementTypes.Attribute
   * @param position Location of the created node
   * @param name Name of the entity, relation or filtered attribute
   * @param fadeIn Optional. Whether the node should fade into view or appear instananeously. Only implemented for entities, and currently (17/11/'21) broken
   * @param dataType For attributes, the type of data being filtered
   * @param collection For relations
   * @returns The created node
   */
  public createNodeFromSchema(
    type: QueryElementTypes,
    position: NodePosition,
    name: string,
    fadeIn = false,
    dataType = '',
    collection = '',
  ): AnyNode {
    let data: AnyNodeData;
    switch (type) {
      case QueryElementTypes.Entity:
        data = {
          name: name,
          fadeIn: false, //TODO: discabled rn due to bug with ReactFlow. In QueryBuilderComponent we discard and reload all nodes on update, causing
          //fadeIn to trigger multiple times.
        };
        break;
      case QueryElementTypes.Relation:
        data = {
          name: name,
          fadeIn: fadeIn,
          collection: collection,
          depth: { min: 1, max: 1 },
        };
        break;
      case QueryElementTypes.Function:
        throw new Error('Cannot created function node from scheme');
      case QueryElementTypes.Attribute:
        data = {
          fadeIn: fadeIn,
          attribute: name,
          value: '',
          dataType: dataType,
          matchType: 'exact',
        };
        break;
    }

    const newNode: Node<any> = {
      id: this.nodesHolder.getNewID(),
      type: type,
      data: data,
      position: position,
    };
    this.nodesHolder.addNode(newNode);
    return newNode;
  }

  /**
   * Create and return a new function node
   */
  public createFunction(type: FunctionTypes, position: NodePosition) {
    // Deep clone functionArgs so that multiple functions can have different arguemnts.
    let functionArgs = Object.assign({}, DefaultFunctionArgs[type]);
    for (let attribute in DefaultFunctionArgs[type]) {
      if (DefaultFunctionArgs[type].hasOwnProperty(attribute))
        functionArgs[attribute] = Object.assign({}, DefaultFunctionArgs[type][attribute]);
    }

    const newNode = {
      id: this.nodesHolder.getNewID(),
      type: QueryElementTypes.Function,
      data: {
        functionType: type,
        args: functionArgs,
        fadeIn: true,
      },
      position: position,
    } as FunctionNode; // This line was added to make it stop complaining but it might break stuff IDK. TODO remove this in merge.
    this.nodesHolder.addNode(newNode);
    return newNode;
  }
  /** Adds a node */
  public addNode(node: AnyNode) {
    this.nodesHolder.addNode(node);
  }

  /**
   * Remove a node and all connected edges
   */
  public RemoveNode(node: AnyNode) {
    this.nodesHolder.removeElements([node]);
  }

  /**
   * Remove an edge and update connected nodes accordingly
   */
  public RemoveEdge(edge: Edge) {
    this.nodesHolder.removeElements([edge]);
  }

  /**
   * Remove elements and delete connected edges and update connected nodes accordingly
   */
  public RemoveElements(elements: AnyElement[]): void {
    this.nodesHolder.removeElements(elements);
  }
  /**
   * Remove all elements (nodes and edges)
   */
  public RemoveAllElements() {
    this.RemoveElements(this.getElementsUseCase.getAllNodes());
  }

  /**
   * Find wether a function is correctly connected.
   * Does not test for text input yet, and nothing is done with the results. TODO
   */
  public testFunctionCorrectness(functionNode: FunctionNode): void {
    const args = functionNode.data?.args;
    if (!args) throw new Error('FunctionNode has no data');

    switch (functionNode.data?.functionType) {
      case FunctionTypes.GroupBy:
        const relation = this.getElementsUseCase.getNodeByHandle(
          functionNode,
          typeToFunctionHandle(FunctionArgTypes.relation),
        ) as RelationNode | undefined;
        const group = this.getElementsUseCase.getNodeByHandle(
          functionNode,
          typeToFunctionHandle(FunctionArgTypes.group),
        ) as EntityNode | FunctionNode | undefined;
        const by = this.getElementsUseCase.getNodeByHandle(
          functionNode,
          typeToFunctionHandle(FunctionArgTypes.by),
        ) as EntityNode | FunctionNode | undefined;
        if (!relation || !group || !by) return;
        else {
          const [left, right] = this.getElementsUseCase.getNodesConnectedToRelation(relation);
          if ((left == group && right == by) || (left == by && right == group)) return;
          else return;
        }
        return;
      default:
        throw new Error('Unsupported function in testFunctionCorrectness');
    }
  }
}
