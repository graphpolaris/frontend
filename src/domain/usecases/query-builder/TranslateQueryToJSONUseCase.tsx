/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  AttributeNode,
  EntityNode,
  RelationNode,
  FunctionNode,
  AnyNode,
  FunctionArgs,
  FunctionArg,
} from '../../entity/query-builder/structures/Nodes';
import {
  Entity,
  Relation,
  GroupBy,
  Constraint,
  TranslatedJSONQuery,
  MachineLearning,
} from '../../entity/query-builder/structures/TranslatedJSONQuery';
import NodesHolder from '../../entity/query-builder/models/NodesHolder';
import GetElementsUseCase from './GetElementsUseCase';
import {
  FunctionArgTypes,
  FunctionTypes,
} from '../../entity/query-builder/structures/FunctionTypes';
import { typeToFunctionHandle } from '../../entity/query-builder/structures/Handles';

/** UseCase for converting a ReactFlow query to our JSON standard format for sending query. */
export default class TranslateQueryToJSONUseCase {
  private nodesHolder: NodesHolder;
  private getElementsUseCase;

  constructor(nodesHolder: NodesHolder, getElementsUseCase: GetElementsUseCase) {
    this.nodesHolder = nodesHolder;
    this.getElementsUseCase = getElementsUseCase;
  }
  /**
   * Converts the ReactFlow query to a json data structure to send the query to the backend.
   * @returns {TranslatedJSONQuery} A JSON object in the `JSONFormat`.
   */
  public TranslateQueryToObject(): TranslatedJSONQuery {
    const result: TranslatedJSONQuery = {
      return: { entities: [], relations: [], groupBys: [] },
      entities: [],
      relations: [],
      groupBys: [],
      machineLearning: [],
      limit: 5000,
    };

    //prettier-ignore
    const entityNodes:   EntityNode[]   = this.getElementsUseCase.getEntityNodes();
    const relationNodes: RelationNode[] = this.getElementsUseCase.getRelationNodes();
    const functionNodes: FunctionNode[] = this.getElementsUseCase.getFunctionNodes();
    //note that attribute nodes are not processed here, but are detected if they are connected to any entity/relation/function node

    //add nodes to JSON query
    entityNodes.forEach((node) => {
      this.AddEntityToJsonObject(result, node);
    });
    relationNodes.forEach((node) => {
      this.AddRelationToJsonObject(result, node);
    });
    functionNodes.forEach((functionNode: FunctionNode) => {
      switch (functionNode.data?.functionType) {
        case FunctionTypes.GroupBy:
          this.AddGroupByToJsonObject(result, functionNode);
          break;
        case FunctionTypes.link:
          this.AddLinkPredictionToJsonObject(result, functionNode);
          break;
        case FunctionTypes.communityDetection:
          this.AddCommunityDetectionToJsonObject(result, functionNode);
          break;
        case FunctionTypes.centrality:
          this.AddCentralityToJsonObject(result, functionNode);
          break;
        case FunctionTypes.shortestPath:
          this.addShortestPathToJsonOBject(result, functionNode);
          break;
      }
    });
    return result;
  }
  /**
   * Adds a new entity field to the result jsonObject. And adds this entity index to the return field.
   * @param jsonObject The result object.
   * @param entityNode The entity node to be added.
   */
  private AddEntityToJsonObject(jsonObject: TranslatedJSONQuery, entityNode: EntityNode) {
    if (entityNode.data == undefined) throw new Error('Query-builder: No data in entity element!');

    // Create the entity object
    const newEntity: Entity = {
      name: entityNode.data.name,
      ID: Number(entityNode.id),
      constraints: this.createConstraints(entityNode),
    };

    // Add the entity to the result jsonObject with the correct index
    jsonObject.entities.push(newEntity);

    // TODO: only do this if we want to return this object
    jsonObject.return.entities.push(Number(entityNode.id));
  }
  /**
   * Adds a new relation field to the result jsonObject. Adds the correct to and from entities and adds this relation index to the return field.
   * @param jsonObject The result object.
   * @param relationNode The relation node to be added.
   */
  private AddRelationToJsonObject(jsonObject: TranslatedJSONQuery, relationNode: RelationNode) {
    if (relationNode.data == undefined)
      throw new Error('Query-builder: No data in relation element!');

    const [left, right] = this.getElementsUseCase.getNodesConnectedToRelation(relationNode);
    if (!left || !right || left.type == null || right.type == null) {
      return;
    }
    const leftType = this.GetConnectedNodesTypeForRelations(left.type);
    const rightType = this.GetConnectedNodesTypeForRelations(right.type);

    const newRelation: Relation = {
      ID: Number(relationNode.id),
      name: relationNode.data.collection,
      depth: relationNode.data.depth,
      fromType: leftType,
      fromID: Number(left.id),
      toType: rightType,
      toID: Number(right.id),
      constraints: this.createConstraints(relationNode),
    };

    // Add the relation to the result jsonObject with the new relation index
    jsonObject.relations.push(newRelation);

    // TODO: only do this if we want to return this object
    jsonObject.return.relations.push(Number(relationNode.id));
  }

  /**
   *
   * @param type The type of the connected node.
   * @returns The value the backend wants for this type of node
   */
  private GetConnectedNodesTypeForRelations(type: string): string {
    if (type == 'function') {
      // Function nodes should by a group by.
      // Because the function node can only be a group by, As of now.
      return 'groupBy';
    }
    // Other wise it should return the type (probably an entity).
    return type;
  }

  /**
   * Adds a new function field to the result jsonObject. And adds this function index to the return field.
   * @param jsonObject The result object.
   * @param entityNode The function node to be added.
   */
  private AddGroupByToJsonObject(jsonObject: TranslatedJSONQuery, functionNode: FunctionNode) {
    const groupArguments = this.getFunctionArguments(functionNode, FunctionArgTypes.group) as [
      [EntityNode | FunctionNode | undefined],
      string | undefined,
    ];
    const byArguments = this.getFunctionArguments(functionNode, FunctionArgTypes.by) as [
      [EntityNode | FunctionNode | undefined],
      string | undefined,
    ];
    const relationNode = this.getFunctionArgument(functionNode, FunctionArgTypes.relation)[0] as
      | RelationNode
      | undefined;
    const appliedModifier = this.getFunctionArgument(functionNode, FunctionArgTypes.modifier)[1] as
      | string
      | undefined;
    if (!groupArguments || !byArguments || !relationNode || !appliedModifier) {
      console.warn('A groupBy argument is not defined');
      return;
    }

    if (groupArguments[0][0]?.type == null || byArguments[0][0]?.type == null) {
      throw new Error('something is null');
    }

    let byArgumentType = byArguments[0][0]?.type;
    let groupArgumentType = groupArguments[0][0]?.type;
    if (byArgumentType == 'function') {
      byArgumentType = 'groupBy';
    }
    if (groupArgumentType == 'function') {
      groupArgumentType = 'groupBy';
    }

    let constrainNodes = this.createConstraints(functionNode);

    const newGroupBy: GroupBy = {
      ID: Number(functionNode.id),

      groupType: groupArgumentType,
      groupID: groupArguments[0].map((groupArgument) => {
        return Number(groupArgument?.id);
      }),
      groupAttribute: groupArguments[1] ? groupArguments[1] : '',

      byType: byArgumentType,
      byID: byArguments[0].map((byArgument) => {
        return Number(byArgument?.id);
      }),
      byAttribute: byArguments[1] ? byArguments[1] : '',

      appliedModifier: appliedModifier,
      relationID: Number(relationNode.id),
      constraints: constrainNodes,
    };

    // Add the function to the result jsonObject with the correct index
    jsonObject.groupBys.push(newGroupBy);
    // TODO: only do this if we want to return this object
    jsonObject.return.groupBys.push(Number(functionNode.id));
  }
  /** Adds LinPRrediction to the JSONObject */
  private AddLinkPredictionToJsonObject(
    jsonObject: TranslatedJSONQuery,
    functionNode: FunctionNode,
  ) {
    const newLinkPrediction: MachineLearning = {
      queuename: 'lpr_queue',
      parameters: [],
    };
    jsonObject.machineLearning.push(newLinkPrediction);
  }
  /** Adds CommunityDetection to the JSONObject */
  private AddCommunityDetectionToJsonObject(
    jsonObject: TranslatedJSONQuery,
    functionNode: FunctionNode,
  ) {
    const newCommunityDetection: MachineLearning = {
      queuename: 'cdt_queue',
      parameters: [],
    };
    jsonObject.machineLearning.push(newCommunityDetection);
  }
  /** Adds Centrality to the JSONObject */
  private AddCentralityToJsonObject(jsonObject: TranslatedJSONQuery, functionNode: FunctionNode) {
    const newCentrality: MachineLearning = {
      queuename: 'ctr_queue',
      parameters: [],
    };
    jsonObject.machineLearning.push(newCentrality);
  }
  /** Adds shortestPath to the JSONObject */
  private addShortestPathToJsonOBject(jsonObject: TranslatedJSONQuery, functionNode: FunctionNode) {
    const newShortestPath: MachineLearning = {
      queuename: 'stp_queue',
      parameters: ['', ''], // Add here if you wish for only source nodes or target nodes.
    };
    jsonObject.machineLearning.push(newShortestPath);
  }

  /**
   * Returns function argument
   */
  private getFunctionArgument(functionNode: FunctionNode, name: FunctionArgTypes) {
    const arg = functionNode.data?.args[name];
    if (!arg) throw new Error("Query-builder function: argument '" + name + "'not found");

    return [
      arg.connectable
        ? this.getElementsUseCase.getNodeByHandle(functionNode, typeToFunctionHandle(name))
        : undefined,
      arg.value,
    ];
  }

  /**
   * Returns function arguments
   * @param functionNode The function node to look for
   * @param name The argument name
   * @returns A list of all nodes that are connected to this argument.
   */
  private getFunctionArguments(functionNode: FunctionNode, name: FunctionArgTypes) {
    const arg = functionNode.data?.args[name];
    if (!arg) throw new Error("Query-builder function: argument '" + name + "'not found");

    return [
      arg.connectable
        ? this.getElementsUseCase.getNodesByHandle(functionNode, typeToFunctionHandle(name))
        : undefined,
      arg.value,
    ];
  }

  /**
   * Creates a constraint object to be added to the result object.
   * @param attributeNode The attribute node to create the constraint from.
   * @returns {Constraint} A constraint object.
   */
  private createConstraints(node: AnyNode): Constraint[] {
    return this.getElementsUseCase
      .getAllConstraints(node)
      .map((attributeNode) => this.createConstraint(attributeNode));
  }

  private createConstraint(attributeNode: AttributeNode): Constraint {
    if (attributeNode.data == undefined)
      throw new Error('Query-builder: No data in attribute element!');

    const constraint: Constraint = {
      attribute: attributeNode.data.attribute,
      value: attributeNode.data.value,
      dataType: attributeNode.data.dataType,
      matchType: attributeNode.data.matchType,
    };
    return constraint;
  }
}
