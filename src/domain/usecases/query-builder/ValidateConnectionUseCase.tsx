/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from '../../entity/broker/broker';
import BrokerListener from '../../../domain/entity/broker/BrokerListenerInterface';
import {
  AnyNode,
  AttributeNode,
  EntityNode,
  RelationNode,
  FunctionNode,
  QueryElementTypes,
} from '../../entity/query-builder/structures/Nodes';
import ParseConstraintsUseCase from './ParseConstraintsUseCase';
import distanceQueryElementUseCase from './DistanceQueryElementUseCase';
import NodesHolder from '../../entity/query-builder/models/NodesHolder';
import {
  functionHandleToType,
  Handles,
  isFunctionHandle,
  typeToFunctionHandle,
} from '../../entity/query-builder/structures/Handles';
import GetElementsUseCase from './GetElementsUseCase';
import {
  FunctionArgTypes,
  FunctionTypes,
} from '../../entity/query-builder/structures/FunctionTypes';
import { DragHandle } from '@material-ui/icons';
import { isEntityNode, isRelationNode } from '../../entity/query-builder/structures/IsNodeType';
import { Attribute } from '../../entity/graph-schema/structures/InputDataTypes';
import { isSchemaResult } from '../../entity/graph-schema/structures/SchemaResultType';

// TODO: make this called from isValidConnection on nodes

/**
 * ValidateConnectionUseCase uses the data from the schema to validate connections in the query panel.
 * The schema data is used to make dictionaries to validate these connections.
 */
export default class ValidateConnectionUseCase implements BrokerListener {
  private parseConstraintsUseCase: ParseConstraintsUseCase;
  private attributeDict: Map<string, Attribute[]>;
  private fromDict: Map<string, string>;
  private toDict: Map<string, string>;
  private nodesHolder: NodesHolder;
  private getElementsUseCase: GetElementsUseCase;

  constructor(nodesHolder: NodesHolder, getElementsUseCase: GetElementsUseCase) {
    this.parseConstraintsUseCase = new ParseConstraintsUseCase();
    this.attributeDict = new Map<string, Attribute[]>();
    this.fromDict = new Map<string, string>();
    this.toDict = new Map<string, string>();
    this.nodesHolder = nodesHolder;
    this.getElementsUseCase = getElementsUseCase;
    Broker.instance().subscribe(this, 'schema_result');
  }

  /**
   * Consumes the message received from the broker.
   * Uses the data from the schema to create three dictionaries that can be used to validate queries.
   * @param jsonObject Contains the schema received from the backend.
   */
  public consumeMessageFromBackend(jsonObject: unknown): void {
    if (isSchemaResult(jsonObject)) {
      this.attributeDict = this.parseConstraintsUseCase.createAttributeDict(jsonObject);
      this.fromDict = this.parseConstraintsUseCase.createFromDict(jsonObject);
      this.toDict = this.parseConstraintsUseCase.createToDict(jsonObject);
    }
  }

  /**
   * ValidateConnection checks if the connection being made between two elements of the query panel are valid.
   * It uses the type of the sourceNode and the type of the targetNode to decide what kind of validation is necessary.
   * @param sourceNode The source of the connection being made.
   * @param targetNode The target of the connection being made.
   * @returns {boolean} True if the connection is valid, false if not.
   */
  public validateConnection(
    sourceNode: AnyNode,
    sourceHandle: string,
    targetNode: AnyNode,
    targetHandle: string,
  ): boolean {
    const unsupportedNodeError = new Error(
      'NodeType not supported in validate ' + sourceNode.type + targetNode.type,
    );
    // prettier-ignore
    switch (sourceNode.type) {

    case QueryElementTypes.Entity: switch (targetNode.type) {

        case QueryElementTypes.Relation: return (
          sourceHandle == Handles.ToRelation &&
          (targetHandle == Handles.RelationLeft || targetHandle == Handles.RelationRight) &&
          this.validateRelation(sourceNode as EntityNode, targetNode as RelationNode, targetHandle)
        )        
        case QueryElementTypes.Entity   :
        case QueryElementTypes.Attribute:
        case QueryElementTypes.Function :  return false;
        default: throw unsupportedNodeError        
    }

    case QueryElementTypes.Relation: switch (targetNode.type) {

        case QueryElementTypes.Entity:    return false;
        case QueryElementTypes.Relation:  return false;
        case QueryElementTypes.Attribute: return false;
        case QueryElementTypes.Function:  return false;
        default: throw unsupportedNodeError
    }

    case QueryElementTypes.Attribute: switch (targetNode.type) {  

      case QueryElementTypes.Attribute: switch (targetNode.type) {
        case QueryElementTypes.Attribute: return false;
        case QueryElementTypes.Relation: 
        case QueryElementTypes.Entity:
        case QueryElementTypes.Function : return targetHandle == Handles.ToAttributeHandle && this.validateAttribute(sourceNode as AttributeNode, targetNode);        default: throw unsupportedNodeError
      }

    case QueryElementTypes.Function: switch (targetNode.type) {

        case QueryElementTypes.Function :
        case QueryElementTypes.Entity   :
        case QueryElementTypes.Relation : return (
          isFunctionHandle(sourceHandle) &&
          this.validateFunctionConnection(sourceNode as FunctionNode, targetNode as EntityNode|RelationNode|FunctionNode, functionHandleToType(sourceHandle), targetHandle)
        )
        case QueryElementTypes.Attribute: return false;
        default: throw unsupportedNodeError
    }

    default: throw unsupportedNodeError
    }
  }

  /**
   * Validates the connection with an Attribute.
   * @param targetNode The attribute node.
   * @param sourceNode The node connected to the attribute node.
   * @returns {boolean} True if the connection would be valid, false if not.
   */
  private validateAttribute(attributeNode: AttributeNode, targetNode: AnyNode): boolean {
    switch (targetNode.type) {
      case QueryElementTypes.Function:
        return this.validateAttributeConnectionToFunction(
          attributeNode,
          targetNode as FunctionNode,
        );
      default:
        return this.validateAttributeConnection(
          attributeNode,
          targetNode as EntityNode | RelationNode,
        );
    }
  }

  /**
   * Validates the connection between an attribute and an entity or relation node.
   * The function checks if the attributeNode is in the list of attributes of targetNode.
   * Note that parameterType are AnyNode, but using wrong node types will give errors.
   * This was done only to prevent numerous type assertions being required in validateConnection, for which this is a helper function.
   * @param attributeNode The attribute connected to the entity or relation node.
   * @param targetNode The FunctionNode node.
   * @returns {boolean} True if the connection would be valid, false if not.
   */
  private validateAttributeConnectionToFunction(
    attributeNode: AttributeNode,
    targetNode: FunctionNode,
  ): boolean {
    // TODO: Check if it represents the correct function (a.k.a. a GroupBy for the correct entity!!!)
    if (targetNode.data?.functionType == FunctionTypes.GroupBy) {
      return true;
    }
    return false;
  }

  /**
   * Validates the connection between an attribute and an entity or relation node.
   * The function checks if the attributeNode is in the list of attributes of targetNode.
   * Note that parameterType are AnyNode, but using wrong node types will give errors.
   * This was done only to prevent numerous type assertions being required in validateConnection, for which this is a helper function.
   * @param attributeNode The attribute connected to the entity or relation node.
   * @param targetNode The entity or relation node.
   * @returns {boolean} True if the connection would be valid, false if not.
   */
  private validateAttributeConnection(
    attributeNode: AttributeNode,
    targetNode: EntityNode | RelationNode,
  ): boolean {
    let entityID;
    entityID = targetNode.data?.name;

    if (!this.attributeDict.has(entityID as string)) {
      throw new Error('node not found in constraint dictionary!');
    } else {
      const attributeList = this.attributeDict.get(entityID as string) as Attribute[];

      for (let attribute of attributeList) {
        if (attribute.name == attributeNode.data?.attribute) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Validates that an entity node is connected to the correct side of a relation node.
   * @param relationNode The relation node.
   * @param entityNode The entity node connected to the relation node.
   * @returns {boolean} True if the connection would be valid, false if not.
   */
  private validateRelation(
    entityNode: EntityNode,
    relationNode: RelationNode,
    handle: string,
  ): boolean {
    let relationID = relationNode.data?.name;
    if (!relationID) throw new Error(relationID + ' : relationID not found on relation');
    const dict = this.getDict(handle);
    if (!dict.has(relationID)) {
      throw new Error(relationID + ' : Relation node not found in constraint dictionary!');
    }
    const entity = dict.get(relationID) as string;
    if (!(entity == entityNode.data?.name)) return false;

    const [left, right] = this.getElementsUseCase.getNodesConnectedToRelation(relationNode);
    switch (handle) {
      case Handles.RelationLeft:
        return typeof left == 'undefined';
      case Handles.RelationRight:
        return typeof right == 'undefined';
      default:
        throw new Error('Not supposed to get this non-entity-relation handle in validateRelation');
    }
  }

  /**
   * Validates that an function node is a node that is a node that is able to connect to the othernode.
   * @param othernode The othernode node.
   * @param functionNode The function node that needs validation.
   * @returns {boolean} True if the connection would be valid, false if not.
   */
  private validateFunctionConnection(
    functionNode: FunctionNode,
    otherNode: EntityNode | RelationNode | FunctionNode,
    sourceHandleAsType: FunctionArgTypes,
    targetHandle: string,
  ): boolean {
    switch (functionNode.data?.functionType) {
      case FunctionTypes.GroupBy:
        return this.validateGroupBy(functionNode, otherNode, sourceHandleAsType, targetHandle);
      default:
        throw new Error('Validating for unsupported function');
    }
  }

  /** Validates wheter the connections made to the groupby are valid
   * @param functionNode The groupbynode
   * @param otherNode the other node
   * @param handle The handle you wish to connect to (groupby has 4 of them)
   * @param targetHandle The handle of the othernode.
   */
  private validateGroupBy(
    functionNode: FunctionNode,
    otherNode: EntityNode | RelationNode | FunctionNode,
    handle: FunctionArgTypes,
    targetHandle: string,
  ) {
    switch (handle) {
      case FunctionArgTypes.group:
      case FunctionArgTypes.by:
        return (
          (targetHandle == Handles.ReceiveFunction && isEntityNode(otherNode)) ||
          (isFunctionHandle(targetHandle) &&
            functionHandleToType(targetHandle) == FunctionArgTypes.result)
        );
      case FunctionArgTypes.relation:
        return targetHandle == Handles.ReceiveFunction && isRelationNode(otherNode);
      case FunctionArgTypes.result:
        return (
          (targetHandle == Handles.RelationLeft || targetHandle == Handles.RelationRight) &&
          isRelationNode(otherNode)
        );
      default:
        throw new Error('Unsupported handle validating on groupBy: ' + handle);
    }
  }

  /**
   * Chooses either the from or to dictionary.
   * @param dict The name of the dictionary.
   * @returns {Map<string, string>} The chosen dictionary.
   */
  private getDict(dict: string): Map<string, string> {
    if (dict == Handles.RelationRight) {
      return this.toDict;
    } else return this.fromDict;
  }
}
