/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { mockElements } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import NodesHolder from '../../../entity/query-builder/models/NodesHolder';
import {
  AnyNode,
  AttributeNode,
  EntityNode,
  QueryElementTypes,
} from '../../../entity/query-builder/structures/Nodes';
import distanceQueryElementUseCase from '../DistanceQueryElementUseCase';

/** Testsuite for the distance query element use case */
describe('distanceQueryElementUseCase', () => {
  it('should calculate the distance between nodes correctly', () => {
    const node1 = mockElements.nodes[0];
    node1.position = { x: 0, y: 30 };
    const node2 = mockElements.nodes[1];
    node2.position = { x: 50, y: 150 };
    const distance = distanceQueryElementUseCase.calcDistance(node1, node2);
    expect(distance).toEqual(130);

    const node3 = mockElements.nodes[3];
    node3.position = { x: 30, y: 30 };
    const node4 = mockElements.nodes[4];
    node4.position = { x: 60, y: 70 };
    const distance2 = distanceQueryElementUseCase.calcDistance(node3, node4);
    expect(distance2).toEqual(50);
  });

  it('should calculate the distance between nodes and all other nodes', () => {
    const node1 = mockElements.nodes[0];
    node1.position = { x: 0, y: 30 };
    const node2 = mockElements.nodes[1];
    node2.position = { x: 50, y: 150 };
    const node3 = mockElements.nodes[6];
    node3.position = { x: 0, y: 150 };
    const node4 = mockElements.nodes[8];
    node4.position = { x: 50, y: 30 };
    const distances = distanceQueryElementUseCase.calcDistancesTo(node1, [node2, node3, node4]);
    expect(distances).toEqual([
      [130, node2],
      [120, node3],
      [50, node4],
    ]);
  });
});
