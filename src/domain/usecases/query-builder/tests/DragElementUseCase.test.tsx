/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { mockElements } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import NodesHolder from '../../../entity/query-builder/models/NodesHolder';
import DragElementUseCase, { closeByDistance } from '../DragElementUseCase';
import ValidateConnectionUseCase from '../ValidateConnectionUseCase';
import {
  EntityNode,
  QueryElementTypes,
  RelationNode,
} from '../../../entity/query-builder/structures/Nodes';
import SetElementsUseCase from '../SetElementsUseCase';
import airportsSchema from '../../../../data/mock-data/schema-result/AirportsSchemaResult';
import GetElementsUseCase from '../GetElementsUseCase';

/** Testsuite for the drag element use case */
describe('dragelementusecase', () => {
  let nodesHolder: NodesHolder;
  let entityNodes: EntityNode[] = [];
  let relationNodes: RelationNode[] = [];
  let getElementsUseCase: GetElementsUseCase;
  let validateConnectionUseCase: ValidateConnectionUseCase;
  let setElementsUseCase: SetElementsUseCase;
  let dragElementUseCase: DragElementUseCase;
  beforeEach(() => {
    nodesHolder = new NodesHolder([]);
    entityNodes = mockElements.nodes.filter(
      (node) => node.type == QueryElementTypes.Entity,
    ) as EntityNode[];
    relationNodes = mockElements.nodes.filter(
      (node) => node.type == QueryElementTypes.Relation,
    ) as RelationNode[];
    getElementsUseCase = new GetElementsUseCase(nodesHolder);
    validateConnectionUseCase = new ValidateConnectionUseCase(nodesHolder, getElementsUseCase);
    validateConnectionUseCase.consumeMessageFromBackend(airportsSchema);
    setElementsUseCase = new SetElementsUseCase(nodesHolder, getElementsUseCase);
    dragElementUseCase = new DragElementUseCase(
      validateConnectionUseCase,
      setElementsUseCase,
      getElementsUseCase,
      nodesHolder,
    );
  });
  it('testing ondrag functionality', () => {
    (entityNodes[0].position = { x: 0, y: 0 }),
      (relationNodes[0].position = { x: 0, y: 5 }),
      setElementsUseCase.addNode(entityNodes[0]);
    setElementsUseCase.addNode(relationNodes[0]);

    const result = dragElementUseCase.findConnectionOptionsAndProximity(entityNodes[0]);

    // We expect 2 results since it can both connect to the left and on the right since this is how the flights database works.
    const expectedResult = [
      [
        5,
        {
          id: '1',
          type: 'entity',
          data: {
            fadeIn: false,
            name: 'airports',
          },
          position: { x: 0, y: 0 },
        },
        'relationsHandle',
        {
          id: '5',
          type: 'relation',
          data: {
            collection: 'flights',
            depth: { max: 1, min: 1 },
            fadeIn: false,
            name: 'flights',
          },
          position: { x: 0, y: 5 },
        },
        'rightEntityHandle',
      ],
      [
        5,
        {
          id: '1',
          type: 'entity',
          data: { fadeIn: false, name: 'airports' },
          position: { x: 0, y: 0 },
        },
        'relationsHandle',
        {
          id: '5',
          type: 'relation',
          data: {
            collection: 'flights',
            depth: { max: 1, min: 1 },
            fadeIn: false,
            name: 'flights',
          },
          position: { x: 0, y: 5 },
        },
        'leftEntityHandle',
      ],
    ];
    expect(result).toEqual(expectedResult);

    dragElementUseCase.OnDragStop(entityNodes[0]); // Should create an edge
    expect(getElementsUseCase.getConnectedEdges(entityNodes[0]).length).toEqual(1);
    expect(getElementsUseCase.getConnectedEdges(relationNodes[0]).length).toEqual(1);
  });
  it('testing while already connected', () => {
    (entityNodes[0].position = { x: 0, y: 0 }),
      (relationNodes[0].position = { x: 0, y: 5 }),
      setElementsUseCase.addNode(entityNodes[0]);
    setElementsUseCase.addNode(relationNodes[0]);
    setElementsUseCase.connectEntityToRelation(entityNodes[0], relationNodes[0], false);

    const result = dragElementUseCase.findConnectionOptionsAndProximity(entityNodes[0]);
    const expectedResult = [
      [
        5,
        {
          id: '1',
          type: 'entity',
          data: { fadeIn: false, name: 'airports' },
          position: { x: 0, y: 0 },
        },
        'relationsHandle',
        {
          id: '5',
          type: 'relation',
          data: {
            collection: 'flights',
            depth: { max: 1, min: 1 },
            fadeIn: false,
            name: 'flights',
          },
          position: { x: 0, y: 5 },
        },
        'rightEntityHandle',
      ],
    ];
    expect(result).toEqual(expectedResult);

    dragElementUseCase.OnDragStop(entityNodes[0]); // Should create an edge
    expect(getElementsUseCase.getConnectedEdges(entityNodes[0]).length).toEqual(2);
    expect(getElementsUseCase.getConnectedEdges(relationNodes[0]).length).toEqual(2);
  });

  it('testing distancelimit', () => {
    (entityNodes[0].position = { x: 0, y: 0 }),
      (relationNodes[0].position = { x: 0, y: closeByDistance + 5 }), // Should be farther than the maximum distance
      setElementsUseCase.addNode(entityNodes[0]);
    setElementsUseCase.addNode(relationNodes[0]);

    const result = dragElementUseCase.findConnectionOptionsAndProximity(entityNodes[0]);
    // We expect 0 results since its over the distance limit
    expect(result).toEqual([]);
    dragElementUseCase.OnDragStop(entityNodes[0]); // Shouldn't create an edge
    expect(getElementsUseCase.getConnectedEdges(entityNodes[0]).length).toEqual(0);
    expect(getElementsUseCase.getConnectedEdges(relationNodes[0]).length).toEqual(0);
  });

  it('Drag start doing nothing', () => {
    dragElementUseCase.OnDragStart(entityNodes[0]);
  });
  it('Drag doing nothing', () => {
    dragElementUseCase.OnDrag(entityNodes[0]);
  });
});
