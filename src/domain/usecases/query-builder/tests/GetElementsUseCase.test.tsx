/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { getConnectedEdges, Node } from 'react-flow-renderer';
import { mockElements } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import SetElementsUseCase from '../SetElementsUseCase';
import GetElementsUseCase from '../GetElementsUseCase';
import NodesHolder from '../../../entity/query-builder/models/NodesHolder';
import { QueryElementTypes } from '../../../entity/query-builder/structures/Nodes';
import EntityNode from '../../../../presentation/view/graph-schema/flow-components/nodes/EntityNode';

/** Testsuite to test the NodeHolder model */
describe('GetElementsUseCase', () => {
  let nodesHolder: NodesHolder;
  let getElementsUseCase: GetElementsUseCase;
  let setElementsUsecase: SetElementsUseCase;
  let entityElements: Node[];
  let relationElements: Node[];
  let attributeElements: Node[];
  let functionElements: Node[];

  beforeEach(() => {
    // Initialize a nodesHolder
    nodesHolder = new NodesHolder(mockElements.nodes);
    getElementsUseCase = new GetElementsUseCase(nodesHolder);
    setElementsUsecase = new SetElementsUseCase(nodesHolder, getElementsUseCase);
    // Collect all the entity, relation and attributeElements from the start elements
    entityElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Entity);
    relationElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Relation);
    attributeElements = mockElements.nodes.filter(
      (node) => node.type == QueryElementTypes.Attribute,
    );
    functionElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Function);
  });

  it('should hold the same amount of nodes as the amount of nodes given as input', () => {
    expect(entityElements.length).toEqual(getElementsUseCase.getEntityNodes().length);
    expect(relationElements.length).toEqual(getElementsUseCase.getRelationNodes().length);
    expect(attributeElements.length).toEqual(getElementsUseCase.getAttributeNodes().length);
    expect(functionElements.length).toEqual(getElementsUseCase.getFunctionNodes().length);
  });

  it('should hold the same amount of nodes as the amount of edges given as input', () => {
    expect(mockElements.nodes.length).toEqual(getElementsUseCase.getAllNodes().length);
  });

  it('should hold the same amount of edges given as input', () => {
    expect(mockElements.links.length).toEqual(getElementsUseCase.getAllEdges().length);
  });

  it('should hold the same the amount of elements given as input', () => {
    expect(mockElements.links.length + mockElements.nodes.length).toEqual(
      getElementsUseCase.getAllElements().length,
    );
  });

  it('should correctly retrieve a node by ID', () => {
    entityElements.forEach((entityElement) => {
      const entityNode = getElementsUseCase.getNodeByID(entityElement.id);
      expect(entityElement).toEqual(entityNode);
    });
    relationElements.forEach((relationElement) => {
      const relationNode = getElementsUseCase.getNodeByID(relationElement.id);
      expect(relationElement).toEqual(relationNode);
    });
    attributeElements.forEach((attributeElement) => {
      const attributeNode = getElementsUseCase.getNodeByID(attributeElement.id);
      expect(attributeElement).toEqual(attributeNode);
    });
    functionElements.forEach((functionElement) => {
      const functionNode = getElementsUseCase.getNodeByID(functionElement.id);
      expect(functionElement).toEqual(functionNode);
    });

    // Also check if we give undefined for a nonexisting node id
    expect(() =>
      getElementsUseCase.getNodeByID('yesyesyesthisnodewillprobablynotexistttttdt'),
    ).toThrowError('NodeID not found in NodesHolder');
  });
  it('should get the correct edges with the correct filters', () => {
    const entitynode = entityElements[0];
    const relationNode0 = relationElements[0];
    const relationNode1 = relationElements[1];
    const attributeNode = attributeElements[0];
    setElementsUsecase.connectEntityToRelation(entitynode, relationNode0, true); // Relation will be target
    setElementsUsecase.connectEntityToRelation(entitynode, relationNode1, false); // Relation will be target
    setElementsUsecase.connectAttribute(attributeNode, entitynode); // Entity wil be target

    //getInEdges test
    const result1 = getElementsUseCase.getInEdges(entitynode);
    expect(result1).toEqual(
      getElementsUseCase.getAllEdges().filter((edge) => edge.target == entitynode.id),
    );
    expect(result1.length).toEqual(1);

    //getOutEdges test
    const result2 = getElementsUseCase.getOutEdges(entitynode);
    expect(result2).toEqual(
      getElementsUseCase.getAllEdges().filter((edge) => edge.source == entitynode.id),
    );
    expect(result2.length).toEqual(2);

    //getInNodes test
    const result3 = getElementsUseCase.getInNodes(entitynode);
    expect(result3).toEqual([attributeNode]);
    expect(result3.length).toEqual(1);

    //getOutNodes test
    const result4 = getElementsUseCase.getOutNodes(entitynode);
    expect(result4).toEqual([relationNode0, relationNode1]);
    expect(result4.length).toEqual(2);
  });
  it('should correctly retrieve all edge connected to a node', () => {
    const entitynode = entityElements[0];
    setElementsUsecase.connectEntityToRelation(entitynode, relationElements[0], false);
    expect(getElementsUseCase.getConnectedEdges(entitynode).length).toEqual(1);
    setElementsUsecase.connectEntityToRelation(entitynode, relationElements[1], true);
    expect(getElementsUseCase.getConnectedEdges(entitynode).length).toEqual(2);
    setElementsUsecase.connectAttribute(attributeElements[0], entitynode);
    expect(getElementsUseCase.getConnectedEdges(entitynode).length).toEqual(3);
    //TODO add connect function here.
  });
});
