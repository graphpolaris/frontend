import { getConnectedEdges, Node } from 'react-flow-renderer';
import { mockElements } from '../../../../data/mock-data/query-builder/mockData';
import SetElementsUseCase from '../../../usecases/query-builder/SetElementsUseCase';
import GetElementsUseCase from '../../../usecases/query-builder/GetElementsUseCase';
import NodesHolder from '../../../entity/query-builder/models/NodesHolder';
import { QueryElementTypes } from '../../../entity/query-builder/structures/Nodes';
import EntityNode from '../../../../presentation/view/graph-schema/flow-components/nodes/EntityNode';

/** Testsuite to test the NodeHolder model */
describe('GetElementsUseCase', () => {
  let nodesHolder: NodesHolder;
  let getElementsUseCase: GetElementsUseCase;
  let setElementsUsecase: SetElementsUseCase;
  let entityElements: Node[];
  let relationElements: Node[];
  let attributeElements: Node[];
  let functionElements: Node[];

  beforeEach(() => {
    // Initialize a nodesHolder
    nodesHolder = new NodesHolder(mockElements.nodes);
    getElementsUseCase = new GetElementsUseCase(nodesHolder);
    setElementsUsecase = new SetElementsUseCase(nodesHolder,getElementsUseCase);
    // Collect all the entity, relation and attributeElements from the start elements
    entityElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Entity);
    relationElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Relation);
    attributeElements = mockElements.nodes.filter(
      (node) => node.type == QueryElementTypes.Attribute,
    );
    functionElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Function);
  });

  it('should hold the same amount of nodes as the amount of nodes given as input', () => {
    expect(entityElements.length).toEqual(getElementsUseCase.getEntityNodes().length);
    expect(relationElements.length).toEqual(getElementsUseCase.getRelationNodes().length);
    expect(attributeElements.length).toEqual(getElementsUseCase.getAttributeNodes().length);
    expect(functionElements.length).toEqual(getElementsUseCase.getFunctionNodes().length);
  });

  it('should hold the same amount of nodes as the amount of edges given as input', () => {
    expect(mockElements.nodes.length).toEqual(getElementsUseCase.getAllNodes().length);
  });

  it('should hold the same amount of edges given as input', () => {
    expect(mockElements.links.length).toEqual(getElementsUseCase.getAllEdges().length);
  });

  it('should hold the same the amount of elements given as input', () => {
    expect(mockElements.links.length + mockElements.nodes.length).toEqual(getElementsUseCase.getAllElements().length);
  });


  it('should correctly retrieve a node by ID', () => {
    entityElements.forEach((entityElement) => {
      const entityNode = getElementsUseCase.getNodeByID(entityElement.id);
      expect(entityElement).toEqual(entityNode);
    });
    relationElements.forEach((relationElement) => {
      const relationNode = getElementsUseCase.getNodeByID(relationElement.id);
      expect(relationElement).toEqual(relationNode);
    });
    attributeElements.forEach((attributeElement) => {
      const attributeNode = getElementsUseCase.getNodeByID(attributeElement.id);
      expect(attributeElement).toEqual(attributeNode);
    });
    functionElements.forEach((functionElement) => {
      const functionNode = getElementsUseCase.getNodeByID(functionElement.id);
      expect(functionElement).toEqual(functionNode);
    });

    // Also check if we give undefined for a nonexisting node id
    expect(() =>
      getElementsUseCase.getNodeByID('yesyesyesthisnodewillprobablynotexistttttdt'),
    ).toThrowError('NodeID not found in NodesHolder');
  });

  it('should correctly retrieve all edge connected to a node', () => 
  {
    const entitynode = entityElements[0];
    setElementsUsecase.connectEntityToRelation(entitynode,relationElements[0],false);
    expect(getElementsUseCase.getConnectedEdges(entitynode).length).toEqual(1);
    setElementsUsecase.connectEntityToRelation(entitynode,relationElements[1],true);
    expect(getElementsUseCase.getConnectedEdges(entitynode).length).toEqual(2);
    setElementsUsecase.connectAttribute(attributeElements[0],entitynode);
    expect(getElementsUseCase.getConnectedEdges(entitynode).length).toEqual(3);
    //TODO add connect function here.
  });
});