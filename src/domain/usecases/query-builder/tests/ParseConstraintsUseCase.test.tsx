/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import ParseConstraintsUseCase from '../ParseConstraintsUseCase';
import { Attribute } from '../../../entity/graph-schema/structures/InputDataTypes';
import { Edge, Node } from '../../../entity/graph-schema/structures/InputDataTypes';

/** Testsuite for the parse constraints use case */
describe('ParseConstraint use case', () => {
  beforeEach(() => jest.resetModules());

  const parser = new ParseConstraintsUseCase();

  it('should correctly create constraint dict', () => {
    const receiveddict = parser.createAttributeDict(schema);

    let attributeDict: Map<string, Attribute[]>;
    attributeDict = new Map<string, Attribute[]>();
    attributeDict.set('Airport', [
      { name: 'city', type: 'string' },
      { name: 'vip', type: 'bool' },
      { name: 'state', type: 'string' },
    ]);
    attributeDict.set('Airport2', [
      { name: 'city', type: 'string' },
      { name: 'vip', type: 'bool' },
      { name: 'state', type: 'string' },
    ]);
    attributeDict.set('Staff', []);
    attributeDict.set('flights', [
      { name: 'arrivalTime', type: 'int' },
      { name: 'departureTime', type: 'int' },
    ]);
    attributeDict.set('employs', [{ name: 'salary', type: 'int' }]);
    expect(receiveddict).toEqual(attributeDict);
  });

  it('should correctly create from dict', () => {
    const receiveddict = parser.createFromDict(schema);
    let fromDict: Map<string, string>;
    fromDict = new Map<string, string>();
    fromDict.set('flights', 'Airport2');
    fromDict.set('employs', 'Airport');
    expect(receiveddict).toEqual(fromDict);
  });

  it('should correctly create to dict', () => {
    const receiveddict = parser.createToDict(schema);
    let toDict: Map<string, string>;
    toDict = new Map<string, string>();

    toDict.set('flights', 'Airport');
    toDict.set('employs', 'Staff');
    expect(receiveddict).toEqual(toDict);
  });

  const nodes: Node[] = [
    {
      name: 'Airport',
      attributes: [
        { name: 'city', type: 'string' },
        { name: 'vip', type: 'bool' },
        { name: 'state', type: 'string' },
      ],
    },
    {
      name: 'Airport2',
      attributes: [
        { name: 'city', type: 'string' },
        { name: 'vip', type: 'bool' },
        { name: 'state', type: 'string' },
      ],
    },
    { name: 'Staff', attributes: [] },
  ];

  const edges: Edge[] = [
    {
      name: 'Airport2:Airport',
      to: 'Airport',
      from: 'Airport2',
      collection: 'flights',
      attributes: [
        { name: 'arrivalTime', type: 'int' },
        { name: 'departureTime', type: 'int' },
      ],
    },
    {
      name: 'Airport:Staff',
      to: 'Staff',
      from: 'Airport',
      collection: 'employs',
      attributes: [{ name: 'salary', type: 'int' }],
    },
  ];

  const schema = {
    nodes: nodes,
    edges: edges,
  };
});
