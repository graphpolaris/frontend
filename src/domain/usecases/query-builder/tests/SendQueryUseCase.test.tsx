/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BackendMessengerMock from '../../../../data/drivers/backend-messenger/BackendMessengerMock';
import SendQueryUseCase from '../SendQueryUseCase';

/** Testsuite for the send query use case */
describe('SendQueryUseCase', () => {
  const backendMessengerMock = new BackendMessengerMock('PlaceHolderUrl');
  const sendQueryUseCase = new SendQueryUseCase(backendMessengerMock);

  it('should return true on successfull request', () => {
    sendQueryUseCase
      .SendQuery('validjsonquery')
      .then((resp) => expect(resp).toBe(true))
      .catch(() => fail());
  });

  it('should catch and log an error', async () => {
    try {
      await sendQueryUseCase.SendQuery('notvalidjsonquery');
    } catch (error) {
      expect(error).toEqual('invalid body');
    }
  });

  it('should throw on server error', async () => {
    try {
      await sendQueryUseCase.SendQuery('no200response');
    } catch (error) {
      expect(error).toEqual('Request to execute query unsuccessful!');
    }
  });
});
