/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Edge, Node } from 'react-flow-renderer';
import { mockElements } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import SetElementsUseCase from '../../../usecases/query-builder/SetElementsUseCase';
import GetElementsUseCase from '../../../usecases/query-builder/GetElementsUseCase';
import NodesHolder from '../../../entity/query-builder/models/NodesHolder';
import {
  AttributeData,
  AttributeNode,
  EntityData,
  EntityNode,
  FunctionData,
  FunctionNode,
  QueryElementTypes,
  RelationData,
  RelationNode,
} from '../../../entity/query-builder/structures/Nodes';
import { NodePosition } from '../../../entity/query-builder/structures/NodePosition';
import { FunctionTypes } from '../../../entity/query-builder/structures/FunctionTypes';

/** Testsuite to test the NodeHolder model */
describe('SetElementsUseCase', () => {
  let nodesHolder: NodesHolder;
  let getElementsUseCase: GetElementsUseCase;
  let setElementsUseCase: SetElementsUseCase;
  let entityElements: Node[];
  let relationElements: Node[];
  let attributeElements: Node[];
  let functionElements: Node[];

  beforeEach(() => {
    // Initialize a nodesHolder
    nodesHolder = new NodesHolder(mockElements.nodes);
    getElementsUseCase = new GetElementsUseCase(nodesHolder);
    setElementsUseCase = new SetElementsUseCase(nodesHolder, getElementsUseCase);
    // Collect all the entity, relation and attributeElements from the start elements
    entityElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Entity);
    relationElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Relation);
    attributeElements = mockElements.nodes.filter(
      (node) => node.type == QueryElementTypes.Attribute,
    );
    functionElements = mockElements.nodes.filter((node) => node.type == QueryElementTypes.Function);
  });

  it('it should correctly add entity nodes', () => {
    setElementsUseCase.RemoveAllElements();
    const entityNode = entityElements[0];
    setElementsUseCase.addNode(entityNode);
    expect(getElementsUseCase.getAllNodes()).toContain(entityNode);
  });
  it('it should correctly add relation nodes', () => {
    setElementsUseCase.RemoveAllElements();
    const relationNode = relationElements[0];
    setElementsUseCase.addNode(relationNode);
    expect(getElementsUseCase.getAllNodes()).toContain(relationNode);
  });
  it('it should correctly add attribute nodes', () => {
    setElementsUseCase.RemoveAllElements();
    const attributeNode = attributeElements[0];
    setElementsUseCase.addNode(attributeNode);
    expect(getElementsUseCase.getAllNodes()).toContain(attributeNode);
  });
  it('it should correctly add function nodes', () => {
    setElementsUseCase.RemoveAllElements();
    const functionNode = functionElements[0];
    setElementsUseCase.addNode(functionNode);
    expect(getElementsUseCase.getAllNodes()).toContain(functionNode);
  });

  it('should create edges on attribute connection to entitynodes ', () => {
    const node1 = getElementsUseCase.getNodeByID('1') as EntityNode;
    const node2 = getElementsUseCase.getNodeByID('7') as AttributeNode;
    setElementsUseCase.connectAttribute(node2, node1);
    expect(getElementsUseCase.getAllEdges().length).toEqual(1);
    const creatededge = getElementsUseCase.getAllEdges()[0] as Edge<any>;
    const expectedResult: Edge<any> = {
      id: (mockElements.nodes.length + 1).toString(),
      source: '7',
      sourceHandle: 'onAttributeHandle',
      target: '1',
      targetHandle: 'attributesHandle',
      animated: false,
    };
    expect(creatededge).toEqual(expectedResult);
  });

  it('should create edges on attribute connection to relationnodes ', () => {
    const node1 = getElementsUseCase.getNodeByID('5') as RelationNode;
    const node2 = getElementsUseCase.getNodeByID('7') as AttributeNode;
    setElementsUseCase.connectAttribute(node2, node1);

    expect(getElementsUseCase.getAllEdges().length).toEqual(1);
    const creatededge = getElementsUseCase.getAllEdges()[0] as Edge<any>;
    const expectedResult: Edge<any> = {
      id: (mockElements.nodes.length + 1).toString(),
      source: '7',
      sourceHandle: 'onAttributeHandle',
      target: '5',
      targetHandle: 'attributesHandle',
      animated: false,
    };
    expect(creatededge).toEqual(expectedResult);
  });

  it('should correctly remove an edge', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const funtionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;

    //Testcase for entity attribute edge
    let edge = undefined;
    setElementsUseCase.connectAttribute(attributeNode, entityNode);
    edge = getElementsUseCase.getConnectedEdges(attributeNode)[0];
    if (edge != undefined) {
      expect(getElementsUseCase.getAllEdges()).toContain(edge);
      setElementsUseCase.RemoveEdge(edge);
      expect(getElementsUseCase.getAllEdges()).not.toContain(edge);
    } else fail('setElementsUsecase.getConnectedEdgesis broken.');

    //Testcase for relationNode attribute edge
    edge = undefined;
    setElementsUseCase.connectAttribute(attributeNode, relationNode);
    edge = getElementsUseCase.getConnectedEdges(attributeNode)[0];
    if (edge != undefined) {
      expect(getElementsUseCase.getAllEdges()).toContain(edge);
      setElementsUseCase.RemoveEdge(edge);
      expect(getElementsUseCase.getAllEdges()).not.toContain(edge);
    } else fail('setElementsUsecase.getConnectedEdgesis broken.');

    //Testcase for entityNode relationNode edge
    edge = undefined;
    setElementsUseCase.connectEntityToRelation(entityNode, relationNode, false);
    edge = getElementsUseCase.getConnectedEdges(relationNode)[0];
    if (edge != undefined) {
      expect(getElementsUseCase.getAllEdges()).toContain(edge);
      setElementsUseCase.RemoveEdge(edge);
      expect(getElementsUseCase.getAllEdges()).not.toContain(edge);
    } else fail('setElementsUsecase.getConnectedEdgesis broken.');

    /** TODO add for functionode edges */
  });

  it('should correctly remove an entityNode node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUseCase.connectEntityToRelation(entityNode, relationNode, false);
    let edge1 = setElementsUseCase.connectAttribute(attributeNode, entityNode);

    // Remove the entityNode
    setElementsUseCase.RemoveNode(entityNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(entityNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);

    /**TODO add a connection to a function node
     * and also check if the correlating edge is removed
     * when the entitynode is removed.
     */
  });
  it('should correctly remove an entityNode node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUseCase.connectEntityToRelation(entityNode, relationNode, false);
    let edge1 = setElementsUseCase.connectAttribute(attributeNode, relationNode);

    // Remove the entityNode
    setElementsUseCase.RemoveNode(entityNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(entityNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);

    /**TODO add a connection to a function node
     * and also check if the correlating edge is removed
     * when the entitynode is removed.
     */
  });
  it('should correctly remove an relation node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const entityNode2 = getElementsUseCase.getNodeByID(entityElements[4].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUseCase.connectEntityToRelation(entityNode, relationNode, false);
    let edge1 = setElementsUseCase.connectAttribute(attributeNode, relationNode);
    let edge2 = setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);

    // Remove the relation
    setElementsUseCase.RemoveNode(relationNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(relationNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge2);
  });

  it('should correctly remove an attribute node', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    const attributeNode = getElementsUseCase.getNodeByID(attributeElements[0].id) as AttributeNode;
    const attributeNode2 = getElementsUseCase.getNodeByID(attributeElements[1].id) as AttributeNode;
    const functionNode = getElementsUseCase.getNodeByID(functionElements[0].id) as FunctionNode;
    let edge0 = setElementsUseCase.connectAttribute(attributeNode, entityNode);
    let edge1 = setElementsUseCase.connectAttribute(attributeNode, relationNode);

    // Remove the attributeNode
    setElementsUseCase.RemoveNode(attributeNode);
    expect(getElementsUseCase.getAllNodes()).not.toContain(attributeNode);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge0);
    setElementsUseCase.RemoveNode(attributeNode2);
    expect(getElementsUseCase.getAllNodes()).not.toContain(attributeNode2);
    expect(getElementsUseCase.getAllEdges()).not.toContain(edge1);
  });

  it('should not create a double edge', () => {
    const entityNode = getElementsUseCase.getNodeByID(entityElements[0].id) as EntityNode;
    const relationNode = getElementsUseCase.getNodeByID(relationElements[0].id) as RelationNode;
    setElementsUseCase.connectEntityToRelation(entityNode, relationNode, true);
    expect(getElementsUseCase.getAllEdges().length).toEqual(1);
    setElementsUseCase.connectEntityToRelation(entityNode, relationNode, true);
    expect(getElementsUseCase.getAllEdges().length).toEqual(1);
  });

  it('should correctly add entity nodes from the schema', () => {
    const nodePosition: NodePosition = { x: 0, y: 1 };
    const nodeName = 'nodeName';
    let entityNode = setElementsUseCase.createNodeFromSchema(
      QueryElementTypes.Entity,
      nodePosition,
      nodeName,
      false,
      '',
      '',
    );
    expect(entityNode.type).toEqual(QueryElementTypes.Entity);
    expect(entityNode.position).toEqual(nodePosition);
    expect((entityNode.data as EntityData).name).toEqual(nodeName);
    expect(getElementsUseCase.getAllNodes()).toContain(entityNode);
  });
  it('should correctly add relation nodes from the schema', () => {
    const nodePosition: NodePosition = { x: 0, y: 1 };
    const nodeName = 'nodeName';
    let relationNode = setElementsUseCase.createNodeFromSchema(
      QueryElementTypes.Relation,
      nodePosition,
      nodeName,
      false,
      '',
      '',
    );
    expect(relationNode.type).toEqual(QueryElementTypes.Relation);
    expect(relationNode.position).toEqual(nodePosition);
    expect((relationNode.data as RelationData).name).toEqual(nodeName);
    expect(getElementsUseCase.getAllNodes()).toContain(relationNode);
  });
  it('should correctly add attribute nodes from the schema', () => {
    const nodePosition: NodePosition = { x: 0, y: 1 };
    const nodeName = 'nodeName';
    let attributeNode = setElementsUseCase.createNodeFromSchema(
      QueryElementTypes.Attribute,
      nodePosition,
      nodeName,
      false,
      '',
      '',
    );
    expect(attributeNode.type).toEqual(QueryElementTypes.Attribute);
    expect(attributeNode.position).toEqual(nodePosition);
    expect((attributeNode.data as AttributeData).attribute).toEqual(nodeName);
    expect(getElementsUseCase.getAllNodes()).toContain(attributeNode);
  });
  it('should correctly add function nodes', () => {
    const nodePosition: NodePosition = { x: 0, y: 1 };
    let functionNode = setElementsUseCase.createFunction(
      FunctionTypes.communityDetection,
      nodePosition,
    );
    expect(functionNode.type).toEqual(QueryElementTypes.Function);
    expect(functionNode.position).toEqual(nodePosition);
    expect((functionNode.data as FunctionData).functionType).toEqual(
      FunctionTypes.communityDetection,
    );
    expect(getElementsUseCase.getAllNodes()).toContain(functionNode);
  });

  it('should throw an error on the wrong Type', () => {
    const nodePosition: NodePosition = { x: 0, y: 1 };
    const nodeName = 'nodeName';
    expect(() =>
      setElementsUseCase.createNodeFromSchema(
        QueryElementTypes.Function,
        nodePosition,
        nodeName,
        false,
        '',
        '',
      ),
    ).toThrowError('Cannot created function node from scheme');
  });
  /** TODO add for function node removal */
});
