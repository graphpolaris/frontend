/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import NodesHolder from '../../../entity/query-builder/models/NodesHolder';
import {
  AttributeNode,
  EntityNode,
  FunctionNode,
  RelationNode,
} from '../../../entity/query-builder/structures/Nodes';
import { TranslatedJSONQuery } from '../../../entity/query-builder/structures/TranslatedJSONQuery';
import TranslateQueryToJSONUseCase from '../TranslateQueryToJSONUseCase';
import { mockElements } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import SetElementsUseCase from '../SetElementsUseCase';
import GetElementsUseCase from '../GetElementsUseCase';
import { Handles, typeToFunctionHandle } from '../../../entity/query-builder/structures/Handles';
import QueryBuilderViewModelImpl from '../../../../presentation/view-model/query-builder/QueryBuilderViewModelImpl';
import {
  FunctionArgTypes,
  FunctionTypes,
} from '../../../entity/query-builder/structures/FunctionTypes';
import { DefaultPosition } from '../../../entity/query-builder/structures/NodePosition';
import { setElements } from 'react-flow-renderer/dist/store/actions';
import { mockElementsGroupBy } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderFlights';
/** Testsuite for the translate query to json use case
 *  TODO MAKE TEST WITH FUNCTION NODE ATTACHED.
 */
describe('visual query to JSON parser', () => {
  // Reset all modules between tests as state of module could interfere with test
  let nodesHolder: NodesHolder;
  let getElementsUseCase: GetElementsUseCase;
  let queryToJSONParser: TranslateQueryToJSONUseCase;
  let setElementsUseCase: SetElementsUseCase;
  beforeEach(() => {
    jest.resetModules();
    nodesHolder = new NodesHolder([]);
    getElementsUseCase = new GetElementsUseCase(nodesHolder);
    queryToJSONParser = new TranslateQueryToJSONUseCase(nodesHolder, getElementsUseCase);
    setElementsUseCase = new SetElementsUseCase(nodesHolder, getElementsUseCase);
  });

  it('should correctly parse a visual query without any any entities or relations', () => {
    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();

    // If there are no entity or relation elements, expect an empty query result
    const expected: TranslatedJSONQuery = {
      return: { entities: [], relations: [], groupBys: [] },
      entities: [],
      relations: [],
      groupBys: [],
      machineLearning: [],
      limit: 5000,
    };

    // Assert that the parsed result is equal to the expected result
    expect(parsedResult).toEqual(expected);
  });

  it('should correctly parse a visual query without any connections', () => {
    // Some entity and relation elements, without any connections between them
    const entityNodes: EntityNode[] = [
      mockElements.nodes[0] as EntityNode,
      mockElements.nodes[1] as EntityNode,
      mockElements.nodes[2] as EntityNode,
      mockElements.nodes[3] as EntityNode,
    ];
    const relationNodes: RelationNode[] = [
      mockElements.nodes[4] as RelationNode,
      mockElements.nodes[5] as RelationNode,
    ];
    entityNodes.forEach((node) => nodesHolder.addNode(node));
    relationNodes.forEach((node) => nodesHolder.addNode(node));
    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();

    /** The watchfull eye might see that the relationship nodes do not
     *  get added to the query eventhough the exits in the query builder.
     *  This is the current implementation of the communication
     *  As relationships only contribute when they have both connections done.
     *  Could be improved in the future by making the implementation as such you don't send queries when there
     *  are unconnected relations.*/
    const expected: TranslatedJSONQuery = {
      return: { entities: [1, 2, 3, 4], relations: [], groupBys: [] },
      entities: [
        { ID: 1, constraints: [], name: 'airports' },
        { ID: 2, constraints: [], name: 'airports' },
        { ID: 3, constraints: [], name: 'airports' },
        { ID: 4, constraints: [], name: 'airports' },
      ],
      relations: [],
      groupBys: [],
      machineLearning: [],
      limit: 5000,
    };

    expect(parsedResult).toEqual(expected);
  });

  it('should correctly parse a visual query with exactly one attributeNode connected to one entityNode', () => {
    // Some entity and relation elements, without any connections between them
    const attributeNodeNewYork: AttributeNode = mockElements.nodes[6] as AttributeNode;
    const entityNode: EntityNode = mockElements.nodes[3] as EntityNode;
    setElementsUseCase.addNode(entityNode);
    setElementsUseCase.addNode(attributeNodeNewYork);
    setElementsUseCase.connectAttribute(attributeNodeNewYork, entityNode);
    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected: TranslatedJSONQuery = {
      return: {
        entities: [4],
        relations: [],
        groupBys: [],
      },
      entities: [
        {
          name: 'airports',
          ID: 4,
          constraints: [
            {
              attribute: 'city',
              value: 'New York',
              dataType: 'string',
              matchType: 'exact',
              ID: 7,
            },
          ],
        },
      ],
      relations: [],
      groupBys: [],
      machineLearning: [],
      limit: 5000,
    };
    expect(parsedResult).toEqual(expected);
  });
  it('should correctly parse a communitydetection query', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    const communityDetectionNode: FunctionNode = mockElements.nodes[17] as FunctionNode;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    setElementsUseCase.addNode(communityDetectionNode);

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected: TranslatedJSONQuery = {
      return: {
        entities: [1, 2],
        relations: [5],
        groupBys: [],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [],
        },
      ],
      groupBys: [],
      machineLearning: [{ queuename: 'cdt_queue', parameters: [] }],
      limit: 5000,
    };
    expect(parsedResult).toEqual(expected);
  });
  it('should correctly parse a linkprediction query', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    const linkPredictionNode: FunctionNode = mockElements.nodes[18] as FunctionNode;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    setElementsUseCase.addNode(linkPredictionNode);

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected: TranslatedJSONQuery = {
      return: {
        entities: [1, 2],
        relations: [5],
        groupBys: [],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [],
        },
      ],
      groupBys: [],
      machineLearning: [{ queuename: 'lpr_queue', parameters: [] }],
      limit: 5000,
    };
    expect(parsedResult).toEqual(expected);
  });
  it('should correctly parse a centrality query', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    const centralityNode: FunctionNode = mockElements.nodes[19] as FunctionNode;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    setElementsUseCase.addNode(centralityNode);

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected: TranslatedJSONQuery = {
      return: {
        entities: [1, 2],
        relations: [5],
        groupBys: [],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [],
        },
      ],
      groupBys: [],
      machineLearning: [{ queuename: 'ctr_queue', parameters: [] }],
      limit: 5000,
    };
    expect(parsedResult).toEqual(expected);
  });
  it('should correctly parse a shortestPath query', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    const shortestPath: FunctionNode = mockElements.nodes[20] as FunctionNode;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    setElementsUseCase.addNode(shortestPath);

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected: TranslatedJSONQuery = {
      return: {
        entities: [1, 2],
        relations: [5],
        groupBys: [],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [],
        },
      ],
      groupBys: [],
      machineLearning: [
        {
          queuename: 'stp_queue',
          parameters: ['', ''],
        },
      ],
      limit: 5000,
    };
    expect(parsedResult).toEqual(expected);
  });

  it('should correctly parse a visual query with one relationship node and 2 connected entities', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected: TranslatedJSONQuery = {
      return: {
        entities: [1, 2],
        relations: [5],
        groupBys: [],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [],
        },
      ],
      groupBys: [],
      machineLearning: [],
      limit: 5000,
    };
    expect(parsedResult).toEqual(expected);

    // Adding an extra node that isn't connected to anything shouldn't change anything
    const entityNode3: EntityNode = mockElements.nodes[2] as EntityNode;
    setElementsUseCase.addNode(entityNode3);
    const parsedResult2: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected2: TranslatedJSONQuery = {
      return: {
        entities: [1, 2, 3],
        relations: [5],
        groupBys: [],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
        { name: 'airports', ID: 3, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [],
        },
      ],
      groupBys: [],
      machineLearning: [],
      limit: 5000,
    };
    expect(parsedResult2).toEqual(expected2);
  });
  it('should correctly parse a visual query with one relationship node and 2 connected entities and an attribute node', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    const attributeNode: AttributeNode = mockElements.nodes[6] as AttributeNode;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.addNode(attributeNode);
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    setElementsUseCase.connectAttribute(attributeNode, relationNode);
    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();
    const expected: TranslatedJSONQuery = {
      return: {
        entities: [1, 2],
        relations: [5],
        groupBys: [],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [
            {
              attribute: 'city',
              value: 'New York',
              dataType: 'string',
              matchType: 'exact',
              ID: 7,
            },
          ],
        },
      ],
      groupBys: [],
      machineLearning: [],
      limit: 5000,
    };
    expect(parsedResult).toEqual(expected);
  });

  it('should correctly parse a visual query with a simple groupBy', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    const functionNode: FunctionNode = mockElements.nodes[15] as FunctionNode;

    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.addNode(functionNode);

    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);

    //Connect function nodes to entity nodes
    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.group),
      entityNode1,
      Handles.ReceiveFunction,
    );
    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.by),
      entityNode2,
      Handles.ReceiveFunction,
    );
    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.relation),
      relationNode,
      Handles.ReceiveFunction,
    );

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();

    const expected: TranslatedJSONQuery = {
      return: {
        entities: [1, 2],
        relations: [5],
        groupBys: [16],
      },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          ID: 5,
          name: 'flights',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 1,
          toType: 'entity',
          toID: 2,
          constraints: [],
        },
      ],
      groupBys: [
        {
          ID: 16,
          groupType: 'entity',
          groupID: [1],
          groupAttribute: 'Day',
          byType: 'entity',
          byID: [2],
          byAttribute: 'city',
          appliedModifier: 'AVG',
          constraints: [],
          relationID: 5,
        },
      ],
      machineLearning: [],
      limit: 5000,
    };

    expect(parsedResult).toEqual(expected);
  });

  it('should parse a correct group by', () => {
    const entityNode1: EntityNode = mockElementsGroupBy.nodes[1] as EntityNode;
    const entityNode2: EntityNode = mockElementsGroupBy.nodes[2] as EntityNode;
    const relationNode: RelationNode = mockElementsGroupBy.nodes[0] as RelationNode;

    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);

    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);

    const functionNode: FunctionNode = mockElementsGroupBy.nodes[3] as FunctionNode;
    //Connect function nodes to entity and relation nodes
    setElementsUseCase.addNode(functionNode);
    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.group),
      entityNode1,
      Handles.ReceiveFunction,
    );

    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.by),
      entityNode2,
      Handles.ReceiveFunction,
    );

    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.relation),
      relationNode,
      Handles.ReceiveFunction,
    );

    const constraintNode: AttributeNode = mockElementsGroupBy.nodes[4] as AttributeNode;
    // Add constraint node and connect it to the groupby function.
    setElementsUseCase.addNode(constraintNode);
    setElementsUseCase.createEdge(
      constraintNode,
      Handles.OnAttribute,
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.constraints),
    );
    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.by),
      entityNode2,
      Handles.ReceiveFunction,
    );
    setElementsUseCase.createEdge(
      functionNode,
      typeToFunctionHandle(FunctionArgTypes.relation),
      relationNode,
      Handles.ReceiveFunction,
    );

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();

    const parsedResult: TranslatedJSONQuery = queryToJSONParser.TranslateQueryToObject();

    const expected: TranslatedJSONQuery = {
      return: { entities: [27, 28], relations: [26], groupBys: [29] },
      entities: [
        { name: 'airports', ID: 1, constraints: [] },
        { name: 'airports', ID: 2, constraints: [] },
      ],
      relations: [
        {
          name: 'parliament',
          ID: 27,
          constraints: [],
        },
        { name: 'parties', ID: 28, constraints: [] },
      ],
      relations: [
        {
          ID: 26,
          name: 'member_of',
          depth: { min: 1, max: 1 },
          fromType: 'entity',
          fromID: 27,
          toType: 'entity',
          toID: 28,
          constraints: [],
        },
      ],
      groupBys: [
        {
          ID: 29,
          appliedModifier: 'AVG',
          byAttribute: '_id',
          byID: [28],
          byType: 'entity',
          constraints: [
            {
              attribute: 'age',
              value: '45',
              dataType: 'int',
              matchType: 'GT',
            },
          ],
          groupAttribute: 'age',
          groupID: [27],
          groupType: 'entity',
          relationID: 26,
        },
      ],
      machineLearning: [],
      limit: 5000,
    };

    expect(parsedResult).toEqual(expected);
  });

  /*These testcases SHOULD BE AT THE BOTTOM
  *BECAUSE IT changes the nodes.
  Nobody knows why */
  it('should throw if attribute data is undefined', () => {
    const entityNode1: EntityNode = mockElements.nodes[1] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[2] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    const attributeNode: AttributeNode = mockElements.nodes[7] as AttributeNode;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);
    setElementsUseCase.addNode(attributeNode);
    attributeNode.data = undefined;
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    setElementsUseCase.connectAttribute(attributeNode, relationNode);
    expect(() => queryToJSONParser.TranslateQueryToObject()).toThrowError(
      'Query-builder: No data in attribute element!',
    );
  });
  it('should throw if entity data is undefined', () => {
    const entityNode1: EntityNode = mockElements.nodes[0] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[1] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;
    entityNode1.data = undefined;
    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);

    expect(() => queryToJSONParser.TranslateQueryToObject()).toThrowError(
      'Query-builder: No data in entity element!',
    );

    //Shouldn't matter for connected relations.
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    expect(() => queryToJSONParser.TranslateQueryToObject()).toThrowError(
      'Query-builder: No data in entity element!',
    );
  });
  it('should throw if relation data is undefined', () => {
    const entityNode1: EntityNode = mockElements.nodes[2] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[3] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;

    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);

    relationNode.data = undefined;
    expect(() => queryToJSONParser.TranslateQueryToObject()).toThrowError(
      'Query-builder: No data in relation element!',
    );

    //Shouldn't matter for connected relations.
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    expect(() => queryToJSONParser.TranslateQueryToObject()).toThrowError(
      'Query-builder: No data in relation element!',
    );
  });
  it('should throw if relation data is undefined', () => {
    const entityNode1: EntityNode = mockElements.nodes[2] as EntityNode;
    const entityNode2: EntityNode = mockElements.nodes[3] as EntityNode;
    const relationNode: RelationNode = mockElements.nodes[4] as RelationNode;

    setElementsUseCase.addNode(entityNode1);
    setElementsUseCase.addNode(entityNode2);
    setElementsUseCase.addNode(relationNode);

    relationNode.data = undefined;
    expect(() => queryToJSONParser.TranslateQueryToObject()).toThrowError(
      'Query-builder: No data in relation element!',
    );

    //Shouldn't matter for connected relations.
    setElementsUseCase.connectEntityToRelation(entityNode1, relationNode, false);
    setElementsUseCase.connectEntityToRelation(entityNode2, relationNode, true);
    expect(() => queryToJSONParser.TranslateQueryToObject()).toThrowError(
      'Query-builder: No data in relation element!',
    );
  });
});
