/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { mockElements } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import mockSchema from '../../../../data/mock-data/schema-result/AirportsSchemaResult';
import { mockElementsGroupBy } from '../../../../data/mock-data/query-builder/mockDataQueryBuilderFlights';
import NodesHolder from '../../../entity/query-builder/models/NodesHolder';
import { FunctionArgTypes } from '../../../entity/query-builder/structures/FunctionTypes';
import { Handles } from '../../../entity/query-builder/structures/Handles';
import {
  AttributeNode,
  EntityNode,
  FunctionNode,
  RelationNode,
} from '../../../entity/query-builder/structures/Nodes';
import GetElementsUseCase from '../GetElementsUseCase';
import SetElementsUseCase from '../SetElementsUseCase';
import TranslateQueryToJSONUseCase from '../TranslateQueryToJSONUseCase';
import ValidateConnectionUseCase from '../ValidateConnectionUseCase';

describe('visual query to JSON parser', () => {
  // Reset all modules between tests as state of module could interfere with test
  let nodesHolder: NodesHolder;
  let getElementsUseCase: GetElementsUseCase;
  let queryToJSONParser: TranslateQueryToJSONUseCase;
  let setElementsUseCase: SetElementsUseCase;
  let validateConnectionUseCase: ValidateConnectionUseCase;

  jest.resetModules();
  const entityNode = mockElements.nodes[0] as EntityNode;
  const relationNode = mockElements.nodes[4] as RelationNode;
  const attributeNode = mockElements.nodes[6] as AttributeNode; // Is an attribute node of the entity node
  const groupByNode = mockElementsGroupBy.nodes[3] as FunctionNode;
  const communityDetectionNode = mockElements.nodes[17] as FunctionNode;
  const faultyNode = mockElements.nodes[14];

  beforeEach(() => {
    nodesHolder = new NodesHolder([]);
    getElementsUseCase = new GetElementsUseCase(nodesHolder);
    queryToJSONParser = new TranslateQueryToJSONUseCase(nodesHolder, getElementsUseCase);
    setElementsUseCase = new SetElementsUseCase(nodesHolder, getElementsUseCase);
    validateConnectionUseCase = new ValidateConnectionUseCase(nodesHolder, getElementsUseCase);

    nodesHolder.addNode(entityNode);
    nodesHolder.addNode(relationNode);
    nodesHolder.addNode(attributeNode);
    nodesHolder.addNode(groupByNode);
    nodesHolder.addNode(communityDetectionNode);
  });

  it('Should validate connections correctly', () => {
    expect(validateConnectionUseCase.validateConnection(entityNode, '', attributeNode, '')).toEqual(
      false,
    );
    expect(validateConnectionUseCase.validateConnection(entityNode, '', entityNode, '')).toEqual(
      false,
    );
    expect(validateConnectionUseCase.validateConnection(entityNode, '', entityNode, '')).toEqual(
      false,
    );
    expect(
      validateConnectionUseCase.validateConnection(entityNode, '', communityDetectionNode, ''),
    ).toEqual(false);

    expect(validateConnectionUseCase.validateConnection(relationNode, '', entityNode, '')).toEqual(
      false,
    );
    expect(
      validateConnectionUseCase.validateConnection(relationNode, '', relationNode, ''),
    ).toEqual(false);
    expect(
      validateConnectionUseCase.validateConnection(relationNode, '', attributeNode, ''),
    ).toEqual(false);
    expect(validateConnectionUseCase.validateConnection(relationNode, '', groupByNode, '')).toEqual(
      false,
    );

    expect(
      validateConnectionUseCase.validateConnection(attributeNode, '', attributeNode, ''),
    ).toEqual(false);
    expect(
      validateConnectionUseCase.validateConnection(
        attributeNode,
        Handles.OnAttribute,
        groupByNode,
        Handles.ToAttributeHandle,
      ),
    ).toEqual(true);
    expect(
      validateConnectionUseCase.validateConnection(attributeNode, '', communityDetectionNode, ''),
    ).toEqual(false);

    expect(
      validateConnectionUseCase.validateConnection(
        groupByNode,
        Handles.FunctionBase + FunctionArgTypes.by,
        entityNode,
        Handles.ReceiveFunction,
      ),
    ).toEqual(true);
    expect(
      validateConnectionUseCase.validateConnection(
        groupByNode,
        Handles.FunctionBase + FunctionArgTypes.relation,
        relationNode,
        Handles.ReceiveFunction,
      ),
    ).toEqual(true);
    expect(
      validateConnectionUseCase.validateConnection(
        groupByNode,
        Handles.FunctionBase + FunctionArgTypes.constraints,
        attributeNode,
        Handles.OnAttribute,
      ),
    ).toEqual(false);
    expect(validateConnectionUseCase.validateConnection(groupByNode, '', groupByNode, '')).toEqual(
      false,
    );

    validateConnectionUseCase.consumeMessageFromBackend(mockSchema);

    expect(
      validateConnectionUseCase.validateConnection(
        attributeNode,
        Handles.OnAttribute,
        entityNode,
        Handles.ToAttributeHandle,
      ),
    ).toEqual(true);
    expect(
      validateConnectionUseCase.validateConnection(attributeNode, '', relationNode, ''),
    ).toEqual(false);
  });

  it('should throw the correct errors', () => {
    expect(() =>
      validateConnectionUseCase.validateConnection(entityNode, '', faultyNode, ''),
    ).toThrowError('NodeType not supported in validate ' + entityNode.type + faultyNode.type);
    expect(() =>
      validateConnectionUseCase.validateConnection(relationNode, '', faultyNode, ''),
    ).toThrowError('NodeType not supported in validate ' + relationNode.type + faultyNode.type);
    expect(() =>
      validateConnectionUseCase.validateConnection(attributeNode, '', faultyNode, ''),
    ).toThrowError('NodeType not supported in validate ' + attributeNode.type + faultyNode.type);
    expect(() =>
      validateConnectionUseCase.validateConnection(communityDetectionNode, '', faultyNode, ''),
    ).toThrowError(
      'NodeType not supported in validate ' + communityDetectionNode.type + faultyNode.type,
    );
    expect(() =>
      validateConnectionUseCase.validateConnection(groupByNode, '', faultyNode, ''),
    ).toThrowError('NodeType not supported in validate ' + groupByNode.type + faultyNode.type);
    expect(() =>
      validateConnectionUseCase.validateConnection(faultyNode, '', faultyNode, ''),
    ).toThrowError('NodeType not supported in validate ' + faultyNode.type + faultyNode.type);

    expect(() =>
      validateConnectionUseCase.validateConnection(faultyNode, '', faultyNode, ''),
    ).toThrowError('NodeType not supported in validate ' + faultyNode.type + faultyNode.type);

    expect(() =>
      validateConnectionUseCase.validateConnection(
        communityDetectionNode,
        'functionHandle_',
        relationNode,
        '',
      ),
    ).toThrowError('Validating for unsupported function');
  });
});
