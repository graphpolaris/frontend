/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BackendMessengerRepository from '../../repository-interfaces/BackendMessengerRepository';

/** Use case for requesting the schema of the currently selected database. */
export default class RequestSchemaUseCase {
  private backendMessenger: BackendMessengerRepository;

  constructor(backendMessenger: BackendMessengerRepository) {
    this.backendMessenger = backendMessenger;
  }

  /**
   * Request the schema of the selected database from the schema.
   * @param databasename The name of the database.
   */
  public requestSchema(databasename: string, cached = true): void {
    // Request the schema.
    const body = {
      cached: cached,
      databaseName: databasename,
    };

    this.backendMessenger
      .SendMessage(JSON.stringify(body), 'schema/', 'POST')
      .then(() => {
        console.log('schema requested for database: ' + databasename);
      })
      .catch((reason) => {
        console.warn(reason);
      });
  }
}
