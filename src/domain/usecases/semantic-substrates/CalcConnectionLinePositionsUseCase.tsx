/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Position } from '../../entity/utils/Position';
import { CalcDistance } from '../utils/CalcDistance';
import { RotateVectorByDeg } from '../utils/RotateVec';

/** The return type for this usecase. controlPoint is used for the curvature of the line. */
type ConnecionLinePositions = {
  start: Position;
  end: Position;
  controlPoint: Position;

  arrowRStart: Position;
  arrowLStart: Position;
};

/** The use case that calculates the line positions */
export default class CalcConnectionLinePositionsUseCase {
  /**
   * Calculates the positions for the points needed to draw the curved line with the arrow.
   * Also offsets the start and end point so they touch the edge of the node, instead of going to the center.
   * @param {Position} startNodePos The position of the start node.
   * @param {Position} endNodePos The position of the end node.
   * @param {number} nodeRadius The node radius, used to calculate the start and end offset.
   * @returns {ConnecionLinePositions} The positions for drawing the curved line.
   */
  public static calculatePositions(
    startNodePos: Position,
    endNodePos: Position,
    nodeRadius: number,
  ): ConnecionLinePositions {
    // Calculate the control point for the quadratic curve path, see https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
    const distance = CalcDistance(startNodePos, endNodePos);
    if (distance == 0) return this.returnStartPosition(startNodePos);
    const controlPoint = this.calculateControlPoint(endNodePos, startNodePos, distance);

    // move the start and end point so they are on the edge of their node
    const movedStartPos = this.calculateMovedStartPos(startNodePos, controlPoint, nodeRadius);
    const endToControlPointDist = CalcDistance(endNodePos, controlPoint);
    const endToControlPointVec = {
      x: (endNodePos.x - controlPoint.x) / endToControlPointDist,
      y: (endNodePos.y - controlPoint.y) / endToControlPointDist,
    };

    const movedEndPos = this.calculateMovedEndPos(endNodePos, endToControlPointVec, nodeRadius);

    // Create arrowhead start points
    const arrowRStart = this.calculateArrowStart(
      movedEndPos,
      RotateVectorByDeg(endToControlPointVec, 30),
    );
    const arrowLStart = this.calculateArrowStart(
      movedEndPos,
      RotateVectorByDeg(endToControlPointVec, -30),
    );

    return {
      start: movedStartPos,
      end: movedEndPos,
      controlPoint,
      arrowRStart,
      arrowLStart,
    };
  }

  /**
   * Creates the connection line positions for a distance of zero.
   * In this special case all values are set to the start node position.
   * @param {Position} startNodePos The position of the start node.
   * @returns {ConnectionLinePosition} The positions for drawing the curved line.
   */
  private static returnStartPosition(startNodePos: Position): ConnecionLinePositions {
    return {
      start: startNodePos,
      end: startNodePos,
      controlPoint: startNodePos,
      arrowRStart: startNodePos,
      arrowLStart: startNodePos,
    };
  }

  /**
   * Calculate the control point for the quadratic curve path.
   * @param {Position} startNodePos The position of the start node.
   * @param {Position} endNodePos The position of the end node.
   * @param {number} distance The distance between the two nodes.
   * @returns {Position} The control point.
   */
  private static calculateControlPoint(
    endNodePos: Position,
    startNodePos: Position,
    distance: number,
  ): Position {
    // Normalized vector from start to end
    const vec: Position = {
      x: (endNodePos.x - startNodePos.x) / distance,
      y: (endNodePos.y - startNodePos.y) / distance,
    };

    // The point between the start and end, moved 15% of the distance closer to the start
    const pointBetween = {
      x: (startNodePos.x + endNodePos.x) / 2 - vec.x * distance * 0.15,
      y: (startNodePos.y + endNodePos.y) / 2 - vec.y * distance * 0.15,
    };

    // The control point for th quadratic curve
    // Move this point 25% of the distance away from the line between the start and end, at a 90 deg. angle
    return {
      x: pointBetween.x + -vec.y * distance * 0.25,
      y: pointBetween.y + vec.x * distance * 0.25,
    };
  }

  /**
   * Calculates the moved start position.
   * @param {Position} startNodePos The position of the start node.
   * @param {Position} controlPoint The control point for the quadratic curve path.
   * @param {number} nodeRadius The node radius, used to calculate the start and end offset.
   * @returns {Position} The moved start position.
   */
  private static calculateMovedStartPos(
    startNodePos: Position,
    controlPoint: Position,
    nodeRadius: number,
  ): Position {
    const startToControlPointDist = CalcDistance(startNodePos, controlPoint);
    return {
      x:
        startNodePos.x + ((controlPoint.x - startNodePos.x) / startToControlPointDist) * nodeRadius,
      y:
        startNodePos.y + ((controlPoint.y - startNodePos.y) / startToControlPointDist) * nodeRadius,
    };
  }

  /**
   * Calculates the moved end position
   * @param {Position} endNodePos The position of the end node.
   * @param {Position} endToControlPointVec The control point vector.
   * @param {number} nodeRadius The node radius, used to calculate the start and end offset.
   * @returns {Position} The moved end position.
   */
  private static calculateMovedEndPos(
    endNodePos: Position,
    endToControlPointVec: Position,
    nodeRadius: number,
  ): Position {
    return {
      x: endNodePos.x - endToControlPointVec.x * nodeRadius,
      y: endNodePos.y - endToControlPointVec.y * nodeRadius,
    };
  }

  /**
   * Calculates the start position of the arrow.
   * @param {Position} movedEndPos The position of the moved end node.
   * @param {Position} rotatedVec The rotated arrow vector.
   * @returns {Position} The arrow's start position.
   */
  private static calculateArrowStart(movedEndPos: Position, rotatedVec: Position): Position {
    const arrowLength = 7;
    return {
      x: movedEndPos.x - rotatedVec.x * arrowLength,
      y: movedEndPos.y - rotatedVec.y * arrowLength,
    };
  }
}
