/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { NodeLinkResultType } from '../../entity/query-result/structures/NodeLinkResultType';
import { AxisLabel, PlotSpecifications } from '../../entity/semantic-substrates/structures/Types';

/** UseCase for calculating default plots from node link query result data. */
export default class CalcDefaultPlotSpecsUseCase {
  /**
   * Calculates default plot specifications for incoming query result data.
   * This determines what the default plots will be after executing a new query.
   * @param {NodeLinkResultType} nodeLinkResult Query result data in the node link format.
   * @returns {PlotSpecifications[]} PlotSpecifications to generate the plots with the result data.
   */
  public static calculate(nodeLinkResult: NodeLinkResultType): PlotSpecifications[] {
    // Search through the first nodes' attributes for the shortest attribute value
    const plotSpecifications: PlotSpecifications[] = [];
    if (nodeLinkResult.nodes.length > 0) {
      const firstNodeAttributes = nodeLinkResult.nodes[0].attributes;
      const firstNodeEntity = nodeLinkResult.nodes[0].id.split('/')[0];

      let shortestStringKey = '';
      let shortestStringValueLength: number = Number.MAX_VALUE;
      for (let key in firstNodeAttributes) {
        if (typeof firstNodeAttributes[key] == 'string') {
          const v = firstNodeAttributes[key];
          if (v.length < shortestStringValueLength) {
            shortestStringKey = key;
            shortestStringValueLength = v.length;
          }
        }
      }

      // The key with the shortest attribute value, will be used to filter nodes for max 3 plots
      const values: string[] = [];
      for (let i = 0; i < nodeLinkResult.nodes.length; i++) {
        // Search for the first three nodes with different attribute values with the given attributekey

        if (nodeLinkResult.nodes[i].id.split('/')[0] != firstNodeEntity) continue;

        const v = nodeLinkResult.nodes[i].attributes[shortestStringKey];
        if (values.includes(v)) continue;

        values.push(v);
        plotSpecifications.push({
          entity: nodeLinkResult.nodes[i].id.split('/')[0],
          labelAttributeType: shortestStringKey,
          labelAttributeValue: nodeLinkResult.nodes[i].attributes[shortestStringKey],
          xAxis: AxisLabel.evenlySpaced, // Use default evenly spaced and # outbound connections on the x and y axis.
          yAxis: AxisLabel.outboundConnections,
          xAxisAttributeType: '',
          yAxisAttributeType: '',
          width: 800,
          height: 200,
        });

        if (values.length >= 3) break;
      }
    }

    return plotSpecifications;
  }
}
