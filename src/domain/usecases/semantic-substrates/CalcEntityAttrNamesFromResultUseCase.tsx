/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
 import {
   AttributeNames,
   EntitiesFromSchema,
 } from '../../entity/semantic-substrates/structures/Types';
 import { NodeLinkResultType } from '../../entity/query-result/structures/NodeLinkResultType';
 
 /** Use case for retrieving entity names and attribute names from a schema result. */
 export default class CalcEntityAttrNamesFromResultUseCase {
   /**
    * Takes a schema result and calculates all the entity names and attribute names per datatype.
    * Used by semantic substrates for the plot titles and add plot selections.
    * @param {NodeLinkResultType} schemaResult A new schema result from the backend.
    * @param {EntitiesFromSchema} All entity names and attribute names per datatype. So we know what is in the Schema.
    * @returns {EntitiesFromSchema} All entity names and attribute names per datatype.
    */
   public static CalcEntityAttrNamesFromResult(nodeLinkResult: NodeLinkResultType, entitiesFromSchema: EntitiesFromSchema): EntitiesFromSchema {
     const listOfNodeTypes : string[] = []

     nodeLinkResult.nodes.forEach((node) => {
        let entityName = node.id.split('/')[0]
        if (!listOfNodeTypes.includes(entityName)){
        listOfNodeTypes.push(entityName);}
     })

     let entitiesFromSchemaPruned : EntitiesFromSchema = { entityNames: [], attributesPerEntity: {} };;
     Object.assign(entitiesFromSchemaPruned, entitiesFromSchema);

     entitiesFromSchemaPruned.entityNames = listOfNodeTypes;
     return entitiesFromSchemaPruned;
   }
 }
 