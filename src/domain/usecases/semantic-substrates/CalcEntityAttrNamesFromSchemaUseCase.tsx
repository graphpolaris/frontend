/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Schema } from '../../entity/graph-schema/structures/InputDataTypes';
import {
  AttributeNames,
  EntitiesFromSchema,
} from '../../entity/semantic-substrates/structures/Types';

/** Use case for retrieving entity names and attribute names from a schema result. */
export default class CalcEntityAttrNamesFromSchemaUseCase {
  /**
   * Takes a schema result and calculates all the entity names and attribute names per datatype.
   * Used by semantic substrates for the plot titles and add plot selections.
   * @param {SchemaResultType} schemaResult A new schema result from the backend.
   * @returns {EntitiesFromSchema} All entity names and attribute names per datatype.
   */
  public static calculate(schemaResult: Schema): EntitiesFromSchema {
    const attributesPerEntity: Record<string, AttributeNames> = {};
    // Go through each entity.
    schemaResult.nodes.forEach((node) => {
      // Extract the attribute names per datatype for each entity.
      const textAttributeNames: string[] = [];
      const boolAttributeNames: string[] = [];
      const numberAttributeNames: string[] = [];
      node.attributes.forEach((attr) => {
        if (attr.type == 'string') textAttributeNames.push(attr.name);
        else if (attr.type == 'int') numberAttributeNames.push(attr.name);
        else if (attr.type == 'bool') boolAttributeNames.push(attr.name);
      });

      // Create a new object with the arrays with attribute names per datatype.
      attributesPerEntity[node.name] = {
        textAttributeNames,
        boolAttributeNames,
        numberAttributeNames,
      };
    });

    // Create the object with entity names and attribute names.
    return {
      entityNames: schemaResult.nodes.map((node) => node.name),
      attributesPerEntity,
    };
  }
}
