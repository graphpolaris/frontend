/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import * as d3 from 'd3-v6';
import { MinMaxType, PlotInputData } from '../../entity/semantic-substrates/structures/Types';
import { Position } from '../../entity/utils/Position';

/**
 * Calculates the scaled positions using d3's scaleLinear functions.
 * Also calculates the min and max x y values.
 */
export default class CalcScaledPosUseCase {
  /**
   * Linearly scales positions to the width and height of the plot
   * @param {PlotInputData} plot The plot for which to scale the nodes positions.
   * @param {MinMaxType} minmaxXAxis The min and max for the x axis.
   * @param {MinMaxType} minmaxYAxis The min and max for the y axis.
   * @returns {Position[]} The scaled positions.
   */
  public static calculate(
    plot: PlotInputData,
    minmaxXAxis: MinMaxType,
    minmaxYAxis: MinMaxType,
  ): Position[] {
    // Create the scale functions with the minmax and width and height of the plot
    const scaleFunctions = CalcScaledPosUseCase.createScaleFunctions(
      minmaxXAxis,
      minmaxYAxis,
      plot.width,
      plot.height,
    );

    // Use the scale functions to scale the nodes positions.
    const scaledPositions: Position[] = [];
    plot.nodes.forEach((node) => {
      scaledPositions.push({
        x: scaleFunctions.xAxis(node.originalPosition.x),
        y: scaleFunctions.yAxis(node.originalPosition.y),
      });
    });

    return scaledPositions;
  }

  /** Uses D3 to create linear scale functions. */
  private static createScaleFunctions = (
    minmaxXAxis: MinMaxType,
    minmaxYAxis: MinMaxType,
    plotWidth: number,
    plotHeight: number,
  ): {
    xAxis: d3.ScaleLinear<number, number, never>;
    yAxis: d3.ScaleLinear<number, number, never>;
  } => {
    // Create the x axis scale funtion with d3
    const xAxisScale = d3
      .scaleLinear()
      .domain([minmaxXAxis.min, minmaxXAxis.max])
      .range([0, plotWidth]);

    // Create the y axis scale funtion with d3
    const yAxisScale = d3
      .scaleLinear()
      .domain([minmaxYAxis.max, minmaxYAxis.min])
      .range([0, plotHeight]);

    return {
      xAxis: xAxisScale,
      yAxis: yAxisScale,
    };
  };
}
