/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { MinMaxType, PlotInputData } from '../../entity/semantic-substrates/structures/Types';

/** UseCase for calculating the min and max for the x and y values of the node positions. */
export default class CalcXYMinMaxUseCase {
  /**
   * Calculates the min and max for the x and y values of the node positions.
   * @param {PlotInputData} plot The input data plot with all the nodes.
   * @returns {{x: MinMaxType; y: MinMaxType}} An object with the min and max for x and y.
   */
  public static calculate(plot: PlotInputData): { x: MinMaxType; y: MinMaxType } {
    // If there are no nodes in the plot, set the min and max to [-10, 10]
    if (plot.nodes.length == 0) {
      return {
        x: {
          min: -10,
          max: 10,
        },
        y: {
          min: -10,
          max: 10,
        },
      };
    }
    // Calculate the min and max values for the x and y positions
    // Start the x and y values of the first node as min and max
    const minmaxX: MinMaxType = {
      min: plot.nodes[0].originalPosition.x,
      max: plot.nodes[0].originalPosition.x,
    };
    const minmaxY: MinMaxType = {
      min: plot.nodes[0].originalPosition.y,
      max: plot.nodes[0].originalPosition.y,
    };
    for (let i = 1; i < plot.nodes.length; i++) {
      const position = plot.nodes[i].originalPosition;

      if (position.x > minmaxX.max) minmaxX.max = position.x;
      else if (position.x < minmaxX.min) minmaxX.min = position.x;

      if (position.y > minmaxY.max) minmaxY.max = position.y;
      else if (position.y < minmaxY.min) minmaxY.min = position.y;
    }

    // Add 20%, so there are no nodes whose position is exactly on the axis
    let xDiff = (minmaxX.max - minmaxX.min) * 0.2;

    minmaxX.min -= xDiff;
    minmaxX.max += xDiff;
    let yDiff = (minmaxY.max - minmaxY.min) * 0.2;

    minmaxY.min -= yDiff;
    minmaxY.max += yDiff;

    // If the min and max are the same, add and subtract 10
    if (minmaxX.min == minmaxX.max) {
      minmaxX.min -= 1;
      minmaxX.max += 1;
    }
    if (minmaxY.min == minmaxY.max) {
      minmaxY.min -= 1;
      minmaxY.max += 1;
    }
    return { x: minmaxX, y: minmaxY };
  }
}
