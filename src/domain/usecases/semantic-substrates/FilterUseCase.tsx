/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  MinMaxType,
  NodeType,
  PlotType,
  RelationType,
} from '../../entity/semantic-substrates/structures/Types';

type Filter = { x: MinMaxType; y: MinMaxType };

/** A use case for applying filters on nodes and filter relations. */
export default class FilterUseCase {
  /**
   * Checks if a node matches the given filters.
   * @param {NodeType} node The node to be matched.
   * @param {Filter} filter The filters to match the node to.
   * @returns True when the node matches the constraints of the filter.
   */
  public static checkIfNodeMatchesFilter(node: NodeType, filter: Filter): boolean {
    return (
      node.originalPosition.x >= filter.x.min &&
      node.originalPosition.x <= filter.x.max &&
      node.originalPosition.y >= filter.y.min &&
      node.originalPosition.y <= filter.y.max
    );
  }

  /**
   * Applies the filter that changed to the filteredRelations list.
   * @param {RelationType[][][]} relations The original unmodified relations. All relations.
   * @param {RelationType[][][]} filteredRelations The current filtered relations. This will be updated with the new filter.
   * Note that initially relations should be a clone of filteredRelations.
   * @param {PlotType[]} plots The plots.
   * @param {Filter[]} filtersPerPlot The filters for each plot, with a filter at the filterIndexThatChanged that is different from the previous call to this function.
   * @param {number} filterIndexThatChanged index for the filter that changed.
   */
  public static filterRelations(
    relations: RelationType[][][],
    filteredRelations: RelationType[][][],
    plots: PlotType[],
    filtersPerPlot: Filter[],
    filterIndexThatChanged: number,
  ): void {
    this.checkOutboundConnections(
      relations,
      filteredRelations,
      plots,
      filtersPerPlot,
      filterIndexThatChanged,
    );
    for (let fromPlot = 0; fromPlot < relations.length; fromPlot++) {
      this.checkInboundConnection(
        relations,
        filteredRelations,
        plots,
        filtersPerPlot,
        filterIndexThatChanged,
        fromPlot,
      );
    }
  }

  /**
   * Check for the inbound connections if the filter has changed
   * @param {RelationType[][][]} relations The original unmodified relations. All relations.
   * @param {RelationType[][][]} filteredRelations The current filtered relations. This will be updated with the new filter.
   * Note that initially relations should be a clone of filteredRelations.
   * @param {PlotType[]} plots The plots.
   * @param {Filter[]} filtersPerPlot The filters for each plot, with a filter at the filterIndexThatChanged that is different from the previous call to this function.
   * @param {number} filterIndexThatChanged index for the filter that changed.
   */
  private static checkOutboundConnections(
    relations: RelationType[][][],
    filteredRelations: RelationType[][][],
    plots: PlotType[],
    filtersPerPlot: Filter[],
    filterIndexThatChanged: number,
  ) {
    // Check all the outbound connections for nodes in the plot for which the filter changed.
    relations[filterIndexThatChanged].forEach((relations, toPlot) => {
      filteredRelations[filterIndexThatChanged][toPlot] = relations.filter((relation) => {
        const fromNode = plots[filterIndexThatChanged].nodes[relation.fromIndex];
        const fromNodeFilter = filtersPerPlot[filterIndexThatChanged];
        const fromNodeMatches = this.checkIfNodeMatchesFilter(fromNode, fromNodeFilter);

        const toNode = plots[toPlot].nodes[relation.toIndex];
        const toNodeFilter = filtersPerPlot[toPlot];
        const toNodeMatches = this.checkIfNodeMatchesFilter(toNode, toNodeFilter);

        // Check if the from- and to-node match their plot filters.
        return fromNodeMatches && toNodeMatches;
      });
    });
  }

  /**
   * Check for the inbound connections if the filter has changed
   * @param {RelationType[][][]} relations The original unmodified relations. All relations.
   * @param {RelationType[][][]} filteredRelations The current filtered relations. This will be updated with the new filter.
   * Note that initially relations should be a clone of filteredRelations.
   * @param {PlotType[]} plots The plots.
   * @param {Filter[]} filtersPerPlot The filters for each plot, with a filter at the filterIndexThatChanged that is different from the previous call to this function.
   * @param {number} filterIndexThatChanged index for the filter that changed.
   * @param {number} fromPlot The index of the current from plot.
   */
  private static checkInboundConnection(
    relations: RelationType[][][],
    filteredRelations: RelationType[][][],
    plots: PlotType[],
    filtersPerPlot: Filter[],
    filterIndexThatChanged: number,
    fromPlot: number,
  ) {
    // Check all the inbound connections for nodes in the plot for which the filter changed.
    const relationsBetweenPlots = relations[fromPlot][filterIndexThatChanged];

    filteredRelations[fromPlot][filterIndexThatChanged] = relationsBetweenPlots.filter(
      (relation) => {
        const toNode = plots[filterIndexThatChanged].nodes[relation.toIndex];
        const toNodeFilter = filtersPerPlot[filterIndexThatChanged];
        const toNodeMatches = this.checkIfNodeMatchesFilter(toNode, toNodeFilter);

        const fromNode = plots[fromPlot].nodes[relation.fromIndex];
        const fromNodeFilter = filtersPerPlot[fromPlot];
        const fromNodeMatches = this.checkIfNodeMatchesFilter(fromNode, fromNodeFilter);

        // Here we also check for connections within the same plot.
        // For these connections we only need to check if the from- or the to-node matches the filter.
        return (
          (fromNodeMatches && toNodeMatches) ||
          (fromPlot == filterIndexThatChanged && (fromNodeMatches || toNodeMatches))
        );
      },
    );
  }
}
