/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { NodeLinkResultType, Node } from '../../entity/query-result/structures/NodeLinkResultType';
import {
  AxisLabel,
  PlotInputData,
  PlotSpecifications,
  RelationType,
} from '../../entity/semantic-substrates/structures/Types';
import ParseToUniqueEdges from '../../usecases/ParseToUniqueEdgesUseCase';

/** A use case for parsing incoming node-link data to plot data */
export default class ToPlotDataParserUseCase {
  /**
   * Parses incoming node link data from the backend to plotdata.
   * @param {NodeLinkResultType} queryResult The query result coming from the backend.
   * @param {NodeAttributesForPlot[]} plotSpecifications The attribute types to use for label and x y values.
   * @returns Plotdata with relations.
   */
  public static parseQueryResult(
    queryResult: NodeLinkResultType,
    plotSpecifications: PlotSpecifications[],
    relationSelectedAttributeNumerical: string,
    relationSelectedAttributeCatecorigal: string,
    relationScaleCalculation: (x: number) => number,
    relationColourCalculation: (x: string) => string,
  ): { plots: PlotInputData[]; relations: RelationType[][][] } {
    const { plots, nodePlotIndexDict } = this.filterNodesWithPlotSpecs(
      queryResult.nodes,
      plotSpecifications,
    );

    // Initialize the relations with empty arrays, an |plots| x |plots| matrix with empty arrays
    const relations: RelationType[][][] = new Array(plots.length)
      .fill([])
      .map(() => new Array(plots.length).fill([]).map(() => []));

    // 2D arrays used to collect the number of out- and inbound connections
    // indexed like so: [plotIndex][nodeIndex in that plot]
    const nOutboundConnsPerNode: number[][] = new Array(plots.length)
      .fill([])
      .map((_, i) => new Array(plots[i].nodes.length).fill(0));
    const nInboundConnsPerNode: number[][] = new Array(plots.length)
      .fill([])
      .map((_, i) => new Array(plots[i].nodes.length).fill(0));

    // Only use unique edges
    const uniqueEdges = ParseToUniqueEdges.parse(queryResult.edges, false);

    /* Search for the maximum value of the value attribute for each edge.
     Using epsilon as first max value so we do never divide by zero.
     */
    let maxValue = Number.EPSILON;
    uniqueEdges.forEach((e) => {
      if (e.count > maxValue) maxValue = e.count;
    });

    // Calculate the # in- and outbound connections and fill the relations.
    uniqueEdges.forEach((edge) => {
      // Check if the from and to are present in any plots
      if (nodePlotIndexDict[edge.from] && nodePlotIndexDict[edge.to]) {
        // Retrieve the from and to indices of the plotindex and nodeindex from the dictionary
        const fromPlotsIndices = nodePlotIndexDict[edge.from];
        const toPlotsIndices = nodePlotIndexDict[edge.to];

        // The same node can be in multiple plots
        // We need to add all its connections
        fromPlotsIndices.forEach((fromPlot) => {
          nOutboundConnsPerNode[fromPlot.plotIndex][fromPlot.nodeIndex]++;

          toPlotsIndices.forEach((toPlot) => {
            // Add the connection to the relations list, with the from and to indices
            relations[fromPlot.plotIndex][toPlot.plotIndex].push({
              fromIndex: fromPlot.nodeIndex,
              toIndex: toPlot.nodeIndex,
              value: relationScaleCalculation(edge.attributes[relationSelectedAttributeNumerical]),
              colour: relationColourCalculation(
                edge.attributes[relationSelectedAttributeCatecorigal],
              ),
            });

            nInboundConnsPerNode[toPlot.plotIndex][toPlot.nodeIndex]++;
          });
        });
      }
    });

    // Determine the node position if plotSpecification had an out- or inboundconnection as x or y axis specified
    this.determineNodePosForInOutboundConn(
      plots,
      plotSpecifications,
      nInboundConnsPerNode,
      nOutboundConnsPerNode,
    );

    return { plots, relations };
  }

  /**
   * If the node position is of # in- or outbound connections, set this as the original node position.
   * @param plots {PlotInputData[]} The plots.
   * @param plotSpecs {PlotSpecifications[]} The plot specifications.
   * @param nInboundConnsPerNode {number[][]} The number of inbound connections for plots.
   * @param nOutboundConnsPerNode {number[][]} The number of outbound connections for plots.
   */
  private static determineNodePosForInOutboundConn(
    plots: PlotInputData[],
    plotSpecs: PlotSpecifications[],
    nInboundConnsPerNode: number[][],
    nOutboundConnsPerNode: number[][],
  ): void {
    // Determine the node position if plotSpecification had an out- or inboundconnection as x or y axis specified
    plots.forEach((plot, plotIndex) => {
      plot.nodes.forEach((node, nodeIndex) => {
        // Determine the x pos if the plotspecXAxis is of enum out- or inboundConnections
        const x = this.getAxisPosOutOrInboundConns(
          plotSpecs[plotIndex].xAxis,
          plotIndex,
          nodeIndex,
          nOutboundConnsPerNode,
          nInboundConnsPerNode,
        );
        if (x != undefined) node.originalPosition.x = x;
        // Same for the y pos
        const y = this.getAxisPosOutOrInboundConns(
          plotSpecs[plotIndex].yAxis,
          plotIndex,
          nodeIndex,
          nOutboundConnsPerNode,
          nInboundConnsPerNode,
        );
        if (y != undefined) node.originalPosition.y = y;
      });
    });
  }

  /**
   * Filters the nodes with the plotSpecifications.
   * @param nodes {Node[]} The query result nodes to filter on.
   * @param plotSpecs {PlotSpecifications[]} The plot specifications used for filtering.
   * @returns plots with the filtered nodes in them, and a nodePlotIndexDict used for getting the plotIndex and nodeIndex for a node.
   */
  private static filterNodesWithPlotSpecs(
    nodes: Node[],
    plotSpecs: PlotSpecifications[],
  ): {
    plots: PlotInputData[];
    nodePlotIndexDict: Record<string, { plotIndex: number; nodeIndex: number }[]>;
  } {
    // Initialze the plots with a title and the default witdh and height
    const plots: PlotInputData[] = [];
    plotSpecs.forEach((plotSpec) => {
      plots.push({
        title: plotSpec.labelAttributeType + ':' + plotSpec.labelAttributeValue,
        nodes: [],
        width: plotSpec.width,
        height: plotSpec.height,
      });
    });

    // Dictionary used for getting the plotIndex and nodeIndex for a node
    // plotIndex: in which plot the node is
    // nodeIndex: the index of the node in the list of nodes for that plot
    // A node could be in multiple plots so that is why the value is an array.
    const nodePlotIndexDict: Record<string, { plotIndex: number; nodeIndex: number }[]> = {};

    // Add all nodes to their plot if they satisfy its corresponding plotspec
    nodes.forEach((node) => {
      for (let i = 0; i < plotSpecs.length; i++) {
        // Check if the node has an label attributeType value that matches the filter
        if (
          plotSpecs[i].entity == node.id.split('/')[0] &&
          plotSpecs[i].labelAttributeValue == node.attributes[plotSpecs[i].labelAttributeType]
        ) {
          // Check if the axisPositioning spec is of equalDistance or attributeType
          const x = this.getAxisPosEqualDistanceOrAttrType(
            plotSpecs[i].xAxis,
            plots[i].nodes.length + 1,
            plotSpecs[i].xAxisAttributeType,
            node.attributes,
          );
          const y = this.getAxisPosEqualDistanceOrAttrType(
            plotSpecs[i].yAxis,
            plots[i].nodes.length + 1,
            plotSpecs[i].yAxisAttributeType,
            node.attributes,
          );

          // Add the node to the correct plot
          plots[i].nodes.push({
            id: node.id,
            data: { text: node.id },
            originalPosition: { x, y },
            attributes: node.attributes,
          });

          if (!nodePlotIndexDict[node.id]) nodePlotIndexDict[node.id] = [];

          nodePlotIndexDict[node.id].push({
            plotIndex: i,
            nodeIndex: plots[i].nodes.length - 1,
          });
        }
      }
    });

    return { plots, nodePlotIndexDict };
  }

  /**
   * Determine the position based on the provided axispositioning specification
   * Check for enums equalDistance and attributeType
   * @param plotSpecAxis
   * @param nodeIndex
   * @param xAxisAttrType
   * @param attributes
   * @returns
   */
  private static getAxisPosEqualDistanceOrAttrType(
    plotSpecAxis: AxisLabel,
    nodeIndex: number,
    xAxisAttrType: string,
    attributes: Record<string, any>,
  ): number {
    switch (plotSpecAxis) {
      case AxisLabel.byAttribute:
        if (attributes[xAxisAttrType] && !isNaN(attributes[xAxisAttrType]))
          return +attributes[xAxisAttrType];
        else return nodeIndex;

      default:
        // plotSpecAxis == equalDistance
        return nodeIndex;
    }
  }

  /**
   *
   * @param plotSpecAxis
   * @param plotIndex
   * @param nodeIndex
   * @param nOutboundConnsPerNode
   * @param nInboundConnsPerNode
   * @returns
   */
  private static getAxisPosOutOrInboundConns(
    plotSpecAxis: AxisLabel,
    plotIndex: number,
    nodeIndex: number,
    nOutboundConnsPerNode: number[][],
    nInboundConnsPerNode: number[][],
  ): number | undefined {
    switch (plotSpecAxis) {
      case AxisLabel.outboundConnections:
        return nOutboundConnsPerNode[plotIndex][nodeIndex];

      case AxisLabel.inboundConnections:
        return nInboundConnsPerNode[plotIndex][nodeIndex];

      default:
        return undefined;
    }
  }
}
