/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { CalcDistance } from '../../utils/CalcDistance';
import CalcConnectionLinePositionsUseCase from '../CalcConnectionLinePositionsUseCase';
import CalcScaledPosUseCase from '../CalcScaledPositionsUseCase';
import CalcXYMinMaxUseCase from '../CalcXYMinMaxUseCase';
import { createRandomData } from './CreateRandom';

/** Testsuite for the calculating of the connection line positioning use case */
describe('CalcConnectionLinePositionsUseCase', () => {
  it('the distance between the control point and start end line', () => {
    const { plots, relations } = createRandomData();

    // Calculate the scaled positions to the width and height of a plot
    const scaledPlots = plots.map((plot) => {
      const minmaxAxis = CalcXYMinMaxUseCase.calculate(plot);
      const scaledPositions = CalcScaledPosUseCase.calculate(plot, minmaxAxis.x, minmaxAxis.y);

      return {
        title: plot.title,
        nodes: plot.nodes.map((node, i) => {
          return { ...node, scaledPosition: scaledPositions[i] };
        }),
        minmaxXAxis: minmaxAxis.x,
        minmaxYAxis: minmaxAxis.y,
        width: plot.width,
        height: plot.height,
        possibletTitleAttributeValues: [],
      };
    });

    for (let fromPlot = 0; fromPlot < plots.length; fromPlot++)
      for (let toPlot = 0; toPlot < plots.length; toPlot++) {
        relations[fromPlot][toPlot].forEach((relation) => {
          const nodeA = scaledPlots[fromPlot].nodes[relation.fromIndex];
          const nodeB = scaledPlots[toPlot].nodes[relation.toIndex];
          const start = nodeA.scaledPosition;
          const end = nodeB.scaledPosition;

          const { controlPoint } = CalcConnectionLinePositionsUseCase.calculatePositions(
            start,
            end,
            10,
          );

          const distStartEnd = CalcDistance(start, end);

          if (distStartEnd != 0) {
            const distanceCPToStartEnd =
              Math.abs(
                (end.x - start.x) * (start.y - controlPoint.y) -
                  (end.y - start.y) * (start.x - controlPoint.x),
              ) / distStartEnd;

            const expectedDistance = distStartEnd * 0.25;

            expect(distanceCPToStartEnd).toBeCloseTo(expectedDistance, 5);
          }
        });
      }
  });
});
