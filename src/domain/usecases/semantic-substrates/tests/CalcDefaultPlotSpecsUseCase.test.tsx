/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import CalcDefaultPlotSpecsUseCase from '../CalcDefaultPlotSpecsUseCase';
import mockNodeLinkResult from '../../../../data/mock-data/query-result/big2ndChamberQueryResult';

/** Testsuite for the calculating of the default plot specs use case */
describe('CalcDefaultPlotSpecUseCase', () => {
  it('should calculate a default plot spec from incoming node link data', () => {
    const expected = [
      {
        entity: 'kamerleden',
        labelAttributeType: 'partij',
        labelAttributeValue: 'VVD',
        xAxis: 'evenly spaced',
        yAxis: '# outbound connections',
        xAxisAttributeType: '',
        yAxisAttributeType: '',
        width: 800,
        height: 200,
      },
      {
        entity: 'kamerleden',
        labelAttributeType: 'partij',
        labelAttributeValue: 'D66',
        xAxis: 'evenly spaced',
        yAxis: '# outbound connections',
        xAxisAttributeType: '',
        yAxisAttributeType: '',
        width: 800,
        height: 200,
      },
      {
        entity: 'kamerleden',
        labelAttributeType: 'partij',
        labelAttributeValue: 'GL',
        xAxis: 'evenly spaced',
        yAxis: '# outbound connections',
        xAxisAttributeType: '',
        yAxisAttributeType: '',
        width: 800,
        height: 200,
      },
    ];

    const received = CalcDefaultPlotSpecsUseCase.calculate(mockNodeLinkResult);
    expect(received).toEqual(expected);
  });
});
