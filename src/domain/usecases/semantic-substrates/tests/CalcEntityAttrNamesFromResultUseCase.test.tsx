/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import CalcEntityAttrNamesFromSchemaUseCase from '../CalcEntityAttrNamesFromSchemaUseCase';
import CalcEntityAttrNamesFromResultUseCase from '../CalcEntityAttrNamesFromResultUseCase';
import chamberSchemaMock from '../../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import big2ndChamberQueryResult from '../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import { Schema } from '../../../entity/graph-schema/structures/InputDataTypes';
import { NodeLinkResultType } from '../../../entity/query-result/structures/NodeLinkResultType';

/** Testsuite for the calculating of the entity attribute names from the schema */
describe('CalcEntityAttrNamesFromResultUseCase', () => {
  it('should calculate the entity and attribute names, and remove entitynames that are not in the result.', () => {
    const receivedSchemaResultType = CalcEntityAttrNamesFromSchemaUseCase.calculate(chamberSchemaMock as Schema);
    const received = CalcEntityAttrNamesFromResultUseCase.CalcEntityAttrNamesFromResult(big2ndChamberQueryResult as NodeLinkResultType, receivedSchemaResultType);

    const expected = {
      entityNames: ['kamerleden', 'commissies' ],
      attributesPerEntity: {
        commissies: {
          textAttributeNames: ['naam'],
          boolAttributeNames: [],
          numberAttributeNames: [],
        },
        kamerleden: {
          textAttributeNames: ['img', 'naam', 'partij', 'woonplaats'],
          boolAttributeNames: [],
          numberAttributeNames: ['anc', 'leeftijd'],
        },
        moties: {
          textAttributeNames: ['datum', 'indiener', 'info'],
          boolAttributeNames: [],
          numberAttributeNames: [],
        },
        partijen: {
          textAttributeNames: ['naam', 'voorzitter'],
          boolAttributeNames: [],
          numberAttributeNames: ['voorzitterId', 'zetels'],
        },
      },
    };

    expect(received).toEqual(expected);
  });
});
