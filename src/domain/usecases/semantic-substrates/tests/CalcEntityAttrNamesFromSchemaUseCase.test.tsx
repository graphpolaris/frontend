/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import CalcEntityAttrNamesFromSchemaUseCase from '../CalcEntityAttrNamesFromSchemaUseCase';
import chamberSchemaMock from '../../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import { Schema } from '../../../entity/graph-schema/structures/InputDataTypes';

/** Testsuite for the calculating of the entity attribute names from the schema */
describe('CalcEntitUAttrNamesFromSchemaUseCase', () => {
  it('should calculate the entity and attribute names', () => {
    const received = CalcEntityAttrNamesFromSchemaUseCase.calculate(chamberSchemaMock as Schema);

    const expected = {
      entityNames: ['commissies', 'kamerleden', 'moties', 'partijen'],
      attributesPerEntity: {
        commissies: {
          textAttributeNames: ['naam'],
          boolAttributeNames: [],
          numberAttributeNames: [],
        },
        kamerleden: {
          textAttributeNames: ['img', 'naam', 'partij', 'woonplaats'],
          boolAttributeNames: [],
          numberAttributeNames: ['anc', 'leeftijd'],
        },
        moties: {
          textAttributeNames: ['datum', 'indiener', 'info'],
          boolAttributeNames: [],
          numberAttributeNames: [],
        },
        partijen: {
          textAttributeNames: ['naam', 'voorzitter'],
          boolAttributeNames: [],
          numberAttributeNames: ['voorzitterId', 'zetels'],
        },
      },
    };

    expect(received).toEqual(expected);
  });
});
