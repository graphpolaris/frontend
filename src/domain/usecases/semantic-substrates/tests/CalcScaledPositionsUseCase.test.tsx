/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { PlotInputData } from '../../../entity/semantic-substrates/structures/Types';
import CalcScaledPositionsUseCase from '../CalcScaledPositionsUseCase';
import CalcXYMinMaxUseCase from '../CalcXYMinMaxUseCase';
import { createRandomData } from './CreateRandom';

/** Testsuite for the calculating of the scaled positions use case */
describe('CalcScaledPositionsUseCase', () => {
  it('should work with 2 nodes with positive x y values', () => {
    const mockNodes = [
      {
        id: '1',
        data: { text: 'mockNode' },
        originalPosition: { x: 10, y: 50 },
      },
      {
        id: '2',
        data: { text: 'mockNode' },
        originalPosition: { x: 20, y: 60 },
      },
    ];

    const input: PlotInputData = {
      title: 'mocktitle',
      nodes: mockNodes,
      width: 200,
      height: 100,
    };

    const { x, y } = CalcXYMinMaxUseCase.calculate(input);

    const expectedScaledPos = input.nodes.map((node) => {
      return {
        x: round2Decimals(scale(x.min, x.max, input.width, node.originalPosition.x)),
        y: round2Decimals(scale(y.max, y.min, input.height, node.originalPosition.y)),
      };
    });

    const received = CalcScaledPositionsUseCase.calculate(input, x, y);
    received.forEach((pos) => {
      pos.x = round2Decimals(pos.x);
      pos.y = round2Decimals(pos.y);
    });

    expect(received).toEqual(expectedScaledPos);
  });

  it('should work with random generated nodes', () => {
    const input = createRandomData().plots[0];

    const { x, y } = CalcXYMinMaxUseCase.calculate(input);

    const expectedScaledPos = input.nodes.map((node) => {
      return {
        x: round2Decimals(scale(x.min, x.max, input.width, node.originalPosition.x)),
        y: round2Decimals(scale(y.max, y.min, input.height, node.originalPosition.y)),
      };
    });

    const received = CalcScaledPositionsUseCase.calculate(input, x, y);
    received.forEach((pos) => {
      pos.x = round2Decimals(pos.x);
      pos.y = round2Decimals(pos.y);
    });

    expect(received).toEqual(expectedScaledPos);
  });

  function scale(sourceMin: number, sourceMax: number, targetMax: number, sourceValue: number) {
    // x.min == 8,  x.max == 22
    // y.min == 48, y.max == 62;

    // [0 - 200], [8 - 22]    ((10-8)/(22 - 8)) * 200
    //                        (200/(22 - 8)) * (22 - 8)

    const coeff = targetMax / (sourceMax - sourceMin);

    return coeff * (sourceValue - sourceMin);
  }

  function round2Decimals(num: number): number {
    return Math.round(num * 100) / 100;
  }
});
