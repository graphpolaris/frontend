/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { NodeData, PlotInputData } from '../../../entity/semantic-substrates/structures/Types';
import CalcXYMinMaxUseCase from '../CalcXYMinMaxUseCase';

/** Testsuite for the calculating of the x and y minmax position */
describe('CalcXYMinMaxUseCase', () => {
  it('should correctly min max width random node data', () => {
    const numberOfNodes = 10;
    const randomXValues = [];
    const randomYValues = [];
    const nodes: {
      id: string;
      data: NodeData;
      originalPosition: {
        x: number;
        y: number;
      };
    }[] = [];
    // Generate some random nodes
    for (let i = 0; i < numberOfNodes; i++) {
      const rx = Math.floor(Math.random() * 100000);
      const ry = Math.floor(Math.random() * 100000);

      nodes.push({
        id: '',
        data: { text: '' },
        originalPosition: { x: rx, y: ry },
      });
      randomXValues.push(rx);
      randomYValues.push(ry);
    }

    let minX = Math.min(...randomXValues);
    let maxX = Math.max(...randomXValues);
    const xDiff = (maxX - minX) * 0.2;
    minX -= xDiff;
    maxX += xDiff;
    let minY = Math.min(...randomYValues);
    let maxY = Math.max(...randomYValues);
    const yDiff = (maxY - minY) * 0.2;
    minY -= yDiff;
    maxY += yDiff;

    const input: PlotInputData = {
      title: 'mockTitle',
      nodes: nodes,
      width: 500,
      height: 600,
    };

    const minmax = CalcXYMinMaxUseCase.calculate(input);
    expect(minmax).toEqual({ x: { min: minX, max: maxX }, y: { min: minY, max: maxY } });
  });

  it('should give something with an empty input', () => {
    const input: PlotInputData = {
      title: 'mockTitle',
      nodes: [],
      width: 500,
      height: 600,
    };

    const minmax = CalcXYMinMaxUseCase.calculate(input);
    expect(minmax).toEqual({ x: { min: -10, max: 10 }, y: { min: -10, max: 10 } });
  });

  it('should calculate min and max of 1 node', () => {
    const input: PlotInputData = {
      title: 'mockTitle',
      nodes: [
        {
          id: 'test',
          data: { text: 'test' },
          originalPosition: { x: 10, y: 50 },
        },
      ],
      width: 500,
      height: 600,
    };

    const minmax = CalcXYMinMaxUseCase.calculate(input);
    expect(minmax).toEqual({ x: { min: 9, max: 11 }, y: { min: 49, max: 51 } });
  });
});
