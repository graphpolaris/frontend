/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { PlotInputData, RelationType } from '../../../entity/semantic-substrates/structures/Types';

/**
 * Function that creates random data to be used for testing.
 * @returns { plots: PlotInputData[]; relations: RelationType[][][] } Randomly created plots and relations.
 */
export function createRandomData(): { plots: PlotInputData[]; relations: RelationType[][][] } {
  const plots: PlotInputData[] = [];

  const nPlots = 4;
  const nNodesPerPlot = 100;
  const maxNRelations = 50;

  const plotNames = ['California', 'Nevada', 'Texas', 'Ohio'];

  for (let i = 0; i < nPlots; i++) {
    const newPlot: PlotInputData = {
      title: plotNames[i],
      width: 800,
      height: 200,
      nodes: [],
    };

    for (let j = 0; j < nNodesPerPlot; j++)
      newPlot.nodes.push({
        id: 'p' + i + '-' + j,
        data: { text: 'p' + i + '-' + j },
        originalPosition: { x: Math.random() * 1000 - 500, y: Math.random() * 1000 - 500 },
        attributes: [],
      });

    plots.push(newPlot);
  }

  const relations: RelationType[][][] = [];
  for (let x = 0; x < plots.length; x++) {
    relations.push([]);
    for (let y = 0; y < plots.length; y++) {
      relations[x].push([]);
      for (let i = 0; i < Math.random() * maxNRelations; i++) {
        relations[x][y].push({
          fromIndex: Math.floor(Math.random() * nNodesPerPlot),
          toIndex: Math.floor(Math.random() * nNodesPerPlot),
          value: 0,
        });
      }
    }
  }

  return { plots, relations };
}
