/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  MinMaxType,
  NodeType,
  PlotType,
} from '../../../entity/semantic-substrates/structures/Types';
import CalcScaledPosUseCase from '../CalcScaledPositionsUseCase';
import CalcXYMinMaxUseCase from '../CalcXYMinMaxUseCase';
import FilterUseCase from '../FilterUseCase';
import { createRandomData } from './CreateRandom';

/** Testsuite for the filter use case */
describe('FilterUseCase', () => {
  it('should check if a node matches a filter', () => {
    const node: NodeType = {
      id: '1',
      data: { text: 'mockTitle' },
      originalPosition: { x: -10, y: 8.3 },
      scaledPosition: { x: 40, y: 80 },
    };

    let filter = {
      x: { min: -43, max: 9 },
      y: { min: -1, max: 8.3 },
    };

    expect(FilterUseCase.checkIfNodeMatchesFilter(node, filter)).toBe(true);

    filter.y.max = 8.2;
    expect(FilterUseCase.checkIfNodeMatchesFilter(node, filter)).toBe(false);
  });

  it('should filter relations', () => {
    const { plots, relations } = createRandomData();
    // Calculate the scaled positions to the width and height of a plot
    let yOffset = 0;
    const scaledPlots: PlotType[] = plots.map((plot) => {
      const minmaxAxis = CalcXYMinMaxUseCase.calculate(plot);
      const scaledPositions = CalcScaledPosUseCase.calculate(plot, minmaxAxis.x, minmaxAxis.y);

      const thisYOffset = yOffset;
      yOffset += plot.height + 50;
      return {
        title: plot.title,
        nodes: plot.nodes.map((node, i) => {
          return { ...node, scaledPosition: scaledPositions[i] };
        }),
        minmaxXAxis: minmaxAxis.x,
        minmaxYAxis: minmaxAxis.y,
        width: plot.width,
        height: plot.height,
        yOffset: thisYOffset,
        possibleTitleAttributeValues: [],
      };
    });

    const filteredRelations = relations.map((i) => i.map((j) => j.map((v) => v))); // Copy relations

    const filters = scaledPlots.map((plot) => {
      return {
        x: { min: plot.minmaxXAxis.min, max: plot.minmaxXAxis.max },
        y: { min: plot.minmaxYAxis.min, max: plot.minmaxYAxis.max },
      };
    });

    // Randomly change a filter 10 times
    for (let i = 0; i < 10; i++) {
      const randomPlotIndex = Math.floor(Math.random() * filters.length);
      filters[randomPlotIndex] = getRandomFilter(scaledPlots[randomPlotIndex]);
      FilterUseCase.filterRelations(
        relations,
        filteredRelations,
        scaledPlots,
        filters,
        randomPlotIndex,
      );

      const expectedFilteredRels = relations.map((toRelations, fromPlot) => {
        return toRelations.map((rels, toPlot) => {
          return rels.filter((relation) => {
            const fromNode = scaledPlots[fromPlot].nodes[relation.fromIndex];
            const toNode = scaledPlots[toPlot].nodes[relation.toIndex];

            const fromMatches = FilterUseCase.checkIfNodeMatchesFilter(fromNode, filters[fromPlot]);
            const toMatches = FilterUseCase.checkIfNodeMatchesFilter(toNode, filters[toPlot]);
            return (fromMatches && toMatches) || (fromPlot == toPlot && (fromMatches || toMatches));
          });
        });
      });

      expect(filteredRelations).toEqual(expectedFilteredRels);
    }
  });

  function getRandomFilter(plot: PlotType): { x: MinMaxType; y: MinMaxType } {
    const xDiff = plot.minmaxXAxis.max - plot.minmaxXAxis.min;
    const minX = plot.minmaxXAxis.min + Math.random() * xDiff;
    const maxX = minX + Math.random() * (plot.minmaxXAxis.max - minX);

    const yDiff = plot.minmaxYAxis.max - plot.minmaxYAxis.min;
    const minY = plot.minmaxYAxis.min + Math.random() * yDiff;
    const maxY = minY + Math.random() * (plot.minmaxYAxis.max - minY);
    return {
      x: { min: minX, max: maxX },
      y: { min: minY, max: maxY },
    };
  }
});
