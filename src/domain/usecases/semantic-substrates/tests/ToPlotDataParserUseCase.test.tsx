/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import ToPlotDataParserUseCase from '../ToPlotDataParserUseCase';
import MockQueryResult from '../../../../data/mock-data/query-result/bigMockQueryResults';
import ParseToUniqueEdges from '../../ParseToUniqueEdgesUseCase';
import {
  AxisLabel,
  PlotSpecifications,
  RelationType,
} from '../../../entity/semantic-substrates/structures/Types';
import { Node } from '../../../entity/graph-schema/structures/InputDataTypes';

/** Testsuite for the plot data parser use case */
describe('ToPlotDataParserUseCase', () => {
  const plotSpecs: PlotSpecifications[] = [
    {
      entity: 'airports',
      labelAttributeType: 'state',
      labelAttributeValue: 'CA',
      xAxis: AxisLabel.evenlySpaced,
      yAxis: AxisLabel.outboundConnections,
      xAxisAttributeType: '',
      yAxisAttributeType: '',
      width: 200,
      height: 200,
    },
    {
      entity: 'airports',
      labelAttributeType: 'state',
      labelAttributeValue: 'TX',
      xAxis: AxisLabel.evenlySpaced,
      yAxis: AxisLabel.outboundConnections,
      xAxisAttributeType: '',
      yAxisAttributeType: '',
      width: 200,
      height: 200,
    },
  ];
  const entityAttributes: Node[] = [
    { name: 'airports', attributes: [{ name: 'state', type: 'string' }] },
  ];

  it('should have the correct nodes in the plots', () => {
    const { plots } = ToPlotDataParserUseCase.parseQueryResult(
      MockQueryResult,
      plotSpecs,
      'long',
      'lat',
      (_: number) => {
        return _;
      },
      (_: string) => {
        return _;
      },
    );

    plots.forEach((plot, i) => {
      const nodeIDsFromResult = plot.nodes.map((node) => node.id);
      const nodeIDs = MockQueryResult.nodes
        .filter(
          (node) =>
            node.id.split('/')[0] == plotSpecs[i].entity &&
            node.attributes.state == plotSpecs[i].labelAttributeValue,
        )
        .map((node) => node.id);

      expect(nodeIDsFromResult.sort()).toEqual(nodeIDs.sort());
    });
  });

  it('should return the right relations', () => {
    const { relations } = ToPlotDataParserUseCase.parseQueryResult(
      MockQueryResult,
      plotSpecs,
      'long',
      'lat',
      (_: number) => {
        return _;
      },
      (_: string) => {
        return _;
      },
    );

    const nodesInPlots = plotSpecs.map((_, i) =>
      MockQueryResult.nodes
        .filter(
          (node) =>
            node.id.split('/')[0] == plotSpecs[i].entity &&
            node.attributes.state == plotSpecs[i].labelAttributeValue,
        )
        .map((node) => node.id),
    );

    /* Using epsilon as first max value so we do never divide by zero.
     */
    const uniqueEdges = ParseToUniqueEdges.parse(MockQueryResult.edges, false);
    let maxValue = Number.EPSILON;
    uniqueEdges.forEach((e) => {
      if (e.count > maxValue) maxValue = e.count;
    });
    const expectedRelations: RelationType[][][] = [];
    for (let fromPlot = 0; fromPlot < plotSpecs.length; fromPlot++) {
      expectedRelations.push([]);
      for (let toPlot = 0; toPlot < plotSpecs.length; toPlot++) {
        expectedRelations[fromPlot].push([]);

        uniqueEdges.forEach((edge) => {
          if (nodesInPlots[fromPlot].includes(edge.from) && nodesInPlots[toPlot].includes(edge.to))
            expectedRelations[fromPlot][toPlot].push({
              fromIndex: nodesInPlots[fromPlot].indexOf(edge.from),
              toIndex: nodesInPlots[toPlot].indexOf(edge.to),
              value: undefined,
              colour: undefined,
            });
        });
      }
    }

    expect(relations).toEqual(expectedRelations);
  });
});
