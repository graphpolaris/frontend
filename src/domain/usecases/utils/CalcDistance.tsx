/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Position } from '../../entity/utils/Position';

/**
 * Calculates the difference between two positions.
 * @param {Position} posA The first position.
 * @param {Position} posB The second position.
 * @return {number} The distance between the first and second position.
 */
export function CalcDistance(posA: Position, posB: Position): number {
  const diffX = posA.x - posB.x;
  const diffY = posA.y - posB.y;

  return Math.sqrt(diffX * diffX + diffY * diffY);
}
