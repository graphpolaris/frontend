/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/**
 * Rotates a vector by given degrees, clockwise.
 * @param {number, number} vec The vector to be rotated.
 * @param {number} degrees The amount of degrees the vector needs to be rotated.
 * @return {number, number} The rotated vector.
 */
export function RotateVectorByDeg(
  vec: { x: number; y: number },
  degrees: number,
): { x: number; y: number } {
  const radians = -(degrees % 360) * 0.01745329251; // degrees * (PI/180)

  return {
    x: vec.x * Math.cos(radians) - vec.y * Math.sin(radians),
    y: vec.x * Math.sin(radians) + vec.y * Math.cos(radians),
  };
}
