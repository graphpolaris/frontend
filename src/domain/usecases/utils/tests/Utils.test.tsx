/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Position } from '../../../entity/utils/Position';
import { CalcDistance } from '../CalcDistance';
import { RotateVectorByDeg } from '../RotateVec';

/** Testsuite for utility distance and rotate functions */
describe('Semantic substrates utils', () => {
  it('should calculate the distance between two positions', () => {
    let posA: Position = { x: -10, y: 0 };
    let posB: Position = { x: 0, y: 0 };
    expect(CalcDistance(posA, posB)).toBe(10);

    posA = { x: 1, y: 1 };
    posB = { x: 1, y: 1 };
    expect(CalcDistance(posA, posB)).toBe(0);

    posA = { x: -91, y: 289 };
    posB = { x: -3390123, y: -99883 };
    expect(CalcDistance(posA, posB)).toBe(
      Math.sqrt((posA.x - posB.x) * (posA.x - posB.x) + (posA.y - posB.y) * (posA.y - posB.y)),
    );
  });

  it('should rotate a vec by degrees', () => {
    let v = { x: 0, y: 3 };
    let rotated = RotateVectorByDeg(v, 90);

    expect(rotated.x).toBeCloseTo(3, 4);
    expect(rotated.y).toBeCloseTo(0, 4);

    for (let i = 0; i < 20; i++) {
      v = { x: -10 + Math.random() * 20, y: -10 + Math.random() * 20 };
      const rDeg = Math.random() * 900;

      rotated = RotateVectorByDeg(v, rDeg);

      const vL = Math.sqrt(v.x * v.x + v.y * v.y);
      v.x /= vL;
      v.y /= vL;
      const rotatedL = Math.sqrt(rotated.x * rotated.x + rotated.y * rotated.y);
      rotated.x /= rotatedL;
      rotated.y /= rotatedL;

      const r = (Math.atan2(v.y, v.x) - Math.atan2(rotated.y, rotated.x)) * 57.2957795;

      expect(r < 0 ? r + 360 : r).toBeCloseTo(rDeg % 360, 3);
    }
  });
});
