import time
import pyautogui
import pywinauto
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

class bcolors:
    OKGREEN = '\033[92m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

res = ""
while(not(res == "1920" or res == "2560" or res == "3840")):   
    res = input("What is your screen width, 1920, 2560, 3840?\n")
res = int(res) / 1920
time.sleep(2)
driver = webdriver.Firefox()

try:
    action = ActionChains(driver)
    # driver.get("http://127.0.0.1:3000/")
    driver.get("http://datastrophe.science.uu.nl/develop/")
    time.sleep(1)
    
    t, c = u'GraphPolaris - Mozilla Firefox', u'GraphPolaris'
    handle = pywinauto.findwindows.find_windows(title=t)[0]
    app = pywinauto.application.Application().connect(handle=handle)
    window = app.window(handle=handle)
    window.set_focus()

    driver.find_element_by_xpath("/html/body/div[1]/div/div/div[1]/header/div/div/button/span[1]/span").click()
    time.sleep(1)
    driver.find_element_by_xpath("/html/body/div[1]/div/div/div[1]/header/div/div/div/button").click()
    time.sleep(1)
    driver.find_element_by_xpath("/html/body/div[7]/div[3]/ul/li[4]").click()
    time.sleep(1)
    driver.find_element_by_xpath("/html/body/div[1]/div/div/div[1]/div/div/form/div[7]/button[1]").click()
    time.sleep(5)
    el1 = driver.find_element_by_xpath("/html/body/div[1]/div/div/div[4]/div/div[2]/div/div[1]/div[1]/div[7]/div/div/div[2]/span[2]")
    pyautogui.moveTo(130 * res,530 * res)
    pyautogui.drag(400 * res,300 * res, 0.2, button='left')
    time.sleep(1)
    driver.find_element_by_xpath("/html/body/div[1]/div/div/div[2]/div/div[1]").click()
    time.sleep(1)
    driver.find_element_by_xpath("/html/body/div[2]/div[3]/ul/li[1]/div/span").click()
    time.sleep(1)
    driver.find_element_by_xpath("/html/body/div[3]/div[3]/ul/li[3]/div/span").click()
    assert driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/div/div[2]/div/div/div/div/span[1]/div/span[1]').text == "2 items"
    assert driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div[1]/span[2]/div/span[1]').text == "1165 items"
    assert driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div[2]/span[2]/div/span[1]').text == "172 items"
    driver.close()
    print(f"{bcolors.OKGREEN}THE SYSTEM TEST HAS PASSED{bcolors.ENDC}")
except Exception as e:
    print(f"{bcolors.FAIL}THE SYSTEM TEST HAS FAILED{bcolors.ENDC}")    
    print("WITH THE FOLLOWING ERROR")
    print(e)
    driver.close()