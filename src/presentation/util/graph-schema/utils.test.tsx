import { ArrowHeadType, Position } from 'react-flow-renderer';
import {
  doBoxesOverlap,
  makeBoundingBox,
  getCenter,
  getMarkerEnd,
  capitalizeFirstLetter,
  getWidthOfText,
  calcWidthEntityNodeBox,
  calcWidthRelationNodeBox,
  calculateAttributeQuality,
  calculateEntityQuality,
  calculateRelationQuality,
  numberPredicates,
} from './utils';

describe('schemaViewModelImpl', () => {
  beforeEach(() => jest.resetModules());

  it('should corectly check if boxes ofverlap', () => {
    const boundingbox1 = makeBoundingBox(0, 0, 10, 10);
    const boundingbox2 = makeBoundingBox(2, 2, 12, 12);
    const boundingbox3 = makeBoundingBox(20, 20, 30, 30);
    const boundingbox4 = makeBoundingBox(2, -20, 30, -30);
    expect(doBoxesOverlap(boundingbox1, boundingbox2)).toEqual(true);
    expect(doBoxesOverlap(boundingbox1, boundingbox3)).toEqual(false);
    expect(doBoxesOverlap(boundingbox1, boundingbox4)).toEqual(false);
  });

  it('should corectly check getMarkerEnd', () => {
    expect(getMarkerEnd(ArrowHeadType.Arrow, undefined)).toEqual(`url(#react-flow__arrow)`);
    expect(getMarkerEnd(undefined, 'test')).toEqual(`url(#test)`);
    expect(getMarkerEnd(undefined, undefined)).toEqual(`none`);
  });

  it('should corectly get the center of the line', () => {
    let sourceX = 0;
    let sourceY = 0;
    let targetX = 10;
    let targetY = 10;
    let offset = 0;
    let sourcePosition;
    let targetPosition;
    const [centerX, centerY, offsetx, offsety] = getCenter({
      sourceX,
      sourceY,
      targetX,
      targetY,
      offset,
      sourcePosition,
      targetPosition,
    });
    expect(centerX).toEqual(5);
    expect(centerY).toEqual(5);
    expect(offsetx).toEqual(5);
    expect(offsety).toEqual(5);
  });

  it('should corectly get the center of the line', () => {
    let sourceX = 0;
    let sourceY = 0;
    let targetX = 10;
    let targetY = 10;
    let offset = 0;
    let sourcePosition = Position.Left;
    let targetPosition = Position.Bottom;
    const [centerX, centerY, offsetx, offsety] = getCenter({
      sourceX,
      sourceY,
      targetX,
      targetY,
      offset,
      sourcePosition,
      targetPosition,
    });
    expect(centerX).toEqual(10);
    expect(centerY).toEqual(0);
    expect(offsetx).toEqual(10);
    expect(offsety).toEqual(0);
  });

  it('should corectly get the center of the line', () => {
    let sourceX = 20;
    let sourceY = 20;
    let targetX = 10;
    let targetY = 10;
    let offset = 0;
    let sourcePosition = Position.Right;
    let targetPosition = Position.Bottom;
    const [centerX, centerY, offsetx, offsety] = getCenter({
      sourceX,
      sourceY,
      targetX,
      targetY,
      offset,
      sourcePosition,
      targetPosition,
    });
    expect(centerX).toEqual(10);
    expect(centerY).toEqual(20);
    expect(offsetx).toEqual(10);
    expect(offsety).toEqual(0);
  });

  it('should corectly get the center of the line', () => {
    let sourceX = 20;
    let sourceY = 20;
    let targetX = 10;
    let targetY = 10;
    let offset = 0;
    let sourcePosition = Position.Right;
    let targetPosition = Position.Right;
    const [centerX, centerY, offsetx, offsety] = getCenter({
      sourceX,
      sourceY,
      targetX,
      targetY,
      offset,
      sourcePosition,
      targetPosition,
    });
    expect(centerX).toEqual(15);
    expect(centerY).toEqual(15);
    expect(offsetx).toEqual(5);
    expect(offsety).toEqual(5);
  });

  it('should corectly get the center of the line', () => {
    let sourceX = 20;
    let sourceY = 20;
    let targetX = 10;
    let targetY = 10;
    let offset = 0;
    let sourcePosition = Position.Top;
    let targetPosition = Position.Left;
    const [centerX, centerY, offsetx, offsety] = getCenter({
      sourceX,
      sourceY,
      targetX,
      targetY,
      offset,
      sourcePosition,
      targetPosition,
    });
    expect(centerX).toEqual(20);
    expect(centerY).toEqual(10);
    expect(offsetx).toEqual(0);
    expect(offsety).toEqual(10);
  });

  it('capitalizeFirstLetter', () => {
    expect(capitalizeFirstLetter('')).toEqual('');
    expect(capitalizeFirstLetter('hello')).toEqual('Hello');
    expect(capitalizeFirstLetter('1 start with a number')).toEqual('1 start with a number');
    expect(capitalizeFirstLetter('ALREADY CAPITALIZED')).toEqual('ALREADY CAPITALIZED');
  });

  it('getWidthOfText', () => {
    const normalWidth = getWidthOfText('hello', 'serif', '12px');
    const boldFontWeight = getWidthOfText('hello', 'serif', '12px', 'bold');
    const biggerFontSize = getWidthOfText('hello', 'serif', '13px');
    const extraLetter = getWidthOfText('helloo', 'serif', '12px');
    const differentFontFamily = getWidthOfText('hello', 'sans-serif', '12px');

    expect(normalWidth).toBeLessThan(boldFontWeight);
    expect(normalWidth).toBeLessThan(biggerFontSize);
    expect(normalWidth).toBeLessThan(extraLetter);
    expect(normalWidth != differentFontFamily).toEqual(true);
  });

  it('calcWidthEntityNodeBox', () => {
    const twoDigits = calcWidthEntityNodeBox(22, 22);
    const oneDigitMoreAttributes = calcWidthEntityNodeBox(1, 0);
    const oneDigitMoreNodes = calcWidthEntityNodeBox(0, 1);
    const twoDigitsMoreAttributes = calcWidthEntityNodeBox(10, 0);
    const twoDigitsMoreNodes = calcWidthEntityNodeBox(0, 99);

    expect(oneDigitMoreAttributes).toEqual(oneDigitMoreNodes);
    expect(oneDigitMoreAttributes).toBeLessThan(twoDigits);

    expect(oneDigitMoreAttributes).toBeLessThan(twoDigitsMoreAttributes);
    expect(twoDigitsMoreAttributes).toEqual(twoDigitsMoreNodes);
    expect(twoDigitsMoreAttributes).toEqual(twoDigits);
  });

  it('calcWidthRelationNodeBox', () => {
    const twoDigits = calcWidthRelationNodeBox(22, 22);
    const oneDigitMoreAttributes = calcWidthRelationNodeBox(1, 0);
    const oneDigitMoreNodes = calcWidthRelationNodeBox(0, 1);
    const twoDigitsMoreAttributes = calcWidthRelationNodeBox(10, 0);
    const twoDigitsMoreNodes = calcWidthRelationNodeBox(0, 99);

    expect(oneDigitMoreAttributes).toEqual(oneDigitMoreNodes);
    expect(oneDigitMoreAttributes).toBeLessThan(twoDigits);

    expect(oneDigitMoreAttributes).toBeLessThan(twoDigitsMoreAttributes);
    expect(twoDigitsMoreAttributes).toEqual(twoDigitsMoreNodes);
    expect(twoDigitsMoreAttributes).toEqual(twoDigits);
  });

  it('should calculate the quality of the attributes correct', () => {
    const data1 = {
      nodeCount: 10,
      attributes: [{ name: 'leeftijd', type: 'number', nullAmount: 0 }],
      summedNullAmount: 0,
    };
    expect(calculateAttributeQuality(data1)).toEqual(0);
    const data2 = {
      nodeCount: 10,
      attributes: [
        { name: 'leeftijd', type: 'number', nullAmount: 2 },
        { name: 'woonplaats', type: 'string', nullAmount: 4 },
      ],
      summedNullAmount: 6,
    };
    expect(Number(calculateAttributeQuality(data2).toFixed(2))).toEqual(30.0);
  });

  it('should calculate the quality of the entities correct', () => {
    const data1 = {
      nodeCount: 10,
      attributes: [],
      summedNullAmount: 0,
      connectedRatio: 0.4,
    };
    expect(Number(calculateEntityQuality(data1).toFixed(2))).toEqual(60.0);
  });

  it('should calculate the quality of the relations correct', () => {
    const data1 = {
      nodeCount: 10,
      attributes: [],
      summedNullAmount: 0,
      fromRatio: 0.8,
      toRatio: 0.6,
    };
    expect(Number(calculateRelationQuality(data1).toFixed(2))).toEqual(30.0);
  });

  it('should calculate truth-values of each predicate correct', () => {
    expect(numberPredicates['Equal'](1, 1)).toEqual(true);
    expect(numberPredicates['Equal'](1, 2)).toEqual(false);

    expect(numberPredicates['NotEqual'](1, 1)).toEqual(false);
    expect(numberPredicates['NotEqual'](1, 2)).toEqual(true);

    expect(numberPredicates['Smaller'](1, 2)).toEqual(true);
    expect(numberPredicates['Smaller'](1, 1)).toEqual(false);
    expect(numberPredicates['Smaller'](1, 0)).toEqual(false);

    expect(numberPredicates['SmallerOrEqual'](1, 2)).toEqual(true);
    expect(numberPredicates['SmallerOrEqual'](1, 1)).toEqual(true);
    expect(numberPredicates['SmallerOrEqual'](1, 0)).toEqual(false);

    expect(numberPredicates['Bigger'](1, 0)).toEqual(true);
    expect(numberPredicates['Bigger'](1, 1)).toEqual(false);
    expect(numberPredicates['Bigger'](1, 2)).toEqual(false);

    expect(numberPredicates['BiggerOrEqual'](1, 0)).toEqual(true);
    expect(numberPredicates['BiggerOrEqual'](1, 1)).toEqual(true);
    expect(numberPredicates['BiggerOrEqual'](1, 2)).toEqual(false);
  });
});
