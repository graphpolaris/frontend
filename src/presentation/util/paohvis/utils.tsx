/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/**
 * Calculate the width of the specified text.
 * @param txt Text input as string.
 * @param fontname Name of the font.
 * @param fontsize Size of the fond in px.
 * @param fontWeight The weight of the font.
 * @returns {number} Width of the textfield in px.
 */
export const getWidthOfText = (
  txt: string,
  fontname: string,
  fontsize: string,
  fontWeight = 'normal',
): number => {
  let c = document.createElement('canvas');
  let ctx = c.getContext('2d') as CanvasRenderingContext2D;
  let fontspec = fontWeight + ' ' + fontsize + ' ' + fontname;
  if (ctx.font !== fontspec) ctx.font = fontspec;
  return ctx.measureText(txt).width;
};

/**
 * Filters duplicate elements from array with a hashtable.
 * From https://stackoverflow.com/questions/9229645/remove-duplicate-values-from-js-array */
export function uniq(element: number[]) {
  const seen: Record<number, boolean> = {};
  return element.filter(function (item) {
    return seen.hasOwnProperty(item) ? false : (seen[item] = true);
  });
}
