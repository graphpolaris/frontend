/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BaseView from '../view/BaseView';
import AbstractBaseViewModelImpl from './AbstractBaseViewModelImpl';

/** Mock view model implementation used for testing. */
class mockBaseViewModelImpl extends AbstractBaseViewModelImpl {
  public notify(): void {
    this.notifyViewAboutChanges();
  }
}

describe('AbstractBaseViewModelImpl', () => {
  it('Should notify the view only when the view is attached', () => {
    const impl = new mockBaseViewModelImpl();

    const notifyViewMock = jest.fn();
    const baseview: BaseView = {
      onViewModelChanged: notifyViewMock,
    };

    impl.notify();
    expect(notifyViewMock).toHaveBeenCalledTimes(0);

    impl.attachView(baseview);
    impl.notify();
    expect(notifyViewMock).toHaveBeenCalledTimes(1);
  });
});
