/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BaseView from '../view/BaseView';
import BaseViewModel from './BaseViewModel';

/**
 * Abstract class that implements the baseViewModel.
 * Defines the default attachView(), detachView() and notifyViewAboutChanges() functions.
 * Should not be instantiated!
 */
export default class AbstractBaseViewModelImpl implements BaseViewModel {
  protected baseView?: BaseView;

  public attachView(baseView: BaseView): void {
    this.baseView = baseView;
  }

  public detachView(): void {
    this.baseView = undefined;
  }

  /**
   * The notifyViewAboutChanges function notifies the BaseView about changes.
   * The baseview should set the state and thus rerender the component.
   */
  protected notifyViewAboutChanges = (): void => {
    if (this.baseView) {
      this.baseView.onViewModelChanged();
    }
  };
}
