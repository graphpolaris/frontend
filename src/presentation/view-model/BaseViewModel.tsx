/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BaseView from '../view/BaseView';

/** An interface for a ViewModel */
export default interface BaseViewModel {
  /** Called by a component to give the ViewModel a reference to this component. Will be called when this component mounts.*/
  attachView(baseView: BaseView): void;
  /** Called by a component to detach a refence to this component from the ViewModel. Will be called when this component unmounts.*/
  detachView(): void;
}
