/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BaseViewModel from '../BaseViewModel';
import { ErrorMessage } from './ErrorsViewModelImpl';

/** This is an interface of ErrorsViewModel. */
export default interface ErrorsViewModel extends BaseViewModel {
  errorMessage: ErrorMessage;
  errorBool: boolean;

  displayError(error: ErrorMessage): void;
  closeError(): void;
}
