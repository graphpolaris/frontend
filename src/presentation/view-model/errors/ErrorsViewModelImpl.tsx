/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import ErrorsViewModel from './ErrorsViewModel';
import Broker from '../../../domain/entity/broker/broker';

/** Message format of the error message from the backend */
export type ErrorMessage = {
  errorStatus: any;
  queryID: any;
};

/** This class is responsible for updating and creating error messages. */
export default class ErrorsViewModelImpl
  extends AbstractBaseViewModelImpl
  implements ErrorsViewModel
{
  public errorMessage: ErrorMessage;
  public errorBool: boolean;

  constructor(listenFor: string[]) {
    super();

    // start listening for each routing key listed
    listenFor.forEach((routingKey: string) => {
      Broker.instance().subscribe(this, routingKey);
    });

    this.errorMessage = { errorStatus: '', queryID: '' };
    this.errorBool = false;
  }

  /**
   * Receives and handles messages that are being received from the backend, if this class is subscribed to the right routing key.
   * @param message Sent message with an unknown data format. A valid error message should have a status property.
   * @param routingKey The routing key of the message. This determines from which microservice the error message came from.
   */
  public consumeMessageFromBackend(message: any, routingKey: string): void {
    let error: ErrorMessage = { errorStatus: message.status, queryID: message.queryID };
    this.displayError(error);
  }

  /**
   * Displays error messages that are received from the backend.
   * @param error Type of error that we are dealing with.
   */
  public displayError(error: ErrorMessage): void {
    // use a switch, in case certain errors will have side effects in the future
    switch (error.errorStatus) {
      case 'Bad request':
      case 'Bad credentials':
      case 'Translation error':
      case 'Database error':
      case 'ML bad request':
        this.errorMessage = error;
        this.errorBool = true;
        this.notifyViewAboutChanges();
        break;
    }
  }

  public closeError(): void {
    this.errorBool = false;
    this.notifyViewAboutChanges();
  }
}
