/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling/interfaces files.
 * See testing plan for more details.*/
import { OnLoadParams } from 'react-flow-renderer';
import BrokerListener from '../../../domain/entity/broker/BrokerListenerInterface';
import {
  AttributeAnalyticsPopupMenuNode,
  NodeQualityPopupNode,
  SchemaElements,
} from '../../../domain/entity/graph-schema/structures/Types';
import BaseViewModel from '../BaseViewModel';

/** This is an interface of SchemaViewModel. */
export default interface SchemaViewModel extends BaseViewModel, BrokerListener {
  zoom: number;
  visible: boolean;
  elements: SchemaElements;
  nodeTypes: {};
  edgeTypes: {};
  myRef: React.RefObject<HTMLDivElement>;

  nodeQualityPopup: NodeQualityPopupNode;
  attributeAnalyticsPopupMenu: AttributeAnalyticsPopupMenuNode;

  // Export function for the schema visualisation
  exportToPDF(): void;
  exportToPNG(): void;
  subscribeToSchemaResult(): void;
  unSubscribeFromSchemaResult(): void;
  subscribeToAnalyticsData(): void;
  unSubscribeFromAnalyticsData(): void;
  onLoad(params: OnLoadParams<any>): void;
}
