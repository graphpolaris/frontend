/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import SchemaViewModel from './SchemaViewModel';
import EntityNode from '../../view/graph-schema/flow-components/nodes/EntityNode';
import RelationNode from '../../view/graph-schema/flow-components/nodes/RelationNode';
import NodeEdge from '../../view/graph-schema/flow-components/edges/NodeEdge';
import { Node, OnLoadParams } from 'react-flow-renderer';
import {
  doBoxesOverlap,
  makeBoundingBox,
  calcWidthRelationNodeBox,
  calcWidthEntityNodeBox,
  numberPredicates,
} from '../../util/graph-schema/utils';
import {
  AttributeAnalyticsData,
  AttributeAnalyticsPopupMenuNode,
  AttributeCategory,
  AttributeWithData,
  BoundingBox,
  NodeQualityDataForEntities,
  NodeQualityDataForRelations,
  NodeQualityPopupNode,
  NodeType,
  SchemaElements,
} from '../../../domain/entity/graph-schema/structures/Types';
import NodeUseCase from '../../../domain/usecases/graph-schema/NodeUseCase';
import EdgeUseCase from '../../../domain/usecases/graph-schema/EdgeUseCase';
import DrawOrderUseCase from '../../../domain/usecases/graph-schema/DrawOrderUseCase';
import Broker from '../../../domain/entity/broker/broker';
import GraphUseCase from '../../../domain/usecases/graph-schema/GraphUseCase';
import SelfEdge from '../../view/graph-schema/flow-components/edges/SelfEdge';
import React from 'react';
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import { SchemaThemeHolder } from '../../../domain/entity/css-themes/themeHolder';
import { isSchemaResult } from '../../../domain/entity/graph-schema/structures/SchemaResultType';
import {
  isAttributeDataEntity,
  isAttributeDataRelation,
} from '../../../domain/entity/graph-schema/structures/AttributeDataType';

import NodeQualityEntityPopupNode from '../../view/graph-schema/flow-components/nodes/popupmenus/NodeQualityEntityPopupNode';
import NodeQualityRelationPopupNode from '../../view/graph-schema/flow-components/nodes/popupmenus/NodeQualityRelationPopupNode';
import '../../view/graph-schema/flow-components/nodes/popupmenus/NodeQualityPopupNode.scss';

import AttributeAnalyticsPopupMenu from '../../view/graph-schema/flow-components/nodes/popupmenus/AttributeAnalyticsPopupMenuNode';
import '../../view/graph-schema/flow-components/nodes/popupmenus/AttributeAnalyticsPopupMenuNode.scss';

import { exportComponentAsPNG, Params, PDFOptions } from 'react-component-export-image';
import {
  Attribute,
  AttributeData,
  EdgeAttributeData,
  NodeAttributeData,
  Schema,
} from '../../../domain/entity/graph-schema/structures/InputDataTypes';
import { getTextOfJSDocComment } from 'typescript';
import NodesHolder from '../../../domain/entity/query-builder/models/NodesHolder';

/** This class is responsible for updating and creating the graph schema. */
export default class SchemaViewModelImpl
  extends AbstractBaseViewModelImpl
  implements SchemaViewModel
{
  private reactFlowInstance: any;
  private relationCounter: number;
  private drawOrderUseCase: DrawOrderUseCase;
  private nodeUseCase: NodeUseCase;
  private edgeUseCase: EdgeUseCase;
  private graphUseCase: GraphUseCase;
  private placeInQueryBuilder: (name: string, type: string) => void;
  public elements: SchemaElements = { nodes: [], edges: [], selfEdges: [] };
  public zoom: number;
  public visible: boolean;

  public nodeQualityPopup: NodeQualityPopupNode;
  public attributeAnalyticsPopupMenu: AttributeAnalyticsPopupMenuNode;

  public nodeQualityData: Record<string, NodeQualityDataForEntities | NodeQualityDataForRelations>;
  public attributeAnalyticsData: Record<string, AttributeAnalyticsData>;

  private entityPopupOffsets = {
    nodeQualityOffset: {
      x: SchemaThemeHolder.entity.width,
      y: SchemaThemeHolder.entity.height - 3,
    },
    attributeQualityOffset: {
      x: SchemaThemeHolder.entity.width,
      y: -SchemaThemeHolder.entity.height + 12,
    },
  };

  private relationPopupOffsets = {
    nodeQualityOffset: {
      x: SchemaThemeHolder.relation.width + 50,
      y: SchemaThemeHolder.relation.height + 2,
    },
    attributeQualityOffset: {
      x: SchemaThemeHolder.relation.width + 50,
      y: -SchemaThemeHolder.relation.height + 17,
    },
  };
  // React flow reference for positioning on drop.
  public myRef: React.RefObject<HTMLDivElement>;

  public nodeTypes = {
    entity: EntityNode,
    relation: RelationNode,
    nodeQualityEntityPopup: NodeQualityEntityPopupNode,
    nodeQualityRelationPopup: NodeQualityRelationPopupNode,
    attributeAnalyticsPopupMenu: AttributeAnalyticsPopupMenu,
  };

  public edgeTypes = {
    nodeEdge: NodeEdge,
    selfEdge: SelfEdge,
  };

  public constructor(
    drawOrderUseCase: DrawOrderUseCase,
    nodeUseCase: NodeUseCase,
    edgeUseCase: EdgeUseCase,
    graphUseCase: GraphUseCase,
    addAttribute: (name: string, type: string) => void,
  ) {
    super();

    this.myRef = React.createRef();
    // TODO: These values need to not be hardcoded.
    this.zoom = 1.3;
    this.relationCounter = 0;
    this.reactFlowInstance = 0;
    this.visible = true;
    this.edgeUseCase = edgeUseCase;
    this.nodeUseCase = nodeUseCase;
    this.drawOrderUseCase = drawOrderUseCase;
    this.graphUseCase = graphUseCase;
    this.placeInQueryBuilder = addAttribute;

    this.nodeQualityPopup = this.emptyNodeQualityPopupNode();
    this.attributeAnalyticsPopupMenu = this.emptyAttributeAnalyticsPopupMenuNode();

    this.nodeQualityData = {};
    this.attributeAnalyticsData = {};
  }

  /**
   * Containts all function calls to create the graph schema.
   * Notifies the view about the changes at the end.
   */
  public createSchema = (elements: SchemaElements): SchemaElements => {
    let drawOrder: Node[];

    drawOrder = this.drawOrderUseCase.createDrawOrder(elements);
    // Create nodes with start position.
    elements.nodes = this.nodeUseCase.setEntityNodePosition(
      drawOrder,
      {
        x: 0,
        y: 0,
      },
      this.toggleNodeQualityPopup,
      this.toggleAttributeAnalyticsPopupMenu,
    );

    // Create the relation-nodes.
    elements.edges.forEach((relation) => {
      this.createRelationNode(
        relation.id,
        relation.data.attributes,
        relation.data.collection,
        elements,
      );
    });

    elements.selfEdges.forEach((relation) => {
      this.createRelationNode(
        relation.id,
        relation.data.attributes,
        relation.data.collection,
        elements,
      );
    });

    // Complement the relation-nodes with extra data that is now accessible.
    elements.edges = this.edgeUseCase.positionEdges(
      drawOrder,
      elements,
      this.setRelationNodePosition,
    );

    this.visible = false;
    this.notifyViewAboutChanges();
    return elements;
  };

  /**
   * consumes the schema send from the backend and uses it to create the SchemaElements for the schema
   * @param jsonObject
   */
  public consumeMessageFromBackend(jsonObject: unknown): void {
    if (isSchemaResult(jsonObject)) {
      // This is always the first message to receive, so reset the global variables.
      this.visible = false;
      this.createSchema({ nodes: [], edges: [], selfEdges: [] });

      this.relationCounter = 0;

      this.nodeQualityPopup.isHidden = true;
      this.attributeAnalyticsPopupMenu.isHidden = true;

      /* Create the graph-schema and add the popup-menu's for as far as possible. 
      * Runs underlying useCase trice to fix a bug with lingering selfEdges. 
      TODO: clean this up.*/
      this.elements = this.createSchema({ nodes: [], edges: [], selfEdges: [] });
      let schemaElements = this.graphUseCase.createGraphFromInputData(jsonObject);
      this.elements = this.createSchema(schemaElements);
      this.notifyViewAboutChanges();
      //End weird fix

      this.visible = true;
      schemaElements = this.graphUseCase.createGraphFromInputData(jsonObject);
      this.elements = this.createSchema(schemaElements);
      this.notifyViewAboutChanges();

      this.addAttributesToAnalyticsPopupMenus(jsonObject);
    } else if (isAttributeDataEntity(jsonObject)) {
      // Add all information from the received message to the popup-menu's.
      this.addAttributeDataToPopupMenusAndElements(jsonObject, 'gsa_node_result', this.elements);
    } else if (isAttributeDataRelation(jsonObject)) {
      // Add all information from the received message to the popup-menu's.
      this.addAttributeDataToPopupMenusAndElements(jsonObject, 'gsa_edge_result', this.elements);
    } else {
      // TODO: This should be an error screen eventually.
      console.log('This is no valid input!');
    }
    this.notifyViewAboutChanges();
  }

  /**
   * Create a reference to the reactflow schema component on load
   * @param reactFlowInstance reactflow instance
   */
  public onLoad = (reactFlowInstance: OnLoadParams<any>): void => {
    this.reactFlowInstance = reactFlowInstance;
  };

  /**
   * Complements the relation-nodes with data that had default values before.
   * It determines the position of the relation-node and the connected entity-nodes.
   * @param centerX Used to determine the center of the edge.
   * @param centerY Used to determine the center of the edge.
   * @param id The id of the relation node.
   * @param from The id of the entity where the edge should connect from.
   * @param to The id of the entity where the edge should connect to.
   * @param attributes The attributes of the relation node.
   */
  public setRelationNodePosition = (
    centerX: number,
    centerY: number,
    id: string,
    from: string,
    to: string,
    attributes: Attribute[],
  ): void => {
    let width: number;
    let overlap: boolean;
    let y: number;

    // Check if the relation-node is in the list of nodes.
    let relation = this.elements.nodes.find((node) => node.id == id);
    if (relation == undefined) throw new Error('Relation ' + id + ' does not exist.');

    // Height of relation/entity + external buttons.
    const height = 20;

    width = SchemaThemeHolder.relation.width + calcWidthRelationNodeBox(attributes.length, 0);
    let x = centerX - SchemaThemeHolder.relation.width / 2;
    y = centerY - height / 2;

    while (this.CheckForOverlap(x, y, width, height)) {
      y = y + 1;
    }

    // Replace the default values for the correct values.
    relation.position = { x: x, y: y };
    relation.data.from = from;
    relation.data.to = to;

    this.relationCounter++;
    if (this.relationCounter == this.elements.edges.length) {
      this.fitToView();
    }
    this.notifyViewAboutChanges();
  };

  /**
   * Creates a new relation-node with some default values.
   * @param id The id of the relation node.
   * @param attributes The list of attributes that this relation-node has.
   * @param collection The collection this relation-node is in.
   */
  public createRelationNode = (
    id: string,
    attributes: Attribute[],
    collection: string,
    schemaElements: SchemaElements,
  ): void => {
    // Height of relation/entity + external buttons.
    const height = 20;
    const width = SchemaThemeHolder.relation.width + calcWidthRelationNodeBox(attributes.length, 0);

    schemaElements.nodes.push({
      type: NodeType.relation,
      id: id,
      position: { x: 0, y: 0 },
      data: {
        width: width,
        height: height,
        collection: collection,
        attributes: attributes,
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
        toggleNodeQualityPopup: this.toggleNodeQualityPopup,
        toggleAttributeAnalyticsPopupMenu: this.toggleAttributeAnalyticsPopupMenu,
      },
    });
  };

  /**
   * Calculates the width and height of the graph-schema-panel.
   * @returns { number; number } The width and the height of the graph-schema-panel.
   */
  public getWidthHeight = (): { width: number; height: number } => {
    const reactFlow = this.myRef.current as HTMLDivElement;
    const reactFlowBounds = reactFlow.getBoundingClientRect();
    const width = reactFlowBounds.right - reactFlowBounds.left;
    const height = reactFlowBounds.bottom - reactFlowBounds.top;
    return { width, height };
  };

  /** Placeholder function for fitting the schema into the view. */
  public fitToView = (): void => {
    let minX = Infinity;
    let maxX = 0;
    let minY = Infinity;
    let maxY = 0;
    let xZoom = 0;
    let yZoom = 0;

    let setY = 0;
    let setX = 0;
    let setZoom = 0;

    this.elements.nodes.forEach((node) => {
      const attributeCount: number = node.data.attributes.length;
      const nodeCount: number = node.data.nodeCount;
      const nodeWidth: number =
        node.type == NodeType.entity
          ? SchemaThemeHolder.entity.width + calcWidthEntityNodeBox(attributeCount, nodeCount)
          : SchemaThemeHolder.relation.width + calcWidthRelationNodeBox(attributeCount, nodeCount);

      if (node.position.x < minX) minX = node.position.x;
      if (node.position.x + nodeWidth > maxX) maxX = node.position.x + nodeWidth;
      if (node.position.y < minY) minY = node.position.y;
      if (node.position.y + node.data.height > maxY) maxY = node.position.y + node.data.height;
    });

    minX -= 10;
    maxX += 90;

    const { width, height } = this.getWidthHeight();

    // Correct for X and Y position with width and height.
    let nodeWidth = Math.abs(maxX - minX);
    let nodeHeight = Math.abs(maxY - minY);

    setX = minX * -1;
    xZoom = width / nodeWidth;
    setY = minY * -1;
    yZoom = height / nodeHeight;

    // TODO: Correct position and zoom for selfEdges.

    if (xZoom >= yZoom) {
      setZoom = yZoom;
      setX = setX + width / 2 - nodeWidth / 2;
    } else {
      setZoom = xZoom;
      setY = setY + height / 2 - nodeHeight / 2;
    }

    try {
      this.reactFlowInstance.setTransform({ x: setX, y: setY, zoom: setZoom });
    } catch {
      console.log('this.reactFlowInstance is undefined!');
    }
  };

  /**
   * this function check for a relation node if it overlaps with any of the other nodes in the schema.
   * It creates boundingbox for the node and checks with all the other nodes if the boxes overlap.
   * @param x Top left x of the node.
   * @param y Top left y of the node.
   * @param width Width of the node.
   * @param height Height of the node.
   * @returns {bool} whether it overlaps.*/
  public CheckForOverlap = (x: number, y: number, width: number, height: number): boolean => {
    const boundingBox = makeBoundingBox(x, y, width, height);
    let elements = this.elements;

    let boundingTemporary: BoundingBox;
    for (let i = 0; i < elements.nodes.length; i++) {
      boundingTemporary = makeBoundingBox(
        elements.nodes[i].position.x,
        elements.nodes[i].position.y,
        elements.nodes[i].data.width,
        elements.nodes[i].data.height,
      );
      if (doBoxesOverlap(boundingBox, boundingTemporary)) {
        return true;
      }
    }
    return false;
  };

  /** Exports the schema builder to a beautiful png file */
  public exportToPNG(): void {
    const { width, height } = this.getWidthHeight();
    exportComponentAsPNG(this.myRef, {
      fileName: 'schemaBuilder',
      pdfOptions: { x: 0, y: 0, w: width, h: height, unit: 'px' } as Partial<PDFOptions>,
    } as Params);
  }

  /** Not implemented method for exporting the schema builder visualisation to PDF. */
  public exportToPDF(): void {
    console.log('Method not implemented.');
  }

  /** Attach the listener to the broker. */
  public subscribeToSchemaResult(): void {
    Broker.instance().subscribe(this, 'schema_result');
  }

  /** Detach the listener to the broker. */
  public unSubscribeFromSchemaResult(): void {
    Broker.instance().unSubscribe(this, 'schema_result');
  }

  /** Attach the listeners to the broker. */
  public subscribeToAnalyticsData(): void {
    Broker.instance().subscribe(this, 'gsa_node_result');
    Broker.instance().subscribe(this, 'gsa_edge_result');
  }

  /** Detach the listeners to the broker. */
  public unSubscribeFromAnalyticsData(): void {
    Broker.instance().unSubscribe(this, 'gsa_node_result');
    Broker.instance().unSubscribe(this, 'gsa_edge_result');
  }

  /**
   * This function is used by relation and entity nodes to hide or show the node-quality popup of that node.
   * @param id of the node for which the new popup is.
   */
  public toggleNodeQualityPopup = (id: string): void => {
    const popup = this.nodeQualityPopup;

    // Hide the popup if the current popup is visible and if the popup belongs to the same node as the given id.
    if (popup.nodeID == id && !popup.isHidden) popup.isHidden = true;
    // Else make and show a new popup for the node with the given id.
    else this.updateNodeQualityPopup(id, this.elements);

    this.notifyViewAboutChanges();
  };

  /**
   * This function shows and updates the node-quality popup for the node which has the given id.
   * @param id of the node for which the new popup is.
   */
  private updateNodeQualityPopup(id: string, schemaElements: SchemaElements) {
    let node = schemaElements.nodes.find((node) => node.id == id);
    if (node == undefined) {
      throw new Error('Node does not exist therefore no popup can be shown.');
    }

    const popup = this.nodeQualityPopup;
    popup.nodeID = id;
    popup.isHidden = false;
    popup.data = this.nodeQualityData[id];

    if (node.type == 'entity') {
      // Make changes to the popup, to make it a popup for entities.
      this.updateToNodeQualityEntityPopup(node);
    } else {
      // Make changes to the popup, to make it a popup for relations.
      this.updateToNodeQualityRelationPopup(node);
    }

    // Hide the attributeAnalyticsPopupMenu so that only one popup is displayed.
    this.attributeAnalyticsPopupMenu.isHidden = true;

    this.notifyViewAboutChanges();
    this.relationCounter++;
    if (this.relationCounter == schemaElements.edges.length) {
      this.fitToView();
    }
  }

  /**
   * This displays the new node-quality popup for the given entity.
   * @param node This is the entity of which you want to display the popup.
   */
  private updateToNodeQualityEntityPopup(node: Node) {
    const popup = this.nodeQualityPopup;
    const offset = this.entityPopupOffsets.nodeQualityOffset;

    popup.position = {
      x: node.position.x + offset.x,
      y: node.position.y + offset.y,
    };

    popup.type = 'nodeQualityEntityPopup';
  }

  /**
   * This displays the new node-quality popup for the given relation.
   * @param node This is the relation of which you want to display the popup.
   */
  private updateToNodeQualityRelationPopup(node: Node) {
    const popup = this.nodeQualityPopup;
    const offset = this.relationPopupOffsets.nodeQualityOffset;

    popup.position = {
      x: node.position.x + offset.x,
      y: node.position.y + offset.y,
    };

    popup.type = 'nodeQualityRelationPopup';
  }

  /**
   * This function is used by relation and entity nodes to hide or show the attribute analyics popup menu of that node.
   * @param id of the node for which the popup is.
   */
  public toggleAttributeAnalyticsPopupMenu = (id: string): void => {
    const popupMenu = this.attributeAnalyticsPopupMenu;

    // Hide the popup menu if the current popup menu is visible and if the popup menu belongs to the same node as the given id.
    if (popupMenu.nodeID == id && !popupMenu.isHidden) popupMenu.isHidden = true;
    // Else make and show a new popup menu for the node with the given id.
    else this.updateAttributeAnalyticsPopupMenu(id, this.elements);

    this.notifyViewAboutChanges();
  };

  /**
   * This displays the attribute-analytics popup menu for the given node (entity or relation).
   * It removes the other menus from the screen.
   * @param id This is the id of the node (entity or relation) of which you want to display the menu.
   */
  public updateAttributeAnalyticsPopupMenu = (id: string, schemaElements: SchemaElements): void => {
    const node = schemaElements.nodes.find((node) => node.id == id);

    if (node == undefined)
      throw new Error('Node ' + id + ' does not exist therefore no popup menu can be shown.');

    const popupMenu = this.attributeAnalyticsPopupMenu;
    // Make new popup menu for the node.
    popupMenu.nodeID = id;
    popupMenu.isHidden = false;
    popupMenu.data = { ...this.attributeAnalyticsData[id] };

    if (node.type == NodeType.entity) {
      const offset = this.entityPopupOffsets.attributeQualityOffset;
      popupMenu.position = {
        x: node.position.x + offset.x,
        y: node.position.y + offset.y,
      };
    } else {
      const offset = this.relationPopupOffsets.attributeQualityOffset;
      popupMenu.position = {
        x: node.position.x + offset.x,
        y: node.position.y + offset.y,
      };
    }

    // Hide the nodeQualityPopup so that only one popup is displayed.
    this.nodeQualityPopup.isHidden = true;

    this.notifyViewAboutChanges();
  };

  /** This removes the node quality popup from the screen. */
  public hideNodeQualityPopup = (): void => {
    this.nodeQualityPopup.isHidden = true;
    this.notifyViewAboutChanges();
  };

  /** This removes the attribute-analytics popup menu from the screen. */
  public hideAttributeAnalyticsPopupMenu = (): void => {
    this.attributeAnalyticsPopupMenu.isHidden = true;
    this.notifyViewAboutChanges();
  };

  /**
   * This sets all the data for the attributesPopupMenu without the attribute data.
   * @param schemaResult This is the schema result that you get (so no attribute data yet).
   */
  addAttributesToAnalyticsPopupMenus = (schemaResult: Schema): void => {
    this.nodeQualityData = {};
    this.attributeAnalyticsData = {};

    // Firstly, loop over all entities and add the quality-data (as far as possible).
    // Then add the attribute-data (as far as possible).
    schemaResult.nodes.forEach((node) => {
      this.nodeQualityData[node.name] = {
        nodeCount: 0,
        notConnectedNodeCount: 0,
        attributeNullCount: 0,
        isAttributeDataIn: false,
        onClickCloseButton: this.hideNodeQualityPopup,
      };
      let attributes: any = [];
      node.attributes.forEach((attribute) => {
        attributes.push({
          attribute: attribute,
          category: AttributeCategory.undefined,
          nullAmount: 0,
        });
      });
      this.attributeAnalyticsData[node.name] = {
        nodeID: node.name,
        nodeType: NodeType.entity,
        attributes: attributes,
        isAttributeDataIn: false,
        onClickCloseButton: this.hideAttributeAnalyticsPopupMenu,
        onClickPlaceInQueryBuilderButton: this.placeInQueryBuilder,
        searchForAttributes: this.searchForAttributes,
        resetAttributeFilters: this.resetAttributeFilters,
        applyAttributeFilters: this.applyAttributeFilters,
      };
    });
    // Secondly, loop over all relations and add the quality-data (as far as possible).
    // Then add the attribute-data (as far as possible).
    schemaResult.edges.forEach((edge) => {
      this.nodeQualityData[edge.collection] = {
        nodeCount: 0,
        fromRatio: 0,
        toRatio: 0,
        attributeNullCount: 0,
        notConnectedNodeCount: 0,
        isAttributeDataIn: false,
        onClickCloseButton: this.hideNodeQualityPopup,
      };
      let attributes: any = [];
      edge.attributes.forEach((attribute) => {
        attributes.push({
          attribute: attribute,
          category: AttributeCategory.undefined,
          nullAmount: 0,
        });
      });
      this.attributeAnalyticsData[edge.collection] = {
        nodeID: edge.collection,
        nodeType: NodeType.relation,
        attributes: attributes,
        isAttributeDataIn: false,
        onClickCloseButton: this.hideAttributeAnalyticsPopupMenu,
        onClickPlaceInQueryBuilderButton: this.placeInQueryBuilder,
        searchForAttributes: this.searchForAttributes,
        resetAttributeFilters: this.resetAttributeFilters,
        applyAttributeFilters: this.applyAttributeFilters,
      };
    });
  };

  /** Returns an empty quality popup node for react-flow. */
  public emptyAttributeAnalyticsPopupMenuNode(): AttributeAnalyticsPopupMenuNode {
    return {
      id: 'attributeAnalyticsPopupMenu',
      nodeID: '',
      position: { x: 0, y: 0 },
      data: {
        nodeID: '',
        nodeType: NodeType.relation,
        attributes: [],
        isAttributeDataIn: false,
        onClickCloseButton: this.hideAttributeAnalyticsPopupMenu,
        onClickPlaceInQueryBuilderButton: this.placeInQueryBuilder,
        searchForAttributes: this.searchForAttributes,
        resetAttributeFilters: this.resetAttributeFilters,
        applyAttributeFilters: this.applyAttributeFilters,
      },
      type: 'attributeAnalyticsPopupMenu',
      isHidden: true,
      className: 'attributeAnalyticsPopupMenu',
    };
  }
  /** Returns an empty quality popup node for react-flow. */
  public emptyNodeQualityPopupNode(): NodeQualityPopupNode {
    return {
      id: 'nodeQualityPopup',
      position: { x: 0, y: 0 },
      data: {
        nodeCount: 0,
        notConnectedNodeCount: 0,
        attributeNullCount: 0,
        isAttributeDataIn: false,
        onClickCloseButton: this.hideNodeQualityPopup,
      },
      type: 'nodeQualityEntityPopup',
      isHidden: true,
      nodeID: '',
      className: 'nodeQualityPopup',
    };
  }

  /**
   * This function adjusts the values from the new attribute data into
   * the attribute-analytics data and the node-quality data.
   * @param attributeData The data that comes from the backend with (some) data about the attributes.
   */
  public addAttributeDataToPopupMenusAndElements = (
    attributeData: AttributeData,
    attributeDataType: string,
    schemaElements: SchemaElements,
  ): void => {
    // Check if attributeData is a node/entity.
    if (attributeDataType === 'gsa_node_result') {
      const entity = attributeData as NodeAttributeData;
      // If it is a entity then add the data for the corresponding entity.
      if (entity.id in this.attributeAnalyticsData) {
        const attributeDataOfEntity = this.attributeAnalyticsData[entity.id];
        attributeDataOfEntity.isAttributeDataIn = true;

        entity.attributes.forEach((attribute) => {
          // Check if attribute is in the list with attributes from the correct entity.
          const attributeFound = attributeDataOfEntity.attributes.find(
            (attribute_) => attribute_.attribute.name == attribute.name,
          );
          if (attributeFound !== undefined) {
            attributeFound.category = attribute.type;
            attributeFound.nullAmount = attribute.nullAmount;
          }
        });
      } // Not throw new error, because it should not crash, a message is enough and not all data will be shown.
      else console.log('entity ' + entity.id + ' is not in attributeAnalyticsData');

      if (entity.id in this.nodeQualityData) {
        const qualityDataOfEntity = this.nodeQualityData[entity.id] as NodeQualityDataForEntities;
        qualityDataOfEntity.nodeCount = entity.length;
        qualityDataOfEntity.attributeNullCount = entity.summedNullAmount;
        qualityDataOfEntity.notConnectedNodeCount = Number((1 - entity.connectedRatio).toFixed(2));
        qualityDataOfEntity.isAttributeDataIn = true;
      } // Not throw new error, because it should not crash, a message is enough and not all data will be shown.
      else console.log('entity ' + entity.id + ' is not in nodeQualityData');

      // Check also if the entity exists in the this.elements-list.
      // If so, add the new data to it.
      const elementsNode = schemaElements.nodes.find((node_) => node_.id == entity.id);
      if (elementsNode !== undefined) {
        elementsNode.data.nodeCount = entity.length;
        elementsNode.data.summedNullAmount = entity.summedNullAmount;
        elementsNode.data.connectedRatio = entity.connectedRatio;
      }
    }

    // Check if attributeData is an edge/relation.
    else if (attributeDataType === 'gsa_edge_result') {
      const relation = attributeData as EdgeAttributeData;
      // If it is a relation then add the data for the corresponding relation.
      if (relation.id in this.attributeAnalyticsData) {
        const attributeDataOfRelation = this.attributeAnalyticsData[relation.id];
        attributeDataOfRelation.isAttributeDataIn = true;

        relation.attributes.forEach((attribute) => {
          // Check if attribute is in the list with attributes from the correct relation.
          const attributeFound = attributeDataOfRelation.attributes.find(
            (attribute_) => attribute_.attribute.name == attribute.name,
          );
          if (attributeFound !== undefined) {
            attributeFound.category = attribute.type;
            attributeFound.nullAmount = attribute.nullAmount;
          }
        });
      } // Not throw new error, because it should not crash, a message is enough and not all data will be shown.
      else console.log('relation ' + relation.id + ' is not in attributeAnalyticsData');

      if (relation.id in this.nodeQualityData) {
        const qualityDataOfRelation = this.nodeQualityData[
          relation.id
        ] as NodeQualityDataForRelations;
        qualityDataOfRelation.nodeCount = relation.length;
        qualityDataOfRelation.attributeNullCount = relation.summedNullAmount;
        qualityDataOfRelation.fromRatio = Number(relation.fromRatio.toFixed(2));
        qualityDataOfRelation.toRatio = Number(relation.toRatio.toFixed(2));
        qualityDataOfRelation.isAttributeDataIn = true;
      } // Not throw new error, because it should not crash, a message is enough and not all data will be shown.
      else console.log('relation ' + relation.id + ' is not in nodeQualityData');

      // Check also if the entity exists in the this.elements-list.
      // If so, add the new data to it.
      const elementsNode = schemaElements.nodes.find((node_) => node_.id == relation.id);
      if (elementsNode !== undefined) {
        elementsNode.data.nodeCount = relation.length;
        elementsNode.data.summedNullAmount = relation.summedNullAmount;
        elementsNode.data.fromRatio = relation.fromRatio;
        elementsNode.data.toRatio = relation.toRatio;
      }
    } else throw new Error('This data is not valid!');
  };

  /**
   * Filter out attributes that do not contain the given searchbar-value.
   * @param id The id of the node the attributes are from.
   * @param searchbarValue The value of the searchbar.
   */
  public searchForAttributes = (id: string, searchbarValue: string): void => {
    const data = this.attributeAnalyticsData[id];
    // Check if there is data available.
    if (data !== undefined) {
      let passedAttributes: AttributeWithData[] = [];
      data.attributes.forEach((attribute) => {
        if (attribute.attribute.name.toLowerCase().includes(searchbarValue.toLowerCase()))
          passedAttributes.push(attribute);
      });
      this.attributeAnalyticsPopupMenu.data.attributes = passedAttributes;
      this.notifyViewAboutChanges();
    }
  };

  /**
   * Reset the current used filters for the attribute-list.
   * @param id The id of the node the attributes are from.
   */
  public resetAttributeFilters = (id: string): void => {
    const data = this.attributeAnalyticsData[id];
    // Check if there is data available.
    if (data !== undefined) {
      this.attributeAnalyticsPopupMenu.data.attributes = data.attributes;
      this.notifyViewAboutChanges();
    }
  };

  /**
   * Applies the chosen filters on the list of attributes of the particular node.
   * @param id The id of the node the attributes are from.
   * @param dataType The given type of the data you want to filter on (numerical, categorical, other).
   * @param predicate The given predicate.
   * @param percentage The given percentage you want to compare the null-values on.
   */
  public applyAttributeFilters = (
    id: string,
    dataType: AttributeCategory,
    predicate: string,
    percentage: number,
  ): void => {
    const data = this.attributeAnalyticsData[id];
    // Check if there is data available.
    if (data !== undefined) {
      let passedAttributes: AttributeWithData[] = [];
      data.attributes.forEach((attribute) => {
        // If the value is undefined it means that this filter is not chosen, so that must not be taken into account for further filtering.
        if (attribute.category == dataType || dataType == AttributeCategory.undefined)
          if (predicate == '' || percentage == -1)
            // If the string is empty it means that this filter is not chosen, so that must not be taken into account for filtering.
            passedAttributes.push(attribute);
          else if (numberPredicates[predicate](attribute.nullAmount, percentage))
            passedAttributes.push(attribute);
      });
      this.attributeAnalyticsPopupMenu.data.attributes = passedAttributes;
      this.notifyViewAboutChanges();
    }
  };
}
