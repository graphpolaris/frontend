/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { schema, schema2 } from '../../../data/mock-data/graph-schema/MockGraph';
import {
  mockAttributeDataNLEdge2,
  mockAttributeDataNLEdge2IncorrectId,
  mockAttributeDataNLNode1,
  mockAttributeDataNLNode2,
  mockAttributeDataNLNode2IncorrectId,
} from '../../../data/mock-data/graph-schema/MockAttributeDataBatchedNL';
import SecondChamberSchemaMock from '../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import GraphUseCase from '../../../domain/usecases/graph-schema/GraphUseCase';
import SchemaViewModelImpl from './SchemaViewModelImpl';
import { Node, Edge, ArrowHeadType } from 'react-flow-renderer';
import DrawOrderUseCase from '../../../domain/usecases/graph-schema/DrawOrderUseCase';
import EdgeUseCase from '../../../domain/usecases/graph-schema/EdgeUseCase';
import NodeUseCase from '../../../domain/usecases/graph-schema/NodeUseCase';
import {
  AttributeCategory,
  BoundingBox,
  NodeQualityDataForEntities,
  NodeQualityDataForRelations,
  NodeType,
} from '../../../domain/entity/graph-schema/structures/Types';
import {
  Attribute,
  AttributeData,
} from '../../../domain/entity/graph-schema/structures/InputDataTypes';
import mockQueryResult from '../../../data/mock-data/query-result/big2ndChamberQueryResult';
import mockSchemaResult from '../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import Broker from '../../../domain/entity/broker/broker';

jest.mock('../../view/graph-schema/SchemaStyleSheet');
jest.mock('../../util/graph-schema/utils.tsx', () => {
  return {
    //TODO Is this already updated?
    getWidthOfText: () => {
      return 10;
    },
    calcWidthRelationNodeBox: () => {
      return 75;
    },
    calcWidthEntityNodeBox: () => {
      return 75;
    },
    makeBoundingBox: (x: number, y: number, width: number, height: number) => {
      let boundingBox: BoundingBox;
      boundingBox = {
        topLeft: { x: x, y: y },
        bottomRight: { x: x + width, y: y + height },
      };
      return boundingBox;
    },
    doBoxesOverlap: (firstBB: BoundingBox, secondBB: BoundingBox) => {
      if (
        firstBB.topLeft.x >= secondBB.bottomRight.x ||
        secondBB.topLeft.x >= firstBB.bottomRight.x
      )
        return false;

      if (
        firstBB.topLeft.y >= secondBB.bottomRight.y ||
        secondBB.topLeft.y >= firstBB.bottomRight.y
      )
        return false;

      return true;
    },
  };
});

describe('schemaViewModelImpl', () => {
  beforeEach(() => jest.resetModules());
  const graphUseCase = new GraphUseCase();
  const drawOrderUseCase = new DrawOrderUseCase();
  const nodeUseCase = new NodeUseCase();
  const edgeUseCase = new EdgeUseCase();
  const attributesInQueryBuilder: any = [];
  const addAttribute = (name: string, type: string) => {
    attributesInQueryBuilder.push({ name: name, type: type });
    return;
  };

  function anonymous(attributes: Attribute[], id: string, hiddenAttributes: boolean): void {
    //Empty methode.
  }

  it('should create a relation', () => {
    const schemaViewModel = new SchemaViewModelImpl(
      drawOrderUseCase,
      nodeUseCase,
      edgeUseCase,
      graphUseCase,
      addAttribute,
    );

    const expectedrelationode = {
      type: 'relation',
      id: '5',
      position: { x: -72.5, y: 20 },
      data: {
        width: 220,
        height: 20,
        collection: 'none',
        attributes: [],
        from: 'from:here',
        to: 'to:here',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    };

    schemaViewModel.createRelationNode('5', [], 'none', schemaViewModel.elements);
    schemaViewModel.setRelationNodePosition(0, 0, '5', 'from:here', 'to:here', []);
    expect(JSON.stringify(schemaViewModel.elements.nodes[0])).toEqual(
      JSON.stringify(expectedrelationode),
    );

    const expectedrelationode2 = {
      type: 'relation',
      id: '6',
      position: { x: -72.5, y: 40 },
      data: {
        width: 220,
        height: 20,
        collection: 'none',
        attributes: [],
        from: 'from:hereagain',
        to: 'to:hereagain',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    };
    schemaViewModel.createRelationNode('6', [], 'none', schemaViewModel.elements);
    schemaViewModel.setRelationNodePosition(0, 0, '6', 'from:hereagain', 'to:hereagain', []);
    expect(JSON.stringify(schemaViewModel.elements.nodes[1])).toEqual(
      JSON.stringify(expectedrelationode2),
    );
  });

  it('should console log that method is not implemented', () => {
    const schemaViewModel = new SchemaViewModelImpl(
      drawOrderUseCase,
      nodeUseCase,
      edgeUseCase,
      graphUseCase,
      addAttribute,
    );
    const consoleSpy = jest.spyOn(console, 'log');
    schemaViewModel.exportToPDF();
    expect(consoleSpy).toHaveBeenCalledWith('Method not implemented.');
  });

  it('fitToView', () => {
    const schemaViewModel = new SchemaViewModelImpl(
      drawOrderUseCase,
      nodeUseCase,
      edgeUseCase,
      graphUseCase,
      addAttribute,
    );
    const getWidthHeight = { width: 300, height: 700 };
    const spy = jest.spyOn(schemaViewModel, 'getWidthHeight');
    spy.mockReturnValue(getWidthHeight);

    schemaViewModel.consumeMessageFromBackend(mockSchemaResult);
    schemaViewModel.fitToView();

    const consoleSpy = jest.spyOn(console, 'log');
    expect(consoleSpy).toHaveBeenCalledWith('this.reactFlowInstance is undefined!');
  });

  /**
   * These are the testcases for consuming messages from the backend
   */
  describe('consumeMessageFromBackend', () => {
    it('should consume schema result only when subscribed to broker for schema_result', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      const mockConsumeMessages = jest.fn();
      const message = 'test schema result';

      schemaViewModel.consumeMessageFromBackend = mockConsumeMessages;

      // should consume message for schema result when subscribed
      schemaViewModel.subscribeToSchemaResult();
      Broker.instance().publish(message, 'schema_result');
      expect(mockConsumeMessages.mock.calls[0][0]).toEqual(message);
      expect(mockConsumeMessages).toBeCalledTimes(1);

      // should not consume message for query_result
      Broker.instance().publish(message, 'query_result');
      expect(mockConsumeMessages).toBeCalledTimes(1);

      // should not consume message for schema result when unsubscribed
      schemaViewModel.unSubscribeFromSchemaResult();
      Broker.instance().publish(message, 'schema_result');
      expect(mockConsumeMessages).toBeCalledTimes(1);
    });

    it('should consume attribute-data only when subscribed to broker for gsa_node_result & gsa_edge_result', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      const mockConsumeMessages = jest.fn();
      const message = 'test analytics data';

      schemaViewModel.consumeMessageFromBackend = mockConsumeMessages;

      // should consume message for schema result when subscribed
      schemaViewModel.subscribeToAnalyticsData();
      Broker.instance().publish(message, 'gsa_node_result');
      Broker.instance().publish(message, 'gsa_edge_result');
      expect(mockConsumeMessages.mock.calls[0][0]).toEqual(message);
      expect(mockConsumeMessages).toBeCalledTimes(2);

      // should not consume message for schema result when unsubscribed
      schemaViewModel.unSubscribeFromAnalyticsData();
      Broker.instance().publish(message, 'schema_result');
      Broker.instance().publish(message, 'gsa_node_result');
      Broker.instance().publish(message, 'gsa_edge_result');
      expect(mockConsumeMessages).toBeCalledTimes(2);
    });

    //TODO: also test the message for the analytics
    it('should console log and should not change any elements when receiving unrelated messages', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      const consoleSpy = jest.spyOn(console, 'log');

      expect(schemaViewModel.visible).toEqual(true);
      schemaViewModel.consumeMessageFromBackend(mockQueryResult);
      expect(consoleSpy).toHaveBeenCalledWith('This is no valid input!');

      expect(schemaViewModel.visible).toEqual(true);
      expect(schemaViewModel.elements.nodes).toEqual([]);
      expect(schemaViewModel.elements.edges).toEqual([]);
    });
  });

  /**
   * These are the testcases for the attribute-analytics popup menu
   */
  describe('AttributeAnalyticsPopupMenu', () => {
    it('should throw error that given id does not exist', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(schema);
      // Nodes that are not in elements cannot have popups
      const name = 'Edje';
      expect(schemaViewModel.elements.nodes.find((node) => node.id == name)).toEqual(undefined);
      expect(() => schemaViewModel.toggleAttributeAnalyticsPopupMenu(name)).toThrowError(
        'Node ' + name + ' does not exist therefore no popup menu can be shown.',
      );
    });

    it('should show the attribute-analytics popup menu', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      const popup = schemaViewModel.attributeAnalyticsPopupMenu;
      schemaViewModel.consumeMessageFromBackend(schema);

      // test for entity node
      const node = schemaViewModel.elements.nodes[0];
      schemaViewModel.toggleAttributeAnalyticsPopupMenu(node.id);
      expect(popup.isHidden).toEqual(false);
      expect(popup.nodeID).toEqual(node.id);

      // test for relation node
      const edge = schemaViewModel.elements.edges[0];
      schemaViewModel.setRelationNodePosition(
        0,
        0,
        edge.id,
        edge.source,
        edge.target,
        edge.data.attributes,
      );
      schemaViewModel.toggleAttributeAnalyticsPopupMenu(edge.id);
      expect(popup.isHidden).toEqual(false);
      expect(popup.nodeID).toEqual(edge.id);
    });

    it('should hide the attribute-analytics popup menu as it was already open', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(schema);
      schemaViewModel.attributeAnalyticsPopupMenu.nodeID = 'Thijs';
      schemaViewModel.attributeAnalyticsPopupMenu.isHidden = false;
      schemaViewModel.toggleAttributeAnalyticsPopupMenu('Thijs');
      expect(schemaViewModel.attributeAnalyticsPopupMenu.isHidden).toEqual(true);
    });

    it('should close the attribute-analytics menu', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(schema);
      schemaViewModel.attributeAnalyticsData['Thijs'].onClickCloseButton();
      expect(schemaViewModel.attributeAnalyticsPopupMenu.isHidden).toEqual(true);
    });

    it('should make an empty attribute-analytics popupmenu', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.attributeAnalyticsPopupMenu =
        schemaViewModel.emptyAttributeAnalyticsPopupMenuNode();
      expect(schemaViewModel.attributeAnalyticsPopupMenu.id).toEqual('attributeAnalyticsPopupMenu');
      expect(schemaViewModel.attributeAnalyticsPopupMenu.nodeID).toEqual('');
      expect(schemaViewModel.attributeAnalyticsPopupMenu.data.nodeType).toEqual(NodeType.relation);
      expect(schemaViewModel.attributeAnalyticsPopupMenu.data.attributes).toEqual([]);
      expect(schemaViewModel.attributeAnalyticsPopupMenu.data.isAttributeDataIn).toEqual(false);
    });

    it('should place an attribute node in the querybuilder', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(schema);
      const node = schemaViewModel.elements.nodes[0];
      const attribute = node.data.attributes;
      schemaViewModel.attributeAnalyticsData[node.id].onClickPlaceInQueryBuilderButton(
        attribute.name,
        attribute.type,
      );
      expect(attributesInQueryBuilder[0]).toEqual({ name: attribute.name, type: attribute.type });
    });

    it('should have a working searchbar', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(mockSchemaResult);
      schemaViewModel.searchForAttributes('kamerleden', 'aa');

      let attributes = schemaViewModel.attributeAnalyticsPopupMenu.data.attributes;
      expect(attributes.length).toEqual(2);

      schemaViewModel.searchForAttributes('kamerleden', '');
      attributes = schemaViewModel.attributeAnalyticsPopupMenu.data.attributes;
      expect(attributes.length).toEqual(6);
    });

    it('should have working filters', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(mockSchemaResult);
      schemaViewModel.consumeMessageFromBackend(mockAttributeDataNLNode1);

      schemaViewModel.applyAttributeFilters(
        'kamerleden',
        AttributeCategory.categorical,
        'Bigger',
        -1,
      );
      let attributes = schemaViewModel.attributeAnalyticsPopupMenu.data.attributes;
      expect(attributes.length).toEqual(1);

      schemaViewModel.resetAttributeFilters('kamerleden');
      attributes = schemaViewModel.attributeAnalyticsPopupMenu.data.attributes;
      expect(attributes.length).toEqual(6);
    });
  });

  /**
   * These are the testcases for the node-quality popup menu
   */
  describe('nodeQualityPopup', () => {
    it('should throw error that given id does not exist', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(schema);
      expect(() => schemaViewModel.toggleNodeQualityPopup('Edje')).toThrowError(
        'Node does not exist therefore no popup can be shown.',
      );
    });

    it('should show the node-quality popup menu for an entity node', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(SecondChamberSchemaMock);
      schemaViewModel.toggleNodeQualityPopup('commissies');
      expect(schemaViewModel.nodeQualityPopup.isHidden).toEqual(false);
      expect(schemaViewModel.nodeQualityPopup.type).toEqual('nodeQualityEntityPopup');
      expect(schemaViewModel.nodeQualityPopup.nodeID).toEqual('commissies');
    });

    it('should show the node-quality popup menu for a relation node', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.createRelationNode('5', [], '5', schemaViewModel.elements);
      schemaViewModel.toggleNodeQualityPopup('5');
      expect(schemaViewModel.nodeQualityPopup.isHidden).toEqual(false);
      expect(schemaViewModel.nodeQualityPopup.type).toEqual('nodeQualityRelationPopup');
      expect(schemaViewModel.nodeQualityPopup.nodeID).toEqual('5');
    });

    it('should hide the node-quality popup menu as it was already open', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(schema);
      schemaViewModel.nodeQualityPopup.nodeID = 'Thijs';
      schemaViewModel.nodeQualityPopup.isHidden = false;
      schemaViewModel.toggleNodeQualityPopup('Thijs');
      expect(schemaViewModel.nodeQualityPopup.isHidden).toEqual(true);
    });

    it('should close the node-quality menu', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(schema);
      schemaViewModel.nodeQualityData['Thijs'].onClickCloseButton();
      expect(schemaViewModel.nodeQualityPopup.isHidden).toEqual(true);
    });

    it('should make an empty node-quality popupmenu', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.nodeQualityPopup = schemaViewModel.emptyNodeQualityPopupNode();
      expect(schemaViewModel.nodeQualityPopup.id).toEqual('nodeQualityPopup');
      expect(schemaViewModel.nodeQualityPopup.nodeID).toEqual('');
      expect(schemaViewModel.nodeQualityPopup.data.nodeCount).toEqual(0);
      expect(schemaViewModel.nodeQualityPopup.data.attributeNullCount).toEqual(0);
      expect(schemaViewModel.nodeQualityPopup.data.isAttributeDataIn).toEqual(false);
    });
  });

  describe('AttributeData', () => {
    it('should process the incoming data correctly for node-data', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(mockSchemaResult);
      schemaViewModel.consumeMessageFromBackend(mockAttributeDataNLNode2);

      const attributeAnalyticsData = schemaViewModel.attributeAnalyticsData['commissies'];
      const nodeQualityData = schemaViewModel.nodeQualityData[
        'commissies'
      ] as NodeQualityDataForEntities;
      expect(attributeAnalyticsData.isAttributeDataIn).toEqual(true);
      expect(attributeAnalyticsData.attributes[0].category).toEqual(AttributeCategory.other);
      expect(attributeAnalyticsData.attributes[0].nullAmount).toEqual(1);
      expect(nodeQualityData.nodeCount).toEqual(38);
      expect(nodeQualityData.attributeNullCount).toEqual(1);
      expect(nodeQualityData.notConnectedNodeCount).toEqual(0.03);
    });

    it('should process the incoming data correctly for edge-data', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(mockSchemaResult);
      schemaViewModel.consumeMessageFromBackend(mockAttributeDataNLEdge2);

      const attributeAnalyticsData = schemaViewModel.attributeAnalyticsData['lid_van'];
      const nodeQualityData = schemaViewModel.nodeQualityData[
        'lid_van'
      ] as NodeQualityDataForRelations;
      expect(attributeAnalyticsData.isAttributeDataIn).toEqual(true);
      expect(attributeAnalyticsData.attributes[0].category).toEqual(AttributeCategory.categorical);
      expect(attributeAnalyticsData.attributes[0].nullAmount).toEqual(0);
      expect(nodeQualityData.nodeCount).toEqual(149);
      expect(nodeQualityData.attributeNullCount).toEqual(0);
      expect(nodeQualityData.fromRatio).toEqual(1);
      expect(nodeQualityData.toRatio).toEqual(1);
    });

    it('should console log when the given data has no corresponding id for entities', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(mockSchemaResult);

      const consoleSpy = jest.spyOn(console, 'log');
      schemaViewModel.consumeMessageFromBackend(mockAttributeDataNLNode2IncorrectId);

      expect(consoleSpy).toBeCalledTimes(2);
    });

    it('should console log when the given data has no corresponding id for relations', () => {
      const schemaViewModel = new SchemaViewModelImpl(
        drawOrderUseCase,
        nodeUseCase,
        edgeUseCase,
        graphUseCase,
        addAttribute,
      );
      schemaViewModel.consumeMessageFromBackend(mockSchemaResult);

      const consoleSpy = jest.spyOn(console, 'log');
      schemaViewModel.consumeMessageFromBackend(mockAttributeDataNLEdge2IncorrectId);

      expect(consoleSpy).toBeCalledTimes(2);
    });
  });

  /** expected results */

  const expectedNodesInElements: Node[] = [
    {
      type: 'entity',
      id: 'Airport',
      position: { x: 0, y: 0 },
      data: {
        attributes: [
          { name: 'city', type: 'string' },
          { name: 'vip', type: 'bool' },
          { name: 'state', type: 'string' },
        ],
        handles: [
          'entityTargetBottom',
          'entityTargetRight',
          'entityTargetRight',
          'entitySourceLeft',
          'entitySourceLeft',
          'entitySourceLeft',
          'entityTargetRight',
        ],
        width: 165,
        height: 20,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 0,
      },
    },
    {
      type: 'entity',
      id: 'Plane',
      position: { x: 0, y: 150 },
      data: {
        attributes: [
          { name: 'type', type: 'string' },
          { name: 'maxFuelCapacity', type: 'int' },
        ],
        handles: ['entitySourceTop', 'entityTargetBottom', 'entityTargetRight'],
        width: 165,
        height: 20,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 0,
      },
    },
    {
      type: 'entity',
      id: 'Staff',
      position: { x: 0, y: 300 },
      data: {
        attributes: [],
        handles: ['entityTargetLeft', 'entitySourceTop', 'entitySourceBottom'],
        width: 165,
        height: 20,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 0,
      },
    },
    {
      type: 'entity',
      id: 'Airport2',
      position: { x: 0, y: 450 },
      data: {
        attributes: [
          { name: 'city', type: 'string' },
          { name: 'vip', type: 'bool' },
          { name: 'state', type: 'string' },
        ],
        handles: ['entitySourceRight', 'entitySourceRight', 'entityTargetTop'],
        width: 165,
        height: 20,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 0,
      },
    },

    {
      type: 'entity',
      id: 'Thijs',
      position: { x: 0, y: 600 },
      data: {
        attributes: [],
        handles: ['entitySourceRight', 'entityTargetLeft'],
        width: 165,
        height: 20,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 0,
      },
    },

    {
      type: 'entity',
      id: 'Unconnected',
      position: { x: 0, y: 750 },
      data: {
        attributes: [],
        handles: [],
        width: 165,
        height: 20,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'arrivalTime',
            type: 'int',
          },
          {
            name: 'departureTime',
            type: 'int',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'salary',
            type: 'int',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'hallo',
            type: 'string',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'hallo',
            type: 'string',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'hallo',
            type: 'string',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'hallo',
            type: 'string',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'hallo',
            type: 'string',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
    {
      type: 'relation',
      id: 'flights',
      position: { x: 0, y: 0 },
      data: {
        width: 220,
        height: 40,
        collection: 'flights',
        attributes: [
          {
            name: 'test',
            type: 'string',
          },
        ],
        from: '',
        to: '',
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
      },
    },
  ];

  const expectedEdgesInElements: Edge[] = [
    {
      id: 'flights',
      source: 'Plane',
      target: 'Airport',
      type: 'nodeEdge',
      label: 'Plane:Airport',
      data: {
        attributes: [],
        d: 0,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceTop',
      targetHandle: 'entityTargetBottom',
    },
    {
      id: 'flights',
      source: 'Airport2',
      target: 'Airport',
      type: 'nodeEdge',
      label: 'Airport2:Airport',
      data: {
        attributes: [
          { name: 'arrivalTime', type: 'int' },
          { name: 'departureTime', type: 'int' },
        ],
        d: 40,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceRight',
      targetHandle: 'entityTargetRight',
    },
    {
      id: 'flights',
      source: 'Thijs',
      target: 'Airport',
      type: 'nodeEdge',
      label: 'Thijs:Airport',
      data: {
        attributes: [{ name: 'hallo', type: 'string' }],
        d: 80,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceRight',
      targetHandle: 'entityTargetRight',
    },
    {
      id: 'flights',
      source: 'Airport',
      target: 'Staff',
      type: 'nodeEdge',
      label: 'Airport:Staff',
      data: {
        attributes: [{ name: 'salary', type: 'int' }],
        d: -40,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceLeft',
      targetHandle: 'entityTargetLeft',
    },
    {
      id: 'flights',
      source: 'Airport',
      target: 'Thijs',
      type: 'nodeEdge',
      label: 'Airport:Thijs',
      data: {
        attributes: [{ name: 'hallo', type: 'string' }],
        d: -80,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceLeft',
      targetHandle: 'entityTargetLeft',
    },
    {
      id: 'flights',
      source: 'Staff',
      target: 'Plane',
      type: 'nodeEdge',
      label: 'Staff:Plane',
      data: {
        attributes: [{ name: 'hallo', type: 'string' }],
        d: 0,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceTop',
      targetHandle: 'entityTargetBottom',
    },
    {
      id: 'flights',
      source: 'Airport2',
      target: 'Plane',
      type: 'nodeEdge',
      label: 'Airport2:Plane',
      data: {
        attributes: [{ name: 'hallo', type: 'string' }],
        d: 120,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceRight',
      targetHandle: 'entityTargetRight',
    },
    {
      id: 'flights',
      source: 'Staff',
      target: 'Airport2',
      type: 'nodeEdge',
      label: 'Staff:Airport2',
      data: {
        attributes: [{ name: 'hallo', type: 'string' }],
        d: 0,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceBottom',
      targetHandle: 'entityTargetTop',
    },
    {
      id: 'flights',
      source: 'Airport',
      target: 'Airport',
      type: 'selfEdge',
      label: 'Airport:Airport',
      data: {
        attributes: [{ name: 'test', type: 'string' }],
        d: 58,
        created: false,
        collection: 'flights',
        edgeCount: 0,
        view: anonymous,
      },
      arrowHeadType: ArrowHeadType.Arrow,
      sourceHandle: 'entitySourceLeft',
      targetHandle: 'entityTargetRight',
    },
  ];

  const expectedAttributes: Node[] = [
    {
      type: 'attribute',
      id: 'Airport:city',
      position: { x: 0, y: 21 },
      data: { name: 'city', datatype: 'string' },
      isHidden: true,
    },
    {
      type: 'attribute',
      id: 'Airport:vip',
      position: { x: 0, y: 41 },
      data: { name: 'vip', datatype: 'bool' },
      isHidden: true,
    },
    {
      type: 'attribute',
      id: 'Airport:state',
      position: { x: 0, y: 61 },
      data: { name: 'state', datatype: 'string' },
      isHidden: true,
    },
    {
      type: 'attribute',
      id: 'Plane:type',
      position: { x: 0, y: 171 },
      data: { name: 'type', datatype: 'string' },
      isHidden: true,
    },
    {
      type: 'attribute',
      id: 'Plane:maxFuelCapacity',
      position: { x: 0, y: 191 },
      data: { name: 'maxFuelCapacity', datatype: 'int' },
      isHidden: true,
    },
    {
      type: 'attribute',
      id: 'Airport2:city',
      position: { x: 0, y: 471 },
      data: { name: 'city', datatype: 'string' },
      isHidden: true,
    },
    {
      type: 'attribute',
      id: 'Airport2:vip',
      position: { x: 0, y: 491 },
      data: { name: 'vip', datatype: 'bool' },
      isHidden: true,
    },
    {
      type: 'attribute',
      id: 'Airport2:state',
      position: { x: 0, y: 511 },
      data: { name: 'state', datatype: 'string' },
      isHidden: true,
    },
  ];
});

/** Result nodes. */
const nodes: Node[] = [
  { type: 'entity', id: 'Thijs', position: { x: 0, y: 0 }, data: { attributes: [] } },
  {
    type: 'entity',
    id: 'Airport',
    position: { x: 0, y: 0 },
    data: { attributes: [] },
  },
  {
    type: 'entity',
    id: 'Airport2',
    position: { x: 0, y: 0 },
    data: { attributes: [] },
  },
  { type: 'entity', id: 'Plane', position: { x: 0, y: 0 }, data: { attributes: [] } },
  { type: 'entity', id: 'Staff', position: { x: 0, y: 0 }, data: { attributes: [] } },
];

/** Result links. */
const edges: Edge[] = [
  {
    id: 'Airport2:Airport',
    label: 'Airport2:Airport',
    type: 'nodeEdge',
    source: 'Airport2',
    target: 'Airport',
    arrowHeadType: ArrowHeadType.Arrow,
    data: {
      d: '',
      attributes: [],
    },
  },
  {
    id: 'Airport:Staff',
    label: 'Airport:Staff',
    type: 'nodeEdge',
    source: 'Airport',
    target: 'Staff',
    arrowHeadType: ArrowHeadType.Arrow,
    data: { d: '', attributes: [] },
  },
  {
    id: 'Plane:Airport',
    label: 'Plane:Airport',
    type: 'nodeEdge',
    source: 'Plane',
    target: 'Airport',
    arrowHeadType: ArrowHeadType.Arrow,
    data: { d: '', attributes: [] },
  },
  {
    id: 'Airport:Thijs',
    label: 'Airport:Thijs',
    type: 'nodeEdge',
    source: 'Airport',
    target: 'Thijs',
    arrowHeadType: ArrowHeadType.Arrow,
    data: { d: '', attributes: [] },
  },
  {
    id: 'Thijs:Airport',
    label: 'Thijs:Airport',
    type: 'nodeEdge',
    source: 'Thijs',
    target: 'Airport',
    arrowHeadType: ArrowHeadType.Arrow,
    data: { d: '', attributes: [] },
  },
  {
    id: 'Staff:Plane',
    label: 'Staff:Plane',
    type: 'nodeEdge',
    source: 'Staff',
    target: 'Plane',
    arrowHeadType: ArrowHeadType.Arrow,
    data: { d: '', attributes: [] },
  },
  {
    id: 'Staff:Airport2',
    label: 'Staff:Airport2',
    type: 'nodeEdge',
    source: 'Staff',
    target: 'Airport2',
    arrowHeadType: ArrowHeadType.Arrow,
    data: { d: '', attributes: [] },
  },
  {
    id: 'Airport2:Plane',
    label: 'Airport2:Plane',
    type: 'nodeEdge',
    source: 'Airport2',
    target: 'Plane',
    arrowHeadType: ArrowHeadType.Arrow,
    data: { d: '', attributes: [] },
  },
];
