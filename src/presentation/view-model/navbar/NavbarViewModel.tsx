/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BaseViewModel from '../BaseViewModel';
import { ClassNameMap } from '@material-ui/styles';

/**
 * NavbarViewModel is an interface used to implement the ViewModel implementation of the NavbarViewModelImplementation.
 * Used to define all public fields used in the NavbarViewModelImplementation.
 *
 * It extends BaseViewModel is an interface of ViewModel holding all public fields for all ViewModels.
 */
export default interface NavbarViewModel extends BaseViewModel {
  styles: ClassNameMap;
  isAuthorized: boolean;
  clientID: string;
  sessionID: string;
  databases: string[];
  currentDatabase: string;

  handleLoginButtonClicked(loginType: string): void;
  updateCurrentDatabase(databaseName: string): void;

  onAddDatabaseFormSubmit(
    username: string,
    password: string,
    hostname: string,
    port: number,
    databaseName: string,
    internalDatabase: string,
    databaseType: string,
  ): void;
}
