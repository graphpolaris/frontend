/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import LoginUseCase from '../../../domain/usecases/authorize/LoginUseCase';
import AuthHolder from '../../../domain/entity/auth/models/AuthHolder';

import NavbarViewModelImpl from '../navbar/NavbarViewModelImpl';
import BackendMessageReceiver from '../../../domain/repository-interfaces/BackendMessageReceiver';
import GraphUseCase from '../../../domain/usecases/graph-schema/GraphUseCase';
import WebSocketHandler from '../../../data/drivers/backend-message-receiver/WebSocketHandler';
import AuthorizeApi from '../../../data/drivers/authorize/AuthorizeApi';
import DatabaseHolder from '../../../domain/entity/navbar/DatabaseHolder';
import RequestSchemaUseCase from '../../../domain/usecases/request-schema/RequestSchemaUseCase';
import BackendMessengerMock from '../../../data/drivers/backend-messenger/BackendMessengerMock';
import DatabaseApi from '../../../data/drivers/database/databaseAPI';

/** Testsuite for authorisation. */
jest.mock('../../view/navbar/NavbarStylesheet');

const domain = 'datastrophe.science.uu.nl';

describe('NavBarViewModelImpl', () => {
  let navbar: NavbarViewModelImpl;
  let authHolder: AuthHolder;
  let loginUseCase: LoginUseCase;
  beforeEach(() => {
    const graphUseCase = new GraphUseCase();
    authHolder = new AuthHolder();
    const backendMessenger = new BackendMessengerMock(domain);
    const backendMessageReceiver: BackendMessageReceiver = new WebSocketHandler(domain);
    const authorizeApi = new AuthorizeApi(backendMessenger);
    const databaseApi = new DatabaseApi(backendMessenger);
    const requestSchemaUseCase = new RequestSchemaUseCase(backendMessenger);
    const databaseHolder = new DatabaseHolder();

    loginUseCase = new LoginUseCase(
      authorizeApi,
      databaseApi,
      authHolder,
      databaseHolder,
      backendMessageReceiver,
      requestSchemaUseCase,
    );
    navbar = new NavbarViewModelImpl(
      loginUseCase,
      requestSchemaUseCase,
      databaseApi,
      authHolder,
      databaseHolder,
    );
  });
  it('onAuthChanged', () => {
    authHolder.onSignedIn('', '');

    const expectedAuth = authHolder.isUserAuthorized();
    const expectedClientID = authHolder.getClientID();
    const expectedSessionID = authHolder.getSessionID();

    navbar.onAuthChanged();

    expect(navbar.isAuthorized).toEqual(expectedAuth);
    expect(navbar.clientID).toEqual(expectedClientID);
    expect(navbar.sessionID).toEqual(expectedSessionID);
    expect(expectedAuth).toEqual(true);
  });

  it('Login button clicked', () => {
    const loginUserMock = jest.fn();
    loginUseCase.loginUser = loginUserMock;

    navbar.handleLoginButtonClicked('testcase');

    expect(loginUserMock).toHaveBeenCalledTimes(1);
  });

  it('Should add a database correctly', () => {
    //ph means Placeholder
    navbar.authHolder.onSignedIn('user', 'user');
    navbar
      .onAddDatabaseFormSubmit(
        'phUsername',
        'phPassword',
        'phHostname',
        123,
        'testdb',
        'phinternalDatabase',
        'phdatabaseType',
      )
      .then(() => {
        let databases = navbar.authHolder.getDatabaseNames();
        expect(databases).toEqual(['testdb']);
      });
  });
});
