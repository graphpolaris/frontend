/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { ClassNameMap } from '@material-ui/styles';
import NavbarViewModel from './NavbarViewModel';
import LoginUseCase, { LoginType } from '../../../domain/usecases/authorize/LoginUseCase';
import AuthHolder from '../../../domain/entity/auth/models/AuthHolder';
import { useStyles } from '../../view/navbar/NavbarStylesheet';
import DatabaseHolder from '../../../domain/entity/navbar/DatabaseHolder';
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import RequestSchemaUseCase from '../../../domain/usecases/request-schema/RequestSchemaUseCase';
import DatabaseListener from '../../../domain/entity/navbar/DatabaseListener';
import DatabaseRepository from '../../../domain/repository-interfaces/DatabaseRepository';

/** NavbarViewModelImpl is the implementation of the interface defined in NavbarViewModel. */
export default class NavbarViewModelImpl
  extends AbstractBaseViewModelImpl
  implements NavbarViewModel, DatabaseListener
{
  public styles: ClassNameMap;

  public isAuthorized: boolean;
  public clientID: string;
  public sessionID: string;

  // The connected database names of the user
  public databases: string[];
  public currentDatabase: string;

  private loginUseCase: LoginUseCase;
  private requestSchemaUseCase: RequestSchemaUseCase;

  public authHolder: AuthHolder;
  public databaseHolder: DatabaseHolder;

  private databaseRepository: DatabaseRepository;

  public constructor(
    loginUseCase: LoginUseCase,
    requestSchemaUseCase: RequestSchemaUseCase,
    databaseRepository: DatabaseRepository,
    authHolder: AuthHolder,
    databaseHolder: DatabaseHolder,
  ) {
    super();
    this.styles = useStyles();

    this.loginUseCase = loginUseCase;
    this.requestSchemaUseCase = requestSchemaUseCase;

    this.databaseRepository = databaseRepository;

    this.authHolder = authHolder;

    this.isAuthorized = this.authHolder.isUserAuthorized();
    this.clientID = this.isAuthorized ? this.authHolder.getClientID() : '';
    this.sessionID = this.isAuthorized ? this.authHolder.getSessionID() : '';

    this.authHolder.addAuthListener(this);

    this.databases = [];
    this.currentDatabase = '';
    this.databaseHolder = databaseHolder;
    this.databaseHolder.addDatabaseListener(this);
  }

  /**
   * Called when the user clicks on a database to switch to.
   * @param databaseName The database name to switch to.
   */
  public updateCurrentDatabase(databaseName: string): void {
    this.databaseHolder.changeDatabase(databaseName);
    // When the database changes, request the new schema
    this.requestSchemaUseCase.requestSchema(databaseName);
  }

  /** Handler for the login button. Requests a new client ID. */
  public handleLoginButtonClicked(loginType: LoginType): void {
    this.loginUseCase.loginUser(loginType);
  }

  /**
   * Called when the user clicks on the 'submit' button of the add database form.
   * @param {string} username The username for login.
   * @param {string} password The password for login.
   * @param {string} hostname The URL of the database.
   * @param {number} port The port of the URL.
   * @param {string} databaseName The name of the database.
   * @param {string} internalDatabase The name of the internal database.
   * @param {string} databaseType The type of the database (e.g. Neo4j or ArangoDB).
   */
  public onAddDatabaseFormSubmit(
    username: string,
    password: string,
    hostname: string,
    port: number,
    databaseName: string,
    internalDatabase: string,
    databaseType: string,
  ): Promise<void | Response> {
    return this.databaseRepository
      .addDatabase({
        databaseName,
        username,
        password,
        port,
        hostname,
        internalDatabaseName: internalDatabase,
        databaseType,
      })
      .then(() =>
        this.databaseRepository.fetchUserDatabases().then((databases) => {
          this.authHolder.setDatabaseNames(databases);
          this.databaseHolder.changeDatabase(databaseName);
          // When the database changes, request the new schema
          console.log('databases ' + databases);
          this.requestSchemaUseCase.requestSchema(databaseName);
        }),
      );
  }

  /** Called when the authorization state changes. */
  public onAuthChanged(): void {
    this.isAuthorized = this.authHolder.isUserAuthorized();
    this.clientID = this.authHolder.getClientID();
    this.sessionID = this.authHolder.getSessionID();
    this.databases = this.authHolder.getDatabaseNames();

    this.notifyViewAboutChanges();
  }

  /** Called by the DatabaseHolder to notify that the current database is changed. */
  public onDatabaseChanged(): void {
    this.currentDatabase = this.databaseHolder.getDatabase();
    this.notifyViewAboutChanges();
  }
}
