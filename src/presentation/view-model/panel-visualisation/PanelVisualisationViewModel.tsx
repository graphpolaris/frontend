/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BaseViewModel from '../BaseViewModel';
import { ClassNameMap } from '@material-ui/styles';
import VisualTypeHolder from '../../../domain/entity/VisualTypeHolders/VisualTypeHolder';
import RawJSONViewModel from '../result-visualisations/raw-json/RawJSONViewModel';
import NodeLinkViewModel from '../result-visualisations/node-link/NodeLinkViewModel';
import SemanticSubstratesViewModel from '../result-visualisations/semantic-substrates/SemanticSubstratesViewModel';
import PaohvisViewModel from '../result-visualisations/paohvis/PaohvisViewModel';
import HivePlotsViewModel from '../result-visualisations/hiveplots/HivePlotsViewModel';

/**
 * VisualisationViewModel is an interface used to implement the ViewModel implementation of the VisualisationViewModelImplementation.
 * Used to define all public fields used in the VisualisationViewModelImplementation.
 * It extends BaseViewModel is an interface of ViewModel holding all public fields for all ViewModels.
 */
export default interface VisualisationViewModel extends BaseViewModel {
  styles: ClassNameMap;
  visualTypeHolder: VisualTypeHolder;
  currentVisual: any;
  rawJSONViewModel: RawJSONViewModel;
  nodeLinkViewModel: NodeLinkViewModel;
  semanticSubstratesViewModel: SemanticSubstratesViewModel;
  paohvisViewModel: PaohvisViewModel;
  hivePlotsViewModel: HivePlotsViewModel;
  resultVisualisationType: string;

  visualChange(visualType: string): void;
}
