/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import PanelsVisualisationViewModelImpl from './PanelVisualisationViewModelImpl';
import VisualTypeHolder from '../../../domain/entity/VisualTypeHolders/VisualTypeHolder';
import NodeLinkViewModel from '../result-visualisations/node-link/NodeLinkViewModel';
import RawJSONViewModel from '../result-visualisations/raw-json/RawJSONViewModel';
import SemanticSubstratesViewModelImpl from '../result-visualisations/semantic-substrates/SemanticSubstratesViewModelImpl';
import PaohvisViewModelImpl from '../result-visualisations/paohvis/PaohvisViewModelImpl';
import HivePlotsViewModelImpl from '../result-visualisations/hiveplots/HivePlotsViewModelImpl';

/** Testsuite for PanelsVisualisationModelImpl. */
jest.mock('../../view/panel-visualisation/PanelVisualisationStylesheet.tsx');
jest.mock('../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet');

let nodeLink: NodeLinkViewModel;
let rawJSON: RawJSONViewModel;
let semanticSubstr = new SemanticSubstratesViewModelImpl();
let visualTypeHolder = new VisualTypeHolder();
let paohvisViewModel = new PaohvisViewModelImpl();
let hivePlotViewModel = new HivePlotsViewModelImpl();
describe('PanelsVisualisationModelImpl', () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it('onVisualTypeChanged', () => {
    const panelVisual = new PanelsVisualisationViewModelImpl(
      visualTypeHolder,
      rawJSON,
      nodeLink,
      semanticSubstr,
      paohvisViewModel,
      hivePlotViewModel,
    );
    const expectVisual = panelVisual.visualTypeHolder.getVisualType();

    panelVisual.onVisualTypeChanged();

    expect(panelVisual.resultVisualisationType).toEqual(expectVisual);
  });
});
