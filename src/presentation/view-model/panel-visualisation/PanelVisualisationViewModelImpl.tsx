/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BaseView from '../../view/BaseView';
import { useStyles } from '../../view/panel-visualisation/PanelVisualisationStylesheet';
import { ClassNameMap } from '@material-ui/styles';
import VisualTypeHolder, {
  PossibleVisualTypes,
} from '../../../domain/entity/VisualTypeHolders/VisualTypeHolder';
import VisualisationViewModel from './PanelVisualisationViewModel';
import VisualTypeListener from '../../../domain/entity/VisualTypeHolders/VisualTypeListener';
import RawJSONViewModel from '../result-visualisations/raw-json/RawJSONViewModel';
import NodeLinkViewModel from '../result-visualisations/node-link/NodeLinkViewModel';
import PaohvisViewModel from '../result-visualisations/paohvis/PaohvisViewModel';
import SemanticSubstratesViewModel from '../result-visualisations/semantic-substrates/SemanticSubstratesViewModel';
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import HivePlotsViewModel from '../result-visualisations/hiveplots/HivePlotsViewModel';

/** VisualisationViewModelImpl is the implementation of the interface defined in VisualisationViewModel. */
export default class VisualisationViewModelImpl
  extends AbstractBaseViewModelImpl
  implements VisualisationViewModel, VisualTypeListener
{
  public styles: ClassNameMap;
  public visualTypeHolder: VisualTypeHolder;
  public resultVisualisationType: string;
  public currentVisual: any;
  public rawJSONViewModel: RawJSONViewModel;
  public nodeLinkViewModel: NodeLinkViewModel;
  public semanticSubstratesViewModel: SemanticSubstratesViewModel;
  public paohvisViewModel: PaohvisViewModel;
  public hivePlotsViewModel: HivePlotsViewModel;

  public constructor(
    visualTypeHolder: VisualTypeHolder,
    rawJSONViewModel: RawJSONViewModel,
    nodeLinkViewModel: NodeLinkViewModel,
    semanticSubstratesViewModel: SemanticSubstratesViewModel,
    paohvisViewModel: PaohvisViewModel,
    hivePlotsViewModel: HivePlotsViewModel,
  ) {
    super();
    this.visualTypeHolder = visualTypeHolder;
    this.rawJSONViewModel = rawJSONViewModel;
    this.nodeLinkViewModel = nodeLinkViewModel;
    this.semanticSubstratesViewModel = semanticSubstratesViewModel;
    this.paohvisViewModel = paohvisViewModel;
    this.hivePlotsViewModel = hivePlotsViewModel;
    this.currentVisual = nodeLinkViewModel;
    this.styles = useStyles();

    this.resultVisualisationType = this.visualTypeHolder.getVisualType();
    this.visualTypeHolder.addVisualTypeListener(this);
  }
  /** Called when a button in the showMe window is pressed.
   *  Currently out of use.
   *  TODO Implement or remove.
   */
  public onVisualTypeChanged(): void {
    this.resultVisualisationType = this.visualTypeHolder.getVisualType();

    // Switch for the viewmodel to call the settings menu from
    switch (this.resultVisualisationType) {
      case 'node-link':
        this.currentVisual = this.nodeLinkViewModel;
        break;
      case 'semantic-substrates':
        this.currentVisual = this.semanticSubstratesViewModel;
        break;
      case 'paohvis':
        this.currentVisual = this.paohvisViewModel;
        break;
      case 'hiveplots':
        this.currentVisual = this.hivePlotsViewModel;
        break;
      default:
        this.currentVisual = this.rawJSONViewModel;
        break;
    }

    this.notifyViewAboutChanges();
  }
  /**
   * This function calls the visualTypeHolder about a change in visualType and closes the menu
   * @param visualType this is the new VisualType
   */
  public visualChange(visualType: string) {
    this.visualTypeHolder.changeVisualType(visualType as PossibleVisualTypes);
  }
}
