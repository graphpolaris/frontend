/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { PanelsViewModelProperties } from '../../../domain/entity/viewmodel/PanelsViewModelProperties';
import BaseViewModel from '../BaseViewModel';
import SchemaViewModel from '../graph-schema/SchemaViewModel';
import NavbarViewModel from '../navbar/NavbarViewModel';

import PanelVisualisationViewModel from '../panel-visualisation/PanelVisualisationViewModel';
import QueryBuilderViewModel from '../query-builder/QueryBuilderViewModel';
import QueryStatusListViewModel from '../query-status-list/QueryStatusListViewModel';
import FunctionsMenuViewModel from '../query-builder/functions-menu/FunctionsMenuViewModel';
import ErrorsViewModel from '../errors/ErrorsViewModel';
/** This is an interface of PanelsViewModel that will be given to the Panels view. */
export default interface PanelsViewModel extends BaseViewModel {
  open: boolean;
  schemaOpen: boolean;
  showQueryOpen: boolean;

  handleMouseDown(event: any, panelType: string): void;
  handleSchemaWidth(e: any): void;
  handleQueryHeight(e: any): void;
  handleMouseUp(): void;
  handleResize(): void;

  queryBuilderViewModel: QueryBuilderViewModel;
  queryStatusListViewModel: QueryStatusListViewModel;
  schemaViewModel: SchemaViewModel;
  panelVisualisationViewModel: PanelVisualisationViewModel;
  functionsMenuViewModel: FunctionsMenuViewModel;

  navbarViewModel: NavbarViewModel;

  classes: any;
  navBarHeight: any;
  schemaDrawerHeight: any;
  queryDrawerHeight: any;

  schemaDrawerWidth: any;
  queryDrawerWidth: any;

  currentColours: any;
}
