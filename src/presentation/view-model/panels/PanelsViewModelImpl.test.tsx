/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import QueryBuilderViewModel from '../query-builder/QueryBuilderViewModel';
import PanelsViewModelImpl from './PanelsViewModelImpl';
import SchemaViewModel from '../graph-schema/SchemaViewModel';
import { createEvent, render, screen } from '@testing-library/react';

import NavbarViewModel from '../navbar/NavbarViewModel';
import PanelVisualisationViewModel from '../panel-visualisation/PanelVisualisationViewModel';
import QueryStatusListViewModelImpl from '../query-status-list/QueryStatusListViewModelImpl';
import FunctionsMenuViewModel from '../query-builder/functions-menu/FunctionsMenuViewModel';
/** Testsuite for the PanelsViewModelImpl. */
jest.mock('../../view/panels/PanelUIStylesheet');
jest.mock('../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet');

let query: QueryBuilderViewModel;
let queryList: QueryStatusListViewModelImpl;
let schema: SchemaViewModel;

let navbar: NavbarViewModel;
let functionsMenuViewModel: FunctionsMenuViewModel;
let panelVisualisation: PanelVisualisationViewModel;
describe('PanelsViewModelImpl', () => {
  beforeEach(() => {
    jest.resetModules();
    panel = new PanelsViewModelImpl(
      query,
      queryList,
      schema,
      navbar,
      panelVisualisation,
      functionsMenuViewModel,
    );
  });
  let panel: PanelsViewModelImpl;

  // Test for handleResize
  it("'handleResize", () => {
    const expectedQueryHeight = window.innerHeight * 0.33;
    const expectedSchemaDrawerHeight = window.innerHeight - panel.navBarHeight;
    const expectedSchemaDrawerWidth = window.innerWidth * 0.25;
    const expectedQueryDrawerWidth = window.innerWidth - expectedSchemaDrawerWidth;

    panel.handleResize();

    expect(panel.queryDrawerHeight).toEqual(expectedQueryHeight);
    expect(panel.schemaDrawerHeight).toEqual(expectedSchemaDrawerHeight);
    expect(panel.schemaDrawerWidth).toEqual(expectedSchemaDrawerWidth);
    expect(panel.queryDrawerWidth).toEqual(expectedQueryDrawerWidth);
  });
  it('HandleMouseDown', () => {
    const down = createEvent.mouseDown(window, {
      clientX: 700,
      clientY: 50,
      buttons: 1,
    });
    const addEventListenerMock = jest.fn();

    document.addEventListener = addEventListenerMock;

    panel.handleMouseDown(down, 'schema');

    expect(panel.moveFunction).toEqual(panel.handleSchemaWidth);
    expect(addEventListenerMock).toBeCalledTimes(2);

    panel.handleMouseDown(down, 'query');
    expect(panel.moveFunction).toEqual(panel.handleQueryHeight);
    expect(addEventListenerMock).toBeCalledTimes(4);
  });

  it('handleMouseUp', () => {
    const removeEventListenerMock = jest.fn();

    document.removeEventListener = removeEventListenerMock;
    panel.handleMouseUp();
    expect(removeEventListenerMock).toBeCalledTimes(2);
  });
});

describe('SchemaWidth', () => {
  beforeEach(() => {
    jest.resetModules();
    panel = new PanelsViewModelImpl(
      query,
      queryList,
      schema,
      navbar,
      panelVisualisation,
      functionsMenuViewModel,
    );
  });
  let panel: PanelsViewModelImpl;

  it('handleSchemaWidth', () => {
    const move = createEvent.mouseMove(window, {
      clientX: 200,
      clientY: 50,
      buttons: 1,
    });
    const expected = 200;
    panel.handleSchemaWidth(move);
    expect(panel.schemaDrawerWidth).toEqual(expected);
  });
  it('handleSchemaWidth out of bounds', () => {
    const move = createEvent.mouseMove(window, {
      clientX: 1200,
      clientY: 50,
      buttons: 1,
    });
    const expected = panel.schemaDrawerWidth;
    panel.handleSchemaWidth(move);
    expect(panel.schemaDrawerWidth).toEqual(expected);
  });
});

describe('QueryHeight', () => {
  beforeEach(() => {
    jest.resetModules();
    panel = new PanelsViewModelImpl(
      query,
      queryList,
      schema,
      navbar,
      panelVisualisation,
      functionsMenuViewModel,
    );
  });
  let panel: PanelsViewModelImpl;

  it('handleQueryHeight', () => {
    const move = createEvent.mouseMove(window, {
      clientX: 700,
      clientY: 600,
      buttons: 1,
    });
    const expected = window.innerHeight - 600;
    panel.handleQueryHeight(move);
    expect(panel.queryDrawerHeight).toEqual(expected);
  });
  it('handleQueryHeight Out of bounds', () => {
    const move = createEvent.mouseMove(window, {
      clientX: 700,
      clientY: -50,
      buttons: 1,
    });
    const expected = panel.queryDrawerHeight; //window.innerHeight - 50;
    panel.handleQueryHeight(move);
    expect(panel.queryDrawerHeight).toEqual(expected);
  });
});
