/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import PanelsViewModel from './PanelsViewModel';
import { useStyles } from '../../view/panels/PanelUIStylesheet';
import QueryBuilderViewModel from '../query-builder/QueryBuilderViewModel';
import SchemaViewModel from '../graph-schema/SchemaViewModel';
import NavbarViewModel from '../navbar/NavbarViewModel';
import PanelVisualisationViewModel from '../panel-visualisation/PanelVisualisationViewModel';
import FunctionsMenuViewModel from '../query-builder/functions-menu/FunctionsMenuViewModel';

import QueryStatusListViewModel from '../query-status-list/QueryStatusListViewModel';
import { ColourPalettes } from '../../../domain/entity/customization/colours';
import { LocalStorage } from '../../../data/drivers/LocalStorage';
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import ErrorsViewModel from '../errors/ErrorsViewModel';
import { PanelsViewModelProperties } from '../../../domain/entity/viewmodel/PanelsViewModelProperties';

/** Implements the PanelsViewModel. */
export default class PanelsViewModelImpl
  extends AbstractBaseViewModelImpl
  implements PanelsViewModel
{
  public open: boolean;
  public schemaOpen: boolean;
  public showQueryOpen: boolean;

  public navBarHeight: number;
  public schemaDrawerHeight: number;
  public queryDrawerHeight: number;

  public schemaDrawerWidth: number;
  public queryDrawerWidth: number;

  public currentColours: any;

  public moveFunction: any;
  public classes: any;

  public schemaViewModel: SchemaViewModel;
  public queryBuilderViewModel: QueryBuilderViewModel;
  public panelVisualisationViewModel: PanelVisualisationViewModel;
  public queryStatusListViewModel: QueryStatusListViewModel;
  public navbarViewModel: NavbarViewModel;
  public functionsMenuViewModel: FunctionsMenuViewModel;

  /**
   * Creates the PanelsViewModelImpl using a given a queryBuilderViewModel and a schemaViewModel.
   * It initialises a panels view.
   */
  public constructor(
    queryBuilderViewModel: QueryBuilderViewModel,
    queryStatusListViewModel: QueryStatusListViewModel,
    schemaViewModel: SchemaViewModel,
    navbarViewModel: NavbarViewModel,
    panelVisualisationViewModel: PanelVisualisationViewModel,
    functionsMenuViewModel: FunctionsMenuViewModel,
  ) {
    super();
    this.queryBuilderViewModel = queryBuilderViewModel;
    this.queryStatusListViewModel = queryStatusListViewModel;
    this.schemaViewModel = schemaViewModel;
    this.navbarViewModel = navbarViewModel;
    this.panelVisualisationViewModel = panelVisualisationViewModel;
    this.functionsMenuViewModel = functionsMenuViewModel;

    this.classes = useStyles();
    this.open = true;
    this.schemaOpen = true;
    this.showQueryOpen = true;

    //If window is the same size as last time
    if (
      window.innerHeight == LocalStorage.instance().cache.panelWidthHeights.windowinnerHeight &&
      window.innerWidth == LocalStorage.instance().cache.panelWidthHeights.windowinnerWidth
    ) {
      //Set panel widths and heights to the same values.
      this.navBarHeight = LocalStorage.instance().cache.panelWidthHeights.navBarHeight;
      this.schemaDrawerHeight = LocalStorage.instance().cache.panelWidthHeights.schemaDrawerHeight;
      this.queryDrawerHeight = LocalStorage.instance().cache.panelWidthHeights.queryDrawerHeight;
      this.schemaDrawerWidth = LocalStorage.instance().cache.panelWidthHeights.schemaDrawerWidth;
      this.queryDrawerWidth = LocalStorage.instance().cache.panelWidthHeights.queryDrawerWidth;
    } //Else set to these default values
    else {
      this.navBarHeight = 50;
      this.schemaDrawerHeight = window.innerHeight - this.navBarHeight;
      this.queryDrawerHeight = window.innerHeight * 0.33;
      this.schemaDrawerWidth = window.innerWidth * 0.25;
      this.queryDrawerWidth = window.innerWidth - this.schemaDrawerWidth;
    }
    this.currentColours = ColourPalettes[LocalStorage.instance().cache.currentColours.key];
    this.SaveToLocalStorage();
  }

  /** Saves the widhts and heights of the components to local storage. */
  public SaveToLocalStorage() {
    LocalStorage.instance().cache.panelWidthHeights.windowinnerHeight = window.innerHeight;
    LocalStorage.instance().cache.panelWidthHeights.windowinnerWidth = window.innerWidth;
    LocalStorage.instance().cache.panelWidthHeights.navBarHeight = this.navBarHeight;
    LocalStorage.instance().cache.panelWidthHeights.queryDrawerHeight = this.queryDrawerHeight;
    LocalStorage.instance().cache.panelWidthHeights.queryDrawerWidth = this.queryDrawerWidth;
    LocalStorage.instance().cache.panelWidthHeights.schemaDrawerHeight = this.schemaDrawerHeight;
    LocalStorage.instance().cache.panelWidthHeights.schemaDrawerWidth = this.schemaDrawerWidth;
    LocalStorage.instance().SaveToCache();
  }
  /**
   * Changes the current panel sizes according to the new resized window size.
   * Then notifies the view about the changes that have been done.
   */
  public handleResize = () => {
    this.queryDrawerHeight = window.innerHeight * 0.33;
    this.schemaDrawerHeight = window.innerHeight - this.navBarHeight;
    this.schemaDrawerWidth = window.innerWidth * 0.25;
    this.queryDrawerWidth = window.innerWidth - this.schemaDrawerWidth;
    this.notifyViewAboutChanges();
    this.SaveToLocalStorage();
  };

  /**
   * Changes the current panel sizes according to the new resized window size.
   * Adds eventListener for mouseUp & mouseMove.
   * On mouseUp the function mouseUp gets called.
   * On mouseMove the function @param moveFunction is called.
   * @param event A mouseDown event.
   * @param panelType The specific dragger of the panel that has been clicked.
   * @param moveFunction This is a function placeholder, which changes to the function that is specified by the panelType case.
   */
  public handleMouseDown = (event: any, panelType: string) => {
    event.preventDefault();
    switch (panelType) {
      case 'schema':
        this.moveFunction = this.handleSchemaWidth;
        break;
      case 'query':
        this.moveFunction = this.handleQueryHeight;
        break;
    }

    document.addEventListener('mouseup', this.handleMouseUp, true);
    document.addEventListener('mousemove', this.moveFunction, true);
  };
  // Removes the EventListeners of mouseup & mousemove
  public handleMouseUp = () => {
    document.removeEventListener('mouseup', this.handleMouseUp, true);
    document.removeEventListener('mousemove', this.moveFunction, true);
  };
  /**
   * Updates the schemaDrawerWidth and notifies View about changes.
   * @param e A mouseMove event.
   * The e.preventDefault() function fixes some buggs with the eventListeners.
   * @param newWidth new calculated width of the drawer according to the mouse X coördinates.
   */
  public handleSchemaWidth = (e: any) => {
    e.preventDefault();
    const newWidth = e.clientX - document.body.offsetLeft;
    // panel boundaries
    if (newWidth < window.innerWidth * 0.5 && newWidth > 4) {
      this.schemaDrawerWidth = newWidth;
      this.notifyViewAboutChanges();
      this.SaveToLocalStorage();
    }
  };
  /**
   * If the newHeight doens't exceed the maximum height it Updates the queryDrawerHeight otherwise nothing happens.
   * Notifies View about changes
   * @param e A mouseMove event.
   * The e.preventDefault() function fixes some buggs with the eventListeners.
   * @param newHeight NewHeight calculates the new height of the drawer according to the mouse Y coördinates.
   */
  public handleQueryHeight = (e: any) => {
    e.preventDefault();
    const newHeight = window.innerHeight - e.clientY;
    //panel boundaries
    if (newHeight < window.innerHeight * 0.55 && newHeight > 4) {
      this.queryDrawerHeight = newHeight;
      this.notifyViewAboutChanges();
      this.SaveToLocalStorage();
    }
  };
}
