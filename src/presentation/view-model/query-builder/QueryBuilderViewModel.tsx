/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { ClassNameMap } from '@material-ui/styles';
import { Connection, FlowElement, OnLoadParams } from 'react-flow-renderer';
import { AnyNode, Edge } from '../../../domain/entity/query-builder/structures/Nodes';
import BaseViewModel from '../BaseViewModel';
import { AnyElement } from '../../../domain/entity/query-builder/structures/Nodes';
import NodesHolder from '../../../domain/entity/query-builder/models/NodesHolder';
import { FunctionTypes } from '../../../domain/entity/query-builder/structures/FunctionTypes';

/** This is an interface of QueryBuildViewModel that will be given to the QueryBuilder view. */
export default interface QueryBuilderViewModel extends BaseViewModel {
  myRef: React.RefObject<HTMLDivElement>;
  nodesHolder: NodesHolder;
  autoSendQueries: boolean;

  // Export function for the schema visualisation
  exportToPDF(): void;
  exportToPNG(): void;

  // Event triggered functions
  onNodeDrag(event: React.MouseEvent<Element, MouseEvent>, node: AnyNode): void;
  onNodeDragStop(event: React.MouseEvent<Element, MouseEvent>, node: AnyNode): void;
  onDragOver(event: React.DragEvent<HTMLDivElement>): void;
  onDrop(event: React.DragEvent<HTMLDivElement>): void;
  onLoad(params: OnLoadParams<any>): void;
  onKeyDown(event: any): void;
  onChangedValue(): void;
  onElementsRemove(elementsToRemove: FlowElement[]): void;
  onConnect(connection: Connection | Edge): void;
  clearBuilder(): void;
  toggleAutoSendQuery(): void;
  addFunction(functionTypes: FunctionTypes): void;
  sendQuery(): void;
  addAttribute(name: string, dataType: string): void;
}
