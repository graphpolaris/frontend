/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import BackendMessengerMock from '../../../data/drivers/backend-messenger/BackendMessengerMock';
import QueryApi from '../../../data/drivers/query/QueryApi';
import DatabaseHolder from '../../../domain/entity/navbar/DatabaseHolder';

import QueryRepository from '../../../domain/repository-interfaces/QueryRepository';
import QueryBuilderViewModelImpl from './QueryBuilderViewModelImpl';

import { mockElements } from '../../../data/mock-data/query-builder/mockDataQueryBuilderTweedekamer';
import {
  AnyNode,
  FunctionNode,
  QueryElementTypes,
} from '../../../domain/entity/query-builder/structures/Nodes';
import { FunctionTypes } from '../../../domain/entity/query-builder/structures/FunctionTypes';
describe('QueryBuilderViewModelImpl', () => {
  let QBVMI: QueryBuilderViewModelImpl; //queryBuilderViewModelImpl
  let queryRepository: QueryRepository;
  let databaseHolder: DatabaseHolder;

  beforeEach(() => {
    queryRepository = new QueryApi(new BackendMessengerMock('mockUrl'));
    databaseHolder = new DatabaseHolder();
    QBVMI = new QueryBuilderViewModelImpl(queryRepository, databaseHolder);
  });

  it('Should correctly switch databases', () => {
    const mockNode: AnyNode = mockElements.nodes[0];
    databaseHolder.changeDatabase('firstDatabase');
    QBVMI.setElementsUseCase.addNode(mockNode);
    expect(QBVMI.getElementsUseCase.getAllNodes().length).toEqual(1);
    databaseHolder.changeDatabase('secondDatabase');
    expect(QBVMI.getElementsUseCase.getAllNodes().length).toEqual(0);
    databaseHolder.changeDatabase('firstDatabase');
    expect(QBVMI.getElementsUseCase.getAllNodes().length).toEqual(1);
  });

  it('Should remove an element', () => {
    const mockNode = mockElements.nodes[0];
    QBVMI.setElementsUseCase.addNode(mockNode);
    expect(QBVMI.getElementsUseCase.getAllNodes().length).toEqual(1);
    QBVMI.onElementsRemove([mockNode]);
    expect(QBVMI.getElementsUseCase.getAllNodes().length).toEqual(0);
  });
  it('Should change the value of autoSendQuery', () => {
    QBVMI.autoSendQueries = false;
    QBVMI.toggleAutoSendQuery();
    expect(QBVMI.autoSendQueries).toEqual(true);
  });
  it('Should add an function', () => {
    QBVMI.addFunction(FunctionTypes.communityDetection);
    expect(
      QBVMI.getElementsUseCase
        .getAllNodes()
        .filter((node) => node.type == QueryElementTypes.Function).length,
    ).toEqual(1);
  });
  it('Should add an attribute', () => {
    QBVMI.addAttribute('blub', 'int');
    expect(
      QBVMI.getElementsUseCase
        .getAllNodes()
        .filter((node) => node.type == QueryElementTypes.Attribute).length,
    ).toEqual(1);
  });
  it('Should clear all elements', () => {
    const node0 = mockElements.nodes[0];
    QBVMI.setElementsUseCase.addNode(node0);
    const node1 = mockElements.nodes[5];
    QBVMI.setElementsUseCase.addNode(node0);
    const node2 = mockElements.nodes[10];
    QBVMI.setElementsUseCase.addNode(node0);
    QBVMI.clearBuilder();
    expect(QBVMI.getElementsUseCase.getAllNodes().length).toEqual(0);
  });
  it('Should print in the console', () => {
    const logSpy = jest.spyOn(console, 'log');
    QBVMI.exportToPDF();
    expect(logSpy).toBeCalledWith('Method not implemented.');
    QBVMI.exportToPNG();
    expect(logSpy).toBeCalledWith('Method not implemented.');
  });
});
