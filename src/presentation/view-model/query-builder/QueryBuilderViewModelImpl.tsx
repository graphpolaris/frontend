/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import NodesHolder from '../../../domain/entity/query-builder/models/NodesHolder';
import { Connection, OnLoadParams } from 'react-flow-renderer';
import {
  EntityNode,
  QueryElementTypes,
  AnyNode,
  RelationNode,
  Edge,
  AnyElement,
} from '../../../domain/entity/query-builder/structures/Nodes';
import TranslateQueryToJSONUseCase from '../../../domain/usecases/query-builder/TranslateQueryToJSONUseCase';
import QueryBuilderViewModel from './QueryBuilderViewModel';
import DragElementUseCase from '../../../domain/usecases/query-builder/DragElementUseCase';
import DatabaseHolder from '../../../domain/entity//navbar/DatabaseHolder';
import DatabaseListener from '../../../domain/entity//navbar/DatabaseListener';
import SetElementsUseCase from '../../../domain/usecases/query-builder/SetElementsUseCase';
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import ValidateConnectionUseCase from '../../../domain/usecases/query-builder/ValidateConnectionUseCase';
import React from 'react';
import Broker from '../../../domain/entity/broker/broker';
import QueryRepository from '../../../domain/repository-interfaces/QueryRepository';
import {
  NodePosition,
  RelationPosToFromEntityPos,
  RelationPosToToEntityPos,
} from '../../../domain/entity/query-builder/structures/NodePosition';
import { FunctionTypes } from '../../../domain/entity/query-builder/structures/FunctionTypes';
import { LocalStorage } from '../../../data/drivers/LocalStorage';
import { Handles } from '../../../domain/entity/query-builder/structures/Handles';
import { FunctionDescription } from '../../../domain/entity/query-builder/structures/FunctionDescriptions';
import GetElementsUseCase from '../../../domain/usecases/query-builder/GetElementsUseCase';

/** An implementation of the `QueryBuilderViewModel` to be used by the query-builder view. */
export default class QueryBuilderViewModelImpl
  extends AbstractBaseViewModelImpl
  implements QueryBuilderViewModel, DatabaseListener
{
  // The nodesholder is an object which holds lists of all entity, relation and attribute nodes
  public nodesHolder: NodesHolder;
  //public elements: any;
  public databaseHolder: DatabaseHolder;

  // React flow refecence for positioning on drop
  public myRef: React.RefObject<HTMLDivElement>;

  // QueryRepository used for sending queries.
  private queryRepository: QueryRepository;

  // Instances of all the usecases used in this implementation.
  private translateQueryUseCase: TranslateQueryToJSONUseCase;
  private dragElementUseCase: DragElementUseCase;
  public setElementsUseCase: SetElementsUseCase;
  public getElementsUseCase: GetElementsUseCase;
  private validateConnectionUseCase: ValidateConnectionUseCase;

  private reactFlowInstance: any;
  private currentDatabase = '';
  private elementsPerDatabase: Map<string, AnyElement[]>;
  private nodesHolderPerDatabase: Map<string, NodesHolder>;

  public autoSendQueries: boolean;

  public constructor(queryRepository: QueryRepository, databaseHolder: DatabaseHolder) {
    //TODO fix reduancies in all this
    super();

    // start running updateNodesHolder
    this.nodesHolder = new NodesHolder([]);
    this.getElementsUseCase = new GetElementsUseCase(this.nodesHolder);
    this.setElementsUseCase = new SetElementsUseCase(this.nodesHolder, this.getElementsUseCase);
    this.validateConnectionUseCase = new ValidateConnectionUseCase(
      this.nodesHolder,
      this.getElementsUseCase,
    );
    this.dragElementUseCase = new DragElementUseCase(
      this.validateConnectionUseCase,
      this.setElementsUseCase,
      this.getElementsUseCase,
      this.nodesHolder,
    );
    this.translateQueryUseCase = new TranslateQueryToJSONUseCase(
      this.nodesHolder,
      this.getElementsUseCase,
    );

    this.nodesHolder.addQueryListener(() => this.triggerAutoQuery());
    this.nodesHolder.addReactFlowListener(() => this.SaveElements());
    this.notifyViewAboutChanges();
    this.nodesHolder.notifyReactFlow;
    // end running updateNodesHolder

    this.myRef = React.createRef();
    this.reactFlowInstance = null;
    this.queryRepository = queryRepository;

    this.databaseHolder = databaseHolder;
    this.databaseHolder.addDatabaseListener(this);
    this.currentDatabase = databaseHolder.getDatabase();

    this.elementsPerDatabase = new Map(
      Object.entries(LocalStorage.instance().cache.elementsperDatabaseObject),
    );

    this.nodesHolderPerDatabase = new Map<string, NodesHolder>();
    this.elementsPerDatabase.forEach((value, key) => {
      this.nodesHolderPerDatabase.set(key, new NodesHolder(value));
    });

    if (this.nodesHolderPerDatabase.has(this.currentDatabase)) {
      const newNodesholder = this.nodesHolderPerDatabase.get(this.currentDatabase);
      if (newNodesholder != null) this.updateNodesHolder(newNodesholder);
    }

    this.autoSendQueries = LocalStorage.instance().cache.autoSendQueries;
  }
  /**
   * For when changing to a new nodesholder in the query builder
   * This is also called when reloading old queries.
   * @param nodesHolder the nodesHolder you which to change to.
   */
  public updateNodesHolder(nodesHolder: NodesHolder) {
    this.nodesHolder = nodesHolder;
    this.getElementsUseCase = new GetElementsUseCase(this.nodesHolder);
    this.setElementsUseCase = new SetElementsUseCase(this.nodesHolder, this.getElementsUseCase);
    this.validateConnectionUseCase = new ValidateConnectionUseCase(
      this.nodesHolder,
      this.getElementsUseCase,
    );
    this.dragElementUseCase = new DragElementUseCase(
      this.validateConnectionUseCase,
      this.setElementsUseCase,
      this.getElementsUseCase,
      this.nodesHolder,
    );
    this.translateQueryUseCase = new TranslateQueryToJSONUseCase(
      this.nodesHolder,
      this.getElementsUseCase,
    );

    this.nodesHolder.addQueryListener(() => this.triggerAutoQuery());
    this.nodesHolder.addReactFlowListener(() => this.SaveElements());
    this.notifyViewAboutChanges();
    this.nodesHolder.notifyReactFlow();
  }

  /** The onDatabaseChanged gets notified when databases are changed
   * First saves the old nodesholder and then loads in the new ones.
   */
  public onDatabaseChanged(): void {
    this.elementsPerDatabase.set(this.currentDatabase, this.getElementsUseCase.getAllElements());
    this.nodesHolderPerDatabase.set(this.currentDatabase, this.nodesHolder);

    this.currentDatabase = this.databaseHolder.getDatabase();
    const loadedNodesHolder = this.nodesHolderPerDatabase.get(this.currentDatabase);
    if (loadedNodesHolder) this.updateNodesHolder(loadedNodesHolder);
    else this.updateNodesHolder(new NodesHolder([]));
    this.SaveElements();
    this.notifyViewAboutChanges();
  }

  /** Saves all nodes and edges to the query builder */
  public SaveElements() {
    this.elementsPerDatabase.set(this.currentDatabase, this.getElementsUseCase.getAllElements());
    const elementsPerDatabaseObject = Object.fromEntries(this.elementsPerDatabase);
    LocalStorage.instance().cache.elementsperDatabaseObject = elementsPerDatabaseObject;
    LocalStorage.instance().SaveToCache();
  }

  /** The onLoad function is called upon the page loading. */
  public onLoad = (reactFlowInstance: OnLoadParams<any>): void => {
    this.reactFlowInstance = reactFlowInstance;
    this.notifyViewAboutChanges();
    //TODO neaten up
  };

  /**
   * Will be called when a node or edge is deleted via ReactFlow.
   * @param elementsToRemove These are the elements that need to be removed.
   */
  public onElementsRemove = (elementsToRemove: AnyElement[]): void => {
    this.setElementsUseCase.RemoveElements(elementsToRemove);
  };

  /**  Changes the autoSendQuery to !autoSendQuery
   *   Also saves it in the cache
   */
  public toggleAutoSendQuery() {
    this.autoSendQueries = !this.autoSendQueries;
    LocalStorage.instance().cache.autoSendQueries = this.autoSendQueries;
    LocalStorage.instance().SaveToCache();
  }

  /** If autosend is off the addition and deletion of nodes doesn't send queries. */
  public triggerAutoQuery() {
    if (this.autoSendQueries) {
      this.sendQuery();
    }
  }
  /**
   * Will translate the current query consisting of ReactFlow elements to a general JSON formatted string.
   * It then calls the sendQuery function of the given sendQueryUseCase with this JSON string.
   * @return {Promise<void>} A promise until the result of the query is returned.
   */
  public sendQuery() {
    try {
      const query = this.translateQueryUseCase.TranslateQueryToObject();
      this.queryRepository
        .sendQuery({ ...query, databaseName: this.currentDatabase })
        .then((id) => Broker.instance().publish({ id, query }, 'query_sent'))
        .catch((reason) => console.error(reason));
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Called by ReactFlow when an node is being dragged.
   * @param event The mouse event that triggers the drag.
   * @param node The node that will be dragged.
   */
  public onNodeDrag = (event: React.MouseEvent<Element, MouseEvent>, node: AnyNode) => {
    this.dragElementUseCase.OnDrag(node);
  };

  /**
   * Called by ReactFlow when the dragging of a node is stopped, (on mouse up).
   * This function also checks if a modifier gets connected and if so send the query to the back-end.
   * @param event The mouse event that stops the drag.
   * @param node The node that stopped dragging.
   */
  public onNodeDragStop = (event: React.MouseEvent<Element, MouseEvent>, node: AnyNode) => {
    this.dragElementUseCase.OnDragStop(node);
  };

  /** Called when the value of an attribute has changed. */
  public onChangedValue = (): void => {
    this.notifyViewAboutChanges();
  };

  public onKeyDown = (event: any): void => {
    if (event.key == 'Enter') {
      this.sendQuery();
    }
    this.notifyViewAboutChanges();
  };

  /** This is called when you wish to add a functionpill to the querybuilder from the menu.
   * @param functionType The functionType of the functionpill you wish to add.
   */
  public addFunction = (functionType: FunctionTypes): void => {
    let position: NodePosition = { x: 0, y: 0 };
    try {
      const reactFlow = this.myRef.current as HTMLDivElement;
      const reactFlowBounds = reactFlow.getBoundingClientRect();
      position = this.reactFlowInstance.project({
        x: reactFlowBounds.width * 0.8,
        y: reactFlowBounds.height * 0.25,
      });
    } catch {
      console.warn('Dropped the new node on (0,0)');
    }
    this.setElementsUseCase.createFunction(functionType, position);
  };

  public onDragOver = (event: React.DragEvent<HTMLDivElement>): void => {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  };

  /**
   * The onDrop is called when the user drops an element from the schema onto the QueryBuilder.
   * In the onDrop query elements will be created based on the data stored in the drag event (datastrasfer).
   * @param event Drag event.
   */
  public onDrop = (event: React.DragEvent<HTMLDivElement>): void => {
    event.preventDefault();

    // The dropped element should be a valid reactflow element
    const data: string = event.dataTransfer.getData('application/reactflow');
    if (data.length == 0) return;

    const dragData = JSON.parse(data);
    const reactFlow = this.myRef.current as HTMLDivElement;
    const reactFlowBounds = reactFlow.getBoundingClientRect();
    const position = this.reactFlowInstance.project({
      //TODO: this position should be centre of entity, rather than topleft
      x: event.clientX - reactFlowBounds.left,
      y: event.clientY - reactFlowBounds.top,
    });

    // prettier-ignore
    switch (dragData.type) {
      case QueryElementTypes.Entity:
        this.setElementsUseCase.createNodeFromSchema(
          QueryElementTypes.Entity,
          position,
          dragData.name,
          dragData.fadeIn
        ); break;
      // Creates a relation element an will also create the 2 related entities together with the connections
      case QueryElementTypes.Relation:
        let wasAutoSendQuery = false
        if(this.autoSendQueries){this.autoSendQueries = false; wasAutoSendQuery = true}
        const relation = this.setElementsUseCase.createNodeFromSchema(
          QueryElementTypes.Relation,
          position,
          dragData.name,
          dragData.fadeIn,
          undefined,
          dragData.collection,
        ) as RelationNode;
        const leftEntity = this.setElementsUseCase.createNodeFromSchema(
          QueryElementTypes.Entity,          
          RelationPosToFromEntityPos(position),
          dragData.from,
          true
        ) as EntityNode;
        const rightEntity = this.setElementsUseCase.createNodeFromSchema(
          QueryElementTypes.Entity,
          RelationPosToToEntityPos(position),
          dragData.to,
          true
        ) as EntityNode; 
        this.setElementsUseCase.connectEntityToRelation(leftEntity, relation, false);
        this.setElementsUseCase.connectEntityToRelation(rightEntity, relation, true);

        //To prevent 5 sends when adding a relation.
        if(wasAutoSendQuery){this.autoSendQueries= true;this.sendQuery();}
        break;
      // Creates an attribute element with the correct dataType  
      case QueryElementTypes.Attribute:
        this.setElementsUseCase.createNodeFromSchema(
          QueryElementTypes.Attribute,
          position,
          dragData.name,
          dragData.fadeIn,
          dragData.datatype
        ); break;
    }
  };

  /**
   * Called by ReactFlow when you connect nodes by dragging from one handle to another
   */
  public onConnect = (connection: Connection | Edge): void => {
    if (
      !connection.source ||
      !connection.sourceHandle ||
      !connection.target ||
      !connection.targetHandle
    )
      throw new Error('Invalid data on connect event');

    const sourceNode = this.getElementsUseCase.getNodeByID(connection.source);
    const sourceHandle = connection.sourceHandle as Handles;
    const targetNode = this.getElementsUseCase.getNodeByID(connection.target);
    const targetHandle = connection.targetHandle as Handles;

    if (
      this.validateConnectionUseCase.validateConnection(
        sourceNode,
        sourceHandle,
        targetNode,
        targetHandle,
      )
    ) {
      this.setElementsUseCase.createEdge(sourceNode, sourceHandle, targetNode, targetHandle);
      return;
    }
    console.warn('NotValid!');
    return;
  };
  /** Removes all elements from the query builder */
  public clearBuilder(): void {
    this.setElementsUseCase.RemoveAllElements();
  }

  /**
   * Places an attribute node on a fixed position in the query builder.
   * @param name The name of the attribute
   * @param dataType The type of the attribute
   */
  public addAttribute = (name: string, dataType: string): void => {
    let position: NodePosition = { x: 0, y: 0 };
    try {
      const reactFlow = this.myRef.current as HTMLDivElement;
      const reactFlowBounds = reactFlow.getBoundingClientRect();
      position = {
        x: reactFlowBounds.left / 2,
        y: reactFlowBounds.height / 2,
      };
    } catch {
      console.warn('Dropped the new node on (0,0)');
    }
    this.setElementsUseCase.createNodeFromSchema(
      QueryElementTypes.Attribute,
      position,
      name,
      false,
      dataType, //string, int, bool
    );
  };

  /** Not implemented method for exporting the query builder visualisation to PNG. */
  public exportToPNG(): void {
    console.log('Method not implemented.');
  }

  /** Not implemented method for exporting the query builder visualisation to PDF. */
  public exportToPDF(): void {
    console.log('Method not implemented.');
  }
}
