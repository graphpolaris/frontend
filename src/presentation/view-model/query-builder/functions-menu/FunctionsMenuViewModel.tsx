/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { ClassNameMap } from '@material-ui/styles';
import BaseViewModel from '../../BaseViewModel';
import { FunctionDescription } from '../../../../domain/entity/query-builder/structures/FunctionDescriptions';

/** This is an interface of FunctionsMenuViewModel that will be given to the FunctionsMenu view. */
export default interface FunctionsMenuViewModel extends BaseViewModel {
  myRef: React.RefObject<HTMLDivElement>;
  functionTypes: Record<string, FunctionDescription>;

  getFunctionKeys(): string[];
  getFunctionInfo(functionType: string): string;
}
