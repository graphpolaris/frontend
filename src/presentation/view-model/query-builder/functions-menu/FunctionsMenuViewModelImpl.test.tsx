/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import FunctionsMenuViewModelImpl from './FunctionsMenuViewModelImpl';
describe('FunctionsMenuViewModelImpl', () => {
  let functionsMenuViewModelImpl: FunctionsMenuViewModelImpl;
  beforeEach(() => {
    functionsMenuViewModelImpl = new FunctionsMenuViewModelImpl();
  });
  it('Should retrieve a description', () => {
    let functionKey = functionsMenuViewModelImpl.getFunctionKeys()[0];
    expect(functionsMenuViewModelImpl.getFunctionInfo(functionKey).length).toBeGreaterThan(20);
  });
});
