/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import FunctionsModelViewModel from './FunctionsMenuViewModel';
import AbstractBaseViewModelImpl from '../../AbstractBaseViewModelImpl';
import {
  FunctionDescription,
  AvailableFunctions,
} from '../../../../domain/entity/query-builder/structures/FunctionDescriptions';
import React from 'react';

export default class FunctionsMenuViewModelImpl
  extends AbstractBaseViewModelImpl
  implements FunctionsModelViewModel {
  public myRef: React.RefObject<HTMLDivElement>;

  public functionTypes: Record<string, FunctionDescription>;

  public constructor() {
    super();
    this.myRef = React.createRef();
    this.functionTypes = AvailableFunctions;
  }

  /** Retrieves all keys from the list of available function types */
  public getFunctionKeys(): string[] {
    return Object.keys(this.functionTypes);
  }

  /** Returns a description of the given function type
   * @param functionType type of function of which its info is needed
   */
  public getFunctionInfo(functionType: string): string {
    return this.functionTypes[functionType].description;
  }
}
