/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BackendMessengerMock from '../../../data/drivers/backend-messenger/BackendMessengerMock';
import QueryApi from '../../../data/drivers/query/QueryApi';
import DatabaseHolder from '../../../domain/entity/navbar/DatabaseHolder';
import QueryBuilderViewModelImpl from '../query-builder/QueryBuilderViewModelImpl';
import QueryStatusListViewModelImpl from './QueryStatusListViewModelImpl';
jest.mock('../../view/query-status-list/QueryStatusListStylesheet');

describe('QueryStatusListViewModelImpl', () => {
  const backendMessenger = new BackendMessengerMock('MockUrl');
  const queryRepository = new QueryApi(backendMessenger);
  const dataBaseHolder = new DatabaseHolder();
  const queryBuilderViewModelImpl = new QueryBuilderViewModelImpl(queryRepository, dataBaseHolder);
  let QSTLVMI = new QueryStatusListViewModelImpl(
    queryRepository,
    dataBaseHolder,
    queryBuilderViewModelImpl,
  );

  beforeEach(
    () =>
      (QSTLVMI = new QueryStatusListViewModelImpl(
        queryRepository,
        dataBaseHolder,
        queryBuilderViewModelImpl,
      )),
  );

  it('should add new queryID items', () => {
    // check that it consumes a 'query_sent' update message.
    const query = {
      return: {
        entities: [],
        relations: [],
      },
      entities: [],
      relations: [],
      limit: 123,
      modifiers: [],
    };
    QSTLVMI.consumeMessageFromBackend({ id: '1a', query }, 'query_sent');
    expect(QSTLVMI.queryIDsOrder[0]).toBe('1a');
    expect(QSTLVMI.queries['1a'].number).toBe(1);
    expect(QSTLVMI.queries['1a'].status).toBe('Query sent');
    expect(QSTLVMI.queries['1a'].sentQuery).toEqual(query);
    expect(QSTLVMI.queries['1a'].resultIsDisplayed).toBe(false);

    // Check that it consumes a 'query_status_update' message.
    QSTLVMI.consumeMessageFromBackend({ queryID: '2b', status: 'Received' }, 'query_status_update');
    expect(QSTLVMI.queryIDsOrder).toHaveLength(2);
    expect(QSTLVMI.queryIDsOrder[0]).toBe('2b');
    expect(QSTLVMI.queries['2b'].number).toBe(2);
    expect(QSTLVMI.queries['2b'].status).toBe('Received');
    expect(QSTLVMI.queries['2b'].sentQuery).toBe(undefined);
    expect(QSTLVMI.queries['2b'].resultIsDisplayed).toBe(false);

    // Now tests that query items are updated:
    // First update 'a1'
    QSTLVMI.consumeMessageFromBackend({ queryID: '1a', status: 'Received' }, 'query_status_update');
    expect(QSTLVMI.queryIDsOrder).toHaveLength(2);
    expect(QSTLVMI.queryIDsOrder[1]).toBe('1a');
    expect(QSTLVMI.queries['1a'].number).toBe(1);
    expect(QSTLVMI.queries['1a'].status).toBe('Received');
    expect(QSTLVMI.queries['1a'].sentQuery).toEqual(query);
    expect(QSTLVMI.queries['1a'].resultIsDisplayed).toBe(false);

    // Update '2b'
    QSTLVMI.consumeMessageFromBackend({ id: '2b', query }, 'query_sent');
    expect(QSTLVMI.queryIDsOrder).toHaveLength(2);
    expect(QSTLVMI.queryIDsOrder[0]).toBe('2b');
    expect(QSTLVMI.queries['2b'].number).toBe(2);
    expect(QSTLVMI.queries['2b'].status).toBe('Received');
    expect(QSTLVMI.queries['2b'].sentQuery).toEqual(query);
    expect(QSTLVMI.queries['2b'].resultIsDisplayed).toBe(false);

    // Update a message to 'Completed'
    QSTLVMI.consumeMessageFromBackend(
      { queryID: '2b', status: 'Completed' },
      'query_status_update',
    );
    expect(QSTLVMI.queryIDsOrder).toHaveLength(2);
    expect(QSTLVMI.queryIDsOrder[0]).toBe('2b');
    expect(QSTLVMI.queries['2b'].number).toBe(2);
    expect(QSTLVMI.queries['2b'].status).toBe('Completed');
    expect(QSTLVMI.queries['2b'].sentQuery).toEqual(query);
    expect(QSTLVMI.queries['2b'].resultIsDisplayed).toBe(true);
  });
  it('Should update the status when a status update happens', () => {
    const logSpy = jest.spyOn(console, 'log');
    QSTLVMI.consumeMessageFromBackend({ queryID: '2b', status: 'Received' }, 'query_error');
    expect(logSpy).toBeCalledWith('Received query_error');
  });
  it('Should warn the user about query_errors', () => {
    const logSpy = jest.spyOn(console, 'log');
    QSTLVMI.consumeMessageFromBackend({ queryID: '2b', status: 'Received' }, 'query_error');
    expect(logSpy).toBeCalledWith('Received query_error');
  });

  it('should send retrieve query request onReloadClicked', () => {
    QSTLVMI.consumeMessageFromBackend({ queryID: '2b', status: 'Received' }, 'query_status_update');

    QSTLVMI.onReloadClicked('2b');
    setTimeout(() => expect(QSTLVMI.queries['2b'].resultIsDisplayed).toBe(true), 200);
    expect(queryBuilderViewModelImpl.getElementsUseCase.getAllElements()).toEqual(
      QSTLVMI.queries['2b'].elements,
    );
  });
});
