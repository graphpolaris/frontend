/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import BaseViewModel from '../BaseViewModel';
import { QueryStatusListItem } from './QueryStatusListViewModelImpl';

/**
 * QueryStatusListViewModel is an interface used to implement the ViewModel implementation for the QueryStatusListComponent.
 * Defines all public fields used in the RQueryStatusListViewModel implementation.
 */
export default interface QueryStatusListViewModel extends BaseViewModel {
  /** The queries with their status and id. */
  queries: Record<string, QueryStatusListItem>;

  /** The query ids, in order of appearing in the list. */
  queryIDsOrder: string[];

  onReloadClicked(queryID: string): void;

  styles: ClassNameMap;
}
