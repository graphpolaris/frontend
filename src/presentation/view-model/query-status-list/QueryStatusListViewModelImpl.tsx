/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import Broker from '../../../domain/entity/broker/broker';
import BrokerListener from '../../../domain/entity/broker/BrokerListenerInterface';
import { TranslatedJSONQuery } from '../../../domain/entity/query-builder/structures/TranslatedJSONQuery';
import {
  isQueryStatusUpdateMessage,
  QueryStatusUpdateMessage,
} from '../../../domain/entity/query-status-update/structures/QueryStatusUpdateMessage';
import QueryStatusListViewModel from './QueryStatusListViewModel';
import { useStyles } from './../../view/query-status-list/QueryStatusListStylesheet';
import QueryRepository from '../../../domain/repository-interfaces/QueryRepository';
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import {
  AnyElement,
  EntityNode,
  RelationNode,
} from '../../../domain/entity/query-builder/structures/Nodes';
import { LocalStorage } from '../../../data/drivers/LocalStorage';
import DatabaseHolder from '../../../domain/entity/navbar/DatabaseHolder';
import GetElementsUseCase from '../../../domain/usecases/query-builder/GetElementsUseCase';
import QueryBuilderViewModelImpl from '../query-builder/QueryBuilderViewModelImpl';
import NodesHolder from '../../../domain/entity/query-builder/models/NodesHolder';

/** Type for all data needed to display a query status bar. */
export type QueryStatusListItem = {
  database: string;
  elements: AnyElement[];
  resultIsDisplayed: boolean;
  number: number;
  time: string;
  status: string;
  sentQuery: TranslatedJSONQuery | undefined;
};

/** QueryStatusViewModel implementation, takes care of the query status list item logic and interactions. */
export default class QueryStatusListViewModelImpl
  extends AbstractBaseViewModelImpl
  implements QueryStatusListViewModel, BrokerListener
{
  public queries: Record<string, QueryStatusListItem>;
  public queryIDsOrder: string[];
  public databaseHolder: DatabaseHolder;
  public styles: ClassNameMap;
  public queryBuilderViewModelImpl: QueryBuilderViewModelImpl;

  private counter: number;

  private queryRepository: QueryRepository;

  /** @param queryRepository {QueryRepository} A QueryRepository implementation. */
  constructor(
    queryRepository: QueryRepository,
    databaseHolder: DatabaseHolder,
    queryBuilderViewModelImpl: QueryBuilderViewModelImpl,
  ) {
    super();
    this.queryRepository = queryRepository;
    this.databaseHolder = databaseHolder;
    this.styles = useStyles();
    this.queries = LocalStorage.instance().cache.queryStatusList.queries;
    this.queryIDsOrder = LocalStorage.instance().cache.queryStatusList.queryIDsOrder;
    this.counter = this.queryIDsOrder.length;
    this.queryBuilderViewModelImpl = queryBuilderViewModelImpl;
    Broker.instance().subscribe(this, 'query_status_update');
    Broker.instance().subscribe(this, 'query_sent');
    Broker.instance().subscribe(this, 'query_error');
  }

  public consumeMessageFromBackend(message: unknown, routingkey: string): void {
    switch (routingkey) {
      case 'query_sent':
        // Check if the incoming message is of type {id: string, query: TranslatedJSONQuery}
        const { id, query } = message as { id: string; query: TranslatedJSONQuery };
        this.consumeQuerySentMessage(id, query);
        break;

      case 'query_status_update':
        if (isQueryStatusUpdateMessage(message)) this.consumeQueryStatusUpdate(message);
        break;

      case 'query_error':
        console.log('Received query_error');
        if (isQueryStatusUpdateMessage(message)) this.consumeQueryStatusUpdate(message);
        break;
    }
  }

  /**
   * Called when the user clicks a reload icon.
   * Requests the backend to retrieve a cached query.
   * @param queryID {string} The id of the query list item on which the reload icon has been clicked.
   */
  public onReloadClicked(queryID: string): void {
    this.queryRepository.retrieveCachedQuery(queryID).then(() => {
      this.databaseHolder.changeDatabase(this.queries[queryID].database);
      this.queryBuilderViewModelImpl.updateNodesHolder(
        new NodesHolder(this.queries[queryID].elements),
      );
      this.setResultDisplayedForQuery(queryID);
      this.notifyViewAboutChanges();
    });
  }
  /** Saves the changes to the cache */
  public saveChanges() {
    LocalStorage.instance().cache.queryStatusList.queries = this.queries;
    LocalStorage.instance().cache.queryStatusList.queryIDsOrder = this.queryIDsOrder;
    LocalStorage.instance().SaveToCache();
  }

  /**
   * Takes care of processing a 'Query sent' message from the backend.
   * @param id {string} The id for the query.
   * @param query {TranslatedJSONQuery} The translated json query, translated on the frontend!
   */
  private consumeQuerySentMessage(id: string, query: TranslatedJSONQuery): void {
    if (this.queries[id]) {
      this.queries[id].sentQuery = query;
    } else {
      this.counter++;
      this.queryIDsOrder.unshift(id);
      this.queries[id] = {
        database: this.databaseHolder.getDatabase(),
        elements: this.queryBuilderViewModelImpl.getElementsUseCase.getAllElements(),
        number: this.counter,
        status: 'Query sent',
        sentQuery: query,
        time: this.getCurrentTimeAsString(),
        resultIsDisplayed: false,
      };
    }
    this.saveChanges();
    this.notifyViewAboutChanges();
  }

  /**
   * Takes care of processing a QueryStatusUpdateMessage from the backend.
   * @param message {QueryStatusUpdateMessage} The message from the backend.
   */
  private consumeQueryStatusUpdate(message: QueryStatusUpdateMessage): void {
    // Check if the queryID is present in the list.
    // If so update the status.
    if (this.queries[message.queryID]) {
      this.queries[message.queryID].status = message.status;
      this.queries[message.queryID].time = this.getCurrentTimeAsString();
      this.queries[message.queryID].resultIsDisplayed = false;
    } else {
      // otherwise we add a new item to the list.
      this.counter++;
      this.queryIDsOrder.unshift(message.queryID);
      this.queries[message.queryID] = {
        database: this.databaseHolder.getDatabase(),
        elements: this.queryBuilderViewModelImpl.getElementsUseCase.getAllElements(),
        number: this.counter,
        status: message.status,
        sentQuery: undefined,
        time: this.getCurrentTimeAsString(),
        resultIsDisplayed: false,
      };
    }
    if (message.status == 'Completed') this.setResultDisplayedForQuery(message.queryID);
    this.saveChanges();
    this.notifyViewAboutChanges();
  }

  /**
   * Sets the query with queryID as the currently displayed result.
   * @param queryID {string} The queryID to set for.
   */
  private setResultDisplayedForQuery(queryID: string): void {
    this.queryIDsOrder.forEach((id) => (this.queries[id].resultIsDisplayed = id == queryID));
  }

  /**
   * Get the current hours and minutes.
   * @returns {string} hours and minutes as string in the format `HH:MM`.
   */
  private getCurrentTimeAsString(): string {
    // get the current time
    const date = new Date();
    const hours = this.addLeadingZero(date.getHours());
    const minutes = this.addLeadingZero(date.getMinutes());
    return hours + ':' + minutes;
  }

  /**
   * Adds leading zeros to "time" numbers, 0 - 24.
   * @param {number} n The number to add leading zero's to.
   * @returns {string} the number in a string with leading zero's if necessary.
   */
  private addLeadingZero(n: number): string {
    return n < 10 ? '0' + n : String(n);
  }
}
