/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from '../../../domain/entity/broker/broker';
import AbstractResultVisViewModelImpl from './AbstractResultVisViewModelImpl';

/** Testsuite for the AbstractResultVisViewModelImpl. */
describe('AbstractResultVisViewModelImpl', () => {
  it('should consume messages when subscribed', () => {
    const resultVisImpl = new AbstractResultVisViewModelImpl();

    const consumeMock = jest.fn();
    resultVisImpl.consumeMessageFromBackend = consumeMock;

    // A message should not be consumed when the viewmodel impl is not yet subcribed
    Broker.instance().publish('test1', 'query_result');
    expect(consumeMock).toHaveBeenCalledTimes(0);

    // When it is subscribed it should.
    resultVisImpl.subscribeToQueryResult();
    Broker.instance().publish('test2', 'query_result');
    expect(consumeMock.mock.calls[0][0]).toEqual('test1');
    expect(consumeMock.mock.calls[1][0]).toEqual('test2');

    // When the view model impl is unsubscribed it shouldn't consume messages anymore
    resultVisImpl.unSubscribeFromQueryResult();
    Broker.instance().publish('test', 'query_result');
    expect(consumeMock).toHaveBeenCalledTimes(2);
  });
});
