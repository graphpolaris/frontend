/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from '../../../domain/entity/broker/broker';
import Exporter from '../../../domain/entity/exporter/Exporter';
import AbstractBaseViewModelImpl from '../AbstractBaseViewModelImpl';
import ResultVisViewModel from './ResultVisViewModel';

/**
 * An abstract implementation of the ResultVisViewModel.
 * Implements the subcribe to/from query result functions.
 */
export default class AbstractResultVisViewModelImpl
  extends AbstractBaseViewModelImpl
  implements ResultVisViewModel
{
  /** Subscribes the ViewModel to the broker with the query_result routingkey. */
  public subscribeToQueryResult(): void {
    Broker.instance().subscribe(this, 'query_result');
  }

  /** Unsubscribes the ViewModel from the broker with the query_result routingkey. */
  public unSubscribeFromQueryResult(): void {
    Broker.instance().unSubscribe(this, 'query_result');
  }

  /** Attach the listeners to the broker. */
  public subscribeToAnalyticsData(): void {
    Broker.instance().subscribe(this, 'gsa_node_result');
  }

  /** Detach the listeners to the broker. */
  public unSubscribeFromAnalyticsData(): void {
    Broker.instance().unSubscribe(this, 'gsa_node_result');
  }

  /** Consumes message from backend. */
  public consumeMessageFromBackend(message: unknown): void {
    console.error('You have to implement the method consumeMessageFromBackend!');
  }

  /** Export to PDF. */
  public exportToPDF(): void {
    console.error('exportToPDF() not implemented!');
  }

  /** Export to PNG. */
  public exportToPNG(): void {
    console.error('exportToPNG() not implemented!');
  }

  /** Sets this result visualisation in the Exporter. */
  public setThisVisAsExportable(): void {
    Exporter.instance().setCurrentResultVisualisation(this);
  }
}
