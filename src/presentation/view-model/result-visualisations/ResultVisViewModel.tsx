/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import BrokerListener from '../../../domain/entity/broker/BrokerListenerInterface';
import Exportable from '../../../domain/entity/exporter/Exportable';
import BaseViewModel from '../BaseViewModel';

export default interface ResultVisViewModel extends BaseViewModel, BrokerListener, Exportable {
  /** Subscribes the ViewModel to the broker with the query_result routingkey. */
  subscribeToQueryResult(): void;

  /** Unsubscribes the ViewModel from the broker with the query_result routingkey. */
  unSubscribeFromQueryResult(): void;

  /** Attach the listeners to the broker. */
  subscribeToAnalyticsData(): void;

  /** Detach the listeners to the broker. */
  unSubscribeFromAnalyticsData(): void;

  /** Sets this result visualisation in the Exporter. */
  setThisVisAsExportable(): void;
}
