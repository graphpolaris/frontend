//This file was made by Sivan and not by GravenvanPolaris.
import * as d3 from 'd3-v6';
import {
  Link,
  NodeLinkResultType,
} from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import {
  HivePlotData,
  HPAxisData,
  HPLinksFromToAxisData,
} from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';
import {
  AxisViewConfig,
  HivePlotsViewConfig,
  PlotViewConfig,
} from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsViewConfig';
import {
  addingVectors,
  copyVector,
  rotateVectorByAngle,
  setMagnitudeVector,
  Vector,
} from '../../../../domain/entity/utils/Position';
import { id } from '../../../view/result-visualisations/hiveplots/HivePlotsComponent';

export default class ConvertNodeLinkResult {
  public static convert(plotConfig: PlotViewConfig, result: NodeLinkResultType): HivePlotData {
    // First, assign the nodes to axis, how this is done depends on the assignAxisBy value.
    const nodesPerAxis = this.assignNodesToAxis(result, plotConfig);

    // Now that we have all the nodes per axis, we are going to determine the position of each node.
    // For this we need to determine a value for each node first.
    // Maybe we need to do some binning first. If that's an option all together.
    const attributesPerIDs = result.nodes.reduce<Record<id, Record<string, any>>>(
      (a, v) => ({ ...a, [v.id]: v.attributes }),
      {},
    );

    const nodePerAxisValues: Record<string, number>[] = nodesPerAxis.map((ids, i) =>
      this.nodesToValues(ids, attributesPerIDs, plotConfig.axis[i].orderNodesBy, result.edges),
    );

    // Properties for determining position values for nodes. These could also be config fields.
    // Warning: this is connected to the width and height the svg of the plot
    // TODO: make this as variable pass through to view so it knows, and uses these values. put this in the config file
    const centerOffset = 20;
    const drawingRadius = 250;
    const axisLength = drawingRadius - 20;

    // The number of bins to use for this plot, for each axis, for now.
    let nBins = plotConfig.maxNOfBins;
    nBins = nBins < 1 ? 200 : nBins;
    // Used for determining node position on the axis
    const degreeStep = (360 / nodePerAxisValues.length) * 0.0174533;
    const initialStartPos = { x: 0, y: centerOffset };
    // Do binning and determine the position of each node.
    const axisData: HPAxisData[] = nodePerAxisValues.map((idsToValues, i) => {
      // First, binning
      // let bins: Record<number, id[]> = {};
      const bins: id[][] = new Array(nBins).fill(0).map(() => []);
      const { min, max } = this.getMinMax(idsToValues);
      const diff = max - min;
      for (const id in idsToValues) {
        // binning
        const v = idsToValues[id] - min;
        const value = v == 0 ? 0 : Math.ceil((v / diff) * nBins) - 1;
        idsToValues[id] = value;

        if (bins[value]) bins[value].push(id);
        else bins[value] = [id];
      }

      // Now get positions and convert to a map.
      // For now, min and max are normalized per axis, this could also be a user config setting.
      // Like an text input field where the min and max are also shown, could be per axis, or for every axis same min and max.
      // Increase the difference so a node is not on exactly the tip of the axis line.
      const d = (nBins - 1) * 0.1;
      const scale = d3
        .scaleLinear()
        .domain([0 - d, nBins - 1 + d])
        .range([0, axisLength]);

      const startPos = rotateVectorByAngle(initialStartPos, -i * degreeStep);

      // Convert the bins to an array where the index is the value (along the axis), it gives the pos and ids
      const binsWithPos = bins.map((ids, i) => {
        const l = scale(i);
        const pos = addingVectors(startPos, setMagnitudeVector(copyVector(startPos), l));

        return { ids, pos };
      });

      return {
        bins: binsWithPos,
        idsToValues,
        min: 0,
        max: nBins - 1,
        startPos,
        length: axisLength,
      };
    });

    // Determine the maximum nodes with the same value over all axis.
    const maxNodesAtSamePos = Math.max(
      ...axisData.map((d) => Math.max(...d.bins.map(({ ids }) => ids.length))),
    );

    // Devide edges over from axis bins and to axis bins. Depending on the nodes' axis.
    const ls: HPLinksFromToAxisData = {};

    // Keep track of the max number of links that have the same from and to bins.
    let maxLinksAtSamePos = 0;
    result.edges.forEach((e) => {
      // Go through each edge and find if they are present in the axisData. If so get their from and to axis.
      const fromAxis = axisData.findIndex((aData) => aData.idsToValues[e.from] != undefined);
      const toAxis = axisData.findIndex((aData) => aData.idsToValues[e.to] != undefined);
      // We don't want links within the same axis.
      if (fromAxis != toAxis && fromAxis != -1 && toAxis != -1) {
        // Retrieve the from and to bin.
        const fromBin = axisData[fromAxis].idsToValues[e.from];
        const toBin = axisData[toAxis].idsToValues[e.to];

        // Check if there are already links for that axis from and to, and bin from and to combination.
        // And add the current link.
        if (!ls[fromAxis]) ls[fromAxis] = { [toAxis]: { [fromBin]: { [toBin]: [e] } } };
        else if (!ls[fromAxis][toAxis]) ls[fromAxis][toAxis] = { [fromBin]: { [toBin]: [e] } };
        else if (!ls[fromAxis][toAxis][fromBin]) ls[fromAxis][toAxis][fromBin] = { [toBin]: [e] };
        else if (!ls[fromAxis][toAxis][fromBin][toBin]) ls[fromAxis][toAxis][fromBin][toBin] = [e];
        else ls[fromAxis][toAxis][fromBin][toBin].push(e);

        // Update the max number of links with the same from and to pos.
        const nLinks = ls[fromAxis][toAxis][fromBin][toBin].length;
        maxLinksAtSamePos = nLinks > maxLinksAtSamePos ? nLinks : maxLinksAtSamePos;
      }
    });

    return {
      axis: axisData,
      links: ls,
      maxNodesAtSamePos,
      maxLinksAtSamePos,
      highLightedNodes: {},
    };
  }

  /** Assigns nodes to each axis depending on the view config for that plot. */
  private static assignNodesToAxis(
    result: NodeLinkResultType,
    plotConfig: PlotViewConfig,
  ): Set<id>[] {
    // TODO: add options for assigning by attribute, for categorical this is voor de hand liggend
    // For numerical, the user should for each axis give a range for this numerical value, x < 30, 30 <= x <= 60, x > 60, for three axis.
    // This way you can also assign by other numerical metrics of node, like Degree, Betweeness and such.
    switch (plotConfig.assignAxisBy) {
      case 'Type':
        return plotConfig.axis.map(
          (a) =>
            new Set<id>(
              result.nodes.filter((n) => n.id.split('/')[0] == a.assignmentValue).map((n) => n.id),
            ),
        );

      // case 'Betweeness':
      //   console.log('HivePlots: filter axis on node betweeness not yet implemented!');
      //   break;

      default:
        /* Connections */
        return this.assignNodesToAxisByConnections(result, plotConfig.axis);
    }
  }

  /**
   * Assigns nodes to exis by connection.
   * @param result A NodeLinkResultType object, the query result.
   * @param axis The axis to assign to, these contain the type of node for that axis
   * @returns An array of sets of node id's for each axis.
   */
  private static assignNodesToAxisByConnections(
    result: NodeLinkResultType,
    axis: AxisViewConfig[],
  ): Set<string>[] {
    const sourceNodes = new Set<id>();
    const targetNodes = new Set<id>();
    const bothNodes = new Set<id>();

    // Sort the nodes by source, target, or both using the links.
    for (const link of result.edges) {
      if (sourceNodes.has(link.to)) {
        sourceNodes.delete(link.to);
        bothNodes.add(link.to);
      } else if (!bothNodes.has(link.to)) targetNodes.add(link.to);

      if (targetNodes.has(link.from)) {
        targetNodes.delete(link.from);
        bothNodes.add(link.from);
      } else if (!bothNodes.has(link.from)) sourceNodes.add(link.from);
    }

    // Create a set for each axis.
    const nodesPerAxis = axis.map((a) => {
      switch (a.assignmentValue) {
        case 'Source':
          return sourceNodes;
        case 'Target':
          return targetNodes;
        case 'Both':
          return bothNodes;
        default:
          return new Set(
            result.nodes
              .map((n) => n.id)
              .filter((id) => !sourceNodes.has(id) && !targetNodes.has(id) && !bothNodes.has(id)),
          );
      }
    });

    return nodesPerAxis;
  }

  /**
   * Determines the values for nodes depending on the orderBy value.
   * @param nodeIDs The id's of the nodes to get a value for.
   * @param nodeAttributes The attributes of the nodes.
   * @param orderBy Can be one of `possibleOrderByValues`, otherwise assume an attribute with key `orderBy` should be used.
   * @returns A record, which holds the value for each id.
   */
  private static nodesToValues(
    nodeIDs: Set<id>,
    nodeAttributes: Record<id, Record<string, any>>,
    orderBy: string,
    resultLinks: Link[],
  ): Record<id, number> {
    let nodeValues: Record<id, number> = {};

    switch (orderBy) {
      case 'id':
        nodeIDs.forEach((id) => {
          nodeValues[id] = this.convertToNumerical(id);
        });
        break;

      case 'Degree':
        nodeValues = this.createNodeValuesByDegree(nodeIDs, resultLinks);
        break;

      case 'Closeness':
        console.log('HivePlots: order by Closenes not yet implemented!');
        break;

      case 'Clustering coefficient':
        console.log('HivePlots: order by Clustering coefficient not yet implemented!');
        break;

      default:
        // assume orderBy is an attribute key, use the attribute values
        nodeIDs.forEach((id) => {
          if (nodeAttributes[id][orderBy])
            nodeValues[id] = this.convertToNumerical(nodeAttributes[id][orderBy]);
        });
        break;
    }
    return nodeValues;
  }

  /** Gives each node a value depending on its degree. Expensive, `O(#nodes*#links)` */
  private static createNodeValuesByDegree(
    nodeIDs: Set<id>,
    resultLinks: Link[],
  ): Record<id, number> {
    const nodeValues: Record<id, number> = {};
    nodeIDs.forEach(
      (id) =>
        (nodeValues[id] = resultLinks.reduce(
          (prev, l) => (l.from == id || l.to == id ? prev + 1 : prev),
          0,
        )),
    );

    return nodeValues;
  }

  /** Returns the min and max value of a nodeValues object. */
  private static getMinMax(idsToValues: Record<id, number>): { min: number; max: number } {
    const arr = Object.values(idsToValues);
    // const max = arr.reduce((cmax, v) => v > cmax ? v : cmax, Number.MIN_VALUE);
    const min = Math.min(...arr);
    const max = Math.max(...arr);
    if (min == max) return { min: min - 1, max: max + 1 };
    return { min, max };
  }

  /** Converts a any value to a numerical value. */
  private static convertToNumerical(v: any): number {
    switch (typeof v) {
      case 'boolean':
        if (v) return 1;
        else return 0;

      case 'number':
        return v;

      case 'string':
        return this.stringToNumerical(v);

      default:
        return 0;
    }
  }
  /** Converts a string to a numerical value, alphabetically. */
  private static stringToNumerical(str: string): number {
    // TODO: for now simple implementation, could/should be more thorough
    let c = 0;
    for (let i = 0; i < str.length; i++) c += str.charCodeAt(i);
    return c;
  }

  // private static GetLinkPos(
  //   from: id,
  //   to: id,
  //   axis: HPAxisData[],
  // ): { fromPos: Vector; toPos: Vector } | undefined {
  //   let fromPos: Vector | undefined;
  //   let toPos: Vector | undefined;
  //   axis.some((a) => {
  //     // Try to find the ids in the axis nodes.
  //     // Use an else if because we don't have any use for a link on the same axis
  //     if (a.idsToValues[from] != undefined) fromPos = a.valueToPos[a.idsToValues[from]].pos;
  //     else if (a.idsToValues[to] != undefined) toPos = a.valueToPos[a.idsToValues[to]].pos;

  //     return fromPos && toPos;
  //   });

  //   if (fromPos && toPos) return { fromPos, toPos };
  //   else undefined;
  // }
}
