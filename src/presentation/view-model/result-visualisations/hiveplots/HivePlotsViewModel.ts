//This file was made by Sivan and not by GravenvanPolaris.
/* istanbul ignore file */
import {
  Node,
  NodeLinkResultType,
} from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import { HivePlotData } from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';
import {
  HivePlotsViewConfig,
  possibleAssignByValues,
} from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsViewConfig';
import ResultVisViewModel from '../ResultVisViewModel';

/**
 * HivePlotsViewModel is an interface used for the ViewModel implementation of the HivePlotsViewModelImplementation.
 * Used to define all public fields used by the view.
 *
 * Extends ResultVisViewModel, this is an interface of ViewModel holding all standard public fields and functions for a result visualisation.
 */
export default interface HivePlotsViewModel extends ResultVisViewModel {
  hivePlotsData: HivePlotData[];
  possibleAxisAssignmentValues: Record<possibleAssignByValues, string[]>;

  highLightedNodes: Node[];

  viewConfig: HivePlotsViewConfig;
  addPlot(name: string): void;
  deleteSelectedPlot(): void;
  setSelectedPlotInConfigPanel(index: number): void;
  addAxis(): void;
  deleteAxis(index: number): void;
  setAssignAxisBy(a: possibleAssignByValues): void;
  setAxisAssignmentValue(axisIndex: number, value: string): void;
  setAxisOrderNodesByValue(axisIndex: number, value: string): void;
  setMaxNOfBins(newMaxNOfBins: number): void;

  onMouseEnteredNode(plotIndex: number, axisIndex: number, binIndex: number): void;
  onMouseLeftNode(plotIndex: number, axisIndex: number, binIndex: number): void;
}
