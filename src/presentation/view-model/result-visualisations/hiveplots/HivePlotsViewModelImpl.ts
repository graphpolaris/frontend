//This file was made by Sivan and not by GravenvanPolaris.
/* istanbul ignore file */
import big2ndChamberQueryResult from '../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import {
  getNodeTypes,
  isNodeLinkResult,
  Node,
  NodeLinkResultType,
} from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import { HivePlotData } from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';
import {
  HivePlotsViewConfig,
  PlotViewConfig,
  possibleAssignByValues,
  possibleAxisAssignments,
} from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsViewConfig';
import AbstractResultVisViewModelImpl from '../AbstractResultVisViewModelImpl';
import ConvertNodeLinkResult from './ConvertNodeLinkResult';
import HivePlotsViewModel from './HivePlotsViewModel';

/** HivePlotsViewModelImpl is the implementation of the interface defined in HivePlotsViewModel. */
export default class HivePlotsViewModelImpl
  extends AbstractResultVisViewModelImpl
  implements HivePlotsViewModel
{
  private queryResult: NodeLinkResultType;
  /** The currently selected plot in the config side panel. A pointer to a plot in the viewConfig. */
  private selectedPlot: PlotViewConfig | undefined;

  public hivePlotsData: HivePlotData[];
  public viewConfig: HivePlotsViewConfig;
  public possibleAxisAssignmentValues: Record<possibleAssignByValues, string[]>;

  public highLightedNodes: Node[] = [];

  constructor() {
    super();

    this.queryResult = { nodes: [], edges: [] };
    this.possibleAxisAssignmentValues = { ...possibleAxisAssignments };

    // TODO: a default first view config should be generated from the initial result
    // this.viewConfig = {
    //   selectedPlotInConfigPanelIndex: -1,

    //   plots: [],
    // };
    this.viewConfig = demoHivePlotsViewConfig;

    this.selectedPlot = this.viewConfig.plots[this.viewConfig.selectedPlotInConfigPanelIndex];

    this.hivePlotsData = [];

    // Consume mock data
    this.consumeMessageFromBackend(big2ndChamberQueryResult);
  }

  public consumeMessageFromBackend(message: unknown): void {
    if (isNodeLinkResult(message)) {
      this.queryResult = message;

      // Get the node types, TODO: we could check the existing viewconfig if it has node type assignAxisByValues which are now not in types anymore
      this.possibleAxisAssignmentValues = {
        ...this.possibleAxisAssignmentValues,
        Type: Object.keys(getNodeTypes(message)),
      };

      this.hivePlotsData = this.viewConfig.plots.map((plotConfig) =>
        ConvertNodeLinkResult.convert(plotConfig, this.queryResult),
      );
      this.notifyViewAboutChanges();
    }
  }

  // --- Functions for changing the viewconfig --- //
  /** Adds a new plot the viewConfig, and sets it as currently selected */
  public addPlot = (name: string): void => {
    // Create a plot with default values.
    const newPlot: PlotViewConfig = {
      name: name,
      assignAxisBy: 'Connections',
      axis: [
        { assignmentValue: 'Source', orderNodesBy: 'Degree' },
        { assignmentValue: 'Target', orderNodesBy: 'Degree' },
        { assignmentValue: 'Both', orderNodesBy: 'Degree' },
      ],
      maxNOfBins: 40,
    };
    this.viewConfig.plots.unshift(newPlot);
    this.selectedPlot = newPlot;
    this.viewConfig.selectedPlotInConfigPanelIndex = 0;
    this.hivePlotsData.unshift({} as HivePlotData);
    this.onViewConfigChanged(true);
  };

  /** Deletes the currently selected plot from the viewConfig and, if any plots are left, assign the first one as the new selected. */
  public deleteSelectedPlot = (): void => {
    // Remove currently selected plot from the array
    if (this.selectedPlot) {
      this.viewConfig.plots.splice(this.viewConfig.selectedPlotInConfigPanelIndex, 1);
      this.hivePlotsData.splice(this.viewConfig.selectedPlotInConfigPanelIndex, 1);

      // Change the currently selected plot to the next one, if available
      if (this.viewConfig.plots.length > 0) {
        this.selectedPlot = this.viewConfig.plots[0];
        this.viewConfig.selectedPlotInConfigPanelIndex = 0;
      } else {
        // If no plots are left, set them to undefined.
        this.selectedPlot = undefined;
        this.viewConfig.selectedPlotInConfigPanelIndex = -1;
      }
      this.onViewConfigChanged();
    }
  };

  /** Changes the currently selected plot in the viewConfig.  */
  public setSelectedPlotInConfigPanel = (index: number): void => {
    this.viewConfig.selectedPlotInConfigPanelIndex = index;
    this.selectedPlot = this.viewConfig.plots[index];

    this.onViewConfigChanged();
  };

  /** Adds an axis to the currently selected Plot. Using an unused assignment value for the new axis. */
  public addAxis = (): void => {
    if (this.selectedPlot) {
      // Find an unused axis assignment value
      const notYetUsedAxisValue = this.possibleAxisAssignmentValues[
        this.selectedPlot.assignAxisBy
      ].find((v) => !this.selectedPlot?.axis.map((a) => a.assignmentValue).includes(v));

      // If we found one, which we should've
      if (notYetUsedAxisValue) {
        this.selectedPlot.axis.push({
          assignmentValue: notYetUsedAxisValue,
          orderNodesBy: 'Degree',
        });
      }

      this.onViewConfigChanged(true);
    }
  };

  /** Deletes the axis with index of the currenlty selected plot. */
  public deleteAxis = (index: number): void => {
    if (this.selectedPlot) {
      this.selectedPlot.axis.splice(index, 1);
      this.onViewConfigChanged(true);
    }
  };

  /** Sets the assignAxisBy value in the viewConfig of the current plot.  */
  public setAssignAxisBy = (a: possibleAssignByValues): void => {
    if (this.selectedPlot && this.selectedPlot.assignAxisBy != a) {
      this.selectedPlot.assignAxisBy = a;
      // set default axis values, with a maximum of 4 axis. Set order by default to degree.
      this.selectedPlot.axis = this.possibleAxisAssignmentValues[a]
        .slice(0, 3)
        .map((p) => ({ assignmentValue: p, orderNodesBy: 'Degree' }));

      this.onViewConfigChanged(true);
    }
  };

  /** Sets the assignmentValue of the axis at specified index in the current selected plot. */
  public setAxisAssignmentValue = (axisIndex: number, value: string): void => {
    if (this.selectedPlot && this.selectedPlot.axis[axisIndex].assignmentValue != value) {
      // Check if there already is an axis with this assign by value.
      const currentAxisWithThisValue = this.selectedPlot.axis.find(
        (a) => a.assignmentValue == value,
      );

      // If so, set this axis with the current value of the axis we want to change.
      if (currentAxisWithThisValue)
        currentAxisWithThisValue.assignmentValue =
          this.selectedPlot.axis[axisIndex].assignmentValue;

      this.selectedPlot.axis[axisIndex].assignmentValue = value;
      this.onViewConfigChanged(true);
    }
  };

  /** Sets the orderNodesBy field of the axis at specified index in the current selected plot. */
  public setAxisOrderNodesByValue = (axisIndex: number, value: string): void => {
    if (this.selectedPlot) {
      this.selectedPlot.axis[axisIndex].orderNodesBy = value;
      this.onViewConfigChanged(true);
    }
  };

  public setMaxNOfBins = (newMaxNOfBins: number): void => {
    if (this.selectedPlot && this.selectedPlot.maxNOfBins != newMaxNOfBins) {
      this.selectedPlot.maxNOfBins = newMaxNOfBins;

      this.onViewConfigChanged(true);
    }
  };

  /** Notifies the view that the viewconfig changed. */
  private onViewConfigChanged(recalculateHPData = false): void {
    // Build a new object so the ConfigPanel gets a new object (new pointer) and will rerender.
    this.viewConfig = { ...this.viewConfig };

    // Convert whole node link result after every change
    // TODO: not optimized!!! can be changed to specific usecases probably

    // Only recalculate for current plot.
    if (recalculateHPData && this.selectedPlot)
      this.hivePlotsData[this.viewConfig.selectedPlotInConfigPanelIndex] =
        ConvertNodeLinkResult.convert(this.selectedPlot, this.queryResult);
    this.notifyViewAboutChanges();
  }
  // --- end --- //

  public onMouseEnteredNode = (plotIndex: number, axisIndex: number, binIndex: number): void => {
    // Set highlighted node
    this.highLightedNodes = this.queryResult.nodes.filter((n) =>
      this.hivePlotsData[plotIndex].axis[axisIndex].bins[binIndex].ids.includes(n.id),
    );

    if (!this.hivePlotsData[plotIndex].highLightedNodes[axisIndex])
      this.hivePlotsData[plotIndex].highLightedNodes[axisIndex] = { [binIndex]: true };
    else {
      this.hivePlotsData[plotIndex].highLightedNodes[axisIndex] = {
        ...this.hivePlotsData[plotIndex].highLightedNodes[axisIndex],
      };
      this.hivePlotsData[plotIndex].highLightedNodes[axisIndex][binIndex] = true;
    }

    this.hivePlotsData[plotIndex].highLightedNodes = {
      ...this.hivePlotsData[plotIndex].highLightedNodes,
    };
    this.hivePlotsData = [...this.hivePlotsData];
    this.notifyViewAboutChanges();
  };
  public onMouseLeftNode = (plotIndex: number, axisIndex: number, binIndex: number): void => {
    // Remove the highlighted node
    this.highLightedNodes = [];

    delete this.hivePlotsData[plotIndex].highLightedNodes[axisIndex][binIndex];
    if (Object.keys(this.hivePlotsData[plotIndex].highLightedNodes[axisIndex]).length == 0)
      delete this.hivePlotsData[plotIndex].highLightedNodes[axisIndex];
    else
      this.hivePlotsData[plotIndex].highLightedNodes[axisIndex] = {
        ...this.hivePlotsData[plotIndex].highLightedNodes[axisIndex],
      };

    this.hivePlotsData[plotIndex].highLightedNodes = {
      ...this.hivePlotsData[plotIndex].highLightedNodes,
    };
    this.hivePlotsData = [...this.hivePlotsData];
    this.notifyViewAboutChanges();
  };
}

// TODO: should be auto generated to always visualize something, also when adding a new plot
const demoHivePlotsViewConfig: HivePlotsViewConfig = {
  selectedPlotInConfigPanelIndex: 0,

  plots: [
    {
      name: 'Plot A',
      assignAxisBy: 'Connections',
      axis: [
        { assignmentValue: 'Source', orderNodesBy: 'Degree' },
        { assignmentValue: 'Target', orderNodesBy: 'Degree' },
        { assignmentValue: 'Both', orderNodesBy: 'Degree' },
      ],
      maxNOfBins: 40,
    },
    {
      name: 'Plot B',
      assignAxisBy: 'Connections',
      axis: [
        { assignmentValue: 'Source', orderNodesBy: 'id' },
        { assignmentValue: 'Target', orderNodesBy: 'Degree' },
        { assignmentValue: 'Other', orderNodesBy: 'id' },
      ],
      maxNOfBins: 40,
    },
  ],
};
