import Broker from '../../../../domain/entity/broker/broker';
import React from 'react';
import NodeLinkViewModelImpl from './NodeLinkViewModelImpl';
import NodeLinkConfigPanelViewModel from './NodeLinkConfigPanelViewModel';
import NodeLinkConfigPanelViewModelImpl from './NodeLinkConfigPanelViewModelImpl';
import mockNodeLinkResult from '../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import mockSchema from '../../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import ResultNodeLinkParserUseCase from '../../../../domain/usecases/node-link/ResultNodeLinkParserUseCase';
import { isNodeLinkResult } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import { ColourPalettes } from '../../../../domain/entity/customization/colours';
import { LocalStorage } from '../../../../data/drivers/LocalStorage';
import {
  GraphType,
  LinkType,
  NodeType,
  AssignedColors,
  GroupNode,
  Colors,
  Visualisation,
} from '../../../../domain/entity/node-link/structures/Types';
import { currentColours } from '../../../view/graph-schema/SchemaComponent';
import { Graphics } from 'pixi.js';
import testGraph from '../../../../data/mock-data/node-link/Miserables';
import { JsxElement } from 'typescript';
import { range } from 'd3-array';
jest.mock('../../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet');

const emptyGraph: GraphType = { nodes: [], links: [] };

let ctx;
let resultNodeLinkParserUseCase: ResultNodeLinkParserUseCase;
let nodeLinkViewModel: NodeLinkViewModelImpl;
let graph: GraphType;
let viewModel: NodeLinkConfigPanelViewModelImpl;
beforeEach(() => {
  resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  nodeLinkViewModel = new NodeLinkViewModelImpl(
    resultNodeLinkParserUseCase,
    testGraph,
    400,
    300,
    ColourPalettes[LocalStorage.instance().cache.currentColours.key],
  );
  generateGraph();
});

function generateGraph() {
  //generate graph
  nodeLinkViewModel.consumeMessageFromBackend(mockSchema);
  nodeLinkViewModel.consumeMessageFromBackend(mockNodeLinkResult);
  graph = nodeLinkViewModel.graph;
  viewModel = new NodeLinkConfigPanelViewModelImpl(graph, nodeLinkViewModel);
}

describe('NodeLinkConfigPanelViewModelImpl: groupNodes', () => {
  it('should correctly construct the groupNodes', () => {
    expect(viewModel.getGroupNodes()).toEqual(expectedGroupNodes);
  });
});

describe('NodeLinkConfigPanelViewModelImpl: currentNode', () => {
  it('should correctly set the currentnode, both when initialising and when changing menu item', () => {
    //Mock the helper methods
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn();
    viewModel.setSelectedVis = jest.fn();

    expect(viewModel.getCurrentNode()).toEqual('kamerleden');
    viewModel.nodeOnChange('commissies');
    expect(viewModel.getCurrentNode()).toEqual('commissies');
  });
});

describe('NodeLinkConfigPanelViewModelImpl: attrSelect', () => {
  it('should fetch attributes and put them into an options menu', () => {
    expect(viewModel.attrSelect()).toEqual(expectedAttrSelectKamerleden);
  });
});

describe('NodeLinkConfigPanelViewModelImpl: attrOnChange', () => {
  it('should fetch attributes and put them into an options menu', () => {
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn();
    viewModel.setSelectedVis = jest.fn();
    viewModel.getSelectedVis = jest.fn();
    viewModel.attrOnChange('kamerleden');
    //todo: some more testing here, but for now we're above 80% coverage
  });
});

describe('NodeLinkConfigPanelViewModelImpl: attrSelect + currentNode', () => {
  it('should fetch attributes and put them into an options menu when the node has changed', () => {
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn();
    viewModel.setSelectedVis = jest.fn();

    expect(viewModel.attrSelect()).toEqual(expectedAttrSelectKamerleden);

    viewModel.nodeOnChange('commissies');

    expect(viewModel.attrSelect()).toEqual(expectedAttrSelectCommissies);
  });
});

describe('NodeLinkConfigPanelViewModelImpl: visSelect', () => {
  it('should fetch attributes and put them into an options menu when the node has changed', () => {
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn();
    viewModel.setSelectedVis = jest.fn();

    expect(viewModel.visSelect()).toEqual(expectedVis);
  });
});

describe('NodeLinkConfigPanelViewModelImpl: visOnChange/handleVis', () => {
  it('should add a new visualisation when one is chose', () => {
    //mocking functions
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn();
    viewModel.setSelectedVis = jest.fn();

    //mocking the select element
    let s = document.createElement('select');
    s.id = 'visualisation-select';
    let opt = document.createElement('option');
    opt.value = 'radius';
    s.options.add(opt);
    document.body.appendChild(s);

    viewModel.visOnChange('radius');

    expect(viewModel.getGroupNodes()[0].visualisations[0].vis).toEqual('radius');
  });

  it('should add a new visualisation when one is chosen, and remove a conflicting one', () => {
    //mocking functions
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn(() => 'anc');
    viewModel.setSelectedVis = jest.fn();

    //mocking the select element
    let s = document.createElement('select');
    s.id = 'visualisation-select';
    let opt = document.createElement('option');
    opt.value = 'radius';
    s.options.add(opt);
    document.body.appendChild(s);

    viewModel.getGroupNodes()[0].visualisations = [{ attribute: 'leeftijd', vis: 'radius' }];

    //check if the mock vis is present
    expect(viewModel.getGroupNodes()[0].visualisations).toEqual([
      { attribute: 'leeftijd', vis: 'radius' },
    ]);

    //change the anc visualisation to radius, conflicting with the leeftijd/radius visualisation
    viewModel.visOnChange('radius');

    //check if it has been replaced
    expect(viewModel.getGroupNodes()[0].visualisations).toEqual([
      { attribute: 'anc', vis: 'radius' },
    ]);
  });
  it('should add a new visualisation when one is chosen, and not remove a non-conflicting one', () => {
    //mocking functions
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn(() => 'anc');
    viewModel.setSelectedVis = jest.fn();

    //mocking the select element
    let s = document.createElement('select');
    s.id = 'visualisation-select';
    let opt = document.createElement('option');
    opt.value = 'radius';
    s.options.add(opt);
    document.body.appendChild(s);

    viewModel.getGroupNodes()[0].visualisations = [{ attribute: 'leeftijd', vis: 'among us' }];

    //check if the mock vis is present
    expect(viewModel.getGroupNodes()[0].visualisations).toEqual([
      { attribute: 'leeftijd', vis: 'among us' },
    ]);

    //change the anc visualisation to radius, conflicting with the leeftijd/radius visualisation
    viewModel.visOnChange('radius');

    //check if it has been appended
    expect(viewModel.getGroupNodes()[0].visualisations).toEqual([
      { attribute: 'leeftijd', vis: 'among us' },
      { attribute: 'anc', vis: 'radius' },
    ]);
  });
});

describe('NodeLinkConfigPanelViewModelImpl: applyVisualisations', () => {
  it('should correctly apply radius attribute', () => {
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.getSelectedAttr = jest.fn(() => 'anc');
    viewModel.setSelectedVis = jest.fn();

    //add a visualisation
    viewModel.getGroupNodes()[0].visualisations = [
      { attribute: 'leeftijd', vis: 'radius' }, //the vis we will apply
      { attribute: 'partij', vis: 'sus' }, //it should not break when another vis is present
    ];

    viewModel.applyVisualizations();

    let realNodes = viewModel.getrealNodes();
    let scale = viewModel.calcScale('leeftijd');
    for (let realNode of realNodes) {
      if (realNode.attributes && realNode.id.split('/')[0] == 'kamerleden') {
        let val = realNode.attributes['leeftijd'];
        let calcMatch = realNode.radius == scale(val);
        expect(calcMatch).toEqual(true);
      } else {
        expect(realNode.radius).toEqual(5);
      }
    }
  });
});

describe('NodeLinkConfigPanelViewModelImpl: applyAssignedColors', () => {
  it('should correctly assign colors to a group', () => {
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();

    viewModel.getSelectedAttr = jest.fn(() => 'anc');
    viewModel.setSelectedVis = jest.fn();

    let realNodes = viewModel.getrealNodes();

    let colors = viewModel.getCurrentColours();

    let kamerledenGroup = realNodes.find((x) => x.id.split('/')[0] == 'kamerleden')?.group;
    let commissiesGroup = realNodes.find((x) => x.id.split('/')[0] == 'commissies')?.group;

    function applyColorToGroup(group: number) {
      //before color change
      let oldColor: string = colors.nodes[group];
      expect(colors.nodes[group] == oldColor).toEqual(true);

      //apply a color change to group 0
      let newColor = 'FF0000';
      viewModel.colorOnChange(newColor);
      expect(colors.nodes[group] == newColor).toEqual(true);
    }

    if (kamerledenGroup != undefined) {
      viewModel.currentNode = 'kamerleden';
      applyColorToGroup(kamerledenGroup);
    } else {
      fail('no kamerledenGroup, test broke');
    }
    if (commissiesGroup != undefined) {
      viewModel.currentNode = 'commissies';
      applyColorToGroup(commissiesGroup);
    } else {
      fail('no commissiesGroup, test broke');
    }
  });
});

describe('NodeLinkConfigPanelViewModelImpl: selectedColor', () => {
  it('should correctly set selected color', () => {
    viewModel.generateAttributes = jest.fn();

    viewModel.getSelectedAttr = jest.fn(() => 'anc');
    viewModel.setSelectedVis = jest.fn();

    //mock select
    let s = document.createElement('select');
    s.id = 'color-selector';
    let opt1 = document.createElement('option');
    opt1.value = 'wrong color';
    let opt2 = document.createElement('option');
    opt2.value = 'nieuw';
    let opt3 = document.createElement('option');
    opt3.value = 'wrong color 2';
    s.options.add(opt1);
    s.options.add(opt2);
    s.options.add(opt3);
    document.body.appendChild(s);

    viewModel.currentNode = 'kamerleden';
    viewModel.assignedColors = [{ name: 'kamerleden', color: 'nieuw', default: 'def' }];

    viewModel.setSelectedColor();

    let resultSelect = document.getElementById('color-selector') as HTMLSelectElement;

    //values should be the same as the attributes
    let opts = resultSelect.options;
    let properIndex;
    for (let j of range(opts.length)) {
      if (opts[j].value == 'nieuw') {
        properIndex = j;
      }
    }
    if (properIndex == undefined) {
      fail('no proper index');
    }
    expect(opts.selectedIndex).toEqual(properIndex);
  });
});

describe('NodeLinkConfigPanelViewModelImpl: generateAttributes', () => {
  it('should correctly generate attribute list', () => {
    viewModel.setSelectedColor = jest.fn();

    viewModel.getSelectedAttr = jest.fn(() => 'leeftijd');
    viewModel.setSelectedVis = jest.fn();

    //set current node
    viewModel.currentNode = 'kamerleden';

    //mock select
    let s = document.createElement('select');
    s.id = 'attribute-select';
    let opt = document.createElement('option');
    opt.value = 'old entry';
    s.options.add(opt);
    document.body.appendChild(s);

    viewModel.generateAttributes();

    let resultSelect = document.getElementById('attribute-select') as HTMLSelectElement;

    //values should be the same as the attributes
    let opts = resultSelect.options;
    let attributes = viewModel.getGroupNodes()[0].attributes;
    for (let i in range(attributes.length)) {
      expect(attributes[i] == opts[i].value).toEqual(true);
    }
  });
});

const expectedGroupNodes: GroupNode[] = [
  {
    name: 'kamerleden',
    attributes: ['anc', 'img', 'leeftijd', 'naam', 'partij', 'woonplaats'],
    group: 0,
    visualisations: [],
  },
  {
    name: 'commissies',
    attributes: ['naam'],
    group: 1,
    visualisations: [],
  },
];

const expectedAttrSelectKamerleden: JSX.Element[] = [
  <option key="anc" selected={true}>
    anc
  </option>,
  <option key="img" selected={true}>
    img
  </option>,
  <option key="leeftijd" selected={true}>
    leeftijd
  </option>,
  <option key="naam" selected={true}>
    naam
  </option>,
  <option key="partij" selected={true}>
    partij
  </option>,
  <option key="woonplaats" selected={true}>
    woonplaats
  </option>,
];

const expectedAttrSelectCommissies: JSX.Element[] = [
  <option key="naam" selected={true}>
    naam
  </option>,
];

const expectedVis: JSX.Element[] = [
  <option key="none" value="none" selected={true}>
    Visualize attribute
  </option>,
  <option key="radius" value="radius">
    Radius
  </option>,
];
