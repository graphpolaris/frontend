/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/** This is an interface of NodeLinkConfigPanelViewModel that will be given to the NodeLinkBuilder view. */
export default interface NodeLinkConfigPanelViewModel {
  //TYPE CUSTOMISATION-----------------------------------------------------------------------------------------------------------
  /** on switching nodes in the dropdown, update variables and dropdowns.
   * @param numberNodeType numberType is the number of the new nodeType that is selected.
   */
  nodeOnChange(numberNodeType: number): void;

  /** initial generation for the node select element */
  nodeSelect(): JSX.Element;

  /**assign a newly selected color to the currentTypeNode
   * @param targetColor targetColors is the new color that is selected.
   */
  colorOnChange(targetColor: string): void;

  /** initial generation for the color select element */
  colorSelect(): JSX.Element | undefined;

  /**retrieve the current visualisation and set the vis dropdown to this, then apply said visualisation
   * @param targetAttribute targetAttribute is the new attribute that is selected.
   */
  attrOnChange(targetAttribute: string): void;

  /** initial generation for the attribute select element */
  attrSelect(): JSX.Element | undefined;

  /**handle setting a new visualisation
   * @param targetVis targetVis is the new visualisation that is selected.
   */
  visOnChange(targetVis: string): void;

  /** initial generation for the visualisation select element */
  visSelect(): JSX.Element;

  //COMMUNITY DETECTION CUSTOMISATION----------------------------------------------------------------------------------------------
  /** initial generation for the community detection cluster on select element */
  cdClusterBasedOnSelect(): JSX.Element | undefined;

  /**handle visualisations for community detection based on another categorical attribute.
   * @param targetCluster target is the categorical attribute on which the new clustering is based on.
   */
  cdClusterBasedOnOnChange(targetCluster: string): void;

  /** initial generation for the community detection value select element
   * updates when the cluster on select element has been changed.
   */
  cdValueSelect(): JSX.Element;

  /**update the currentNodeCommunityDetection to the selected value
   * and update dropdown of community detection color select element.
   * @param targetClusterValue cluster is the number which represents the selected cluster.
   */
  cdValueOnChange(targetClusterValue: number): void;

  /** initial generation for the community detection color select element */
  cdColorSelect(): JSX.Element;

  /**assign a newly selected color to the currentCommunityDetectionNode
   * @param targetColor targetColors is the new color that is selected.
   */
  cdColorOnChange(targetColor: string): void;
}
