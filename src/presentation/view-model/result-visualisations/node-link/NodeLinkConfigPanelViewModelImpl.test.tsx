/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

// FOLLOWING LINES OF 'NodeLinkConfigPanelViewModelImpl' ARE NOT TESTED:
// 425, 444, 454, 465, 483-546: HTML select tag funstions
import Broker from '../../../../domain/entity/broker/broker';
import React from 'react';
import NodeLinkViewModelImpl from './NodeLinkViewModelImpl';
import NodeLinkConfigPanelViewModel from './NodeLinkConfigPanelViewModel';
import NodeLinkConfigPanelViewModelImpl from './NodeLinkConfigPanelViewModelImpl';
import mockNodeLinkResult from '../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import mockSchema from '../../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import ResultNodeLinkParserUseCase from '../../../../domain/usecases/node-link/ResultNodeLinkParserUseCase';
import { isNodeLinkResult } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import { ColourPalettes } from '../../../../domain/entity/customization/colours';
import { LocalStorage } from '../../../../data/drivers/LocalStorage';
import {
  GraphType,
  LinkType,
  NodeType,
  AssignedColors,
  TypeNode,
  CommunityDetectionNode,
  Colors,
  Visualisation,
} from '../../../../domain/entity/node-link/structures/Types';
import { currentColours } from '../../../view/graph-schema/SchemaComponent';
import { Graphics } from 'pixi.js';
import testGraph from '../../../../data/mock-data/node-link/Miserables';
import { JsxElement } from 'typescript';
import { range } from 'd3-array';

jest.mock('../../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet');

const emptyGraph: GraphType = {
  nodes: [],
  links: [],
  linkPrediction: false,
  shortestPath: false,
  communityDetection: false,
};

let ctx;
let resultNodeLinkParserUseCase: ResultNodeLinkParserUseCase;
let nodeLinkViewModel: NodeLinkViewModelImpl;
let graph: GraphType;
let viewModel: NodeLinkConfigPanelViewModelImpl;

beforeEach(() => {
  resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  nodeLinkViewModel = new NodeLinkViewModelImpl(
    resultNodeLinkParserUseCase,
    testGraph,
    400,
    300,
    ColourPalettes[LocalStorage.instance().cache.currentColours.key],
  );
  generateGraph();
});

function generateGraph() {
  //generate graph
  nodeLinkViewModel.consumeMessageFromBackend(mockSchema);
  nodeLinkViewModel.consumeMessageFromBackend(mockNodeLinkResult);
  graph = nodeLinkViewModel.graph;
  viewModel = new NodeLinkConfigPanelViewModelImpl(graph, nodeLinkViewModel);
}

//Initialisation
describe('NodeLinkConfigPanelViewModelImpl: initialisation', () => {
  it('should insert attribute with index -1 into typeNode.attributes', () => {
    let realNodes = viewModel.getrealNodes();
    realNodes[172] = {
      id: 'kamerleden/0',
      type: 0,
      cluster: 0,
      attributes: {
        test: 'testValue',
      },
    };
    viewModel.makeNodeTypes();
    let typeNodes = viewModel.getTypeNodes();
    expect(typeNodes[0].attributes[6]).toEqual('test');
  });

  it('should return boolean that indicates whether currentNodeType is set', () => {
    expect(viewModel.nodeIsSet()).toEqual(true);
    viewModel.setCurrentNodeType(-1);
    expect(viewModel.nodeIsSet()).toEqual(false);
  });

  it('should set the currentNodeType when node is not set', () => {
    viewModel.nodeIsSet = jest.fn(() => false);
    viewModel.makeNodeTypes();
    let received = viewModel.getCurrentNodeType();
    let expectedTypeNodes = viewModel.getTypeNodes();
    let expected = expectedTypeNodes[0].type;
    expect(received).toEqual(expected);
  });

  it('should correctly initialize the colors for different node collections based on type', () => {
    expect(viewModel.assignedColorsTypes).toEqual(expectedAssignedColorsTypes);
  });

  it('should correctly set the new selected color in assignedColorsTypes', () => {
    expect(viewModel.assignedColorsTypes.find((x) => x.collection == 2)?.color).toEqual('Default');
    viewModel.setCurrentNodeType(2);
    viewModel.colorOnChange('ff0000');
    expect(viewModel.assignedColorsTypes.find((x) => x.collection == 2)?.color).toEqual('ff0000');
  });

  it('should correctly set the new selected color in currentColors', () => {
    let currentColors = viewModel.getCurrentColours();
    expect(currentColors.nodes[2]).toEqual('ff0000');
    viewModel.setCurrentNodeType(2);
    viewModel.colorOnChange('00ff00');
    let updatedCurrentColors = viewModel.getCurrentColours();
    expect(updatedCurrentColors.nodes[2]).toEqual('00ff00');
    viewModel.colorOnChange('Default');
  });
});

//Apply visualisations
describe('NodeLinkConfigPanelViewModelImpl: applyVisualisations', () => {
  it('should correctly apply radius attribute', () => {
    //add a visualisation
    viewModel.getTypeNodes()[0].visualisations = [
      { attribute: 'leeftijd', vis: 'radius' }, //the vis we will apply
      { attribute: 'partij', vis: 'sus' }, //it should not break when another vis is present
    ];

    viewModel.applyVisualizations();

    let realNodes = viewModel.getrealNodes();
    let scale = viewModel.calcScale('leeftijd');
    for (let realNode of realNodes) {
      if (realNode.attributes && realNode.id.split('/')[0] == 'kamerleden') {
        let val = realNode.attributes['leeftijd'];
        let calcMatch = realNode.radius == scale(val);
        expect(calcMatch).toEqual(true);
      } else {
        expect(realNode.radius).toEqual(5);
      }
    }
  });
});

describe('NodeLinkConfigPanelViewModelImpl: applyAssignedColors', () => {
  it('should correctly assign colors to a group', () => {
    let realNodes = viewModel.getrealNodes();
    let colors = viewModel.getCurrentColours();
    let kamerledenType = realNodes.find((x) => x.id.split('/')[0] == 'kamerleden')?.type;
    let commissiesType = realNodes.find((x) => x.id.split('/')[0] == 'commissies')?.type;

    function applyColorToType(type: number) {
      //before color change
      let oldColor: string = colors.nodes[type];
      expect(colors.nodes[type] == oldColor).toEqual(true);

      //apply a color change to group 0
      let newColor = 'FF0000';
      viewModel.colorOnChange(newColor);
      expect(colors.nodes[type] == newColor).toEqual(true);
      viewModel.colorOnChange(oldColor);
    }

    if (kamerledenType != undefined) {
      viewModel.setCurrentNodeType(1); // 'kamerleden'
      applyColorToType(kamerledenType);
    } else {
      fail('no kamerledenGroup, test broke');
    }
    if (commissiesType != undefined) {
      viewModel.setCurrentNodeType(2); // 'comissies'
      applyColorToType(commissiesType);
    } else {
      fail('no commissiesGroup, test broke');
    }
  });
});

//DOM manipulation
describe('NodeLinkConfigPanelViewModelImpl: generateAttributes', () => {
  it('should correctly generate attribute list', () => {
    viewModel.setSelectedColor = jest.fn();
    viewModel.getSelectedAttr = jest.fn(() => 'leeftijd');
    viewModel.setSelectedVis = jest.fn();

    //set current node
    viewModel.setCurrentNodeType(1); // 'kamerleden'

    //mock select
    let select = document.createElement('select');
    select.id = 'attribute-select';
    let options = document.createElement('option');
    options.value = 'old entry';
    select.options.add(options);
    document.body.appendChild(select);

    viewModel.generateAttributes();

    let resultSelect = document.getElementById('attribute-select') as HTMLSelectElement;

    //values should be the same as the attributes
    let opts = resultSelect.options;
    let attributes = viewModel.getTypeNodes()[0].attributes;
    for (let i in range(attributes.length)) {
      expect(attributes[i] == opts[i].value).toEqual(true);
    }
  });
});

//Generators (selects)

//Getters
describe('NodeLinkConfigViewModelImpl: get functions', () => {
  it('should correctly return the currentTypeNode, both when initialising and when changing menu item', () => {
    expect(viewModel.getCurrentNodeType()).toEqual(1);
    viewModel.setCurrentNodeType(2);
    expect(viewModel.getCurrentNodeType()).toEqual(2);
  });

  it('should correctly return the typeNodes', () => {
    expect(viewModel.getTypeNodes()).toEqual(expectedTypeNodes);
  });

  it('should correctly return the realNodes', () => {
    let nodes = viewModel.getrealNodes();
    expect(nodes.length).toEqual(172);
    expect(nodes[0].type).toEqual(1);
    expect(nodes[42].id).toEqual('kamerleden/108');
  });

  it('should correctly return currentColours', () => {
    let currentColours = viewModel.getCurrentColours();
    expect(currentColours.nodes[0]).toEqual('181520');
    expect(currentColours.nodes[3]).toEqual('d56a50');
  });

  it('should correctly return the selected attribute', () => {
    viewModel.generateAttributes();
    let select = document.getElementById('attribute-select') as HTMLSelectElement;
    //check if undefined
    if (select) {
      select.options.selectedIndex = 0;
      expect(viewModel.getSelectedAttr()).toEqual('anc');
      select.options.selectedIndex = 3;
      expect(viewModel.getSelectedAttr()).toEqual('naam');
      select.options.selectedIndex = 0;
    }
  });

  it('should correctly return the selected visualisation', () => {
    let select = document.createElement('select');
    select.id = 'visualisation-select';
    let option1 = document.createElement('option');
    option1.value = 'visualize attribute';
    let option2 = document.createElement('option');
    option2.value = 'radius';
    select.options.add(option1);
    select.options.add(option2);
    document.body.appendChild(select);
    //let select = document.getElementById('visualisation-select') as HTMLSelectElement;
    //check if undefined
    if (select) {
      select.options.selectedIndex = 0;
      expect(viewModel.getSelectedVis()).toEqual('visualize attribute');
      select.options.selectedIndex = 1;
      expect(viewModel.getSelectedVis()).toEqual('radius');
      select.options.selectedIndex = 0;
    }
    viewModel.removeAllOptions(select);
  });
});

//Setters
describe('NodeLinkConfigPanelViewModelImpl: setters', () => {
  it('should set the visualisation select element to the correct index', () => {
    //getElementById because the element is already created (line270)
    let select = document.getElementById('visualisation-select') as HTMLSelectElement;
    let option1 = document.createElement('option');
    option1.value = 'visualize attribute';
    let option2 = document.createElement('option');
    option2.value = 'radius';
    select.options.add(option1);
    select.options.add(option2);
    document.body.appendChild(select);

    viewModel.setCurrentNodeType(1);
    viewModel.getTypeNodes()[0].visualisations = [
      { attribute: 'anc', vis: 'radius' },
      { attribute: 'img', vis: 'visualize attribute' },
      { attribute: 'leeftijd', vis: 'radius' },
    ];

    viewModel.setSelectedVis('anc');
    expect(select.options.selectedIndex).toEqual(1);
    viewModel.setSelectedVis('img');
    expect(select.options.selectedIndex).toEqual(0);
    viewModel.setSelectedVis('leeftijd');
    expect(select.options.selectedIndex).toEqual(1);
    viewModel.setSelectedVis('test');
    expect(select.options.selectedIndex).toEqual(0);
    viewModel.removeAllOptions(select);
  });

  it('should correctly set the color select element to the correct index', () => {
    //mock select
    let select = document.createElement('select');
    select.id = 'color-select';
    let option1 = document.createElement('option');
    option1.value = 'wrong color';
    let option2 = document.createElement('option');
    option2.value = 'nieuw';
    let option3 = document.createElement('option');
    option3.value = 'wrong color 2';
    select.options.add(option1);
    select.options.add(option2);
    select.options.add(option3);
    document.body.appendChild(select);

    viewModel.setCurrentNodeType(1); // 'kamerleden'
    viewModel.assignedColorsTypes = [{ collection: 1, color: 'nieuw', default: 'def' }];
    viewModel.setSelectedColor();

    let resultSelect = document.getElementById('color-select') as HTMLSelectElement;

    //values should be the same as the attributes
    let opts = resultSelect.options;
    let properIndex;
    for (let j of range(opts.length)) {
      if (opts[j].value == 'nieuw') {
        properIndex = j;
      }
    }
    if (properIndex == undefined) {
      fail('no proper index');
    }
    expect(resultSelect.selectedIndex).toEqual(properIndex);
    viewModel.removeAllOptions(select);
  });
});

//OnChange
describe('NodeLinkConfigPanelViewModelImpl: onChange functions', () => {
  it('should correctly set the currentNodeType', () => {
    viewModel.setSelectedColor = jest.fn();
    viewModel.generateAttributes = jest.fn();
    viewModel.setSelectedVis = jest.fn();
    viewModel.applyVisualizations = jest.fn();
    viewModel.nodeOnChange(2);
    let currentNode = viewModel.getCurrentNodeType();
    expect(currentNode).toEqual(2);
  });

  it('should add a new visualisation when one is chosen', () => {
    viewModel.applyVisualizations = jest.fn();
    viewModel.getSelectedAttr = jest.fn(() => 'leeftijd');
    viewModel.getSelectedVis = jest.fn(() => 'radius');

    viewModel.visOnChange('radius');
    expect(viewModel.getTypeNodes()[0].visualisations[0].vis).toEqual('radius');
  });

  it('should add a new visualisation when one is chosen, and remove a conflicting one', () => {
    viewModel.getTypeNodes()[0].visualisations = [{ attribute: 'leeftijd', vis: 'radius' }];
    //check if the mock vis is present
    expect(viewModel.getTypeNodes()[0].visualisations).toEqual([
      { attribute: 'leeftijd', vis: 'radius' },
    ]);

    //change the attribute to anc visualisation to radius, conflicting with the leeftijd/radius visualisation
    viewModel.getSelectedAttr = jest.fn(() => 'anc');
    viewModel.getSelectedVis = jest.fn(() => 'radius');
    viewModel.visOnChange('radius');

    //check if it has been replaced
    expect(viewModel.getTypeNodes()[0].visualisations).toEqual([
      { attribute: 'anc', vis: 'radius' },
    ]);
  });

  it('should add a new visualisation when one is chosen, and not remove a non-conflicting one', () => {
    viewModel.getTypeNodes()[0].visualisations = [{ attribute: 'leeftijd', vis: 'among us' }];
    //check if the mock vis is present
    expect(viewModel.getTypeNodes()[0].visualisations).toEqual([
      { attribute: 'leeftijd', vis: 'among us' },
    ]);

    //change the anc visualisation to radius, conflicting with the leeftijd/radius visualisation
    viewModel.getSelectedAttr = jest.fn(() => 'anc');
    viewModel.getSelectedVis = jest.fn(() => 'radius');
    viewModel.visOnChange('radius');

    //check if it has been appended
    expect(viewModel.getTypeNodes()[0].visualisations).toEqual([
      { attribute: 'leeftijd', vis: 'among us' },
      { attribute: 'anc', vis: 'radius' },
    ]);
  });

  it('should confirm that selected attribute is already listed in visualisations', () => {
    viewModel.getSelectedAttr = jest.fn(() => 'leeftijd');
    viewModel.getSelectedVis = jest.fn(() => 'visualize attribute');
    viewModel.getTypeNodes()[0].visualisations = [{ attribute: 'leeftijd', vis: 'radius' }];
    viewModel.handleVis();
    let updatedVisualisations = viewModel.getTypeNodes()[0].visualisations;
    expect(updatedVisualisations).toEqual([{ attribute: 'leeftijd', vis: 'visualize attribute' }]);
  });

  it('should add "none" to the typeNode.visualisations', () => {
    viewModel.getSelectedAttr = jest.fn(() => 'leeftijd');
    viewModel.getSelectedVis = jest.fn(() => 'none');
    viewModel.getTypeNodes()[0].visualisations = [{ attribute: 'leeftijd', vis: 'radius' }];
    viewModel.handleVis();
    let updatedVisualisations = viewModel.getTypeNodes()[0].visualisations;
    expect(updatedVisualisations).toEqual([{ attribute: 'leeftijd', vis: 'none' }]);
  });

  it('should correctly set the index of the visualisation select according to the attribute', () => {
    let select = document.getElementById('visualisation-select') as HTMLSelectElement;
    let option1 = document.createElement('option');
    option1.value = 'visualize attribute';
    let option2 = document.createElement('option');
    option2.value = 'radius';
    select.options.add(option1);
    select.options.add(option2);
    document.body.appendChild(select);

    viewModel.setCurrentNodeType(1);
    viewModel.getTypeNodes()[0].visualisations = [
      { attribute: 'anc', vis: 'radius' },
      { attribute: 'img', vis: 'visualize attribute' },
      { attribute: 'leeftijd', vis: 'radius' },
    ];
    viewModel.attrOnChange('leeftijd');
    expect(select.options.selectedIndex).toEqual(1);
  });
});

// Helper funcitons
describe('NodeLinkConfigViewModelImpl: helper functions', () => {
  //mock the select element
  let select = document.createElement('select');
  select.id = 'helper-select-mock';
  it('should remove all options of a select element', () => {
    let option1 = document.createElement('option');
    select.options.add(option1);
    let option2 = document.createElement('option');
    select.options.add(option2);
    let option3 = document.createElement('option');
    select.options.add(option3);
    expect(select.options.length).toEqual(3);
    viewModel.removeAllOptions(select);
    expect(select.options.length).toEqual(0);
  });

  it('should add all values of the array to the select element', () => {
    viewModel.addPossibleValuesToSelect(select, ['uno', 'dos', 'tres', 'cuatro']);
    expect(select.options.length).toEqual(4);
    expect(select.options[2].value).toEqual('tres');
    viewModel.addPossibleValuesToSelect(select, ['alpha', 'beta', 'gamma']);
    expect(select.options.length).toEqual(3);
    expect(select.options[0].value).toEqual('alpha');
  });
});

const expectedTypeNodes: TypeNode[] = [
  {
    name: 'kamerleden',
    attributes: ['anc', 'img', 'leeftijd', 'naam', 'partij', 'woonplaats'],
    type: 1,
    visualisations: [],
  },
  {
    name: 'commissies',
    attributes: ['naam'],
    type: 2,
    visualisations: [],
  },
];

const expectedAssignedColorsTypes: AssignedColors[] = [
  {
    collection: 1,
    color: 'Default',
    default: 'd49350',
  },
  {
    collection: 2,
    color: 'Default',
    default: '1e9797',
  },
];
