/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  GraphType,
  LinkType,
  NodeType,
  AssignedColors,
  Colors,
  TypeNode,
  CommunityDetectionNode,
} from '../../../../domain/entity/node-link/structures/Types';
import NodeLinkViewModel from './NodeLinkViewModel';
import { map, range } from 'd3-array';
import React, { useRef, useState } from 'react';
import { ThumbUpSharp } from '@material-ui/icons';
import { group } from 'console';
import { values } from 'd3-v4';
export default class NodeLinkConfigPanelViewModelImpl implements NodeLinkConfigPanelViewModelImpl {
  //VARIABLES---------------------------------------------------------------------------------------------------------------
  private graph: GraphType;
  private currentColours: any;
  public nodeLinkViewModel: NodeLinkViewModel;
  private realNodes: NodeType[];
  private unEditedNodes: NodeType[];
  private typeNodes: TypeNode[] = [];
  public communityDetectionNodes: CommunityDetectionNode[] = [];
  public assignedColorsTypes: AssignedColors[] = [];
  public assignedColorsCommunityDetection: AssignedColors[] = [];
  private colors: Colors[] = [
    {
      name: 'Default',
    },
    {
      name: 'FF0000',
    },
    {
      name: '0083FF',
    },
    {
      name: 'FF00F7',
    },
  ];

  public currentNodeType = 0;
  public currentNodeCommunityDetection = 0;

  public getCurrentNodeType(): number {
    return this.currentNodeType;
  }
  public getCurrentNodeCommunityDetection(): number {
    return this.currentNodeCommunityDetection;
  }
  public setCurrentNodeType(num: number): void {
    this.currentNodeType = num;
  }
  public setCurrentNodeCommunityDetection(num: number): void {
    this.currentNodeCommunityDetection = num;
  }
  public getTypeNodes(): TypeNode[] {
    return this.typeNodes;
  }
  public getCommunityDetectionNodes(): CommunityDetectionNode[] {
    return this.communityDetectionNodes;
  }
  public getrealNodes() {
    return this.realNodes;
  }
  public getCurrentColours() {
    return this.currentColours;
  }

  public constructor(graph: GraphType, nodeLinkViewModel: NodeLinkViewModel) {
    this.graph = graph;
    this.currentColours = nodeLinkViewModel.currentColours;
    this.nodeLinkViewModel = nodeLinkViewModel;
    this.realNodes = graph.nodes;
    this.unEditedNodes = graph.nodes;
    this.init();
  }

  //INITIALISATION----------------------------------------------------------------------------------------------------------
  //initialise the visualisation

  /**init initializes the visualisation by calling the following functions;
   * makeNodeType: translates the NodeTypes received from the NodeLinkViewModel into collectionNodes (typeNode),
   * applyVisualizations: takes the stored Visualtion objects and applies them to the screen, using helper functions,
   * initializeAssignedColorsTypes: initializeAssignedColorsTypes initializes the AssignedColors for the typeNodes.
   */
  init() {
    this.makeNodeTypes();
    this.applyVisualizations();
    this.initializeAssignedColorsTypes();
    this.makeNodeCommunityDetection();
    this.initializeAssignedColorsCommunityDetection();
  }

  /**makeNodeTypes translates the NodeTypes received from the NodeLinkViewModel into collectionNodes (typeNode)*/
  makeNodeTypes() {
    this.typeNodes = [];
    for (let entry of this.realNodes) {
      let entryType = entry.id.split('/')[0];
      let node = this.typeNodes.find((x) => x.name == entryType);
      if (node) {
        //if node is already logged
        //then insert any unlogged attribute names
        //loop over realNode attribute names not stored yet, push them on the typeNode
        for (let attr in entry.attributes) {
          if (node.attributes.indexOf(attr) == -1) {
            node.attributes.push(attr);
          }
        }
      } else {
        //if node not in typeNodes yet
        //push all attributes on the typeNode
        let newattributes = [];
        for (let attr in entry.attributes) {
          newattributes.push(attr);
        }
        this.typeNodes.push({
          name: entryType,
          attributes: newattributes,
          type: entry.type,
          visualisations: [],
        });
      }
    }
    //this makes sure we don't get a null reference/ not found when currentnode is not set yet
    if (!this.nodeIsSet() && this.typeNodes[0]) {
      if (this.typeNodes[0].type) {
        this.setCurrentNodeType(this.typeNodes[0].type);
      }
    }
  }

  /**to prevent uninitialised errors when no diagram is present */
  nodeIsSet() {
    for (let i in range(this.typeNodes.length)) {
      let n = this.typeNodes[i as any];
      if (this.currentNodeType == n.type) {
        return true;
      }
    }
    return false;
  }

  /**makeNodeCommunityDetection translates the nodes from the NodeLinkViewModel into communityDetectionNodes*/
  makeNodeCommunityDetection() {
    let numberOfClusters = this.graph.numberOfMlClusters;
    if (numberOfClusters) {
      for (let i = 0; i < numberOfClusters; i++) {
        this.communityDetectionNodes.push({ cluster: i + 1 });
      }
    }
  }

  /**initializeAssignedColorsTypes initializes the AssignedColors for the typeNodes*/
  initializeAssignedColorsTypes() {
    this.assignedColorsTypes = [];
    for (let j in range(this.typeNodes.length)) {
      let type = this.typeNodes[j as any].type;
      let defaultcolor = '000000';
      //check if both currentColours and type are not undefined
      if (this.currentColours && type) {
        defaultcolor = this.currentColours.nodes[type];
      }

      this.assignedColorsTypes.push({
        collection: this.typeNodes[j].type,
        color: 'Default',
        default: defaultcolor,
      });
    }
  }

  /**initializeAssignedColorsCommunityDetection initializes the AssignedColors for the communityDetectionNodes*/
  initializeAssignedColorsCommunityDetection() {
    this.assignedColorsCommunityDetection = [];
    for (let j in range(this.communityDetectionNodes.length)) {
      let cluster = this.communityDetectionNodes[j].cluster;
      let defaultcolor = '000000';
      if (this.currentColours) {
        defaultcolor = this.currentColours.nodes[cluster];
      }

      this.assignedColorsCommunityDetection.push({
        collection: this.communityDetectionNodes[j].cluster,
        color: 'Default',
        default: defaultcolor,
      });
    }
  }

  //APPLYING VISUALISATIONS-------------------------------------------------------------------------------------------------
  /**handleVis takes all the currently said menu options and generates a new Visualisation, placing it in the correct typeNode */
  handleVis() {
    //retrieve the currently selected attribute (so not from memory)
    let attr = this.getSelectedAttr();
    //get the currently set Vis
    let vis = this.getSelectedVis();
    //Set visualistation to the selected attribute and visualisation method
    let node = this.typeNodes.find((x) => x.type == this.currentNodeType);
    if (node) {
      let newVis = { attribute: attr, vis: vis };
      if (vis != 'none') {
        let currentVis = node.visualisations.find((x) => x.attribute == attr);
        //check if this attr is already visualised
        if (currentVis) {
          let index = node.visualisations.indexOf(currentVis);
          node.visualisations[index] = newVis;
        }

        let conflictingVis = node.visualisations.find((x) => x.vis == vis);
        //check if this visualisation is already in use (for combining multiple visualisations per nodetype)
        if (conflictingVis) {
          let index = node.visualisations.indexOf(conflictingVis);
          node.visualisations.splice(index, 1);
          node.visualisations.push(newVis);
        }
        //else a new visualisation
        else {
          node.visualisations.push(newVis);
        }
      } else {
        let current = node.visualisations.find((x) => x.attribute == attr);
        if (current) {
          let index = node.visualisations.indexOf(current);
          node.visualisations[index] = newVis;
        }
      }
    }
    this.applyVisualizations();
  }

  /**applyVisualizations takes the stored Visualtion objects and applies them to the screen, using helper functions */
  applyVisualizations() {
    //remove all currently made changes to the NodeType[] nodes
    this.resetRadiusNodes();

    //iterate over typeNodes
    for (let i in range(this.typeNodes.length)) {
      let visualisations = this.typeNodes[i].visualisations;
      let nodeType = this.typeNodes[i].name;
      if (visualisations) {
        for (let i in range(visualisations.length)) {
          let vis = visualisations[i];
          let attr = vis.attribute;
          let attrVal = 1;

          //todo check attrVal for real and make sure vis is compatible with said value
          // if number visualization
          if (typeof attrVal === 'number') {
            this.applyNumericalAttribute(nodeType, attr, vis.vis);
          }
          // other visualisations
          else {
            //todo implement some other visualisations
          }
        }
      }
    }
    this.nodeLinkViewModel.UpdateRadius(
      this.graph,
      //this.nodeLinkViewModel.graph,
      this.nodeLinkViewModel.radius,
    );
  }

  /**applyNumericalAttributes handles all numerical related visualisations like age, seniority, seats, amounts etc*/
  applyNumericalAttribute(nodetype: string, attr: string, vis: string) {
    //scaling function
    let scale = this.calcScale(attr);

    let localNode = this.typeNodes.find((x) => x.name == nodetype);
    if (localNode) {
      //all realNodes that belong to this collection
      let realNodes =
        this.realNodes.filter((x) => x.id.split('/')[0] == localNode?.name) || this.typeNodes[0];
      for (let j in range(realNodes.length)) {
        let realNode = realNodes[j];
        if (vis == 'radius') {
          this.applyRadiusAttr(attr, realNode, scale);
        }
      }
    }
    this.nodeLinkViewModel.UpdateRadius(this.graph, 5);
  }

  /**applyRadiusAttr gives nodes a radius based on their value. Expects a scaling function*/
  applyRadiusAttr(attr: string, realNode: NodeType, scale: (x: number) => number) {
    let scaleAuto = scale;

    if (realNode.attributes != undefined) {
      let attributeValue = realNode.attributes[attr];
      if (attributeValue) {
        realNode.radius = scaleAuto(attributeValue);
      }
    }
  }
  /**calScale is a higher order helper function for applyRadiusAttr/applyNumericalattribute, which returns a linear scaling function */
  calcScale(attr: string): (x: number) => number {
    //list all values
    let values: number[] = [];
    this.realNodes.forEach((node) => {
      if (node.attributes) {
        if (node.attributes[attr] != undefined) {
          values.push(node.attributes[attr]);
        }
      }
    });
    //pixel sizes for the nodes
    let maxP = 10;
    let minP = 5;
    //value min/max
    let minX = Math.min(...values);
    let maxX = Math.max(...values);
    let a = (maxP - minP) / (maxX - minX);
    let b = maxP - a * maxX;
    //linear scaling between minP and maxP (based on y = ax + b)
    return (x: number) => a * x + b;
  }

  /**applyAssignedColors assigns the chosen colors to the assignedColors(Types/CommunityDetection) holders,
   * which then updates the currentColors.
   * @param colorsBasedOn colorBasedOn is a string which indicates which assignedColors(Types/CommunityDetection) needs to be changed.
   */
  applyAssignedColors(colorsBasedOn: string) {
    let newNodeColors = this.currentColours.nodes; //holder variable for new node settings
    //different implementation for which assignedColors (Types/CommunityDetection) needs to be changed
    switch (colorsBasedOn) {
      case 'type': {
        for (let j in range(this.assignedColorsTypes.length)) {
          let node = this.assignedColorsTypes[j as any];
          let node1 = this.typeNodes.find((x) => x.type == node.collection) || this.typeNodes[0]; //find the node in MockNodes that matches this Assigned color entry
          let type = node1.type; //find the type it belongs to
          //check if current colours and type are initialised, then apply to storage
          if (this.currentColours.nodes && type) {
            if (node?.color != 'Default') {
              newNodeColors[type] = node?.color; //assign the color to the newNodeColors storage
            } else {
              newNodeColors[type] = node.default;
            }
          }
        }
        break;
      }
      case 'communityDetection': {
        for (let j in range(this.assignedColorsCommunityDetection.length)) {
          let node = this.assignedColorsCommunityDetection[j as any];
          let node1 =
            this.communityDetectionNodes.find((x) => x.cluster == node.collection)?.cluster ||
            this.communityDetectionNodes[0].cluster; //find the node in MockNodes that matches this Assigned color entry
          let cluster = node1; //find the cluster it belongs to
          //check if current colours is initialised, then apply to storage
          if (this.currentColours.nodes) {
            if (node?.color != 'Default') {
              newNodeColors[cluster] = node?.color; //assign the color to the newNodeColors storage
            } else {
              newNodeColors[cluster] = node.default;
            }
          }
        }
        break;
      }
      default: {
      }
    }
    //apply the stored colors
    this.currentColours.nodes = newNodeColors;
    this.nodeLinkViewModel.currentColours = this.currentColours;
    this.nodeLinkViewModel.UpdateColours(this.graph, 5);
  }

  /**resetRadiusNodes resets the radius of all the nodes to the standardRadius (this.nodeLinkViewModel.radius).*/
  resetRadiusNodes() {
    let standardRadius = this.nodeLinkViewModel.radius;
    for (let i in range(this.realNodes.length)) {
      this.realNodes[i].radius = standardRadius;
    }
  }

  /**use this when deep copy is fixed, for now use the other one*/
  // resetNodesWIP() {
  //   throw new Error('warning not implemented');
  //   this.graph.nodes = this.unEditedNodes;
  //   this.realNodes = this.graph.nodes;
  // }

  /** initializeMlAttributeValues gets the number of clusters from the NodeLinkViewModel and creates an string array of the different clusters. */
  initializeMlAttributeValues(): void {
    let mldata = [];
    let numberOfMlClusters = this.graph.numberOfMlClusters;
    // check if not undefined
    if (numberOfMlClusters) {
      for (let index = 0; index < numberOfMlClusters; index++) mldata[index] = String(index + 1);
    }
    this.nodeLinkViewModel.uniqueAttributeValues['mldata'] = mldata;
  }

  //DOM MANIPULATION--------------------------------------------------------------------------------------------------------
  /** fill the attribute list with the correct values for the node */
  generateAttributes() {
    const attributes = this.typeNodes.find((x) => x.type == this.currentNodeType)?.attributes;
    const sel = document.getElementById('attribute-select') as HTMLSelectElement;
    let opts = sel.options;
    for (let i in range(opts.length)) {
      opts.remove(0);
    }
    if (attributes) {
      for (let j in range(attributes.length)) {
        let newOption = document.createElement('option');
        newOption.value = attributes[j];
        newOption.text = attributes[j];
        opts.add(newOption);
      }
    }
  }

  //GENERATORS--------------------------------------------------------------------------------------------------------------
  /** initial generation for the node select element */
  nodeSelect() {
    return (
      <select onChange={(e) => this.nodeOnChange(parseInt(e.target.value))} defaultValue="">
        <option value="" disabled>
          Node Type
        </option>
        {this.typeNodes.map((node) => (
          <option key={node.name} value={node.type}>
            {node.name}
          </option>
        ))}
      </select>
    );
  }

  /** initial generation for the attribute select element */
  attrSelect() {
    return (
      <select
        id="attribute-select"
        onChange={(e) => {
          this.attrOnChange(e.target.value);
        }}
        defaultValue=""
      >
        <option value="" key="" disabled>
          Attributes
        </option>
        {this.typeNodes
          .find((node) => node.type == this.currentNodeType)
          ?.attributes.map((attributes) => (
            <option key={attributes}>{attributes}</option>
          ))}
      </select>
    );
  }
  /** initial generation for the color select element */
  colorSelect() {
    return (
      <select
        id="color-select"
        onChange={(e) => {
          this.colorOnChange(e.target.value);
        }}
        defaultValue="Default"
      >
        {this.colors.map((color) => (
          <option key={color.name} value={color.name}>
            {color.name}
          </option>
        ))}
      </select>
    );
  }

  /** initial generation for the visualisation select element */
  visSelect() {
    return (
      <select
        id="visualisation-select"
        onChange={(e) => this.visOnChange(e.target.value)}
        defaultValue="Visualize attribute"
      >
        <option key="none" value="none">
          Visualize attribute
        </option>
        <option key="radius" value="radius">
          Radius
        </option>
      </select>
    );
  }

  /** initial generation for the community detection cluster on select element */
  cdClusterBasedOnSelect() {
    let uniqueValues = this.getUniqueAttributes();
    return (
      <select
        // cd = community detection
        id="cd-cluster-select"
        onChange={(e) => this.cdClusterBasedOnOnChange(e.target.value)}
        defaultValue=""
      >
        {uniqueValues.map((attr) => (
          <option key={attr} value={attr}>
            {attr}
          </option>
        ))}
      </select>
    );
  }

  /** initial generation for the community detection value select element
   * updates when the cluster on select element has been changed.
   */
  cdValueSelect() {
    this.initializeMlAttributeValues();
    return (
      <select
        id="cd-value-select"
        // selectedIndex + 1 so you dont get 0, which is interpreted as 'undefined'
        onChange={(e) => this.cdValueOnChange(e.target.selectedIndex + 1)}
      >
        {this.nodeLinkViewModel.uniqueAttributeValues['mldata'].map((attrValue) => (
          <option key={attrValue} value={attrValue}>
            {attrValue}
          </option>
        ))}
      </select>
    );
  }

  /** initial generation for the community detection color select element */
  cdColorSelect() {
    return (
      <select
        id="cd-color-select"
        onChange={(e) => {
          this.cdColorOnChange(e.target.value);
        }}
        defaultValue="Default"
      >
        {this.colors.map((color) => (
          <option key={color.name} value={color.name}>
            {color.name}
          </option>
        ))}
      </select>
    );
  }

  //GETTERS---------------------------------------------------------------------------------------------------------------------
  /** returns value of selected attribute of attribute select element */
  getSelectedAttr(): string {
    const sel = document.getElementById('attribute-select') as HTMLSelectElement;
    return sel.value;
  }
  /** returns value of selected visualisation of visualisation select element */
  getSelectedVis(): string {
    const sel = document.getElementById('visualisation-select') as HTMLSelectElement;
    return sel.value;
  }
  /** returns value of selected cluster of community detection cluster on select element */
  getSelectedClusterBasedOn(): any {
    const select = document.getElementById('cd-cluster-select') as HTMLSelectElement;
    return select.value;
  }
  /** returns unique values of the selected cluster of community detection value select element */
  getValuesOfSelectedCluster(): any[] {
    const select = document.getElementById('cd-value-select') as HTMLSelectElement;
    const options = select.options;
    let values: any[] = [];
    for (let index in range(options.length)) {
      values[index] = options[index].value;
    }
    return values;
  }
  /** returns unique categorical attributes */
  getUniqueAttributes(): string[] {
    let uniqueAttributes = Object.keys(this.nodeLinkViewModel.uniqueAttributeValues);
    // delete 'mldata'.
    uniqueAttributes.forEach((attribute, index) => {
      if (attribute == 'mldata') uniqueAttributes.splice(index, 1);
    });
    // add 'mldata' to the front, so it appears immediately after sends query with community detection.
    let mldata = ['mldata'];
    return mldata.concat(uniqueAttributes);
  }

  //SETTERS------------------------------------------------------------------------------------------------------------------
  /** set the index of the visualisation select element according to the attribute string.
   * @param attr attr is the attribute the visualisation select element should get set to.
   */
  setSelectedVis(attr: string) {
    const visualisations = this.typeNodes.find(
      (x) => x.type == this.currentNodeType,
    )?.visualisations; //stored vis method
    const val = visualisations?.find((x) => x.attribute == attr)?.vis;
    const sel = document.getElementById('visualisation-select') as HTMLSelectElement;
    const opts = sel.options;
    if (val) {
      for (let opt, j = 0; (opt = opts[j]); j++) {
        if (opt.value == val) {
          sel.selectedIndex = j;
          break;
        }
      }
    } else {
      sel.selectedIndex = 0;
    }
  }
  /** Find the selected value in assignedcolors for current node and select it in the dropdown */
  setSelectedColor() {
    const val = this.assignedColorsTypes.find((x) => x.collection == this.currentNodeType)?.color;
    const sel = document.getElementById('color-select') as HTMLSelectElement;
    const opts = sel.options;
    for (let opt, j = 0; (opt = opts[j]); j++) {
      if (opt.value == val) {
        sel.selectedIndex = j;
        break;
      }
    }
  }

  /**find the selected value in assignedcolors for current communityDetectionNode
   * and select it in the dropdown of the community detection color select element.*/
  setSelectedCdColor() {
    const val = this.assignedColorsCommunityDetection.find(
      (x) => x.collection == this.currentNodeCommunityDetection,
    )?.color;
    const sel = document.getElementById('cd-color-select') as HTMLSelectElement;
    const opts = sel.options;
    for (let opt, j = 0; (opt = opts[j]); j++) {
      if (opt.value == val) {
        sel.selectedIndex = j;
        break;
      }
    }
  }

  //------------------------------------------------------------------------------------------------------------------------
  //ONCHANGE FUNCTIONS------------------------------------------------------------------------------------------------------
  /** on switching nodes in the dropdown, update variables and dropdowns.
   * @param numberNodeType numberType is the number of the new nodeType that is selected.
   */
  nodeOnChange(numberNodeType: number) {
    this.setCurrentNodeType(numberNodeType);
    this.setSelectedColor();
    this.generateAttributes();
    this.setSelectedVis(this.getSelectedAttr());
    this.applyVisualizations();
  }

  /**assign a newly selected color to the currentTypeNode
   * @param targetColor targetColors is the new color that is selected.
   */
  colorOnChange(targetColor: string) {
    let node = this.assignedColorsTypes.find((x) => x.collection == this.currentNodeType);
    //check if not undefined
    if (node && node.collection) {
      node.color = targetColor;
      this.nodeLinkViewModel.resetClusterOfNodes(node.collection); //reset cluster of nodes which could have been changed by community detection ml algorithm
    }
    this.applyAssignedColors('type');
    this.applyVisualizations();
  }

  /**retrieve the current visualisation and set the vis dropdown to this, then apply said visualisation
   * @param targetAttribute targetAttribute is the new attribute that is selected.
   */
  attrOnChange(targetAttribute: string) {
    this.setSelectedVis(targetAttribute);
    this.handleVis();
  }

  /**handle setting a new visualisation
   * @param targetVis targetVis is the new visualisation that is selected.
   */
  visOnChange(targetVis: string) {
    this.handleVis();
    return;
  }

  /**handle visualisations for community detection based on another categorical attribute.
   * @param targetCluster target is the categorical attribute on which the new clustering is based on.
   */
  cdClusterBasedOnOnChange(targetCluster: string) {
    let attributeValueSelect = document.getElementById('cd-value-select') as HTMLSelectElement;
    this.removeAllOptions(attributeValueSelect);
    let uniqueValues = this.nodeLinkViewModel.uniqueAttributeValues[targetCluster];
    this.addPossibleValuesToSelect(attributeValueSelect, uniqueValues);
    if (targetCluster == 'mldata') {
      for (let i = 0; i < this.realNodes.length; i++) {
        let currentMlData = this.realNodes[i].clusterAccoringToMLData;
        let currentMlDataString = '';
        // check if not undefined
        if (currentMlData) {
          currentMlDataString = currentMlData.toString();
        }
        const sameAsCurrentValue = (element: string) => element == currentMlDataString;
        let valueIndex = uniqueValues.findIndex(sameAsCurrentValue);
        // save group of node in mldata entry to later reset the group entry to the correct group
        this.realNodes[i].cluster = valueIndex;
      }
    } else {
      for (let i = 0; i < this.realNodes.length; i++) {
        let attributes = this.realNodes[i].attributes;
        if (typeof attributes != 'undefined') {
          let currentValue = attributes[targetCluster as keyof NodeType];
          const sameAsCurrentValue = (element: string) => element == currentValue;
          let clusterIndex = uniqueValues.findIndex(sameAsCurrentValue);
          if (clusterIndex != -1) {
            // clusterIndex + 1 so you dont get 0, which is interpreted as 'undefined'.
            // only if node does not have the attribute on which is being clustered.
            clusterIndex++;
          }
          this.realNodes[i].cluster = clusterIndex;
        }
      }
    }
    this.graph.nodes = this.realNodes;
    this.nodeLinkViewModel.UpdateColours(this.graph, 5);
    //this.makeNodeCommunityDetection();
  }

  /**update the currentNodeCommunityDetection to the selected value
   * and update dropdown of community detection color select element.
   * @param targetClusterValue cluster is the number which represents the selected cluster.
   */
  cdValueOnChange(targetClusterValue: number) {
    this.setCurrentNodeCommunityDetection(targetClusterValue);
    this.setSelectedCdColor();
  }

  /**assign a newly selected color to the currentCommunityDetectionNode
   * @param targetColor targetColors is the new color that is selected.
   */
  cdColorOnChange(targetColor: string) {
    let node = this.assignedColorsCommunityDetection.find(
      (x) => x.collection == this.currentNodeCommunityDetection,
    );
    //check if not undefined
    if (node) {
      node.color = targetColor;
      //reset group of nodes which could have been changed by community detection ml algorithm
    }
    this.applyAssignedColors('communityDetection');
    this.applyVisualizations();
  }

  //HELPER FUNCTIONS-----------------------------------------------------------------------------------------------------------
  /**removeAllOptions removes all options from a select element
   * @param select select is the select element from which all options will be removed.
   */
  removeAllOptions(select: HTMLSelectElement) {
    let options = select.options;
    while (options.length > 0) {
      select.remove(0);
    }
  }

  /**addPossibleValuesToSelect adds all values of a string array as options to a select element
   * @param select select is the select element where new options will be added.
   * @param uniqueValues uniqueValues is a string array with all unique values of a cluster.
   */
  addPossibleValuesToSelect(select: HTMLSelectElement, uniqueValues: string[]) {
    this.removeAllOptions(select);
    for (let index in range(uniqueValues.length)) {
      let currentValue = uniqueValues[index];
      let newOption = new Option(currentValue, currentValue);
      select.add(newOption, undefined);
    }
  }
}
