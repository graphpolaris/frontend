/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from '../../../../domain/entity/broker/broker';
import React from 'react';
import NodeLinkViewModelImpl from './NodeLinkViewModelImpl';
import NodeLinkConfigPanelViewModel from './NodeLinkConfigPanelViewModel';
import NodeLinkConfigPanelViewModelImpl from './NodeLinkConfigPanelViewModelImpl';
import mockNodeLinkResult from '../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import mockSchema from '../../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import cd2ndChamber from '../../../../data/mock-data/ml-node-link/cd-2ndChamber';
import cdGraphPolaris from '../../../../data/mock-data/ml-node-link/cd-GraphPolaris';
import ResultNodeLinkParserUseCase from '../../../../domain/usecases/node-link/ResultNodeLinkParserUseCase';
import { isNodeLinkResult } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import { ColourPalettes } from '../../../../domain/entity/customization/colours';
import { LocalStorage } from '../../../../data/drivers/LocalStorage';
import {
  GraphType,
  LinkType,
  NodeType,
  AssignedColors,
  TypeNode,
  CommunityDetectionNode,
  Colors,
  Visualisation,
} from '../../../../domain/entity/node-link/structures/Types';
import { currentColours } from '../../../view/graph-schema/SchemaComponent';
import { Graphics } from 'pixi.js';
import testGraph from '../../../../data/mock-data/node-link/Miserables';
import { JsxElement } from 'typescript';
import { range } from 'd3-array';
jest.mock('../../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet');

let resultNodeLinkParserUseCase: ResultNodeLinkParserUseCase;
let nodeLinkViewModel: NodeLinkViewModelImpl;
let graph: GraphType;
let viewModel: NodeLinkConfigPanelViewModelImpl;
beforeEach(() => {
  resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  nodeLinkViewModel = new NodeLinkViewModelImpl(
    resultNodeLinkParserUseCase,
    testGraph,
    400,
    300,
    ColourPalettes[LocalStorage.instance().cache.currentColours.key],
  );
  generateGraph();
});

function generateGraph() {
  //generate graph
  nodeLinkViewModel.consumeMessageFromBackend(cdGraphPolaris);
  graph = nodeLinkViewModel.graph;
  viewModel = new NodeLinkConfigPanelViewModelImpl(graph, nodeLinkViewModel);
}

//Initialisation
describe('NodeLinkConfigPanelViewModelImpl: initialisation community detection', () => {
  it('should correctly fill the community detection value select with the unique values', () => {
    //mocking the select element
    let select = document.createElement('select');
    select.id = 'cd-value-select';
    document.body.appendChild(select);
    viewModel.nodeLinkViewModel.uniqueAttributeValues['mldata'] = ['1', '2'];

    if (select) {
      viewModel.cdClusterBasedOnOnChange('mldata');
      expect(select.options.length).toEqual(2);
    }
    viewModel.removeAllOptions(select);
  });

  it('should correctly initialize the colors for different node collections based on community detection clusters', () => {
    viewModel.communityDetectionNodes = [{ cluster: 1 }, { cluster: 2 }];
    viewModel.initializeAssignedColorsCommunityDetection();
    expect(viewModel.assignedColorsCommunityDetection).toEqual(
      expectedAssignedColorsCommunityDetection,
    );
  });

  it('should correctly initialize the values of the mldata attribute', () => {
    viewModel.initializeMlAttributeValues();
    expect(viewModel.nodeLinkViewModel.uniqueAttributeValues['mldata']).toEqual(['1', '2']);
  });
});

//Apply visualisations

//Generators (selects)

//Getters
describe('NodeLinkConfigPanelViewModelImpl: community detection get functions', () => {
  it('should correctly return the currentNodeCommunityDetection, both when initialising and when changing menu item', () => {
    expect(viewModel.getCurrentNodeCommunityDetection()).toEqual(0);
    viewModel.setCurrentNodeCommunityDetection(4);
    expect(viewModel.getCurrentNodeCommunityDetection()).toEqual(4);
  });

  it('should correctly return the communityDetectionNodes', () => {
    viewModel.communityDetectionNodes = [{ cluster: 1 }, { cluster: 2 }];
    expect(viewModel.getCommunityDetectionNodes()).toEqual(expectedCommunityDetectionNodes);
  });

  it('should correctly return the selected cluster', () => {
    //mocking the select element
    let select = document.createElement('select');
    select.id = 'cd-cluster-select';
    let option1 = document.createElement('option');
    option1.value = 'first';
    select.options.add(option1);
    let option2 = document.createElement('option');
    option2.value = 'second';
    select.options.add(option2);
    document.body.appendChild(select);
    select.options[0].selected = true;
    expect(viewModel.getSelectedClusterBasedOn()).toEqual('first');
    select.options[1].selected = true;
    expect(viewModel.getSelectedClusterBasedOn()).toEqual('second');
    viewModel.removeAllOptions(select);
  });

  it('should correctly return the unique values of the selected cluster', () => {
    //mocking the select element
    let select = document.getElementById('cd-value-select') as HTMLSelectElement;
    viewModel.removeAllOptions(select);
    let option1 = document.createElement('option');
    option1.value = 'first';
    select.options.add(option1);
    let option2 = document.createElement('option');
    option2.value = 'second';
    select.options.add(option2);
    document.body.appendChild(select);
    expect(viewModel.getValuesOfSelectedCluster()).toEqual(['first', 'second']);
    viewModel.removeAllOptions(select);
  });

  it('should correctly return the unique categorical attributes', () => {
    viewModel.nodeLinkViewModel.uniqueAttributeValues = {
      ['sex']: ['Male', 'Female'],
      ['team']: ['GraphPolaris', 'GravenVanPolaris'],
      ['mldata']: ['1', '2'],
    };
    let uniqueAttributes = viewModel.getUniqueAttributes();
    expect(uniqueAttributes).toEqual(['mldata', 'sex', 'team']);
  });
});

//Setters
describe('NodeLinkConfigPanelViewModelImpl: community detection setters', () => {
  it('should correctly set the community detection color select element to the correct index', () => {
    //mock select
    let s = document.createElement('select');
    s.id = 'cd-color-select';
    let opt1 = document.createElement('option');
    opt1.value = 'wrong color';
    let opt2 = document.createElement('option');
    opt2.value = 'wrong color 2';
    let opt3 = document.createElement('option');
    opt3.value = 'nieuw';
    s.options.add(opt1);
    s.options.add(opt2);
    s.options.add(opt3);
    document.body.appendChild(s);

    viewModel.setCurrentNodeCommunityDetection(1); // 'kamerleden'
    viewModel.assignedColorsCommunityDetection = [
      { collection: 1, color: 'nieuw', default: 'def' },
    ];
    viewModel.setSelectedCdColor();

    let resultSelect = document.getElementById('cd-color-select') as HTMLSelectElement;

    //values should be the same as the attributes
    let opts = resultSelect.options;
    let properIndex;
    for (let j of range(opts.length)) {
      if (opts[j].value == 'nieuw') {
        properIndex = j;
      }
    }
    if (properIndex == undefined) {
      fail('no proper index');
    }
    expect(resultSelect.selectedIndex).toEqual(properIndex);
    viewModel.removeAllOptions(s);
  });
});

//OnChange
describe('NodeLinkConfigPanelViewModelImpl: Onchange community detection functions', () => {
  it('should correctly set the currentNodeCommunityDetection', () => {
    viewModel.cdValueOnChange(2);
    let currentNode = viewModel.getCurrentNodeCommunityDetection();
    expect(currentNode).toEqual(2);
  });

  it('should correctly set the new selected color in assignedColorsCommunityDetection', () => {
    viewModel.communityDetectionNodes = expectedCommunityDetectionNodes;
    viewModel.assignedColorsCommunityDetection = expectedAssignedColorsCommunityDetection;
    expect(
      viewModel.assignedColorsCommunityDetection.find((x) => x.collection == 2)?.color,
    ).toEqual('Default');
    viewModel.setCurrentNodeCommunityDetection(2);
    viewModel.cdColorOnChange('cd0000');
    expect(
      viewModel.assignedColorsCommunityDetection.find((x) => x.collection == 2)?.color,
    ).toEqual('cd0000');
  });

  // it('should correctly set the new selected color in currentColors', () => {
  //   let currentColors = viewModel.getCurrentColours();
  //   expect(currentColors.nodes[2]).toEqual('cd0000');
  //   viewModel.setCurrentNodeCommunityDetection(2);
  //   viewModel.cdColorOnChange('0000cd');
  //   let updatedCurrentColors = viewModel.getCurrentColours();
  //   expect(updatedCurrentColors.nodes[2]).toEqual('0000cd');
  // });

  //TODO: cdClusterBasedOnOnChange, need GSA!
  // it('should correctly set the cluster entry in realNodes based on a cluster chosen by the user: mldata', () => {
  //   viewModel.getSelectedClusterBasedOn = jest.fn(() => 'mldata');
  //   viewModel.getValuesOfSelectedCluster = jest.fn(() => [0, 1]);
  //   let realNodes = viewModel.getrealNodes();
  //   expect(realNodes[11].id).toEqual('gp/12');
  //   expect(realNodes[11].clusterAccoringToMLData).toEqual(2);

  //   expect(realNodes[0].id).toEqual('gp/1');
  //   expect(realNodes[0].clusterAccoringToMLData).toEqual(1);

  //   viewModel.cdClusterBasedOnOnChange('mldata');
  //   expect(realNodes[11].cluster).toEqual(1);
  // });

  it('should correctly set the cluster entry in realNodes based on a cluster chosen by the user: default', () => {
    viewModel.getSelectedClusterBasedOn = jest.fn(() => 'team');
    viewModel.getValuesOfSelectedCluster = jest.fn(() => ['GravenVanPolaris', 'GraphPolaris']);
    let realNodes = viewModel.getrealNodes();
    expect(realNodes[4].id).toEqual('gp/5');

    expect(realNodes[13].id).toEqual('gpt/14');

    viewModel.nodeLinkViewModel.uniqueAttributeValues['team'] = [
      'GravenVanPolaris',
      'GraphPolaris',
    ];
    viewModel.cdClusterBasedOnOnChange('team');
    expect(realNodes[4].cluster).toEqual(1);
    expect(realNodes[13].cluster).toEqual(2);
  });
});

const expectedCommunityDetectionNodes: CommunityDetectionNode[] = [
  {
    cluster: 1,
  },
  {
    cluster: 2,
  },
];

const expectedAssignedColorsCommunityDetection: AssignedColors[] = [
  {
    collection: 1,
    color: 'Default',
    default: 'd49350',
  },
  {
    collection: 2,
    color: 'Default',
    default: '1e9797',
  },
];
