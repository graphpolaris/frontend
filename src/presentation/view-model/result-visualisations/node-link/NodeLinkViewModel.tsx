/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  GraphType,
  LinkType,
  NodeType,
} from '../../../../domain/entity/node-link/structures/Types';
import * as d3 from 'd3-v6';
import ResultVisViewModel from '../ResultVisViewModel';
import { NodesAttrCollection } from '../utils/attributes-config/AttributesConfigPanelViewModelImpl';

/** This is an interface of NodeLinkViewModel that will be given to the NodeLinkBuilder view. */
export default interface NodeLinkViewModel extends ResultVisViewModel {
  graph: GraphType;
  stage: PIXI.Container;
  links: PIXI.Graphics;
  simulation: d3.Simulation<NodeType, LinkType>;
  myRef: React.RefObject<HTMLDivElement>;
  renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
  dragOffset: any;
  panOffset: any;
  scalexy: number;
  width: number;
  height: number;
  currentColours: any;
  radius: number;
  visibleAttributes: NodesAttrCollection;

  jaccardThreshold: number;
  uniqueAttributeValues: { [key: string]: string[] };
  numberOfMlClusters: number;
  updateJaccardThreshHold(): void;
  UpdateColours(graph: GraphType, radius: number): void;
  UpdateAttributes(graph: GraphType): void;
  UpdateRadius(graph: GraphType, radius: number): void;
  SetNodeGraphics(graph: GraphType, radius: number): void;
  ticked(): void;
  selectD3Elements(): void;
  startSimulation(): void;
  handleResize(): void;
  resetClusterOfNodes(type: number): void;
  showAttributes(node: NodeType): void;
  highlightNode(node: NodeType): void;
  highlightLinks(node: NodeType): void;
  getRelatedLinks(nodes: NodeType[]): void;
  ToggleInformationOnNode(node: NodeType): void;
}
