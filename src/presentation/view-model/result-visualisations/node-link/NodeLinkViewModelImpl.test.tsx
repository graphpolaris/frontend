/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { LocalStorage } from '../../../../data/drivers/LocalStorage';
import { ColourPalettes } from '../../../../domain/entity/customization/colours';
import NodeLinkViewModelImpl from './NodeLinkViewModelImpl';
import ResultNodeLinkParserUseCase from '../../../../domain/usecases/node-link/ResultNodeLinkParserUseCase';
import spLotrBig from '../../../../data/mock-data/ml-node-link/sp-lotrBig';

import lpLotrSmall from '../../../../data/mock-data/ml-node-link/lp-lotrSmall';
import { LinkType, NodeType } from '../../../../domain/entity/node-link/structures/Types';

describe('NodeLinkViewModelImpl shortest path working?', () => {
  let nodeLinkViewModel: NodeLinkViewModelImpl;
  const resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  const graphShortestPath = resultNodeLinkParserUseCase.parseQueryResult(spLotrBig);
  beforeEach(() => {
    nodeLinkViewModel = new NodeLinkViewModelImpl(
      resultNodeLinkParserUseCase,
      graphShortestPath,
      window.innerWidth,
      window.innerHeight,
      ColourPalettes[LocalStorage.instance().cache.currentColours.key],
    );
  });
  it('Should update the list of currentShortestPathEdges', () => {
    //setup
    if (nodeLinkViewModel.graph.nodes == undefined) {
      fail();
    }
    const node: NodeType | undefined = nodeLinkViewModel.graph.nodes.find(
      (node) => node.id == 'character/166',
    );
    if (node == null || node.shortestPathData == null)
      fail('Something went wrong in the test data');
    const node2: NodeType | undefined = nodeLinkViewModel.graph.nodes.find(
      (node) => node.id == 'character/169',
    );
    if (node2 == null || node2.shortestPathData == null)
      fail('Something went wrong in the test data');
    //Testing
    //With one node there should be no path
    nodeLinkViewModel.ToggleInformationOnNode(node);
    const expectedResult: LinkType[] = [];
    expect(nodeLinkViewModel.currentShortestPathEdges).toEqual(expectedResult);

    //With multiple nodes there should be a path
    nodeLinkViewModel.ToggleInformationOnNode(node2);

    const expectedResult2: LinkType[] = [
      {
        mlEdge: false,
        source: 'character/157',
        target: 'character/166',
        value: 1,
      },
      {
        mlEdge: false,
        source: 'character/156',
        target: 'character/157',
        value: 1,
      },
      {
        mlEdge: false,
        source: 'character/169',
        target: 'character/156',
        value: 1,
      },
    ];
    expect(nodeLinkViewModel.currentShortestPathEdges).toEqual(expectedResult2);
  });
});

describe('NodeLinkViewModelImpl linkprediction', () => {
  const resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  const graph = resultNodeLinkParserUseCase.parseQueryResult(lpLotrSmall);
  let nodeLinkViewModel: NodeLinkViewModelImpl;
  let mlLinks: LinkType[];
  let nonMlLinks: LinkType[];
  beforeEach(() => {
    nodeLinkViewModel = new NodeLinkViewModelImpl(
      resultNodeLinkParserUseCase,
      graph,
      window.innerWidth,
      window.innerHeight,
      ColourPalettes[LocalStorage.instance().cache.currentColours.key],
    );
    mlLinks = nodeLinkViewModel.graph.links.filter((link: LinkType) => link.mlEdge == true);
    nonMlLinks = graph.links.filter((link) => link.mlEdge == false);
  });
  it('Should update the list of currentlyHighlightednodes.&& currentlyHighlighted edges', () => {
    const node: NodeType | undefined = nodeLinkViewModel.graph.nodes.find(
      (node) => node.id == 'character/70',
    );
    if (node == null) fail('Something went wrong in the test data');

    //Node selected on
    nodeLinkViewModel.ToggleInformationOnNode(node);
    expect(node.selected).toEqual(true);
    expect(nodeLinkViewModel.currentHightlightedNodes.includes(node)).toEqual(true);
    nodeLinkViewModel
      .getRelatedLinks([node])
      .forEach((link) =>
        expect(nodeLinkViewModel.currentlyHighlightedLinks.includes(link)).toEqual(true),
      );

    //Node selected off
    nodeLinkViewModel.ToggleInformationOnNode(node);
    expect(node.selected).toEqual(false);
    expect(nodeLinkViewModel.currentHightlightedNodes.includes(node)).toEqual(false);
    nodeLinkViewModel
      .getRelatedLinks([node])
      .forEach((link) =>
        expect(nodeLinkViewModel.currentlyHighlightedLinks.includes(link)).toEqual(false),
      );
  });

  it('Should get the correct related edges', () => {
    const node: NodeType | undefined = nodeLinkViewModel.graph.nodes.find(
      (node) => node.id == 'character/70',
    );
    if (node == null) fail('Something went wrong in the test data');

    nodeLinkViewModel.jaccardThreshold = 0.3;
    let relatedlinks = nodeLinkViewModel.getRelatedLinks([node]);
    expect(relatedlinks.length).toEqual(5);

    nodeLinkViewModel.jaccardThreshold = 0.4;
    relatedlinks = nodeLinkViewModel.getRelatedLinks([node]);
    expect(relatedlinks.length).toEqual(4);
  });

  it('Should check if the links is visible', () => {
    //All non mlLinks should be visable
    nonMlLinks.forEach((link) => expect(nodeLinkViewModel.isLinkVisiable(link)).toEqual(true));

    nodeLinkViewModel.jaccardThreshold = 0.3;
    //For mlLinks all mlLinks should only be visable if their value is higher then the jaccardThreshold.
    mlLinks.forEach((link) => {
      expect(nodeLinkViewModel.isLinkVisiable(link)).toEqual(
        link.value > nodeLinkViewModel.jaccardThreshold,
      );
    });
  });
});
