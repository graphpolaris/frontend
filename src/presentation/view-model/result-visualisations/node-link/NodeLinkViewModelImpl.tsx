/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  GraphType,
  LinkType,
  NodeType,
} from '../../../../domain/entity/node-link/structures/Types';
import NodeLinkViewModel from './NodeLinkViewModel';
import * as PIXI from 'pixi.js';
import React from 'react';
import * as d3 from 'd3-v6';
import { jsPDF } from 'jspdf';
import ResultNodeLinkParserUseCase from '../../../../domain/usecases/node-link/ResultNodeLinkParserUseCase';
import { isNodeLinkResult } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import AbstractResultVisViewModelImpl from '../AbstractResultVisViewModelImpl';
import { NodesAttrCollection } from '../utils/attributes-config/AttributesConfigPanelViewModelImpl';
import Broker from '../../../../domain/entity/broker/broker';
import { isAttributeDataEntity } from '../../../../domain/entity/graph-schema/structures/AttributeDataType';
import {
  AttributeData,
  NodeAttributeData,
} from '../../../../domain/entity/graph-schema/structures/InputDataTypes';
import { AttributeCategory } from '../../../../domain/entity/graph-schema/structures/Types';

export default class NodeLinkViewModelImpl
  extends AbstractResultVisViewModelImpl
  implements NodeLinkViewModel
{
  private resultNodeLinkParserUseCase: ResultNodeLinkParserUseCase;
  private nodesMoving;

  public graph: GraphType;
  public stage: PIXI.Container;
  public links: PIXI.Graphics;
  public simulation: d3.Simulation<NodeType, LinkType>;
  public myRef: React.RefObject<HTMLDivElement>;
  public renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
  public dragOffset: any;
  public panOffset: any;
  public scalexy: number;
  public width: number;
  public height: number;
  public currentColours: any;
  public radius: number;
  public visibleAttributes: NodesAttrCollection;
  public currentHightlightedNodes: NodeType[];
  public currentlyHighlightedLinks: LinkType[];
  public currentShortestPathEdges: LinkType[];
  public jaccardThreshold: number;
  public uniqueAttributeValues: { [key: string]: any[] };
  public numberOfMlClusters: number;

  /**
   * Creates the NodeLinkViewModelImpl using a given graph and window dimensions.
   * It initialises the d3 simulation, the PIXI containers and the offsets for dragging and zooming.
   *
   * @param graph The graph has a list of nodes and a list of links to build the nodelink diagram.
   * @param width The width of the window.
   * @param height The height of the window.
   * @param currentColours The current colour palette.
   */
  public constructor(
    resultNodeLinkParserUseCase: ResultNodeLinkParserUseCase,
    graph: GraphType,
    width: number,
    height: number,
    currentColours: any,
  ) {
    super();

    this.radius = 5;

    this.resultNodeLinkParserUseCase = resultNodeLinkParserUseCase;

    // Graph holds the nodes and edges for the simulation and graphics
    this.graph = graph;

    // Stage is the PIXI container holding all the graphics
    this.stage = new PIXI.Container();

    // Links is a graphics object that draws the lines between the nodes
    this.links = new PIXI.Graphics();

    // Simulation is a d3 object that calculates the movement and position of the nodes and edges
    this.simulation = d3.forceSimulation<NodeType, LinkType>();

    // Create a hook where the renderer will be attached to
    this.myRef = React.createRef();

    // The width and height are for the window
    this.width = width;
    this.height = height;
    this.scalexy = 1;

    this.renderer = PIXI.autoDetectRenderer(this.width, this.height, {
      antialias: !0,
      transparent: !0,
      resolution: 1,
    });

    this.dragOffset = {
      x: 0,
      y: 0,
    };

    this.panOffset = {
      x: 0,
      y: 0,
    };

    this.currentColours = currentColours;

    // this becomes true when the nodes are moving due to onDrag being active
    this.nodesMoving = false;

    // this is a dictionary of attributes that should be hidden
    this.visibleAttributes = {};
    this.currentHightlightedNodes = [];
    this.currentlyHighlightedLinks = [];
    this.currentShortestPathEdges = [];
    this.jaccardThreshold = -1; // All should be visable regardless now.
    this.uniqueAttributeValues = {};
    this.numberOfMlClusters = 0;
  }

  /**
   * Will be called by the broker if a query_result is available.
   * This object is subscribed with the query_result routingkey.
   * @param jsonObject The query_result coming from the broker.
   */
  public consumeMessageFromBackend(jsonObject: unknown): void {
    if (isNodeLinkResult(jsonObject)) {
      this.graph = this.resultNodeLinkParserUseCase.parseQueryResult(jsonObject);

      this.SetNodeGraphics(this.graph, this.radius);
      this.simulation.restart();
      this.notifyViewAboutChanges();
    } else if (isAttributeDataEntity(jsonObject)) {
      // Add all information from the received message to the popup-menu's.
      this.initializeUniqueAttributes(jsonObject, 'gsa_node_result');

      this.notifyViewAboutChanges();
    }
  }

  /** Exports the nodelink diagram as a pdf for downloading. */
  public exportToPDF(): void {
    this.renderer.extract.canvas(this.stage).toBlob(function (b) {
      if (!b) return;
      const pdf = new jsPDF();
      if (b) {
        pdf.addImage(URL.createObjectURL(b), 'JPEG', 0, 0, 100, 100, '', 'NONE', 0);
        pdf.save('diagram.pdf');
      } else {
        console.log('null blob in exportToPDF');
      }
    }, 'image/png');
  }

  /** Exports the nodelink diagram as a png for downloading. */
  public exportToPNG(): void {
    this.renderer.extract.canvas(this.stage).toBlob(function (b) {
      if (b) {
        const a = document.createElement('a');
        document.body.append(a);
        a.download = 'diagram';
        a.href = URL.createObjectURL(b);
        a.click();
        a.remove();
      } else {
        console.log('null blob in exportToPNG');
      }
    }, 'image/png');
  }

  /** SelectD3Elements sets the eventlisteners for drag and drop and zooming. */
  public selectD3Elements(): void {
    let radius = this.radius;
    let radiusoffset = 5;
    // Set event listeners for drag and drop behavior
    d3.select<HTMLCanvasElement, any>(this.renderer.view).call(
      d3
        .drag<HTMLCanvasElement, any>()
        .container(this.renderer.view)
        .subject((event: any) =>
          this.simulation.find(
            (event.x - this.panOffset.x) / this.scalexy,
            (event.y - this.panOffset.y) / this.scalexy,
            radius + radiusoffset / this.scalexy,
          ),
        )
        .on('start', this.onDragStart)
        .on('drag', this.onDragMove)
        .on('end', this.onDragEnd),
    );

    // Eventlisteners for zooming (mousewheel events)
    d3.select<HTMLCanvasElement, any>(this.renderer.view).call(
      d3.zoom<HTMLCanvasElement, any>().on('zoom', this.zoom),
    );
  }

  /** StartSimulation starts the d3 forcelink simulation. This has to be called after the simulation is initialised. */
  public startSimulation(): void {
    this.simulation
      .force(
        'link',
        d3.forceLink().id((d: any) => d.id),
      )
      .force('charge', d3.forceManyBody())
      .force('center', d3.forceCenter(this.width / 2, this.height / 2));

    // Set PIXI as rendering engine
    this.myRef.current?.appendChild(this.renderer.view);
  }

  /** UpdateColours updates the colours of the nodelink elements */
  public UpdateColours = (graph: GraphType, radius: number) => {
    // update for each node in graph
    graph.nodes.forEach((node: NodeType) => {
      let item = this.stage.getChildByName('node_' + node.id);
      if (item != null) {
        item.destroy();
        this.createNode(node, radius);
      }
    });

    // update text colour (written after nodes so that text appears on top of nodes)
    this.UpdateAttributes(graph);

    // refresh
    this.simulation.alphaTarget(0).restart();
  };

  /**
   * Refreshes the attributes. This function is called when the attribute filter has been changed
   * @param graph the graph of which its nodes should be checked for attributes
   */
  public UpdateAttributes = (graph: GraphType) => {
    this.simulation.alphaTarget(0).restart(); // required so that the visualisation updates
    graph.nodes.forEach((node: NodeType) => {
      if (node.gfxAttributes !== undefined) {
        // destroy and add the attributes pop-up again
        node.gfxAttributes.destroy();
        this.createAttributes(node);
        node.gfxAttributes.scale.x = 1 / this.scalexy + 0.001 * this.scalexy;
        node.gfxAttributes.scale.y = 1 / this.scalexy + 0.001 * this.scalexy;
        // make the pop-up visible if it was visible before
        if (node.selected) {
          node.gfxAttributes.alpha = 1;
        }
      }
    });
  };

  /** UpdateRadius works just like UpdateColours, but also applies radius*/
  public UpdateRadius = (graph: GraphType, radius: number) => {
    // update for each node in graph
    graph.nodes.forEach((node: NodeType) => {
      let item = this.stage.getChildByName('node_' + node.id);
      if (item != null) {
        item.destroy();
        if (node.radius) {
          this.createNode(node, node.radius);
        } else {
          this.createNode(node, radius);
        }
      }
    });

    // update text colour (written after nodes so that text appears on top of nodes)
    graph.nodes.forEach((node: NodeType) => {
      if (node.gfxAttributes !== undefined) {
        let selected = node.selected === true;
        node.gfxAttributes.destroy();
        this.createAttributes(node);
        if (selected) {
          this.showAttributes(node);
        }
      }
    });

    // refresh
    this.simulation.alphaTarget(0).restart();
  };

  /**
   * SetNodeGraphics is an initialising function. It attaches the nodes and links to the simulation.
   * It creates graphic objects and adds these to the PIXI containers. It also clears both of these of previous nodes and links.
   * @param graph The graph returned from the database and that is parsed into a nodelist and edgelist.
   * @param radius The radius of the node.
   */
  public SetNodeGraphics = (graph: GraphType, radius: number) => {
    // Set the nodes for the d3 simulation
    this.simulation.nodes(graph.nodes);
    this.simulation.force<d3.ForceLink<NodeType, LinkType>>('link')?.links(graph.links);

    // Clear the stage and add the new nodes and links
    this.simulation.alpha(0.5).restart();
    this.stage.removeChildren();
    this.stage.addChild(this.links);

    // Create and initialise graphic objects for the nodes
    graph.nodes.forEach((node: NodeType) => {
      if (node.radius) {
        this.createNode(node, node.radius);
      } else {
        this.createNode(node, radius);
      }
    });
  };

  /**
   * Zoom is the function called when an event is triggered by using the mousewheel.
   * @param event The event of the mousewheel.
   */
  public zoom = (event: any) => {
    // Update the pan offset with the mousemovement
    this.panOffset.x += event.sourceEvent.movementX;
    this.panOffset.y += event.sourceEvent.movementY;

    // Get the mouse position in the container coördinates
    let mousePos = {
      x:
        (event.sourceEvent.x - event.sourceEvent.target.offsetLeft - this.panOffset.x) /
        this.scalexy,
      y:
        (event.sourceEvent.y - event.sourceEvent.target.offsetTop - this.panOffset.y) /
        this.scalexy,
    };

    this.simulation.restart();

    // Change the scale
    if (event.sourceEvent.deltaY < 0) {
      this.scalexy *= 1.2;
      this.stage.scale.x = this.scalexy;
      this.stage.scale.y = this.scalexy;
    } else if (event.sourceEvent.deltaY > 0) {
      this.scalexy /= 1.2;
      this.stage.scale.x = this.scalexy;
      this.stage.scale.y = this.scalexy;
    }

    // Scale the text of the nodes
    this.graph.nodes.forEach((node: NodeType) => {
      if (node.gfxAttributes) {
        node.gfxAttributes.scale.x = 1 / this.scalexy + 0.001 * this.scalexy;
        node.gfxAttributes.scale.y = 1 / this.scalexy + 0.001 * this.scalexy;
      }
    });

    // Calculate the difference in position, to zoom at the mouse
    let dx =
      ((event.sourceEvent.x - event.sourceEvent.target.offsetLeft - this.panOffset.x) /
        this.scalexy -
        mousePos.x) *
      this.scalexy;
    let dy =
      ((event.sourceEvent.y - event.sourceEvent.target.offsetTop - this.panOffset.y) /
        this.scalexy -
        mousePos.y) *
      this.scalexy;
    if (dx && dy) {
      this.panOffset.x += dx;
      this.panOffset.y += dy;
    }

    // Apply the panOffset changes to the stage position
    this.stage.position.x = this.panOffset.x;
    this.stage.position.y = this.panOffset.y;
    this.renderer.render(this.stage);
  };

  /**
   * Creates and adds a node to the stage
   * @param node The node from the graph that needs to be made
   * @param radius The radius of the visible node
   */
  private createNode = (node: NodeType, radius: number, selected?: boolean) => {
    node.gfx = new PIXI.Graphics();
    node.selected = selected;
    const lineColour = selected
      ? this.currentColours.nodeHighlightedEdge
      : this.currentColours.visBackground;
    const lineWidth = selected ? 3 : 1.5;
    node.gfx.lineStyle(lineWidth, Number('0x' + lineColour));
    //check if not undefined.
    if (node.cluster) {
      node.gfx.beginFill(this.colour(node.cluster));
    } else {
      //if cluster is undefined, use type to determine the color.
      //check if not undefined.
      if (node.type) {
        node.gfx.beginFill(this.colour(node.type));
      }
    }
    node.gfx.drawCircle(0, 0, radius);
    node.gfx.interactive = true;
    node.gfx.hitArea = new PIXI.Circle(0, 0, 4);
    node.gfx.name = 'node_' + node.id;
    this.stage.addChild(node.gfx);
  };

  /**
   * Creates a list of attributes from a given node, that can be visualised when the node is clicked
   * @param node The node from the graph which attributes need to be visualised
   */
  private createAttributes = (node: NodeType) => {
    node.gfxAttributes = new PIXI.Graphics();
    node.gfxAttributes.beginFill(0xeeeeee);
    node.gfxAttributes.name = 'attributes_' + node.id;
    node.gfxAttributes.alpha = 0;
    node.gfxAttributes.interactive = true;
    // add title
    let gfxName = new PIXI.Text();
    if (node.displayInfo != undefined) {
      gfxName.text = node.displayInfo;
    } else {
      gfxName.text = node.id;
    }
    gfxName.style.fontSize = 16;
    gfxName.style.fontFamily = 'Poppins, arial, sans-serif';
    gfxName.style.fontWeight = 'bold';
    gfxName.x = 10;
    gfxName.y = 10;
    gfxName.name = 'header';
    node.gfxAttributes.addChild(gfxName);

    // add attributes container
    let container = new PIXI.Container();
    node.gfxAttributes.addChild(container);
    // add container for keys
    let keys = new PIXI.Container();
    container.addChild(keys);
    // add container for values
    let values = new PIXI.Container();
    container.addChild(values);

    // add attributes
    if (node.attributes) {
      let index = 0;
      for (let key in node.attributes) {
        let attributes = this.visibleAttributes[node.id.split('/')[0]];
        let attributeHidden = attributes ? attributes[key] === false : false;
        if (attributeHidden) continue; // if attribute should be hidden, then skip
        this.addAttribute(keys, values, key, String(node.attributes[key]), index);
        index++;
      }
    }

    // position attribute containers
    container.x = 10;
    container.y = 45;
    values.x = keys.width + 10;

    // calculate width
    const width = this.calcAttrWidth(node.gfxAttributes) + 40;
    const height = container.height + 55;
    node.gfxAttributes.drawRoundedRect(0, 0, width, height, 10);

    // add stripe
    let gfxLine = new PIXI.Graphics();
    gfxLine.beginFill(0xbbbbbb);
    gfxLine.drawRect(10, 35, width - 20, 1);
    node.gfxAttributes.addChild(gfxLine);

    this.stage.addChild(node.gfxAttributes);
  };

  /**
   * Helper function of createAttributes: adds an attribute to the new attributes graphic
   */
  private addAttribute(
    keys: PIXI.Container,
    values: PIXI.Container,
    key: string,
    value: string,
    index: number,
  ): void {
    // add text for key
    let keyText = new PIXI.Text();
    keyText.style.fontSize = 14;
    keyText.style.fontFamily = 'Poppins, arial, sans-serif';
    keyText.style.fontWeight = 'bold';
    keyText.name = 'key';
    keyText.text = key + ':';
    keyText.y = index * 20;
    keys.addChild(keyText);
    // add text for value
    let valueText = new PIXI.Text();
    valueText.style.fontSize = 14;
    valueText.style.fontFamily = 'Poppins, arial, sans-serif';
    valueText.name = 'value';
    valueText.text = value;
    valueText.y = index * 20;
    values.addChild(valueText);
  }

  /**
   * Helper function of createAttributes: calculates the width of the attributes graphic
   */
  private calcAttrWidth(gfxAttributes: PIXI.Graphics): number {
    let width = 0;
    gfxAttributes.children.forEach((child) => {
      width = this.calcObjectWidth(child as PIXI.Text | PIXI.Graphics, width);
    });

    return width;
  }

  /**
   * Helper function of calcAttrWidth: calculates the width of a child element of the attributes graphic
   */
  private calcObjectWidth(object: PIXI.Text | PIXI.Graphics, minWidth?: number): number {
    let newWidth = object.width;
    return minWidth ? Math.max(newWidth, minWidth) : newWidth;
  }

  /**
   * The first click that starts the dragging.
   * @param event The mouseDown event.
   */
  private onDragStart = (event: any) => {
    event.subject.fx = event.subject.x;
    event.subject.fy = event.subject.y;
    this.dragOffset.x = event.subject.x;
    this.dragOffset.y = event.subject.y;

    // Toggle display of the attributes of the node
    let node = this.simulation.find(event.x, event.y, 15);

    // Null check
    if (node) {
      this.ToggleInformationOnNode(node);
    }
  };

  /**
   * Calls for the Display that pops up and highlights the node and the links
   * if ShortestPath turns on, it turns off the highlightedlinks!
   * @param node The node clicked in the event
   */
  public ToggleInformationOnNode(node: NodeType) {
    this.simulation.alphaTarget(0).restart(); // renderer will not always update without this line
    this.showAttributes(node);
    this.highlightNode(node);
    this.highlightLinks(node);
    this.showShortestPath();
  }

  /**
   * The actual drawing of the shortest path is done in the ticked method
   * This recalculates what should be shown and adds it to a list currentShortestPathEdges
   * Small note; the order in which nodes are clicked matters.
   * Also turns off highlightLinks
   * */
  public showShortestPath(): boolean {
    let shortestPathNodes: NodeType[] = [];
    this.currentHightlightedNodes.forEach((node) => {
      if (node.shortestPathData != undefined) {
        shortestPathNodes.push(node);
      }
    });
    if (shortestPathNodes.length < 2) {
      this.currentShortestPathEdges = [];
      return false;
    }
    let index = 0;
    let allPaths: LinkType[] = [];
    while (index < shortestPathNodes.length - 1) {
      let shortestPathData = shortestPathNodes[index].shortestPathData;
      if (shortestPathData === undefined) {
        console.warn('Something went wrong with shortest path calculation');
      } else {
        let path: string[] = shortestPathData[shortestPathNodes[index + 1].id];
        allPaths = allPaths.concat(this.getShortestPathEdges(path));
      }
      index++;
    }
    this.currentShortestPathEdges = allPaths;

    //This turns of the highlightedlinks
    this.currentlyHighlightedLinks = [];
    return true;
  }

  /**
   * Gets the edges corresponding to the shortestPath.
   * @param pathString The path as a string.
   * @returns The path as a LinkType[]
   */
  public getShortestPathEdges(pathString: string[]): LinkType[] {
    try {
      let newPath: LinkType[] = [];
      let index = 0;
      while (index < pathString.length) {
        if (pathString[index + 1] == undefined) {
          index++;
          continue;
        }
        let edgeFound = false;
        this.graph.links.forEach((link: any) => {
          const { source, target } = link;
          if (
            (pathString[index] == source.id && pathString[index + 1] == target.id) ||
            (pathString[index] == source && pathString[index + 1] == target) ||
            (pathString[index + 1] == source.id && pathString[index] == target.id) ||
            (pathString[index + 1] == source && pathString[index] == target)
          ) {
            newPath.push(link);
            edgeFound = true;
          }
        });
        if (!edgeFound) {
          console.warn('skipped path: ' + pathString[index] + ' ' + pathString[index + 1]);
        }
        index++;
      }
      return newPath;
    } catch {
      return [];
    }
  }

  /**
   * When mouseDown on a node, this is called.
   * @param event The moving of the mouse.
   */
  private onDragMove = (event: any) => {
    // this condition only passes when this is the first iteration of onDragMove after onDragStart
    if (!this.nodesMoving) {
      this.simulation.alphaTarget(0.3).restart(); // this makes all the nodes move
      this.nodesMoving = true;
    }

    // Moving the node
    if (this.scalexy != 1) {
      event.subject.fx = this.dragOffset.x - (this.dragOffset.x - event.x) / this.scalexy;
      event.subject.fy = this.dragOffset.y - (this.dragOffset.y - event.y) / this.scalexy;
    } else {
      event.subject.fx = event.x;
      event.subject.fy = event.y;
    }
  };

  /**
   * MouseUp on the node, stopping the drag.
   * @param event The mouseUp event.
   */
  private onDragEnd = (event: any) => {
    if (this.nodesMoving) {
      this.simulation.alphaTarget(0); // this will stop the nodes from moving
      this.nodesMoving = false;
    }
    event.subject.fx = null;
    event.subject.fy = null;
  };

  /**
   * Updates the currentlyHighlightedLinks value.
   * That value is used in the ticked function.
   * @param node The node clicked in the event.
   */
  public highlightLinks(node: NodeType) {
    //If node is already clicked we should remove it from the list.
    if (this.currentHightlightedNodes.includes(node)) {
      const index = this.currentHightlightedNodes.indexOf(node, 0);
      if (index > -1) {
        this.currentHightlightedNodes.splice(index, 1);
      }
    }
    //Else add it to the list.
    else this.currentHightlightedNodes.push(node);
    //Update the list of edges that need to be highlighted
    this.currentlyHighlightedLinks = this.getRelatedLinks(this.currentHightlightedNodes);
  }

  /**
   * When clicking on a node, toggle the select, making the text visible/invisible.
   * @param node The node which text should be toggled.
   */
  public showAttributes = (node: NodeType) => {
    if (node.selected) {
      if (node.gfxAttributes) {
        node.gfxAttributes.alpha = 0;
        node.selected = false;
      }
    } else {
      if (node.gfxAttributes) node.gfxAttributes.destroy();
      this.createAttributes(node);
      if (node.gfxAttributes) {
        node.gfxAttributes.scale.x = 1 / this.scalexy + 0.001 * this.scalexy;
        node.gfxAttributes.scale.y = 1 / this.scalexy + 0.001 * this.scalexy;
        node.gfxAttributes.alpha = 1;
        node.selected = true;
      }
    }
  };

  /** Highlight the selected node */
  public highlightNode = (node: NodeType) => {
    const radius = node.radius ? node.radius : this.radius;
    let item = this.stage.getChildByName('node_' + node.id);
    if (item != null) {
      item.destroy();
      this.createNode(node, radius, node.selected);
    }
  };

  /**
   * Used when you select nodes.
   * The highlight is drawn in ticked.
   * @param nodes The nodes you want to related edges to.
   * @returns {LinkType[]} All the links related to all the nodes
   */
  public getRelatedLinks(nodes: NodeType[]): LinkType[] {
    let relatedLinks: LinkType[] = [];
    this.graph.links.forEach((link: LinkType) => {
      const { source, target } = link;
      if (this.isLinkVisiable(link)) {
        nodes.forEach((node: NodeType) => {
          if (source == node || target == node || source == node.id || target == node.id) {
            relatedLinks.push(link);
          }
        });
      }
    });
    return relatedLinks;
  }

  /** When the screen gets resized, resize the node link canvas ViewModel. */
  public handleResize = () => {
    this.width = window.innerWidth;
    this.height = window.innerHeight - 6;
    this.renderer.resize(this.width, this.height);
    this.ticked();
    this.notifyViewAboutChanges();
  };

  /** Ticked is the updater function of the simulation. Every 'tick' all the new positions of the nodes (and thus the edges) are recalculated and updated. */
  public ticked = () => {
    this.graph.nodes.forEach((node: any) => {
      node.gfx.position = new PIXI.Point(node.x, node.y);
      // Update attributes position if they exist
      if (node.gfxAttributes) {
        node.gfxAttributes.position = new PIXI.Point(
          node.x - node.gfxAttributes.width / 2,
          node.y - node.gfxAttributes.height - 20,
        );
      }
    });

    this.links.clear();
    this.links.alpha = 0.6;

    // Update forces of the links
    this.graph.links.forEach((link: any) => {
      const { source, target } = link;
      this.links.lineStyle(0, 0x000000);
      // Check if link is a machine learning edge
      if (link.mlEdge && this.jaccardThreshold != null) {
        if (link.value > this.jaccardThreshold) {
          this.links.moveTo(source.x, source.y);
          this.links.lineTo(target.x, target.y);
          this.links.lineStyle(link.value * 1.8, 0x0000ff);
        }
      } else this.links.lineStyle(0.2, 0x000000);

      //Check if link is hightlighted
      if (this.currentlyHighlightedLinks.includes(link)) {
        //If highlighted and ML edge make it purple
        if (link.mlEdge && this.jaccardThreshold != null) {
          if (link.value > this.jaccardThreshold != null) {
            this.links.lineStyle(link.value * 1.8, 0xaa00ff);
          }
          //If highlight and not ML edge make it red
        } else {
          this.links.lineStyle(1.0, 0xff0000);
        }
      }

      if (this.currentShortestPathEdges.includes(link)) {
        this.links.lineStyle(3.0, 0x00ff00);
      }

      this.links.moveTo(source.x, source.y);
      this.links.lineTo(target.x, target.y);
    });

    this.links.endFill();

    // This updates the canvas
    this.renderer.render(this.stage);
  };

  /**
   * Colour is a function that takes a string of a number and returns a number of a color out of the d3 color scheme.
   * @param num Num is the input string representing a number of a colorgroup.
   * @returns {number} A number corresponding to a color in the d3 color scheme.
   */
  public colour = (num: number) => {
    //num = num % 4;
    let col = this.currentColours.nodes[num];
    return Number('0x' + col);
  };

  //MACHINE LEARNING--------------------------------------------------------------------------------------------------
  /**
   * updates the JacccardThresh value.
   * This is called in the component
   * This makes testing purposes easier and makes sure you dont have to read out the value 2000 times,
   * but only when you change the value.
   */
  public updateJaccardThreshHold(): void {
    let slider = document.getElementById('Slider');
    this.jaccardThreshold = Number(slider?.innerText);
  }

  /**
   * Checks wheter a link is visible.
   * This is used for highlighting nodes.
   * @param link The link you want to check wheter it's visable or not
   * @returns {boolean}
   */
  public isLinkVisiable(link: LinkType): boolean {
    //About the next line, If you don't do this here but lets say in the constructor it errors. So far no performance issues where noticed.
    if (link.mlEdge) {
      if (link.value > this.jaccardThreshold) {
        return true;
      }
    } else return true;
    return false;
  }

  /** initializeUniqueAttributes fills the uniqueAttributeValues with data from graph scheme analytics.
   * @param attributeData NodeAttributeData returned by graph scheme analytics.
   * @param attributeDataType Routing key.
   */
  public initializeUniqueAttributes(attributeData: AttributeData, attributeDataType: string): void {
    if (attributeDataType === 'gsa_node_result') {
      const entity = attributeData as NodeAttributeData;
      entity.attributes.forEach((attribute) => {
        if (attribute.type == AttributeCategory.categorical) {
          let nameAttribute = attribute.name;
          let valuesAttribute = attribute.uniqueCategoricalValues;
          // check if not null
          if (valuesAttribute) {
            this.uniqueAttributeValues[nameAttribute] = valuesAttribute;
          }
        }
      });
    }
  }

  /**
   * resetClusterOfNodes is a function that resets the cluster of the nodes that are being customised by the user,
   * after a community detection algorithm, where the cluster of these node could have been changed.
   */
  public resetClusterOfNodes = (type: number): void => {
    this.graph.nodes.forEach((node: NodeType) => {
      let numberOfClusters = this.numberOfMlClusters;
      if (node.cluster == type) {
        node.cluster = numberOfClusters;
      }
      if (node.type == type) {
        node.cluster = node.type;
      }
    });
  };
}
