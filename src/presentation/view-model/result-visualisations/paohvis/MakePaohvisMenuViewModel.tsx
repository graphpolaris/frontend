/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import BrokerListener from '../../../../domain/entity/broker/BrokerListenerInterface';
import { EntitiesFromSchema, NodeOrder, RelationsFromSchema } from '../../../../domain/entity/paohvis/structures/Types';
import BaseViewModel from '../../BaseViewModel';

/**
 * MakePaohvisMenuViewModel is an interface used to implement the ViewModel implementation of MakePaohvisMenuViewModelImpl.
 * Used to define all public fields used in the MakePaohvisMenuViewModelImpl.
 *
 * Extends BaseViewModel is an interface of ViewModel holding all public fields for all ViewModels.
 */
export default interface MakePaohvisMenuViewModel extends BaseViewModel, BrokerListener {
  entityVertical: string;
  entityHorizontal: string;
  relationName: string;
  isEntityVerticalEqualToRelationFrom: boolean;
  attributeNameAndOrigin: string;
  isSelectedAttributeFromEntity: boolean;
  isButtonEnabled: boolean;
  sortOrder: NodeOrder;
  isReverseOrder: boolean;

  entitiesFromSchema: EntitiesFromSchema;
  relationsFromSchema: RelationsFromSchema;
  entitiesFromQueryResult: string[];

  relationNameOptions: string[];
  attributeNameOptions: Record<string, string[]>;

  onClickMakeButton(): void;
  onChangeEntity(event: React.ChangeEvent<HTMLInputElement>): void;
  onChangeRelationName(event: React.ChangeEvent<HTMLInputElement>): void;
  onChangeAttributeName(event: React.ChangeEvent<HTMLInputElement>): void;
  onChangeSortOrder(event: React.ChangeEvent<HTMLInputElement>): void;
  onClickReverseOrder(): void;

  /** Subscribes this viewmodel to schema_result and query_result from the backend. Should be called when the view mounts. */
  subscribeToBroker(): void;

  /** Unsubscribes this viewmodel from schema_result and query_result from the backend. Should be called when the view unmounts. */
  unSubscribeFromBroker(): void;
}
