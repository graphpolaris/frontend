/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  Attribute,
  AttributeOrigin,
  EntitiesFromSchema,
  EntityOrigin,
  NodeOrder,
  PaohvisNodeOrder,
  RelationsFromSchema,
  ValueType,
} from '../../../../domain/entity/paohvis/structures/Types';
import AbstractBaseViewModelImpl from '../../AbstractBaseViewModelImpl';
import MakePaohvisMenuViewModel from './MakePaohvisMenuViewModel';
import * as d3 from 'd3-v6';
import { AttributeNames } from '../../../../domain/entity/semantic-substrates/structures/Types';
import { isNodeLinkResult } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import React from 'react';
import Broker from '../../../../domain/entity/broker/broker';
import { isSchemaResult } from '../../../../domain/entity/graph-schema/structures/SchemaResultType';
import calcEntitiesFromQueryResult from '../../../../domain/usecases/paohvis/CalcEntitiesFromQueryResult';
import CalcAttrRelNamesFromSchemaUseCase from '../../../../domain/usecases/paohvis/CalcEntityAttrAndRelNamesFromSchemaUseCase';

/** MakePaohvisMenuViewModelImpl is an implementation of MakePaohvisMenuViewModel. */
export default class MakePaohvisMenuViewModelImpl
  extends AbstractBaseViewModelImpl
  implements MakePaohvisMenuViewModel
{
  makePaohvis: (
    entityVertical: string,
    entityHorizontal: string,
    relationName: string,
    isEntityFromRelationFrom: boolean,
    chosenAttribute: Attribute,
    nodeOrder: PaohvisNodeOrder,
  ) => void;

  entityVertical: string;
  entityHorizontal: string;
  relationName: string;
  isEntityVerticalEqualToRelationFrom: boolean;
  attributeNameAndOrigin: string;
  isSelectedAttributeFromEntity: boolean;
  isButtonEnabled: boolean;
  sortOrder: NodeOrder;
  isReverseOrder: boolean;

  entitiesFromSchema: EntitiesFromSchema;
  relationsFromSchema: RelationsFromSchema;

  relationNameOptions: string[];
  attributeNameOptions: Record<string, string[]>;
  entitiesFromQueryResult: string[];

  relationValue = '?';
  attributeValue = '?';
  possibleAttrValues: string[] = [];

  public constructor(
    makePaohvis: (
      entityVertical: string,
      entityHorizontal: string,
      relationName: string,
      isEntityFromRelationFrom: boolean,
      chosenAttribute: Attribute,
      nodeOrder: PaohvisNodeOrder,
    ) => void,
  ) {
    super();

    this.makePaohvis = makePaohvis;
    this.entityVertical = '';
    this.entityHorizontal = '';
    this.relationName = '';
    this.isEntityVerticalEqualToRelationFrom = true;
    this.attributeNameAndOrigin = '';
    this.isSelectedAttributeFromEntity = false;
    this.isButtonEnabled = false;
    this.entitiesFromQueryResult = [];
    this.sortOrder = NodeOrder.degree;
    this.isReverseOrder = false;

    this.entitiesFromSchema = { entityNames: [], attributesPerEntity: {}, relationsPerEntity: {} };
    this.relationsFromSchema = {
      relationCollection: [],
      relationNames: {},
      attributesPerRelation: {},
    };
    this.relationNameOptions = [];
    this.attributeNameOptions = {};
  }

  /**
   * Will be called by the broker if a query_result or schema_result is available.
   * This object is subscribed with the query_result and schema_routing routingkey.
   * @param message from backend.
   */
  public consumeMessageFromBackend(message: unknown): void {
    if (isSchemaResult(message)) {
      this.resetConfig();

      this.entitiesFromSchema =
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesAndRelations(message);
      this.relationsFromSchema =
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesFromRelation(message);

      this.notifyViewAboutChanges();
    } else if (isNodeLinkResult(message)) {
      this.resetConfig();

      this.entitiesFromQueryResult = calcEntitiesFromQueryResult(message);
      this.notifyViewAboutChanges();
    }
  }

  /** This resets the configuration. Should be called when the possible entities and relations change. */
  private resetConfig(): void {
    this.entityVertical = '';
    this.entityHorizontal = '';
    this.relationName = '';
    this.isEntityVerticalEqualToRelationFrom = true;
    this.attributeNameAndOrigin = '';
    this.isSelectedAttributeFromEntity = false;
    this.isButtonEnabled = false;
    this.entitiesFromQueryResult = [];

    this.relationNameOptions = [];
    this.attributeNameOptions = {};
  }

  /** Subscribes this viewmodel to schema_result and query_result from the backend. Should be called when the view mounts. */
  public subscribeToBroker(): void {
    Broker.instance().subscribe(this, 'schema_result');
    Broker.instance().subscribe(this, 'query_result');
  }

  /** Unsubscribes this viewmodel from schema_result and query_result from the backend. Should be called when the view unmounts. */
  public unSubscribeFromBroker(): void {
    Broker.instance().unSubscribe(this, 'schema_result');
    Broker.instance().unSubscribe(this, 'query_result');
  }

  /**
   * Called when the entity field is changed.
   * Calculates the `relationNameOptions`, resets the `entityHorizontal`, `relationName` and `attributeName` and sets the new state.
   * @param {React.ChangeEvent<HTMLInputElement>} event The event that is given by the input field when a change event is fired.
   * It has the value of the entity you clicked on.
   */
  public onChangeEntity = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const newEntity = event.target.value;
    this.relationNameOptions = [];
    const relationNames: string[] = this.entitiesFromSchema.relationsPerEntity[newEntity];

    // check if there are any relations
    if (relationNames) {
      // push all relation options to relationNameOptions
      relationNames.forEach((relation) => {
        let relationSplit = this.relationsFromSchema.relationNames[relation].split(':');

        //check if relation is valid
        if (
          this.entitiesFromQueryResult.includes(relationSplit[0]) &&
          this.entitiesFromQueryResult.includes(relationSplit[1])
        ) {
          // check if relation is selfedge
          if (relationSplit[0] == relationSplit[1]) {
            const relationFrom = `${relation}:${EntityOrigin.from}`;
            const relationTo = `${relation}:${EntityOrigin.to}`;
            if (
              !this.relationNameOptions.includes(relationFrom) &&
              !this.relationNameOptions.includes(relationTo)
            ) {
              this.relationNameOptions.push(relationFrom);
              this.relationNameOptions.push(relationTo);
            }
          } else this.relationNameOptions.push(relation);
        }
      });
      // filter out duplicates
      this.relationNameOptions = this.relationNameOptions.filter(
        (n, i) => this.relationNameOptions.indexOf(n) === i,
      );
    }

    this.relationValue = '';
    this.attributeValue = '';

    this.entityVertical = newEntity;
    this.entityHorizontal = '';
    this.relationName = '';
    this.isEntityVerticalEqualToRelationFrom = true;
    this.attributeNameAndOrigin = '';
    this.isButtonEnabled = false;
    this.attributeNameOptions = {};

    this.notifyViewAboutChanges();
  };

  /**
   * Called when the relationName field is changed.
   * Calculates the possible attribute values, resets the `attributeValue`, sets the `entityHorizontal`.
   * @param {React.ChangeEvent<HTMLInputElement>} event The event that is given by the input field when a change event is fired.
   * It has the value of the relation you clicked on.
   */
  public onChangeRelationName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.relationName = event.target.value;
    this.attributeValue = '';
    this.attributeNameAndOrigin = '';
    this.isButtonEnabled = false;

    const newRelationSplit = event.target.value.split(':');
    const newRelation = newRelationSplit[0];

    // This value should always be there
    this.attributeNameOptions[AttributeOrigin.noAttribute] = [ValueType.noAttribute];

    const relationNames = this.relationsFromSchema.relationNames[newRelation];
    if (relationNames == undefined) throw new Error('This entity does not exist in the schema.');

    const relationSplit: string[] = relationNames.split(':');
    const entityVertical = this.entityVertical;

    // check if relation is self edge
    if (relationSplit[0] == relationSplit[1]) {
      const entityOrigin = newRelationSplit[1];

      if (!this.isValidEntityOrigin(entityOrigin))
        throw new Error(
          `The entity "${entityVertical}" has an invalid entity origin: ${entityOrigin}.`,
        );

      this.isEntityVerticalEqualToRelationFrom = entityOrigin == EntityOrigin.from;
      this.entityHorizontal = relationSplit[0];
    }
    // check if entityVertical is the entity in the 'from' of the relation
    else if (entityVertical == relationSplit[0]) {
      this.isEntityVerticalEqualToRelationFrom = true;
      this.entityHorizontal = relationSplit[1];
    }
    // check if entityVertical is the entity in the 'to' of the relation
    else if (entityVertical == relationSplit[1]) {
      this.isEntityVerticalEqualToRelationFrom = false;
      this.entityHorizontal = relationSplit[0];
    } else
      throw new Error(
        `The relationNames from this.relationsFromSchema for ${newRelation} is invalid`,
      );

    this.attributeNameOptions[AttributeOrigin.entity] = [];

    if (this.entitiesFromSchema.attributesPerEntity[this.entityHorizontal]) {
      let allAttributesOfEntity: AttributeNames =
        this.entitiesFromSchema.attributesPerEntity[this.entityHorizontal];

      let attributeNamesOfEntity: string[] = allAttributesOfEntity.textAttributeNames
        .concat(allAttributesOfEntity.numberAttributeNames)
        .concat(allAttributesOfEntity.boolAttributeNames);
      this.attributeNameOptions[AttributeOrigin.entity] = attributeNamesOfEntity;
    }

    this.attributeNameOptions[AttributeOrigin.relation] = [];

    if (this.relationsFromSchema.attributesPerRelation[newRelation]) {
      let allAttributesOfRelation: AttributeNames =
        this.relationsFromSchema.attributesPerRelation[newRelation];

      let attributeNamesOfRelation: string[] = allAttributesOfRelation.textAttributeNames
        .concat(allAttributesOfRelation.numberAttributeNames)
        .concat(allAttributesOfRelation.boolAttributeNames);
      this.attributeNameOptions[AttributeOrigin.relation] = attributeNamesOfRelation;
    }

    this.notifyViewAboutChanges();
  };

  /**
   * Called when the attributeName field is changed.
   * Sets the chosen attribute value, enables the "Make" button and sets the state.
   * @param {React.ChangeEvent<HTMLInputElement>} event The event that is given by the input field when a change event is fired.
   * It has the value of the attributeName you clicked on.
   */
  public onChangeAttributeName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const newAttribute = event.target.value;

    const isSelectedAttributeFromEntity = newAttribute.split(':')[1] == AttributeOrigin.entity;
    this.attributeValue = newAttribute;

    // render changes
    this.attributeNameAndOrigin = newAttribute;
    this.isSelectedAttributeFromEntity = isSelectedAttributeFromEntity;
    this.isButtonEnabled = true;
    this.notifyViewAboutChanges();
  };

  /**
   * Called when the sort order field is changed.
   * Sets the state by changing the sort order.
   * @param {React.ChangeEvent<HTMLInputElement>} event The event that is given by the input field when a change event is fired.
   */
  public onChangeSortOrder = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const newSortOrder: NodeOrder = event.target.value as NodeOrder;
    const nodeOrders: string[] = Object.values(NodeOrder);
    if (nodeOrders.includes(newSortOrder)) {
      this.unflipReverseIconButton();

      // render changes
      this.sortOrder = newSortOrder;
      this.isReverseOrder = false;
      this.notifyViewAboutChanges();
    } else throw new Error(newSortOrder + ' is not a sort order.');
  };

  /**
   * Called when the user clicks the reverse order button.
   * Sets the state and changes the svg of the button.
   * @param {React.ChangeEvent<HTMLInputElement>} event The event that is given by the input field when a change event is fired.
   */
  public onClickReverseOrder = (): void => {
    //change the svg of the button to be the reversed variant
    this.isReverseOrder = !this.isReverseOrder;
    const isReverseOrder = this.isReverseOrder;
    switch (this.sortOrder) {
      case NodeOrder.alphabetical:
        if (isReverseOrder) this.flipReverseIconButtonLabel();
        else this.unflipReverseIconButtonLabel();
        break;
      default:
        if (isReverseOrder) this.flipReverseIconButton();
        else this.unflipReverseIconButton();
        break;
    }

    this.notifyViewAboutChanges();
  };

  /**
   * Called when the user clicks the "Make" button.
   * Checks if all fields are valid before calling `makePaohvis()`.
   */
  public onClickMakeButton = (): void => {
    const relationName = this.relationName.split(':')[0];

    if (
      this.entitiesFromSchema.entityNames.includes(this.entityVertical) &&
      this.entitiesFromSchema.relationsPerEntity[this.entityVertical].includes(relationName)
    ) {
      const attributeNameAndOrigin: string[] = this.attributeNameAndOrigin.split(':');
      const attributeName: string = attributeNameAndOrigin[0];
      const attributeOrigin: AttributeOrigin = attributeNameAndOrigin[1] as AttributeOrigin;
      const chosenAttribute: Attribute = {
        name: attributeName,
        type: this.getTypeOfAttribute(attributeName),
        origin: attributeOrigin,
      };

      this.makePaohvis(
        this.entityVertical,
        this.entityHorizontal,
        relationName,
        this.isEntityVerticalEqualToRelationFrom,
        chosenAttribute,
        {
          orderBy: this.sortOrder,
          isReverseOrder: this.isReverseOrder,
        },
      );
    } else {
      this.isButtonEnabled = false;
      this.notifyViewAboutChanges();
      throw new Error('Error: chosen entity or relation is invalid.');
    }
  };

  /**
   * Gets the type of the value of the specified attribute by .
   * @param attributeName The name of the specified attribute.
   * @returns {ValueType} The type of the value of the specified attribute.
   */
  private getTypeOfAttribute(attributeName: string): ValueType {
    // get the correct AttributeNames for the attribute
    const attributeNames = this.isSelectedAttributeFromEntity
      ? this.entitiesFromSchema.attributesPerEntity[this.entityHorizontal]
      : this.relationsFromSchema.attributesPerRelation[this.relationName.split(':')[0]];

    // look up to which ValueType the attribute belongs
    if (attributeNames.boolAttributeNames.includes(attributeName)) return ValueType.bool;
    if (attributeNames.numberAttributeNames.includes(attributeName)) return ValueType.number;
    if (attributeNames.textAttributeNames.includes(attributeName)) return ValueType.text;
    if (attributeName == ValueType.noAttribute) return ValueType.noAttribute;

    throw new Error('Attribute ' + attributeName + ' does not exist');
  }

  /**
   * Checks if the given string is a valid EntityOrigin.
   * @param entityOrigin string that should be checked.
   * @returns {boolean} returns true if the given string is a valid EntityOrigin.
   */
  private isValidEntityOrigin(entityOrigin: string): boolean {
    return entityOrigin == EntityOrigin.from || entityOrigin == EntityOrigin.to;
  }

  /**
   * Called when the user clicks the reverse order button when `Alphabetical` is selected.
   * Changes the svg of the button to indicate that the order is reversed.
   */
  private flipReverseIconButtonLabel(): void {
    d3.select('.reverseIconButton').select('#reverseButtonLabel').text('Z-A');
  }
  /**
   * Called when the user clicks the reverse order button when `Alphabetical` is selected.
   * Changes the svg of the button back to normal.
   */
  private unflipReverseIconButtonLabel(): void {
    d3.select('.reverseIconButton').select('#reverseButtonLabel').text('A-Z');
  }
  /**
   * Called when the user clicks the reverse order button when `Degree` is selected.
   * Changes the svg of the button to indicate that the order is reversed.
   */
  private flipReverseIconButton(): void {
    d3.select('.reverseIconButton').transition().style('transform', 'scaleY(-1)');
  }
  /**
   * Called when the user clicks the reverse order button when `Degree` is selected.
   * Changes the svg of the button back to normal.
   */
  private unflipReverseIconButton(): void {
    d3.select('.reverseIconButton').transition().style('transform', 'scaleY(1)');
  }
}
