/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import PaohvisListener from '../../../../domain/entity/paohvis/models/PaohvisListener';
import BrokerListener from '../../../../domain/entity/broker/BrokerListenerInterface';
import {
  EntitiesFromSchema,
  FilterType,
  RelationsFromSchema,
} from '../../../../domain/entity/paohvis/structures/Types';
import BaseViewModel from '../../BaseViewModel';

/**
 * PaohvisFilterViewModel is an interface used to implement the ViewModel implementation of PaohvisFilterViewModelImpl.
 * Used to define all public fields used in the PaohvisFilterViewModelImpl.
 *
 * Extends BaseViewModel is an interface of ViewModel holding all public fields for all ViewModels.
 */
export default interface PaohvisFilterViewModel
  extends BaseViewModel,
    PaohvisListener,
    BrokerListener {
  isTargetEntity: boolean;
  filterTarget: string;
  attributeNameAndType: string;
  attributeType: string;
  predicate: string;
  compareValue: any;
  isFilterButtonEnabled: boolean;
  predicateTypeList: string[];
  attributeNamesOptions: string[];

  onChangeAttributeName(event: React.ChangeEvent<HTMLInputElement>): void;
  onChangePredicate(event: React.ChangeEvent<HTMLInputElement>): void;
  onChangeCompareValue(event: React.ChangeEvent<HTMLInputElement>): void;
  onClickFilterPaohvisButton(): void;
  onClickResetFilter(): void;

  /** Subscribes this viewmodel to schema_result and query_result from the backend. Should be called when the view mounts. */
  subscribeToBroker(): void;

  /** Unsubscribes this viewmodel from schema_result and query_result from the backend. Should be called when the view unmounts. */
  unSubscribeFromBroker(): void;
}
