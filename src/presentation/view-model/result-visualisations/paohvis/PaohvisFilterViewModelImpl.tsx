/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import Broker from '../../../../domain/entity/broker/broker';
import AbstractBaseViewModelImpl from '../../AbstractBaseViewModelImpl';
import PaohvisFilterViewModel from './PaohvisFilterViewModel';
import {
  AttributeNames,
  EntitiesFromSchema,
  FilterType,
  RelationsFromSchema,
} from '../../../../domain/entity/paohvis/structures/Types';
import { isSchemaResult } from '../../../../domain/entity/graph-schema/structures/SchemaResultType';
import CalcAttrRelNamesFromSchemaUseCase from '../../../../domain/usecases/paohvis/CalcEntityAttrAndRelNamesFromSchemaUseCase';
import { isNodeLinkResult } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import {
  boolPredicates,
  numberPredicates,
  textPredicates,
} from '../../../../domain/entity/paohvis/models/FilterPredicates';

/** The configuration fields for one Paohvis. */
type PaohvisFilterViewModelImplProps = {
  axis: FilterType;
  filterPaohvis(
    isTargetEntity: boolean,
    filterTarget: string,
    attributeName: string,
    predicate: string,
    compareValue: string,
  ): void;
  resetFilter(isTargetEntity: boolean): void;
};

/** PaohvisFilterViewModelImpl is an implementation of PaohvisFilterViewModel. */
export default class PaohvisFilterViewModelImpl
  extends AbstractBaseViewModelImpl
  implements PaohvisFilterViewModel
{
  isTargetEntity: boolean;
  filterTarget: string;
  attributeNameAndType: string;
  attributeType: string;
  predicate: string;
  compareValue: string;
  isFilterButtonEnabled: boolean;
  predicateTypeList: string[];
  attributeNamesOptions: string[];

  namesPerEntityOrRelation: string[];
  attributesPerEntityOrRelation: Record<string, AttributeNames>;

  axis: FilterType;
  filterPaohvis: (
    isTargetEntity: boolean,
    entityOrRelationType: string,
    attributeName: string,
    predicate: string,
    compareValue: string,
  ) => void;
  resetFilter: (isTargetEntity: boolean) => void;

  public constructor(props: PaohvisFilterViewModelImplProps) {
    super();

    this.isTargetEntity = props.axis != FilterType.edge;
    this.filterTarget = '';
    this.attributeNameAndType = '';
    this.attributeType = '';
    this.predicate = '';
    this.compareValue = '';
    this.isFilterButtonEnabled = false;
    this.predicateTypeList = [];
    this.attributeNamesOptions = [];

    this.namesPerEntityOrRelation = [];
    this.attributesPerEntityOrRelation = {};

    this.axis = props.axis;
    this.filterPaohvis = props.filterPaohvis;
    this.resetFilter = props.resetFilter;
  }

  /**
   * Will be called by the broker if a query_result or schema_result is available.
   * This object is subscribed with the query_result and schema_routing routingkey.
   * @param message from backend.
   */
  public consumeMessageFromBackend(message: unknown): void {
    if (isSchemaResult(message)) {
      if (this.isTargetEntity) {
        const entitiesFromSchema: EntitiesFromSchema =
          CalcAttrRelNamesFromSchemaUseCase.calculateAttributesAndRelations(message);

        this.namesPerEntityOrRelation = entitiesFromSchema.entityNames;
        this.attributesPerEntityOrRelation = entitiesFromSchema.attributesPerEntity;
      } else {
        const relationsFromSchema: RelationsFromSchema =
          CalcAttrRelNamesFromSchemaUseCase.calculateAttributesFromRelation(message);

        this.namesPerEntityOrRelation = relationsFromSchema.relationCollection;
        this.attributesPerEntityOrRelation = relationsFromSchema.attributesPerRelation;
      }

      this.filterTarget = '';
      this.attributeNamesOptions = [];
      this.notifyViewAboutChanges();
    } else if (isNodeLinkResult(message)) {
      this.filterTarget = '';
      this.attributeNamesOptions = [];
      this.notifyViewAboutChanges();
    }
  }

  /** Subscribes this viewmodel to schema_result and query_result from the backend. Should be called when the view mounts. */
  public subscribeToBroker(): void {
    Broker.instance().subscribe(this, 'schema_result');
    Broker.instance().subscribe(this, 'query_result');
  }

  /** Unsubscribes this viewmodel from schema_result and query_result from the backend. Should be called when the view unmounts. */
  public unSubscribeFromBroker(): void {
    Broker.instance().unSubscribe(this, 'schema_result');
    Broker.instance().unSubscribe(this, 'query_result');
  }

  /**
   * This is called whenever a Paohvis table is made.
   * @param entityVertical that is on the y-axis of the Paohis table.
   * @param entityHorizontal that is on the x-axis of the Paohis table.
   * @param relationName that is used as the hyperedge of the Paohis table.
   */
  public onTableMade(entityVertical: string, entityHorizontal: string, relationName: string): void {
    this.determineFilterTarget(entityVertical, entityHorizontal, relationName);
    this.updateAttributeNameOptions();
    this.notifyViewAboutChanges();
  }

  /**
   * Updates the list of attributes that can be chosen from for the filter component.
   * Takes all attributes from the filter target and converts them to the format 'name:type'.
   */
  private updateAttributeNameOptions() {
    const filterTarget = this.filterTarget;
    const namesPerEntityOrRelation = this.namesPerEntityOrRelation;
    const attributesPerEntityOrRelation = this.attributesPerEntityOrRelation;

    if (!namesPerEntityOrRelation.includes(filterTarget))
      throw new Error('The filter target does not exist in the schema');

    // Add all possible options for attributes to attributeNamesOptions
    // check all text attributes
    if (attributesPerEntityOrRelation[filterTarget].textAttributeNames.length > 0)
      attributesPerEntityOrRelation[filterTarget].textAttributeNames.map((attributeName) =>
        this.attributeNamesOptions.push(`${attributeName}:text`),
      );

    // check all number attributes
    if (attributesPerEntityOrRelation[filterTarget].numberAttributeNames.length > 0)
      attributesPerEntityOrRelation[filterTarget].numberAttributeNames.map((attributeName) =>
        this.attributeNamesOptions.push(`${attributeName}:number`),
      );

    // check all bool attributes
    if (attributesPerEntityOrRelation[filterTarget].boolAttributeNames.length > 0)
      attributesPerEntityOrRelation[filterTarget].boolAttributeNames.map((attributeName) =>
        this.attributeNamesOptions.push(`${attributeName}:bool`),
      );
  }

  /**
   * This is called when the name of the attribute is changed in a filter component.
   * Based on the type of the chosen attribute, a different list with predicates is generated.
   * @param event that called this eventhandler.
   */
  public onChangeAttributeName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const newAttributeNameAndType = event.target.value;
    const newAttributeSplit = event.target.value.split(':');
    const newAttributeType = newAttributeSplit[1];

    if (!this.containsFilterTargetChosenAttribute(newAttributeNameAndType))
      throw new Error(
        'The chosen attribute does not exist in the entity/relation that will be filtered',
      );

    switch (newAttributeType) {
      case 'text':
        this.predicateTypeList = Object.keys(textPredicates);
        break;
      case 'number':
        this.predicateTypeList = Object.keys(numberPredicates);
        break;
      case 'bool':
        this.predicateTypeList = Object.keys(boolPredicates);
        break;
    }

    this.attributeNameAndType = newAttributeNameAndType;
    this.attributeType = newAttributeType;
    this.predicate = '';
    this.compareValue = '';
    this.isFilterButtonEnabled = false;

    this.notifyViewAboutChanges();
  };

  /**
   * This is called when the value of the predicate is changed in the filter component.
   * @param event that called this eventhandler.
   */
  public onChangePredicate = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const newpredicate = event.target.value;

    if (!this.isPredicateValid(newpredicate)) throw new Error('The chosen predicate is invalid');

    this.predicate = newpredicate;
    this.compareValue = '';
    this.isFilterButtonEnabled = false;

    this.notifyViewAboutChanges();
  };

  /**
   * This is called when the value to compare the attribute with is changed in a filter component.
   * Based on the type of the chosen attribute, the compareValue has some conditions on what type it should be.
   * If attribute is numerical, the value has to be a number, etc.
   * @param event that called this eventhandler.
   */
  public onChangeCompareValue = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.compareValue = event.target.value;
    this.isFilterButtonEnabled = this.isFilterConfigurationValid();
    this.notifyViewAboutChanges();
  };

  /**
   * This is called when the "apply filter" button is clicked.
   * It checks if the input is correct and sends the information for the filters to the PaohvisViewModelImpl.
   */
  public onClickFilterPaohvisButton = (): void => {
    if (!this.isFilterConfigurationValid())
      throw new Error('Error: chosen attribute of predicate-value is invalid.');

    this.filterPaohvis(
      this.isTargetEntity,
      this.filterTarget,
      this.attributeNameAndType,
      this.predicate,
      this.compareValue,
    );
  };

  /**
   * This resets the filters for the chosen filter component.
   */
  public onClickResetFilter = (): void => {
    this.attributeNameAndType = '';
    this.attributeType = '';
    this.predicate = '';
    this.compareValue = '';
    this.isFilterButtonEnabled = false;
    this.predicateTypeList = [];
    this.resetFilter(this.isTargetEntity);

    this.notifyViewAboutChanges();
  };

  /**
   * Checks if the filter configuration is valid.
   * @returns {boolean} true if the filter configuration is valid.
   */
  private isFilterConfigurationValid(): boolean {
    return (
      this.containsFilterTargetChosenAttribute(this.attributeNameAndType) &&
      this.isPredicateValid(this.predicate) &&
      this.isCompareValueTypeValid(this.compareValue)
    );
  }
  /**
   * Checks if the given predicate is valid for the filter target.
   * @param predicate that should be checked.
   * @returns {boolean} true if the predicate is valid.
   */
  private isPredicateValid(predicate: string): boolean {
    return this.predicateTypeList.includes(predicate);
  }

  /**
   * This is used when the compareValue has changed.
   * @param {string} compareValue is the value that will be used with the predicate.
   * @returns {boolean} true if the chosen compareValue and chosen attribute are a valid combination.
   */
  private isCompareValueTypeValid(compareValue: string): boolean {
    const lowerCaseCompareValue: string = compareValue.toLowerCase();
    return (
      (this.attributeType == 'number' && !Number.isNaN(Number(compareValue))) ||
      (this.attributeType == 'bool' &&
        (lowerCaseCompareValue == 'true' || lowerCaseCompareValue == 'false')) ||
      (this.attributeType == 'text' && compareValue.length > 0)
    );
  }

  /**
   * This determines whether the given attribute belongs to the filter target.
   * @param attributeNameAndType is the chosen attribute in the format of 'name:type'
   * @returns {boolean} true when the given attribute belongs to the filter target.
   */
  private containsFilterTargetChosenAttribute(attributeNameAndType: string): boolean {
    const filterTarget = this.filterTarget;
    const split: string[] = attributeNameAndType.split(':');
    const attributeName = split[0];
    const attributeType = split[1];
    const attributesPerEntityOrRelation = this.attributesPerEntityOrRelation;

    switch (attributeType) {
      case 'text':
        return attributesPerEntityOrRelation[filterTarget].textAttributeNames.includes(
          attributeName,
        );
      case 'number':
        return attributesPerEntityOrRelation[filterTarget].numberAttributeNames.includes(
          attributeName,
        );
      case 'bool':
        return attributesPerEntityOrRelation[filterTarget].boolAttributeNames.includes(
          attributeName,
        );
      default:
        return false;
    }
  }

  /**
   * This determines on which entity/relation the filter is going to be applied to.
   * @param entityVertical is the entity type that belongs to the Y-axis of the Paohvis table.
   * @param entityHorizontal is the entity type that belongs to the X-axis of the Paohvis table.
   * @param relationName is the relation type that is that is displayed in the Paohvis table
   */
  private determineFilterTarget(
    entityVertical: string,
    entityHorizontal: string,
    relationName: string,
  ): void {
    if (this.axis == FilterType.yaxis) this.filterTarget = entityVertical;
    else if (this.axis == FilterType.xaxis) this.filterTarget = entityHorizontal;
    else this.filterTarget = relationName;
  }
}
