/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import {
  PaohvisData,
  EntitiesFromSchema,
  RelationsFromSchema,
  PaohvisFilters,
  PaohvisNodeOrder,
  Attribute,
} from '../../../../domain/entity/paohvis/structures/Types';
import ResultVisViewModel from '../ResultVisViewModel';
import { NodeLinkResultType } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import MakePaohvisMenuViewModel from './MakePaohvisMenuViewModel';
import PaohvisFilterViewModel from './PaohvisFilterViewModel';

/**
 * PaohvisViewModel is an interface used to implement the ViewModel implementation of the PaohvisViewModelImplementation.
 * Used to define all public fields used in the PaohvisViewModelImplementation.
 *
 * Extends BaseViewModel is an interface of ViewModel holding all public fields for all ViewModels.
 */
export default interface PaohvisViewModel extends ResultVisViewModel {
  makePaohvisMenuViewModel: MakePaohvisMenuViewModel;
  xAxisPaohvisFilterViewModel: PaohvisFilterViewModel;
  yAxisPaohvisFilterViewModel: PaohvisFilterViewModel;
  relationPaohvisFilterViewModel: PaohvisFilterViewModel;

  rowHeight: number;
  hyperedgeColumnWidth: number;
  gapBetweenRanges: number;

  data: PaohvisData;

  hyperedgesOnRow: number[][];
  allHyperEdges: number[][];

  nodeLinkQueryResult: NodeLinkResultType;
  // The entities with names and attribute parameters from the schema
  entitiesFromSchema: EntitiesFromSchema;
  relationsFromSchema: RelationsFromSchema;
  entityVertical: string;
  entityHorizontal: string;
  chosenRelation: string;
  nodeOrder: PaohvisNodeOrder;
  paohvisFilters: PaohvisFilters;

  recalculate(): void;
  onMouseEnterRow(rowIndex: number): void;
  onMouseLeaveRow(rowIndex: number): void;
  onMouseEnterHyperEdge(colIndex: number, nameToShow: string): void;
  onMouseLeaveHyperEdge(colIndex: number): void;
  onMouseMoveToolTip(e: React.MouseEvent): void;

  onClickMakeButton(
    entityVertical: string,
    entityHorizontal: string,
    relationName: string,
    isEntityVerticalEqualToRelationFrom: boolean,
    chosenAttribute: Attribute,
    nodeOrder: PaohvisNodeOrder,
  ): void;

  onClickFilterButton(
    isTargetEntity: boolean,
    entityOrRelationType: string,
    attribute: string,
    predicate: string,
    compareValue: string,
  ): void;

  onClickResetButton(isTargetEntity: boolean): void;

  /** Subscribes this viewmodel to schema_result from the backend. Should be called when the view mounts. */
  subscribeToSchemaResult(): void;

  /** Unsubscribes this viewmodel from schema_result from the backend. Should be called when the view unmounts. */
  unSubscribeFromSchemaResult(): void;
}
