/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import AbstractResultVisViewModelImpl from '../AbstractResultVisViewModelImpl';
import PaohvisViewModel from './PaohvisViewModel';
import {
  isNodeLinkResult,
  NodeLinkResultType,
} from '../../../../domain/entity/query-result/structures/NodeLinkResultType';

import {
  PaohvisData,
  EntitiesFromSchema,
  RelationsFromSchema,
  NodeOrder,
  PaohvisFilters,
  FilterInfo,
  PaohvisNodeOrder,
  Attribute,
  PaohvisAxisInfo,
  Relation,
  AttributeOrigin,
  ValueType,
  FilterType,
} from '../../../../domain/entity/paohvis/structures/Types';
import CalcAttrRelNamesFromSchemaUseCase from '../../../../domain/usecases/paohvis/CalcEntityAttrAndRelNamesFromSchemaUseCase';
import { isSchemaResult } from '../../../../domain/entity/graph-schema/structures/SchemaResultType';
import Broker from '../../../../domain/entity/broker/broker';

import ToPaohvisDataParserUseCase from '../../../../domain/usecases/paohvis/ToPaohvisDataParserUsecase';

import * as d3 from 'd3-v6';
import React from 'react';
import PaohvisFilterViewModel from './PaohvisFilterViewModel';
import MakePaohvisMenuViewModel from './MakePaohvisMenuViewModel';
import PaohvisFilterViewModelImpl from './PaohvisFilterViewModelImpl';
import MakePaohvisMenuViewModelImpl from './MakePaohvisMenuViewModelImpl';
import PaohvisHolder from '../../../../domain/entity/paohvis/models/PaohvisHolder';

/** PaohvisViewModelIImpl is an implementation of PaohvisViewModel. */
export default class PaohvisViewModelImpl
  extends AbstractResultVisViewModelImpl
  implements PaohvisViewModel
{
  rowHeight = 30;
  hyperedgeColumnWidth = 20;
  gapBetweenRanges = 5;

  data: PaohvisData = { rowLabels: [], hyperEdgeRanges: [], maxRowLabelWidth: 0 };
  hyperedgesOnRow: number[][] = [];
  allHyperEdges: number[][] = [];

  public nodeLinkQueryResult: NodeLinkResultType;
  public entitiesFromSchema: EntitiesFromSchema;
  public relationsFromSchema: RelationsFromSchema;
  public entityVertical: string;
  public entityHorizontal: string;
  public chosenRelation: string;
  public isEntityVerticalEqualToRelationFrom: boolean;
  public nodeOrder: PaohvisNodeOrder;
  public paohvisFilters: PaohvisFilters;

  private axisInfo: PaohvisAxisInfo;
  private toPaohvisDataParserUseCase: ToPaohvisDataParserUseCase;

  public makePaohvisMenuViewModel: MakePaohvisMenuViewModel;
  public xAxisPaohvisFilterViewModel: PaohvisFilterViewModel;
  public yAxisPaohvisFilterViewModel: PaohvisFilterViewModel;
  public relationPaohvisFilterViewModel: PaohvisFilterViewModel;
  private paohvisHolder: PaohvisHolder;

  public constructor() {
    super();

    this.makePaohvisMenuViewModel = new MakePaohvisMenuViewModelImpl(this.onClickMakeButton);

    // make the 3 PaohvisFilterViewModels
    this.xAxisPaohvisFilterViewModel = new PaohvisFilterViewModelImpl({
      axis: FilterType.xaxis,
      filterPaohvis: this.onClickFilterButton,
      resetFilter: this.onClickResetButton,
    });
    this.yAxisPaohvisFilterViewModel = new PaohvisFilterViewModelImpl({
      axis: FilterType.yaxis,
      filterPaohvis: this.onClickFilterButton,
      resetFilter: this.onClickResetButton,
    });
    this.relationPaohvisFilterViewModel = new PaohvisFilterViewModelImpl({
      axis: FilterType.edge,
      filterPaohvis: this.onClickFilterButton,
      resetFilter: this.onClickResetButton,
    });

    // setup the observer for the PaohvisFilterViewModels
    this.paohvisHolder = new PaohvisHolder();
    this.paohvisHolder.addListener(this.xAxisPaohvisFilterViewModel);
    this.paohvisHolder.addListener(this.yAxisPaohvisFilterViewModel);
    this.paohvisHolder.addListener(this.relationPaohvisFilterViewModel);

    this.nodeLinkQueryResult = { nodes: [], edges: [] };
    this.entitiesFromSchema = { entityNames: [], attributesPerEntity: {}, relationsPerEntity: {} };
    this.relationsFromSchema = {
      relationCollection: [],
      relationNames: {},
      attributesPerRelation: {},
    };
    this.entityVertical = '';
    this.entityHorizontal = '';
    this.chosenRelation = '';
    this.isEntityVerticalEqualToRelationFrom = true;
    this.nodeOrder = {
      orderBy: NodeOrder.degree,
      isReverseOrder: false,
    };
    this.paohvisFilters = { nodeFilters: [], edgeFilters: [] };
    this.axisInfo = {
      selectedAttribute: {
        name: '',
        type: ValueType.noAttribute,
        origin: AttributeOrigin.noAttribute,
      },
      relation: { collection: '', from: '', to: '' },
      isYAxisEntityEqualToRelationFrom: true,
    };

    this.toPaohvisDataParserUseCase = new ToPaohvisDataParserUseCase(this.nodeLinkQueryResult);
  }

  /**
   * Will be called by the broker if a query_result or schema_result is available.
   * This object is subscribed with the query_result and schema_routing routingkey.
   * @param message from backend.
   */
  public consumeMessageFromBackend(message: unknown): void {
    // check if the message is a (valid) nodelink-result.
    if (isNodeLinkResult(message)) {
      if (this.entitiesFromSchema.entityNames.length == 0) {
        throw new Error('Paohvis: No valid schema available.');
      }
      this.nodeLinkQueryResult = message;
      this.toPaohvisDataParserUseCase = new ToPaohvisDataParserUseCase(message);

      //reset table
      this.data = { rowLabels: [], hyperEdgeRanges: [], maxRowLabelWidth: 0 };
      this.recalculate();

      this.notifyViewAboutChanges();
    } else if (isSchemaResult(message)) {
      // When a schema is received, extract the entity names, and attributes per data type
      this.entitiesFromSchema =
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesAndRelations(message);
      this.relationsFromSchema =
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesFromRelation(message);

      this.notifyViewAboutChanges();
    }
  }

  /**
   * Recalculates the hyperEdges and hyperEdgeRanges for the state.
   */
  public recalculate(): void {
    // Create connecteHyperedgesInColumns, to quickly lookup which hyperedges are on a row
    this.allHyperEdges = [];

    this.data.hyperEdgeRanges.forEach((hyperEdgeRange) => {
      hyperEdgeRange.hyperEdges.forEach((hyperEdge) => {
        this.allHyperEdges.push(hyperEdge.indices);
      });
    });

    this.hyperedgesOnRow = this.data.rowLabels.map(() => []);
    this.allHyperEdges.forEach((hyperedge, i) => {
      hyperedge.forEach((row) => {
        this.hyperedgesOnRow[row].push(i);
      });
    });
  }

  /**
   * Handles the visual changes when you enter a certain row on hovering.
   * @param {number} rowIndex This is the index which states in which row you are hovering.
   */
  onMouseEnterRow = (rowIndex: number): void => {
    const rowsToHighlight = new Set<number>();
    const colsToHighlight: number[] = [];
    this.hyperedgesOnRow[rowIndex].forEach((hyperedge) => {
      colsToHighlight.push(hyperedge);

      this.allHyperEdges[hyperedge].forEach((row) => {
        rowsToHighlight.add(row);
      });
    });

    this.highlightAndFadeRows(rowsToHighlight);
    this.highlightAndFadeHyperEdges(colsToHighlight);
  };

  /**
   * Handles the visual changes when you leave a certain row on hovering.
   * @param {number} rowIndex This is the index which states in which row you were hovering.
   */
  onMouseLeaveRow = (rowIndex: number): void => {
    const rowsToUnHighlight = new Set<number>();
    const colsToUnHighlight: number[] = [];
    this.hyperedgesOnRow[rowIndex].forEach((hyperedge) => {
      colsToUnHighlight.push(hyperedge);

      this.allHyperEdges[hyperedge].forEach((row) => {
        rowsToUnHighlight.add(row);
      });
    });
    this.unHighlightAndUnFadeRows(rowsToUnHighlight);
    this.unHighlightAndUnFadeHyperEdges(colsToUnHighlight);
  };

  /**
   * Handles the visual changes when you enter a certain hyperEdge on hovering.
   * @param colIndex This is the index which states in which column you are hovering.
   * @param nameToShow This is the name of the entity which must be shown when you are hovering on this certain hyperEdge.
   */
  onMouseEnterHyperEdge = (colIndex: number, nameToShow: string): void => {
    this.highlightAndFadeHyperEdges([colIndex]);
    this.highlightAndFadeRows(new Set(this.allHyperEdges[colIndex]));
    this.showColName(nameToShow);
  };

  /**
   * Handles the visual changes when you leave a certain hyperEdge on hovering.
   * @param colIndex This is the index which states in which column you were hovering.
   */
  onMouseLeaveHyperEdge = (colIndex: number): void => {
    this.unHighlightAndUnFadeHyperEdges([colIndex]);
    this.unHighlightAndUnFadeRows(new Set(this.allHyperEdges[colIndex]));
    this.hideColName();
  };

  /**
   * This calculates a new position based on the current position of the mouse.
   * @param {React.MouseEvent} e This is the position of the mouse.
   */
  onMouseMoveToolTip = (e: React.MouseEvent) => {
    d3.select('.tooltip')
      .style('top', d3.pointer(e)[1] - 25 + 'px')
      .style('left', d3.pointer(e)[0] + 5 + 'px');
  };

  /**
   * This makes sure that the correct rows are highlighted and the correct rows are faded.
   * @param {Set<number>} rows This is the set with the numbers of the rows which must be highlighted.
   */
  private highlightAndFadeRows(rows: Set<number>) {
    rows.forEach((row) => this.highlightRow(row));

    const rowsToFade = [];
    for (let i = 0; i < this.data.rowLabels.length; i++) if (!rows.has(i)) rowsToFade.push(i);
    rowsToFade.forEach((row) => this.fadeRow(row));
  }

  /**
   * This makes sure that the correct rows are unhighlighted and the correct rows are unfaded.
   * @param {Set<number>} rows This is the set with the numbers of the rows which must be unhighlighted.
   */
  private unHighlightAndUnFadeRows(rows: Set<number>) {
    rows.forEach((row) => this.unHighlightRow(row));

    const rowsToUnFade = [];
    for (let i = 0; i < this.data.rowLabels.length; i++) if (!rows.has(i)) rowsToUnFade.push(i);
    rowsToUnFade.forEach((row) => this.unFadeRow(row));
  }

  /**
   * This makes sure that the correct hyperEdges are highlighted and the correct rows are faded.
   * @param {number[]} hyperEdges This is the list with the numbers of the hyperEdges which must be highlighted.
   */
  private highlightAndFadeHyperEdges(hyperEdges: number[]) {
    hyperEdges.forEach((hyperEdge) => this.highlightHyperedge(hyperEdge));

    const colsToFade = [];
    for (let i = 0; i < this.allHyperEdges.length; i++)
      if (!hyperEdges.includes(i)) colsToFade.push(i);
    colsToFade.forEach((col) => this.fadeHyperedge(col));
  }

  /**
   * This makes sure that the correct hyperEdges are unhighlighted and the correct rows are unfaded.
   * @param {number[]} hyperEdges This is the list with the numbers of the hyperEdges which must be unhighlighted.
   */
  private unHighlightAndUnFadeHyperEdges(hyperEdges: number[]) {
    hyperEdges.forEach((hyperEdge) => this.unHighlightHyperedge(hyperEdge));

    const colsToUnFade = [];
    for (let i = 0; i < this.allHyperEdges.length; i++)
      if (!hyperEdges.includes(i)) colsToUnFade.push(i);
    colsToUnFade.forEach((col) => this.unFadeHyperedge(col));
  }

  /**
   * This highlights one row given its index.
   * @param {number} rowIndex This is the index of the row that must be highlighted.
   */
  private highlightRow(rowIndex: number) {
    const row = d3.selectAll('.row-' + rowIndex);
    row.select('text').transition().duration(120).style('font-size', '15px');
  }

  /**
   * This unhighlights one row given its index.
   * @param {number} rowIndex This is the index of the row that must be unhighlighted.
   */
  private unHighlightRow(rowIndex: number) {
    const row = d3.selectAll('.row-' + rowIndex);
    row.select('text').transition().duration(120).style('font-size', '14px');
  }

  /**
   * This highlights one hyperEdge given its index.
   * @param {number} columnIndex This is the index of the hyperEdge that must be highlighted.
   */
  private highlightHyperedge(columnIndex: number) {
    const hyperedge = d3.select('.col-' + columnIndex);
    hyperedge.selectAll('circle').transition().duration(120).style('fill', 'orange');
  }

  /**
   * This unhighlights one hyperEdge given its index.
   * @param {number} columnIndex This is the index of the hyperEdge that must be unhighlighted.
   */
  private unHighlightHyperedge(columnIndex: number) {
    const hyperedge = d3.select('.col-' + columnIndex);
    hyperedge.selectAll('circle').transition().duration(120).style('fill', 'white');
  }

  /**
   * This fades one row given its index.
   * @param {number} rowIndex This is the index of the row that must be faded.
   */
  private fadeRow(rowIndex: number) {
    const row = d3.selectAll('.row-' + rowIndex);
    row.select('text').attr('opacity', '.3');
  }

  /**
   * This unfades one row given its index.
   * @param {number} rowIndex This is the index of the row that must be unfaded.
   */
  private unFadeRow(rowIndex: number) {
    const row = d3.selectAll('.row-' + rowIndex);
    row.select('text').attr('opacity', '1');
  }

  /**
   * This fades one hyperEdge given its index.
   * @param {number} columnIndex This is the index of the hyperEdge that must be faded.
   */
  private fadeHyperedge(columnIndex: number) {
    const hyperedge = d3.select('.col-' + columnIndex);
    hyperedge.selectAll('circle').attr('opacity', '.3');
    hyperedge.selectAll('line').attr('opacity', '.3');
  }

  /**
   * This unfades one hyperEdge given its index.
   * @param {number} columnIndex This is the index of the hyperEdge that must be unfaded.
   */
  private unFadeHyperedge(columnIndex: number) {
    const hyperedge = d3.select('.col-' + columnIndex);
    hyperedge.selectAll('circle').attr('opacity', '1');
    hyperedge.selectAll('line').attr('opacity', '1');
  }

  /**
   * This shows the name when hovering on a hyperEdge.
   * @param {string} nameToShow This is the name that must be shown.
   */
  private showColName = (nameToShow: string): void => {
    d3.select('.tooltip').text(nameToShow).style('visibility', 'visible');
  };

  /** This hides the name when leaving a hyperEdge. */
  private hideColName = (): void => {
    d3.select('.tooltip').style('visibility', 'hidden');
  };

  /**
   * Makes the new PAOHvis visualisation.
   * @param {string} entityVertical This is the name of the vertical entity (so on the left).
   * @param {string} entityHorizontal This is the name of the horizontal entity (so at the top).
   * @param {string} relationName This is the (collection)-name of the relation.
   * @param {boolean} isEntityVerticalEqualToRelationFrom Tells if the vertical entity is the from or to of the relation.
   * @param {Attribute} chosenAttribute This is the attribute on which the PAOHvis must be grouped by.
   * @param {PaohvisNodeOrder} nodeOrder Defines the sorting order of the PAOHvis visualisation.
   */
  public onClickMakeButton = (
    entityVertical: string,
    entityHorizontal: string,
    relationName: string,
    isEntityVerticalEqualToRelationFrom: boolean,
    chosenAttribute: Attribute,
    nodeOrder: PaohvisNodeOrder,
  ): void => {
    this.entityVertical = entityVertical;
    this.entityHorizontal = entityHorizontal;
    this.chosenRelation = relationName;
    this.isEntityVerticalEqualToRelationFrom = isEntityVerticalEqualToRelationFrom;
    this.nodeOrder = nodeOrder;
    this.setAxisInfo(relationName, isEntityVerticalEqualToRelationFrom, chosenAttribute);

    this.makePaohvisTable();

    this.paohvisHolder.onTableMade(entityVertical, entityHorizontal, relationName);
  };

  /**
   * This method parses data, makes a new Paohvis Table and lets the view re-render.
   */
  private makePaohvisTable() {
    // set new data
    this.data = this.toPaohvisDataParserUseCase.parseQueryResult(this.axisInfo, this.nodeOrder);

    // update view
    this.recalculate();
    this.notifyViewAboutChanges();
  }

  /**
   * This method makes and sets a new PaohvisAxisInfo.
   * @param relationName is the relation that will be used in the Paohvis table.
   * @param isEntityVerticalEqualToRelationFrom is true when the entity on the y-axis belongs to the 'from' part of the relation.
   * @param chosenAttribute is the chosen attribute that will be used to divide the HyperEdges into HyperEdgeRanges.
   */
  private setAxisInfo(
    relationName: string,
    isEntityVerticalEqualToRelationFrom: boolean,
    chosenAttribute: Attribute,
  ): void {
    const namesOfEntityTypes = this.relationsFromSchema.relationNames[relationName].split(':');
    const relation: Relation = {
      collection: relationName,
      from: namesOfEntityTypes[0],
      to: namesOfEntityTypes[1],
    };

    // set axisInfo
    this.axisInfo = {
      selectedAttribute: chosenAttribute,
      relation: relation,
      isYAxisEntityEqualToRelationFrom: isEntityVerticalEqualToRelationFrom,
    };
  }

  /** Subscribes this viewmodel to schema_result from the backend. Should be called when the view mounts. */
  public subscribeToSchemaResult(): void {
    Broker.instance().subscribe(this, 'schema_result');
  }

  /** Unsubscribes this viewmodel from schema_result from the backend. Should be called when the view unmounts. */
  public unSubscribeFromSchemaResult(): void {
    Broker.instance().unSubscribe(this, 'schema_result');
  }

  /**
   * Filters the current PAOHvis visualisation.
   * @param {boolean} isTargetEntity Tells if the the filter you want to remove is applied to an entity or relation.
   * @param {string} entityOrRelationType This is the type of the target.
   * @param {string} attribute This is the chosen attribute of the target you want to filter on.
   * @param {string} predicate This is the chosen predicate of how you want to compare the attribute.
   * @param {string} compareValue This is the chosen value which you want to compare the attribute values to.
   */
  public onClickFilterButton = (
    isTargetEntity: boolean,
    entityOrRelationType: string,
    attribute: string,
    predicate: string,
    compareValue: string,
  ): void => {
    const attributeName: string = attribute.split(':')[0];
    const attributeType: string = attribute.split(':')[1];
    const lowerCaseCompareValue = compareValue.toLowerCase();
    let compareValueTyped: any;
    // the compareValue must be typed based on the type of the attribute.
    switch (attributeType) {
      case ValueType.number:
        if (!Number.isNaN(Number(compareValue))) compareValueTyped = Number(compareValue);
        else throw new Error('Error: This is not a correct input');
        break;
      case ValueType.bool:
        if (lowerCaseCompareValue == 'true') compareValueTyped = true;
        else if (lowerCaseCompareValue == 'false') compareValueTyped = false;
        else throw new Error('Error: This is not a correct input');
        break;
      default:
        compareValueTyped = compareValue;
        break;
    }
    const newFilter: FilterInfo = {
      targetGroup: entityOrRelationType,
      attributeName: attributeName,
      value: compareValueTyped,
      predicateName: predicate,
    };

    if (isTargetEntity) {
      // The current filter is removed and then the new one is added.
      // TODO: to keep all filters, delete this line and improve config menu to see all filters instead of only the last one.
      this.paohvisFilters.nodeFilters.pop();
      this.paohvisFilters.nodeFilters.push(newFilter);
    } else {
      // The current filter is removed and then the new one is added.
      // TODO: to keep all filters, delete this line and improve config menu to see all filters instead of only the last one.
      this.paohvisFilters.edgeFilters.pop();
      this.paohvisFilters.edgeFilters.push(newFilter);
    }

    this.filterPaohvis();
  };

  /**
   * Resets the current chosen filter.
   * @param {boolean} isTargetEntity Tells if the the filter you want to remove is applied to an entity or relation.
   */
  public onClickResetButton = (isTargetEntity: boolean): void => {
    if (isTargetEntity) this.paohvisFilters.nodeFilters.pop();
    else this.paohvisFilters.edgeFilters.pop();

    this.filterPaohvis();
  };

  /** This method filters and makes a new Paohvis table. */
  private filterPaohvis(): void {
    this.toPaohvisDataParserUseCase.setPaohvisFilters(this.paohvisFilters);
    this.makePaohvisTable();
  }
}
