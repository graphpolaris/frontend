/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  queryResultParliament,
  queryResultFlights,
} from '../../../../../data/mock-data/paohvis/mockInputData';
import { parliamentData2, flights } from '../../../../../data/mock-data/paohvis/mockSchemaResult';
import Broker from '../../../../../domain/entity/broker/broker';
import { Schema } from '../../../../../domain/entity/graph-schema/structures/InputDataTypes';
import { isSchemaResult } from '../../../../../domain/entity/graph-schema/structures/SchemaResultType';
import {
  AttributeOrigin,
  EntitiesFromSchema,
  EntityOrigin,
  NodeOrder,
  RelationsFromSchema,
  ValueType,
} from '../../../../../domain/entity/paohvis/structures/Types';
import calcEntitiesFromQueryResult from '../../../../../domain/usecases/paohvis/CalcEntitiesFromQueryResult';
import CalcEntityAttrAndRelNamesFromSchemaUseCase from '../../../../../domain/usecases/paohvis/CalcEntityAttrAndRelNamesFromSchemaUseCase';
import MakePaohvisMenuViewModel from '../MakePaohvisMenuViewModel';
import MakePaohvisMenuViewModelImpl from '../MakePaohvisMenuViewModelImpl';

const emptyEntitiesFromSchema: EntitiesFromSchema = {
  entityNames: [],
  attributesPerEntity: {},
  relationsPerEntity: {},
};
const emptyRelationsFromSchema: RelationsFromSchema = {
  relationCollection: [],
  relationNames: {},
  attributesPerRelation: {},
};

const makePaohvis = jest.fn();

describe('MakePaohvisMenuViewModel', () => {
  //setup viewmodel
  const makePaohvisMenuViewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);
  makePaohvisMenuViewModel.consumeMessageFromBackend(parliamentData2);
  makePaohvisMenuViewModel.consumeMessageFromBackend(queryResultParliament);

  it('should return the names of all relations from the query result that are connected with the chosen entity for asymmetric relations', () => {
    // This event contains the name of the entity that is on the left side (from) of the relation to which it is connected
    let event = {
      target: {
        value: 'parliament',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeEntity(event);

    expect(makePaohvisMenuViewModel.relationNameOptions).toEqual(['part_of', 'member_of']);

    // This event contains the name of the entity that is on the right side (to) of the relation to which it is connected
    event = {
      target: {
        value: 'parties',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeEntity(event);

    expect(makePaohvisMenuViewModel.relationNameOptions).toEqual(['member_of']);
  });
});

describe('MakePaohvisMenuViewModel', () =>
  it('should return the names of all relations from the query result that are connected with the chosen entity for symmetric relations', () => {
    const makePaohvisMenuViewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);

    makePaohvisMenuViewModel.consumeMessageFromBackend(flights);
    makePaohvisMenuViewModel.consumeMessageFromBackend(queryResultFlights);

    // This event contains the name of the entity that is on the left side (from) of the relation to which it is connected
    let event = {
      target: {
        value: 'airports',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeEntity(event);

    expect(makePaohvisMenuViewModel.relationNameOptions).toEqual([
      'flights:' + EntityOrigin.from,
      'flights:' + EntityOrigin.to,
    ]);
    expect(makePaohvisMenuViewModel.relationValue).toEqual('');
    expect(makePaohvisMenuViewModel.attributeValue).toEqual('');
  }));

describe('MakePaohvisMenuViewModel', () =>
  it('should return the names of all attributes from the chosen entity counterpart for symmetric relations + the attributes of the chosen relation', () => {
    const makePaohvisMenuViewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);

    makePaohvisMenuViewModel.consumeMessageFromBackend(parliamentData2);
    makePaohvisMenuViewModel.consumeMessageFromBackend(queryResultParliament);

    let event = {
      target: {
        value: 'parliament',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeEntity(event);
    // This event contains the relation of which the chosen entity is the left part (from)
    event = {
      target: {
        value: 'member_of',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeRelationName(event);
    expect(makePaohvisMenuViewModel.relationName).toEqual('member_of');
    expect(
      makePaohvisMenuViewModel.attributeNameAndOrigin == '' &&
        !makePaohvisMenuViewModel.isButtonEnabled,
    ).toEqual(true);
    expect(makePaohvisMenuViewModel.attributeNameOptions['No attribute']).toEqual(['No attribute']);
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('chairmanId');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('name');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('seats');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('chairman');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('isChairman');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('amountOfTimes');
    expect(makePaohvisMenuViewModel.attributeValue).toEqual('');

    // This event contains the name of the entity that is on the right side (to) of the relation to which it is connected
    event = {
      target: {
        value: 'parties',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeEntity(event);

    event = {
      target: {
        value: 'member_of',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeRelationName(event);
    expect(makePaohvisMenuViewModel.relationName).toEqual('member_of');
    expect(
      makePaohvisMenuViewModel.attributeNameAndOrigin == '' &&
        !makePaohvisMenuViewModel.isButtonEnabled,
    ).toEqual(true);
    expect(makePaohvisMenuViewModel.attributeNameOptions['No attribute']).toEqual(['No attribute']);
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('age');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('img');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('party');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('residence');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('seniority');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('name');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('weight');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('isChairman');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('isChairman');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('amountOfTimes');
  }));

describe('MakePaohvisMenuViewModel', () => {
  const makePaohvisMenuViewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);

  makePaohvisMenuViewModel.consumeMessageFromBackend(flights);
  makePaohvisMenuViewModel.consumeMessageFromBackend(queryResultFlights);

  it('should return the names of all attributes from the chosen entity counterpart for symmetric relations + the attributes of the chosen relation', () => {
    let event = {
      target: {
        value: 'airports',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeEntity(event);
    // This event contains the relation of which the chosen entity is the left part (from)
    event = {
      target: {
        value: 'flights:' + EntityOrigin.from,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeRelationName(event);
    expect(makePaohvisMenuViewModel.relationName).toEqual('flights:' + EntityOrigin.from);
    expect(
      makePaohvisMenuViewModel.attributeNameAndOrigin == '' &&
        !makePaohvisMenuViewModel.isButtonEnabled,
    ).toEqual(true);
    expect(makePaohvisMenuViewModel.attributeNameOptions['No attribute']).toContain('No attribute');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('city');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('country');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('lat');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('long');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('name');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('state');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('vip');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('ArrTime');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('ArrTimeUTC');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Day');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('DayOfWeek');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('DepTime');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('DepTimeUTC');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Distance');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('FlightNum');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Month');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('TailNum');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('UniqueCarrier');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Year');

    // This event contains the relation of which the chosen entity is the right part (to)
    event = {
      target: {
        value: 'flights:' + EntityOrigin.to,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeRelationName(event);
    expect(makePaohvisMenuViewModel.relationName).toEqual('flights:' + EntityOrigin.to);
    expect(
      makePaohvisMenuViewModel.attributeNameAndOrigin == '' &&
        !makePaohvisMenuViewModel.isButtonEnabled,
    ).toEqual(true);
    expect(makePaohvisMenuViewModel.attributeNameOptions['No attribute']).toContain('No attribute');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('city');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('country');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('lat');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('long');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('name');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('state');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Entity']).toContain('vip');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('ArrTime');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('ArrTimeUTC');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Day');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('DayOfWeek');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('DepTime');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('DepTimeUTC');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Distance');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('FlightNum');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Month');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('TailNum');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('UniqueCarrier');
    expect(makePaohvisMenuViewModel.attributeNameOptions['Relation']).toContain('Year');
  });

  it('should throw an error when the onChangeRelationName event is invalid', () => {
    let event = {
      target: {
        value: 'flights:bbb',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    expect(() => makePaohvisMenuViewModel.onChangeRelationName(event)).toThrowError(
      'The entity "airports" has an invalid entity origin: bbb.',
    );
    expect(
      makePaohvisMenuViewModel.attributeNameAndOrigin == '' &&
        !makePaohvisMenuViewModel.isButtonEnabled,
    ).toEqual(true);

    event = {
      target: {
        value: 'Gibberish:' + EntityOrigin.to,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    expect(() => makePaohvisMenuViewModel.onChangeRelationName(event)).toThrowError(
      'This entity does not exist in the schema.',
    );
    expect(
      makePaohvisMenuViewModel.attributeNameAndOrigin == '' &&
        !makePaohvisMenuViewModel.isButtonEnabled,
    ).toEqual(true);
  });

  it('onChangeAttributeName should change values', () => {
    expect(makePaohvisMenuViewModel.attributeNameAndOrigin).toEqual('');
    expect(makePaohvisMenuViewModel.isSelectedAttributeFromEntity).toEqual(false);
    expect(makePaohvisMenuViewModel.isButtonEnabled).toEqual(false);

    // attribute from relation
    let event = {
      target: {
        value: 'Year:' + AttributeOrigin.relation,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeAttributeName(event);

    expect(makePaohvisMenuViewModel.attributeNameAndOrigin).toEqual(
      'Year:' + AttributeOrigin.relation,
    );
    expect(makePaohvisMenuViewModel.isSelectedAttributeFromEntity).toEqual(false);
    expect(makePaohvisMenuViewModel.isButtonEnabled).toEqual(true);

    // attribute from entity
    event = {
      target: {
        value: 'name:' + AttributeOrigin.entity,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeAttributeName(event);

    expect(makePaohvisMenuViewModel.attributeNameAndOrigin).toEqual(
      'name:' + AttributeOrigin.entity,
    );
    expect(makePaohvisMenuViewModel.isSelectedAttributeFromEntity).toEqual(true);
    expect(makePaohvisMenuViewModel.isButtonEnabled).toEqual(true);
  });

  it('should reverse sort order', () => {
    expect(makePaohvisMenuViewModel.sortOrder).toEqual(NodeOrder.degree);
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(false);
    makePaohvisMenuViewModel.onClickReverseOrder();
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(true);
    makePaohvisMenuViewModel.onClickReverseOrder();
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(false);

    // change sort order to alphabetical
    const event = {
      target: {
        value: NodeOrder.alphabetical,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeSortOrder(event);

    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(false);
    makePaohvisMenuViewModel.onClickReverseOrder();
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(true);
    makePaohvisMenuViewModel.onClickReverseOrder();
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(false);
  });

  it('should change sort order', () => {
    // change sort order to alphabetical
    let event = {
      target: {
        value: NodeOrder.alphabetical,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeSortOrder(event);

    expect(makePaohvisMenuViewModel.sortOrder).toEqual(NodeOrder.alphabetical);
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(false);

    // set isReverseOrder to true to check if it will be set to false when sort order is changed
    makePaohvisMenuViewModel.onClickReverseOrder();
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(true);

    // change sort order to degree
    event = {
      target: {
        value: NodeOrder.degree,
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenuViewModel.onChangeSortOrder(event);

    expect(makePaohvisMenuViewModel.sortOrder).toEqual(NodeOrder.degree);
    expect(makePaohvisMenuViewModel.isReverseOrder).toEqual(false);

    // change to invalid sort order
    event = {
      target: {
        value: 'aaa',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    expect(() => makePaohvisMenuViewModel.onChangeSortOrder(event)).toThrowError(
      'aaa is not a sort order.',
    );
  });

  it('onClickMakeButton', () => {
    // valid settings
    makePaohvisMenuViewModel.onChangeEntity({
      target: {
        value: 'airports',
      },
    } as React.ChangeEvent<HTMLInputElement>);
    makePaohvisMenuViewModel.onChangeRelationName({
      target: {
        value: 'flights:' + EntityOrigin.to,
      },
    } as React.ChangeEvent<HTMLInputElement>);
    makePaohvisMenuViewModel.onChangeAttributeName({
      target: {
        value: 'name:' + AttributeOrigin.entity,
      },
    } as React.ChangeEvent<HTMLInputElement>);
    makePaohvisMenuViewModel.onChangeSortOrder({
      target: {
        value: NodeOrder.alphabetical,
      },
    } as React.ChangeEvent<HTMLInputElement>);

    expect(makePaohvis).toHaveBeenCalledTimes(0);
    makePaohvisMenuViewModel.onClickMakeButton();
    expect(makePaohvis).toHaveBeenCalledTimes(1);

    // check for number attribute
    makePaohvisMenuViewModel.onChangeAttributeName({
      target: {
        value: 'lat:' + AttributeOrigin.entity,
      },
    } as React.ChangeEvent<HTMLInputElement>);
    makePaohvisMenuViewModel.onClickMakeButton();
    expect(makePaohvis).toHaveBeenCalledTimes(2);

    // check for bool attribute
    makePaohvisMenuViewModel.onChangeAttributeName({
      target: {
        value: 'vip:' + AttributeOrigin.entity,
      },
    } as React.ChangeEvent<HTMLInputElement>);
    makePaohvisMenuViewModel.onClickMakeButton();
    expect(makePaohvis).toHaveBeenCalledTimes(3);

    // check for no attribute
    makePaohvisMenuViewModel.onChangeAttributeName({
      target: {
        value: 'No attribute',
      },
    } as React.ChangeEvent<HTMLInputElement>);
    makePaohvisMenuViewModel.onClickMakeButton();
    expect(makePaohvis).toHaveBeenCalledTimes(4);

    // invalid attribute
    makePaohvisMenuViewModel.onChangeAttributeName({
      target: {
        value: 'aaa',
      },
    } as React.ChangeEvent<HTMLInputElement>);

    expect(() => makePaohvisMenuViewModel.onClickMakeButton()).toThrowError(
      'Attribute aaa does not exist',
    );
    expect(makePaohvis).toHaveBeenCalledTimes(4);

    // invalid relation
    makePaohvisMenuViewModel.entityVertical = 'aaa';
    expect(() => makePaohvisMenuViewModel.onClickMakeButton()).toThrowError(
      'Error: chosen entity or relation is invalid.',
    );
    expect(makePaohvis).toHaveBeenCalledTimes(4);

    // invalid entity
    makePaohvisMenuViewModel.relationName = 'aaa';
    expect(() => makePaohvisMenuViewModel.onClickMakeButton()).toThrowError(
      'Error: chosen entity or relation is invalid.',
    );
    expect(makePaohvis).toHaveBeenCalledTimes(4);
  });
});

describe('MakePaohvisMenuViewModel', () => {
  it('consumeMessageFromBackend', () => {
    const makePaohvisMenuViewModel: MakePaohvisMenuViewModel = new MakePaohvisMenuViewModelImpl(
      makePaohvis,
    );
    expect(makePaohvisMenuViewModel.relationsFromSchema).toEqual(emptyRelationsFromSchema);
    expect(makePaohvisMenuViewModel.entitiesFromSchema).toEqual(emptyEntitiesFromSchema);
    expect(makePaohvisMenuViewModel.entitiesFromQueryResult).toEqual([]);

    makePaohvisMenuViewModel.subscribeToBroker();
    Broker.instance().publish(parliamentData2, 'schema_result');
    // relationsFromSchema and entitiesFromSchema should be calculated correctly when a valid schema result comes in
    if (isSchemaResult(parliamentData2)) {
      const resultRelationsFromSchema =
        CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(parliamentData2);
      const resultEntitiesFromSchema =
        CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesAndRelations(parliamentData2);

      expect(makePaohvisMenuViewModel.relationsFromSchema).toEqual(resultRelationsFromSchema);
      expect(makePaohvisMenuViewModel.entitiesFromSchema).toEqual(resultEntitiesFromSchema);

      // relationsFromSchema and entitiesFromSchema should not change when an invalid schema result comes in
      Broker.instance().publish('very invalid', 'schema_result');
      expect(makePaohvisMenuViewModel.relationsFromSchema).toEqual(resultRelationsFromSchema);
      expect(makePaohvisMenuViewModel.entitiesFromSchema).toEqual(resultEntitiesFromSchema);
    }

    // entitiesFromQueryResult should be calculated correctly when a valid query result comes in
    const resultEntitiesFromQueryResult = calcEntitiesFromQueryResult(queryResultParliament);
    Broker.instance().publish(queryResultParliament, 'query_result');
    expect(makePaohvisMenuViewModel.entitiesFromQueryResult).toEqual(resultEntitiesFromQueryResult);

    // entitiesFromQueryResult should not change when an invalid query result comes in
    Broker.instance().publish('super invalid', 'query_result');
    expect(makePaohvisMenuViewModel.entitiesFromQueryResult).toEqual(resultEntitiesFromQueryResult);

    makePaohvisMenuViewModel.unSubscribeFromBroker();
  });

  it('subscription to broker', () => {
    const viewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);
    const mockConsumeMessageFromBackend = jest.fn();
    viewModel.consumeMessageFromBackend = mockConsumeMessageFromBackend;

    // should consume messages from backend when subscribed to broker
    viewModel.subscribeToBroker();
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(2); // 2 from the earlier invalid backend messages
    Broker.instance().publish('hi', 'schema_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(3);
    Broker.instance().publish('hi', 'query_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(4);

    // should not consume messages from backend anymore when unsubscribed from broker
    viewModel.unSubscribeFromBroker();
    Broker.instance().publish('hi', 'schema_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(4);
    Broker.instance().publish('hi', 'query_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(4);
  });

  describe('schema with no relations', () => {
    const viewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);
    const schemaWithNoRelations: Schema = { edges: [], nodes: [{ name: 'name', attributes: [] }] };
    viewModel.consumeMessageFromBackend(schemaWithNoRelations);

    viewModel.onChangeEntity({
      target: {
        value: 'name',
      },
    } as React.ChangeEvent<HTMLInputElement>);
    expect(viewModel.relationNameOptions).toEqual([]);
  });

  describe('onChangeRelationName', () => {
    it('invalid event', () => {
      const viewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);
      viewModel.consumeMessageFromBackend(parliamentData2);
      viewModel.consumeMessageFromBackend(queryResultParliament);

      viewModel.onChangeEntity({
        target: {
          value: 'parliament',
        },
      } as React.ChangeEvent<HTMLInputElement>);

      viewModel.relationsFromSchema.relationNames = { member_of: 'this is invalid' };
      expect(() =>
        viewModel.onChangeRelationName({
          target: {
            value: 'member_of',
          },
        } as React.ChangeEvent<HTMLInputElement>),
      ).toThrowError('The relationNames from this.relationsFromSchema for member_of is invalid');
    });

    it('no attributes', () => {
      const viewModel = new MakePaohvisMenuViewModelImpl(makePaohvis);
      const schemaWithNoAttributes = {
        edges: [
          {
            name: 'a:b',
            to: 'a',
            from: 'b',
            collection: 'collection1',
            attributes: [],
          },
          {
            name: 'a:b',
            to: 'a',
            from: 'b',
            collection: 'collection2',
            attributes: [{ name: 'hasAttribute', type: 'bool' }],
          },
        ],
        nodes: [
          {
            name: 'a',
            attributes: [{ name: 'hasAttribute', type: 'bool' }],
          },
          {
            name: 'b',
            attributes: [],
          },
        ],
      };
      viewModel.consumeMessageFromBackend(schemaWithNoAttributes);

      // no attributes from the entity and relation
      viewModel.onChangeEntity({
        target: {
          value: 'a',
        },
      } as React.ChangeEvent<HTMLInputElement>);
      viewModel.onChangeRelationName({
        target: {
          value: 'collection1',
        },
      } as React.ChangeEvent<HTMLInputElement>);

      expect(viewModel.attributeNameOptions).toEqual({
        Entity: [],
        'No attribute': [ValueType.noAttribute],
        Relation: [],
      });

      // 1 attribute from the relation and no attributes from the entity
      viewModel.onChangeEntity({
        target: {
          value: 'a',
        },
      } as React.ChangeEvent<HTMLInputElement>);
      viewModel.onChangeRelationName({
        target: {
          value: 'collection2',
        },
      } as React.ChangeEvent<HTMLInputElement>);

      expect(viewModel.attributeNameOptions).toEqual({
        Entity: [],
        'No attribute': [ValueType.noAttribute],
        Relation: ["hasAttribute"],
      });

      // 1 attribute from the relation and 1 attribute from the entity
      viewModel.onChangeEntity({
        target: {
          value: 'b',
        },
      } as React.ChangeEvent<HTMLInputElement>);
      viewModel.onChangeRelationName({
        target: {
          value: 'collection2',
        },
      } as React.ChangeEvent<HTMLInputElement>);

      expect(viewModel.attributeNameOptions).toEqual({
        Entity: ["hasAttribute"],
        'No attribute': [ValueType.noAttribute],
        Relation: ["hasAttribute"],
      });
    });
  });
});
