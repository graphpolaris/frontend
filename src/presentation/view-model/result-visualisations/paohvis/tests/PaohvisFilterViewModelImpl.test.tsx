/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { EntitiesFromSchema, FilterType, RelationsFromSchema } from '../../../../../domain/entity/paohvis/structures/Types';
import PaohvisFilterViewModelImpl from '../PaohvisFilterViewModelImpl';
import Broker from '../../../../../domain/entity/broker/broker';
import CalcAttrRelNamesFromSchemaUseCase from '../../../../../domain/usecases/paohvis/CalcEntityAttrAndRelNamesFromSchemaUseCase';
import { isSchemaResult } from '../../../../../domain/entity/graph-schema/structures/SchemaResultType';
import {
  boolPredicates,
  numberPredicates,
  textPredicates,
} from '../../../../../domain/entity/paohvis/models/FilterPredicates';

import {
  parliamentData2,
  flights as flightsSchemaResult,
} from '../../../../../data/mock-data/paohvis/mockSchemaResult';
import flightsQueryResult from '../../../../../data/mock-data/query-result/smallFlightsQueryResults';
import { queryResultParliament } from '../../../../../data/mock-data/paohvis/mockInputData';

describe('PaohvisFilterViewModelImpl', () => {
  const mockFilterFunction = jest.fn();
  const mockresetFilter = jest.fn();

  const xAxisVM = new PaohvisFilterViewModelImpl({
    axis: FilterType.xaxis,
    filterPaohvis: mockFilterFunction,
    resetFilter: mockresetFilter,
  });
  const yAxisVM = new PaohvisFilterViewModelImpl({
    axis: FilterType.yaxis,
    filterPaohvis: mockFilterFunction,
    resetFilter: mockresetFilter,
  });
  const relationVM = new PaohvisFilterViewModelImpl({
    axis: FilterType.edge,
    filterPaohvis: mockFilterFunction,
    resetFilter: mockresetFilter,
  });

  const viewModels = [xAxisVM, yAxisVM, relationVM];

  describe('relation between the same entity types', () => {
    it('isTargetEntity should be true if the PaohvisFilterViewModelImpl is for one of the axes and false for the relation', () => {
      expect(xAxisVM.isTargetEntity).toEqual(true);
      expect(yAxisVM.isTargetEntity).toEqual(true);
      expect(relationVM.isTargetEntity).toEqual(false);

      testButtonShouldBeDisabled();
    });

    it('consumeMessageFromBackend', () => {
      viewModels.forEach((vm) => vm.subscribeToBroker());

      // for schema_result messages
      Broker.instance().publish(flightsSchemaResult, 'schema_result');

      // entitiesFromSchema and relationsFromSchema should be calculated when a schemaresult comes in
      if (isSchemaResult(flightsSchemaResult)) {
        const entitiesFromSchema =
          CalcAttrRelNamesFromSchemaUseCase.calculateAttributesAndRelations(flightsSchemaResult);
        const relationsFromSchema =
          CalcAttrRelNamesFromSchemaUseCase.calculateAttributesFromRelation(flightsSchemaResult);

        testButtonShouldBeDisabled();

        viewModels.forEach((vm) => {
          if (vm.isTargetEntity) {
            expect(vm.namesPerEntityOrRelation).toEqual(entitiesFromSchema.entityNames);
            expect(vm.attributesPerEntityOrRelation).toEqual(
              entitiesFromSchema.attributesPerEntity,
            );
          } else {
            expect(vm.namesPerEntityOrRelation).toEqual(relationsFromSchema.relationCollection);
            expect(vm.attributesPerEntityOrRelation).toEqual(
              relationsFromSchema.attributesPerRelation,
            );
          }
          expect(vm.filterTarget).toEqual('');
          expect(vm.attributeNamesOptions).toEqual([]);
        });
      }

      // for query_result messages
      // the filter target should be reset after a query result
      viewModels.forEach((vm) => {
        vm.filterTarget = 'hi';
      });

      Broker.instance().publish(flightsQueryResult, 'query_result');
      viewModels.forEach((vm) => {
        expect(vm.filterTarget).toEqual('');
        expect(vm.attributeNamesOptions).toEqual([]);
      });
    });

    it('onTableMade', () => {
      // should throw error if the filter target does not exist in the schema
      expect(() => xAxisVM.onTableMade('a', 'a', 'a')).toThrowError(
        'The filter target does not exist in the schema',
      );

      // filter target should be updated
      xAxisVM.onTableMade('airports', 'airports', 'flights');
      expect(xAxisVM.filterTarget).toEqual('airports');

      yAxisVM.onTableMade('airports', 'airports', 'flights');
      expect(yAxisVM.filterTarget).toEqual('airports');

      relationVM.onTableMade('airports', 'airports', 'flights');
      expect(relationVM.filterTarget).toEqual('flights');

      // attributeNamesOptions should be updated
      const attributeNamesOptionsForEntities = [
        'city:text',
        'country:text',
        'lat:number',
        'long:number',
        'name:text',
        'state:text',
        'vip:bool',
      ];
      attributeNamesOptionsForEntities.forEach((attributeNameAndType) => {
        expect(xAxisVM.attributeNamesOptions).toContain(attributeNameAndType);
        expect(yAxisVM.attributeNamesOptions).toContain(attributeNameAndType);
      });

      const attributeNamesOptionsForRelation = [
        'ArrTime:number',
        'ArrTimeUTC:text',
        'Day:number',
        'DayOfWeek:number',
        'DepTime:number',
        'DepTimeUTC:text',
        'Distance:number',
        'FlightNum:number',
        'Month:number',
        'TailNum:text',
        'UniqueCarrier:text',
        'Year:number',
      ];
      attributeNamesOptionsForRelation.forEach((attributeNameAndType) => {
        expect(relationVM.attributeNamesOptions).toContain(attributeNameAndType);
      });

      testButtonShouldBeDisabled();
    });

    it('onChangeAttributeName', () => {
      testValidEvent({ target: { value: 'city:text' } }, xAxisVM);
      testValidEvent({ target: { value: 'vip:bool' } }, yAxisVM);
      testValidEvent({ target: { value: 'Day:number' } }, relationVM);
      testButtonShouldBeDisabled();

      // should throw an error if the chosen attribute does not exist in the filter target
      expect(() =>
        xAxisVM.onChangeAttributeName({
          target: { value: 'aaaa:text' },
        } as React.ChangeEvent<HTMLInputElement>),
      ).toThrowError(
        'The chosen attribute does not exist in the entity/relation that will be filtered',
      );
      expect(() =>
        yAxisVM.onChangeAttributeName({
          target: { value: 'long:bbb' },
        } as React.ChangeEvent<HTMLInputElement>),
      ).toThrowError(
        'The chosen attribute does not exist in the entity/relation that will be filtered',
      );

      function testValidEvent(event: any, viewModel: PaohvisFilterViewModelImpl) {
        event as React.ChangeEvent<HTMLInputElement>;
        const attributeNameAndType: string = event.target.value;
        const type: string = event.target.value.split(':')[1];

        viewModel.onChangeAttributeName(event);

        expect(viewModel.attributeNameAndType).toEqual(attributeNameAndType);
        expect(viewModel.attributeType).toEqual(type);
        if (type == 'bool') {
          expect(viewModel.predicateTypeList).toEqual(Object.keys(boolPredicates));
        } else if (type == 'number') {
          expect(viewModel.predicateTypeList).toEqual(Object.keys(numberPredicates));
        } else if (type == 'text') {
          expect(viewModel.predicateTypeList).toEqual(Object.keys(textPredicates));
        }
      }
    });

    it('onChangePredicate', () => {
      testValidEvent({ target: { value: 'contains' } }, xAxisVM);
      testValidEvent({ target: { value: '==' } }, yAxisVM);
      testValidEvent({ target: { value: '>=' } }, relationVM);
      testButtonShouldBeDisabled();

      // should throw error if the chosen predicate is invalid for the attribute type
      testInvalidEvent({ target: { value: '<' } }, xAxisVM);
      testInvalidEvent({ target: { value: '>' } }, yAxisVM);
      testInvalidEvent({ target: { value: 'excludes' } }, relationVM);

      function testValidEvent(event: any, vm: PaohvisFilterViewModelImpl) {
        event as React.ChangeEvent<HTMLInputElement>;
        vm.onChangePredicate(event);
        expect(vm.predicate).toEqual(event.target.value);
        expect(vm.compareValue).toEqual('');
      }
      function testInvalidEvent(event: any, vm: PaohvisFilterViewModelImpl) {
        event as React.ChangeEvent<HTMLInputElement>;
        expect(() => vm.onChangePredicate(event)).toThrowError('The chosen predicate is invalid');
      }
    });

    it('onChangeCompareValue', () => {
      testEvent({ target: { value: 'aaa' } }, xAxisVM);
      testEvent({ target: { value: 'true' } }, yAxisVM);
      testEvent({ target: { value: '123' } }, relationVM);

      // the filter button should be disabled for an invalid compare value
      testInvalidEvent({ target: { value: '' } }, xAxisVM);
      testInvalidEvent({ target: { value: 'aaa' } }, yAxisVM);
      testInvalidEvent({ target: { value: 'false' } }, relationVM);

      function testEvent(event: any, vm: PaohvisFilterViewModelImpl) {
        event as React.ChangeEvent<HTMLInputElement>;
        vm.onChangeCompareValue(event);
        expect(vm.compareValue).toEqual(event.target.value);
        expect(vm.isFilterButtonEnabled).toEqual(true);
      }
      function testInvalidEvent(event: any, vm: PaohvisFilterViewModelImpl) {
        event as React.ChangeEvent<HTMLInputElement>;
        vm.onChangeCompareValue(event);
        expect(vm.compareValue).toEqual(event.target.value);
        expect(vm.isFilterButtonEnabled).toEqual(false);
      }
    });

    it('onClickFilterPaohvisButton', () => {
      expect(mockFilterFunction).toHaveBeenCalledTimes(0);

      // setup valid filter configuration
      const onChangeAttributeNameEvent = {
        target: { value: 'country:text' },
      } as React.ChangeEvent<HTMLInputElement>;
      const onChangePredicateEvent = {
        target: { value: '!=' },
      } as React.ChangeEvent<HTMLInputElement>;
      const onChangeCompareValueEvent = {
        target: { value: 'a' },
      } as React.ChangeEvent<HTMLInputElement>;
      xAxisVM.onChangeAttributeName(onChangeAttributeNameEvent);
      xAxisVM.onChangePredicate(onChangePredicateEvent);
      xAxisVM.onChangeCompareValue(onChangeCompareValueEvent);

      // should call filterPaohvis when it has a valid filter configuration
      xAxisVM.onClickFilterPaohvisButton();
      expect(mockFilterFunction).toHaveBeenCalledTimes(1);

      // should throw error when the filter configuration is invalid and the filterPaohvis shouldn't be called
      expect(() => yAxisVM.onClickFilterPaohvisButton()).toThrowError(
        'Error: chosen attribute of predicate-value is invalid.',
      );
      expect(mockFilterFunction).toHaveBeenCalledTimes(1);
    });

    it('onClickResetFilter', () => {
      expect(mockresetFilter).toHaveBeenCalledTimes(0);
      let count = 0;
      viewModels.forEach((vm) => {
        testEvent(++count, vm);
      });

      function testEvent(count: number, vm: PaohvisFilterViewModelImpl) {
        vm.onClickResetFilter();
        expect(vm.attributeNameAndType).toEqual('');
        expect(vm.attributeType).toEqual('');
        expect(vm.predicate).toEqual('');
        expect(vm.compareValue).toEqual('');
        expect(vm.isFilterButtonEnabled).toEqual(false);
        expect(vm.predicateTypeList).toEqual([]);
        expect(mockresetFilter).toHaveBeenCalledTimes(count);
      }
    });
  });

  describe('relation between different entity types', () => {
    it('consumeMessageFromBackend', () => {
      const schemaResult = parliamentData2;
      const queryResult = queryResultParliament;

      viewModels.forEach((vm) => vm.subscribeToBroker());

      // for schema_result messages
      Broker.instance().publish(schemaResult, 'schema_result');

      // entitiesFromSchema and relationsFromSchema should be calculated when a schemaresult comes in
      if (isSchemaResult(schemaResult)) {
        const entitiesFromSchema =
          CalcAttrRelNamesFromSchemaUseCase.calculateAttributesAndRelations(schemaResult);
        const relationsFromSchema =
          CalcAttrRelNamesFromSchemaUseCase.calculateAttributesFromRelation(schemaResult);

        testButtonShouldBeDisabled();
        testValidSchemaResult(entitiesFromSchema, relationsFromSchema);

        // a schema_result message that does not contain a valid schema result should not change anything
        Broker.instance().publish('totally not valid', 'schema_result');
        testValidSchemaResult(entitiesFromSchema, relationsFromSchema);
      }

      // for query_result messages
      viewModels.forEach((vm) => {
        vm.filterTarget = 'hi';
        vm.attributeNamesOptions = ['hi'];
      });

      // a query_result message that does not contain a valid query result should not change anything
      Broker.instance().publish('not a valid queryResult', 'query_result');
      viewModels.forEach((vm) => {
        expect(vm.filterTarget).toEqual('hi');
        expect(vm.attributeNamesOptions).toEqual['hi'];
      });

      // a valid query_result message should reset the filtertarget and attributeNamesOptions
      Broker.instance().publish(queryResult, 'query_result');
      viewModels.forEach((vm) => {
        expect(vm.filterTarget).toEqual('');
        expect(vm.attributeNamesOptions).toEqual([]);
      });

      viewModels.forEach((vm) => vm.unSubscribeFromBroker());

      function testValidSchemaResult(entitiesFromSchema: EntitiesFromSchema, relationsFromSchema: RelationsFromSchema) {
        viewModels.forEach((vm) => {
          if (vm.isTargetEntity) {
            expect(vm.namesPerEntityOrRelation).toEqual(entitiesFromSchema.entityNames);
            expect(vm.attributesPerEntityOrRelation).toEqual(
              entitiesFromSchema.attributesPerEntity
            );
          } else {
            expect(vm.namesPerEntityOrRelation).toEqual(relationsFromSchema.relationCollection);
            expect(vm.attributesPerEntityOrRelation).toEqual(
              relationsFromSchema.attributesPerRelation
            );
          }
          expect(vm.filterTarget).toEqual('');
          expect(vm.attributeNamesOptions).toEqual([]);
        });
      }
    });

    it('onTableMade', () => {
      // should throw error if the filter target does not exist in the schema
      expect(() => xAxisVM.onTableMade('a', 'a', 'a')).toThrowError(
        'The filter target does not exist in the schema',
      );

      // filter target should be updated
      xAxisVM.onTableMade('parliament', 'commissions', 'part_of');
      expect(xAxisVM.filterTarget).toEqual('commissions');

      yAxisVM.onTableMade('parliament', 'commissions', 'part_of');
      expect(yAxisVM.filterTarget).toEqual('parliament');

      relationVM.onTableMade('parliament', 'commissions', 'part_of');
      expect(relationVM.filterTarget).toEqual('part_of');

      // attributeNamesOptions should be updated
      const attributeNamesOptionsForCommissions = ['name:text'];
      const attributeNamesOptionsForParliament = [
        'img:text',
        'party:text',
        'residence:text',
        'name:text',
        'age:number',
        'seniority:number',
        'weight:number',
        'isChairman:bool',
      ];
      expect(xAxisVM.attributeNamesOptions).toEqual(attributeNamesOptionsForCommissions);
      expect(yAxisVM.attributeNamesOptions).toEqual(attributeNamesOptionsForParliament);
      expect(relationVM.attributeNamesOptions).toEqual([]);

      testButtonShouldBeDisabled();
    });
  });

  describe('subscription to broker', () => {
    const viewModel = new PaohvisFilterViewModelImpl({
      axis: FilterType.xaxis,
      filterPaohvis: mockFilterFunction,
      resetFilter: mockresetFilter,
    });
    const mockConsumeMessageFromBackend = jest.fn();
    viewModel.consumeMessageFromBackend = mockConsumeMessageFromBackend;

    // should consume messages from backend when subscribed to broker
    viewModel.subscribeToBroker();
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(0);
    Broker.instance().publish('hi', 'schema_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(1);
    Broker.instance().publish('hi', 'query_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(2);

    // should not consume messages from backend anymore when unsubscribed from broker
    viewModel.unSubscribeFromBroker();
    Broker.instance().publish('hi', 'schema_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(2);
    Broker.instance().publish('hi', 'query_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(2);
  });

  function testButtonShouldBeDisabled() {
    viewModels.forEach((vm) => expect(vm.isFilterButtonEnabled).toEqual(false));
  }
});
