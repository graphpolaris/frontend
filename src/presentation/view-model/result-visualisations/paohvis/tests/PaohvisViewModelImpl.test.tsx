/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from '../../../../../domain/entity/broker/broker';
import PaohvisViewModel from '../PaohvisViewModel';
import PaohvisViewModelImpl from '../PaohvisViewModelImpl';
import CalcAttrRelNamesFromSchemaUseCase from '../../../../../domain/usecases/paohvis/CalcEntityAttrAndRelNamesFromSchemaUseCase';
import { isSchemaResult } from '../../../../../domain/entity/graph-schema/structures/SchemaResultType';
import {
  AttributeOrigin,
  NodeOrder,
  ValueType,
} from '../../../../../domain/entity/paohvis/structures/Types';

import {
  parliamentData2,
  flights as flightsSchemaResult,
} from '../../../../../data/mock-data/paohvis/mockSchemaResult';
import flightsQueryResult from '../../../../../data/mock-data/query-result/smallFlightsQueryResults';
import { queryResultParliament } from '../../../../../data/mock-data/paohvis/mockInputData';

describe('PaohvisViewModelImpl', () => {
  it('onClickMakeButton -> recalculate -> onClickFilterButton -> onClickResetButton', () => {
    const paohvisViewModel = new PaohvisViewModelImpl();
    paohvisViewModel.consumeMessageFromBackend(parliamentData2);
    paohvisViewModel.consumeMessageFromBackend(queryResultParliament);
    paohvisViewModel.xAxisPaohvisFilterViewModel.consumeMessageFromBackend(
      parliamentData2,
      'schema_result',
    );
    paohvisViewModel.yAxisPaohvisFilterViewModel.consumeMessageFromBackend(
      parliamentData2,
      'schema_result',
    );
    paohvisViewModel.relationPaohvisFilterViewModel.consumeMessageFromBackend(
      parliamentData2,
      'schema_result',
    );

    expect(paohvisViewModel.hyperedgesOnRow).toEqual([]);
    expect(paohvisViewModel.allHyperEdges).toEqual([]);

    paohvisViewModel.onClickMakeButton(
      'parliament',
      'parties',
      'member_of',
      true,
      {
        name: 'name',
        type: ValueType.text,
        origin: AttributeOrigin.entity,
      },
      { orderBy: NodeOrder.alphabetical, isReverseOrder: false },
    );
    const resultData = {
      rowLabels: [
        'parliament/137',
        'parliament/138',
        'parliament/140',
        'parliament/141',
        'parliament/148',
      ],
      hyperEdgeRanges: [
        {
          rangeText: 'D66',
          hyperEdges: [
            {
              indices: [1, 3],
              frequencies: [0, 1, 0, 1, 0],
              nameToShow: 'parties/3',
            },
          ],
        },
        {
          rangeText: 'GL',
          hyperEdges: [
            {
              indices: [2],
              frequencies: [0, 0, 1, 0, 0],
              nameToShow: 'parties/2',
            },
          ],
        },
        {
          rangeText: 'PVV',
          hyperEdges: [
            {
              indices: [0, 4],
              frequencies: [1, 0, 0, 0, 1],
              nameToShow: 'parties/4',
            },
          ],
        },
      ],
    };
    // check if the data is parsed correctly
    expect(paohvisViewModel.data.rowLabels).toEqual(resultData.rowLabels);
    expect(paohvisViewModel.data.hyperEdgeRanges).toEqual(resultData.hyperEdgeRanges);

    // check if recalculate works correctly
    const resultHyperEdgesOnRow = [[2], [0], [1], [0], [2]];
    const resultAllHyperedges = [[1, 3], [2], [0, 4]];
    expect(paohvisViewModel.hyperedgesOnRow).toEqual(resultHyperEdgesOnRow);
    expect(paohvisViewModel.allHyperEdges).toEqual(resultAllHyperedges);

    // check if the PaohvisListeners get notified
    expect(paohvisViewModel.xAxisPaohvisFilterViewModel.filterTarget).toEqual('parties');
    expect(paohvisViewModel.yAxisPaohvisFilterViewModel.filterTarget).toEqual('parliament');
    expect(paohvisViewModel.relationPaohvisFilterViewModel.filterTarget).toEqual('member_of');

    // should filter correctly
    paohvisViewModel.onClickFilterButton(true, 'parliament', 'isChairman:bool', '==', 'TRUE');
    expect(paohvisViewModel.data.rowLabels).toEqual(['parliament/137', 'parliament/140']);
    expect(paohvisViewModel.data.hyperEdgeRanges).toEqual([
      {
        rangeText: 'GL',
        hyperEdges: [
          {
            indices: [1],
            frequencies: [0, 1],
            nameToShow: 'parties/2',
          },
        ],
      },
      {
        rangeText: 'PVV',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1, 0],
            nameToShow: 'parties/4',
          },
        ],
      },
    ]);

    // check if data gets properly reset
    paohvisViewModel.onClickResetButton(true);
    expect(paohvisViewModel.data.rowLabels).toEqual(resultData.rowLabels);
    expect(paohvisViewModel.data.hyperEdgeRanges).toEqual(resultData.hyperEdgeRanges);
    expect(paohvisViewModel.hyperedgesOnRow).toEqual(resultHyperEdgesOnRow);
    expect(paohvisViewModel.allHyperEdges).toEqual(resultAllHyperedges);
  });

  it('should filter all types correctly', () => {
    // check on boolean attributes
    const paohvisViewModel = new PaohvisViewModelImpl();
    paohvisViewModel.consumeMessageFromBackend(flightsSchemaResult);
    paohvisViewModel.consumeMessageFromBackend(flightsQueryResult);
    paohvisViewModel.xAxisPaohvisFilterViewModel.consumeMessageFromBackend(
      flightsSchemaResult,
      'schema_result',
    );
    paohvisViewModel.yAxisPaohvisFilterViewModel.consumeMessageFromBackend(
      flightsSchemaResult,
      'schema_result',
    );
    paohvisViewModel.relationPaohvisFilterViewModel.consumeMessageFromBackend(
      flightsSchemaResult,
      'schema_result',
    );

    paohvisViewModel.onClickMakeButton(
      'airports',
      'airports',
      'flights',
      true,
      {
        name: 'Year',
        type: ValueType.number,
        origin: AttributeOrigin.relation,
      },
      { orderBy: NodeOrder.degree, isReverseOrder: true },
    );
    paohvisViewModel.onClickFilterButton(true, 'airports', 'vip:bool', '!=', 'false');
    expect(paohvisViewModel.data.rowLabels).toEqual(['airports/JFK', 'airports/SFO']);
    expect(paohvisViewModel.data.hyperEdgeRanges).toEqual([
      {
        rangeText: '2008',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1, 0],
            nameToShow: 'airports/SFO',
          },
          {
            indices: [1],
            frequencies: [0, 1],
            nameToShow: 'airports/JFK',
          },
        ],
      },
    ]);

    expect(paohvisViewModel.paohvisFilters).toEqual({
      nodeFilters: [
        { targetGroup: 'airports', attributeName: 'vip', predicateName: '!=', value: false },
      ],
      edgeFilters: [],
    });

    expect(() =>
      paohvisViewModel.onClickFilterButton(true, 'airports', 'vip:bool', '!=', 'invalid'),
    ).toThrowError('Error: This is not a correct input');

    // check on number attributes
    paohvisViewModel.onClickResetButton(true);
    expect(paohvisViewModel.paohvisFilters).toEqual({
      nodeFilters: [],
      edgeFilters: [],
    });

    paohvisViewModel.onClickFilterButton(false, 'flights', 'Distance:number', '>', '200');
    expect(paohvisViewModel.data.rowLabels).toEqual(['airports/JFK']);
    expect(paohvisViewModel.data.hyperEdgeRanges).toEqual([
      {
        rangeText: '2008',
        hyperEdges: [
          {
            indices: [0],
            frequencies: [1],
            nameToShow: 'airports/SFO',
          },
        ],
      },
    ]);

    paohvisViewModel.onClickFilterButton(false, 'flights', 'Distance:number', '<', '200');
    expect(paohvisViewModel.data).toEqual({
      rowLabels: [],
      hyperEdgeRanges: [],
      maxRowLabelWidth: 10,
    });

    expect(paohvisViewModel.paohvisFilters).toEqual({
      nodeFilters: [],
      edgeFilters: [
        { targetGroup: 'flights', attributeName: 'Distance', predicateName: '<', value: 200 },
      ],
    });

    paohvisViewModel.onClickResetButton(false);
    expect(paohvisViewModel.paohvisFilters).toEqual({
      nodeFilters: [],
      edgeFilters: [],
    });

    expect(() =>
      paohvisViewModel.onClickFilterButton(false, 'flights', 'Distance:number', '<', 'hi'),
    ).toThrowError('Error: This is not a correct input');

    // check on text attributes
    paohvisViewModel.onClickFilterButton(true, 'airports', 'name:text', 'contains', 'F');
    expect(paohvisViewModel.data.rowLabels).toEqual(['airports/JFK', 'airports/SFO']);
    expect(paohvisViewModel.data.hyperEdgeRanges).toEqual([
      {
        hyperEdges: [
          { frequencies: [1, 0], indices: [0], nameToShow: 'airports/SFO' },
          { frequencies: [0, 1], indices: [1], nameToShow: 'airports/JFK' },
        ],
        rangeText: '2008',
      },
    ]);
  });

  it('onMouseEvents', () => {
    const paohvisViewModel: PaohvisViewModel = new PaohvisViewModelImpl();
    {
      paohvisViewModel.data = {
        rowLabels: [
          'parliament/137',
          'parliament/138',
          'parliament/140',
          'parliament/141',
          'parliament/148',
        ],
        hyperEdgeRanges: [
          {
            rangeText: 'D66',
            hyperEdges: [
              {
                indices: [1, 3],
                frequencies: [0, 1, 0, 1, 0],
                nameToShow: 'parties/3',
              },
            ],
          },
          {
            rangeText: 'GL',
            hyperEdges: [
              {
                indices: [2],
                frequencies: [0, 0, 1, 0, 0],
                nameToShow: 'parties/2',
              },
            ],
          },
          {
            rangeText: 'PVV',
            hyperEdges: [
              {
                indices: [0, 4],
                frequencies: [1, 0, 0, 0, 1],
                nameToShow: 'parties/4',
              },
            ],
          },
        ],
        maxRowLabelWidth: 10,
      };
      paohvisViewModel.hyperedgesOnRow = [[2], [0], [1], [0], [2]];
      paohvisViewModel.allHyperEdges = [[1, 3], [2], [0, 4]];
      paohvisViewModel.onMouseEnterRow(0);
      paohvisViewModel.onMouseLeaveRow(0);
      paohvisViewModel.onMouseEnterHyperEdge(0, 'parties/3');
      paohvisViewModel.onMouseLeaveHyperEdge(0);

      const event = {} as React.MouseEvent<Element, MouseEvent>;
      paohvisViewModel.onMouseMoveToolTip(event);
    }
  });

  it('consumeMessageFromBackend', () => {
    const paohvisViewModel: PaohvisViewModel = new PaohvisViewModelImpl();
    paohvisViewModel.subscribeToQueryResult();
    paohvisViewModel.subscribeToSchemaResult();

    // should not take the query result if schema has no entities
    expect(paohvisViewModel.entitiesFromSchema).toEqual({
      entityNames: [],
      attributesPerEntity: {},
      relationsPerEntity: {},
    });
    expect(() =>
      paohvisViewModel.consumeMessageFromBackend(flightsQueryResult, 'query_result'),
    ).toThrowError('');
    expect(paohvisViewModel.nodeLinkQueryResult).toEqual({ edges: [], nodes: [] });

    // check if entitiesFromSchema and relationsFromSchema gets correctly updated when the broker sends a schema result
    Broker.instance().publish(flightsSchemaResult, 'schema_result');
    if (isSchemaResult(flightsSchemaResult)) {
      expect(paohvisViewModel.entitiesFromSchema).toEqual(
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesAndRelations(flightsSchemaResult),
      );
      expect(paohvisViewModel.relationsFromSchema).toEqual(
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesFromRelation(flightsSchemaResult),
      );

      // check if entitiesFromSchema and relationsFromSchema do not change when the broker sends an invalid schema result
      Broker.instance().publish('totally invalid', 'schema_result');
      expect(paohvisViewModel.entitiesFromSchema).toEqual(
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesAndRelations(flightsSchemaResult),
      );
      expect(paohvisViewModel.relationsFromSchema).toEqual(
        CalcAttrRelNamesFromSchemaUseCase.calculateAttributesFromRelation(flightsSchemaResult),
      );
    }

    // check if nodeLinkQueryResult gets correctly updated when the broker sends a query result
    Broker.instance().publish(flightsQueryResult, 'query_result');
    expect(paohvisViewModel.nodeLinkQueryResult).toEqual(flightsQueryResult);

    // check if nodeLinkQueryResult does not change when the broker sends an invalid query result
    Broker.instance().publish('not valid at all', 'query_result');
    expect(paohvisViewModel.nodeLinkQueryResult).toEqual(flightsQueryResult);
  });

  describe('subscription to broker', () => {
    const viewModel = new PaohvisViewModelImpl();
    const mockConsumeMessageFromBackend = jest.fn();
    viewModel.consumeMessageFromBackend = mockConsumeMessageFromBackend;

    // should consume messages from backend when subscribed to broker
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(0);

    viewModel.subscribeToSchemaResult();
    Broker.instance().publish('hi', 'schema_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(1);

    viewModel.subscribeToQueryResult();
    Broker.instance().publish('hi', 'query_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(2);

    // should not consume messages from backend anymore when unsubscribed from broker
    viewModel.unSubscribeFromSchemaResult();
    Broker.instance().publish('hi', 'schema_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(2);

    viewModel.unSubscribeFromQueryResult();
    Broker.instance().publish('hi', 'query_result');
    expect(mockConsumeMessageFromBackend).toHaveBeenCalledTimes(2);
  });
});
