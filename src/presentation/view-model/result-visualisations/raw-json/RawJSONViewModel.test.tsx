import AbstractResultVisViewModelImpl from '../AbstractResultVisViewModelImpl';
import RawJSONViewModel from './RawJSONViewModel';
import RawJSONViewModelImpl from './RawJSONViewModelImpl';

describe('RawJSONViewModel', () => {
  beforeEach(() => jest.resetModules());

  it('consumeMessageFromBackend', () => {
    const rawjson = new RawJSONViewModelImpl();
    rawjson.consumeMessageFromBackend(400);

    expect(rawjson.queryResult).toEqual(400);
  });
});
