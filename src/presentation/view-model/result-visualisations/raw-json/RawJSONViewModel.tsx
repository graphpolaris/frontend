/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import ResultVisViewModel from '../ResultVisViewModel';

/**
 * RawJSONViewModel is an interface used to implement the ViewModel implementation of the RawJSONViewModelImplementation.
 * Used to define all public fields used in the RawJSONViewModelImplementation.
 *
 * Extends BaseViewModel is an interface of ViewModel holding all public fields for all ViewModels.
 */
export default interface RawJSONViewModel extends ResultVisViewModel {
  queryResult: unknown;
}
