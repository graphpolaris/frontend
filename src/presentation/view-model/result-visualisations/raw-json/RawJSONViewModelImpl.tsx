/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import AbstractResultVisViewModelImpl from '../AbstractResultVisViewModelImpl';
import RawJSONViewModel from './RawJSONViewModel';

/** RawJSONViewModelImpl is the implementation of the interface defined in RawJSONViewModel. */
export default class RawJSONViewModelImpl
  extends AbstractResultVisViewModelImpl
  implements RawJSONViewModel {
  public queryResult: unknown;

  public consumeMessageFromBackend(message: unknown): void {
    this.queryResult = message;

    this.notifyViewAboutChanges();
  }
}
