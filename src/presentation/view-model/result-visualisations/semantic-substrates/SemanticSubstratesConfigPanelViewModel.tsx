import { EntityWithAttributes } from '../../../../domain/entity/semantic-substrates/config-panel/Types';

/** This is an interface of SemanticSubstratesConfigPanelViewModel that will be given to the FSS view. */
export default interface SemanticSubstratesConfigPanelViewModel {
  nodes: EntityWithAttributes[];
  relations: EntityWithAttributes[];

  makeNodeTypes(): void;
  makeRelationTypes(): void;
  onNodeChange(s: string): void;
  onAttributeChange(s: string): void;
  onRelationAttributeChange(s: string): void;
  getUniqueRelationAttributes(): string[];
  getUniqueNodeAttributes(): string[];
}
