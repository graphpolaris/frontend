import SemanticSubstratesConfigPanelViewModelImpl from './SemanticSubstratesConfigPanelViewModelImpl';
import SemanticSubstratesViewModelImpl from './SemanticSubstratesViewModelImpl';
import big2ndChamberQueryResult from '../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import smallFlightsQueryResults from '../../../../data/mock-data/query-result/smallFlightsQueryResults';

jest.mock('../../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet');

let parties = ['BIJ1', 'VVD', 'PvdA', 'PVV', 'BBB', 'D66', 'GL'];

describe('SemanticSubstratesConfigPanelViewModelImpl: unique attribute creation', () => {
  it('Should consume result corectly to generate the correct unique node attributes.', () => {
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: big2ndChamberQueryResult,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeNodeTypes();
    FSSConfigPanelViewModel.onNodeChange('kamerleden');
    expect(FSSConfigPanelViewModel.getUniqueNodeAttributes()).toEqual([
      'anc',
      'img',
      'leeftijd',
      'naam',
      'partij',
      'woonplaats',
    ]);
  });

  it('Should consume result corectly to generate the correct unique relation attributes.', () => {
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: big2ndChamberQueryResult,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    expect(FSSConfigPanelViewModel.getUniqueRelationAttributes()).toEqual([]);
  });
});

describe('SemanticSubstratesConfigPanelViewModelImpl: unique attribute creation', () => {
  it('Should consume result corectly to generate the correct unique node attributes.', () => {
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: smallFlightsQueryResults,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeNodeTypes();
    FSSConfigPanelViewModel.onNodeChange('airports');
    expect(FSSConfigPanelViewModel.getUniqueNodeAttributes()).toEqual([
      'city',
      'country',
      'lat',
      'long',
      'name',
      'state',
      'vip',
    ]);
  });

  it('Should consume result corectly to generate the correct unique relation attributes.', () => {
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: smallFlightsQueryResults,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    expect(FSSConfigPanelViewModel.getUniqueRelationAttributes()).toEqual([
      'Year',
      'Month',
      'Day',
      'DayOfWeek',
      'DepTime',
      'ArrTime',
      'DepTimeUTC',
      'ArrTimeUTC',
      'UniqueCarrier',
      'FlightNum',
      'TailNum',
      'Distance',
    ]);
  });
});

describe('SemanticSubstratesConfigPanelViewModelImpl: Correct values should be set', () => {
  it("Should consume result corectly and get the calculations for the numerical attribute 'leeftijd' ", () => {
    let attributeToSelect = 'leeftijd';
    let nodeToSelect = 'kamerleden';
    let maxAgeInParlement = 69;
    let minAgeInParlement = 23;
    let minSize = 1;
    let maxSize = 10;
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: big2ndChamberQueryResult,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    FSSConfigPanelViewModel.makeNodeTypes();
    FSSConfigPanelViewModel.onNodeChange(nodeToSelect);

    expect(FSSConfigPanelViewModel.currentNode).toEqual(nodeToSelect);
    expect(FSSConfigPanelViewModel.isNodeAttributeNumber(attributeToSelect)).toEqual(true);

    let calculationF = FSSConfigPanelViewModel.getTheScaleCalculationForNodes(
      attributeToSelect,
      minSize,
      maxSize,
    );
    expect(calculationF(maxAgeInParlement)).toEqual(maxSize);
    expect(calculationF((maxAgeInParlement + minAgeInParlement) / 2)).toEqual(
      (maxSize + minSize) / 2,
    );
    expect(calculationF(minAgeInParlement)).toEqual(minSize);
  });

  it("Should consume result corectly and get the calculations for the catecorigal attribute 'partij' ", () => {
    let attributeToSelect = 'partij';
    let nodeToSelect = 'kamerleden';
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: big2ndChamberQueryResult,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    FSSConfigPanelViewModel.makeNodeTypes();
    FSSConfigPanelViewModel.onNodeChange(nodeToSelect);

    expect(FSSConfigPanelViewModel.currentNode).toEqual(nodeToSelect);
    expect(FSSConfigPanelViewModel.isNodeAttributeNumber(attributeToSelect)).toEqual(false);

    let calculationF = FSSConfigPanelViewModel.getTheColourCalculationForNodes(attributeToSelect);
    parties.forEach((party) => expect(calculationF(party)).toBeDefined());
    expect(calculationF('Not a party')).toBeUndefined();
  });
});

describe('SemanticSubstratesConfigPanelViewModelImpl: Correct values should be set for smallFlightsQueryResults', () => {
  it("Should consume result corectly and get the calculations for the numerical attribute 'distance' ", () => {
    let attributeToSelect = 'Distance';
    let maxDistance = 487;
    let minDistance = 200;
    let minSize = 1;
    let maxSize = 10;
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: smallFlightsQueryResults,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    FSSConfigPanelViewModel.makeNodeTypes();

    expect(FSSConfigPanelViewModel.isRelationAttributeNumber(attributeToSelect)).toEqual(true);

    let calculationF = FSSConfigPanelViewModel.getTheScaleCalculationForRelations(
      attributeToSelect,
      minSize,
      maxSize,
    );
    expect(calculationF(maxDistance)).toEqual(maxSize);
    expect(calculationF((maxDistance + minDistance) / 2)).toEqual((maxSize + minSize) / 2);
    expect(calculationF(minDistance)).toEqual(minSize);
  });

  it("Should consume result corectly and get the calculations for the catecorigal attribute 'UniqueCarrier' ", () => {
    let attributeToSelect = 'UniqueCarrier';
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: smallFlightsQueryResults,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    FSSConfigPanelViewModel.makeNodeTypes();

    expect(FSSConfigPanelViewModel.isRelationAttributeNumber(attributeToSelect)).toEqual(false);

    let calculationF =
      FSSConfigPanelViewModel.getTheColourCalculationForRelations(attributeToSelect);
    expect(calculationF('NW')).toBeDefined();
    expect(calculationF('WN')).toBeDefined();
    expect(calculationF('Not a UniqueCarrier')).toBeUndefined();
  });

  it("Should consume result corectly and get the calculations for the catecorigal attribute 'state' ", () => {
    let attributeToSelect = 'state';
    let nodeToSelect = 'airports';
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: smallFlightsQueryResults,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    FSSConfigPanelViewModel.makeNodeTypes();
    FSSConfigPanelViewModel.onNodeChange(nodeToSelect);

    expect(FSSConfigPanelViewModel.currentNode).toEqual(nodeToSelect);
    expect(FSSConfigPanelViewModel.isNodeAttributeNumber(attributeToSelect)).toEqual(false);

    let calculationF = FSSConfigPanelViewModel.getTheColourCalculationForNodes(attributeToSelect);
    expect(calculationF('CA')).toBeDefined();
    expect(calculationF('NY')).toBeDefined();
    expect(calculationF('Not a state')).toBeUndefined();
  });

  it('Should update the visualisation when the attribute changes', () => {
    let attributeToSelect = 'state';
    let attributeToSelect2 = 'lat';
    let nodeToSelect = 'airports';
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: smallFlightsQueryResults,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    FSSConfigPanelViewModel.makeNodeTypes();
    FSSConfigPanelViewModel.onNodeChange(nodeToSelect);

    // change the attribute for the first time
    FSSConfigPanelViewModel.onAttributeChange(attributeToSelect);
    expect(FSSConfigPanelViewModel.currentAttribute).toEqual(attributeToSelect);

    // change the attribute for the second time, this time with an attribute that has numbers as values
    FSSConfigPanelViewModel.onAttributeChange(attributeToSelect2);
    expect(FSSConfigPanelViewModel.currentAttribute).toEqual(attributeToSelect2);
  });

  it('Should update the visualisation when the relation attribute changes', () => {
    let attributeToSelect = 'TailNum';
    let attributeToSelect2 = 'Distance';
    let nodeToSelect = 'airports';
    let mockProperties = {
      fssViewModel: new SemanticSubstratesViewModelImpl(),
      graph: smallFlightsQueryResults,
    };

    let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModelImpl =
      new SemanticSubstratesConfigPanelViewModelImpl(mockProperties);

    FSSConfigPanelViewModel.makeRelationTypes();
    FSSConfigPanelViewModel.makeNodeTypes();
    FSSConfigPanelViewModel.onNodeChange(nodeToSelect);

    // change the attribute for the first time
    FSSConfigPanelViewModel.onRelationAttributeChange(attributeToSelect);
    expect(FSSConfigPanelViewModel.currentRelationAttribute).toEqual(attributeToSelect);

    // change the attribute for the second time, this time with an attribute that has numbers as values
    FSSConfigPanelViewModel.onRelationAttributeChange(attributeToSelect2);
    expect(FSSConfigPanelViewModel.currentRelationAttribute).toEqual(attributeToSelect2);
  });
});
