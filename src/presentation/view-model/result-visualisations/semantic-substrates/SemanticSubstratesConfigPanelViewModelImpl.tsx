import { range } from 'd3-array';
import { ColourPalettes } from '../../../../domain/entity/customization/colours';
import {
  EntityWithAttributes,
  FSSConfigPanelProps,
} from '../../../../domain/entity/semantic-substrates/config-panel/Types';
import { Link, Node } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import SemanticSubstratesViewModel from './SemanticSubstratesViewModel';
import SemanticSubstratesConfigPanelViewModel from './SemanticSubstratesConfigPanelViewModel';
import MakeTypesFromGraphUseCase from '../../../../domain/usecases/graph-data/MakeTypesFromGraphUseCase';

/** Viewmodel for rendering config input fields for Faceted Semantic Subrate attributes. */
export default class SemanticSubstratesConfigPanelViewModelImpl
  implements SemanticSubstratesConfigPanelViewModel
{
  public nodes: EntityWithAttributes[];
  public relations: EntityWithAttributes[];

  minimalNodeSize = 2;
  maximalNodeSize = 10;

  minimalRelationWidth = 0.05;
  maximalRelationWidth = 4;

  nodesloaded: Node[];
  relationsloaded: Link[];
  public currentNode = '';
  currentRelation = '';
  currentAttribute = '';
  currentRelationAttribute = '';
  // Faceted Semantic Substrates View Model.
  fssViewModel: SemanticSubstratesViewModel;
  /**
   * The constructor for the FSSConfigPanelViewModelImpl (FacetedSemanticSubstratesConfigPanelViewModelImpl).
   * This handles the view for changing how to display the attributes of the nodes and relations
   * in the FSS view.
   * @param props The properties for the FacetedSemanticSubstratesConfigPanel. These define how the
   * fss ViewModel is generated.
   */
  public constructor(props: FSSConfigPanelProps) {
    let graph = props.graph;
    let fssViewModel = props.fssViewModel;

    this.nodes = [];
    this.relations = [];

    this.nodesloaded = graph.nodes;
    this.relationsloaded = graph.edges;
    this.fssViewModel = fssViewModel;
  }

  /**
   * Creates a list of unique node types based on the current list of nodes.
   */
  public makeNodeTypes() {
    this.nodes = MakeTypesFromGraphUseCase.makeNodeTypes(this.nodesloaded);
    if (!this.isNodeSet()) {
      if (this.nodes[0]) {
        this.currentNode = this.nodes[0].name;
      }
    }
  }

  /**
   * Creates a list of unique relation types based on the current list of relations.
   */
  public makeRelationTypes() {
    this.relations = MakeTypesFromGraphUseCase.makeRelationTypes(this.relationsloaded);
    if (!this.isRelationSet()) {
      if (this.relations[0]) {
        this.currentRelation = this.relations[0].name;
      }
    }
  }

  /**
   * Checks if the current node type exists in the types list.
   * @returns True if the node has been set.
   */
  private isNodeSet() {
    for (let i in range(this.nodes.length)) {
      let node = this.nodes[i];
      if (this.currentNode == node.name) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if the current relation type exists in the types list.
   * @returns True if the function has been set.
   */
  private isRelationSet() {
    for (let i in range(this.relations.length)) {
      let relation = this.relations[i];
      if (this.currentRelation == relation.name) {
        return true;
      }
    }
    return false;
  }

  /**
   * Updates the state variables and dropdowns. Should be triggered on switching nodes in the dropdown.
   * @param newCurrentNode The new node type that has been selected.
   */
  public onNodeChange(newCurrentNode: string) {
    this.currentNode = newCurrentNode;
    this.makeNodeTypes();
  }

  /**
   * Tells the FSSViewModel what node attributes have been changed, when something has changed.
   */
  public onAttributeChange(attributeSelected: string) {
    //Retrieve the current visualisation and set the vis dropdown to this.
    this.currentAttribute = attributeSelected;
    if (this.isNodeAttributeNumber(attributeSelected)) {
      this.fssViewModel.selectedAttributeNumerical = attributeSelected;
      this.fssViewModel.changeSelectedAttributeNumerical(
        this.getTheScaleCalculationForNodes(
          attributeSelected,
          this.minimalNodeSize,
          this.maximalNodeSize,
        ),
      );
    } else {
      this.fssViewModel.selectedAttributeCatecorigal = attributeSelected;
      this.fssViewModel.changeSelectedAttributeCatecorigal(
        this.getTheColourCalculationForNodes(attributeSelected),
      );
    }
  }

  /**
   * Tells the FSSViewModel what relation attributes have been changed, when something has changed.
   */
  public onRelationAttributeChange(attributeSelected: string) {
    //Retrieve the current visualisation and set the vis dropdown to this.
    this.currentRelationAttribute = attributeSelected;
    if (this.isRelationAttributeNumber(attributeSelected)) {
      this.fssViewModel.relationSelectedAttributeNumerical = attributeSelected;
      this.fssViewModel.changeRelationSelectedAttributeNumerical(
        this.getTheScaleCalculationForRelations(
          this.fssViewModel.relationSelectedAttributeNumerical,
          this.minimalRelationWidth,
          this.maximalRelationWidth,
        ),
      );
    } else {
      this.fssViewModel.relationSelectedAttributeCatecorigal = attributeSelected;
      this.fssViewModel.changeRelationSelectedAttributeCatecorigal(
        this.getTheColourCalculationForRelations(
          this.fssViewModel.relationSelectedAttributeCatecorigal,
        ),
      );
    }
  }

  /**
   * Gets a list of all different attributes that are present in the loaded nodes list.
   * @returns List of strings that each represent an attribute.
   */
  public getUniqueNodeAttributes() {
    let uniqueValues: string[] = [];
    this.nodesloaded.forEach((node) => {
      if (node.attributes) {
        for (const attribute in node.attributes) {
          if (uniqueValues.find((x) => x == attribute) == undefined) {
            uniqueValues.push(attribute);
          }
        }
      }
    });
    return uniqueValues;
  }

  /**
   * Gets a list with unique relation attributes that are presented in the loaded relations list.
   * @returns List of string that each represent an attribute.
   */
  public getUniqueRelationAttributes() {
    let uniqueValues: string[] = [];
    this.relationsloaded.forEach((relation) => {
      if (relation.attributes) {
        for (const attribute in relation.attributes) {
          if (uniqueValues.find((x) => x == attribute) == undefined) {
            uniqueValues.push(attribute);
          }
        }
      }
    });
    return uniqueValues;
  }

  /**
   * Checks if the attribute is a numerical value.
   * @param attribute The attribute of the nodes to check for.
   * @returns True if all values of that attribute are numbers. Returns false
   * if one or more values for this attribute are not a number
   */
  public isNodeAttributeNumber(attribute: string): boolean {
    let values: number[] = [];
    for (const node of this.nodesloaded) {
      if (node.attributes) {
        if (node.attributes[attribute] != undefined) {
          if (isNaN(node.attributes[attribute])) return false;
        }
      }
    }
    return true;
  }

  /**
   * Checks if all values of an attribute of the list of relations are a number.
   * @param attribute The attribute of the relations to check for
   * @returns True if all values of that attribute are numbers. Returns false
   * if one or more values for this attribute are not a number
   */
  public isRelationAttributeNumber(attribute: string): boolean {
    for (const relation of this.relationsloaded) {
      if (relation.attributes) {
        if (relation.attributes[attribute] != undefined) {
          if (isNaN(relation.attributes[attribute])) return false;
        }
      }
    }
    return true;
  }

  /**
   * Gets the scaling value for pixel sizes.
   * @param attribute The selected attribute.
   * @param minP Minimum node size.
   * @param maxP Maximum node size.
   * @returns Scaling value as a number.
   */
  public getTheScaleCalculationForNodes(
    attribute: string,
    minP: number,
    maxP: number,
  ): (x: number) => number {
    let values: number[] = [];
    this.nodesloaded.forEach((node) => {
      if (node.attributes) {
        if (node.attributes[attribute] != undefined) {
          values.push(node.attributes[attribute]);
        }
      }
    });

    //value min/max
    let minX = Math.min(...values);
    let maxX = Math.max(...values);

    let a = (maxP - minP) / (maxX - minX);
    let b = maxP - a * maxX;

    //linear scaling between minP and maxP
    return (x: number) => a * x + b; // - minX;
  }

  /**
   * Creates a function to get the colours for each value of a given attribute of a relation.
   * @param attribute The attribute to generate a colour calculation for.
   * @returns Returns colourisation fucntion for each different attribute value
   */
  public getTheColourCalculationForNodes(attribute: string): (x: string) => string {
    let uniqueValues: string[] = [];
    this.nodesloaded.forEach((node) => {
      if (node.attributes) {
        if (node.attributes[attribute] != undefined) {
          uniqueValues.push(node.attributes[attribute]);
        }
      }
    });

    let colours = ColourPalettes['default'].nodes;

    // Create the key value pairs.
    let valueToColour: Record<string, string> = {};
    let i = 0;
    uniqueValues.forEach((uniqueValue) => {
      valueToColour[uniqueValue] = '#' + colours[i];
      i++;
      if (i > colours.length) {
        i = 0;
      }
    });

    //Get a colour for each attribute
    return (x: string) => valueToColour[x];
  }

  /**
   * returns the scaling function for relations. (Can be used in pixel size.)
   * @param attribute Attribute to generate a size calculation for.
   * @param minP Minimal width
   * @param maxP Maximum width
   * @returns
   */
  public getTheScaleCalculationForRelations(
    attribute: string,
    minP: number,
    maxP: number,
  ): (x: number) => number {
    let values: number[] = [];
    this.relationsloaded.forEach((relation) => {
      if (relation.attributes) {
        if (relation.attributes[attribute] != undefined) {
          values.push(relation.attributes[attribute]);
        }
      }
    });

    //value min/max
    let minX = Math.min(...values);
    let maxX = Math.max(...values);

    let a = (maxP - minP) / (maxX - minX);
    let b = maxP - a * maxX;

    //linear scaling between minP and maxP
    return (x: number) => a * x + b; // - minX;
  }

  /**
   * Generates a function that returns a value for each given possible value of an attribute.
   * @param attribute The attribute to generate the function for.
   * @returns A function to get the correct colour for a relation attribute.
   */
  public getTheColourCalculationForRelations(attribute: string): (x: string) => string {
    let uniqueValues: string[] = [];
    this.relationsloaded.forEach((relation) => {
      if (relation.attributes) {
        if (relation.attributes[attribute] != undefined) {
          uniqueValues.push(relation.attributes[attribute]);
        }
      }
    });

    let colours = ColourPalettes['default'].elements.relation;

    // Create the key value pairs.
    let valueToColour: Record<string, string> = {};
    let i = 0;
    uniqueValues.forEach((uniqueValue) => {
      valueToColour[uniqueValue] = '#' + colours[i];
      i++;
      if (i > colours.length) {
        i = 0;
      }
    });

    //Get a colour for each attribute
    return (x: string) => valueToColour[x];
  }
}
