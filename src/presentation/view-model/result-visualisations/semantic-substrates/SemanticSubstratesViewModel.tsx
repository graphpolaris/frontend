/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import { NodeLinkResultType } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';

import {
  EntitiesFromSchema,
  MinMaxType,
  PlotSpecifications,
  PlotType,
  RelationType,
} from '../../../../domain/entity/semantic-substrates/structures/Types';
import ResultVisViewModel from '../ResultVisViewModel';

/** This is an interface of QueryBuildViewModel that will be given to the QueryBuilder view. */
export default interface SemanticSubstratesViewModel extends ResultVisViewModel {
  styles: ClassNameMap;

  plots: PlotType[];
  plotSpecifications: PlotSpecifications[];

  // Relations is a 3d array, with the first array denoting the outbound plot, the second array denotes the inbound plot
  // For example, [plot1][plot2] will give all edges which are outbound from plot1 and ingoing to plot2
  filteredRelations: RelationType[][][];
  allRelations: RelationType[][][];

  nodeLinkQueryResult: NodeLinkResultType;

  // Determines if connections [fromPlotIndex][toPlotIndex] will be shown
  visibleRelations: boolean[][];

  // The entities with names and attribute parameters from the schema
  entitiesFromSchema: EntitiesFromSchema;
  entitiesFromSchemaPruned: EntitiesFromSchema;

  selectedAttributeNumerical: string;
  selectedAttributeCatecorigal: string;
  relationSelectedAttributeNumerical: string;
  relationSelectedAttributeCatecorigal: string;

  nodeRadius: number;
  nodeColors: string[];

  changeSelectedAttributeNumerical(scale: (x: number) => number): void;
  changeSelectedAttributeCatecorigal(getColour: (x: string) => string): void;
  changeRelationSelectedAttributeNumerical(scale: (x: number) => number): void;
  changeRelationSelectedAttributeCatecorigal(scale: (x: string) => string): void;
  onCheckboxChanged(fromPlotIndex: number, toPlotIndex: number, value: boolean): void;
  onBrush(plotIndex: number, xRange: MinMaxType, yRange: MinMaxType): void;
  onDelete(plotIndex: number): void;
  onAxisLabelChanged(plotIndex: number, axis: 'x' | 'y', value: string): void;
  onPlotTitleChanged(plotIndex: number, entity: string, attrName: string, attrValue: string): void;

  /** Subscribes this viewmodel to schema_result from the backend. Called when the view mounts. */
  subscribeToSchemaResult(): void;

  /** unSubscribes this viewmodel to schema_result  from the backend. Called when the view unmounts. */
  unSubscribeFromSchemaResult(): void;

  addPlot(entity: string, attributeName: string, attributeValue: string): void;
}
