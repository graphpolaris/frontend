/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { ClassNameMap } from '@material-ui/styles';
import { useSemanticSubstratesStyles } from '../../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet';
import Broker from '../../../../domain/entity/broker/broker';
import {
  MinMaxType,
  PlotType,
  RelationType,
  AxisLabel,
  PlotSpecifications,
  EntitiesFromSchema,
} from '../../../../domain/entity/semantic-substrates/structures/Types';
import CalcScaledPosUseCase from '../../../../domain/usecases/semantic-substrates/CalcScaledPositionsUseCase';
import FilterUseCase from '../../../../domain/usecases/semantic-substrates/FilterUseCase';

import SemanticSubstratesViewModel from './SemanticSubstratesViewModel';
import {
  isNodeLinkResult,
  NodeLinkResultType,
} from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import { isSchemaResult } from '../../../../domain/entity/graph-schema/structures/SchemaResultType';
import CalcDefaultPlotSpecsUseCase from '../../../../domain/usecases/semantic-substrates/CalcDefaultPlotSpecsUseCase';
import ToPlotDataParserUseCase from '../../../../domain/usecases/semantic-substrates/ToPlotDataParserUseCase';
import CalcXYMinMaxUseCase from '../../../../domain/usecases/semantic-substrates/CalcXYMinMaxUseCase';
import CalcEntityAttrNamesFromSchemaUseCase from '../../../../domain/usecases/semantic-substrates/CalcEntityAttrNamesFromSchemaUseCase';
import AbstractResultVisViewModelImpl from '../AbstractResultVisViewModelImpl';
import CalcEntityAttrNamesFromResultUseCase from '../../../../domain/usecases/semantic-substrates/CalcEntityAttrNamesFromResultUseCase';

/** An implementation of the `SemanticSubstratesViewModel` to be used by the semantic-substrates view. */
export default class SemanticSubstratesViewModelImpl
  extends AbstractResultVisViewModelImpl
  implements SemanticSubstratesViewModel
{
  public styles: ClassNameMap;

  public plots: PlotType[];
  public plotSpecifications: PlotSpecifications[] = [];

  // Relations is a 3d array, with the first array denoting the outbound plot, the second array denotes the inbound plot
  // For example, [plot1][plot2] will give all edges which are outbound from plot1 and ingoing to plot2
  public allRelations: RelationType[][][];
  public filteredRelations: RelationType[][][];

  public nodeLinkQueryResult: NodeLinkResultType = { nodes: [], edges: [] };

  // Determines if connections [fromPlotIndex][toPlotIndex] will be shown
  public visibleRelations: boolean[][];

  public nodeRadius = 3;
  public nodeColors: string[] = ['#D56A50', '#1E9797', '#d49350', '#1e974a', '#D49350'];

  private gapBetweenPlots = 50;

  // Used for filtering the relations when brushing (brush is the square selection tool)
  public filtersPerPlot: { x: MinMaxType; y: MinMaxType }[];

  public entitiesFromSchema: EntitiesFromSchema;
  public entitiesFromSchemaPruned: EntitiesFromSchema;

  public selectedAttributeNumerical: string;
  public selectedAttributeCatecorigal: string;
  public relationSelectedAttributeNumerical: string;
  public relationSelectedAttributeCatecorigal: string;

  /**
   * These functions are mock function for now, but can be properly implemented later down the line.
   */
  public scaleCalculation: (x: number) => number = (x: number) => {
    return 3;
  };
  public colourCalculation: (x: string) => string = (x: string) => {
    return '#d56a50';
  };
  public relationColourCalculation: (x: string) => string = (x: string) => {
    return '#d49350';
  };
  private relationScaleCalculation: (x: number) => number = (x: number) => {
    return 1;
  };

  /** Initializes the plots and relations arrays as empty arrays. */
  public constructor() {
    super();

    this.styles = useSemanticSubstratesStyles();

    this.plotSpecifications = [];

    this.plots = [];
    this.allRelations = [];
    this.filteredRelations = [];

    this.selectedAttributeNumerical = 'long';
    this.selectedAttributeCatecorigal = 'state';
    this.relationSelectedAttributeNumerical = 'Distance';
    this.relationSelectedAttributeCatecorigal = 'Day';

    this.filtersPerPlot = [];

    this.visibleRelations = [];

    this.entitiesFromSchema = { entityNames: [], attributesPerEntity: {} };
    this.entitiesFromSchemaPruned = { entityNames: [], attributesPerEntity: {} };
  }

  /**
   * Consumes a message coming from the backend.
   * @param message The returned query result from the backend.
   */
  public consumeMessageFromBackend(message: unknown): void {
    if (isNodeLinkResult(message)) {
      // Only apply new result if we have a valid schema
      if (this.entitiesFromSchema.entityNames.length == 0) {
        console.error('Semantic substrates: No valid schema available.');
        return;
      }

      this.nodeLinkQueryResult = message;

      // Generate default plots, if there aren't any currently
      if (this.plotSpecifications.length == 0) {
        this.plotSpecifications = CalcDefaultPlotSpecsUseCase.calculate(message);
      }
      this.entitiesFromSchemaPruned =
        CalcEntityAttrNamesFromResultUseCase.CalcEntityAttrNamesFromResult(
          message,
          this.entitiesFromSchema,
        );

      this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);

      this.notifyViewAboutChanges();
    } else if (isSchemaResult(message)) {
      // When a schema is received, extract the entity names, and attributes per data type
      this.entitiesFromSchema = CalcEntityAttrNamesFromSchemaUseCase.calculate(message);

      this.notifyViewAboutChanges();
    }
  }

  /**
   * Update the scaling of the nodes after a new attribute has been selected. This function is called when the new attribute has numbers as values.
   * @param scale The scaling function provided by the config panel ViewModel.
   */
  public changeSelectedAttributeNumerical(scale: (x: number) => number): void {
    this.scaleCalculation = scale;
    this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);
    this.notifyViewAboutChanges();
  }

  /**
   * Update the colours of the nodes after a new attribute has been selected. This function is called when the new attribute has NaNs as values.
   * @param getColour The colouring function provided by the config panel ViewModel.
   */
  public changeSelectedAttributeCatecorigal(getColour: (x: string) => string): void {
    this.colourCalculation = getColour;
    this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);
    this.notifyViewAboutChanges();
  }

  /**
   * Update the scaling of the nodes after a new relation attribute has been selected. This function is called when the new relation attribute has numbers as values.
   * @param scale The scaling function provided by the config panel ViewModel.
   */
  public changeRelationSelectedAttributeNumerical(scale: (x: number) => number): void {
    this.relationScaleCalculation = scale;
    this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);
    this.notifyViewAboutChanges();
  }

  /**
   * Update the colours of the nodes after a new relation attribute has been selected. This function is called when the new relation attribute has NaNs as values.
   * @param getColour The colouring function provided by the config panel ViewModel.
   */
  public changeRelationSelectedAttributeCatecorigal(getColour: (x: string) => string): void {
    this.relationColourCalculation = getColour;
    this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);
    this.notifyViewAboutChanges();
  }

  /**
   * Apply plot specifications to a node link query result. Calculates the plots and relations using the provided plot specs.
   * @param {PlotSpecifications[]} plotSpecs The plotspecs to filter the new plots with.
   * @param {NodeLinkResultType} queryResult The query result to apply the plot specs to.
   */
  private applyNewPlotSpecifications(
    plotSpecs: PlotSpecifications[],
    queryResult: NodeLinkResultType,
  ): void {
    this.plotSpecifications = plotSpecs;

    // Parse the incoming data to plotdata with the auto generated plot specifications
    const { plots, relations } = ToPlotDataParserUseCase.parseQueryResult(
      queryResult,
      this.plotSpecifications,
      this.relationSelectedAttributeNumerical,
      this.relationSelectedAttributeCatecorigal,
      this.relationScaleCalculation,
      this.relationColourCalculation,
    );
    this.allRelations = relations;
    // Clone relations to filteredRelations, because the filters are initialized with no constraints
    this.filteredRelations = relations.map((i) => i.map((j) => j.map((v) => v)));

    // Calculate the scaled positions to the width and height of a plot
    let yOffset = 0;
    this.plots = plots.map((plot, i) => {
      const minmaxAxis = CalcXYMinMaxUseCase.calculate(plot);
      const scaledPositions = CalcScaledPosUseCase.calculate(plot, minmaxAxis.x, minmaxAxis.y);

      // Collect all possible values for a certain attribute, used for the autocomplete title field
      const possibletTitleAttributeValues = queryResult.nodes.reduce(
        (values: Set<string>, node) => {
          // Filter on nodes which are of the entity type in the plotspec
          // Use a Set so we only collect unique values
          if (
            node.id.split('/')[0] == plotSpecs[i].entity &&
            plotSpecs[i].labelAttributeType in node.attributes
          )
            values.add(node.attributes[plotSpecs[i].labelAttributeType]);
          return values;
        },
        new Set<string>(),
      );

      // Accumulate the yOffset for each plot, with the width and gapbetweenplots
      const thisYOffset = yOffset;
      yOffset += plot.height + this.gapBetweenPlots;
      return {
        title: plot.title,
        nodes: plot.nodes.map((node, i) => {
          return { ...node, scaledPosition: scaledPositions[i] };
        }),
        selectedAttributeNumerical: this.selectedAttributeNumerical,
        scaleCalculation: this.scaleCalculation,
        selectedAttributeCatecorigal: this.selectedAttributeCatecorigal,
        colourCalculation: this.colourCalculation,
        minmaxXAxis: minmaxAxis.x,
        minmaxYAxis: minmaxAxis.y,
        width: plot.width,
        height: plot.height,
        yOffset: thisYOffset,
        possibleTitleAttributeValues: Array.from(possibletTitleAttributeValues),
      };
    });

    // Initialize filters with no constraints
    this.filtersPerPlot = this.plots.map((plot) => {
      return {
        x: { min: plot.minmaxXAxis.min, max: plot.minmaxXAxis.max },
        y: { min: plot.minmaxYAxis.min, max: plot.minmaxYAxis.max },
      };
    });

    // Initialize the visible relations with all false values
    if (this.visibleRelations.length != this.plots.length)
      this.visibleRelations = new Array(this.plots.length)
        .fill([])
        .map(() => new Array(this.plots.length).fill(false));
  }

  /**
   * Updates the visualisation when a checkbox is pressed.
   * @param fromPlotIndex Which plot the connections should come from.
   * @param toPlotIndex Which plot the connections should go to.
   * @param value Whether the box has been checked or not.
   */
  public onCheckboxChanged(fromPlotIndex: number, toPlotIndex: number, value: boolean): void {
    this.visibleRelations[fromPlotIndex][toPlotIndex] = value;
    this.notifyViewAboutChanges();
  }

  /** Callback for the square selection tool on the plots.
   * Changes the filter of the plot index and applies this new filter.
   * @param {number} plotIndex The index of the plot where there is brushed.
   * @param {MinMaxType} xRange The min and max x values of the selection.
   * @param {MinMaxType} yRange The min and max y values of the selection.
   */
  public onBrush(plotIndex: number, xRange: MinMaxType, yRange: MinMaxType) {
    // Change the filter of the corresponding plot
    this.filtersPerPlot[plotIndex] = {
      x: xRange,
      y: yRange,
    };

    // Apply the new filter to the relations
    FilterUseCase.filterRelations(
      this.allRelations,
      this.filteredRelations,
      this.plots,
      this.filtersPerPlot,
      plotIndex,
    );

    this.notifyViewAboutChanges();
  }

  /**
   * Prepends a new plot specifications and re-applies this to the current nodelink query result.
   * @param {string} entity The entity filter for the new plot.
   * @param {string} attributeName The attribute to filter on for the new plot.
   * @param {string} attributeValue The value for the attributeto filter on for the new plot.
   */
  public addPlot(entity: string, attributeName: string, attributeValue: string): void {
    this.plotSpecifications = [
      {
        entity: entity,
        labelAttributeType: attributeName,
        labelAttributeValue: attributeValue,
        xAxis: AxisLabel.evenlySpaced, // Use default evenly spaced and # outbound connections on the x and y axis
        yAxis: AxisLabel.outboundConnections,
        xAxisAttributeType: '',
        yAxisAttributeType: '',
        width: 800, // The default width and height are 800x200
        height: 200,
      },
      ...this.plotSpecifications,
    ];

    this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);

    this.notifyViewAboutChanges();
  }

  /**
   * Callback when the delete button of a plot is pressed.
   * Will remove values at plotIndex from the `plotSpecifications`, `plots` , `filtersPerPlot` , `relations` , `filteredRelations` and `visibleRelations`.
   * @param {number} plotIndex The index for the plot that needs to be deleted.
   */
  public onDelete(plotIndex: number): void {
    this.plotSpecifications.splice(plotIndex, 1);

    // Recalculate the plot y offsets with this plot removed.
    for (let i = plotIndex + 1; i < this.plots.length; i++)
      this.plots[i].yOffset -= this.plots[plotIndex].height + this.gapBetweenPlots;

    this.plots.splice(plotIndex, 1);
    this.filtersPerPlot.splice(plotIndex, 1);

    this.allRelations.splice(plotIndex, 1);
    this.allRelations = this.allRelations.map((r) => r.filter((_, i) => i != plotIndex));
    this.filteredRelations.splice(plotIndex, 1);
    this.filteredRelations = this.filteredRelations.map((r) => r.filter((_, i) => i != plotIndex));
    this.visibleRelations.splice(plotIndex, 1);
    this.visibleRelations = this.visibleRelations.map((r) => r.filter((_, i) => i != plotIndex));

    this.notifyViewAboutChanges();
  }

  /**
   * Changes the axislabel of a plot.
   * @param {number} plotIndex The plot number for which to change an axis.
   * @param {'x' | 'y'} axis The axis to change. Can only be 'x' or 'y'.
   * @param {string} newLabel The axis label.
   */
  public onAxisLabelChanged(plotIndex: number, axis: 'x' | 'y', newLabel: string): void {
    const plotSpec = this.plotSpecifications[plotIndex];

    const axisLabels: string[] = Object.values(AxisLabel);
    if (axisLabels.includes(newLabel) && newLabel != AxisLabel.byAttribute) {
      // If the new axis label is one of "# outbound conn", "# inbound conn" or "evenly spaced"
      if (axis === 'x') plotSpec.xAxis = newLabel as AxisLabel;
      else if (axis === 'y') plotSpec.yAxis = newLabel as AxisLabel;
    } else {
      // Else it is an attribute of the entity
      if (axis === 'x') {
        plotSpec.xAxis = AxisLabel.byAttribute;
        plotSpec.xAxisAttributeType = newLabel;
      } else if (axis === 'y') {
        plotSpec.yAxis = AxisLabel.byAttribute;
        plotSpec.yAxisAttributeType = newLabel;
      }
    }

    this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);

    this.notifyViewAboutChanges();
  }

  /**
   * Applies the new plot filter. Called when the user changes the plot title/filter.
   * @param {number} plotIndex The plotindex of which the title is changed.
   * @param {string} entity The new entity value for the plot filter, might be unchanged.
   * @param {string} attrName The new attribute name for the plot filter, might be unchanged.
   * @param {string} attrValue The new attribute value for the plot filter.
   */
  public onPlotTitleChanged(
    plotIndex: number,
    entity: string,
    attrName: string,
    attrValue: string,
  ): void {
    // If the entity or the attrName changed, auto select a default attrValue
    if (
      (entity != this.plotSpecifications[plotIndex].entity ||
        attrName != this.plotSpecifications[plotIndex].labelAttributeType) &&
      attrValue == this.plotSpecifications[plotIndex].labelAttributeValue
    ) {
      const firstValidNode = this.nodeLinkQueryResult.nodes.find(
        (node) => node.id.split('/')[0] == entity && attrName in node.attributes,
      );
      if (firstValidNode != undefined) attrValue = firstValidNode.attributes[attrName];
    }

    this.plotSpecifications[plotIndex] = {
      ...this.plotSpecifications[plotIndex],
      entity,
      labelAttributeType: attrName,
      labelAttributeValue: attrValue,
    };

    this.applyNewPlotSpecifications(this.plotSpecifications, this.nodeLinkQueryResult);

    this.notifyViewAboutChanges();
  }

  /**
   * Subscribe to the schema result routing key so that this view model is receiving results that are to be visualised.
   */
  public subscribeToSchemaResult(): void {
    Broker.instance().subscribe(this, 'schema_result');
  }

  /**
   * Unsubscribe from the schema result routing key so that this view model no longer handles query results from the backend.
   */
  public unSubscribeFromSchemaResult(): void {
    Broker.instance().unSubscribe(this, 'schema_result');
  }
}
