/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import Broker from '../../../../../domain/entity/broker/broker';
import SemanticSubstratesViewModelImpl from '../SemanticSubstratesViewModelImpl';

/** Testsuite for the recieving of results from the broker. */
jest.mock(
  '../../../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet',
);

describe('SemanticSubstratesViewModelImpl: Broker subscriptions', () => {
  it('should consume schema results when subscribed', () => {
    const viewModel = new SemanticSubstratesViewModelImpl();

    const mockConsumeMessages = jest.fn();
    viewModel.consumeMessageFromBackend = mockConsumeMessages;

    viewModel.subscribeToSchemaResult();
    Broker.instance().publish('test schema result', 'schema_result');
    expect(mockConsumeMessages.mock.calls[0][0]).toEqual('test schema result');

    viewModel.unSubscribeFromSchemaResult();
    Broker.instance().publish('test schema result', 'schema_result');
    expect(mockConsumeMessages).toBeCalledTimes(1);
  });
});
