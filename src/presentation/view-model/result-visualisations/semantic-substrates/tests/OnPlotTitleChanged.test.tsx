/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import SemanticSubstratesViewModelImpl from '../SemanticSubstratesViewModelImpl';
import mockNodeLinkResult from '../../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import mockSchema from '../../../../../data/mock-data/schema-result/2ndChamberSchemaMock';

/** Testsuite for the changing of the title. */
jest.mock(
  '../../../../view/result-visualisations/semantic-substrates/SemanticSubstratesStylesheet.ts',
);

describe('SemanticSubstratesViewModelImpl:OnPlotTitleChanged', () => {
  it('should change the plot title', () => {
    // Initialize a ViewModel with some plots
    const viewModelImpl = new SemanticSubstratesViewModelImpl();
    viewModelImpl.consumeMessageFromBackend(mockSchema);
    viewModelImpl.consumeMessageFromBackend(mockNodeLinkResult);

    const onViewModelChangedMock = jest.fn();
    const baseViewMock = {
      onViewModelChanged: onViewModelChangedMock,
    };
    viewModelImpl.attachView(baseViewMock);

    const i = 1;
    const oldPlotSpec = viewModelImpl.plotSpecifications[i];
    viewModelImpl.onPlotTitleChanged(i, 'commissies', 'naam', 'Defensie');

    expect(viewModelImpl.plotSpecifications[i]).toEqual({
      ...oldPlotSpec,
      entity: 'commissies',
      labelAttributeType: 'naam',
      labelAttributeValue: 'Defensie',
    });

    // And check if the NotifyViewAboutChanges is called
    expect(onViewModelChangedMock).toBeCalledTimes(1);
  });
});
