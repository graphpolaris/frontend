/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { NodesAttrCollection } from './AttributesConfigPanelViewModelImpl';

/** This is an interface of AttributesConfigPanelViewModel that will be given to the AttributesConfigPanel view. */
export default interface AttributesConfigPanelViewModel {
  visibleAttributes: NodesAttrCollection;
  currentNode: string | undefined;

  toggleAttribute(attribute: string): void;
  getAttributes(): Record<string, boolean>;
  nodeOnChange(s: string): void;
  nodeSelect(): JSX.Element;
  makeNodeTypes(): void;
}
