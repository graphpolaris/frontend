import mockSchema from '../../../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import ResultNodeLinkParserUseCase from '../../../../../domain/usecases/node-link/ResultNodeLinkParserUseCase';
import NodeLinkViewModelImpl from '../../node-link/NodeLinkViewModelImpl';
import AttributesConfigPanelViewModelImpl from './AttributesConfigPanelViewModelImpl';
import { ColourPalettes } from '../../../../../domain/entity/customization/colours';
import { LocalStorage } from '../../../../../data/drivers/LocalStorage';
import { GraphType } from '../../../../../domain/entity/node-link/structures/Types';
import testGraph from '../../../../../data/mock-data/node-link/Miserables';
import mockNodeLinkResult from '../../../../../data/mock-data/query-result/big2ndChamberQueryResult';

let resultNodeLinkParserUseCase: ResultNodeLinkParserUseCase;
let nodeLinkViewModel: NodeLinkViewModelImpl;
let graph: GraphType;
let viewModel: AttributesConfigPanelViewModelImpl;

beforeEach(() => {
  resultNodeLinkParserUseCase = new ResultNodeLinkParserUseCase();
  nodeLinkViewModel = new NodeLinkViewModelImpl(
    resultNodeLinkParserUseCase,
    testGraph,
    400,
    300,
    ColourPalettes[LocalStorage.instance().cache.currentColours.key],
  );
  viewModel = new AttributesConfigPanelViewModelImpl(nodeLinkViewModel);
  generateGraph();
  viewModel.makeNodeTypes();
});

function generateGraph() {
  //generate graph
  nodeLinkViewModel.consumeMessageFromBackend(mockSchema);
  nodeLinkViewModel.consumeMessageFromBackend(mockNodeLinkResult);
  graph = nodeLinkViewModel.graph;
}

describe('AttributesConfigPanelViewModel', () => {
  it('Loads the correct nodes and checklist based on the dataset', () => {
    const expectedNodes = ['kamerleden', 'commissies'];
    const expectedAttributes = ['anc', 'img', 'leeftijd', 'naam', 'partij', 'woonplaats'];
    // check if the right nodes has loaded
    expect(Object.keys(viewModel.visibleAttributes)).toEqual(
      expectedNodes,
    );
    // check if the right attributes have loaded
    expect(Object.keys(viewModel.visibleAttributes['kamerleden'])).toEqual(
      expectedAttributes,
    );
  });

  it('Correctly switches between nodes within the same graph', () => {
    expect(viewModel.currentNode).toEqual('kamerleden');
    viewModel.nodeOnChange('commissies');
    expect(viewModel.currentNode).toEqual('commissies');
  });

  it('Updates the attributes list in NodeLink', () => {
    expect(viewModel.visibleAttributes['kamerleden']['leeftijd']).toBeTruthy();
    viewModel.toggleAttribute('leeftijd'); // simulate click on attribute checkbox
    expect(viewModel.visibleAttributes['kamerleden']['leeftijd']).toBeFalsy();
  });
});
