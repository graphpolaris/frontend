import { TypeNode } from '../../../../../domain/entity/node-link/structures/Types';
import AbstractBaseViewModelImpl from '../../../AbstractBaseViewModelImpl';
import React, { useRef, useState } from 'react';
import NodeLinkViewModel from '../../node-link/NodeLinkViewModel';
import AttributesConfigPanelViewModel from './AttributesConfigPanelViewModel';

export type AttributesCollection = Record<string, boolean>;
export type NodesAttrCollection = Record<string, AttributesCollection>;

export default class AttributesConfigPanelViewModelImpl
  extends AbstractBaseViewModelImpl
  implements AttributesConfigPanelViewModel
{
  public visibleAttributes: NodesAttrCollection;
  public currentNode: string | undefined;
  private nodeLinkViewModel: NodeLinkViewModel;
  private groupNodes: TypeNode[];

  public constructor(nodeLinkViewModel: NodeLinkViewModel) {
    super();
    this.visibleAttributes = {};
    this.nodeLinkViewModel = nodeLinkViewModel;
    this.groupNodes = [];
    this.currentNode = '';
    this.makeNodeTypes();
  }

  /**
   * Toggles an attribute of the currently selected node
   * @param attribute Which attribute should be toggled
   */
  public toggleAttribute(attribute: string): void {
    if (!this.currentNode) return;

    this.visibleAttributes[this.currentNode][attribute] =
      !this.visibleAttributes[this.currentNode][attribute];
    this.nodeLinkViewModel.UpdateAttributes(this.nodeLinkViewModel.graph);
    this.notifyViewAboutChanges();
  }

  /** on switching nodes in the dropdown, update variables and dropdowns */
  public nodeOnChange(s: string): void {
    if (this.groupNodes.length == 0) return;

    this.currentNode = s;
    this.initAttributes();
    this.notifyViewAboutChanges();
  }

  /** initial generation for the node select element */
  public nodeSelect(): JSX.Element {
    return (
      <select id="nodeSelect" onChange={(e) => this.nodeOnChange(e.target.value)}>
        {this.groupNodes.map((node) => (
          <option key={node.name} value={node.name}>
            {node.name}
          </option>
        ))}
      </select>
    );
  }

  /**
   * Returns attributes of currentNode and whether they are visible
   * @returns Dictionary where the key is the attribute name and the value whether the attribute should be visible or not
   */
  public getAttributes(): Record<string, boolean> {
    this.nodeLinkViewModel.visibleAttributes = this.visibleAttributes;
    return this.currentNode ? this.visibleAttributes[this.currentNode] : {};
  }

  /**
   * Gets the attributes for each group node if they are not known yet. Added attributes are visible by default
   */
  public initAttributes(): void {
    this.groupNodes.forEach((node) => {
      if (this.visibleAttributes[node.name] === undefined) this.visibleAttributes[node.name] = {};
      node.attributes.forEach((attr) => {
        if (this.visibleAttributes[node.name][attr] === undefined)
          this.visibleAttributes[node.name][attr] = true;
      });
    });
  }

  /**
   * Gets which group nodes there are in NodeLink, changes `currentNode` to the first node in the list and gets their attributes with `initAttributes()`
   */
  public makeNodeTypes() {
    let newGroupNodes = this.newGroupNodes();
    if (this.sameGroupNodes(newGroupNodes)) return;

    this.groupNodes = newGroupNodes;
    this.currentNode = newGroupNodes.length > 0 ? this.groupNodes[0].name : undefined;
    this.visibleAttributes = {};
    this.initAttributes();
  }

  /**
   * Gets which group nodes there are in the current NodeLink graph
   * @returns A list of group nodes
   */
  private newGroupNodes(): TypeNode[] {
    let newGroupNodes = [];
    for (let entry of this.nodeLinkViewModel.graph.nodes) {
      let entryType = entry.id.split('/')[0];
      let node = newGroupNodes.find((x) => x.name == entryType);
      if (node) {
        // if node is already logged then insert any unlogged attribute names
        // loop over realNode attribute names not stored yet, push them on the GroupNode
        for (let attr in entry.attributes) {
          if (node.attributes.indexOf(attr) == -1) {
            node.attributes.push(attr);
          }
        }
      } else {
        //if node not in groupNodes yet
        // push all attributes on the groupNode
        let newattributes = [];
        for (let attr in entry.attributes) {
          newattributes.push(attr);
        }
        newGroupNodes.push({
          name: entryType,
          attributes: newattributes,
          type: entry.type,
          visualisations: [],
        });
      }
    }

    return newGroupNodes;
  }

  /**
   * Checks if a new list of group nodes is the same as the current list
   * @param newGroupNodes The new list of group nodes
   * @returns If the new list is the same as `this.groupNodes`
   */
  private sameGroupNodes(newGroupNodes: TypeNode[]): boolean {
    for (let groupNode of newGroupNodes) {
      let node = this.groupNodes.find((x) => x.name == groupNode.name);
      if (!node) {
        return false; // new node type found
      }
    }
    return newGroupNodes.length == this.groupNodes.length; // contains no different elements and have same length
  }
}
