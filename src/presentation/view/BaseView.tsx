/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling/interfaces files.
 * See testing plan for more details.*/

export default interface BaseView {
  /** Will be called by the ViewModel to update state and render this component. */
  onViewModelChanged(): void;
}
