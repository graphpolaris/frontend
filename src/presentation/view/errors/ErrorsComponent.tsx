/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import React, { ReactElement } from 'react';
import ErrorsViewModel from '../../view-model/errors/ErrorsViewModel';
import ErrorsViewModelImpl, { ErrorMessage } from '../../view-model/errors/ErrorsViewModelImpl';
import BaseView from '../BaseView';
import './Errors.scss';

/** ErrorsComponentProps is an interface containing the ErrorsViewModel. */
export interface ErrorsComponentProps {
  listenFor: string[];
}

class ErrorsComponent
  extends React.Component<ErrorsComponentProps>
  implements BaseView
{
  private errorsViewModel: ErrorsViewModel;

  constructor(props: ErrorsComponentProps) {
    super(props);

    this.errorsViewModel = new ErrorsViewModelImpl(this.props.listenFor);
  }

  /** The componenDidMount function is called after the site is loaded and the component attached. */
  public componentDidMount(): void {
    this.errorsViewModel.attachView(this);
  }

  /** The componentWillUnmount function is called when the site is closed to remove the components. */
  public componentWillUnmount(): void {
    this.errorsViewModel.detachView();
  }

  /** Called from the view model and updates the state of this component. */
  public onViewModelChanged(): void {
    this.setState({
      errorBoolComponent: this.errorsViewModel.errorBool,
      errorMessageComponent: this.errorsViewModel.errorMessage,
    });
  }

  /** Renders the component. */
  render(): ReactElement {
    const componentHidden = this.errorsViewModel.errorBool ? "" : "hidden";

    return (
      <div className={"errorsComponent root " + componentHidden}>
        <div 
          className="errorBox"
          style={{ 
            backgroundColor: '#e9e9e9', 
            color: 'black', 
            borderBottom: '6px solid #ff6961' 
          }}
        >
          <button onClick={() => this.errorsViewModel.closeError()}>X</button>
          <h1>{this.errorsViewModel.errorMessage.errorStatus}</h1>
          <p>queryID: {this.errorsViewModel.errorMessage.queryID}</p>
        </div>
      </div>
    );
  }
}

export default ErrorsComponent;