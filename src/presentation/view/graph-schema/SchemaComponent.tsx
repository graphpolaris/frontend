/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import BaseView from '../BaseView';
import SchemaViewModel from '../../view-model/graph-schema/SchemaViewModel';
import ReactFlow, {
  ControlButton,
  Controls,
  FlowElement,
  ReactFlowProvider,
} from 'react-flow-renderer';
import { ClassNameMap, WithStyles, withStyles } from '@material-ui/styles';
import SettingsIcon from '@material-ui/icons/Settings';
import exportIcon from '../icons/ExportIcon.png';

import { CircularProgress, ListItemText, Menu, MenuItem } from '@material-ui/core';
import { useStyles } from './SchemaStyleSheet';

export let currentColours: any;

/** SchemaComponentProps is an interface containing the SchemaViewModel. */
export interface SchemaComponentProps extends WithStyles<typeof useStyles> {
  schemaViewModel: SchemaViewModel;
  currentColours: any;
}

/** The interface for the state in the schema component */
export interface SchemaComponentState {
  zoom: number;
  visible: boolean;
  elements: FlowElement[];
  myRef: React.RefObject<HTMLDivElement>;
  // Elements holder for the export menu
  exportMenuAnchor?: Element;
}

/** This class attaches the observers and renders the schema to the screen */
class SchemaComponent
  extends React.Component<SchemaComponentProps, SchemaComponentState>
  implements BaseView
{
  private schemaViewModel: SchemaViewModel;
  private currentColours: any;

  public constructor(props: SchemaComponentProps) {
    super(props);

    const { schemaViewModel } = props;
    this.schemaViewModel = schemaViewModel;

    this.state = {
      zoom: schemaViewModel.zoom,
      myRef: schemaViewModel.myRef,
      visible: schemaViewModel.visible,
      elements: [
        ...schemaViewModel.elements.nodes,
        ...schemaViewModel.elements.edges,
        ...schemaViewModel.elements.selfEdges,
        schemaViewModel.nodeQualityPopup,
        schemaViewModel.attributeAnalyticsPopupMenu,
      ],
      exportMenuAnchor: undefined,
    };
  }

  /** Attach the Viewmodel Observer and Attach the Websocket Observer. */
  public componentDidMount(): void {
    this.schemaViewModel.subscribeToSchemaResult();
    this.schemaViewModel.subscribeToAnalyticsData();
    this.schemaViewModel.attachView(this);
  }

  /** Deattach the Viewmodel Observer and Attach the Websocket Observer. */
  public componentWillUnmount(): void {
    this.schemaViewModel.unSubscribeFromSchemaResult();
    this.schemaViewModel.unSubscribeFromAnalyticsData();
    this.schemaViewModel.detachView();
  }

  /**
   * We update the state of our statecomponent on each update of the viewmodel.
   * Gets the parameters from schemaviewmodel.
   */
  public onViewModelChanged(): void {
    this.setState({
      myRef: this.schemaViewModel.myRef,
      zoom: this.schemaViewModel.zoom,
      visible: this.schemaViewModel.visible,
      elements: [
        ...this.schemaViewModel.elements.nodes,
        ...this.schemaViewModel.elements.edges,
        ...this.schemaViewModel.elements.selfEdges,
        this.schemaViewModel.nodeQualityPopup,
        this.schemaViewModel.attributeAnalyticsPopupMenu,
      ],
    });
  }

  /** The function of the component that renders the schema */
  public render(): JSX.Element {
    const { zoom, elements, myRef, visible } = this.state;
    const styles = this.props.classes;
    currentColours = this.props.currentColours;

    return (
      <div
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: '#' + currentColours.visBackground,
        }}
        ref={myRef}
      >
        <ReactFlowProvider>
          <ReactFlow
            className={styles.schemaPanel}
            elements={elements}
            defaultZoom={zoom}
            nodeTypes={this.schemaViewModel.nodeTypes}
            edgeTypes={this.schemaViewModel.edgeTypes}
            nodesDraggable={false}
            onlyRenderVisibleElements={false}
            onLoad={this.schemaViewModel.onLoad}
          >
            <Controls
              showInteractive={false}
              showZoom={false}
              showFitView={true}
              className={styles.controls}
            >
              <ControlButton
                className={styles.exportButton}
                title={'Export graph schema'}
                onClick={(event) => {
                  event.stopPropagation();
                  this.setState({
                    ...this.state,
                    exportMenuAnchor: event.currentTarget,
                  });
                }}
              >
                <img src={exportIcon} width={21}></img>
              </ControlButton>
            </Controls>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                visibility: visible ? 'visible' : 'hidden',
              }}
            >
              <CircularProgress />
            </div>
          </ReactFlow>
        </ReactFlowProvider>
        <Menu
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          anchorEl={this.state.exportMenuAnchor}
          keepMounted
          open={Boolean(this.state.exportMenuAnchor)}
          onClose={() => this.setState({ ...this.state, exportMenuAnchor: undefined })}
        >
          <MenuItem className={styles.menuText} onClick={() => this.schemaViewModel.exportToPNG()}>
            {'Export to PNG'}
          </MenuItem>
          <MenuItem className={styles.menuText} onClick={() => this.schemaViewModel.exportToPDF()}>
            {'Export to PDF'}
          </MenuItem>
        </Menu>
      </div>
    );
  }
}
export default withStyles(useStyles)(SchemaComponent);
