/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import { BorderTopOutlined, PowerInputSharp } from '@material-ui/icons';
import { ClassNameMap } from '@material-ui/styles';
import { SchemaThemeHolder } from '../../../domain/entity/css-themes/themeHolder';

export const useStyles = (theme: Theme) =>
  createStyles({
    schemaPanel: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      '& div': {
        '& svg': {
          zIndex: 2,
        },
      },
    },

    /**
     * When changing the height of the nodes, or the height of the buttons on the nodes, note that there is a hardcoded value 'height'
     * in schemaViewModelImpl (in the method 'setRelationNodePosition') that is based on these heights.
     */
    entityNode: {
      background: SchemaThemeHolder.entity.baseColor,
      display: 'flex',
      borderRadius: '1px',
      fontFamily: SchemaThemeHolder.fontFamily,
      fontWeight: 'bold',
      fontSize: `${SchemaThemeHolder.fontSize}px`,
      width: `${SchemaThemeHolder.entity.width}px`,
      lineHeight: `${SchemaThemeHolder.entity.height}px`,
    },

    nodeWrapper: {
      display: 'inherit',
      color: SchemaThemeHolder.entity.textColor,
      textAlign: 'center',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: 'inherit',
    },

    nodeSpan: {
      margin: '0 0.5em 0 1em',
      float: 'right',
    },

    nodeData: {
      flexGrow: 2,
    },

    entityNodeAttributesBox: {
      height: 20,
      position: 'absolute',
      left: '115px',
      top: '-33%',
      transform: 'translateX(50%)',
      borderRadius: '1px',
      boxShadow: '-1px 2px 5px #888888',
      textAlign: 'right',
    },

    entityNodeNodesBox: {
      height: 20,
      position: 'absolute',
      left: '115px',
      top: '54%',
      transform: 'translateX(50%)',
      borderRadius: '1px',
      boxShadow: '-1px 2px 5px #888888',
      textAlign: 'right',
    },

    /**
     * Code for the shape of the relationNodes
     */
    relationNode: {
      height: 36,
      display: 'flex',
      fontFamily: SchemaThemeHolder.fontFamily,
      fontWeight: 'bold',
      fontSize: `${SchemaThemeHolder.fontSize}px`,
      width: `${SchemaThemeHolder.relation.width}px`,
      lineHeight: `${SchemaThemeHolder.relation.height}px`,
    },

    relationNodeTriangleGeneral: {
      position: 'absolute',
      width: 0,
      height: 0,
      margin: 'auto 0',
      borderStyle: 'solid',
      borderColor: 'transparent',
    },

    relationNodeTriangleLeft: {
      transform: 'translateX(-100%)',
      top: '0px',
      borderWidth: '18px 24px 18px 0',
    },

    relationNodeSmallTriangleLeft: {
      transform: 'translateX(-100%)',
      top: '30px',
      borderWidth: '0 8px 6px 0',
    },

    relationNodeTriangleRight: {
      transform: `translateX(${SchemaThemeHolder.relation.width}px)`,
      top: '0px',
      borderWidth: '18px 0 18px 24px',
    },

    relationNodeSmallTriangleRight: {
      transform: `translateX(${SchemaThemeHolder.relation.width}px)`,
      top: '30px',
      borderWidth: '0 0 6px 8px',
    },

    relationNodeAttributesBox: {
      position: 'absolute',
      top: '-4px',
      transform: `translateX(${SchemaThemeHolder.relation.width + 4}px)`,
      clipPath: 'polygon(0 0, 100% 0, 100% 100%, 26.5px 100%)',
      height: 20,
      textAlign: 'right',
    },

    relationNodeNodesBox: {
      position: 'absolute',
      top: '20px',
      transform: `translateX(${SchemaThemeHolder.relation.width + 4}px)`,
      clipPath: 'polygon(26.5px 0, 100% 0, 100% 100%, 0 100%)',
      height: 20,
      textAlign: 'right',
    },

    arrowup: {
      width: 0,
      height: 0,
      position: 'absolute',
      left: '50%',
      top: '-20%',
      transform: 'translateX(-50%)',
      margin: '0 auto',
      backgroundColor: 'transparent',
      borderStyle: 'solid',
      borderTopWidth: 0,
      borderRightWidth: 8,
      borderBottomWidth: 5,
      borderLeftWidth: 8,
      borderTopColor: 'transparent',
      borderRightColor: 'transparent',
      borderLeftColor: 'transparent',
    },

    arrowdown: {
      width: 0,
      height: 0,
      position: 'absolute',
      left: '50%',
      bottom: '-20%',
      transform: 'translateX(-50%)',
      margin: '0 auto',
      backgroundColor: 'transparent',
      borderStyle: 'solid',
      borderTopWidth: 5,
      borderRightWidth: 8,
      borderBottomWidth: 0,
      borderLeftWidth: 8,
      borderRightColor: 'transparent',
      borderBottomColor: 'transparent',
      borderLeftColor: 'transparent',
    },

    controls: {
      left: 'auto !important',
      bottom: 'auto !important',
      top: '10px',
      right: '20px',
      width: 'auto !important',
    },

    exportButton: {
      left: 'auto !important',
      bottom: 'auto !important',
      top: '10px',
      right: '20px',
      '& svg': {
        transform: 'scale(1.4)',
      },
    },

    menuText: {
      fontSize: 'small',
      fontFamily: 'Poppins, sans-serif',
    },
  });
