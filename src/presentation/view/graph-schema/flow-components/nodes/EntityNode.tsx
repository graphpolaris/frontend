/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState } from 'react';
import { Node, Handle, Position } from 'react-flow-renderer';
import { makeStyles } from '@material-ui/core/styles';
import { useStyles } from '../../SchemaStyleSheet';
import { currentColours } from '../../SchemaComponent';
import {
  calcWidthEntityNodeBox,
  calculateAttributeQuality,
  calculateEntityQuality,
} from '../../../../util/graph-schema/utils';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

/**
 * EntityNode is the node that represents the database entities.
 * It has four handles on the top, bottom, right and left to connect edges.
 * It also has a text to display the entity name and show the amount of attributes it contains.
 * @param node Node with data for the specified entity.
 */
export default function EntityNode(node: Node) {
  const styles = madeStyles();
  const [hidden, setHidden] = useState<boolean>(true);
  const data = node.data;
  /**
   * adds drag functionality in order to be able to drag the entityNode to the schema
   * @param event React Mouse drag event
   */
  const onDragStart = (event: React.DragEvent<HTMLDivElement>) => {
    event.dataTransfer.setData(
      'application/reactflow',
      JSON.stringify({ type: 'entity', name: node.id }),
    );
    event.dataTransfer.effectAllowed = 'move';
  };

  /**
   * displays the NodeQualityPopup when clicked, and hides them when clicked again
   */
  const onClickToggleNodeQualityPopup = (): void => {
    data.toggleNodeQualityPopup(node.id);
  };

  /**
   * displays the attribute analytics popup menu when clicked, and hides them when clicked again
   */
  const onClickToggleAttributeAnalyticsPopupMenu = (): void => {
    data.toggleAttributeAnalyticsPopupMenu(node.id);
  };

  return (
    <div
      className={styles.entityNode}
      onDragStart={(event) => onDragStart(event)}
      draggable
      style={{
        backgroundColor: '#' + currentColours.nodesBase[0],
        borderTop: `4px solid ${'#' + currentColours.nodesBase[0]}`,
        borderBottom: `6px solid ${'#' + currentColours.elements.entityBase[0]}`,
      }}
    >
      <div
        className={styles.entityNodeAttributesBox}
        onClick={() => onClickToggleAttributeAnalyticsPopupMenu()}
        style={{
          borderBottomColor: '#' + currentColours.elements.entityBase[0],
          width: calcWidthEntityNodeBox(data.attributes.length, data.nodeCount) + 'ch',
          background: `linear-gradient(-90deg, 
           ${'#' + currentColours.nodesBase} 0%, 
           ${'#' + currentColours.nodesBase} ${calculateAttributeQuality(data)}%, 
           ${'#' + currentColours.elements.entitySecond[0]} ${calculateAttributeQuality(data)}%)`,
        }}
      >
        <span
          style={{
            paddingLeft: '5px',
            float: 'left',
          }}
        >
          A
        </span>
        <span className={styles.nodeSpan}>{data.attributes.length}</span>
      </div>
      <div
        className={styles.entityNodeNodesBox}
        onClick={() => onClickToggleNodeQualityPopup()}
        style={{
          borderBottomColor: '#' + currentColours.elements.entityBase[0],
          width: calcWidthEntityNodeBox(data.attributes.length, data.nodeCount) + 'ch',
          background: `linear-gradient(-90deg, 
           ${'#' + currentColours.nodesBase} 0%, 
           ${'#' + currentColours.nodesBase} ${calculateEntityQuality(data)}%, 
           ${'#' + currentColours.elements.entitySecond[0]} ${calculateEntityQuality(data)}%)`,
        }}
      >
        <span
          style={{
            paddingLeft: '5px',
            float: 'left',
          }}
        >
          N
        </span>
        <span className={styles.nodeSpan}>{data.nodeCount}</span>
      </div>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entitySourceLeft"
        position={Position.Left}
        type="source"
        hidden={Array.from(data.handles).includes('entitySourceLeft') ? false : true}
      ></Handle>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entityTargetLeft"
        position={Position.Left}
        type="target"
        hidden={Array.from(data.handles).includes('entityTargetLeft') ? false : true}
      ></Handle>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entitySourceRight"
        position={Position.Right}
        type="source"
        hidden={Array.from(data.handles).includes('entitySourceRight') ? false : true}
      ></Handle>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entityTargetRight"
        position={Position.Right}
        type="target"
        hidden={Array.from(data.handles).includes('entityTargetRight') ? false : true}
      ></Handle>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entitySourceTop"
        position={Position.Top}
        type="source"
        hidden={Array.from(data.handles).includes('entitySourceTop') ? false : true}
      ></Handle>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entityTargetTop"
        position={Position.Top}
        type="target"
        hidden={Array.from(data.handles).includes('entityTargetTop') ? false : true}
      ></Handle>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entitySourceBottom"
        position={Position.Bottom}
        type="source"
        hidden={Array.from(data.handles).includes('entitySourceBottom') ? false : true}
      ></Handle>
      <Handle
        style={{ pointerEvents: 'none' }}
        id="entityTargetBottom"
        position={Position.Bottom}
        type="target"
        hidden={Array.from(data.handles).includes('entityTargetBottom') ? false : true}
      ></Handle>
      <div className={styles.nodeWrapper}>
        <span className={styles.nodeData} style={{ color: '#' + currentColours.elementText }}>
          {node.id}
        </span>
      </div>
    </div>
  );
}
