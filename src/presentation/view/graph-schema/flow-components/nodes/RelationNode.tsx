/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { createRef, useRef, useState } from 'react';
import { Node } from 'react-flow-renderer';
import { makeStyles } from '@material-ui/core/styles';
import { useStyles } from '../../SchemaStyleSheet';
import { currentColours } from '../../SchemaComponent';
import {
  calculateAttributeQuality,
  calculateRelationQuality,
  calcWidthRelationNodeBox,
} from '../../../../util/graph-schema/utils';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

/**
 * Relation node component that renders a relation node for the schema.
 * Can be dragged and dropped to the query builder.
 * @param data Input data for the specified relation.
 */
export default function RelationNode(node: Node) {
  const styles = madeStyles();
  const [hidden, setHidden] = useState<boolean>(true);
  const data = node.data;

  /**
   * Adds drag functionality in order to be able to drag the relationNode to the schema.
   * @param event React Mouse drag event.
   */
  const onDragStart = (event: React.DragEvent<HTMLDivElement>) => {
    event.dataTransfer.setData(
      'application/reactflow',
      JSON.stringify({
        type: 'relation',
        name: node.id,
        from: data.from,
        to: data.to,
        collection: data.collection,
      }),
    );
    event.dataTransfer.effectAllowed = 'move';
  };

  /**
   * Displays the NodeQualityPopup when clicked, and hides them when clicked again
   */
  const onClickToggleNodeQualityPopup = (): void => {
    data.toggleNodeQualityPopup(data.collection);
  };

  /**
   * displays the attribute analytics popup menu when clicked, and hides them when clicked again
   */
  const onClickToggleAttributeAnalyticsPopupMenu = (): void => {
    data.toggleAttributeAnalyticsPopupMenu(data.collection);
  };

  const widthExternalBoxes = calcWidthRelationNodeBox(data.attributes.length, data.nodeCount);

  return (
    <div onDragStart={(event) => onDragStart(event)} draggable>
      <div
        className={styles.relationNode}
        style={{
          background: '#' + currentColours.nodesBase[0],
          borderTop: `4px solid ${'#' + currentColours.nodesBase[0]}`,
          borderBottom: `6px solid ${'#' + currentColours.elements.relationBase[0]}`,
        }}
      >
        <div
          className={[styles.relationNodeTriangleGeneral, styles.relationNodeTriangleLeft].join(
            ' ',
          )}
          style={{ borderRightColor: '#' + currentColours.nodesBase[0] }}
        ></div>
        <div
          className={[
            styles.relationNodeTriangleGeneral,
            styles.relationNodeSmallTriangleLeft,
          ].join(' ')}
          style={{ borderRightColor: '#' + currentColours.elements.relationBase[0] }}
        ></div>
        <div
          className={[styles.relationNodeTriangleGeneral, styles.relationNodeTriangleRight].join(
            ' ',
          )}
          style={{ borderLeftColor: '#' + currentColours.nodesBase[0] }}
        ></div>
        <div
          className={[
            styles.relationNodeTriangleGeneral,
            styles.relationNodeSmallTriangleRight,
          ].join(' ')}
          style={{ borderLeftColor: '#' + currentColours.elements.relationBase[0] }}
        ></div>
        <div
          className={styles.arrowup}
          style={{ borderBottomColor: '#' + currentColours.nodesBase[0] }}
        ></div>
        <div
          className={styles.relationNodeAttributesBox}
          onClick={() => onClickToggleAttributeAnalyticsPopupMenu()}
          style={{
            width: widthExternalBoxes + 'px',
            backgroundImage: `linear-gradient(-90deg, 
              ${'#' + currentColours.nodesBase} 0%, 
              ${'#' + currentColours.nodesBase} ${calculateAttributeQuality(data)}%, 
              ${'#' + currentColours.elements.relationSecond[0]} ${calculateAttributeQuality(
              data,
            )}%)`,
          }}
        >
          <span
            style={{
              paddingLeft: '25px',
              float: 'left',
            }}
          >
            A
          </span>
          <span className={styles.nodeSpan}>{data.attributes.length}</span>
        </div>
        <div
          className={styles.relationNodeNodesBox}
          onClick={() => onClickToggleNodeQualityPopup()}
          style={{
            width: widthExternalBoxes + 'px',
            backgroundImage: `linear-gradient(-90deg, 
              ${'#' + currentColours.nodesBase} 0%, 
              ${'#' + currentColours.nodesBase} ${calculateRelationQuality(data)}%, 
              ${'#' + currentColours.elements.relationSecond[0]} ${calculateRelationQuality(
              data,
            )}%)`,
          }}
        >
          <span
            style={{
              paddingLeft: '25px',
              float: 'left',
            }}
          >
            N
          </span>
          <span className={styles.nodeSpan}>{data.nodeCount}</span>
        </div>

        <div
          className={styles.nodeWrapper}
          style={{ backgroundColor: '#' + currentColours.nodesBase[0] }}
        >
          <span className={styles.nodeData}>{data.collection}</span>
        </div>
        <div
          className={styles.arrowdown}
          style={{ borderTopColor: '#' + currentColours.elements.relationBase[0] }}
        ></div>
      </div>
    </div>
  );
}
