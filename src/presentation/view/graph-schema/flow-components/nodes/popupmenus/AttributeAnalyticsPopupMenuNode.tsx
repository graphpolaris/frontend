/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import {
  ButtonBase,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@material-ui/core';
import { ExpandMore, Visibility } from '@material-ui/icons';
import React, { ReactElement } from 'react';
import { FlowElement } from 'react-flow-renderer';
import {
  AttributeAnalyticsData,
  AttributeCategory,
  NodeType,
} from '../../../../../../domain/entity/graph-schema/structures/Types';
import { currentColours } from '../../../SchemaComponent';
import Filter from './filterbar';
import Search from './searchbar';

/**
 * AttributeAnalyticsPopupMenuNode is the node that represents the popup menu that shows the attribute analytics for an entity or relation
 * @param data Input data of type AttributeAnalyticsData, which is for the popup menu.
 */
export default function AttributeAnalyticsPopupMenu({ data }: FlowElement<AttributeAnalyticsData>) {
  if (data == undefined) throw new Error('No Attribute Analytics data is available for the node.');
  let entityOrRelationBase: NodeType;
  if (data.nodeType == NodeType.entity) entityOrRelationBase = currentColours.elements.entityBase;
  else entityOrRelationBase = currentColours.elements.relationBase;

  let dataCopy = {...data};
  let attributesDivs = calculateAttributeDivs(data);
  let render = renderPopupMenu(dataCopy, attributesDivs);
  return render;

  /**
   * Creates the divs of the attributes with their data processed.
   * @param attributeData Data that is contained in the attributes.
   * @returns A list of divs that contain the appearance and information of the attributes.
   */
  function calculateAttributeDivs(attributeData: AttributeAnalyticsData): any[] {
    let attributesDivs: any[] = [];
    if(attributeData.isAttributeDataIn) {
      attributeData.attributes.forEach(attributeItem => {
        attributesDivs.push(
          <Accordion 
            className="attributesAccordion">
            <AccordionSummary
              className="attribute"
              expandIcon={<ExpandMore className="expandIcon" />}
            >
              {attributeItem.attribute.name}
            </AccordionSummary>
            <AccordionDetails className="accordionDetails">{attributeItem.category}</AccordionDetails>
            <AccordionDetails className="accordionDetails">
              <span>Null-values:</span>
              <span
                className="nullAmountValue"
                style={{
                  backgroundColor: '#' + entityOrRelationBase[0],
                }}
              >
                {attributeItem.nullAmount}%
              </span>
            </AccordionDetails>
            <AccordionDetails className="accordionDetails">
              <div className="attributeButtons">
                <span>See visualisation</span>
                <span className="rightSideValue">
                  <Visibility className="visualisationEye" />
                </span>
              </div>
            </AccordionDetails>
            <AccordionDetails className="accordionDetails">
              <div
                className="attributeButtons"
                onClick={() =>
                  attributeData.onClickPlaceInQueryBuilderButton(
                    attributeItem.attribute.name,
                    attributeItem.attribute.type,
                  )
                }
              >
                <span>Place in query builder</span>
              </div>
            </AccordionDetails>
          </Accordion>
        );
      });
    } else {
      attributeData.attributes.forEach((attributeItem) => {
        attributesDivs.push(
          <Accordion className="attributesAccordion">
            <AccordionSummary
              className="attribute"
              expandIcon={<ExpandMore className="expandIcon" />}
            >
              {attributeItem.attribute.name}
            </AccordionSummary>
            <AccordionDetails className="accordionDetails">
              <div
                className="attributeButtons"
                onClick={() =>
                  attributeData.onClickPlaceInQueryBuilderButton(
                    attributeItem.attribute.name,
                    attributeItem.attribute.type,
                  )
                }
              >
                <span>Place in query builder</span>
              </div>
            </AccordionDetails>
          </Accordion>,
        );
      });
    }
    return attributesDivs;
  }

  /**
   * Renders the popup-menu.
   * @param data This contains the data of the attributes.
   * @param attributesDivs This contains the list of divs of the attributes.
   */
  function renderPopupMenu(data: AttributeAnalyticsData, attributesDivs: any[]) {
    return (
      <div>
        <div className="title">
          <span id="name">Attributes</span>
          <span className="rightSideValue">{data.attributes.length}</span>
        </div>
        <Search 
          searchForAttributes={(
            searchbarValue: string,
          ) => 
            data.searchForAttributes(
              data.nodeID,
              searchbarValue,
          )}/>
        <Filter 
          data={data}
          resetAttributeFilters={() =>
            data.resetAttributeFilters(
              data.nodeID,
          )}
          applyAttributeFilters={(
            dataType: AttributeCategory,
            predicate: string,
            percentage: number,
          ) =>
            data.applyAttributeFilters(
              data.nodeID,
              dataType,
              predicate,
              percentage,
          )}/>
  
        <div className="attributesWrapper">{attributesDivs}</div>
  
        <div className="closeButtonWrapper">
          <ButtonBase
            onClick={() => data.onClickCloseButton()}
            id="closeButton"
            style={{
              backgroundColor: '#' + entityOrRelationBase[0],
            }}
          >
            Close
          </ButtonBase>
        </div>
      </div>
    );
  }
  
}
