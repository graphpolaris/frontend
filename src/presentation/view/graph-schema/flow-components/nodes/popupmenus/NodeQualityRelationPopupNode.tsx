/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import { ButtonBase } from '@material-ui/core';
import React from 'react';
import { FlowElement } from 'react-flow-renderer';
import {
  NodeQualityDataForRelations,
  NodeType,
} from '../../../../../../domain/entity/graph-schema/structures/Types';
import { currentColours } from '../../../SchemaComponent';

/**
 * NodeQualityRelationPopupNode is the node that represents the popup that shows the node quality for a relation
 * @param data Input data of type NodeQualityDataForRelations, which is for the popup.
 */
export default function NodeQualityRelationPopupNode({
  data,
}: FlowElement<NodeQualityDataForRelations>): JSX.Element {
  if (data == undefined) throw new Error('No node quality data is available for this node.');

  if (data.isAttributeDataIn)
    return (
      <div>
        <div className="title">
          <span id="name">Nodes</span>
          <span className="rightSideValue">{data.nodeCount}</span>
        </div>
        <div className="information">
          <div>
            <span>Null values</span>
            <span className="rightSideValue">{data.attributeNullCount}</span>
          </div>
          <div>
            <span>From-ratio</span>
            <span className="rightSideValue">{data.fromRatio * 100}%</span>
          </div>
          <div>
            <span>To-ratio</span>
            <span className="rightSideValue">{data.toRatio * 100}%</span>
          </div>
        </div>
        <div className="closeButtonWrapper">
          <ButtonBase
            onClick={() => data.onClickCloseButton()}
            id="closeButton"
            style={{
              backgroundColor: '#' + currentColours.elements.relationBase[0],
            }}
          >
            Close
          </ButtonBase>
        </div>
      </div>
    );
  else
    return (
      <div>
        <div className="title">
          <span id="name">Nodes</span>
        </div>
        <div className="information"></div>
        <div className="closeButtonWrapper">
          <ButtonBase
            onClick={() => data.onClickCloseButton()}
            id="closeButton"
            style={{
              backgroundColor: '#' + currentColours.elements.relationBase[0],
            }}
          >
            Close
          </ButtonBase>
        </div>
      </div>
    );
}
