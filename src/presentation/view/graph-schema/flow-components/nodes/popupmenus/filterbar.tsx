/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
*/

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import { Accordion, AccordionDetails, AccordionSummary, ButtonBase } from "@material-ui/core";
import { ExpandMore } from "@material-ui/icons";
import React, { ReactElement, useState } from "react"
import { AttributeAnalyticsData, AttributeCategory, NodeType } from "../../../../../../domain/entity/graph-schema/structures/Types";
import { currentColours } from '../../../SchemaComponent';

/** The typing for the props of the filter bar. */
type FilterbarProps = {
    data: AttributeAnalyticsData,
    resetAttributeFilters() : void,
    applyAttributeFilters(dataType: AttributeCategory, predicate: string, percentage: number) : void, 
};

/** The variables in the state of the filter bar. */
type FilterbarState = {
  dataTypeValue: string,
  nullValuesPredicate: string,
  nullValuesValue: string,
  onHoverApplyButton: boolean,
  onHoverResetButton: boolean,
};

/** React component that renders a filter bar with input fields for choosing and changing the filters. */
export default class Filter extends React.Component<
FilterbarProps,
FilterbarState
> {
  constructor(props: FilterbarProps) {
    super(props);
    
    this.state = {
        dataTypeValue: '',
        nullValuesPredicate: '',
        nullValuesValue: '',
        onHoverApplyButton: false,
        onHoverResetButton: false,
    };
  }

  /**
   * Processes the chosen data-type in the new state.
   * @param event The event that contains the value of the chosen data-type.
   */
  public dataTypeChanged = (event: any) => {
    const newValue = event.target.value;

    this.setState({
      dataTypeValue: newValue,
    });
  }

  /**
   * Processes the chosen predicate in the new state.
   * @param event The event that contains the value of the chosen predicate.
   */
  public nullValuesPredicateChanged = (event: any) => {
    const newValue = event.target.value;

    this.setState({
      nullValuesPredicate: newValue,
    });
  }

    /**
   * Processes the chosen null-values-amount in the new state.
   * @param event The event that contains the value of the chosen null-values-amount.
   */
  public nullValuesValueChanged = (event: any) => {
    const value = event.target.validity.valid ? event.target.value : this.state.nullValuesValue;
    let newValue = value;
    if (Number (newValue) < 0) newValue = '0';
    if (Number (newValue) > 100) newValue = '100';

    this.setState({
      nullValuesValue: newValue,
    });
  }

  /**
   * This resets the current filters and let the popup-menu show the original list of attributes again.
   */
  public resetFilters = () => {
    this.setState({
        ...this.state,
        dataTypeValue: '',
        nullValuesPredicate: '',
        nullValuesValue: '',
    });
    this.props.resetAttributeFilters();
  }

  /**
   * This applies the chosen filters and let the popup-menu show only the attributes that satifsy the chosen filters.
   */
  public applyFilters = () => {
    // Check if the chosen null-amount-value is valid.
    if (!(this.state.nullValuesValue == '') && Number.isNaN(Number(this.state.nullValuesValue))) {
      this.setState({
        ...this.state,  
      });
      return;
    }

    // Sets the data-type.
    let dataType;
    if (this.state.dataTypeValue == 'Categorical') dataType = AttributeCategory.categorical;
    else if (this.state.dataTypeValue == 'Numerical') dataType = AttributeCategory.numerical;
    else if (this.state.dataTypeValue == 'Other') dataType = AttributeCategory.other;
    else dataType = AttributeCategory.undefined;

    // Sets the percentage of the null-amount-values.
    let percentage;
    if (this.state.nullValuesValue == '') percentage = -1;
    else percentage = Number (this.state.nullValuesValue);

    this.props.applyAttributeFilters(
        dataType,
        this.state.nullValuesPredicate,
        percentage,
    );
  }

  // Makes sure that the hovered button get the right hover-state.
  public toggleHoverApply = () => {
    const currentHover = this.state.onHoverApplyButton;
    this.setState({
        ...this.state,
        onHoverApplyButton: !currentHover,
        onHoverResetButton: false,
      });
  };

  // Resets all buttons to the original hover-state.
  public toggleHoverReset = () => {
    const currentHover = this.state.onHoverResetButton;
    this.setState({
        ...this.state,
        onHoverApplyButton: false,
        onHoverResetButton: !currentHover,
      });
  };

  render(): ReactElement {
    let entityOrRelationBase: NodeType;
    if (this.props.data.nodeType == NodeType.entity) entityOrRelationBase = currentColours.elements.entityBase;
    else entityOrRelationBase = currentColours.elements.relationBase;
    
    let applyButtonColor;
    if (this.state.onHoverApplyButton) applyButtonColor = '#' + entityOrRelationBase[0];
    else applyButtonColor = "inherit";

    let resetButtonColor;
    if (this.state.onHoverResetButton) resetButtonColor = '#' + entityOrRelationBase[0];
    else resetButtonColor = "inherit";

    return (
      <div className="bar">
        <Accordion 
          className={["attributesAccordion", "filterbarAccordion"].join(
              ' ',
            )}
        >
          <AccordionSummary
            className="filterbarSummary"
            expandIcon={<ExpandMore className="expandIcon" />}
          >
            Filter bar
          </AccordionSummary>
          <AccordionDetails 
            className={["accordionDetails", "accordionDetailsFilterbar"].join(
              ' ',
            )}
          >
            <span>Datatype:</span>
              <span>
                <select 
                  className={["nullAmountValue", "dataTypeSelect"].join(
                    ' ',
                  )}
                  style={{
                    backgroundColor: '#' + entityOrRelationBase[0],
                  }}
                  value={this.state.dataTypeValue} 
                  onChange={this.dataTypeChanged}>
                    <option value=""></option>
                    <option value="Categorical">Categorical</option>
                    <option value="Numerical">Numerical</option>
                    <option value="Other">Other</option>
                </select>
              </span>
          </AccordionDetails>
          <AccordionDetails 
            className={["accordionDetails", "accordionDetailsFilterbar"].join(
              ' ',
            )}
          >
            <span>Null-values:</span>
            <span 
               className={["nullAmountValue", "nullValuesBox"].join(
                ' ',
              )}
               style={{
                backgroundColor: '#' + entityOrRelationBase[0],
              }}
              >
                <input 
                className="nullValuesSelect"
                  style={{
                    backgroundColor: '#' + entityOrRelationBase[0],
                  }}
                  value={this.state.nullValuesValue} 
                  onChange={this.nullValuesValueChanged}
                  type="text"
                  pattern="[0-9]*"
                  >
                </input>
                %
              </span>
            <span>
                <select 
                  className={["nullAmountValue", "dataTypeSelect", "predicateSelect"].join(
                    ' ',
                  )}
                  style={{
                    backgroundColor: '#' + entityOrRelationBase[0],
                  }}
                  value={this.state.nullValuesPredicate} 
                  onChange={this.nullValuesPredicateChanged}
                >
                  <option value=""></option>
                  <option value="Equal">==</option>
                  <option value="NotEqual">!=</option>
                  <option value="Smaller">&#60;</option>
                  <option value="SmallerOrEqual">&#8804;</option>
                  <option value="Bigger">&#62;</option>
                  <option value="BiggerOrEqual">&#8805;</option>
              </select>
            </span>
            
            <ButtonBase
              className="attributeButtons"
              onClick={() => this.resetFilters()}
              onMouseEnter={this.toggleHoverReset}
              onMouseLeave={this.toggleHoverReset} 
              id="resetFiltersButton"
              style={{
                backgroundColor: resetButtonColor,
              }}
            >
              Reset
            </ButtonBase>
            <ButtonBase
              className="attributeButtons"
              onClick={() => this.applyFilters()}
              onMouseEnter={this.toggleHoverApply}
              onMouseLeave={this.toggleHoverApply} 
              id="applyFiltersButton"
              style={{
                backgroundColor: applyButtonColor,
              }}
            >
              Apply
            </ButtonBase>
          </AccordionDetails>
        </Accordion>
      </div>
    )
  }
}
