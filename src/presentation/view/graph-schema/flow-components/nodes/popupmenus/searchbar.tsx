/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
*/

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import React, { ReactElement, useState } from "react"
import { AttributeAnalyticsData } from "../../../../../../domain/entity/graph-schema/structures/Types";

/** The typing for the props of the searchbar. */
type SearchbarProps = {
  searchForAttributes(value: string): void, 
};

/** The variables in the state of the searchbar. */
type SearchbarState = {
  searchbarValue: string,
};

/** React component that renders a searchbar with input fields for choosing and changing the filters. */
export default class Search extends React.Component<
SearchbarProps,
SearchbarState
> {
  constructor(props: SearchbarProps) {
    super(props);
    
    this.state = {
      searchbarValue: '',
    };
  }

  /**
   * Sets the state with the new searchbar-value.
   * It also calls the function which applies the value to the list, so that the value is processed instantanious.
   * @param event The event that contains the value in the searchbar.
   */
  public searchbarValueChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;

    this.setState({
      searchbarValue: newValue,
    });

    this.props.searchForAttributes(newValue);
  }

  render(): ReactElement {
    return (
      <input
        className="bar"
        type="text" 
        name="searchValue" 
        placeholder="Searchbar"
        onChange={ this.searchbarValueChanged }  
        value={ this.state.searchbarValue }
      />
    )
  }
}
