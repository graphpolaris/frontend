/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import { ClassNameMap } from '@material-ui/styles';
import { TextField, Button, WithStyles, withStyles, NativeSelect } from '@material-ui/core';
import { useStyles } from './AddDatabaseFormUIStylesheet';

/** AddDatabaseFormProps is an interface containing the AuthViewModel. */
export interface AddDatabaseFormProps extends WithStyles<typeof useStyles> {
  open: boolean;
  onClose(): void;
  onSubmit(
    username: string,
    password: string,
    hostname: string,
    port: number,
    databaseName: string,
    internalDatabase: string,
    databaseType: string,
  ): void;
}

/** AddDatabaseFormState is an interface containing the databasehost information. */
export interface AddDatabaseFormState {
  username: string;
  password: string;
  hostname: string;
  port: number;
  databaseName: string;
  internalDatabase: string;
  databaseType: string;

  styles: ClassNameMap;
}

/** AddDatabaseForm is the View implementation for the connect screen that will be rendered. */
class AddDatabaseForm extends React.Component<AddDatabaseFormProps, AddDatabaseFormState> {
  public constructor(props: AddDatabaseFormProps) {
    super(props);

    this.state = {
      username: 'root',
      password: 'DikkeDraak',
      hostname: 'https://datastrophe.science.uu.nl/',
      port: 8529,
      databaseName: 'Tweede Kamer Dataset',
      internalDatabase: 'TweedeKamer',
      styles: this.props.classes,
      databaseType: 'arangodb',
    };
  }

  /**
   * Validates if the port value is numerical. Only then will the state be updated.
   * @param port The new port value.
   */
  private handlePortChanged(port: string): void {
    if (!isNaN(Number(port))) this.setState({ ...this.state, port: Number(port) });
  }

  /** Handles the submit button click. Calls the onSubmit in the props with all the fields. */
  private handleSubmitClicked(): void {
    this.props.onSubmit(
      this.state.username,
      this.state.password,
      this.state.hostname,
      this.state.port,
      this.state.databaseName,
      this.state.internalDatabase,
      this.state.databaseType,
    );
  }

  public render(): JSX.Element {
    const {
      username,
      password,
      port,
      hostname,
      databaseName,
      internalDatabase,
      styles,
      databaseType,
    } = this.state;

    return this.props.open ? (
      <div
        className={styles.wrapper}
        onMouseDown={() => {
          this.props.onClose();
        }}
      >
        <div
          className={styles.authWrapper}
          onMouseDown={(e) => {
            e.stopPropagation();
          }}
        >
          <h1 className={styles.header}>Database Connect</h1>
          <form
            className={styles.formWrapper}
            onSubmit={(event: React.FormEvent) => {
              event.preventDefault();
              this.handleSubmitClicked();
            }}
          >
            <div className={styles.loginContainer}>
              <TextField
                className={styles.passLabel}
                label="Database name"
                type="databaseName"
                value={databaseName}
                onChange={(event) =>
                  this.setState({ ...this.state, databaseName: event.currentTarget.value })
                }
                required
              />
            </div>
            <div className={styles.loginContainer}>
              <NativeSelect
                className={styles.passLabel}
                value={databaseType}
                onChange={(event) => {
                  this.setState({ ...this.state, databaseType: event.currentTarget.value });
                }}
              >
                <option value="arangodb">arangodb</option>
                <option value="neo4j">neo4j</option>
              </NativeSelect>
            </div>
            <div className={styles.loginContainerRow}>
              <TextField
                className={styles.hostLabel}
                label="Hostname/IP"
                type="hostname"
                value={hostname}
                onChange={(event) =>
                  this.setState({ ...this.state, hostname: event.currentTarget.value })
                }
                required
              />
              <TextField
                className={styles.portLabel}
                label="Port"
                type="port"
                value={port}
                onChange={(event) => this.handlePortChanged(event.currentTarget.value)}
                required
              />
            </div>
            <div className={styles.loginContainer}>
              <TextField
                className={styles.userLabel}
                label="Username"
                type="username"
                value={username}
                onChange={(event) =>
                  this.setState({ ...this.state, username: event.currentTarget.value })
                }
                required
              />
            </div>
            <div className={styles.loginContainer}>
              <TextField
                className={styles.passLabel}
                label="Password"
                type="password"
                value={password}
                onChange={(event) =>
                  this.setState({ ...this.state, password: event.currentTarget.value })
                }
                required
              />
            </div>
            <div className={styles.loginContainer}>
              <TextField
                className={styles.passLabel}
                label="Internal database"
                type="internalDatabaseName"
                value={internalDatabase}
                onChange={(event) =>
                  this.setState({ ...this.state, internalDatabase: event.currentTarget.value })
                }
                required
              />
            </div>
            <div className={styles.loginContainerButton}>
              <Button variant="outlined" type="submit">
                Submit
              </Button>
              <Button
                className={styles.cancelButton}
                variant="outlined"
                onClick={() => {
                  this.props.onClose();
                }}
              >
                Cancel
              </Button>
            </div>
          </form>
        </div>
      </div>
    ) : (
      <></>
    );
  }
}
export default withStyles(useStyles)(AddDatabaseForm);
