/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const useStyles = (theme: Theme) =>
  createStyles({
    wrapper: {
      height: '100vh',
      display: 'flex',
      alignItems: 'center',
      width: '100vw',
      backgroundColor: 'rgba(0,0,0,0.87)',
      position: 'absolute',
      zIndex: 1225,
    },
    authWrapper: {
      fontFamily: 'Poppins, sans-serif',
      display: 'flex',
      flexDirection: 'column',
      flexWrap: 'wrap',
      justifyContent: 'center',
      maxWidth: '400px',
      margin: 'auto',
      overflow: 'auto',
      minHeight: '300px',
      backgroundColor: '#f7f8fc',
      padding: '30px',
      borderRadius: '5px',
      paddingBottom: '300px',
    },
    header: {
      textAlign: 'center',
    },
    formWrapper: {},
    loginContainer: {
      margin: '5px 0px !important',
    },
    loginContainerRow: {
      display: 'flex',
      flexDirection: 'row',
      margin: '5px 0px',
    },
    loginContainerButton: {
      marginTop: '10px !important',
      '& button': {
        width: '100%',
      },
    },
    hostLabel: {
      flex: '1 0 66.66667%',
      '& div': {
        width: '95%',
      },
    },
    portLabel: {
      flex: '1 0 33.33334%',
    },
    userLabel: {
      width: '100%',
    },
    passLabel: {
      width: '100%',
    },
    cancelButton: {
      marginTop: '2.5%',
    },
  });
export { useStyles };
