/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import BaseView from '../BaseView';
import { ClassNameMap } from '@material-ui/styles';
import NavbarViewModel from '../../view-model/navbar/NavbarViewModel';
import {
  AppBar,
  Toolbar,
  CssBaseline,
  Typography,
  MenuItem,
  ListItemText,
  Menu,
  IconButton,
  Button,
} from '@material-ui/core/';
import { AccountCircle } from '@material-ui/icons';
import logo from './logogp.png';
import logo2 from './logogpwhite.png';
import AddDatabaseForm from './AddDatabaseForm/AddDatabaseFormComponent';
import { LocalStorage } from '../../../data/drivers/LocalStorage';

/** NavbarComponentProps is an interface containing the NavbarViewModel. */
export interface NavbarComponentProps {
  navbarViewModel: NavbarViewModel;
  currentColours: any;
  changeColourPalette: () => void;
}

/** NavbarComponentState is an interface containing the type of visualisations. */
export interface NavbarComponentState {
  styles: ClassNameMap;

  isAuthorized: boolean;
  clientID: string;
  sessionID: string;
  databases: string[];
  currentDatabase: string;

  // The anchor for rendering the user data (clientID, sessionID, add database, etc) menu
  userMenuAnchor?: Element;
  // The anchor for rendering the menu for selecting a database to use
  selectDatabaseMenuAnchor?: Element;
  // Determines if the addDatabaseForm will be shown
  showAddDatabaseForm: boolean;
}

/** NavbarComponent is the View implementation for Navbar */
export default class NavbarComponent
  extends React.Component<NavbarComponentProps, NavbarComponentState>
  implements BaseView
{
  private navbarViewModel: NavbarViewModel;

  public constructor(props: NavbarComponentProps) {
    super(props);

    const { navbarViewModel, currentColours } = props;
    this.navbarViewModel = navbarViewModel;

    this.state = {
      isAuthorized: false,
      styles: navbarViewModel.styles,
      clientID: navbarViewModel.clientID,
      sessionID: navbarViewModel.sessionID,
      databases: [],
      currentDatabase: navbarViewModel.currentDatabase,

      userMenuAnchor: undefined,
      selectDatabaseMenuAnchor: undefined,
      showAddDatabaseForm: false,
    };
  }

  public componentDidMount(): void {
    this.navbarViewModel.attachView(this);
  }

  public componentWillUnmount(): void {
    this.navbarViewModel.detachView();
  }

  /** onViewModelChanged updates the NavbarComponent each time the navbarViewModel changes */
  public onViewModelChanged(): void {
    this.setState({
      isAuthorized: this.navbarViewModel.isAuthorized,
      clientID: this.navbarViewModel.clientID,
      sessionID: this.navbarViewModel.sessionID,
      databases: this.navbarViewModel.databases,
      currentDatabase: this.navbarViewModel.currentDatabase,
    });
  }

  /** Closes the user menu. Also closes all nested menu's. */
  private closeUserMenu(): void {
    // If a nested window is open, close the main user menu a bit later
    if (this.state.selectDatabaseMenuAnchor != undefined) {
      this.setState({ selectDatabaseMenuAnchor: undefined });
      setTimeout(() => this.setState({ userMenuAnchor: undefined }), 100);
    } else this.setState({ userMenuAnchor: undefined });
  }

  /**
   * Render will render the navigation bar
   * @return {JSX.Element} The TypeScript code of the navigation bar
   */
  public render(): JSX.Element {
    const { styles } = this.state;
    const currentLogo = this.props.currentColours.logo == 'white' ? logo2 : logo;

    return (
      <div className={styles.root}>
        <CssBaseline />
        <AppBar
          title="GraphPolaris"
          style={{ zIndex: 1250, backgroundColor: '#' + this.props.currentColours.background }}
          position="fixed"
          className={styles.appBar}
        >
          <Toolbar style={{ marginLeft: -5 }}>
            <a href="https://graphpolaris.com/" className={styles.logo}>
              <img src={currentLogo} className={styles.logo} />
            </a>
            <div className={styles.menubox}>
              <Button
                className={styles.menuText}
                style={{ color: '#' + this.props.currentColours.menuText }}
                onClick={this.props.changeColourPalette}
              >
                Change Palette
              </Button>
              <Button
                href="https://graphpolaris.com/"
                className={styles.menuText}
                style={{ color: '#' + this.props.currentColours.menuText }}
              >
                Home
              </Button>
              <Button
                href="https://graphpolaris.com/index.php/products/"
                className={styles.menuText}
                style={{ color: '#' + this.props.currentColours.menuText }}
              >
                Products
              </Button>
              <Button
                href="https://graphpolaris.com#usecases"
                className={styles.menuText}
                style={{ color: '#' + this.props.currentColours.menuText }}
              >
                Use Cases
              </Button>
              <Button
                href="https://graphpolaris.com#earlyadoption"
                className={styles.menuText}
                style={{ color: '#' + this.props.currentColours.menuText }}
              >
                Contact
              </Button>
              {this.state.isAuthorized ? (
                <div>
                  <IconButton
                    color="inherit"
                    onClick={(event) =>
                      this.setState({ ...this.state, userMenuAnchor: event.currentTarget })
                    }
                  >
                    <AccountCircle htmlColor={'#' + this.props.currentColours.menuText} />
                  </IconButton>
                  <Menu
                    id="user-menus"
                    getContentAnchorEl={null}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                    transformOrigin={{ vertical: 'top', horizontal: 'left' }}
                    anchorEl={this.state.userMenuAnchor}
                    keepMounted
                    open={Boolean(this.state.userMenuAnchor)}
                    onClose={() => this.closeUserMenu()}
                  >
                    <MenuItem>
                      <ListItemText primary={'clientID: ' + this.state.clientID} />
                    </MenuItem>
                    <MenuItem>
                      <ListItemText primary={'sessionID: ' + this.state.sessionID} />
                    </MenuItem>
                    <MenuItem
                      onClick={() => this.navbarViewModel.handleLoginButtonClicked('google')}
                    >
                      <ListItemText primary={'request new'} />
                    </MenuItem>
                    <MenuItem onClick={() => LocalStorage.instance().Reset()}>
                      <ListItemText primary={'Empty localstorage'} />
                    </MenuItem>
                    <MenuItem
                      onClick={() =>
                        this.setState({
                          ...this.state,
                          userMenuAnchor: undefined,
                          showAddDatabaseForm: true,
                        })
                      }
                    >
                      <ListItemText primary={'Add database'} />
                    </MenuItem>
                    <MenuItem
                      onClick={(event) =>
                        this.setState({
                          ...this.state,
                          selectDatabaseMenuAnchor: event.currentTarget,
                        })
                      }
                    >
                      <ListItemText primary={'Change database'} />
                    </MenuItem>
                    <Menu
                      id="databases-menus"
                      getContentAnchorEl={null}
                      anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
                      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                      anchorEl={this.state.selectDatabaseMenuAnchor}
                      keepMounted
                      disableAutoFocusItem
                      open={Boolean(this.state.selectDatabaseMenuAnchor)}
                      onClose={() => this.closeUserMenu()}
                    >
                      {this.state.databases.length > 0 ? (
                        this.state.databases.map((database) => (
                          <MenuItem
                            key={database}
                            selected={database == this.state.currentDatabase}
                            onClick={() => {
                              if (this.state.currentDatabase != database) {
                                this.navbarViewModel.updateCurrentDatabase(database);
                                this.closeUserMenu();
                              }
                            }}
                          >
                            <ListItemText primary={database} />
                          </MenuItem>
                        ))
                      ) : (
                        <MenuItem key="placeholder" value="" disabled>
                          no databases connected
                        </MenuItem>
                      )}
                    </Menu>
                  </Menu>
                </div>
              ) : (
                <div>
                  <Button
                    className={styles.loginButton}
                    onClick={(event) =>
                      this.setState({ ...this.state, userMenuAnchor: event.currentTarget })
                    }
                  >
                    <span
                      className={styles.loginButtonText}
                      style={{ color: '#' + this.props.currentColours.menuText }}
                    >
                      Login
                    </span>
                  </Button>
                  <Menu
                    id="user-menus"
                    getContentAnchorEl={null}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                    transformOrigin={{ vertical: 'top', horizontal: 'left' }}
                    anchorEl={this.state.userMenuAnchor}
                    keepMounted
                    open={Boolean(this.state.userMenuAnchor)}
                    onClose={() => this.closeUserMenu()}
                  >
                    <MenuItem
                      onClick={() => this.navbarViewModel.handleLoginButtonClicked('google')}
                    >
                      <ListItemText primary={'Login with Google'} />
                    </MenuItem>
                    <MenuItem
                      onClick={() => this.navbarViewModel.handleLoginButtonClicked('github')}
                    >
                      <ListItemText primary={'Login with GitHub'} />
                    </MenuItem>
                    <MenuItem onClick={() => this.navbarViewModel.handleLoginButtonClicked('free')}>
                      <ListItemText primary={'Login with free (debug)'} />
                    </MenuItem>
                  </Menu>
                </div>
              )}
            </div>
          </Toolbar>
        </AppBar>
        <AddDatabaseForm
          open={this.state.showAddDatabaseForm}
          onClose={() => this.setState({ ...this.state, showAddDatabaseForm: false })}
          onSubmit={(...params) => {
            this.navbarViewModel.onAddDatabaseFormSubmit(...params);
            this.setState({ ...this.state, showAddDatabaseForm: false });
          }}
        />
      </div>
    );
  }
}
