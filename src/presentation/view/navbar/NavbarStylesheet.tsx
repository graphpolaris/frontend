/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      overflow: 'hidden',
    },
    appBar: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      backgroundColor: '#fff',
      height: 50,
    },
    menubox: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      width: '100%',
    },
    menuText: {
      margin: 5,
      color: '#181520',
      fontFamily: 'Poppins, sans-serif',
      fontWeight: 400,
      fontSize: 15,
      alignSelf: 'flex-end',
      textTransform: 'none',
    },
    logo: {
      margin: 'auto',
      display: 'block',
    },
    loginButton: {
      margin: 6,
    },
    loginButtonText: {
      color: 'black',
      fontWeight: 'bold',
    },
  }),
);

export { useStyles };
