/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import BaseView from '../BaseView';
import { ClassNameMap } from '@material-ui/styles';
import PanelVisualisationViewModel from '../../view-model/panel-visualisation/PanelVisualisationViewModel';
import RawJSONViewModel from '../../view-model/result-visualisations/raw-json/RawJSONViewModel';
import NodeLinkViewModel from '../../view-model/result-visualisations/node-link/NodeLinkViewModel';
import SemanticSubstratesViewModel from '../../view-model/result-visualisations/semantic-substrates/SemanticSubstratesViewModel';
import SemanticSubstrates from '../result-visualisations/semantic-substrates/SemanticSubstratesComponent';
import RawJSONComponent from '../result-visualisations/raw-json/RawJSONComponent';
import NodeLinkComponent from '../result-visualisations/node-link/NodeLinkComponent';
import { ControlButton, Controls, ReactFlowProvider } from 'react-flow-renderer';
import SettingsIcon from '@material-ui/icons/Settings';
import { ListItemText, Menu, MenuItem } from '@material-ui/core';
import PaohvisViewModel from '../../view-model/result-visualisations/paohvis/PaohvisViewModel';
import PaohvisComponent from '../result-visualisations/paohvis/PaohvisComponent';
import HivePlotsViewModel from '../../view-model/result-visualisations/hiveplots/HivePlotsViewModel';
import HivePlotsComponent from '../result-visualisations/hiveplots/HivePlotsComponent';

import NodeLinkIcon from './iconsMenu/NodelinkIcon.png';
import SemanticSubstrateIcon from './iconsMenu/SemanticSubstrateIcon.png';
import JsonIcon from './iconsMenu/JsonIcon.png';
import PaohvisIcon from './iconsMenu/PaohvisIcon.png';
import HiveplotIcon from './iconsMenu/HiveplotIcon.png';
import ChangeVisualisationIcon from './iconsMenu/ChangeVisualisationIcon.png';
import ExportIcon from '../icons/ExportIcon.png';

/** PanelVisualisationComponentProps is an interface containing the PanelVisualisationViewModel. */
export interface PanelVisualisationComponentProps {
  panelVisualisationViewModel: PanelVisualisationViewModel;
  currentColours: any;
  visible: boolean;
}

/** PanelVisualisationComponentState is an interface containing the type of visualisations. */
export interface PanelVisualisationComponentState {
  rawJSONViewModel: RawJSONViewModel;
  nodeLinkViewModel: NodeLinkViewModel;
  semanticSubstratesViewModel: SemanticSubstratesViewModel;
  paohvisViewModel: PaohvisViewModel;
  hivePlotsViewModel: HivePlotsViewModel;
  currentVisual: any;
  resultVisualisationType: string;
  styles: ClassNameMap;

  // Elements holder for the export menu
  visualMenuAnchor?: Element;
  exportMenuAnchor?: Element;
}

/** PanelVisualisationComponent is the View implementation for Visualisation Panel. */
export default class PanelVisualisationComponent
  extends React.Component<PanelVisualisationComponentProps, PanelVisualisationComponentState>
  implements BaseView
{
  private panelVisualisationViewModel: PanelVisualisationViewModel;

  /**
   * Contructor the create the PanelVisualisationComponent.
   * @param props Props holds the panelVisualisationViewModel for updating the PanelVisualisationComponent.
   */
  public constructor(props: PanelVisualisationComponentProps) {
    super(props);

    const { panelVisualisationViewModel } = props;
    this.panelVisualisationViewModel = panelVisualisationViewModel;

    this.state = {
      rawJSONViewModel: panelVisualisationViewModel.rawJSONViewModel,
      nodeLinkViewModel: panelVisualisationViewModel.nodeLinkViewModel,
      semanticSubstratesViewModel: panelVisualisationViewModel.semanticSubstratesViewModel,
      paohvisViewModel: panelVisualisationViewModel.paohvisViewModel,
      hivePlotsViewModel: panelVisualisationViewModel.hivePlotsViewModel,
      resultVisualisationType: panelVisualisationViewModel.resultVisualisationType,
      currentVisual: panelVisualisationViewModel.currentVisual,

      visualMenuAnchor: undefined,
      exportMenuAnchor: undefined,

      styles: panelVisualisationViewModel.styles,
    };
  }

  /**
   * The componentWillUnmount function is a void function.
   * It attaches the view from the panelVisualisationViewModel when the PanelVisualisationComponent mounts.
   */
  public componentDidMount(): void {
    this.panelVisualisationViewModel.attachView(this);
  }

  /**
   * The componentWillUnmount is a void function detaching the view
   * from the panelVisualisationViewModel when the PanelVisualisationComponent unmounts.
   */
  public componentWillUnmount(): void {
    this.panelVisualisationViewModel.detachView();
  }

  /** The onViewModelChanged updates the PanelVisualisationComponent each time the panelVisualisationViewModel changes. */
  public onViewModelChanged(): void {
    this.setState({
      rawJSONViewModel: this.panelVisualisationViewModel.rawJSONViewModel,
      nodeLinkViewModel: this.panelVisualisationViewModel.nodeLinkViewModel,
      semanticSubstratesViewModel: this.panelVisualisationViewModel.semanticSubstratesViewModel,
      paohvisViewModel: this.panelVisualisationViewModel.paohvisViewModel,
      hivePlotsViewModel: this.panelVisualisationViewModel.hivePlotsViewModel,
      resultVisualisationType: this.panelVisualisationViewModel.resultVisualisationType,
      currentVisual: this.panelVisualisationViewModel.currentVisual,
    });
  }

  /** Closes the cogwheel menu. Also closes all nested menu's. */
  private closeCogWheelMenu(): void {
    // If a nested window is open, close the main cogwheel menu a bit later
    if (this.state.exportMenuAnchor != undefined || this.state.visualMenuAnchor != undefined) {
      this.setState({ ...this.state, visualMenuAnchor: undefined, exportMenuAnchor: undefined });
    }
  }

  /**
   * The renderVisualisation renders the visualisation, currently either nodelink, semanticsubstrates, text number and JSON.
   * @return {JSX.Element} The TypeScript code for the visualisation.
   */
  renderVisualisation = (): JSX.Element => {
    switch (this.state.resultVisualisationType) {
      case 'node-link':
        return (
          <NodeLinkComponent
            nodeLinkViewModel={this.state.nodeLinkViewModel}
            currentColours={this.props.currentColours}
          />
        );
      case 'semantic-substrates':
        return (
          <SemanticSubstrates
            semanticSubstratesViewModel={this.state.semanticSubstratesViewModel}
          />
        );
      case 'hiveplots':
        return <HivePlotsComponent HivePlotsViewModel={this.state.hivePlotsViewModel} />;

      case 'paohvis':
        return <PaohvisComponent paohvisViewModel={this.state.paohvisViewModel} />;

      default:
        return <RawJSONComponent rawJSONViewModel={this.state.rawJSONViewModel} />;
    }
  };

  /**
   * Render will render the visualisation panel
   * @return {JSX.Element} The TypeScript code for the panel component.
   */
  public render(): JSX.Element {
    const { styles } = this.state;

    // Generate the MenuItem elements for all the possible visualisations.
    const possibleVisualisationTypes = [
      'node-link',
      'semantic-substrates',
      'json',
      'paohvis',
      'hiveplots',
    ];

    const visualisationTypeNames = [
      'Nodelink',
      'Semantic substrates',
      'JSON',
      'PAOHVis',
      'HivePlots',
    ];

    const visualisationIcons = [
      NodeLinkIcon,
      SemanticSubstrateIcon,
      JsonIcon,
      PaohvisIcon,
      HiveplotIcon,
    ];
    const visualisationMenuItems = possibleVisualisationTypes.map((visType, i) => {
      const isCurrentVis =
        this.panelVisualisationViewModel.visualTypeHolder.getVisualType() == visType;

      return (
        <MenuItem
          className={styles.menuTextChangeVisualisation}
          key={visType}
          onClick={() => {
            if (!isCurrentVis) {
              this.closeCogWheelMenu();
              this.panelVisualisationViewModel.visualChange(visType);
            }
          }}
          selected={isCurrentVis}
        >
          <img src={visualisationIcons[i]} className={styles.icon}></img>
          {visualisationTypeNames[i]}
        </MenuItem>
      );
    });

    return (
      <div
        className={styles.root}
        style={{ visibility: this.props.visible ? 'visible' : 'hidden' }}
      >
        {this.renderVisualisation()}
        <ReactFlowProvider>
          <Controls
            showInteractive={false}
            showZoom={false}
            showFitView={false} //TODO: make true and onclick function, first look at implementation other fitViews.
            className={styles.controls}
          >
            <ControlButton
              className={styles.buttons}
              title={'Change visualisation'}
              onClick={(event) => {
                event.stopPropagation();
                this.setState({
                  ...this.state,
                  visualMenuAnchor: event.currentTarget,
                });
              }}
            >
              <img src={ChangeVisualisationIcon} width={20}></img>
            </ControlButton>
            <ControlButton
              className={styles.buttons}
              title={'Export visualisation'}
              onClick={(event) => {
                event.stopPropagation();
                this.setState({
                  ...this.state,
                  exportMenuAnchor: event.currentTarget,
                });
              }}
            >
              <img src={ExportIcon} width={21}></img>
            </ControlButton>
            <Menu
              id="visualisation menu"
              getContentAnchorEl={null}
              anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              anchorEl={this.state.visualMenuAnchor}
              keepMounted
              open={Boolean(this.state.visualMenuAnchor)}
              onClose={() => this.closeCogWheelMenu()}
            >
              {visualisationMenuItems}
            </Menu>
            <Menu
              id="exportation menu"
              getContentAnchorEl={null}
              anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              anchorEl={this.state.exportMenuAnchor}
              keepMounted
              open={Boolean(this.state.exportMenuAnchor)}
              onClose={() => this.closeCogWheelMenu()}
            >
              <MenuItem
                className={styles.menuTextExport}
                onClick={() => {
                  this.closeCogWheelMenu();
                  this.state.currentVisual.exportToPNG();
                }}
              >
                {'Export to PNG'}
              </MenuItem>
              <MenuItem
                className={styles.menuTextExport}
                onClick={() => {
                  this.closeCogWheelMenu();
                  this.state.currentVisual.exportToPDF();
                }}
              >
                {'Export to PDF'}
              </MenuItem>
            </Menu>
          </Controls>
        </ReactFlowProvider>
      </div>
    );
  }
}
