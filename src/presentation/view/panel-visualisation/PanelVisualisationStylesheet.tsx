/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      flexShrink: 1,
      flexDirection: 'column',
      overflowY: 'hidden',
      overflowX: 'hidden',
      position: 'relative',
    },

    controls: {
      left: 'auto !important',
      bottom: 'auto !important',
      top: '10px',
      right: '30px',
      width: 'auto !important',
    },

    button1: {
      position: 'absolute',
      left: 'auto !important',
      bottom: 'auto !important',
      top: '3px',
      right: '30px',
      '& svg': {
        transform: 'scale(1.4)',
      },
    },
    button2: {
      position: 'absolute',
      left: 'auto !important',
      bottom: 'auto !important',
      top: '33px',
      right: '30px',
      '& svg': {
        transform: 'scale(1.4)',
      },
    },
    icon: {
      width: '30px',
      height: '30px',
      marginRight: 8,
      marginLeft: 0,
    },
    menuTextChangeVisualisation: {
      fontSize: 'small',
      fontFamily: 'Poppins, sans-serif',
      padding: 2,
      // paddingTop: 2,
      // paddingBottom: 2,
      paddingRight: 4,
      // paddingLeft: 2,
    },
    menuTextExport: {
      fontSize: 'small',
      fontFamily: 'Poppins, sans-serif',
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 10,
      paddingRight: 20,
    },
  }),
);

export { useStyles };
