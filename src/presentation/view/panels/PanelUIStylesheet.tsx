/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: '100%',
    },
    title: {
      flexGrow: 1,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      flexShrink: 0,
    },
    drawerLeft: {
      flexShrink: 0,
    },
    drawerBottom: {
      flexShrink: 0,
    },
    drawerRight: {
      flexShrink: 0,
    },
    drawerPaper: {
      borderTop: '2px solid '.concat(theme.palette.divider),
      borderLeft: '2px solid '.concat(theme.palette.divider),
      borderRight: '2px solid '.concat(theme.palette.divider),
      borderBottom: '2px solid '.concat(theme.palette.divider),
      background: '#fdfdfd', //'#fbf8f473',
    },
    drawerPaperRight: {
      borderTop: '2px solid '.concat(theme.palette.divider),
      borderLeft: '2px solid '.concat(theme.palette.divider),
      borderRight: '2px solid '.concat(theme.palette.divider),
      borderBottom: '2px solid '.concat(theme.palette.divider),
      background: '#fbf8f4',
    },
    drawerPaperBottom: {
      borderTop: '2px solid '.concat(theme.palette.divider),
      borderLeft: '2px solid '.concat(theme.palette.divider),
      borderRight: '2px solid '.concat(theme.palette.divider),
      borderBottom: '2px solid '.concat(theme.palette.divider),
      background: '#fffcf8',
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      // Necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: 'flex-start',
    },

    schemaDragger: {
      width: '5px',
      cursor: 'ew-resize',
      padding: '4px 0 0',
      borderTop: '1px solid #ddd',
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      zIndex: 100,
      backgroundColor: '#f4f7f9',
    },
    queryDragger: {
      height: '5x',
      top: 0,
      left: 0,
      right: 0,
      cursor: 'ns-resize',
      padding: '4px 0 0',
      borderTop: '1px solid #ddd',
      position: 'absolute',
      zIndex: 100,
      backgroundColor: '#f4f7f9',
    },
  }),
);
export { useStyles };
