/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';

import { Drawer, CssBaseline } from '@material-ui/core/';
import QueryBuilderViewModel from '../../view-model/query-builder/QueryBuilderViewModel';

import GraphSchemaComponent, { currentColours } from '../graph-schema/SchemaComponent';
import QueryBuilderComponent from '../query-builder/QueryBuilderComponent';
import SchemaViewModel from '../../view-model/graph-schema/SchemaViewModel';
import PanelViewModel from '../../view-model/panels/PanelsViewModel';
import BaseView from '../BaseView';

import NavbarViewModel from '../../view-model/navbar/NavbarViewModel';
import NavbarComponent from '../navbar/NavbarComponent';
import PanelVisualisationViewModel from '../../view-model/panel-visualisation/PanelVisualisationViewModel';
import PanelVisualisationComponent from '../panel-visualisation/PanelVisualisationComponent';
import QueryPanelComponent from '../query-panel/QueryPanelComponent';
import QueryStatusListViewModel from '../../view-model/query-status-list/QueryStatusListViewModel';
import FunctionsMenuViewModel from '../../view-model/query-builder/functions-menu/FunctionsMenuViewModel';

import { ColourPalettes } from '../../../domain/entity/customization/colours';
import LocalStorageRepository from '../../../domain/repository-interfaces/LocalStorageRepository';
import { LocalStorage } from '../../../data/drivers/LocalStorage';
import ErrorsComponent from '../errors/ErrorsComponent';

/** PanelComponentProps is an interface containing the PanelViewModel. */
export interface PanelComponentProps {
  panelViewModel: PanelViewModel;
}

/** PanelComponentState holds the viewmodels of the panels and their sizes. */
export interface PanelComponentState {
  queryBuilderViewModel: QueryBuilderViewModel;
  queryStatusListViewModel: QueryStatusListViewModel;
  schemaViewModel: SchemaViewModel;
  functionsMenuViewModel: FunctionsMenuViewModel;

  navbarViewModel: NavbarViewModel;
  panelVisualisationViewModel: PanelVisualisationViewModel;

  classes: any;
  navBarHeight: any;
  schemaDrawerHeight: any;
  queryDrawerHeight: any;

  schemaDrawerWidth: any;
  queryDrawerWidth: any;

  currentColours: any;
  showError: boolean;
}

/** Panels holds all drawers and their panels. */
export default class Panels
  extends React.Component<PanelComponentProps, PanelComponentState>
  implements BaseView
{
  panelViewModel: PanelViewModel;
  colourPaletteIndex: number;

  public constructor(props: PanelComponentProps) {
    super(props);
    const { panelViewModel } = props;
    this.panelViewModel = panelViewModel;

    this.colourPaletteIndex = LocalStorage.instance().cache.currentColours.index;
    this.changeColourPalette = this.changeColourPalette.bind(this);

    this.state = {
      queryBuilderViewModel: panelViewModel.queryBuilderViewModel,
      queryStatusListViewModel: panelViewModel.queryStatusListViewModel,
      schemaViewModel: panelViewModel.schemaViewModel,

      navbarViewModel: panelViewModel.navbarViewModel,
      panelVisualisationViewModel: panelViewModel.panelVisualisationViewModel,
      functionsMenuViewModel: panelViewModel.functionsMenuViewModel,

      classes: panelViewModel.classes,
      navBarHeight: panelViewModel.navBarHeight,
      schemaDrawerHeight: panelViewModel.schemaDrawerHeight,
      queryDrawerHeight: panelViewModel.queryDrawerHeight,

      schemaDrawerWidth: panelViewModel.schemaDrawerWidth,
      queryDrawerWidth: panelViewModel.queryDrawerWidth,
      currentColours: panelViewModel.currentColours,
      showError: false,
    };
  }
  /** Updates all values that have changed. */
  onViewModelChanged(): void {
    this.setState({
      navBarHeight: this.panelViewModel.navBarHeight,
      schemaDrawerHeight: this.panelViewModel.schemaDrawerHeight,
      queryDrawerHeight: this.panelViewModel.queryDrawerHeight,

      schemaDrawerWidth: this.panelViewModel.schemaDrawerWidth,
      queryDrawerWidth: this.panelViewModel.queryDrawerWidth,
      schemaViewModel: this.panelViewModel.schemaViewModel,
      functionsMenuViewModel: this.panelViewModel.functionsMenuViewModel,

      navbarViewModel: this.panelViewModel.navbarViewModel,
      panelVisualisationViewModel: this.panelViewModel.panelVisualisationViewModel,
      showError: false,
    });
  }

  /** Changes the colour palette */
  changeColourPalette(): void {
    if (this.colourPaletteIndex < ColourPalettes.availablePalettes.length - 1) {
      this.colourPaletteIndex++;
    } else {
      this.colourPaletteIndex = 0;
    }
    LocalStorage.instance().cache.currentColours.index = this.colourPaletteIndex;
    const key: string = ColourPalettes.availablePalettes[this.colourPaletteIndex];
    LocalStorage.instance().cache.currentColours.key = key;
    LocalStorage.instance().SaveToCache();
    this.setState({ currentColours: ColourPalettes[key] });
  }

  /** The componenDidMount function is called after the site is loaded and the component attached. */
  componentDidMount() {
    this.panelViewModel.attachView(this);
    // Adds a eventlistener for when the window is resized
    window.addEventListener('resize', this.panelViewModel.handleResize);
  }

  /** The componentWillUnmount function is called when the site is closed to remove the components. */
  componentWillUnmount() {
    this.panelViewModel.detachView();
    // Removes an eventlistener for resize
    window.removeEventListener('resize', this.panelViewModel.handleResize);
  }

  /** Returns the render component. */
  public render(): JSX.Element {
    const {
      schemaDrawerHeight,
      queryDrawerHeight,
      navBarHeight,
      schemaDrawerWidth,
      classes,
      queryBuilderViewModel,
      queryStatusListViewModel,
      functionsMenuViewModel,
      schemaViewModel,
      navbarViewModel,
      panelVisualisationViewModel,
      currentColours,
    } = this.state;

    return (
      <div className={classes.root} style={{ backgroundColor: '#' + currentColours.background }}>
        <CssBaseline />
        <NavbarComponent
          navbarViewModel={navbarViewModel}
          currentColours={currentColours}
          changeColourPalette={this.changeColourPalette}
        />
        <div
          style={{
            marginTop: navBarHeight,
            width: window.innerWidth - schemaDrawerWidth,
            // Height is determined by queryDrawerheight + 5px of border dragger and minus the navbar height
            height: window.innerHeight - queryDrawerHeight - 5 - navBarHeight,
            marginLeft: schemaDrawerWidth,
            marginBottom: queryDrawerHeight,
            flexShrink: 1,
          }}
        >
          <ErrorsComponent listenFor={['ml_error', 'query_error']} />
          <PanelVisualisationComponent
            panelVisualisationViewModel={panelVisualisationViewModel}
            currentColours={currentColours}
            visible={!this.state.showError}
          ></PanelVisualisationComponent>
        </div>
        <Drawer
          className={classes.drawerLeft}
          variant="persistent"
          PaperProps={{
            style: {
              width: schemaDrawerWidth,
              height: schemaDrawerHeight,
              top: navBarHeight,
              borderColor: '#' + currentColours.dragger,
              backgroundColor: '#' + currentColours.visBackground,
            },
          }}
          anchor="left"
          open={true}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div
            onMouseDown={(e) => this.panelViewModel.handleMouseDown(e, 'schema')}
            className={classes.schemaDragger}
            style={{
              backgroundColor: '#' + currentColours.dragger,
              borderTopColor: '#' + currentColours.dragger,
            }}
          />
          <GraphSchemaComponent schemaViewModel={schemaViewModel} currentColours={currentColours} />
        </Drawer>
        <Drawer
          className={classes.drawerBottom}
          variant="persistent"
          PaperProps={{
            style: {
              width: window.innerWidth - schemaDrawerWidth,
              height: queryDrawerHeight,
              left: schemaDrawerWidth,
              top: window.innerHeight - queryDrawerHeight,
              borderColor: '#' + currentColours.dragger,
              backgroundColor: '#' + currentColours.visBackground,
            },
          }}
          anchor="bottom"
          open={true}
          classes={{
            paper: classes.drawerPaperBottom,
          }}
        >
          <div
            onMouseDown={(e) => this.panelViewModel.handleMouseDown(e, 'query')}
            className={classes.queryDragger}
            style={{
              backgroundColor: '#' + currentColours.visBackground,
              borderTopColor: '#' + currentColours.dragger,
            }}
          />
          <QueryPanelComponent
            queryBuilderViewModel={queryBuilderViewModel}
            functionsMenuViewModel={functionsMenuViewModel}
            queryStatusListViewModel={queryStatusListViewModel}
            currentColours={currentColours}
          />
        </Drawer>
      </div>
    );
  }
}
