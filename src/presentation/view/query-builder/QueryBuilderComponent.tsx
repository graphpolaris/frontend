/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import ReactFlow, {
  Background,
  FlowElement,
  ConnectionMode,
  Controls,
  ControlButton,
} from 'react-flow-renderer';

import EntityFlowElement from './custom-flow-elements/EntityFlowElement';
import RelationFlowElement from './custom-flow-elements/RelationFlowElement';
import AttributeFlowElement from './custom-flow-elements/AttributeElement/AttributeFlowElement';
import ConnectionLine from './custom-flow-elements/ConnectionLine';
import QueryBuilderViewModel from '../../view-model/query-builder/QueryBuilderViewModel';
import BaseView from '../BaseView';
import EdgeLine from './custom-flow-elements/EdgeLine';
import FunctionFlowElement from './custom-flow-elements/FunctionElement/FunctionFlowElement';
import { ClassNameMap, WithStyles, withStyles } from '@material-ui/styles';
import SettingsIcon from '@material-ui/icons/Settings';
import DeleteIcon from '@material-ui/icons/Delete';
import ExportIcon from '../icons/ExportIcon.png';
import { ListItemText, Menu, MenuItem } from '@material-ui/core';
import { useStyles } from './QueryBuilderStylesheet';
import FunctionsMenu from './functions-menu/FunctionsMenu';
import FunctionsMenuViewModel from '../../view-model/query-builder/functions-menu/FunctionsMenuViewModel';
import './QueryBuilder.scss';
import { useState } from 'react';
import { AnyElement, AnyNode } from '../../../domain/entity/query-builder/structures/Nodes';

export let currentColours: any;

/** Interface for the props. Should only have a `QueryBuilderViewModel`*/
export interface QueryBuilderComponentProps extends WithStyles<typeof useStyles> {
  queryBuilderViewModel: QueryBuilderViewModel;
  functionsMenuViewModel: FunctionsMenuViewModel;

  currentColours: any;
}
/** Interface for the state of the query builder. Only has `FlowElement[]` */
export interface QueryBuilderComponentState {
  myRef: React.RefObject<HTMLDivElement>;
  elements: FlowElement[];

  // Element holder for the export menu anchor
  exportMenuAnchor?: Element;
  settingsMenuAnchor?: Element;
}

/** FlowElement typing for custom elements */
const nodeTypes = {
  entity: EntityFlowElement,
  relation: RelationFlowElement,
  attribute: AttributeFlowElement,
  function: FunctionFlowElement,
};

/** Our custom edge/link types. Currently not in use, plan to implement custom edges later on again */
const edgeTypes = {
  custom: EdgeLine,
};

/** A React component for rendering the querybuilder. Funcionality is handled by the given `QueryBuilderViewModel` */
class QueryBuilderComponent
  extends React.Component<QueryBuilderComponentProps, QueryBuilderComponentState>
  implements BaseView
{
  private queryBuilderViewModel: QueryBuilderViewModel;
  private functionsMenuViewModel: FunctionsMenuViewModel;

  public constructor(props: QueryBuilderComponentProps) {
    super(props);

    const { queryBuilderViewModel, functionsMenuViewModel } = props;
    this.functionsMenuViewModel = functionsMenuViewModel;

    this.queryBuilderViewModel = props.queryBuilderViewModel;
    this.state = {
      myRef: this.queryBuilderViewModel.myRef,
      elements: [],
      exportMenuAnchor: undefined,
      settingsMenuAnchor: undefined,
    };

    this.updateNodesholder();
  }

  /** Updates the elements in the state */
  private updateElements(elements: AnyElement[]): void {
    this.setState((oldState) => {
      return {
        myRef: oldState.myRef,
        elements: elements,
      };
    });
  }

  //TODO: unregister from old NH when changing NH
  private updateNodesholder() {
    this.queryBuilderViewModel.nodesHolder.addReactFlowListener((elements: AnyElement[]) =>
      this.updateElements(elements),
    );
  }

  public componentDidMount(): void {
    this.queryBuilderViewModel.attachView(this);
  }

  public componentWillUnmount(): void {
    this.queryBuilderViewModel.detachView();
  }

  /** Update the state of this React component to render the changes. Called by the `QueryBuilderViewModel` */
  public onViewModelChanged(): void {
    this.setState({
      myRef: this.queryBuilderViewModel.myRef,
    });
    this.updateNodesholder();
  }

  /** Render the whole app with canvas, flowelement and controls */
  public render(): JSX.Element {
    const { elements, myRef } = this.state;
    const styles = this.props.classes;
    currentColours = this.props.currentColours;

    return (
      <div
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: '#' + this.props.currentColours.visBackground,
        }}
        onKeyDown={this.queryBuilderViewModel.onKeyDown}
        ref={myRef}
      >
        <ReactFlow
          elements={elements}
          connectionLineComponent={ConnectionLine}
          connectionMode={ConnectionMode.Loose}
          snapGrid={[10, 10]}
          snapToGrid={true}
          defaultZoom={1.3}
          onElementsRemove={this.queryBuilderViewModel.onElementsRemove}
          deleteKeyCode={46}
          nodeTypes={nodeTypes}
          edgeTypes={edgeTypes}
          onChange={this.queryBuilderViewModel.onChangedValue}
          onNodeDrag={this.queryBuilderViewModel.onNodeDrag}
          onNodeDragStop={this.queryBuilderViewModel.onNodeDragStop}
          onDragOver={this.queryBuilderViewModel.onDragOver}
          onDrop={this.queryBuilderViewModel.onDrop}
          onLoad={this.queryBuilderViewModel.onLoad}
          onConnect={this.queryBuilderViewModel.onConnect}
          onlyRenderVisibleElements={false}
        >
          <Controls showZoom={false} showInteractive={false} className={styles.controls}>
            <ControlButton
              className={styles.buttons}
              title={'Remove all elements'}
              onClick={() => this.queryBuilderViewModel.clearBuilder()}
            >
              <DeleteIcon />
            </ControlButton>
            <ControlButton
              className={styles.buttons}
              title={'Export querybuilder'}
              onClick={(event) => {
                event.stopPropagation();
                this.setState({ ...this.state, exportMenuAnchor: event.currentTarget });
              }}
            >
              <img src={ExportIcon} width={21}></img>
            </ControlButton>
            <ControlButton
              className={styles.buttons}
              title={'Other settings'}
              onClick={(event) => {
                event.stopPropagation();
                this.setState({
                  ...this.state,
                  settingsMenuAnchor: event.currentTarget,
                });
              }}
            >
              <SettingsIcon />
            </ControlButton>
          </Controls>
          <FunctionsMenu
            functionsMenuViewModel={this.functionsMenuViewModel}
            queryBuilderViewModel={this.queryBuilderViewModel}
            currentColours={currentColours}
          />
          <Background gap={10} size={0.7} />
        </ReactFlow>
        <Menu
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          anchorEl={this.state.exportMenuAnchor}
          keepMounted
          open={Boolean(this.state.exportMenuAnchor)}
          onClose={() => this.setState({ ...this.state, exportMenuAnchor: undefined })}
        >
          <MenuItem
            className={styles.menuText}
            onClick={() => this.queryBuilderViewModel.exportToPNG()}
          >
            {'Export to PNG'}
          </MenuItem>
          <MenuItem
            className={styles.menuText}
            onClick={() => this.queryBuilderViewModel.exportToPDF()}
          >
            {'Export to PDF'}
          </MenuItem>
        </Menu>
        <Menu
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          anchorEl={this.state.settingsMenuAnchor}
          keepMounted
          open={Boolean(this.state.settingsMenuAnchor)}
          onClose={() => this.setState({ ...this.state, settingsMenuAnchor: undefined })}
        >
          <MenuItem onClick={() => this.queryBuilderViewModel.sendQuery()}>
            <ListItemText primary={'Send query'} />
          </MenuItem>
          <MenuItem onClick={() => this.queryBuilderViewModel.toggleAutoSendQuery()}>
            <ListItemText
              primary={
                'Automatically send queries : ' +
                this.queryBuilderViewModel.autoSendQueries.toString()
              }
            />
          </MenuItem>
        </Menu>
      </div>
    );
  }
}
export default withStyles(useStyles)(QueryBuilderComponent);
