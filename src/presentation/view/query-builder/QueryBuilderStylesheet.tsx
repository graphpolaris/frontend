/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

/** CSS Styling for the Query Panel and the React Flow Elements */
import { Theme, createStyles } from '@material-ui/core/styles';
import { QueryBuilderThemeHolder } from '../../../domain/entity/css-themes/themeHolder';
const useStyles = (theme: Theme) =>
  createStyles({
    '@global': {
      '.react-flow__edges': {
        zIndex: '3',
      },
      '.react-flow__nodes': {},
      '.react-flow__pane': {},
      '.react-flow__edge-default .selected': {
        stroke: 'gray !important',
      },
    },
    //controls
    controls: {
      left: 'auto !important',
      bottom: 'auto !important',
      top: '10px',
      right: '20px',
      width: 'auto !important',
    },
    buttons: {
      left: 'auto !important',
      bottom: 'auto !important',
      top: '10px',
      right: '20px',
      '& svg': {
        transform: 'scale(1.4)',
      },
    },
    menuText: {
      fontSize: 'small',
      fontFamily: 'Poppins, sans-serif',
    },

    // General style
    handleFunction: {
      border: 0,
      borderRadius: '50%',
      left: 40,
      width: 6,
      height: 6,
      background: 'rgba(0, 0, 0, 0.3)',
      marginBottom: 16,
      '&::before': {
        content: '""',
        width: 4,
        height: 4,
        left: 1,
        bottom: 1,
        border: 0,
        borderRadius: '50%',
        background: 'rgba(255, 255, 255, 0.6)',
        zIndex: -1,
        display: 'inline-block',
        position: 'fixed',
      },
    },

    // Relation element
    relation: {
      height: 36,
      minWidth: `${QueryBuilderThemeHolder.relation.width}px`,
      display: 'flex',
      textAlign: 'center',
      color: QueryBuilderThemeHolder.relation.textColor,
      lineHeight: `${QueryBuilderThemeHolder.relation.height}px`,
      fontFamily: QueryBuilderThemeHolder.fontFamily,
      fontWeight: 'bold',
      fontSize: `${QueryBuilderThemeHolder.fontSize}px`,
    },

    relationWrapper: {
      display: 'inherit',
      alignItems: 'center',
      justifyContent: 'space-between',
    },

    relationNodeTriangleGeneral: {
      position: 'absolute',
      width: 0,
      height: 0,
      margin: 'auto 0',
      borderStyle: 'solid',
      borderColor: 'transparent',
    },

    relationNodeTriangleLeft: {
      transform: 'translateX(-100%)',
      top: '0px',
      borderWidth: '18px 24px 18px 0',
    },

    relationNodeSmallTriangleLeft: {
      transform: 'translateX(-100%)',
      top: '30px',
      borderWidth: '0 8px 6px 0',
    },

    relationNodeTriangleRight: {
      right: '-24px',
      top: '0px',
      borderWidth: '18px 0 18px 24px',
    },

    relationNodeSmallTriangleRight: {
      right: '-8px',
      top: '30px',
      borderWidth: '0 0 6px 8px',
    },

    relationHandleFiller: {},

    relationHandleLeft: {
      position: 'absolute',
      top: '50%',
      marginLeft: '15px',
      borderStyle: 'solid',
      borderWidth: '8px 12px 8px 0',
      borderRadius: '0px',
      left: 'unset',
      borderColor: `transparent rgba(0, 0, 0, 0.3) transparent transparent`,
      background: 'transparent',
      '&::before': {
        content: '""',
        borderStyle: 'solid',
        borderWidth: '6px 8px 6px 0',
        borderColor: `transparent rgba(255, 255, 255, 0.6) transparent transparent`,
        background: 'transparent',
        zIndex: -1,
        display: 'inline-block',
        position: 'absolute',
        top: '-0.5em',
        left: '0.25em',
      },
    },

    relationHandleRight: {
      position: 'absolute',
      marginRight: '19px',
      borderStyle: 'solid',
      borderWidth: '8px 0 8px 12px',
      borderRadius: '0px',
      left: 'unset',
      borderColor: `transparent transparent transparent rgba(0, 0, 0, 0.3)`,
      background: 'transparent',
      '&::before': {
        content: '""',
        borderStyle: 'solid',
        borderWidth: '6px 0 6px 8px',
        borderColor: `transparent transparent transparent rgba(255, 255, 255, 0.6)`,
        background: 'transparent',
        zIndex: -1,
        display: 'inline-block',
        position: 'absolute',
        top: '-0.5em',
        right: '0.25em',
      },
    },

    relationHandleBottom: {
      border: 0,
      borderRadius: 0,
      width: 8,
      height: 8,
      left: 'unset',
      marginBottom: 18,
      marginLeft: 40,
      background: 'rgba(0, 0, 0, 0.3)',
      transform: 'rotate(-45deg)',
      transformOrigin: 'center',
      margin: `${QueryBuilderThemeHolder.relation.handle.width}px`,
      '&::before': {
        content: '""',
        width: 6,
        height: 6,
        left: 1,
        bottom: 1,
        border: 0,
        borderRadius: 0,
        background: 'rgba(255, 255, 255, 0.6)',
        zIndex: -1,
        display: 'inline-block',
        position: 'fixed',
      },
    },

    relationDataWrapper: {
      marginLeft: 80,
    },

    relationSpan: {
      float: 'left',
      marginLeft: 5,
    },

    relationInputHolder: {
      display: 'flex',
      float: 'right',
      marginRight: '20px',
      marginTop: '4px',
      marginLeft: '5px',
      maxWidth: '80px',
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      borderRadius: '2px',
      alignItems: 'center',
      maxHeight: '12px',
    },

    relationInput: {
      zIndex: 1,
      cursor: 'text',
      minWidth: '0px',
      maxWidth: '1.5ch',
      border: 'none',
      background: 'transparent',
      textAlign: 'center',
      fontFamily: 'monospace',
      fontWeight: 'bold',
      fontSize: '11px',
      color: '#181520',
      userSelect: 'none',
      fontStyle: 'italic',
      '&:focus': {
        outline: 'none',
        userSelect: 'none',
      },
      '&::placeholder': {
        outline: 'none',
        userSelect: 'none',
        fontStyle: 'italic',
      },
    },

    relationReadonly: {
      cursor: 'grab !important',
      color: '#181520 !important',
      userSelect: 'none',
      fontStyle: 'normal !important',
    },

    relationHandleFunction: {
      marginLeft: 20,
      marginBottom: '18px !important',
    },

    // Entity element
    entity: {
      background: QueryBuilderThemeHolder.entity.baseColor,
      display: 'flex',
      borderRadius: '1px',
      fontFamily: QueryBuilderThemeHolder.fontFamily,
      fontWeight: 'bold',
      fontSize: `${QueryBuilderThemeHolder.fontSize}px`,
      minWidth: `${QueryBuilderThemeHolder.entity.width}px`,
      color: QueryBuilderThemeHolder.entity.textColor,

      paddingTop: 2,
      paddingRight: 5,
      paddingBottom: 12,
      paddingLeft: 5,
    },

    entityFade: {
      opacity: 1,
      animation: '$fade-in 600ms cubic-bezier(0.4, 0, 1, 1)',
      animationIterationCount: 1,
    },

    '@keyframes fade-in': {
      '0%': { opacity: 0 },
      '40%': { opacity: 0 },
      '100%': { opacity: 1 },
    },

    entityHandleLeft: {
      border: 0,
      borderRadius: 0,
      left: 12,
      width: 8,
      height: 8,
      marginBottom: 15,
      background: 'rgba(0, 0, 0, 0.3)',
      transformOrigin: 'center',
      '&::before': {
        content: '""',
        width: 6,
        height: 6,
        left: 1,
        bottom: 1,
        border: 0,
        borderRadius: 0,
        background: 'rgba(255, 255, 255, 0.6)',
        zIndex: -1,
        display: 'inline-block',
        position: 'fixed',
      },
    },

    entityHandleBottom: {
      border: 0,
      borderRadius: 0,
      width: 8,
      height: 8,
      left: 27.5,
      marginBottom: 15,
      background: 'rgba(0, 0, 0, 0.3)',
      transform: 'rotate(-45deg)',
      transformOrigin: 'center',
      '&::before': {
        content: '""',
        width: 6,
        height: 6,
        left: 1,
        bottom: 1,
        border: 0,
        borderRadius: 0,
        background: 'rgba(255, 255, 255, 0.6)',
        zIndex: -1,
        display: 'inline-block',
        position: 'fixed',
      },
    },

    handleFunctionEntity: {
      marginLeft: '5px',
    },

    entityWrapper: {
      height: 6,
      marginLeft: 55,
      marginRight: 5,
      color: QueryBuilderThemeHolder.entity.textColor,
    },

    entitySpan: {
      float: 'left',
    },

    // Attribute Element
    attributeMain: {
      background: '#bfb6af',
      color: '#FFF',
      borderRadius: '18px',
      fontFamily: 'monospace',
      fontWeight: 'bold',
      fontSize: '11px',
      paddingTop: '2px',
      paddingRight: '5px',
      paddingBottom: '12px',
      paddingLeft: '5px',
    },

    attributeHandleLeft: {
      border: 0,
      borderRadius: 5,
      left: 12,
      width: 8,
      height: 8,
      marginBottom: 8.5,
      background: 'rgba(0, 0, 0, 0.3)',
      transformOrigin: 'center',
      '&::before': {
        content: '""',
        width: 6,
        height: 6,
        left: 1,
        bottom: 1,
        border: 0,
        borderRadius: 5,
        background: 'rgba(255, 255, 255, 0.6)',
        zIndex: -1,
        display: 'inline-block',
        position: 'fixed',
      },
    },

    attributeInput: {
      float: 'right',
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      borderRadius: '2px',
      display: 'flex',
      '& input': {
        cursor: 'text',
        background: 'transparent',
        border: 'none',
        textAlign: 'right',
        fontFamily: 'monospace',
        fontWeight: 'bold',
        fontSize: '11px',
        color: 'black',
        userSelect: 'none',
        fontStyle: 'italic',
        '&:read-only': {
          cursor: 'grab',
          color: 'black',
          userSelect: 'none',
          fontStyle: 'normal',
        },
        '&:focus': {
          outline: 'none',
          userSelect: 'none',
        },
        '&::placeholder': {
          outline: 'none',
          userSelect: 'none',
          fontStyle: 'italic',
        },
      },
    },
    attributeWrapper: {
      height: 4,
      marginLeft: 15,
      marginRight: 5,
      color: 'black',
    },
    attributeWrapperSpan: {
      float: 'left',
      width: 60,
    },
    // Attribute select component
    matchTypeSelect: {
      float: 'left',
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      borderRadius: '2px',
      display: 'flex',
      marginRight: '10px',
      '& select': {
        background: 'transparent',
        border: 'none',
        appearance: 'none',

        fontFamily: 'monospace',
        fontSize: '11px',
        textAlign: 'center',
      },
      '& option': {
        fontFamily: 'monospace',
        fontSize: '11px',
      },
    },
    matchModifierTypeSelect: {
      float: 'left',
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      borderRadius: '2px',

      textAlign: 'center',
      '& select': {
        background: 'transparent',
        border: 'none',
        appearance: 'none',
        fontFamily: 'monospace',
        fontWeight: 'bolder',
        color: 'black',
        fontSize: 11,
      },
      '& option': {
        fontFamily: 'monospace',
        fontSize: '11px',
      },
    },
    disable: {
      opacity: '1!important',
      pointerEvents: 'none',
    },

    modifier: {
      color: '#181520',
      backgroundColor: '#d56a50',
      borderRadius: '5px',
      fontFamily: 'monospace',
      fontWeight: 'bolder',
      fontSize: 11,

      paddingTop: 2,
      paddingRight: 5,
      paddingBottom: 12,
      paddingLeft: 5,
    },
    modifierWrapper: {
      height: 6,
      marginLeft: 5,
      marginRight: 5,
      color: 'black',
    },
    modifierInput: {
      float: 'right',
      backgroundColor: '#ee917a',
      borderRadius: '2px',
      paddingLeft: '2px',
      paddingRight: '2px',
      display: 'flex',
    },
    modifierSpan: {
      float: 'left',
    },

    // Function element
    function: {
      backgroundColor: QueryBuilderThemeHolder.function.baseColor,
      display: 'flex',
      borderRadius: '1px',
      fontFamily: QueryBuilderThemeHolder.fontFamily,
      fontWeight: 'bold',
      fontSize: `${QueryBuilderThemeHolder.fontSize}px`,
      color: QueryBuilderThemeHolder.function.textColor,
      minWidth: '140px',
      textAlign: 'center',
      lineHeight: `${QueryBuilderThemeHolder.function.height}px`,
      paddingTop: 2,
      paddingRight: 5,
      paddingBottom: 4,
      paddingLeft: 5,

      '&::before': {
        position: 'absolute',
        content: '""',
        width: '100%',
        left: '0px',
        top: '0px',
        height: '100%',
        borderRadius: '3px',
        zIndex: -1,
        backgroundColor: QueryBuilderThemeHolder.function.baseColor,
        borderBottom: 'none',
      },

      '&::after': {
        position: 'absolute',
        content: '""',
        width: '100%',
        left: '0px',
        top: '0',
        height: '100%',
        borderRadius: '3px',
        zIndex: -1,
        backgroundColor: QueryBuilderThemeHolder.function.baseColor,
        borderTop: 'none',
      },
    },

    functionWrapper: {
      display: 'block',
      width: 'inherit',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    functionHandleFiller: {
      flex: '1 1 0',
      display: 'flow-root',
    },
    functionHandle: {
      border: 0,
      borderRadius: 0,
      width: 8,
      height: 8,
      left: 9,
      top: 7,
      background: 'rgba(0, 0, 0, 0.3)',
      transformOrigin: 'center',
      position: 'relative',
      float: 'right',
      marginRight: '20px',
      '&::before': {
        content: '""',
        width: 6,
        height: 6,
        left: 1,
        bottom: 1,
        border: 0,
        borderRadius: 0,
        background: 'rgba(255, 255, 255, 0.6)',
        zIndex: -1,
        display: 'inline-block',
        position: 'fixed',
      },
    },
    functionHandleBottom: {
      border: 0,
      borderRadius: 0,
      width: 8,
      height: 8,
      left: 27.5,
      marginBottom: 10,
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      transform: 'rotate(-45deg)',
      transformOrigin: 'center',
      '&::before': {
        content: '""',
        width: 6,
        height: 6,
        left: 1,
        bottom: 1,
        border: 0,
        borderRadius: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        zIndex: -1,
        display: 'inline-block',
        position: 'fixed',
      },
    },
    functionInputHolder: {
      display: 'flex',
      float: 'right',
      marginRight: '20px',
      marginTop: '4px',
      marginLeft: '5px',
      maxWidth: '80px',
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      borderRadius: '2px',
      alignItems: 'center',
      maxHeight: '12px',
    },
    functionInput: {
      zIndex: 1,
      cursor: 'text',
      minWidth: '0px',
      maxWidth: '1.5ch',
      height: '14px',
      border: 'none',
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      textAlign: 'center',
      fontFamily: 'monospace',
      fontWeight: 'bold',
      fontSize: '11px',
      color: '#181520',
      userSelect: 'none',
      fontStyle: 'italic',
      float: 'right',
      margin: '3px 0',
      marginRight: '10px',
      '&:focus': {
        outline: 'none',
        userSelect: 'none',
      },
      '&::placeholder': {
        outline: 'none',
        userSelect: 'none',
        fontStyle: 'italic',
      },
    },
    functionReadonly: {
      cursor: 'grab !important',
      color: '#181520 !important',
      userSelect: 'none',
      fontStyle: 'normal !important',
    },
    functionSpan: {
      float: 'left',
      marginLeft: 20,
      marginRight: 20,
    },
    functionSpanRight: {
      float: 'right',
      marginRight: 10,
    },

    functionDataWrapper: {},

    // This is used to override the previous color of the handle, for that to work it has to be on the bottom of the file
    handleConnectedFill: {
      '&::before': {
        background: '#181520',
      },
    },
    handleConnectedBorderRight: {
      '&::before': {
        borderColor: 'transparent transparent transparent #181520',
      },
    },
    handleConnectedBorderLeft: {
      '&::before': {
        borderColor: 'transparent #181520 transparent transparent',
      },
    },
  });
export { useStyles };
