/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState } from 'react';
import { FlowElement, Handle, Position } from 'react-flow-renderer';
import { QueryBuilderThemeHolder } from '../../../../../domain/entity/css-themes/themeHolder';
import {
  AttributeData,
  AttributeNode,
} from '../../../../../domain/entity/query-builder/structures/Nodes';
import { useStyles } from '../../QueryBuilderStylesheet';
import { makeStyles } from '@material-ui/core/styles';
import Select from './SelectComponent';
import { currentColours } from '../../../graph-schema/SchemaComponent';
import NodesHolder from '../../../../../domain/entity/query-builder/models/NodesHolder';
import { Handles } from '../../../../../domain/entity/query-builder/structures/Handles';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

/**  Component for rendering the attribute flow element */
export default function AttributeFlowElement(
  attributeNode: AttributeNode,
  nodesHolder: NodesHolder,
) {
  const styles = madeStyles();
  const [read, setRead] = useState(true);
  const data = attributeNode.data;

  /**
   * Check if the pressed key is enter in order to send the new query.
   * @param event Key press event.
   */
  const _onKeyDown = (event: any): void => {
    if (event.key == 'Enter') setRead(true);
  };

  /**
   * Checks if the string input is a number.
   * @param x String input.
   * @returns {boolean} True if input is a number.
   */
  const isNumber = (x: string): boolean => {
    {
      if (typeof x != 'string') return false;
      return !Number.isNaN(x) && !Number.isNaN(parseFloat(x));
    }
  };

  /**
   * Calculates the width of an element based on the length of a monospaced font.
   * @param str Input string.
   * @returns {string} Containing the length in css format.
   */
  const calcWidth = (str: string) => {
    if (str == '') {
      return 1.5 + 'ch';
    }
    return str.length + 0.5 + 'ch';
  };

  /**
   * Input contraint checker for the attribute input fields.
   * @param type Data.dataType.
   * @param str Input string.
   * @returns {string} Result string after the contraints are applied.
   */
  const inputConstraint = (type: string, str: string): string => {
    let res = '';
    switch (type) {
      case 'string':
        res = str;
        break;
      case 'bool':
        res = str;
        break; // TODO: only false and true live update will break since it will not allow to write more that 1 letter
      case 'int':
        isNumber(str) ? (res = str) : (res = '');
        break; // TODO: check if letters after number
      default:
        res = str;
        break;
    }
    return res;
  };

  //TODO: docstrings
  const className = styles.attributeHandleLeft + ' ' + (false ? styles.handleConnectedFill : '');

  const onChange = (e: any) => {
    if (data != undefined) {
      data.value = inputConstraint(data.dataType, e.target.value);
      e.target.style.maxWidth = calcWidth(data.value);
    }
  };

  /**Constraint datatypes back end.
   * string		  MatchTypes: EQ/NEQ/contains/excludes.
   * int   MatchTypes: EQ/NEQ/GT/LT/GET/LET.
   * bool     MatchTypes: EQ/NEQ.
   */
  //TODO: fix use of relation boilerplate styling

  return (
    <div
      className={styles.attributeMain}
      style={{ backgroundColor: '#' + currentColours.elements.attribute[0] }}
    >
      <Handle
        id={Handles.OnAttribute}
        type="source"
        position={Position.Bottom}
        className={styles.attributeHandleLeft + ' ' + (false ? styles.handleConnectedFill : '')}
        style={{ backgroundColor: '#' + currentColours.elements.attribute[1] }}
      />
      <div className={styles.attributeWrapper}>
        <span className={styles.attributeWrapperSpan}>{data?.attribute}</span>
        <Select data={data} />
        <span className={styles.attributeInput}>
          <input type="hidden"></input>
          <input
            style={{ maxWidth: calcWidth(data?.value || '') }}
            type="string"
            readOnly={read}
            placeholder={'?'}
            value={data?.value || ''}
            onChange={onChange}
            onDoubleClick={() => {
              setRead(false);
            }}
            onBlur={() => {
              setRead(true);
            }}
            onKeyDown={_onKeyDown}
          ></input>
        </span>
      </div>
    </div>
  );
}
