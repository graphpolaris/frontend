/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStyles } from '../../QueryBuilderStylesheet';
import { currentColours } from '../../../graph-schema/SchemaComponent';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

export default function SelectComponent({ data }: any) {
  const styles = madeStyles();
  const [disable, setDisable] = useState(true);
  const [disClass, setDisClass] = useState(styles.disable);

  /**
   * Calculate the width of the select element based on the displayed value.
   * @param str Input string.
   * @returns {string} Containing the length in css format.
   */
  const calcSelectWidth = (str: string): string => {
    if (str == '') {
      return 1.5 + 'ch';
    }
    return str.length + 1.5 + 'ch';
  };

  /** Disable the select field */
  const disableSelect = (): void => {
    setDisable(true);
    setDisClass(styles.disable);
  };

  /** Enable the select field */
  const enableSelect = (): void => {
    setDisable(false);
    setDisClass('');
  };

  /**
   * Constant switch to append the right options for the select element based on the data.dataType.
   * @returns {JSX.Element} Option list using React.Fragment as parent element.
   */
  const list = (): JSX.Element => {
    switch (data.dataType) {
      case 'string':
        return (
          <React.Fragment>
            <option value="EQ">==</option>
            <option value="NEQ">!=</option>
            <option value="contains">contains</option>
            <option value="excludes">excludes</option>
          </React.Fragment>
        );
      case 'int':
      case 'float':
        return (
          <React.Fragment>
            <option value="EQ">==</option>
            <option value="NEQ">!=</option>
            <option value="GT">{'>'}</option>
            <option value="LT">{'<'}</option>
            <option value="GET">{'>='}</option>
            <option value="LET">{'<='}</option>
          </React.Fragment>
        );
      case 'bool':
        return (
          <React.Fragment>
            <option value="EQ">==</option>
            <option value="NEQ">!=</option>
          </React.Fragment>
        );
      default:
        return <option>Error</option>;
    }
  };

  return (
    <div
      className={styles.matchTypeSelect}
      onDoubleClick={enableSelect}
      onBlur={disableSelect}
      style={{ backgroundColor: '#' + currentColours.elements.attribute[1] }}
    >
      <select
        style={{ maxWidth: calcSelectWidth('==') }}
        name="operators"
        className={disClass}
        disabled={disable}
        onChange={(e) => {
          data.matchType = e.target.value;
          e.target.style.maxWidth = calcSelectWidth(e.target.value);
        }}
      >
        {list()}
      </select>
    </div>
  );
}
