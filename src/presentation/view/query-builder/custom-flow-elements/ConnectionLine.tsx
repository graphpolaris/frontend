/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import { ConnectionLineComponentProps } from 'react-flow-renderer';

/**
 * A custom query element to render the line when connecting flow elements.
 * @param {ConnectionLineComponentProps} param0 Source and target coordinates of the edges.
 */
export default function ConnectionLine({
  sourceX,
  sourceY,
  targetX,
  targetY,
}: ConnectionLineComponentProps) {
  return (
    <g>
      <path
        fill="none"
        stroke="#222"
        strokeWidth={2.5}
        className="animated"
        d={`M${sourceX},${sourceY}L ${targetX},${targetY}`}
      />
      <circle cx={sourceX} cy={sourceY} fill="#fff" r={3} stroke="#222" strokeWidth={1.5} />
      <circle cx={targetX} cy={targetY} fill="#fff" r={3} stroke="#222" strokeWidth={1.5} />
    </g>
  );
}
