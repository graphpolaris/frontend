/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import { EdgeProps, getSmoothStepPath, Position } from 'react-flow-renderer';
import { Handles } from '../../../../domain/entity/query-builder/structures/Handles';

/**
 * A custom query element edge line component.
 * @param {EdgeProps} param0 The coordinates for the start and end point, the id and the style.
 */
export default function EdgeLine({
  id,
  sourceX,
  sourceY,
  targetX,
  targetY,
  style,
  sourceHandleId,
  targetHandleId,
}: EdgeProps) {
  //Centering the line
  sourceY -= 3;
  targetY -= 3;

  // Correct line positions with hardcoded numbers, because react flow lacks this functionality
  if (sourceHandleId == Handles.ToAttributeHandle) sourceX += 2;

  if (targetHandleId == Handles.ToAttributeHandle) targetX += 2;

  let spos: Position = Position.Bottom;
  if (sourceHandleId == Handles.RelationLeft) {
    spos = Position.Left;
    sourceX += 7;
    sourceY += 3;
  } else if (sourceHandleId == Handles.RelationRight) {
    spos = Position.Right;
    sourceX -= 2;
    sourceY -= 3;
  } else if (
    sourceHandleId !== undefined &&
    sourceHandleId !== null &&
    sourceHandleId.includes('functionHandle')
  ) {
    spos = Position.Top;
    sourceX -= 4;
    sourceY += 3;
  }

  let tpos: Position = Position.Bottom;
  if (targetHandleId == Handles.RelationLeft) {
    tpos = Position.Left;
    targetX += 7;
    targetY += 3;
  } else if (targetHandleId == Handles.RelationRight) {
    tpos = Position.Right;
    targetX -= 2;
    targetY -= 3;
  }

  // Create smoothstep line
  const path = getSmoothStepPath({
    sourceX: sourceX,
    sourceY: sourceY,
    sourcePosition: spos,
    targetX: targetX,
    targetY: targetY,
    targetPosition: tpos,
  });

  return (
    <g stroke="black">
      <path id={id} fill="none" strokeWidth={5} style={style} d={path} />
    </g>
  );
}
