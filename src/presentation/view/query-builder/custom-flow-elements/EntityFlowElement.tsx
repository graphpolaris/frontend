/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useEffect } from 'react';
import { FlowElement, Handle, Position } from 'react-flow-renderer';
import { makeStyles } from '@material-ui/core/styles';
import { EntityData, EntityNode } from '../../../../domain/entity/query-builder/structures/Nodes';
import { useStyles } from '../QueryBuilderStylesheet';
import { currentColours } from '../../graph-schema/SchemaComponent';
import NodesHolder from '../../../../domain/entity/query-builder/models/NodesHolder';
import { Handles } from '../../../../domain/entity/query-builder/structures/Handles';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

/**
 * Component to render an entity flow element
 * @param {FlowElement<EntityData>)} param0 The data of an entity flow element.
 */
export default function EntityFlowElement(entityNode: EntityNode, nodesHolder: NodesHolder) {
  const styles = madeStyles();

  // TODO: Change flow element width when text overflows
  const data = entityNode.data as EntityData;
  const animation = data.fadeIn ? styles.entityFade : '';

  return (
    <div
      className={`${styles.entity} ${animation} query_builder-entity`}
      style={{
        backgroundColor: '#' + currentColours.nodesBase[0],
        borderTop: `4px solid ${'#' + currentColours.nodesBase[0]}`,
        borderBottom: `6px solid ${'#' + currentColours.elements.entityBase[0]}`,
      }}
    >
      <Handle
        id={Handles.ToRelation}
        type="source"
        position={Position.Bottom}
        className={styles.entityHandleLeft + ' ' + (false ? styles.handleConnectedFill : '')}
      />
      <Handle
        id={Handles.ToAttributeHandle}
        type="target"
        position={Position.Bottom}
        className={styles.entityHandleBottom + ' ' + (false ? styles.handleConnectedFill : '')}
      />
      <Handle
        id={Handles.ReceiveFunction}
        type="target"
        position={Position.Bottom}
        className={
          styles.handleFunction +
          ' ' +
          styles.handleFunctionEntity +
          ' ' +
          (false ? styles.handleConnectedFill : '')
        }
      />
      <div className={styles.entityWrapper}>
        <span className={styles.entitySpan}>{data.name}</span>
      </div>
    </div>
  );
}
