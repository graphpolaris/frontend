/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState } from 'react';
import { Handle, Position } from 'react-flow-renderer';
import { makeStyles } from '@material-ui/core/styles';
import {
  FunctionData,
  FunctionNode,
} from '../../../../../domain/entity/query-builder/structures/Nodes';
import { useStyles } from '../../QueryBuilderStylesheet';
import NodesHolder from '../../../../../domain/entity/query-builder/models/NodesHolder';
import { capitalizeFirstLetter } from '../../../../util/graph-schema/utils';
import { Handles } from '../../../../../domain/entity/query-builder/structures/Handles';
import { currentColours } from '../../../graph-schema/SchemaComponent';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

const countArgs = (data: FunctionData | undefined) => {
  if (data !== undefined) {
    let count = 0;

    for (const name in data.args) {
      if (data.args[name].visible) {
        count++;
      }
    }
    return count;
  }
  return 1;
};

/**
 * Component to render a relation flow element
 * @param { FlowElement<FunctionData>} param0 The data of a relation flow element.
 */
export default function RelationFlowElement(functionNode: FunctionNode, nodesHolder: NodesHolder) {
  const styles = madeStyles();
  const [read, setRead] = useState(true);
  const data = functionNode.data;

  const numOfArgs = countArgs(data);
  const height = numOfArgs * 20;

  const _onKeyDown = (event: any): void => {
    if (event.key == 'Enter') setRead(true);
  };

  const getArgs = (styles: any, data: FunctionData | undefined, setRead: any) => {
    let rows: JSX.Element[] = [];

    if (data != undefined) {
      let index = 0;

      for (const name in data.args) {
        const item = data.args[name];
        if (item.visible) {
          rows.push(
            <span className={styles.functionHandleFiller}>
              <span className={styles.functionSpan}>{capitalizeFirstLetter(name)}</span>
              <Handle
                id={Handles.FunctionBase + name}
                type="source"
                position={Position.Top}
                className={styles.functionHandle + ' ' + (false ? styles.handleConnectedFill : '')}
                style={{
                  visibility: item.connectable ? 'inherit' : 'hidden',
                }}
              />
              {item.value !== undefined && (
                <input
                  className={styles.functionInput}
                  style={{ maxWidth: 50 }}
                  type="string"
                  placeholder={'?'}
                  value={item.value}
                  onChange={(e) => {
                    if (item.value != undefined) {
                      item.value = e.target.value;
                      //TODO restore SetElementsUseCase.updateFunctionCompleteness(data);
                    }
                  }}
                  onDoubleClick={() => {
                    setRead(false);
                  }}
                  onBlur={() => {
                    setRead(true);
                  }}
                  onKeyDown={_onKeyDown}
                ></input>
              )}
            </span>,
          );
          index++;
        }
      }
    }

    return rows;
  };

  const rows = getArgs(styles, data, setRead);
  const entity = undefined; //TODO fix: data !== undefined ? data.entityName : undefined;

  return (
    <div>
      <div
        className={styles.function}
        style={{
          minHeight: height,
          background: '#' + currentColours.nodesBase[0],
          borderTop: `4px solid ${'#' + currentColours.nodesBase[0]}`,
          borderBottom: `6px solid ${'#' + currentColours.elements.function[0]}`,
        }}
      >
        <div className={styles.functionWrapper}>{rows}</div>
      </div>
      <div className={`${styles.entity} entityWrapper ${entity === undefined ? 'hidden' : ''}`}>
        <Handle
          id={Handles.ToRelation}
          type="source"
          position={Position.Bottom}
          className={styles.entityHandleLeft + ' ' + (false ? styles.handleConnectedFill : '')}
        />
        <Handle
          id={Handles.ToAttributeHandle}
          type="source"
          position={Position.Bottom}
          className={styles.entityHandleBottom + ' ' + (false ? styles.handleConnectedFill : '')}
        />
        <div className={styles.entityWrapper}>
          <span className={styles.entitySpan}>{entity ? entity : ''}</span>
        </div>
      </div>
    </div>
  );
}
