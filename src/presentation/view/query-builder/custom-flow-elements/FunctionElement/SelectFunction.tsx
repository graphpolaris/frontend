/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStyles } from '../../QueryBuilderStylesheet';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

/**
 * The flow element for the modifier.
 * @param param0 The data of the modifier flow element.
 */
export default function ModifierFlowElement({ data }: any) {
  const styles = madeStyles();
  const [disable, setDisable] = useState(true);
  const [disClass, setDisClass] = useState(styles.disable);

  /**
   * Calculate the width of the select element based on the displayed value.
   * @param str Input string.
   * @returns String containg the length in css format.
   */
  const calcSelectWidth = (str: string): string => {
    if (str == '') return 1.5 + 'ch';
    return str.length + 1.5 + 'ch';
  };

  /** Disable the select field */
  const disableSelect = (): void => {
    setDisable(true);
    setDisClass(styles.disable);
  };

  /** Enable the select field */
  const enableSelect = (): void => {
    setDisable(false);
    setDisClass('');
  };

  /**
   * Constant switch to append the right options for the select element based on the data.type.
   * @returns {JSX.Element} Option list using React.Fragment as parent element.
   */
  const list = (): JSX.Element => {
    return (
      <React.Fragment>
        <option color="black" value="COUNT">
          COUNT
        </option>
        <option value="SUM">SUM</option>
        <option value="MIN">MIN</option>
        <option value="MAX">MAX</option>
      </React.Fragment>
    );
  };

  return (
    <div
      className={styles.matchModifierTypeSelect}
      onBlur={disableSelect}
      onDoubleClick={enableSelect}
    >
      <select
        style={{
          color: disable ? 'black' : 'black',
          maxWidth: calcSelectWidth('COUNT'),
        }}
        name="operators"
        className={disClass}
        disabled={disable}
        onChange={(e) => {
          data.type = e.target.value;
          e.target.style.maxWidth = calcSelectWidth(e.target.value);
        }}
      >
        {list()}
      </select>
    </div>
  );
}
