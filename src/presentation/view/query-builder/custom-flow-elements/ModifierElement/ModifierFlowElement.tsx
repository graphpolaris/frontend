/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import { FlowElement, Handle, Position } from 'react-flow-renderer';
import { makeStyles } from '@material-ui/core/styles';
import { useStyles } from '../../QueryBuilderStylesheet';
import Select from './SelectModifier';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

/**
 * Component to render an entity flow element
 * @param param0 Data of the flow element.
 */
export default function ModifierFlowElement({ data }: FlowElement) {
  const styles = madeStyles();

  return (
    <div className={styles.modifier}>
      <div className={styles.modifierWrapper}>
        <span className={styles.modifierInput}>
          <span className={styles.modifierSpan}>
            <Select data={data} />
          </span>
        </span>
      </div>
    </div>
  );
}
