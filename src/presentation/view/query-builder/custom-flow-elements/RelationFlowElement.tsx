/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useRef, useState } from 'react';
import { FlowElement, Handle, Position } from 'react-flow-renderer';
import { setSyntheticLeadingComments } from 'typescript';
import { makeStyles } from '@material-ui/core/styles';
import {
  RelationData,
  RelationNode,
} from '../../../../domain/entity/query-builder/structures/Nodes';
import { useStyles } from '../QueryBuilderStylesheet';
import { currentColours } from '../../graph-schema/SchemaComponent';
import { Handles } from '../../../../domain/entity/query-builder/structures/Handles';
import NodesHolder from '../../../../domain/entity/query-builder/models/NodesHolder';

// Create style constant to prevent rereaction of styles
const madeStyles = makeStyles(useStyles);

/**
 * Component to render a relation flow element
 * @param { FlowElement<RelationData>} param0 The data of a relation flow element.
 */
export default function RelationFlowElement(relationNode: RelationNode, nodesHolder: NodesHolder) {
  const styles = madeStyles();
  const minRef = useRef<HTMLInputElement>(null);
  const maxRef = useRef<HTMLInputElement>(null);
  const data = relationNode.data;

  const [readOnlyMin, setReadOnlyMin] = useState(true);
  const [readOnlyMax, setReadOnlyMax] = useState(true);

  const onDepthChanged = (depth: string) => {
    // Don't allow depth above 99
    const limit = 99;
    if (data != undefined) {
      data.depth.min = data.depth.min >= limit ? limit : data.depth.min;
      data.depth.max = data.depth.max >= limit ? limit : data.depth.max;

      // Check for for valid depth: min <= max
      if (depth == 'min') {
        if (data.depth.min > data.depth.max) data.depth.max = data.depth.min;
        setReadOnlyMin(true);
      } else if (depth == 'max') {
        if (data.depth.max < data.depth.min) data.depth.min = data.depth.max;
        setReadOnlyMax(true);
      }

      // Set to the correct width
      if (maxRef.current) maxRef.current.style.maxWidth = calcWidth(data.depth.max);
      if (minRef.current) minRef.current.style.maxWidth = calcWidth(data.depth.min);
    }
  };

  const isNumber = (x: string) => {
    {
      if (typeof x != 'string') return false;
      return !Number.isNaN(x) && !Number.isNaN(parseFloat(x));
    }
  };

  const calcWidth = (data: number) => {
    return data.toString().length + 0.5 + 'ch';
  };

  return (
    <div
      className={styles.relation}
      style={{
        background: '#' + currentColours.nodesBase[0],
        borderTop: `4px solid ${'#' + currentColours.nodesBase[0]}`,
        borderBottom: `6px solid ${'#' + currentColours.elements.relationBase[0]}`,
      }}
    >
      <div className={styles.relationWrapper}>
        <div
          className={[styles.relationNodeTriangleGeneral, styles.relationNodeTriangleLeft].join(
            ' ',
          )}
          style={{ borderRightColor: '#' + currentColours.nodesBase[0] }}
        >
          <span className={styles.relationHandleFiller}>
            <Handle
              id={Handles.RelationLeft}
              type="target"
              position={Position.Left}
              className={
                styles.relationHandleLeft + ' ' + (false ? styles.handleConnectedBorderLeft : '')
              }
            />
          </span>
        </div>
        <div
          className={[
            styles.relationNodeTriangleGeneral,
            styles.relationNodeSmallTriangleLeft,
          ].join(' ')}
          style={{ borderRightColor: '#' + currentColours.elements.relationBase[0] }}
        ></div>
        <div
          className={[styles.relationNodeTriangleGeneral, styles.relationNodeTriangleRight].join(
            ' ',
          )}
          style={{ borderLeftColor: '#' + currentColours.nodesBase[0] }}
        >
          <span className={styles.relationHandleFiller}>
            <Handle
              id={Handles.RelationRight}
              type="target"
              position={Position.Right}
              className={
                styles.relationHandleRight + ' ' + (false ? styles.handleConnectedBorderRight : '')
              }
            />
          </span>
        </div>
        <div
          className={[
            styles.relationNodeTriangleGeneral,
            styles.relationNodeSmallTriangleRight,
          ].join(' ')}
          style={{ borderLeftColor: '#' + currentColours.elements.relationBase[0] }}
        ></div>

        <span className={styles.relationHandleFiller}>
          <Handle
            id={Handles.ToAttributeHandle}
            type="target"
            position={Position.Bottom}
            className={
              styles.relationHandleBottom + ' ' + (false ? styles.handleConnectedFill : '')
            }
          />
        </span>
        <div className={styles.relationDataWrapper}>
          <span className={styles.relationSpan}>{data?.name}</span>
          <span className={styles.relationInputHolder}>
            <span>[</span>
            <input
              className={styles.relationInput + ' ' + (readOnlyMin ? styles.relationReadonly : '')}
              ref={minRef}
              type="string"
              min={0}
              readOnly={readOnlyMin}
              placeholder={'?'}
              value={data?.depth.min}
              onChange={(e) => {
                if (data != undefined) {
                  data.depth.min = isNumber(e.target.value) ? parseInt(e.target.value) : 0;
                  e.target.style.maxWidth = calcWidth(data.depth.min);
                }
              }}
              onDoubleClick={() => {
                setReadOnlyMin(false);
              }}
              onBlur={(e) => {
                onDepthChanged('min');
              }}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  onDepthChanged('min');
                }
              }}
            ></input>
            <span>..</span>
            <input
              className={styles.relationInput + ' ' + (readOnlyMax ? styles.relationReadonly : '')}
              ref={maxRef}
              type="string"
              min={0}
              readOnly={readOnlyMax}
              placeholder={'?'}
              value={data?.depth.max}
              onChange={(e) => {
                if (data != undefined) {
                  data.depth.max = isNumber(e.target.value) ? parseInt(e.target.value) : 0;
                  e.target.style.maxWidth = calcWidth(data.depth.max);
                }
              }}
              onDoubleClick={() => {
                setReadOnlyMax(false);
              }}
              onBlur={(e) => {
                onDepthChanged('max');
              }}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  onDepthChanged('max');
                }
              }}
            ></input>
            <span>]</span>
          </span>
        </div>
        <Handle
          id={Handles.ReceiveFunction}
          type="target"
          position={Position.Bottom}
          className={
            styles.relationHandleFunction +
            ' ' +
            styles.handleFunction +
            ' ' +
            (false ? styles.handleConnectedFill : '')
          }
        />
      </div>
    </div>
  );
}
