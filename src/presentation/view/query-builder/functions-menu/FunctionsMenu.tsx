/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState, useEffect, useRef } from 'react';

import BaseView from '../../BaseView';
import FunctionsMenuViewModel from '../../../view-model/query-builder/functions-menu/FunctionsMenuViewModel';
import QueryBuilderViewModel from '../../../view-model/query-builder/QueryBuilderViewModel';
import './FunctionsMenu.scss';
import clsx from 'clsx';
import { LocalStorage } from '../../../../data/drivers/LocalStorage';
import { FunctionTypes } from '../../../../domain/entity/query-builder/structures/FunctionTypes';

/** Interface for the props */
export interface FunctionsMenuComponentProps {
  functionsMenuViewModel: FunctionsMenuViewModel;
  queryBuilderViewModel: QueryBuilderViewModel;
  currentColours: any;
}

/** The width of the .functionsMenuWrapper div of the function menu.
 *  This width is based off the width of the .functionsMenuContent div.
 *  This is so that the show/hide animation only slides the buttons as far as this width.
 */
let buttonsWidth = 500;
let wrapperWidth = 1000;

/** A React component for rendering the function menu. Funcionality is handled by the given `FunctionsMenuViewModel` */
export default function FunctionsMenu(props: FunctionsMenuComponentProps) {
  const { functionsMenuViewModel, queryBuilderViewModel } = props;

  const [infoEnabled, setInfoEnabled] = useState(false);
  const [infoVisible, setInfoVisible] = useState(false);
  const [infoText, setInfoText] = useState('');
  const [position, setPosition] = useState(0);

  /** keep track of the element widths of specific div elements */
  let functionsRef = useRef<HTMLDivElement>(null);
  let wrapperRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    // update the detected width of the functions container
    buttonsWidth = functionsRef.current ? functionsRef.current.offsetWidth + 45 : 500;
    // update the detected width of the wrapper
    wrapperWidth = wrapperRef.current ? wrapperRef.current.offsetWidth : 1000;
  });

  /** save, whether the functions menu is shown or now, in cache */
  const [isFunctionMenuOpen, setIsFunctionMenuOpen] = useState(
    LocalStorage.instance().cache.functionsMenuOpen,
  );

  /** On menu toggle: update status and save it to cache */
  const changeFunctionMenuIsOpen = () => {
    setIsFunctionMenuOpen(!isFunctionMenuOpen);
    LocalStorage.instance().cache.functionsMenuOpen = isFunctionMenuOpen;
    LocalStorage.instance().SaveToCache();
  };

  /** Show information about the function that is being hovered over
   *  @param functionType function type of which its description should be used
   */
  const showFunctionInfo = (functionType: string) => {
    // update text
    setInfoText(functionsMenuViewModel.getFunctionInfo(functionType));
    // change display to 'block' before the element fades in
    setInfoEnabled(true);
    // change opacity to 1
    setTimeout(function () {
      setInfoVisible(true);
    }, 1);
  };

  /** On hover end: Hide function information */
  const hideFunctionInfo = () => {
    // change opacity to 0
    setInfoVisible(false);
    // change display to 'none' after the element has faded out
    setTimeout(function () {
      setInfoEnabled(false);
    }, 100);
  };

  /** Returns whether the slider can be moved further to the right */
  const canMoveRight = () => {
    return position > wrapperWidth - buttonsWidth;
  };

  /** On click: move slider to the left if possible */
  const moveSliderLeft = () => {
    let newPos = Math.min(0, position + 200);
    setPosition(newPos);
  };

  /** On click: move slider to the right if possible */
  const moveSliderRight = () => {
    let newPos = Math.max(wrapperWidth - buttonsWidth, position - 200);
    if (canMoveRight()) setPosition(newPos);
  };

  const functions = functionsMenuViewModel.getFunctionKeys(); // currently hard-coded in Functions.tsx
  /** Creates a button element for each type of function */
  const funcButtons = () => {
    let rows: JSX.Element[] = [];

    functions.forEach((func, placeInList) => {
      rows.push(
        <button
          onClick={() =>
            queryBuilderViewModel.addFunction(functionsMenuViewModel.functionTypes[func].type)
          }
          className="button functionsMenuButton"
          id={'functionsMenuButton-' + func + placeInList}
          key={'uniqueKeyFunctionMenuButton' + placeInList}
        >
          {functionsMenuViewModel.functionTypes[func].name}
          <span
            className="functionsMenuButtonInfo"
            onMouseEnter={() => showFunctionInfo(func)}
            onMouseLeave={() => hideFunctionInfo()}
          >
            i
          </span>
        </button>,
      );
    });

    return rows;
  };

  // render the functions menu
  return (
    <div className="functionsMenu">
      <button
        onClick={changeFunctionMenuIsOpen}
        className="button functionsMenuButton functionsMenuFoldButton"
      >
        {isFunctionMenuOpen ? ' 𝑓 ◀' : ' 𝑓 ▶'}
      </button>
      <div className="functionsMenuWrapper" ref={wrapperRef}>
        <div
          className={clsx('functionsMenuContent', {
            ['visible']: isFunctionMenuOpen,
          })}
        >
          <div className="functionsMenuSlider">
            <div className="functionsMenuFunctions" style={{ left: position }} ref={functionsRef}>
              {funcButtons()}
            </div>
          </div>
          <div className="functionsMenuCarousel">
            <button
              onClick={moveSliderLeft}
              className={clsx('functionsMenuCarouselButton button', {
                ['disabled']: position >= 0,
              })}
            >
              <span>◀</span>
            </button>
            <button
              onClick={moveSliderRight}
              className={clsx('functionsMenuCarouselButton button', {
                ['disabled']: !canMoveRight(),
              })}
            >
              <span>▶</span>
            </button>
          </div>
        </div>
      </div>
      <div
        className={clsx('functionsMenuInfo', {
          ['visible']: infoVisible,
          ['enabled']: infoEnabled,
        })}
      >
        {infoText}
      </div>
    </div>
  );
}
