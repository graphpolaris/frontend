/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState } from 'react';
import QueryBuilderViewModel from '../../view-model/query-builder/QueryBuilderViewModel';
import QueryStatusListViewModel from '../../view-model/query-status-list/QueryStatusListViewModel';
import QueryBuilderComponent from '../query-builder/QueryBuilderComponent';
import SentQueryStatusListComponent from '../query-status-list/QueryStatusListComponent';
import FunctionsMenuViewModel from '../../view-model/query-builder/functions-menu/FunctionsMenuViewModel';
import { useStyles } from './QueryPanelStylesheet';
import clsx from 'clsx';
import { LocalStorage } from '../../../data/drivers/LocalStorage';
import { Background } from 'react-flow-renderer';

/** Interface for the QueryPanelComponent props. */
type QueryPanelProps = {
  queryBuilderViewModel: QueryBuilderViewModel;
  functionsMenuViewModel: FunctionsMenuViewModel;
  queryStatusListViewModel: QueryStatusListViewModel;
  currentColours: any;
};

/** React component, renders containers for the query builder and the query status list. Takes care of opening the query list panel logic. */
export default function QueryPanelComponent(props: QueryPanelProps) {
  const classes = useStyles();
  const [isQueryListOpen, setIsQueryListOpen] = useState(
    LocalStorage.instance().cache.queryListOpen,
  );

  const changeIsQueryListOpen = (): void => {
    LocalStorage.instance().cache.queryListOpen = !isQueryListOpen;
    LocalStorage.instance().SaveToCache();
    setIsQueryListOpen(!isQueryListOpen);
  };

  return (
    <div className={classes.root}>
      <div
        className={clsx(classes.queryBuilderContainer, {
          [classes.queryBuilderContainerSmall]: isQueryListOpen,
        })}
        style={{
          backgroundColor: '#' + props.currentColours.visBackground,
        }}
      >
        <QueryBuilderComponent
          queryBuilderViewModel={props.queryBuilderViewModel}
          functionsMenuViewModel={props.functionsMenuViewModel}
          currentColours={props.currentColours}
        />
      </div>
      <div
        className={classes.verticalSideBarContainer}
        style={{ backgroundColor: '#' + props.currentColours.background }}
      >
        <div
          onClick={changeIsQueryListOpen}
          className={classes.queriesListButton}
          style={{ backgroundColor: '#' + props.currentColours.background }}
        >
          <h4
            style={{
              backgroundColor: '#' + props.currentColours.dragger,
              color: '#' + props.currentColours.menuText,
            }}
          >
            {isQueryListOpen ? 'Queries ▼' : 'Queries ▲'}
          </h4>
        </div>
      </div>
      <div
        className={clsx(classes.queryListContainer, {
          [classes.queryListContainerOpen]: isQueryListOpen,
        })}
      >
        <SentQueryStatusListComponent
          queryStatusListViewModel={props.queryStatusListViewModel}
          currentColours={props.currentColours}
        />
      </div>
    </div>
  );
}
