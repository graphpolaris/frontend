/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) => {
  const queryListWidth = 240;
  const verticalSideBarWidth = '1.4rem';

  return createStyles({
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      overflow: 'hidden',
    },

    queryBuilderContainer: {
      transition: 'width .3s',
      width: `calc(100% - ${verticalSideBarWidth})`,
      height: '100%',
    },
    queryBuilderContainerSmall: {
      width: `calc(100% - ${verticalSideBarWidth} - ${queryListWidth}px)`,
    },

    verticalSideBarContainer: {
      width: verticalSideBarWidth,
      height: '100%',
      marginTop: 5,
      backgroundColor: '#f2f2f2',
    },
    queriesListButton: {
      height: verticalSideBarWidth,
      position: 'absolute',
      backgroundColor: '#dfdfdf',
      borderRadius: '5px 0px 0px 0px',
      cursor: 'pointer',
      transform: 'rotate(-90deg) translate(-100%,0)',
      transformOrigin: 'top left',
      transition: 'background-color .1s',

      '&:hover': {
        backgroundColor: 'lightgrey',
      },

      '& h4': {
        padding: '0px 8px',
        textAlign: 'center',
        margin: '0',
        width: 100,
      },
    },

    queryListContainer: {
      transition: 'width .3s',
      position: 'relative',
      width: 0,
      height: '100%',
    },
    queryListContainerOpen: {
      width: queryListWidth,
    },
  });
});
export { useStyles };
