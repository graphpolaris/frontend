/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import React, { ReactElement } from 'react';
import { QueryStatusListItem } from '../../view-model/query-status-list/QueryStatusListViewModelImpl';
import CachedIcon from '@material-ui/icons/Cached';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import clsx from 'clsx';
import { TranslatedJSONQuery } from '../../../domain/entity/query-builder/structures/TranslatedJSONQuery';

/**
 * Retrieves the correct classname for a query item, depending on the status.
 * @param status {string} The status for the query item.
 * @param classes {ClassNameMap} The material-ui classes.
 * @returns {string} The classname for this query status item.
 */
function getClassNameForStatus(status: string, classes: ClassNameMap): string {
  switch (status) {
    case 'Query sent':
      return classes.querySent;
    case 'Received':
      return classes.received;
    case 'Translating':
      return classes.translating;
    case 'Getting database credentials':
      return classes.gettingCredentials;
    case 'Processing on database':
      return classes.processing;
    case 'Completed':
      return classes.completed;
    case 'Translation error':
      return classes.queryError;
    case 'Bad request':
      return classes.queryError;
    case 'Bad credentials':
      return classes.queryError;
    case 'Database error':
      return classes.queryError;
    default:
      return classes.unkown;
  }
}
/**
 * Sends the related query to the console
 * @param sentQuery The query that is send to the console.
 */
function sendQueryToConsole(sentQuery: TranslatedJSONQuery) {
  console.log('sent query : ');
  console.log({ sentQuery }.sentQuery);
  console.log(JSON.stringify({ sentQuery }.sentQuery));
}

/** An interface for the QueryItem props.  */
interface QueryItemProps extends QueryStatusListItem {
  styles: ClassNameMap;
  queryID: string;

  onReloadClicked(): void;
}
/**
 * React component for rendering one query status list item.
 * Certain options, like 'reload', will only be rendered once the status is at a certain stage (Completed for reload).
 */
export default class QueryItem extends React.Component<QueryItemProps> {
  render(): ReactElement {
    const { styles, status, sentQuery, number, time, resultIsDisplayed } = this.props;

    return (
      <div className={styles.queryListItemContainer}>
        <div
          className={clsx(
            styles.itemTextContainer,
            getClassNameForStatus(status, styles),
            resultIsDisplayed && styles.resultIsDisplayed,
          )}
        >
          <p className={styles.itemText} title={status}>
            <span style={{ fontWeight: 'bold' }}>{number}. </span>
            <span style={{ fontSize: 12 }}>{time} </span>
            {status}
          </p>
        </div>

        <div className={styles.queryItemOptionsContainer}>
          <div className={styles.queryItemOption}>
            <InfoOutlinedIcon fontSize="small" />
          </div>
          {sentQuery && (
            <div
              onClick={() => {
                sendQueryToConsole(sentQuery);
                navigator.clipboard.writeText(JSON.stringify({ sentQuery }.sentQuery));
              }}
              className={styles.queryItemOption}
              title="Print related translated query to the console."
            >
              <DescriptionOutlinedIcon fontSize="small" />
            </div>
          )}
          {status == 'Completed' && (
            <div
              title="Reload this query and querybuilder."
              onClick={() => this.props.onReloadClicked()}
              className={styles.queryItemOption}
            >
              <CachedIcon fontSize="small" />
            </div>
          )}
        </div>
      </div>
    );
  }
}
