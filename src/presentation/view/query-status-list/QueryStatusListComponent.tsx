/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { ReactElement } from 'react';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import QueryStatusListViewModel from '../../view-model/query-status-list/QueryStatusListViewModel';
import { QueryStatusListItem } from '../../view-model/query-status-list/QueryStatusListViewModelImpl';
import BaseView from '../BaseView';
import QueryItem from './QueryStatusItemComponent';
import { queries } from '@testing-library/dom';
import { ColourPalettes } from '../../../domain/entity/customization/colours';

/** The interface for the QueryStatusListComponent props. Only a QueryStatusListViewModel is needed. */
export interface QueryStatusListProps {
  queryStatusListViewModel: QueryStatusListViewModel;
  currentColours: any;
}

/** An interface for the QueryStatusListComponent state. */
export interface QueryStatusListState {
  queries: Record<string, QueryStatusListItem>;
  queryOrder: string[];

  styles: ClassNameMap;
  currentColours: any;
}

/** React component for rendering the QueryStatusList view. */
export default class QueryStatusListComponent
  extends React.Component<QueryStatusListProps, QueryStatusListState>
  implements BaseView
{
  constructor(props: QueryStatusListProps) {
    super(props);

    this.state = {
      queries: props.queryStatusListViewModel.queries,
      queryOrder: props.queryStatusListViewModel.queryIDsOrder,
      styles: props.queryStatusListViewModel.styles,
      currentColours: props.currentColours,
    };
  }

  public componentDidMount(): void {
    this.props.queryStatusListViewModel.attachView(this);
  }
  public componentWillUnmount(): void {
    this.props.queryStatusListViewModel.detachView();
  }

  public onViewModelChanged(): void {
    this.setState({
      queries: this.props.queryStatusListViewModel.queries,
      queryOrder: this.props.queryStatusListViewModel.queryIDsOrder,
      styles: this.props.queryStatusListViewModel.styles,
    });
  }

  render(): ReactElement {
    // Generate all the query status list items.
    const items = this.state.queryOrder.map((queryID, index) => (
      <QueryItem
        key={queryID}
        {...this.state.queries[queryID]}
        queryID={queryID}
        onReloadClicked={() => this.props.queryStatusListViewModel.onReloadClicked(queryID)}
        styles={this.state.styles}
      ></QueryItem>
    ));

    return (
      <div
        className={this.state.styles.root}
        style={{ backgroundColor: '#' + this.props.currentColours.dragger }}
      >
        {items}
      </div>
    );
  }
}
