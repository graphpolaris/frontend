/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'absolute',
      overflowY: 'auto',
      height: '100%',
      width: '100%',
      paddingTop: 7,
      backgroundColor: '#dfdfdf',
    },

    queryListItemContainer: {
      position: 'relative',
      transition: 'margin .2s',

      '&:hover': {
        marginBottom: '1.7rem',

        '& $itemTextContainer': {
          zIndex: 3,
          boxShadow: '0px 0px 4px gray',
        },
        '& $queryItemOptionsContainer': {
          top: '1.4rem',
          zIndex: 2,

          '& $queryItemOption': {
            height: 24,
            paddingTop: 3,
          },
        },
      },
    },
    itemTextContainer: {
      position: 'relative',
      margin: 10,
      borderRadius: 3,
      zIndex: 1,
      transition: 'boxShadow .2s',
    },
    itemText: {
      margin: 0,
      paddingLeft: 8,
      paddingRight: 8,
      paddingTop: 2,
      paddingBottom: 2,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },

    querySent: { backgroundColor: '#58dfff' },
    received: { backgroundColor: '#33beec' },
    translating: { backgroundColor: '#ef476f' },
    gettingCredentials: { backgroundColor: '#FAA369' },
    processing: { backgroundColor: '#ffd166' },
    completed: { backgroundColor: '#06d6a0' },
    unknown: { backgroundColor: 'grey' },
    queryError: { backgroundColor: '#ff6961' },

    resultIsDisplayed: { backgroundColor: '#00f2b3' },

    queryItemOptionsContainer: {
      display: 'flex',
      flexDirection: 'row-reverse',
      width: '90%',

      position: 'absolute',
      top: 0,
      right: 10,
      zIndex: 0,

      transition: 'top 0.2s',
    },
    queryItemOption: {
      backgroundColor: 'lightgrey',
      margin: '0px 2px',
      padding: '0px 2px',
      borderRadius: '0px 0px 2px 2px',
      boxShadow: '0px 1px 2px gray',
      transition: 'transform .1s',
      height: 20,

      '&:hover': {
        backgroundColor: '#cacaca',
        transform: 'translate(0,1px)',
        cursor: 'pointer',
      },

      '&:active': {
        backgroundColor: '#bfbfbf',
      },
    },
  }),
);
export { useStyles };
