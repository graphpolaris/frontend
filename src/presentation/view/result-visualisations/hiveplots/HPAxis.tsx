//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */
import React, { useRef } from 'react';
import * as d3 from 'd3-v6';
import { normalizeVector, Vector } from '../../../../domain/entity/utils/Position';
import { color, id } from './HivePlotsComponent';
import { HPNodeData } from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';

type HPAxisProps = {
  axisIndex: number;
  bins: HPNodeData[];
  length: number;
  startPos: Vector;
  /** The maximum number of nodes on the same place, used for determining opacity. */
  maxNodesAtSamePos: number;

  // Variables that should come from or generated via a HPVisViewConfig file
  nodeColor: color;
  /** nodeColors could be used for highlighting nodes. TODO: this should probably be something like Map<color, Set<number>> */
  highlightNodeColors: Map<number, color>;

  minOpacity: number;
  onMouseEnteredNode(axisIndex: number, binIndex: number): void;
  onMouseLeftNode(axisIndex: number, binIndex: number): void;
};
const HPAxis = React.memo(function HPAxis({
  axisIndex,
  bins,
  startPos,
  length,
  maxNodesAtSamePos,
  minOpacity,
  nodeColor,
  highlightNodeColors,
  onMouseEnteredNode,
  onMouseLeftNode,
}: HPAxisProps) {
  // console.log('rendered');
  const ref = useRef<SVGGElement>(null);

  const nodeElements: JSX.Element[] = [];
  bins.map(({ ids, pos }, i) => {
    const nNodes = ids.length;
    if (nNodes > 0) {
      const r = 5;

      nodeElements.push(<circle key={'w' + i} cx={pos.x} cy={pos.y} r={r} fill="white" />);
      nodeElements.push(
        <circle
          key={i}
          cx={pos.x}
          cy={pos.y}
          r={r}
          fill={highlightNodeColors.get(i) || nodeColor}
          stroke={'lightgrey'}
          opacity={(nNodes / maxNodesAtSamePos) * (1 - minOpacity) + minOpacity}
          onMouseEnter={(e) => {
            e.currentTarget.style.fill = '#24BAF0';
            e.currentTarget.style.opacity = '1';
            onMouseEnteredNode(axisIndex, i);
          }}
          onMouseLeave={(e) => {
            e.currentTarget.style.fill = '';
            e.currentTarget.style.opacity = '';
            onMouseLeftNode(axisIndex, i);
          }}
        />,
      );
    }
  });

  const vec = normalizeVector(startPos);

  return (
    <g ref={ref}>
      <line
        x1={startPos.x}
        y1={startPos.y}
        x2={startPos.x + vec.x * length}
        y2={startPos.y + vec.y * length}
        stroke="darkgrey"
        strokeWidth={2}
      />
      {nodeElements}
      {/* <g>
        <text x={startPos.x + vec.x * (length + 5)} y={startPos.y + vec.y * (length + 5)}>
          {axisIndex}
        </text>
      </g> */}
    </g>
  );
});

export default HPAxis;
