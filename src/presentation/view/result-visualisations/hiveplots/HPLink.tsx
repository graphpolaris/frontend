//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */
import React from 'react';
import { HPLinkData } from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';
import { addMagnitudeVector, crossProductVectors, dotProductVectors, rotateVectorByAngle, vectorToString } from '../../../../domain/entity/utils/Position';

/** Props for HPLink component. */
type HPLinkProps = HPLinkData & {
  maxLinksAtSamePos: number;
  minOpacity: number;
  highLight?: boolean;
};
/** HPLink renders a cubic bezier curved link between a source and target position. */
const HPLink = React.memo(function HPLink({
  fromPos,
  toPos,
  resultLinks,
  highLight,
  maxLinksAtSamePos,
  minOpacity,
}: HPLinkProps) {
  // console.log('rendered');
  // Determine cubic curve points
  // First, determine angle between sourcePos and targetPos
  const angle = Math.atan2(crossProductVectors(fromPos, toPos), dotProductVectors(fromPos, toPos));
  const rotateAngle = angle * 0.3;
  const p1 = addMagnitudeVector(rotateVectorByAngle(fromPos, rotateAngle), 5);
  const p2 = addMagnitudeVector(rotateVectorByAngle(toPos, -rotateAngle), 5);

  const opacity = (resultLinks.length / maxLinksAtSamePos) * (1 - minOpacity) + minOpacity;

  return (
    <path
      d={`M ${vectorToString(fromPos)} C ${vectorToString(p1)}, ${vectorToString(p2)}, ${vectorToString(toPos)}`}
      stroke={highLight ? '#0078A3' : 'grey'}
      fill="transparent"
      strokeWidth={highLight ? '1.5' : '1'}
      opacity={highLight ? 1 : opacity}
    ></path>
  );
});

export default HPLink;
