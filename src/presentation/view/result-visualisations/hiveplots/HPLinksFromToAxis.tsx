//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */
import React from 'react';
import {
  HPAxisData,
  HPLinksFromToBinsData,
} from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';
import HPLink from './HPLink';

/** The props needed for HPLinksFromToAxis. */
type HPLinksFromToAxisProps = {
  fromAxis: HPAxisData;
  toAxis: HPAxisData;
  linksFromToBins: HPLinksFromToBinsData;
  /** The links to highlight, per from and to bins on the axis. */
  linksFromBinsToHighLight?: { [key: number]: boolean };
  linksToBinsToHighLight?: { [key: number]: boolean };
  maxLinksAtSamePos: number;
  minOpacity: number;
};

/** Renders all the links from and to one axis. */
const HPLinksFromToAxis = React.memo(function LinksFromToAxis({
  fromAxis,
  toAxis,
  linksFromToBins,
  linksFromBinsToHighLight,
  linksToBinsToHighLight,
  maxLinksAtSamePos,
  minOpacity,
}: HPLinksFromToAxisProps) {
  // console.log('rendered');
  const linkElements: React.ReactElement[] = [];

  for (const fromBin in linksFromToBins)
    for (const toBin in linksFromToBins[fromBin]) {
      const links = linksFromToBins[fromBin][toBin];

      linkElements.push(
        <HPLink
          key={fromBin + '-' + toBin}
          fromPos={fromAxis.bins[fromBin].pos}
          toPos={toAxis.bins[toBin].pos}
          resultLinks={links}
          maxLinksAtSamePos={maxLinksAtSamePos}
          minOpacity={minOpacity}
          highLight={
            (linksFromBinsToHighLight && linksFromBinsToHighLight[fromBin]) ||
            (linksToBinsToHighLight && linksToBinsToHighLight[toBin])
          }
        />,
      );
    }

  return <g>{linkElements}</g>;
});

export default HPLinksFromToAxis;
