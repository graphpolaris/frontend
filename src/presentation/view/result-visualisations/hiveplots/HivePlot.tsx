//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */
import React, { useCallback } from 'react';
import { HivePlotData } from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';
import { color } from './HivePlotsComponent';
import HPAxis from './HPAxis';
import HPLinksFromToAxis from './HPLinksFromToAxis';
import styles from './HivePlotsComponent.module.scss';

const defaultHighlightNodeColors = new Map<number, color>();
const defaultNodeColor = '#0078A3';

/** The props needed to render one hiveplot. */
type HivePlotProps = HivePlotData & {
  name: string;
  plotIndex: number;
  minNodeOpacity?: number;
  minLinkOpacity?: number;
  nodeColor?: color;
  hightlightNodeColors?: Map<number, color>;
  onMouseEnteredNode(plotIndex: number, axisIndex: number, binIndex: number): void;
  onMouseLeftNode(plotIndex: number, axisIndex: number, binIndex: number): void;
};

/** Renders one hiveplot svg. */
const HivePlot = React.memo(function HivePlot({
  name,
  plotIndex,
  axis,
  links,
  maxNodesAtSamePos,
  maxLinksAtSamePos,
  highLightedNodes,
  minNodeOpacity = 0.2,
  minLinkOpacity = 0.2,
  nodeColor = defaultNodeColor,
  hightlightNodeColors = defaultHighlightNodeColors,
  onMouseEnteredNode,
  onMouseLeftNode,
}: HivePlotProps) {
  // Use useCallbacks for hover events so the HPAxis element doesn't rerender each time.
  const onMouseEnter = useCallback(
    (axisIndex, binIndex) => onMouseEnteredNode(plotIndex, axisIndex, binIndex),
    [plotIndex],
  );
  const onMouseLeave = useCallback(
    (axisIndex, binIndex) => onMouseLeftNode(plotIndex, axisIndex, binIndex),
    [plotIndex],
  );

  // console.log('rendered');
  // The widht, height and margin for the hiveplot svg.
  const { width, height, margin } = { width: 500, height: 500, margin: 5 };

  // Render the axis for this plot. HPAxis also renders the nodes on the plots.
  const axisElements = axis.map(({ bins, length, startPos }, i) => (
    <HPAxis
      key={startPos.x + startPos.y}
      axisIndex={i}
      bins={bins}
      startPos={startPos}
      length={length}
      maxNodesAtSamePos={maxNodesAtSamePos}
      minOpacity={minNodeOpacity}
      nodeColor={nodeColor}
      highlightNodeColors={hightlightNodeColors}
      onMouseEnteredNode={onMouseEnter}
      onMouseLeftNode={onMouseLeave}
    />
  ));

  /** All the link elements for this plot. */
  const linksPerFromToAxis: React.ReactElement[] = [];
  for (const fromAxis in links) {
    for (const toAxis in links[fromAxis]) {
      linksPerFromToAxis.push(
        <HPLinksFromToAxis
          key={fromAxis + '-' + toAxis}
          fromAxis={axis[fromAxis]}
          toAxis={axis[toAxis]}
          linksFromToBins={links[fromAxis][toAxis]}
          maxLinksAtSamePos={maxLinksAtSamePos}
          minOpacity={minLinkOpacity}
          linksFromBinsToHighLight={highLightedNodes[fromAxis]}
          linksToBinsToHighLight={highLightedNodes[toAxis]}
        />,
      );
    }
  }

  const w = width + margin * 2;
  const h = height + margin * 2;
  return (
    <svg viewBox={`0 0 ${width} ${height}`}>
      <g transform={`translate(${w / 2 + margin},${h / 2 + margin}) scale(1,-1)`}>
        {linksPerFromToAxis}
        {axisElements}
      </g>
      <text x="40" y="40" className={styles.hivePlotText}>
        {name}
      </text>
    </svg>
  );
});

export default HivePlot;
