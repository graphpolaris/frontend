//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */
import React, { ReactElement } from 'react';
import styles from './HivePlotsComponent.module.scss';
import HivePlotsViewModel from '../../../view-model/result-visualisations/hiveplots/HivePlotsViewModel';
import BaseView from '../../BaseView';
import VisConfigPanelComponent from '../utils/VisConfigPanel/VisConfigPanel';
import HivePlotsConfigPanel from './HivePlotsConfigPanel';
import HivePlot from './HivePlot';
import {
  HivePlotsViewConfig,
  possibleAssignByValues,
} from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsViewConfig';
import { HivePlotData } from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsDataType';
import { Node } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';

// const demoValues = [generateRandomValues(), generateRandomValues(), generateRandomValues()];
export type color = string;
export type id = string;

/** HivePlotsProps is an interface containing the HivePlotsViewModel. */
export interface HivePlotsProps {
  HivePlotsViewModel: HivePlotsViewModel;
}

/** HivePlotsState is an interface containing the queryResult. */
export interface HivePlotsState {
  viewConfig: HivePlotsViewConfig;
  possibleAxisAssignmentValues: Record<possibleAssignByValues, string[]>;
  hivePlotsData: HivePlotData[];
  highLightedNodes: Node[];
}

/** Renders the query result object as a hiveplot. */
export default class HivePlotsComponent
  extends React.Component<HivePlotsProps, HivePlotsState>
  implements BaseView {
  private hivePlotsViewModel: HivePlotsViewModel;

  public constructor(props: HivePlotsProps) {
    super(props);

    this.hivePlotsViewModel = props.HivePlotsViewModel;

    this.state = {
      viewConfig: this.hivePlotsViewModel.viewConfig,
      possibleAxisAssignmentValues: this.hivePlotsViewModel.possibleAxisAssignmentValues,
      hivePlotsData: this.hivePlotsViewModel.hivePlotsData,
      highLightedNodes: this.hivePlotsViewModel.highLightedNodes,
    };
  }

  /** The componenDidMount function is called after the site is loaded and the component attached. */
  public componentDidMount(): void {
    this.hivePlotsViewModel.attachView(this);
    this.hivePlotsViewModel.subscribeToQueryResult();
    this.hivePlotsViewModel.setThisVisAsExportable();
  }

  /** The componentWillUnmount function is called when the site is closed to remove the components. */
  public componentWillUnmount(): void {
    this.hivePlotsViewModel.detachView();
    this.hivePlotsViewModel.unSubscribeFromQueryResult();
  }

  /** The onViewModelChanged function sets the queryResult. */
  public onViewModelChanged(): void {
    this.setState({
      viewConfig: this.hivePlotsViewModel.viewConfig,
      possibleAxisAssignmentValues: this.hivePlotsViewModel.possibleAxisAssignmentValues,
      hivePlotsData: this.hivePlotsViewModel.hivePlotsData,
      highLightedNodes: this.hivePlotsViewModel.highLightedNodes,
    });
  }

  /** Renders the html. */
  render(): ReactElement {
    return (
      <div className={styles.container}>
        <div className={styles.visContainer}>
          <div style={{ transform: 'scale(-1,1)' }}>
            <p className={styles.title}>HivePlots!</p>
            <div className={styles.hivePlotsContainer}>
              {this.state.hivePlotsData.map((HPData, i) => (
                <div
                  key={this.state.viewConfig.plots[i].name + i}
                  className={styles.hivePlotContainer}
                >
                  <HivePlot
                    name={this.state.viewConfig.plots[i].name}
                    plotIndex={i}
                    axis={HPData.axis}
                    links={HPData.links}
                    maxNodesAtSamePos={HPData.maxNodesAtSamePos}
                    maxLinksAtSamePos={HPData.maxLinksAtSamePos}
                    highLightedNodes={HPData.highLightedNodes}
                    onMouseEnteredNode={this.hivePlotsViewModel.onMouseEnteredNode}
                    onMouseLeftNode={this.hivePlotsViewModel.onMouseLeftNode}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={styles.nodeInfoContainer}>
          {/* TODO: the attribute to display of node info could be an option in the panel. Which attribute to use. */}
          <p>
            {this.state.highLightedNodes.reduce(
              (prev, n) =>
                prev +
                ' ' +
                (n.attributes['name']
                  ? n.attributes['name']
                  : n.attributes['label']
                  ? n.attributes['label']
                  : n.id),
              '',
            )}
          </p>
        </div>
        <VisConfigPanelComponent>
          <HivePlotsConfigPanel
            hivePlotsViewConfig={this.state.viewConfig}
            possibleAxisAssignments={this.state.possibleAxisAssignmentValues}
            addPlot={this.hivePlotsViewModel.addPlot}
            deleteSelectedPlot={this.hivePlotsViewModel.deleteSelectedPlot}
            selectedPlotChanged={this.hivePlotsViewModel.setSelectedPlotInConfigPanel}
            addAxis={this.hivePlotsViewModel.addAxis}
            deleteAxis={this.hivePlotsViewModel.deleteAxis}
            assignAxisByChanged={this.hivePlotsViewModel.setAssignAxisBy}
            axisAssignmentValueChanged={this.hivePlotsViewModel.setAxisAssignmentValue}
            axisOrderNodesByValueChanged={this.hivePlotsViewModel.setAxisOrderNodesByValue}
            setMaxNOfBins={this.hivePlotsViewModel.setMaxNOfBins}
          />
        </VisConfigPanelComponent>
      </div>
    );
  }
}
