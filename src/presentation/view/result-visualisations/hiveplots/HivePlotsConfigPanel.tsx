//This file wasn't made or edited by GravenvanPolaris.
/* istanbul ignore file */
import React, { useEffect, useRef, useState } from 'react';
import styles from './HivePlotsConfigPanel.module.scss';
// import { ReactComponent as QuestionMarkSVG } from '../utils/VisConfigPanel/QuestionMarkIcon.svg';
import {
  HivePlotsViewConfig,
  possibleAssignByValues,
  possibleOrderByValues,
} from '../../../../domain/entity/result-visualisations/hiveplots/HivePlotsViewConfig';

/** Props for this component */
type HPConfigPanelProps = {
  hivePlotsViewConfig: HivePlotsViewConfig;
  possibleAxisAssignments: Record<possibleAssignByValues, string[]>;
  selectedPlotChanged(newSelectedPlotIndex: number): void;
  addPlot(name: string): void;
  deleteSelectedPlot(): void;
  addAxis(): void;
  deleteAxis(index: number): void;
  assignAxisByChanged(a: possibleAssignByValues): void;
  axisAssignmentValueChanged(axisIndex: number, value: string): void;
  axisOrderNodesByValueChanged(axisIndex: number, value: string): void;
  setMaxNOfBins(newMaxNOfBins: number): void;
};
/** Component for rendering config input fields for HivePlot vis. */
const HivePlotsConfigPanel = React.memo(function HivePlotsConfigPanel({
  hivePlotsViewConfig,
  possibleAxisAssignments,
  selectedPlotChanged,
  addPlot,
  deleteSelectedPlot,
  assignAxisByChanged,
  addAxis,
  deleteAxis,
  axisAssignmentValueChanged,
  axisOrderNodesByValueChanged,
  setMaxNOfBins,
}: HPConfigPanelProps) {
  // console.log('config panel rendered!');
  const maxBinsInputRef = useRef<HTMLInputElement>(null);
  const [addingAPlot, setAddingAPlot] = useState(false);
  const { selectedPlotInConfigPanelIndex: currentPlot, plots } = hivePlotsViewConfig;

  const plot = plots[currentPlot] ? plots[currentPlot] : undefined;

  const onPlotSelectionChanged = (e: React.ChangeEvent<HTMLSelectElement>): void => {
    if (e.target.value == 'add') setAddingAPlot(true);
    else if (e.target.value != plot?.name)
      selectedPlotChanged(plots.findIndex((p) => p.name == e.target.value));
  };

  const addingAPlotFinished = (name: string): void => {
    if (name != '' && plots.every((p) => p.name != name)) {
      addPlot(name);
      setAddingAPlot(false);
    }
  };

  const maxNOfBinsChanged = (v: number) => {
    if (v != plot?.maxNOfBins) {
      const clampedV = !isNaN(v) ? Math.max(1, Math.min(200, v)) : 1;
      setMaxNOfBins(clampedV);
    }
  };

  return (
    <div className={styles.container}>
      <p className={styles.title}>HivePlot settings:</p>

      <div className={styles.selectContainer} title="Select the hiveplot you want to edit.">
        {addingAPlot || plots.length < 1 ? (
          <>
            <p>Add a plot: </p>
            <input
              type="text"
              autoFocus
              style={{ maxWidth: 100 }}
              // onBlur={(e) => addingAPlotFinished(e.currentTarget.value)}
              onKeyUp={(e) =>
                e.key === 'Enter' ? addingAPlotFinished(e.currentTarget.value) : true
              }
            ></input>
          </>
        ) : (
          <>
            <p>Plot: </p>
            <select
              defaultValue={currentPlot > -1 ? currentPlot : 'add'}
              onChange={onPlotSelectionChanged}
            >
              {plots.map((p) => (
                <option key={p.name} value={p.name}>
                  {p.name}
                </option>
              ))}
              <option
                value="add"
                // onClick={() => {
                //   setAddingAPlot(true);
                //   console.log('hi');
                // }}
              >
                Add plot...
              </option>
            </select>
            <p
              className={styles.deleteButton}
              onClick={deleteSelectedPlot}
              title="Delete this plot"
            >
              X
            </p>
          </>
        )}
      </div>

      <p className={styles.subtitle}>- Axis assignment</p>
      <div className={styles.subContainer}>
        <div
          className={styles.selectContainer}
          title="Select a node parameter for axis assignment."
        >
          <p>Assign by: </p>
          <ConfigPanelSelect
            defaultValue={plot?.assignAxisBy || ''}
            options={Object.keys(possibleAxisAssignments)}
            onChange={assignAxisByChanged}
          />
        </div>

        {/* <div className={styles.selectContainer}>
          <p>Order axis individually:</p>
          <input
            type="checkbox"
            onChange={() => console.log('TODO: order axis individually')}
          ></input>
        </div> */}

        {plot && (
          <>
            <div
              className={styles.axisContainer}
              title="Define rules to assign each node to an axis."
            >
              {plot.axis.map((a, i) => (
                <div
                  key={a.assignmentValue + a.orderNodesBy}
                  className={styles.singleAxisContainer}
                >
                  <p className={styles.subsubtitle}>Axis {i + 1}: </p>
                  <p
                    className={styles.deleteButton}
                    onClick={() => deleteAxis(i)}
                    title="Delete this axis"
                  >
                    X
                  </p>
                  <div className={styles.selectContainer}>
                    <p>Nodes:</p>
                    <ConfigPanelSelect
                      defaultValue={a.assignmentValue}
                      options={possibleAxisAssignments[plot.assignAxisBy]}
                      onChange={(v) => axisAssignmentValueChanged(i, v)}
                    />
                  </div>

                  <div className={styles.selectContainer}>
                    <p>Order by:</p>
                    <ConfigPanelSelect
                      defaultValue={a.orderNodesBy}
                      options={possibleOrderByValues}
                      onChange={(v) => axisOrderNodesByValueChanged(i, v)}
                    />
                  </div>
                </div>
              ))}
            </div>

            {possibleAxisAssignments[plot.assignAxisBy].length > plot.axis.length && (
              <div className={styles.addAxisButtonContainer}>
                <button onClick={() => addAxis()}>Add axis</button>
              </div>
            )}
          </>
        )}
      </div>

      <p className={styles.subtitle}>- Number of bins per axis</p>
      <div className={styles.subContainer}>
        <input
          ref={maxBinsInputRef}
          key={plot?.maxNOfBins}
          type="number"
          style={{ width: '4rem', marginBottom: 30 }}
          onBlur={(e) => maxNOfBinsChanged(parseInt(e.currentTarget.value))}
          onKeyUp={(e) =>
            e.key === 'Enter' ? maxNOfBinsChanged(parseInt(e.currentTarget.value)) : true
          }
          onClick={(e) => maxNOfBinsChanged(parseInt(e.currentTarget.value))}
          defaultValue={(plot?.maxNOfBins || 40) + ''}
        ></input>
      </div>
    </div>
  );
});
export default HivePlotsConfigPanel;

function ConfigPanelSelect({
  defaultValue,
  options,
  onChange,
}: {
  defaultValue: string;
  options: string[];
  onChange(v: string): void;
}) {
  return (
    <select value={defaultValue} onChange={(e) => onChange(e.target.value)}>
      {options.map((v) => (
        <option key={v} value={v}>
          {v}
        </option>
      ))}
    </select>
  );
}
