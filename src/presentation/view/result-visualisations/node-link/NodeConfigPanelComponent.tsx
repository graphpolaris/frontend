/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useRef, useState } from 'react';
import styles from './NodeLinkConfigPanel.module.scss';
import { GraphType, NodeType } from '../../../../domain/entity/node-link/structures/Types';
import NodeLinkViewModel from '../../../view-model/result-visualisations/node-link/NodeLinkViewModel';
import NodeLinkConfigPanelViewModel from '../../../view-model/result-visualisations/node-link/NodeLinkConfigPanelViewModel';
import NodeLinkConfigPanelViewModelImpl from '../../../view-model/result-visualisations/node-link/NodeLinkConfigPanelViewModelImpl';
import NodeLinkViewModelImpl from '../../../view-model/result-visualisations/node-link/NodeLinkViewModelImpl';
import { Slider } from '@material-ui/core';

/** Props for this component */
type NLConfigPanelProps = {
  graph: GraphType;
  nlViewModel: NodeLinkViewModel;
};
export default function NodeLinkConfigPanelComponent(props: NLConfigPanelProps) {
  let graph: GraphType = props.graph;
  let nodeLinkViewModel: NodeLinkViewModel = props.nlViewModel;

  const nodeLinkConfigPanelViewModel: NodeLinkConfigPanelViewModel =
    new NodeLinkConfigPanelViewModelImpl(graph, nodeLinkViewModel);
  const nlv = nodeLinkConfigPanelViewModel; //short name, for convenience

  return (
    <div className={styles.container}>
      <p className={styles.title}>NodeLink Settings:</p>

      {/* Experimental options implementation */}
      <p className={styles.subtitle}>- Visual Assignment</p>
      <div className={styles.subContainer}>
        <p>Node: </p>
        {nlv.nodeSelect()}
      </div>

      {/* Visuals */}
      <div className={styles.subContainer}>
        <p>Colors: </p>
        {nlv.colorSelect()}
      </div>

      {/* Attributes */}
      <div className={styles.subContainer}>
        <p>Attribute: </p>
        {nlv.attrSelect()}
      </div>

      {/* Representation */}
      <div className={styles.subContainer}>
        <p>Representation: </p>
        {nlv.visSelect()}
      </div>

      {/* Check if community detection pill is used */}
      {props.nlViewModel.graph.communityDetection ? (
        <div>
          <p className={styles.title}>Community Detection Settings:</p>

          {/* List of categorical attributes + mldata */}
          <div className={styles.subContainer}>
            <p>Cluster on: </p>
            {nlv.cdClusterBasedOnSelect()}
          </div>

          {/* Values of concerned categorical attribute/mldata */}
          <div className={styles.subContainer}>
            <p>Value: </p>
            {nlv.cdValueSelect()}
          </div>

          {/* Visuals */}
          <div className={styles.subContainer}>
            <p>Colors:</p>
            {nlv.cdColorSelect()}
          </div>
        </div>
      ) : (
        <div></div>
      )}

      {/* Check if link prediction pill is used */}
      {props.nlViewModel.graph.linkPrediction ? (
        <div>
          {/* Slider for link prediction */}
          <p className={styles.title}>Link Prediction Slider:</p>
          <div className={styles.subContainer}>
            <Slider
              style={{ width: '80%', marginLeft: '20px' }}
              id="Slider"
              defaultValue={0.5}
              step={0.05}
              min={0}
              max={1}
              marks={true}
              valueLabelDisplay="auto"
              // To render every change
              onChange={() => {
                props.nlViewModel.updateJaccardThreshHold();
                props.nlViewModel.ticked();
              }}
            ></Slider>
          </div>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
}
