/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { Component } from 'react';
import BaseView from '../../BaseView';
import * as PIXI from 'pixi.js';
import * as d3 from 'd3-v6';
import VisConfigPanelComponent from '../utils/VisConfigPanel/VisConfigPanel';
import {
  GraphType,
  LinkType,
  NodeType,
} from '../../../../domain/entity/node-link/structures/Types';
import NodeLinkViewModel from '../../../view-model/result-visualisations/node-link/NodeLinkViewModel';
import { currentColours } from '../../graph-schema/SchemaComponent';
import NodeLinkConfigPanelComponent from './NodeConfigPanelComponent';
import AttributesConfigPanel from '../utils/attributes-config/AttributesConfigPanel';
import AttributesConfigPanelViewModel from '../../../view-model/result-visualisations/utils/attributes-config/AttributesConfigPanelViewModel';

/** NodeLinkComponentProps is an interface containing the nodeLinkViewModel. */
export interface NodeLinkComponentProps {
  nodeLinkViewModel: NodeLinkViewModel;
  currentColours: any;
}

/** This is the interface of the NodeLinkComponentState. */
export interface NodeLinkComponentState {
  graph: GraphType;
  width: number;
  height: number;
  stage: PIXI.Container;
  links: PIXI.Graphics;
  simulation: d3.Simulation<NodeType, LinkType>;
  myRef: React.RefObject<HTMLDivElement>;
  renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
  dragOffset: any;
  panOffset: any;
  scalexy: number;
}

/** The class that holds the NodeLinkComponent. */
export default class NodeLinkComponent
  extends Component<NodeLinkComponentProps, NodeLinkComponentState>
  implements BaseView
{
  private nodeLinkViewModel: NodeLinkViewModel;

  public constructor(props: NodeLinkComponentProps) {
    super(props);

    const { nodeLinkViewModel } = props;
    this.nodeLinkViewModel = nodeLinkViewModel;

    this.state = {
      graph: nodeLinkViewModel.graph,
      width: nodeLinkViewModel.width,
      height: nodeLinkViewModel.height,
      stage: nodeLinkViewModel.stage,
      links: nodeLinkViewModel.links,
      simulation: nodeLinkViewModel.simulation,
      myRef: nodeLinkViewModel.myRef,
      renderer: nodeLinkViewModel.renderer,
      panOffset: nodeLinkViewModel.panOffset,
      dragOffset: nodeLinkViewModel.dragOffset,
      scalexy: nodeLinkViewModel.scalexy,
    };
  }

  /** Return default radius */
  public Radius() {
    return 5;
  }

  /**
   * The componenDidMount function is called after the site is loaded and the component attached.
   * It calls upon initialising and starting functions. It is more or less the same as using useEffect in a functional component.
   */
  public componentDidMount() {
    // Attach Viewmodel Observer
    this.nodeLinkViewModel.attachView(this);

    window.addEventListener('resize', this.nodeLinkViewModel.handleResize);

    // Init simulation
    this.nodeLinkViewModel.startSimulation();

    // Init the graph, with node radius 5
    this.nodeLinkViewModel.SetNodeGraphics(this.state.graph, this.Radius());

    // Select d3 elements for zooming, panning and dragging
    this.nodeLinkViewModel.selectD3Elements();

    this.state.simulation.on('tick', this.nodeLinkViewModel.ticked);

    this.nodeLinkViewModel.subscribeToQueryResult();
    this.nodeLinkViewModel.setThisVisAsExportable();
    this.nodeLinkViewModel.subscribeToAnalyticsData();
  }

  /** The componentWillUnmount function is called when the site is closed to remove the components. */
  public componentWillUnmount() {
    // Detach Viewmodel Observer
    this.nodeLinkViewModel.detachView();

    window.removeEventListener('resize', this.nodeLinkViewModel.handleResize);

    this.nodeLinkViewModel.unSubscribeFromQueryResult();
    this.nodeLinkViewModel.unSubscribeFromAnalyticsData();
  }

  /** The onViewModelChanged function is part of the observer pattern, it is called by the observer when for example the graph changes. */
  public onViewModelChanged(): void {
    this.setState({
      graph: this.nodeLinkViewModel.graph,
      width: this.nodeLinkViewModel.width,
      height: this.nodeLinkViewModel.height,
      stage: this.nodeLinkViewModel.stage,
      links: this.nodeLinkViewModel.links,
      simulation: this.nodeLinkViewModel.simulation,
      myRef: this.nodeLinkViewModel.myRef,
      renderer: this.nodeLinkViewModel.renderer,
      panOffset: this.nodeLinkViewModel.panOffset,
      dragOffset: this.nodeLinkViewModel.dragOffset,
      scalexy: this.nodeLinkViewModel.scalexy,
    });
  }

  /** Returns the render component. */
  public render() {
    this.nodeLinkViewModel.currentColours = this.props.currentColours;
    this.nodeLinkViewModel.UpdateColours(this.nodeLinkViewModel.graph, 5);
    return (
      <div className="viscontainer">
        <div
          style={{
            width: '100%',
            height: '100%',
            overflow: 'hidden',
            backgroundColor: '#' + this.props.currentColours.visBackground,
          }}
          className="renderer"
          ref={this.state.myRef}
        ></div>
        <VisConfigPanelComponent>
          <NodeLinkConfigPanelComponent
            graph={this.state.graph}
            nlViewModel={this.nodeLinkViewModel}
          />
        </VisConfigPanelComponent>
        <VisConfigPanelComponent isLeft>
          <AttributesConfigPanel nodeLinkViewModel={this.nodeLinkViewModel} />
        </VisConfigPanelComponent>
      </div>
    );
  }
}
