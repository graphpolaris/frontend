/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import * as d3 from 'd3-v6';
import * as hyperEdgeType from '../../../../domain/entity/paohvis/structures/Types';
// import style from './PaohvisComponent.module.scss';

type HyperEdgeRangeProps = {
  index: number;
  text: string;
  hyperEdges: hyperEdgeType.HyperEdge[];
  nRows: number;
  colOffset: number;
  xOffset: number;
  yOffset: number;
  rowHeight: number;
  hyperedgeColumnWidth: number;
  gapBetweenRanges: number;

  onMouseEnter: (col: number, nameToShow: string) => void;
  onMouseLeave: (col: number) => void;
};
export default class HyperEdgeRange extends React.Component<HyperEdgeRangeProps> {
  render() {
    // calculate the width of the hyperEdgeRange.
    const rectWidth =
      this.props.hyperEdges.length * this.props.hyperedgeColumnWidth +
      2 * this.props.gapBetweenRanges;

    const rows: JSX.Element[] = [];
    for (let i = 0; i < this.props.nRows; i++)
      rows.push(
        <g key={i} className={'row-' + i}>
          <rect
            key={i.toString()}
            y={i * this.props.rowHeight}
            width={rectWidth}
            height={this.props.rowHeight}
            fill={i % 2 === 0 ? '#e6e6e6' : '#f5f5f5'}
            style={{ zIndex: -1 }}
          />
        </g>,
      );

    let hyperedges = this.props.hyperEdges.map((hyperEdge, i) => (
      <HyperEdge
        key={i}
        col={i + this.props.colOffset}
        connectedRows={hyperEdge.indices}
        numbersOnRows={hyperEdge.frequencies}
        nameToShow={hyperEdge.nameToShow}
        rowHeight={this.props.rowHeight}
        xOffset={
          this.props.hyperedgeColumnWidth / 2 +
          i * this.props.hyperedgeColumnWidth +
          this.props.gapBetweenRanges
        }
        radius={8}
        onMouseEnter={this.props.onMouseEnter}
        onMouseLeave={this.props.onMouseLeave}
      />
    ));

    // * 3 because we have a gapBetweenRanges as padding
    const xOffset =
      this.props.xOffset +
      (this.props.index * 3 + 1) * this.props.gapBetweenRanges +
      this.props.colOffset * this.props.hyperedgeColumnWidth;

    return (
      <g transform={'translate(' + xOffset + ', +' + this.props.yOffset + ')'}>
        <text
          textDecoration={'underline'}
          x="10"
          y={this.props.rowHeight / 2}
          dy=".35em"
          style={{ fontWeight: 600, transform: 'translate3d(-10px, 15px, 0px) rotate(-30deg)' }}
        >
          {this.props.text}
        </text>
        <g transform={'translate(0,' + this.props.rowHeight + ')'}>
          {rows}
          {hyperedges}
        </g>
      </g>
    );
  }
}

type HyperEdgeProps = {
  col: number;
  connectedRows: number[];
  numbersOnRows: number[];
  rowHeight: number;
  xOffset: number;
  radius: number;
  nameToShow: string;
  onMouseEnter: (col: number, nameToShow: string) => void;
  onMouseLeave: (col: number) => void;
};

class HyperEdge extends React.Component<HyperEdgeProps> {
  ref = React.createRef<SVGGElement>();

  constructor(props: HyperEdgeProps) {
    super(props);
    this.ref = React.createRef<SVGRectElement>();
  }

  /** Adds mouse events when the user hovers over and out of a hyperedge. */
  public componentDidMount(): void {
    d3.select(this.ref.current)
      .on('mouseover', () => this.props.onMouseEnter(this.props.col, this.props.nameToShow))
      .on('mouseout', () => this.props.onMouseLeave(this.props.col));
  }

  render() {
    // render all circles on the correct position.
    const circles = this.props.connectedRows.map((row, i) => {
      return (
        <g key={'row' + row + ' col' + i}>
          <circle
            cx={0}
            cy={row * this.props.rowHeight}
            r={this.props.radius}
            fill={'white'}
            stroke={'black'}
            strokeWidth={1}
          />
        </g>
      );
    });

    // create a line between two circles.
    const lines: JSX.Element[] = [];
    let y1 = this.props.connectedRows[0] * this.props.rowHeight + this.props.radius;
    let y2;
    for (let i = 1; i < this.props.connectedRows.length; i++) {
      y2 = this.props.connectedRows[i] * this.props.rowHeight - this.props.radius;
      lines.push(
        <line key={'line' + i} x1={0} y1={y1} x2={0} y2={y2} stroke={'black'} strokeWidth={1} />,
      );
      y1 = this.props.connectedRows[i] * this.props.rowHeight + this.props.radius;
    }

    const yOffset = this.props.rowHeight * 0.5;

    return (
      <g
        ref={this.ref}
        className={'col-' + this.props.col}
        transform={'translate(' + this.props.xOffset + ',' + yOffset + ')'}
      >
        {lines}
        {circles}
      </g>
    );
  }
}
