/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { Button, MenuItem, TextField } from '@material-ui/core';
import React, { ReactElement } from 'react';
import {
  EntitiesFromSchema,
  NodeOrder,
  RelationsFromSchema,
} from '../../../../domain/entity/paohvis/structures/Types';
import { Sort } from '@material-ui/icons';
import { IconButton } from '@material-ui/core/';
import './MakePaohvisMenu.scss';
import BaseView from '../../BaseView';
import MakePaohvisMenuViewModel from '../../../view-model/result-visualisations/paohvis/MakePaohvisMenuViewModel';

/** The typing for the props of the Paohvis menu */
type MakePaohvisMenuProps = {
  makePaohvisMenuViewModel: MakePaohvisMenuViewModel;
};

/** The variables in the state of the PAOHvis menu */
type MakePaohvisMenuState = {
  entityVertical: string;
  entityHorizontal: string;
  relationName: string;
  isEntityVerticalEqualToRelationFrom: boolean;
  attributeNameAndOrigin: string;
  isSelectedAttributeFromEntity: boolean;
  isButtonEnabled: boolean;
  sortOrder: NodeOrder;
  isReverseOrder: boolean;

  entitiesFromSchema: EntitiesFromSchema;
  relationsFromSchema: RelationsFromSchema;
  entitiesFromQueryResult: string[];

  relationNameOptions: string[];
  attributeNameOptions: Record<string, string[]>;
};

/** React component that renders a menu with input fields for adding a new Paohvis visualisation. */
export default class MakePaohvisMenu
  extends React.Component<MakePaohvisMenuProps, MakePaohvisMenuState>
  implements BaseView
{
  private makePaohvisMenuViewModel: MakePaohvisMenuViewModel;

  constructor(props: MakePaohvisMenuProps) {
    super(props);

    this.makePaohvisMenuViewModel = this.props.makePaohvisMenuViewModel;

    this.state = {
      entityVertical: '',
      entityHorizontal: '',
      relationName: '',
      isEntityVerticalEqualToRelationFrom: true,
      attributeNameAndOrigin: '',
      isSelectedAttributeFromEntity: false,
      isButtonEnabled: false,
      sortOrder: NodeOrder.degree,
      isReverseOrder: false,

      entitiesFromSchema: { entityNames: [], attributesPerEntity: {}, relationsPerEntity: {} },
      relationsFromSchema: {
        relationCollection: [],
        relationNames: {},
        attributesPerRelation: {},
      },
      entitiesFromQueryResult: [],

      relationNameOptions: [],
      attributeNameOptions: {},
    };
  }

  public componentDidMount(): void {
    this.makePaohvisMenuViewModel.attachView(this);
    this.makePaohvisMenuViewModel.subscribeToBroker();
  }

  public componentWillUnmount(): void {
    this.makePaohvisMenuViewModel.detachView();
    this.makePaohvisMenuViewModel.unSubscribeFromBroker();
  }

  /** Is called when the state of the ViewModel changes. */
  public onViewModelChanged(): void {
    this.setState({
      entityVertical: this.makePaohvisMenuViewModel.entityVertical,
      entityHorizontal: this.makePaohvisMenuViewModel.entityHorizontal,
      relationName: this.makePaohvisMenuViewModel.relationName,
      isEntityVerticalEqualToRelationFrom:
        this.makePaohvisMenuViewModel.isEntityVerticalEqualToRelationFrom,
      attributeNameAndOrigin: this.makePaohvisMenuViewModel.attributeNameAndOrigin,
      isSelectedAttributeFromEntity: this.makePaohvisMenuViewModel.isSelectedAttributeFromEntity,
      isButtonEnabled: this.makePaohvisMenuViewModel.isButtonEnabled,
      sortOrder: this.makePaohvisMenuViewModel.sortOrder,
      isReverseOrder: this.makePaohvisMenuViewModel.isReverseOrder,

      entitiesFromSchema: this.makePaohvisMenuViewModel.entitiesFromSchema,
      relationsFromSchema: this.makePaohvisMenuViewModel.relationsFromSchema,
      entitiesFromQueryResult: this.makePaohvisMenuViewModel.entitiesFromQueryResult,

      relationNameOptions: this.makePaohvisMenuViewModel.relationNameOptions,
      attributeNameOptions: this.makePaohvisMenuViewModel.attributeNameOptions,
    });
  }

  render(): ReactElement {
    // Retrieve the possible entity options. If none available, set helper message.
    let entityMenuItems: ReactElement[];
    if (this.state.entitiesFromQueryResult.length > 0)
      entityMenuItems = this.state.entitiesFromQueryResult.map((entity) => (
        <MenuItem key={entity} value={entity}>
          {entity}
        </MenuItem>
      ));
    else
      entityMenuItems = [
        <MenuItem key="placeholder" value="" disabled>
          No query data available
        </MenuItem>,
      ];

    // Retrieve the possible relationName options. If none available, set helper message.
    let relationNameMenuItems: ReactElement[];
    if (this.state.relationNameOptions.length > 0)
      relationNameMenuItems = this.state.relationNameOptions.map((relation) => (
        <MenuItem key={relation} value={relation}>
          {relation}
        </MenuItem>
      ));
    else
      relationNameMenuItems = [
        <MenuItem key="placeholder" value="" disabled>
          First select an entity with one or more relations
        </MenuItem>,
      ];

    // Retrieve all the possible attributeName options. If none available, set helper message.
    let attributeNameMenuItems: ReactElement[] = [];
    let attributeNameMenuItemsNoAttribute: ReactElement[] = [];
    let attributeNameMenuItemsEntity: ReactElement[] = [];
    let attributeNameMenuItemsRelation: ReactElement[] = [];
    if (this.state.attributeNameOptions['Entity'] && this.state.attributeNameOptions['Relation']) {
      attributeNameMenuItemsNoAttribute = this.state.attributeNameOptions['No attribute'].map(
        (attribute) => (
          <MenuItem key={attribute} value={`${attribute}:NoAttribute`}>
            {attribute}
          </MenuItem>
        ),
      );
      attributeNameMenuItemsEntity = this.state.attributeNameOptions['Entity'].map((attribute) => (
        <MenuItem key={`${attribute}:Entity`} value={`${attribute}:Entity`}>
          {`${attribute} : Entity`}
        </MenuItem>
      ));
      attributeNameMenuItemsRelation = this.state.attributeNameOptions['Relation'].map(
        (attribute) => (
          <MenuItem key={`${attribute}:Relation`} value={`${attribute}:Relation`}>
            {`${attribute} : Relation`}
          </MenuItem>
        ),
      );

      attributeNameMenuItems = attributeNameMenuItemsNoAttribute
        .concat(attributeNameMenuItemsEntity)
        .concat(attributeNameMenuItemsRelation);
    } else
      attributeNameMenuItems = [
        <MenuItem key="placeholder" value="" disabled>
          First select an relation with one or more attributes
        </MenuItem>,
      ];

    // make sort order menu items
    const sortOrderMenuItems: ReactElement[] = Object.values(NodeOrder).map((nodeOrder) => {
      return (
        <MenuItem key={nodeOrder} value={nodeOrder}>
          {nodeOrder}
        </MenuItem>
      );
    });

    // make the reverse button
    let reverseIcon: ReactElement;
    switch (this.state.sortOrder) {
      case NodeOrder.alphabetical:
        reverseIcon = <span id="reverseButtonLabel">A-Z</span>;
        break;
      default:
        reverseIcon = <Sort className={'reverseSortIcon'} />;
        break;
    }

    // return the whole MakePaohvisMenu
    return (
      <div className="makePaohvisMenu">
        <TextField
          select
          className="textFieldMakePaohvisMenu"
          id="standard-select-entity"
          label="Entity"
          value={this.state.entityVertical}
          onChange={this.makePaohvisMenuViewModel.onChangeEntity}
        >
          {entityMenuItems}
        </TextField>
        <TextField
          select
          className="textFieldMakePaohvisMenu"
          id="standard-select-relation"
          label="Relation"
          value={this.state.relationName}
          onChange={this.makePaohvisMenuViewModel.onChangeRelationName}
        >
          {relationNameMenuItems}
        </TextField>
        <TextField
          select
          className="textFieldMakePaohvisMenu"
          id="standard-select-attribute"
          label="Attribute"
          value={this.state.attributeNameAndOrigin}
          onChange={this.makePaohvisMenuViewModel.onChangeAttributeName}
        >
          {attributeNameMenuItems}
        </TextField>
        <TextField
          select
          className="textFieldMakePaohvisMenu"
          id="standard-select-sort-order"
          label="Sort order"
          value={this.state.sortOrder}
          onChange={this.makePaohvisMenuViewModel.onChangeSortOrder}
        >
          {sortOrderMenuItems}
        </TextField>
        <IconButton
          className={'reverseIconButton'}
          color="inherit"
          onClick={this.makePaohvisMenuViewModel.onClickReverseOrder}
        >
          {reverseIcon}
        </IconButton>

        <Button
          id="makeButton"
          variant="contained"
          color="default"
          disabled={!this.state.isButtonEnabled}
          onClick={this.makePaohvisMenuViewModel.onClickMakeButton}
        >
          <span>Make</span>
        </Button>
      </div>
    );
  }
}
