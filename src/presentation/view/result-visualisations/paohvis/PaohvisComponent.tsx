/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import BaseView from '../../BaseView';
import React from 'react';
import HyperEdgeRange from './HyperEdgesRange';
import RowLabelColumn from './RowLabelColumn';
import PaohvisViewModel from '../../../view-model/result-visualisations/paohvis/PaohvisViewModel';
import { PaohvisData } from '../../../../domain/entity/paohvis/structures/Types';
import MakePaohvisMenu from './MakePaohvisMenu';
import VisConfigPanelComponent from '../utils/VisConfigPanel/VisConfigPanel';
import PaohvisFilterComponent from './PaohvisFilterComponent';
import styles from './PaohvisComponent.module.scss';
import { FilterType } from '../../../../domain/entity/paohvis/structures/Types';

import Tooltip from './Tooltip';
import { getWidthOfText } from '../../../util/paohvis/utils';
import visConfigPanelStyle from '../utils/VisConfigPanel/VisConfigPanel.module.scss';

export interface PaohvisComponentProps {
  paohvisViewModel: PaohvisViewModel;
}

/** The variables in the state of the PAOHvis component */
type PaohvisComponentState = {
  rowHeight: number;
  hyperedgeColumnWidth: number;
  gapBetweenRanges: number;

  data: PaohvisData;
  makePaohvisButtonAnchor: Element | null;
  entityVertical: string;
  entityHorizontal: string;
  relation: string;

  isEntityFromRelationFrom: boolean;
};

/** React component that renders the whole Paohvis visualisation. */
export default class Paohvis
  extends React.Component<PaohvisComponentProps, PaohvisComponentState>
  implements BaseView
{
  svgRef = React.createRef<SVGSVGElement>();
  private paohvisViewModel: PaohvisViewModel;

  constructor(props: PaohvisComponentProps) {
    super(props);
    this.paohvisViewModel = props.paohvisViewModel;

    this.state = {
      rowHeight: props.paohvisViewModel.rowHeight,
      hyperedgeColumnWidth: props.paohvisViewModel.hyperedgeColumnWidth,
      gapBetweenRanges: props.paohvisViewModel.gapBetweenRanges,
      data: props.paohvisViewModel.data,
      makePaohvisButtonAnchor: null,
      entityVertical: '',
      entityHorizontal: '',
      relation: '',
      isEntityFromRelationFrom: true,
    };
  }

  /** Is called when the state of the ViewModel changes. */
  public onViewModelChanged(): void {
    this.setState({
      rowHeight: this.paohvisViewModel.rowHeight,
      hyperedgeColumnWidth: this.paohvisViewModel.hyperedgeColumnWidth,
      gapBetweenRanges: this.paohvisViewModel.gapBetweenRanges,
      data: this.paohvisViewModel.data,
      entityVertical: this.paohvisViewModel.entityVertical,
      entityHorizontal: this.paohvisViewModel.entityHorizontal,
      relation: this.paohvisViewModel.chosenRelation,
    });
  }
  public componentDidMount(): void {
    this.paohvisViewModel.attachView(this);
    this.paohvisViewModel.subscribeToSchemaResult();
    this.paohvisViewModel.subscribeToQueryResult();
    this.paohvisViewModel.setThisVisAsExportable();
  }
  public componentWillUnmount(): void {
    this.paohvisViewModel.detachView();
    this.paohvisViewModel.unSubscribeFromSchemaResult();
    this.paohvisViewModel.unSubscribeFromQueryResult();
  }

  render(): JSX.Element {
    const hyperEdgeRanges = this.state.data.hyperEdgeRanges;
    const rowLabelColumnWidth = this.state.data.maxRowLabelWidth;
    const hyperedgeColumnWidth = this.state.hyperedgeColumnWidth;

    //calculate yOffset
    let maxColWidth = 0;
    hyperEdgeRanges.forEach((hyperEdgeRange) => {
      const textLength = getWidthOfText(
        hyperEdgeRange.rangeText,
        styles.tableFontFamily,
        styles.tableFontSize,
        styles.tableFontWeight,
      );
      if (textLength > maxColWidth) maxColWidth = textLength;
    });
    const columnLabelAngleInRadians = Math.PI / 6;
    const yOffset = Math.sin(columnLabelAngleInRadians) * maxColWidth;
    const expandButtonWidth = parseFloat(visConfigPanelStyle.expandButtonSize.split('px')[0]);
    const margin = 5;

    //calc table width
    let tableWidth = 0;
    let tableWidthWithExtraColumnLabelWidth = 0;
    hyperEdgeRanges.forEach((hyperEdgeRange) => {
      const columnLabelWidth =
        Math.cos(columnLabelAngleInRadians) *
        getWidthOfText(
          hyperEdgeRange.rangeText,
          styles.tableFontFamily,
          styles.tableFontSize,
          styles.tableFontWeight,
        );
      const columnWidth =
        hyperEdgeRange.hyperEdges.length * hyperedgeColumnWidth + this.state.gapBetweenRanges * 3;

      tableWidth += columnWidth;

      if (columnLabelWidth > columnWidth) {
        const currentTableWidthWithLabel = tableWidth + columnLabelWidth;
        if (currentTableWidthWithLabel > tableWidthWithExtraColumnLabelWidth)
          tableWidthWithExtraColumnLabelWidth = currentTableWidthWithLabel;
      }
      if (tableWidth > tableWidthWithExtraColumnLabelWidth)
        tableWidthWithExtraColumnLabelWidth = tableWidth;
    });
    tableWidthWithExtraColumnLabelWidth += rowLabelColumnWidth + expandButtonWidth + margin;

    //make  all hyperEdgeRanges
    let colOffset = 0;
    let hyperEdgeRangeColumns: JSX.Element[] = [];
    hyperEdgeRanges.forEach((hyperEdgeRange, i) => {
      hyperEdgeRangeColumns.push(
        <HyperEdgeRange
          key={i}
          index={i}
          text={hyperEdgeRange.rangeText}
          hyperEdges={hyperEdgeRange.hyperEdges}
          nRows={this.state.data.rowLabels.length}
          colOffset={colOffset}
          xOffset={rowLabelColumnWidth}
          yOffset={yOffset}
          rowHeight={this.state.rowHeight}
          hyperedgeColumnWidth={hyperedgeColumnWidth}
          gapBetweenRanges={this.state.gapBetweenRanges}
          onMouseEnter={this.paohvisViewModel.onMouseEnterHyperEdge}
          onMouseLeave={this.paohvisViewModel.onMouseLeaveHyperEdge}
        />,
      );
      colOffset += hyperEdgeRange.hyperEdges.length;
    });

    //display message when there is no Paohvis table to display
    let tableMessage = <span></span>;
    let configPanelMessage = <div></div>;
    if (this.state.data.rowLabels.length == 0) {
      tableMessage = (
        <div id={styles.tableMessage}> Please choose a valid PAOHvis configuration </div>
      );
      configPanelMessage = (
        <div id={styles.configPanelMessage}> Please make a PAOHvis table first </div>
      );
    }

    // returns the whole PAOHvis visualisation panel
    return (
      <div className={styles.container}>
        <div className={styles.visContainer}>
          <div style={{ transform: 'scale(-1,1)' }}>
            <div
              style={{ width: '100%', height: '100%', overflow: 'auto' }}
              onMouseMove={this.paohvisViewModel.onMouseMoveToolTip}
            >
              <MakePaohvisMenu // render the MakePAOHvisMenu
                makePaohvisMenuViewModel={this.paohvisViewModel.makePaohvisMenuViewModel}
              />
              {tableMessage}
              <svg
                ref={this.svgRef}
                style={{
                  width: tableWidthWithExtraColumnLabelWidth,
                  height: yOffset + (this.state.data.rowLabels.length + 1) * this.state.rowHeight,
                }}
              >
                <RowLabelColumn // render the PAOHvis itself
                  onMouseEnter={this.paohvisViewModel.onMouseEnterRow}
                  onMouseLeave={this.paohvisViewModel.onMouseLeaveRow}
                  titles={this.state.data.rowLabels}
                  width={rowLabelColumnWidth}
                  rowHeight={this.state.rowHeight}
                  yOffset={yOffset}
                />
                {hyperEdgeRangeColumns}
              </svg>
              <Tooltip />
            </div>
          </div>
        </div>
        <VisConfigPanelComponent>
          {configPanelMessage}
          <PaohvisFilterComponent // render the PaohvisFilterComponent with all three different filter options
            paohvisFilterViewModel={this.paohvisViewModel.yAxisPaohvisFilterViewModel}
            axis={FilterType.yaxis}
          />
          <PaohvisFilterComponent
            paohvisFilterViewModel={this.paohvisViewModel.xAxisPaohvisFilterViewModel}
            axis={FilterType.xaxis}
          />
          <PaohvisFilterComponent
            paohvisFilterViewModel={this.paohvisViewModel.relationPaohvisFilterViewModel}
            axis={FilterType.edge}
          />
        </VisConfigPanelComponent>
      </div>
    );
  }
}
