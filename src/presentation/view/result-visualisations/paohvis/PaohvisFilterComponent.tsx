/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { ReactElement, useState } from 'react';
import { Button, MenuItem, TextField } from '@material-ui/core';
import styles from './PaohvisFilterComponent.module.scss';
import { FilterType } from '../../../../domain/entity/paohvis/structures/Types';
import BaseView from '../../BaseView';
import PaohvisFilterViewModel from '../../../view-model/result-visualisations/paohvis/PaohvisFilterViewModel';

/** The configuration fields for one Paohvis. */
type PaohvisFilterComponentProps = {
  paohvisFilterViewModel: PaohvisFilterViewModel;
  axis: FilterType;
};

type PaohvisFilterComponentState = {
  isTargetEntity: boolean;
  filterTarget: string;
  attributeNameAndType: string;
  predicate: string;
  compareValue: any;
  isFilterButtonEnabled: boolean;
  predicateTypeList: string[];
  attributeNamesOptions: string[];

  entityVertical: string;
  entityHorizontal: string;
  relation: string;
};

/** Component for rendering the filters for PAOHvis */
export default class PaohvisFilterComponent
  extends React.Component<PaohvisFilterComponentProps, PaohvisFilterComponentState>
  implements BaseView
{
  paohvisFilterViewModel: PaohvisFilterViewModel;

  constructor(props: PaohvisFilterComponentProps) {
    super(props);

    this.paohvisFilterViewModel = props.paohvisFilterViewModel;

    this.state = {
      isTargetEntity: true,
      filterTarget: '',
      attributeNameAndType: '',
      predicate: '',
      compareValue: '',
      isFilterButtonEnabled: false,
      predicateTypeList: [],
      attributeNamesOptions: [],

      entityVertical: '',
      entityHorizontal: '',
      relation: '',
    };
  }

  public componentDidMount(): void {
    this.paohvisFilterViewModel.attachView(this);
    this.paohvisFilterViewModel.subscribeToBroker();
  }

  public componentWillUnmount(): void {
    this.paohvisFilterViewModel.detachView();
    this.paohvisFilterViewModel.unSubscribeFromBroker();
  }

  onViewModelChanged(): void {
    this.setState({
      isTargetEntity: this.paohvisFilterViewModel.isTargetEntity,
      filterTarget: this.paohvisFilterViewModel.filterTarget,
      attributeNameAndType: this.paohvisFilterViewModel.attributeNameAndType,
      predicate: this.paohvisFilterViewModel.predicate,
      compareValue: this.paohvisFilterViewModel.compareValue,
      isFilterButtonEnabled: this.paohvisFilterViewModel.isFilterButtonEnabled,
      predicateTypeList: this.paohvisFilterViewModel.predicateTypeList,
      attributeNamesOptions: this.paohvisFilterViewModel.attributeNamesOptions,
    });
  }

  render(): ReactElement {
    const attributeNamesOptions = this.state.attributeNamesOptions;

    // Check if the given entity or relation is in the queryResult
    let attributeNameMenuItems: ReactElement[] = [];
    if (attributeNamesOptions.length > 0) {
      attributeNameMenuItems = attributeNamesOptions.map((attributeNameAndType) => {
        const attributeName = attributeNameAndType.split(':')[0];
        return (
          <MenuItem key={attributeName} value={attributeNameAndType}>
            {attributeNameAndType}
          </MenuItem>
        );
      });
    } else
      attributeNameMenuItems = [
        <MenuItem key="placeholder" value="" disabled>
          First select an entity/relation with one or more attributes
        </MenuItem>,
      ];

    let predicateTypeList: ReactElement[] = [];
    if (this.state.predicateTypeList.length > 0) {
      predicateTypeList = this.state.predicateTypeList.map((predicate) => (
        <MenuItem key={predicate} value={predicate}>
          {predicate}
        </MenuItem>
      ));
    } else
      predicateTypeList = [
        <MenuItem key="placeholder" value="" disabled>
          First select an attribute
        </MenuItem>,
      ];

    return (
      <div>
        <div className={styles.container}>
          <p className={styles.title}>PAOHVis filters{this.props.axis}:</p>
          <div className={styles.selectContainer}>
            <p className={styles.subtitle}>{this.state.filterTarget}</p>
            <div style={{ padding: 10, display: 'block', width: '100%' }}>
              <TextField
                select
                id="standard-select-entity"
                style={{ minWidth: 120, marginRight: 20 }}
                label="Attribute"
                value={this.state.attributeNameAndType}
                onChange={this.paohvisFilterViewModel.onChangeAttributeName}
              >
                {attributeNameMenuItems}
              </TextField>
              <TextField
                select
                id="standard-select-relation"
                style={{ minWidth: 120, marginRight: 20 }}
                label="Relation"
                value={this.state.predicate}
                onChange={this.paohvisFilterViewModel.onChangePredicate}
              >
                {predicateTypeList}
              </TextField>
              <TextField
                id="standard-select-relation"
                style={{ minWidth: 120, marginRight: 20 }}
                label="Value"
                value={this.state.compareValue}
                onChange={this.paohvisFilterViewModel.onChangeCompareValue}
              >
                {}
              </TextField>

              <div style={{ height: 40, paddingTop: 10, marginBottom: 10 }}>
                <Button
                  variant="contained"
                  color="default"
                  disabled={!this.state.isFilterButtonEnabled}
                  onClick={this.paohvisFilterViewModel.onClickFilterPaohvisButton}
                >
                  <span style={{ fontWeight: 'bold' }}>Apply filter</span>
                </Button>
              </div>
              <div style={{ height: 40, marginBottom: 20 }}>
                <Button
                  variant="contained"
                  color="default"
                  onClick={this.paohvisFilterViewModel.onClickResetFilter}
                >
                  <span style={{ fontWeight: 'bold' }}>Reset filter</span>
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
