/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import React from 'react';
import * as d3 from 'd3-v6';

type RowLabelColumnProps = {
  onMouseEnter: (row: number) => void;
  onMouseLeave: (row: number) => void;

  titles: string[];
  width: number;
  rowHeight: number;
  yOffset: number;
};
export default class RowLabelColumn extends React.Component<RowLabelColumnProps> {
  render() {
    const titleRows = this.props.titles.map((title, i) => (
      <RowLabel
        key={i}
        index={i}
        title={title}
        width={this.props.width}
        rowHeight={this.props.rowHeight}
        yOffset={this.props.yOffset}
        onMouseEnter={this.props.onMouseEnter}
        onMouseLeave={this.props.onMouseLeave}
      />
    ));

    return <g>{titleRows}</g>;
  }
}

type RowLabelProps = {
  onMouseEnter: (row: number) => void;
  onMouseLeave: (row: number) => void;

  title: string;
  width: number;
  rowHeight: number;
  index: number;
  yOffset: number;
};

class RowLabel extends React.Component<RowLabelProps> {
  ref = React.createRef<SVGGElement>();

  constructor(props: RowLabelProps) {
    super(props);
    this.ref = React.createRef<SVGRectElement>();
  }

  public componentDidMount(): void {
    d3.select(this.ref.current)
      .on('mouseover', () => this.props.onMouseEnter(this.props.index))
      .on('mouseout', () => this.props.onMouseLeave(this.props.index));
  }

  render() {
    return (
      <g
        ref={this.ref}
        className={'row-' + this.props.index}
        transform={
          'translate(0,' +
          (this.props.yOffset + this.props.rowHeight + this.props.index * this.props.rowHeight) +
          ')'
        }
      >
        <rect
          width={this.props.width}
          height={this.props.rowHeight}
          fill={this.props.index % 2 === 0 ? '#e6e6e6' : '#f5f5f5'}
        ></rect>
        <text x="10" y={this.props.rowHeight / 2} dy=".35em" style={{ fontWeight: 600 }}>
          {this.props.title}
        </text>
      </g>
    );
  }
}
