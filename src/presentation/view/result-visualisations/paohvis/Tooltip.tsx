/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import React from 'react';
import * as d3 from 'd3-v6';
import './Tooltip.scss';

export default class Tooltip extends React.Component {
  onMouseOver(e: any) {
    d3.select('.tooltip')
      .style('top', d3.pointer(e)[1])
      .style('left', d3.pointer(e)[0] + 5);
  }

  render() {
    return (
      <div style={{ position: 'absolute' }} className="tooltip">
        {' '}
      </div>
    );
  }
}
