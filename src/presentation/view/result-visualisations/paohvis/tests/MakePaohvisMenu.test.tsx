/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import {
  queryResultParliament,
  queryResultFlights,
} from '../../../../../data/mock-data/paohvis/mockInputData';
import SecndChamberSchemaMock from '../../../../../data/mock-data/schema-result/2ndChamberSchemaMock';
import SecndChamberQueryResultMock from '../../../../../data/mock-data/query-result/big2ndChamberQueryResult';
import {
  animalData,
  parliamentData2,
  flights,
} from '../../../../../data/mock-data/paohvis/mockSchemaResult';
import MakePaohvisMenu from '../MakePaohvisMenu';
import {
  EntitiesFromSchema,
  RelationsFromSchema,
} from '../../../../../domain/entity/paohvis/structures/Types';
import CalcEntityAttrAndRelNamesFromSchemaUseCase from '../../../../../domain/usecases/paohvis/CalcEntityAttrAndRelNamesFromSchemaUseCase';
import { isSchemaResult } from '../../../../../domain/entity/graph-schema/structures/SchemaResultType';
import {
  isNodeLinkResult,
  NodeLinkResultType,
} from '../../../../../domain/entity/query-result/structures/NodeLinkResultType';
import PaohvisViewModelImpl from '../../../../view-model/result-visualisations/paohvis/PaohvisViewModelImpl';

let entitiesFromSchema: EntitiesFromSchema = {
  entityNames: [],
  attributesPerEntity: {},
  relationsPerEntity: {},
};
let relationsFromSchema: RelationsFromSchema = {
  relationCollection: [],
  relationNames: {},
  attributesPerRelation: {},
};
let entitiesFromQueryResult: string[] = [];

describe('MakePaohvisMenu', () =>
  it('should return the names of all relations from the query result that are connected with the chosen entity for asymmetric relations', () => {
    if (isSchemaResult(parliamentData2)) {
      entitiesFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesAndRelations(
        parliamentData2,
      );
      relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
        parliamentData2,
      );
    }
    if (isNodeLinkResult(queryResultParliament)) {
      entitiesFromQueryResult = PaohvisViewModelImpl.calcEntitiesFromQueryResult(
        queryResultParliament,
      );
    }
    let makePaohvisMenu: MakePaohvisMenu;
    makePaohvisMenu = new MakePaohvisMenu({
      entitiesFromSchema: entitiesFromSchema,
      relationsFromSchema: relationsFromSchema,
      queryResult: queryResultParliament,
      entitiesFromQueryResult: entitiesFromQueryResult,
      makePaohvis() {},
    });

    // This event contains the name of the entity that is on the left side (from) of the relation to which it is connected
    let event = {
      target: {
        value: 'parliament',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenu.entityChanged(event);

    expect(makePaohvisMenu.relationNameOptions).toEqual(['part_of', 'member_of']);

    // This event contains the name of the entity that is on the right side (to) of the relation to which it is connected
    event = {
      target: {
        value: 'parties',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenu.entityChanged(event);

    expect(makePaohvisMenu.relationNameOptions).toEqual(['member_of']);
  }));

describe('MakePaohvisMenu', () =>
  it('should return the names of all relations from the query result that are connected with the chosen entity for symmetric relations', () => {
    if (isSchemaResult(flights)) {
      entitiesFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesAndRelations(
        flights,
      );
      relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
        flights,
      );
    }
    if (isNodeLinkResult(queryResultFlights)) {
      entitiesFromQueryResult = PaohvisViewModelImpl.calcEntitiesFromQueryResult(
        queryResultFlights,
      );
    }
    let makePaohvisMenu: MakePaohvisMenu;
    makePaohvisMenu = new MakePaohvisMenu({
      entitiesFromSchema: entitiesFromSchema,
      relationsFromSchema: relationsFromSchema,
      queryResult: queryResultFlights,
      entitiesFromQueryResult: entitiesFromQueryResult,
      makePaohvis() {},
    });

    // This event contains the name of the entity that is on the left side (from) of the relation to which it is connected
    let event = {
      target: {
        value: 'airports',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenu.entityChanged(event);

    expect(makePaohvisMenu.relationNameOptions).toEqual(['flights:from', 'flights:to']);
  }));

// TODO: need to fix this test
// describe('MakePaohvisMenu', () =>
//   it('should return the names of all attributes from the chosen entity counterpart for symmetric relations + the attributes of the chosen relation', () => {
//     if (isSchemaResult(parliamentData2)) {
//       entitiesFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesAndRelations(
//         parliamentData2,
//       );
//       relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
//         parliamentData2,
//       );
//     }
//     if (isNodeLinkResult(queryResultParliament)) {
//       entitiesFromQueryResult = PaohvisViewModelImpl.calcEntitiesFromQueryResult(
//         queryResultParliament,
//       );
//     }
//     let makePaohvisMenu: MakePaohvisMenu;
//     makePaohvisMenu = new MakePaohvisMenu({
//       entitiesFromSchema: entitiesFromSchema,
//       relationsFromSchema: relationsFromSchema,
//       queryResult: queryResultParliament,
//       entitiesFromQueryResult: entitiesFromQueryResult,
//       makePaohvis() {},
//     });

//     let event = {
//       target: {
//         value: 'parliament',
//       },
//     } as React.ChangeEvent<HTMLInputElement>;
//     makePaohvisMenu.entityChanged(event);
//     // This event contains the relation of which the chosen entity is the left part (from)
//     event = {
//       target: {
//         value: 'member_of',
//       },
//     } as React.ChangeEvent<HTMLInputElement>;
//     makePaohvisMenu.relationNameChanged(event);

//     expect(makePaohvisMenu.attributeNameOptions['No attribute']).toEqual(['No attribute']);
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('chairmanId');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('country');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('name');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('seats');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('chairman');
//     expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('isChairman');
//     expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('amountOfTimes');

//     // This event contains the name of the entity that is on the right side (to) of the relation to which it is connected
//     event = {
//       target: {
//         value: 'parties',
//       },
//     } as React.ChangeEvent<HTMLInputElement>;
//     makePaohvisMenu.entityChanged(event);

//     event = {
//       target: {
//         value: 'member_of',
//       },
//     } as React.ChangeEvent<HTMLInputElement>;
//     makePaohvisMenu.relationNameChanged(event);
//     expect(makePaohvisMenu.attributeNameOptions['No attribute']).toEqual(['No attribute']);
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('age');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('img');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('party');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('residence');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('seniority');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('name');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('weight');
//     expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('isChairman');
//     expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('isChairman');
//     expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('amountOfTimes');
//   }));

describe('MakePaohvisMenu', () =>
  it('should return the names of all attributes from the chosen entity counterpart for symmetric relations + the attributes of the chosen relation', () => {
    if (isSchemaResult(flights)) {
      entitiesFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesAndRelations(
        flights,
      );
      relationsFromSchema = CalcEntityAttrAndRelNamesFromSchemaUseCase.calculateAttributesFromRelation(
        flights,
      );
    }
    if (isNodeLinkResult(queryResultFlights)) {
      entitiesFromQueryResult = PaohvisViewModelImpl.calcEntitiesFromQueryResult(
        queryResultFlights,
      );
    }
    let makePaohvisMenu: MakePaohvisMenu;
    makePaohvisMenu = new MakePaohvisMenu({
      entitiesFromSchema: entitiesFromSchema,
      relationsFromSchema: relationsFromSchema,
      queryResult: queryResultFlights,
      entitiesFromQueryResult: entitiesFromQueryResult,
      makePaohvis() {},
    });

    let event = {
      target: {
        value: 'airports',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenu.entityChanged(event);
    // This event contains the relation of which the chosen entity is the left part (from)
    event = {
      target: {
        value: 'flights:from',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenu.relationNameChanged(event);
    expect(makePaohvisMenu.attributeNameOptions['No attribute']).toContain('No attribute');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('city');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('country');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('lat');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('long');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('name');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('state');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('vip');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('ArrTime');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('ArrTimeUTC');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Day');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('DayOfWeek');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('DepTime');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('DepTimeUTC');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Distance');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('FlightNum');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Month');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('TailNum');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('UniqueCarrier');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Year');

    // This event contains the relation of which the chosen entity is the right part (to)
    event = {
      target: {
        value: 'flights:to',
      },
    } as React.ChangeEvent<HTMLInputElement>;
    makePaohvisMenu.relationNameChanged(event);
    expect(makePaohvisMenu.attributeNameOptions['No attribute']).toContain('No attribute');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('city');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('country');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('lat');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('long');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('name');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('state');
    expect(makePaohvisMenu.attributeNameOptions['Entity']).toContain('vip');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('ArrTime');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('ArrTimeUTC');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Day');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('DayOfWeek');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('DepTime');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('DepTimeUTC');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Distance');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('FlightNum');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Month');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('TailNum');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('UniqueCarrier');
    expect(makePaohvisMenu.attributeNameOptions['Relation']).toContain('Year');
  }));
