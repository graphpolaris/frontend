/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { ReactElement } from 'react';
import ReactJson from 'react-json-view';
import RawJSONViewModel from '../../../view-model/result-visualisations/raw-json/RawJSONViewModel';
import BaseView from '../../BaseView';

/** RawJSONProps is an interface containing the RawJSONViewModel. */
export interface RawJSONProps {
  rawJSONViewModel: RawJSONViewModel;
}

/** RawJSONComponentState is an interface containing the queryResult. */
export interface RawJSONComponentState {
  queryResult: any;
}

/** Renders the query result object as json. Using react-json-view. */
export default class RawJSONComponent
  extends React.Component<RawJSONProps, RawJSONComponentState>
  implements BaseView
{
  private rawJSONViewModel: RawJSONViewModel;

  public constructor(props: RawJSONProps) {
    super(props);

    this.rawJSONViewModel = props.rawJSONViewModel;

    this.state = {
      queryResult: props.rawJSONViewModel.queryResult,
    };
  }

  /** The componenDidMount function is called after the site is loaded and the component attached. */
  public componentDidMount(): void {
    this.rawJSONViewModel.attachView(this);
    this.rawJSONViewModel.subscribeToQueryResult();
    this.rawJSONViewModel.setThisVisAsExportable();
  }

  /** The componentWillUnmount function is called when the site is closed to remove the components. */
  public componentWillUnmount(): void {
    this.rawJSONViewModel.detachView();
    this.rawJSONViewModel.unSubscribeFromQueryResult();
  }

  /** The onViewModelChanged function sets the queryResult. */
  public onViewModelChanged(): void {
    this.setState({
      queryResult: this.rawJSONViewModel.queryResult,
    });
  }

  /** Returns the render component. */
  render(): ReactElement {
    return (
      <div style={{ overflowY: 'auto' }}>
        <div
          style={{
            marginTop: '40px',
            paddingLeft: '30px',
          }}
        >
          <ReactJson
            src={this.state.queryResult}
            collapsed={1}
            quotesOnKeys={false}
            displayDataTypes={false}
          />
        </div>
      </div>
    );
  }
}
