/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';

/** The style for the component. */
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:hover': {
        '& .display': {
          '& $background': {
            fill: '#009100',
            transitionDelay: '0s',
          },
          '& $plus': {
            transform: 'translate(-4px,0)',
            transitionDelay: '0s',

            '& line': {
              transition: '.2s',
              transitionDelay: '0s',
              strokeWidth: 3,
            },
            '& $xAxis': {
              transform: 'translate(0,0) scale(1,1)',
            },
            '& $yAxis': {
              transform: 'translate(0,0)',
            },
            '& .nodes': {
              opacity: 0,
              transitionDelay: '.0s',
            },
          },
        },
      },
    },

    background: {
      transition: '.1s',
      transitionDelay: '.1s',
    },

    plus: {
      transformBox: 'fill-box',
      transformOrigin: 'center center',
      transition: '.2s',
      transitionDelay: '.1s',
      transform: 'translate(0,0)',

      '& line': {
        transition: '.2s',
        transitionDelay: '.1s',
        strokeWidth: 1,
      },

      '& .nodes': {
        transitionDelay: '.1s',
      },
    },

    xAxis: {
      transformBox: 'fill-box',
      transformOrigin: 'center right',
      transform: 'translate(0,5px) scale(1.4,1)',
    },
    yAxis: {
      transform: 'translate(-11px,0px)',
    },
  }),
);

/**
 * Contains a component that renders the "add plot" button with SVG elements for semantic substrates.
 * @param props The x and y, and an on click event function.
 */
export default function AddPlotButtonComponent(props: {
  x?: number;
  y?: number;
  onClick(event: React.MouseEvent<SVGRectElement>): void;
}) {
  const classes = useStyles();

  return (
    <g transform={`translate(${props.x || 0},${props.y || 0})`} className={classes.root}>
      <g className={'display'}>
        <rect
          className={classes.background}
          x="5"
          y="1"
          width="108"
          height="29"
          rx="3"
          fill="green"
        />
        <path
          d="M20.986 18.264H17.318L16.73 20H14.224L17.78 10.172H20.552L24.108 20H21.574L20.986 18.264ZM20.37 16.416L19.152 12.818L17.948 16.416H20.37ZM24.7143 16.08C24.7143 15.2773 24.8636 14.5727 25.1623 13.966C25.4703 13.3593 25.8856 12.8927 26.4083 12.566C26.9309 12.2393 27.5143 12.076 28.1583 12.076C28.6716 12.076 29.1383 12.1833 29.5583 12.398C29.9876 12.6127 30.3236 12.902 30.5663 13.266V9.64H32.9603V20H30.5663V18.88C30.3423 19.2533 30.0203 19.552 29.6003 19.776C29.1896 20 28.7089 20.112 28.1583 20.112C27.5143 20.112 26.9309 19.9487 26.4083 19.622C25.8856 19.286 25.4703 18.8147 25.1623 18.208C24.8636 17.592 24.7143 16.8827 24.7143 16.08ZM30.5663 16.094C30.5663 15.4967 30.3983 15.0253 30.0623 14.68C29.7356 14.3347 29.3343 14.162 28.8583 14.162C28.3823 14.162 27.9763 14.3347 27.6403 14.68C27.3136 15.016 27.1503 15.4827 27.1503 16.08C27.1503 16.6773 27.3136 17.1533 27.6403 17.508C27.9763 17.8533 28.3823 18.026 28.8583 18.026C29.3343 18.026 29.7356 17.8533 30.0623 17.508C30.3983 17.1627 30.5663 16.6913 30.5663 16.094ZM34.2162 16.08C34.2162 15.2773 34.3656 14.5727 34.6642 13.966C34.9722 13.3593 35.3876 12.8927 35.9102 12.566C36.4329 12.2393 37.0162 12.076 37.6602 12.076C38.1736 12.076 38.6402 12.1833 39.0602 12.398C39.4896 12.6127 39.8256 12.902 40.0682 13.266V9.64H42.4622V20H40.0682V18.88C39.8442 19.2533 39.5222 19.552 39.1022 19.776C38.6916 20 38.2109 20.112 37.6602 20.112C37.0162 20.112 36.4329 19.9487 35.9102 19.622C35.3876 19.286 34.9722 18.8147 34.6642 18.208C34.3656 17.592 34.2162 16.8827 34.2162 16.08ZM40.0682 16.094C40.0682 15.4967 39.9002 15.0253 39.5642 14.68C39.2376 14.3347 38.8362 14.162 38.3602 14.162C37.8842 14.162 37.4782 14.3347 37.1422 14.68C36.8156 15.016 36.6522 15.4827 36.6522 16.08C36.6522 16.6773 36.8156 17.1533 37.1422 17.508C37.4782 17.8533 37.8842 18.026 38.3602 18.026C38.8362 18.026 39.2376 17.8533 39.5642 17.508C39.9002 17.1627 40.0682 16.6913 40.0682 16.094ZM49.555 13.294C49.7883 12.93 50.1103 12.636 50.521 12.412C50.9316 12.188 51.4123 12.076 51.963 12.076C52.607 12.076 53.1903 12.2393 53.713 12.566C54.2356 12.8927 54.6463 13.3593 54.945 13.966C55.253 14.5727 55.407 15.2773 55.407 16.08C55.407 16.8827 55.253 17.592 54.945 18.208C54.6463 18.8147 54.2356 19.286 53.713 19.622C53.1903 19.9487 52.607 20.112 51.963 20.112C51.4216 20.112 50.941 20 50.521 19.776C50.1103 19.552 49.7883 19.2627 49.555 18.908V23.724H47.161V12.188H49.555V13.294ZM52.971 16.08C52.971 15.4827 52.803 15.016 52.467 14.68C52.1403 14.3347 51.7343 14.162 51.249 14.162C50.773 14.162 50.367 14.3347 50.031 14.68C49.7043 15.0253 49.541 15.4967 49.541 16.094C49.541 16.6913 49.7043 17.1627 50.031 17.508C50.367 17.8533 50.773 18.026 51.249 18.026C51.725 18.026 52.131 17.8533 52.467 17.508C52.803 17.1533 52.971 16.6773 52.971 16.08ZM59.0569 9.64V20H56.6629V9.64H59.0569ZM64.3478 20.112C63.5825 20.112 62.8918 19.9487 62.2758 19.622C61.6692 19.2953 61.1885 18.8287 60.8338 18.222C60.4885 17.6153 60.3158 16.906 60.3158 16.094C60.3158 15.2913 60.4932 14.5867 60.8478 13.98C61.2025 13.364 61.6878 12.8927 62.3038 12.566C62.9198 12.2393 63.6105 12.076 64.3758 12.076C65.1412 12.076 65.8318 12.2393 66.4478 12.566C67.0638 12.8927 67.5492 13.364 67.9038 13.98C68.2585 14.5867 68.4358 15.2913 68.4358 16.094C68.4358 16.8967 68.2538 17.606 67.8898 18.222C67.5352 18.8287 67.0452 19.2953 66.4198 19.622C65.8038 19.9487 65.1132 20.112 64.3478 20.112ZM64.3478 18.04C64.8052 18.04 65.1925 17.872 65.5098 17.536C65.8365 17.2 65.9998 16.7193 65.9998 16.094C65.9998 15.4687 65.8412 14.988 65.5238 14.652C65.2158 14.316 64.8332 14.148 64.3758 14.148C63.9092 14.148 63.5218 14.316 63.2138 14.652C62.9058 14.9787 62.7518 15.4593 62.7518 16.094C62.7518 16.7193 62.9012 17.2 63.1998 17.536C63.5078 17.872 63.8905 18.04 64.3478 18.04ZM74.0599 17.97V20H72.8419C71.9739 20 71.2972 19.79 70.8119 19.37C70.3266 18.9407 70.0839 18.2453 70.0839 17.284V14.176H69.1319V12.188H70.0839V10.284H72.4779V12.188H74.0459V14.176H72.4779V17.312C72.4779 17.5453 72.5339 17.7133 72.6459 17.816C72.7579 17.9187 72.9446 17.97 73.2059 17.97H74.0599Z"
          fill="white"
        />
        <g className={classes.plus}>
          <line
            className={classes.xAxis}
            x1="103.987"
            y1="15.6278"
            x2="88.9872"
            y2="15.5"
            stroke="white"
            strokeWidth="3"
          />
          <line
            className={classes.yAxis}
            x1="96.5"
            y1="8"
            x2="96.5"
            y2="23"
            stroke="white"
            strokeWidth="3"
          />
          <g className={'nodes'}>
            <circle cx="89" cy="16" r="1" fill="white" />
            <circle cx="94" cy="14" r="1" fill="white" />
            <circle cx="99" cy="14" r="1" fill="white" />
          </g>
        </g>
      </g>
      <g>
        <rect
          x="5"
          y="1"
          width="108"
          height="29"
          rx="3"
          fill="transparent"
          onClick={(e) => props.onClick(e)}
        />
      </g>
    </g>
  );
}
