/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import * as d3 from 'd3-v6';
import React, { ReactElement } from 'react';
import {
  AxisLabel,
  MinMaxType,
} from '../../../../domain/entity/semantic-substrates/structures/Types';

/** The variables in the props of the brush component*/
type BrushProps = {
  width: number;
  height: number;

  xAxisDomain: MinMaxType;
  yAxisDomain: MinMaxType;

  xAxisLabel: AxisLabel;
  yAxisLabel: AxisLabel;

  onBrush(xRange: MinMaxType, yRange: MinMaxType): void;
};

/** The variables in the state of the brush component*/
type BrushState = {
  xBrushRange: MinMaxType;
  yBrushRange: MinMaxType;
  brushActive: boolean;
};

/**
 * Takes care of the brush (square selection) functionality for a plot.
 * Also renders the x y selections ticks with their values.
 */
export default class BrushComponent extends React.Component<BrushProps, BrushState> {
  ref = React.createRef<SVGGElement>();

  xAxisScale: d3.ScaleLinear<number, number, never> = d3.scaleLinear();
  yAxisScale: d3.ScaleLinear<number, number, never> = d3.scaleLinear();

  constructor(props: BrushProps) {
    super(props);
    this.state = {
      xBrushRange: { min: 0, max: this.props.width },
      yBrushRange: { min: 0, max: this.props.height },
      brushActive: false,
    };

    this.updateXYAxisScaleFunctions();
  }

  public componentDidMount(): void {
    const d3Selected = d3.select(this.ref.current);

    // Add square selection tool to this plot with the width and height. Also called brush.
    d3Selected.append('g').call(
      d3
        .brush()
        .extent([
          [0, 0],
          [this.props.width, this.props.height],
        ])
        .on('brush', (e) => {
          this.onBrush(e.selection);
        })
        .on('end', (e) => {
          this.onBrushEnd(e.selection);
        }),
    );
  }

  public componentDidUpdate(prevProps: BrushProps): void {
    if (
      this.props.xAxisDomain != prevProps.xAxisDomain ||
      this.props.yAxisDomain != prevProps.yAxisDomain
    ) {
      this.updateXYAxisScaleFunctions();
    }
  }

  /** Updates the X Y axis scale for the plots */
  private updateXYAxisScaleFunctions(): void {
    // Create the x axis scale funtion with d3
    const xAxisDomain = [this.props.xAxisDomain.min, this.props.xAxisDomain.max];
    this.xAxisScale = d3.scaleLinear().domain(xAxisDomain).range([0, this.props.width]);

    // Create the y axis scale funtion with d3
    const yAxisDomain = [this.props.yAxisDomain.max, this.props.yAxisDomain.min];
    this.yAxisScale = d3.scaleLinear().domain(yAxisDomain).range([0, this.props.height]);
  }

  /**
   * Called when brushing. Brush will be displayed and the props callback gets called.
   * @param {number[][]} selection A 2D array where the first row is the topleft coordinates and the second bottomright.
   */
  private onBrush(selection: number[][] | undefined): void {
    if (selection != undefined) {
      // Set the state with the inverted selection, this will give us the original positions, (the values)
      const newState: BrushState = {
        xBrushRange: {
          min: this.xAxisScale.invert(selection[0][0]),
          max: this.xAxisScale.invert(selection[1][0]),
        },
        yBrushRange: {
          min: this.yAxisScale.invert(selection[1][1]),
          max: this.yAxisScale.invert(selection[0][1]),
        },
        brushActive: true,
      };
      this.setState(newState);

      // Add event when brushing, call the onBrush with start and end positions
      this.props.onBrush(newState.xBrushRange, newState.yBrushRange);
    }
  }

  /**
   * Called when brush ends. If the selection was empty reset selection to cover whole plot and notify viewmodel.
   * @param {number[][]} selection A 2D array where the first row is the topleft coordinates and the second bottomright.
   */
  private onBrushEnd(selection: number[][] | undefined): void {
    if (selection == undefined) {
      // Set the state with the inverted selection, this will give us the original positions, (the values)
      const newState: BrushState = {
        xBrushRange: {
          min: this.xAxisScale.invert(0),
          max: this.xAxisScale.invert(this.props.width),
        },
        yBrushRange: {
          min: this.yAxisScale.invert(this.props.height),
          max: this.yAxisScale.invert(0),
        },
        brushActive: false,
      };
      this.setState(newState);

      // Add event when brushing, call the onBrush with start and end positions
      this.props.onBrush(newState.xBrushRange, newState.yBrushRange);
    }
  }

  /**
   * Renders the X axis brush ticks depending on the brushActive state.
   * @param xRangePositions The x positions of the brush ticks.
   */
  private renderXAxisBrushTicks(xRangePositions: MinMaxType): ReactElement {
    if (this.state.brushActive && this.props.xAxisLabel != AxisLabel.evenlySpaced) {
      return (
        <g>
          <line
            x1={xRangePositions.min}
            y1={this.props.height - 5}
            x2={xRangePositions.min}
            y2={this.props.height}
            stroke={'red'}
            strokeWidth={1}
            fill={'transparent'}
          />
          <text x={xRangePositions.min} y={this.props.height - 8} textAnchor="middle" fontSize={10}>
            {+this.state.xBrushRange.min.toFixed(2)}
          </text>
          <line
            x1={xRangePositions.max}
            y1={this.props.height - 5}
            x2={xRangePositions.max}
            y2={this.props.height}
            stroke={'red'}
            strokeWidth={1}
            fill={'transparent'}
          />
          <text x={xRangePositions.max} y={this.props.height - 8} textAnchor="middle" fontSize={10}>
            {+this.state.xBrushRange.max.toFixed(2)}
          </text>
        </g>
      );
    }

    return <></>;
  }

  /**
   * Renders the Y axis brush ticks depending on the brushActive state.
   * @param yRangePositions The y positions of the brush ticks.
   */
  private renderYAxisBrushTicks(yRangePositions: MinMaxType): ReactElement {
    if (this.state.brushActive && this.props.yAxisLabel != AxisLabel.evenlySpaced) {
      return (
        <g>
          <line
            x1={0}
            y1={yRangePositions.min}
            x2={5}
            y2={yRangePositions.min}
            stroke={'red'}
            strokeWidth={1}
            fill={'transparent'}
          />
          <text x={8} y={yRangePositions.min + 3} fontSize={10}>
            {+this.state.yBrushRange.min.toFixed(2)}
          </text>
          <line
            x1={0}
            y1={yRangePositions.max}
            x2={5}
            y2={yRangePositions.max}
            stroke={'red'}
            strokeWidth={1}
            fill={'transparent'}
          />
          <text x={8} y={yRangePositions.max + 3} fontSize={10}>
            {+this.state.yBrushRange.max.toFixed(2)}
          </text>
        </g>
      );
    }

    return <></>;
  }

  public render(): ReactElement {
    // Scale the brush to the scaled range
    // Used for rendering the positions of the x and y axis brush ticks
    const xRangePositions: MinMaxType = {
      min: this.xAxisScale(this.state.xBrushRange.min),
      max: this.xAxisScale(this.state.xBrushRange.max),
    };
    const yRangePositions: MinMaxType = {
      min: this.yAxisScale(this.state.yBrushRange.min),
      max: this.yAxisScale(this.state.yBrushRange.max),
    };

    return (
      <g ref={this.ref}>
        {this.renderXAxisBrushTicks(xRangePositions)}
        {this.renderYAxisBrushTicks(yRangePositions)}
      </g>
    );
  }
}
