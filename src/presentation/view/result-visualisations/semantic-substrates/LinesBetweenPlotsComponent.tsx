/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { ReactElement } from 'react';
import {
  NodeType,
  PlotType,
  RelationType,
} from '../../../../domain/entity/semantic-substrates/structures/Types';
import { Position } from '../../../../domain/entity/utils/Position';
import CalcConnectionLinePositionsUseCase from '../../../../domain/usecases/semantic-substrates/CalcConnectionLinePositionsUseCase';

/** Props of the lines between the plots component */
type LinesBetweenPlotsProps = {
  fromPlot: PlotType;
  toPlot: PlotType;
  relations: RelationType[];
  nodeRadius: number;
  color: string;
};
/** Renders all the connection lines between nodes from two plots. */
export default class LinesBetweenPlots extends React.Component<LinesBetweenPlotsProps> {
  render(): JSX.Element {
    const { fromPlot, toPlot, relations, nodeRadius, color } = this.props;

    // The JSX elements to render a connection line with arrow
    const lines = relations.map((relation) => (
      <LineBetweenNodesComponent
        key={
          fromPlot.nodes[relation.fromIndex].data.text + toPlot.nodes[relation.toIndex].data.text
        }
        fromNode={fromPlot.nodes[relation.fromIndex]}
        fromPlotYOffset={fromPlot.yOffset}
        toNode={toPlot.nodes[relation.toIndex]}
        toPlotYOffset={toPlot.yOffset}
        width={relation.value ?? 1}
        nodeRadius={nodeRadius}
        color={relation.colour ?? color}
      />
    ));

    return <g>{lines}</g>;
  }
}

/** Props of the nodes between the plots component. */
type LineBetweenNodesProps = {
  fromNode: NodeType;
  fromPlotYOffset: number;

  toNode: NodeType;
  toPlotYOffset: number;

  width: number;
  color: string;
  nodeRadius: number;
};
/** React component for drawing a single connectionline between nodes for semantic substrates. */
class LineBetweenNodesComponent extends React.Component<LineBetweenNodesProps> {
  render(): ReactElement {
    const { fromNode, toNode, fromPlotYOffset, toPlotYOffset, width, color, nodeRadius } =
      this.props;

    // The start and end position with their plot offset
    const startNodePos: Position = {
      x: fromNode.scaledPosition.x,
      y: fromNode.scaledPosition.y + fromPlotYOffset,
    };

    const endNodePos: Position = {
      x: toNode.scaledPosition.x,
      y: toNode.scaledPosition.y + toPlotYOffset,
    };

    // Get the positions to draw the arrow
    const { start, end, controlPoint, arrowRStart, arrowLStart } =
      CalcConnectionLinePositionsUseCase.calculatePositions(startNodePos, endNodePos, nodeRadius);

    // Create the curved line path
    const path = `M ${start.x} ${start.y} Q ${controlPoint.x} ${controlPoint.y} ${end.x} ${end.y}`;

    return (
      <g pointerEvents={'none'}>
        <path d={path} stroke={color} strokeWidth={width} fill="transparent" />
        <line
          x1={arrowRStart.x}
          y1={arrowRStart.y}
          x2={end.x}
          y2={end.y}
          stroke={color}
          strokeWidth={width}
          fill="transparent"
        />
        <line
          x1={arrowLStart.x}
          y1={arrowLStart.y}
          x2={end.x}
          y2={end.y}
          stroke={color}
          strokeWidth={width}
          fill="transparent"
        />
      </g>
    );
  }
}
