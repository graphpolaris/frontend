/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { Button, MenuItem, Popover, TextField } from '@material-ui/core';
import React, { ReactElement } from 'react';
import { Node } from '../../../../../domain/entity/query-result/structures/NodeLinkResultType';
import { EntitiesFromSchema } from '../../../../../domain/entity/semantic-substrates/structures/Types';
import OptimizedAutocomplete from './OptimizedAutocomplete';

/** The typing for the props of the plot popups */
type AddPlotPopupProps = {
  anchorEl: Element | null;
  open: boolean;
  entitiesFromSchema: EntitiesFromSchema;
  nodeLinkResultNodes: Node[];

  // Called when the the popup should close.
  handleClose(): void;
  // Called when the has filled in the required field and pressed the "add" button.
  addPlot(entity: string, attributeName: string, attributeValue: string): void;
};

/** The variables in the state of the plot popups */
type AddPlotPopupState = {
  entity: string;
  attributeName: string;
  isButtonEnabled: boolean;
};

/** React component that renders a popup with input fields for adding a plot. */
export default class AddPlotPopup extends React.Component<AddPlotPopupProps, AddPlotPopupState> {
  classes: any;
  possibleAttrValues: string[] = [];
  attributeNameOptions: string[] = [];
  attributeValue = '?';

  constructor(props: AddPlotPopupProps) {
    super(props);

    this.state = {
      entity: '',
      attributeName: '',
      isButtonEnabled: false,
    };
  }

  /**
   * Called when the entity field is changed.
   * Sets the `attributeNameOptions`, resets the `attributeValue` and sets the state.
   * @param {React.ChangeEvent<HTMLInputElement>} event The event that is given by the input field when a change event is fired.
   */
  private entityChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newEntity = event.target.value;

    this.attributeNameOptions = [];
    if (this.props.entitiesFromSchema.attributesPerEntity[newEntity])
      this.attributeNameOptions =
        this.props.entitiesFromSchema.attributesPerEntity[newEntity].textAttributeNames;

    this.attributeValue = '';
    this.setState({ ...this.state, entity: newEntity, attributeName: '', isButtonEnabled: false });
  };

  /**
   * Called when the attribute name field is changed.
   * Sets the possible attribute values, resets the `attributeValue`, enables the "Add" button if all fields valid and sets the state.
   * @param {React.ChangeEvent<HTMLInputElement>} event The event that is given by the input field when a change event is fired.
   */
  private attrNameChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newAttrName = event.target.value;

    this.possibleAttrValues = Array.from(
      this.props.nodeLinkResultNodes.reduce((values: Set<string>, node) => {
        if (this.state.entity == node.id.split('/')[0] && newAttrName in node.attributes)
          values.add(node.attributes[newAttrName]);
        return values;
      }, new Set<string>()),
    );

    // Reset attribute value.
    this.attributeValue = '';
    // Filter the possible attribute values from the entity attributes from the schema.
    const isButtonDisabled =
      this.props.entitiesFromSchema.entityNames.includes(this.state.entity) &&
      this.props.entitiesFromSchema.attributesPerEntity[
        this.state.entity
      ].textAttributeNames.includes(newAttrName);
    this.setState({
      ...this.state,
      attributeName: newAttrName,
      isButtonEnabled: isButtonDisabled,
    });
  };

  /**
   * Called when the user clicks the "Add" button.
   * Checks if all fields are valid before calling `handleClose()` and `addPlot()`.
   */
  private addPlotButtonClicked = () => {
    if (
      this.props.entitiesFromSchema.entityNames.includes(this.state.entity) &&
      this.props.entitiesFromSchema.attributesPerEntity[
        this.state.entity
      ].textAttributeNames.includes(this.state.attributeName)
    ) {
      this.props.handleClose();
      this.props.addPlot(
        this.state.entity,
        this.state.attributeName,
        this.attributeValue == '' ? '?' : this.attributeValue,
      );
    } else
      this.setState({
        ...this.state,
        isButtonEnabled: false,
      });
  };

  render(): ReactElement {
    const { open, anchorEl, entitiesFromSchema, handleClose } = this.props;

    // Retrieve the possible entity options. If none available, set helper message.
    let entityMenuItems: ReactElement[];
    if (this.props.entitiesFromSchema.entityNames.length > 0)
      entityMenuItems = entitiesFromSchema.entityNames.map((entity) => (
        <MenuItem key={entity} value={entity}>
          {entity}
        </MenuItem>
      ));
    else
      entityMenuItems = [
        <MenuItem key="placeholder" value="" disabled>
          No schema data available
        </MenuItem>,
      ];

    // Retrieve the possible attributeName options. If none available, set helper message.
    let attributeNameMenuItems: ReactElement[];
    if (this.attributeNameOptions.length > 0)
      attributeNameMenuItems = this.attributeNameOptions.map((attribute) => (
        <MenuItem key={attribute} value={attribute}>
          {attribute}
        </MenuItem>
      ));
    else
      attributeNameMenuItems = [
        <MenuItem key="placeholder" value="" disabled>
          First select an entity
        </MenuItem>,
      ];

    return (
      <div>
        <Popover
          id={'simple-addplot-popover'}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <div style={{ padding: 20, paddingTop: 10, display: 'flex' }}>
            <TextField
              select
              id="standard-select-entity"
              style={{ minWidth: 120 }}
              label="Entity"
              value={this.state.entity}
              onChange={this.entityChanged}
            >
              {entityMenuItems}
            </TextField>
            <TextField
              select
              id="standard-select-attribute"
              style={{ minWidth: 120, marginLeft: 20, marginRight: 20 }}
              label="Attribute"
              value={this.state.attributeName}
              onChange={this.attrNameChanged}
            >
              {attributeNameMenuItems}
            </TextField>
            <OptimizedAutocomplete
              currentValue={this.attributeValue}
              options={this.possibleAttrValues}
              onChange={(v) => (this.attributeValue = v)}
              useMaterialStyle={{ label: 'Value', helperText: '' }}
            />
            <div style={{ height: 40, paddingTop: 10, marginLeft: 30 }}>
              <Button
                variant="contained"
                color="default"
                disabled={!this.state.isButtonEnabled}
                onClick={this.addPlotButtonClicked}
              >
                <span style={{ fontWeight: 'bold' }}>Add</span>
              </Button>
            </div>
          </div>
        </Popover>
      </div>
    );
  }
}
