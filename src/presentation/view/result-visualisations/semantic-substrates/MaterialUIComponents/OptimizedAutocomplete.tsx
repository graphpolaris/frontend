/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ListSubheader from '@material-ui/core/ListSubheader';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { VariableSizeList, ListChildComponentProps } from 'react-window';
import { TextField, Typography } from '@material-ui/core';
import {
  Autocomplete,
  AutocompleteRenderGroupParams,
  AutocompleteRenderInputParams,
} from '@material-ui/lab';

const LISTBOX_PADDING = 8; // px

function renderRow(props: ListChildComponentProps) {
  const { data, index, style } = props;
  return React.cloneElement(data[index], {
    style: {
      ...style,
      top: (style.top as number) + LISTBOX_PADDING,
    },
  });
}

const OuterElementContext = React.createContext({});

// eslint-disable-next-line react/display-name
const OuterElementType = React.forwardRef<HTMLDivElement>((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data: any) {
  const ref = React.useRef<VariableSizeList>(null);
  React.useEffect(() => {
    if (ref.current != null) {
      ref.current.resetAfterIndex(0, true);
    }
  }, [data]);
  return ref;
}

// Adapter for react-window
const ListboxComponent = React.forwardRef<HTMLDivElement>(function ListboxComponent(props, ref) {
  // eslint-disable-next-line react/prop-types
  const { children, ...other } = props;
  const itemData = React.Children.toArray(children);
  const theme = useTheme();
  const smUp = useMediaQuery(theme.breakpoints.up('sm'), { noSsr: true });
  const itemCount = itemData.length;
  const itemSize = smUp ? 36 : 48;

  const getChildSize = (child: React.ReactNode) => {
    if (React.isValidElement(child) && child.type === ListSubheader) {
      return 48;
    }

    return itemSize;
  };

  const getHeight = () => {
    if (itemCount > 8) {
      return 8 * itemSize;
    }
    return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
  };

  const gridRef = useResetCache(itemCount);

  return (
    <div ref={ref}>
      <OuterElementContext.Provider value={other}>
        <VariableSizeList
          itemData={itemData}
          height={getHeight() + 2 * LISTBOX_PADDING}
          width="100%"
          ref={gridRef}
          outerElementType={OuterElementType}
          innerElementType="ul"
          itemSize={(index: number) => getChildSize(itemData[index])}
          overscanCount={5}
          itemCount={itemCount}
        >
          {renderRow}
        </VariableSizeList>
      </OuterElementContext.Provider>
    </div>
  );
});

const useStyles = makeStyles({
  listbox: {
    boxSizing: 'border-box',
    '& ul': {
      padding: 0,
      margin: 0,
    },
  },
});

const renderGroup = (params: AutocompleteRenderGroupParams) => [
  <ListSubheader key={params.key} component="div">
    {params.group}
  </ListSubheader>,
  params.children,
];

type OptimizedAutocomplete = {
  currentValue: string;
  options: string[];
  useMaterialStyle?: { label: string; helperText: string };
  /** Called when the value of the input field changes. */
  onChange?(value: string): void;
  /** Called when the user leaves focus of the input field. */
  onLeave?(value: string): void;
};
/** Renders the autocomplete input field with the given props. */
export default function OptimizedAutocomplete(props: OptimizedAutocomplete) {
  const classes = useStyles();

  let newValue = props.currentValue;

  return (
    <Autocomplete
      id="optimized-autocomplete"
      style={{ width: 200 }}
      disableListWrap
      autoHighlight
      classes={classes}
      ListboxComponent={ListboxComponent as React.ComponentType<React.HTMLAttributes<HTMLElement>>}
      renderGroup={renderGroup}
      options={props.options}
      // groupBy={(option: string) => option[0].toUpperCase()}
      renderInput={(params: AutocompleteRenderInputParams) =>
        props.useMaterialStyle != undefined ? (
          <TextField
            {...params}
            variant="standard"
            // size="small"
            label={props.useMaterialStyle.label}
            helperText={props.useMaterialStyle.helperText}
          />
        ) : (
          <div ref={params.InputProps.ref}>
            <input style={{ width: 200 }} type="text" {...params.inputProps} autoFocus />
          </div>
        )
      }
      // defaultValue={props.currentValue}
      placeholder={props.currentValue}
      renderOption={(option: string) => <Typography noWrap>{option}</Typography>}
      onChange={(_: React.ChangeEvent<any>, value: string | null) => {
        newValue = value || '?';
        if (props.onChange) props.onChange(newValue);
      }}
      onBlur={() => {
        if (props.onLeave) props.onLeave(newValue);
      }}
      onKeyDown={(e: React.KeyboardEvent<HTMLInputElement>): void => {
        if (e.key == 'Enter' && props.onLeave) props.onLeave(newValue);
      }}
    />
  );
}
