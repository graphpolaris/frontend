/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { ReactElement } from 'react';
import { Menu, MenuItem } from '@material-ui/core';
import styles from './styles/PlotAxisLabelStyles.module.css';

/** Props of the axis labels component. */
type PlotAxisLabelsProps = {
  xAxisLabel: string;
  yAxisLabel: string;
  axisLabelOptions: string[];

  width: number;
  height: number;

  // Callback when an axis label changed
  onAxisLabelChanged(axis: 'x' | 'y', value: string): void;
};
type PlotAxisLabelsState = {
  menuAnchor: Element | null;
  menuOpenForAxis: 'x' | 'y';
};
/** Component for rendering the axis labels of an semantic substrates plot.
 * With functionality to edit the labels.
 */
export default class PlotAxisLabelsComponent extends React.Component<
  PlotAxisLabelsProps,
  PlotAxisLabelsState
> {
  constructor(props: PlotAxisLabelsProps) {
    super(props);
    this.state = { menuAnchor: null, menuOpenForAxis: 'x' };
  }

  render(): ReactElement {
    const { width, height, xAxisLabel, yAxisLabel, axisLabelOptions } = this.props;

    return (
      <g>
        <text
          className={styles.xLabelText}
          x={width - 10}
          y={height + 35}
          onClick={(event: React.MouseEvent<SVGTextElement>) => {
            this.setState({
              menuAnchor: event.currentTarget,
              menuOpenForAxis: 'x',
            });
          }}
        >
          {xAxisLabel}
        </text>
        <text
          className={styles.yLabelText}
          x={-10}
          y={-50}
          transform={'rotate(-90)'}
          onClick={(event: React.MouseEvent<SVGTextElement>) => {
            this.setState({
              menuAnchor: event.currentTarget,
              menuOpenForAxis: 'y',
            });
          }}
        >
          {yAxisLabel}
        </text>
        <Menu
          id="simple-menu"
          anchorEl={this.state.menuAnchor}
          keepMounted
          getContentAnchorEl={null}
          anchorOrigin={
            this.state.menuOpenForAxis == 'x'
              ? { vertical: 'bottom', horizontal: 'center' }
              : { vertical: 'center', horizontal: 'right' }
          }
          transformOrigin={
            this.state.menuOpenForAxis == 'x'
              ? { vertical: 'top', horizontal: 'center' }
              : { vertical: 'center', horizontal: 'left' }
          }
          open={Boolean(this.state.menuAnchor)}
          onClose={() => this.setState({ menuAnchor: null })}
          transitionDuration={150}
          PaperProps={{
            style: {
              maxHeight: 48 * 4.5, // 48 ITEM_HEIGHT
            },
          }}
        >
          {axisLabelOptions.map((option) => (
            <MenuItem
              key={option}
              selected={option == (this.state.menuOpenForAxis == 'x' ? xAxisLabel : yAxisLabel)}
              onClick={() => {
                this.setState({ menuAnchor: null });

                this.props.onAxisLabelChanged(this.state.menuOpenForAxis, option);
              }}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
      </g>
    );
  }
}
