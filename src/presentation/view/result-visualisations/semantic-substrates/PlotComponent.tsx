/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { ReactElement } from 'react';
import * as d3 from 'd3-v6';
import {
  EntitiesFromSchema,
  MinMaxType,
  PlotType,
  AxisLabel,
  PlotSpecifications,
  NodeType,
} from '../../../../domain/entity/semantic-substrates/structures/Types';
import BrushComponent from './BrushComponent';
import PlotTitleComponent from './PlotTitleComponent';
import PlotAxisLabelsComponent from './PlotAxisLabelsComponent';

/** Props of the plots component */
type PlotProps = {
  plotData: PlotType;
  plotSpecification: PlotSpecifications;
  entitiesFromSchema: EntitiesFromSchema;
  nodeColor: string;
  nodeRadius: number;

  selectedAttributeNumerical: string;
  selectedAttributeCatecorigal: string;
  scaleCalculation: (x: number) => number;
  colourCalculation: (x: string) => string;

  width: number;
  height: number;

  onBrush(xRange: MinMaxType, yRange: MinMaxType): void;
  onDelete(): void;
  onAxisLabelChanged(axis: 'x' | 'y', value: string): void;

  onTitleChanged(entity: string, attrName: string, attrValue: string): void;
};

/** State for the plots component */
type PlotState = {
  menuAnchor: Element | null;
};

/** React component to render a plot with axis and square selection tool. */
export default class Plot extends React.Component<PlotProps, PlotState> {
  ref = React.createRef<SVGGElement>();

  xAxisScale: d3.ScaleLinear<number, number, never> = d3.scaleLinear();
  yAxisScale: d3.ScaleLinear<number, number, never> = d3.scaleLinear();

  d3XAxis: d3.Selection<SVGGElement, unknown, null, undefined> | undefined;
  d3YAxis: d3.Selection<SVGGElement, unknown, null, undefined> | undefined;

  constructor(props: PlotProps) {
    super(props);
    this.state = {
      menuAnchor: null,
    };

    this.updateXYAxisScaleFunctions();
  }

  public componentDidMount(): void {
    const d3Selected = d3.select(this.ref.current);

    // Add x axis ticks
    this.d3XAxis = d3Selected
      .append('g')
      .attr('transform', 'translate(0,' + this.props.height + ')')
      .call(d3.axisBottom(this.xAxisScale));

    // Add y axis ticks
    this.d3YAxis = d3Selected.append('g').call(d3.axisLeft(this.yAxisScale));
  }

  public componentDidUpdate(prevProps: PlotProps): void {
    if (
      this.props.plotData.minmaxXAxis != prevProps.plotData.minmaxXAxis ||
      this.props.plotData.minmaxYAxis != prevProps.plotData.minmaxYAxis
    ) {
      this.updateXYAxisScaleFunctions();

      // Update x axis ticks
      if (this.d3XAxis) {
        this.d3XAxis.transition().duration(1000).call(d3.axisBottom(this.xAxisScale));
      }

      // Update y axis ticks
      if (this.d3YAxis) {
        this.d3YAxis.transition().duration(1000).call(d3.axisLeft(this.yAxisScale));
      }
    }
  }

  /** Update the x and y axis scale functions with the new axis domains. */
  private updateXYAxisScaleFunctions(): void {
    // Create the x axis scale funtion with d3
    if (this.props.plotSpecification.xAxis == AxisLabel.evenlySpaced)
      this.xAxisScale = d3.scaleLinear().domain([]).range([this.props.width, 0]);
    else
      this.xAxisScale = d3
        .scaleLinear()
        .domain([this.props.plotData.minmaxXAxis.max, this.props.plotData.minmaxXAxis.min])
        .range([this.props.width, 0]);

    // Create the y axis scale funtion with d3
    if (this.props.plotSpecification.yAxis == AxisLabel.evenlySpaced)
      this.yAxisScale = d3.scaleLinear().domain([]).range([0, this.props.height]);
    else
      this.yAxisScale = d3
        .scaleLinear()
        .domain([this.props.plotData.minmaxYAxis.max, this.props.plotData.minmaxYAxis.min])
        .range([0, this.props.height]);
  }

  public render(): ReactElement {
    const {
      width,
      height,
      // yOffset,
      plotData,
      nodeColor,
      nodeRadius,
      entitiesFromSchema,
      plotSpecification,
      selectedAttributeNumerical,
      selectedAttributeCatecorigal,
      scaleCalculation,
      colourCalculation,
      onDelete,
      onBrush,
      onAxisLabelChanged,
      onTitleChanged,
    } = this.props;

    const circles = plotData.nodes.map((node) => (
      <circle
        key={node.data.text}
        cx={node.scaledPosition.x}
        cy={node.scaledPosition.y}
        r={Math.abs(IfAttributeIsNumber(nodeRadius, node))}
        fill={colourCalculation(node.attributes[selectedAttributeCatecorigal])}
      />
    ));

    function IfAttributeIsNumber(nodeRadius: number, node: NodeType): number {
      console.log(selectedAttributeNumerical);
      let possibleRadius = Number(node.attributes[selectedAttributeNumerical]);
      if (!isNaN(possibleRadius)) {
        return scaleCalculation(possibleRadius);
      }
      return nodeRadius;
    }

    // Determine the x y axis label, if it's byAttribute, give it the attribute type.
    let xAxisLabel: string;
    if (plotSpecification.xAxis == AxisLabel.byAttribute)
      xAxisLabel = plotSpecification.xAxisAttributeType;
    else xAxisLabel = plotSpecification.xAxis;
    let yAxisLabel: string;
    if (plotSpecification.yAxis == AxisLabel.byAttribute)
      yAxisLabel = plotSpecification.yAxisAttributeType;
    else yAxisLabel = plotSpecification.yAxis;

    const axisLabelOptions = [
      ...entitiesFromSchema.attributesPerEntity[plotSpecification.entity].numberAttributeNames,
      AxisLabel.inboundConnections,
      AxisLabel.outboundConnections,
      AxisLabel.evenlySpaced,
    ];

    return (
      <g ref={this.ref} transform={'translate(0,' + plotData.yOffset + ')'}>
        <PlotTitleComponent
          pos={{ x: 0, y: -5 }}
          nodeColor={nodeColor}
          entity={plotSpecification.entity}
          attributeName={plotSpecification.labelAttributeType}
          attributeValue={plotSpecification.labelAttributeValue}
          entitiesFromSchema={entitiesFromSchema}
          onTitleChanged={onTitleChanged}
          possibleAttrValues={plotData.possibleTitleAttributeValues}
        />
        {circles}
        <PlotAxisLabelsComponent
          width={width}
          height={height}
          xAxisLabel={xAxisLabel}
          yAxisLabel={yAxisLabel}
          axisLabelOptions={axisLabelOptions}
          onAxisLabelChanged={onAxisLabelChanged}
        />
        <g
          onClick={() => onDelete()}
          transform={`translate(${width + 3},${height - 30})`}
          className="delete-plot-icon"
        >
          <rect x1="-1" y1="-1" width="26" height="26" fill="transparent" />
          <path d="M3,6V24H21V6ZM8,20a1,1,0,0,1-2,0V10a1,1,0,0,1,2,0Zm5,0a1,1,0,0,1-2,0V10a1,1,0,0,1,2,0Zm5,0a1,1,0,0,1-2,0V10a1,1,0,0,1,2,0Z" />
          <path
            className="lid"
            d="M22,2V4H2V2H7.711c.9,0,1.631-1.1,1.631-2h5.315c0,.9.73,2,1.631,2Z"
          />
        </g>
        <BrushComponent
          width={width}
          height={height}
          xAxisDomain={plotData.minmaxXAxis}
          yAxisDomain={plotData.minmaxYAxis}
          xAxisLabel={plotSpecification.xAxis}
          yAxisLabel={plotSpecification.yAxis}
          onBrush={onBrush}
        />
      </g>
    );
  }
}
