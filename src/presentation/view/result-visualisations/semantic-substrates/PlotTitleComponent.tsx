/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { Menu, MenuItem } from '@material-ui/core';
import Color from 'color';
import React, { ReactElement } from 'react';
import { EntitiesFromSchema } from '../../../../domain/entity/semantic-substrates/structures/Types';
import { Position } from '../../../../domain/entity/utils/Position';
import { getWidthOfText } from '../../../util/graph-schema/utils';
import OptimizedAutocomplete from './MaterialUIComponents/OptimizedAutocomplete';
import styles from './styles/PlotTitleStyles.module.css';

/** Props of the plots title component. */
type PlotTitleProps = {
  entity: string;
  attributeName: string;
  attributeValue: string;
  entitiesFromSchema: EntitiesFromSchema;
  nodeColor: string;
  pos: Position;
  possibleAttrValues: string[];
  onTitleChanged(entity: string, attrName: string, attrValue: string): void;
};

/** State of the plots title component. */
type PlotTitleState = {
  isEditingAttrValue: boolean;

  // Dropdown when editing entity or attributeName
  menuAnchor: Element | null;
  menuItems: string[];
  menuItemsOnClick(value: string): void;
};
/**
 * A semantic substrates React component for rendering a plot title.
 * With functionality to change the entity, attributeType and attributeName.
 */
export default class PlotTitleComponent extends React.Component<PlotTitleProps, PlotTitleState> {
  constructor(props: PlotTitleProps) {
    super(props);

    this.state = {
      isEditingAttrValue: false,
      menuAnchor: null,
      menuItems: [],
      menuItemsOnClick: () => true,
    };
  }

  /**
   * Will be called when the value of the entity changes.
   * @param {string} value The new entity value.
   */
  private onEntityChanged(value: string): void {
    // Get the first attribute name for this entity.
    const attrName =
      this.props.entitiesFromSchema.attributesPerEntity[value].textAttributeNames.length > 0
        ? this.props.entitiesFromSchema.attributesPerEntity[value].textAttributeNames[0]
        : '?';

    this.props.onTitleChanged(value, attrName, '?');
  }

  /**
   * Will be called when the attribute name changes.
   * @param {string} value The new attribute name value.
   */
  private onAttrNameChanged(value: string): void {
    this.props.onTitleChanged(this.props.entity, value, '?');
  }

  /**
   * Will be called when the attribute value changes.
   * @param {string} value The new value of the attribute value.
   */
  private onAttrValueChanged(value: string): void {
    if (value == '') value = '?';

    // only update the state if the attribute value didn't change
    // If the attribute value did change, this component will be rerendered anyway
    if (value != this.props.attributeValue)
      this.props.onTitleChanged(this.props.entity, this.props.attributeName, value);
    else
      this.setState({
        isEditingAttrValue: false,
      });
  }

  /**
   * Renders the attribute value, either plain svg <text> element or, <input> if the user is editing this field.
   * @param {number} xOffset The x position offset.
   * @returns {ReactElement} The svg elements to render for the attribute value.
   */
  private renderAttributeValue(xOffset: number): ReactElement {
    if (this.state.isEditingAttrValue)
      return (
        <foreignObject x={xOffset} y="-16" width="200" height="150">
          <div>
            <OptimizedAutocomplete
              options={this.props.possibleAttrValues}
              currentValue={this.props.attributeValue}
              onLeave={(v: string) => this.onAttrValueChanged(v)}
            />
          </div>
        </foreignObject>
      );
    else
      return (
        <text
          x={xOffset}
          className={styles.clickable}
          onClick={() => this.setState({ isEditingAttrValue: true })}
        >
          {this.props.attributeValue}
        </text>
      );
  }

  render(): ReactElement {
    const { entity, attributeName, entitiesFromSchema, pos, nodeColor } = this.props;

    const withOffset = getWidthOfText(entity + ' ', 'arial', '15px', 'bold');
    const attrNameOffset = getWidthOfText('with ', 'arial', '15px') + withOffset;
    const colonOffset =
      getWidthOfText(attributeName + ' ', 'arial', '15px', 'bold') + attrNameOffset;
    const attrValueOffset = getWidthOfText(': ', 'arial', '15px', 'bold') + colonOffset;

    const nodeColorDarkened = Color(nodeColor).darken(0.3).hex();

    return (
      <g transform={`translate(${pos.x},${pos.y})`}>
        <text
          className={styles.clickable}
          fill={nodeColorDarkened}
          onClick={(event: React.MouseEvent<SVGTextElement>) => {
            this.setState({
              menuAnchor: event.currentTarget,
              menuItems: entitiesFromSchema.entityNames,
              menuItemsOnClick: (v: string) => this.onEntityChanged(v),
            });
          }}
        >
          {entity}
        </text>
        <text x={withOffset} fontSize={15}>
          with
        </text>
        <text
          className={styles.clickable}
          x={attrNameOffset}
          onClick={(event: React.MouseEvent<SVGTextElement>) => {
            this.setState({
              menuAnchor: event.currentTarget,
              menuItems: entitiesFromSchema.attributesPerEntity[entity].textAttributeNames,
              menuItemsOnClick: (v: string) => this.onAttrNameChanged(v),
            });
          }}
        >
          {attributeName}
        </text>
        <text x={colonOffset} fontWeight="bold" fontSize={15}>
          :
        </text>

        {this.renderAttributeValue(attrValueOffset)}

        <Menu
          id="simple-menu"
          anchorEl={this.state.menuAnchor}
          keepMounted
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={Boolean(this.state.menuAnchor)}
          onClose={() => this.setState({ menuAnchor: null })}
          transitionDuration={150}
          PaperProps={{
            style: {
              maxHeight: 48 * 4.5, // 48 ITEM_HEIGHT
            },
          }}
        >
          {this.state.menuItems.map((option) => (
            <MenuItem
              key={option}
              selected={option == this.state.menuAnchor?.innerHTML}
              onClick={() => {
                this.setState({ menuAnchor: null, menuItems: [] });

                this.state.menuItemsOnClick(option);
              }}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
      </g>
    );
  }
}
