/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import Color from 'color';
import React, { ReactElement } from 'react';
import {
  PlotType,
  RelationType,
} from '../../../../domain/entity/semantic-substrates/structures/Types';

/** All the props needed to visualize all the semantic substrates checkboxes. */
type SVGCheckboxesWithSemanticSubstrLabelProps = {
  plots: PlotType[];
  relations: RelationType[][][];
  visibleRelations: boolean[][];
  nodeColors: string[];
  onCheckboxChanged(fromPlot: number, toPlot: number, value: boolean): void;
};

/** Renders all the checkboxes for a semantic substrates visualisation. */
export default class SVGCheckboxesWithSemanticSubstrLabel extends React.Component<SVGCheckboxesWithSemanticSubstrLabelProps> {
  render(): ReactElement {
    const { plots, relations, visibleRelations, nodeColors, onCheckboxChanged } = this.props;

    const checkboxGroups: ReactElement[] = [];

    // Go through each relation.
    for (let fromPlot = 0; fromPlot < relations.length; fromPlot++) {
      const checkboxes: JSX.Element[] = [];
      // The from- and toPlot title color will be a bit darker than the node colors for their plots.
      const fromColor = Color(nodeColors[fromPlot]).darken(0.3).hex();
      for (let toPlot = 0; toPlot < relations[fromPlot].length; toPlot++) {
        if (relations[fromPlot][toPlot].length > 0)
          // Add a checkbox for the connections between fromPlot and toPlot.
          checkboxes.push(
            <g
              key={plots[fromPlot].title + fromPlot + '-' + plots[toPlot].title + toPlot}
              transform={'translate(0,' + toPlot * 30 + ')'}
            >
              <text x={20} y={12} fill={fromColor} style={{ fontWeight: 'bold' }}>
                {plots[fromPlot].title}
              </text>
              <path
                fill="transparent"
                stroke={'black'}
                d={`M${110} 7 l40 0 l-0 0 l-15 -5 m15 5 l-15 5`}
              />
              <text
                x={170}
                y={12}
                fill={Color(nodeColors[toPlot]).darken(0.3).hex()}
                style={{ fontWeight: 'bold' }}
              >
                {plots[toPlot].title}
              </text>
              <SVGCheckboxComponent
                x={0}
                y={0}
                key={plots[fromPlot].title + plots[toPlot].title + fromPlot + toPlot}
                width={15}
                value={visibleRelations[fromPlot][toPlot]}
                onChange={(value: boolean) => {
                  onCheckboxChanged(fromPlot, toPlot, value);
                }}
              />
            </g>,
          );
      }

      // Offset the height of the checkboxes for each plot.
      checkboxGroups.push(
        <g
          key={plots[fromPlot].title + fromPlot}
          transform={
            'translate(' + (plots[fromPlot].width + 30) + ',' + plots[fromPlot].yOffset + ')'
          }
        >
          {checkboxes}
        </g>,
      );
    }

    return <g>{checkboxGroups}</g>;
  }
}

/** The props for the SVG checkbox component */
type SVGCheckBoxProps = {
  x: number;
  y: number;
  width: number;
  value: boolean;
  // Called when the the checkbox state changes.
  onChange(value: boolean): void;
};

/** The state for the SVG checkbox component  */
type SVGCheckBoxState = {
  value: boolean;
};
/** Renders a simple checkbox in SVG elements. */
export class SVGCheckboxComponent extends React.Component<SVGCheckBoxProps, SVGCheckBoxState> {
  static defaultProps = {
    width: 15,
    value: false,
    onChange: () => true,
  };
  constructor(props: SVGCheckBoxProps) {
    super(props);
    this.state = { value: this.props.value };
  }

  render(): ReactElement {
    return (
      <g>
        <rect
          x={this.props.x}
          y={this.props.y}
          rx="0"
          ry="0"
          width={this.props.width}
          height={this.props.width}
          style={{ fill: 'transparent', stroke: 'grey', strokeWidth: 2 }}
          onClick={() => {
            const newVal = this.state.value ? false : true;
            this.props.onChange(newVal);
            this.setState({ value: newVal });
          }}
        />
        <rect
          x={this.props.x + 3}
          y={this.props.y + 3}
          rx="0"
          ry="0"
          width={this.props.width - 6}
          height={this.props.width - 6}
          style={{ fill: this.state.value ? '#00a300' : 'white' }}
          onClick={() => {
            const newVal = this.state.value ? false : true;
            this.props.onChange(newVal);
            this.setState({ value: newVal });
          }}
        />
      </g>
    );
  }
}
