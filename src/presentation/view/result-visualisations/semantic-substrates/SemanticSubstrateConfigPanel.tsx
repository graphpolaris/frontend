/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useRef, useState } from 'react';
import styles from './SemanticSubstratesComponent.module.scss';
import {
  EntityWithAttributes,
  FSSConfigPanelProps,
} from '../../../../domain/entity/semantic-substrates/config-panel/Types';
import SemanticSubstratesConfigPanelViewModelImpl from '../../../view-model/result-visualisations/semantic-substrates/SemanticSubstratesConfigPanelViewModelImpl';
import SemanticSubstratesConfigPanelViewModel from '../../../view-model/result-visualisations/semantic-substrates/SemanticSubstratesConfigPanelViewModel';

/** Component for rendering config input fields for Faceted Semantic Subrate attributes. */
export default function FSSConfigPanel(props: FSSConfigPanelProps) {
  let FSSConfigPanelViewModel: SemanticSubstratesConfigPanelViewModel =
    new SemanticSubstratesConfigPanelViewModelImpl(props);

  FSSConfigPanelViewModel.makeNodeTypes();
  FSSConfigPanelViewModel.makeRelationTypes();

  return (
    <div className={styles.container}>
      <p className={styles.title}>Semantic Substrates Settings:</p>

      {/* Experimental options implementation */}
      <p className={styles.subtitle}>- Visual Assignment</p>
      <div className={styles.subContainer}>
        <p>Node: </p>
        <select onChange={(e) => FSSConfigPanelViewModel.onNodeChange(e.target.value)}>
          {FSSConfigPanelViewModel.nodes.map((n) => (
            <option key={n.name} value={n.name}>
              {n.name}
            </option>
          ))}
        </select>
      </div>

      {/* NODE ATTRIBUTES */}
      <div className={styles.subContainer}>
        <p>Attribute: </p>
        <select
          id="attribute-select"
          onChange={(e) => {
            FSSConfigPanelViewModel.onAttributeChange(e.target.value);
          }}
        >
          {FSSConfigPanelViewModel.getUniqueNodeAttributes().map((name) => (
            <option key={name} value={name}>
              {name}
            </option>
          ))}
        </select>
      </div>

      {/* RELATION ATTRIBUTES */}
      <div className={styles.subContainer}>
        <p>Relation Attribute: </p>
        <select
          id="relation-attribute-select"
          onChange={(e) => {
            FSSConfigPanelViewModel.onRelationAttributeChange(e.target.value);
          }}
        >
          {FSSConfigPanelViewModel.getUniqueRelationAttributes().map((name) => (
            <option key={name} value={name}>
              {name}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
}
