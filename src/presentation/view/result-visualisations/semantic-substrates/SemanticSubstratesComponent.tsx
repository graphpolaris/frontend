/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import Color from 'color';
import './SemanticSubstratesStylesheet.ts';
import {
  EntitiesFromSchema,
  MinMaxType,
  PlotSpecifications,
  PlotType,
  RelationType,
} from '../../../../domain/entity/semantic-substrates/structures/Types';
import Plot from './PlotComponent';
import LinesBetweenPlots from './LinesBetweenPlotsComponent';
import BaseView from '../../BaseView';
import SemanticSubstratesViewModel from '../../../view-model/result-visualisations/semantic-substrates/SemanticSubstratesViewModel';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import SVGCheckboxesWithSemanticSubstrLabel from './SVGCheckBoxComponent';
import AddPlotPopup from './MaterialUIComponents/AddPlotPopup';
import AddPlotButtonComponent from './AddPlotButtonComponent';
import { NodeLinkResultType } from '../../../../domain/entity/query-result/structures/NodeLinkResultType';
import FSSConfigPanel from './SemanticSubstrateConfigPanel';
import VisConfigPanelComponent from '../utils/VisConfigPanel/VisConfigPanel';
/** SemanticSubstratesProps is an interface containing the SemanticSubstratesViewModel. */
export interface SemanticSubstratesProps {
  semanticSubstratesViewModel: SemanticSubstratesViewModel;
}

type SemanticSubstratesState = {
  plots: PlotType[];
  plotSpecifications: PlotSpecifications[];
  // Relations is a 3d array, with the first array denoting the outbound plot, the second array denotes the inbound plot
  // For example, [plot1][plot2] will give all edges which are outbound from plot1 and ingoing to plot2
  filteredRelations: RelationType[][][];
  allRelations: RelationType[][][];
  nodeLinkQueryResult: NodeLinkResultType;
  // Determines if connections [fromPlotIndex][toPlotIndex] will be shown.
  visibleRelations: boolean[][];
  entitiesFromSchema: EntitiesFromSchema;
  nodeRadius: number;
  nodeColors: string[];
  styles: ClassNameMap;
  addPlotButtonAnchor: Element | null;
};

/** React component for rendering a semantic substrates visualization using a `SemanticSubstratesViewModel`. */
export default class SemanticSubstrates
  extends React.Component<SemanticSubstratesProps, SemanticSubstratesState>
  implements BaseView
{
  semanticSubstratesViewModel: SemanticSubstratesViewModel;
  inputRef = React.createRef<HTMLInputElement>();

  constructor(props: SemanticSubstratesProps) {
    super(props);
    this.semanticSubstratesViewModel = props.semanticSubstratesViewModel;

    // Initialize all relation lines to be invisible
    this.state = {
      plots: props.semanticSubstratesViewModel.plots,
      plotSpecifications: props.semanticSubstratesViewModel.plotSpecifications,
      allRelations: props.semanticSubstratesViewModel.allRelations,
      filteredRelations: props.semanticSubstratesViewModel.filteredRelations,
      visibleRelations: props.semanticSubstratesViewModel.visibleRelations,
      nodeLinkQueryResult: props.semanticSubstratesViewModel.nodeLinkQueryResult,
      entitiesFromSchema: props.semanticSubstratesViewModel.entitiesFromSchemaPruned,
      nodeRadius: props.semanticSubstratesViewModel.nodeRadius,
      nodeColors: props.semanticSubstratesViewModel.nodeColors,
      styles: props.semanticSubstratesViewModel.styles,

      addPlotButtonAnchor: null,
    };
  }

  public componentDidMount(): void {
    this.semanticSubstratesViewModel.attachView(this);
    this.semanticSubstratesViewModel.subscribeToSchemaResult();
    this.semanticSubstratesViewModel.subscribeToQueryResult();
    this.semanticSubstratesViewModel.setThisVisAsExportable();
  }
  public componentWillUnmount(): void {
    this.semanticSubstratesViewModel.detachView();
    this.semanticSubstratesViewModel.unSubscribeFromSchemaResult();
    this.semanticSubstratesViewModel.unSubscribeFromQueryResult();
  }

  public onViewModelChanged(): void {
    this.setState({
      plots: this.semanticSubstratesViewModel.plots,
      plotSpecifications: this.semanticSubstratesViewModel.plotSpecifications,
      allRelations: this.semanticSubstratesViewModel.allRelations,
      filteredRelations: this.semanticSubstratesViewModel.filteredRelations,
      visibleRelations: this.semanticSubstratesViewModel.visibleRelations,
      nodeLinkQueryResult: this.semanticSubstratesViewModel.nodeLinkQueryResult,
      entitiesFromSchema: this.semanticSubstratesViewModel.entitiesFromSchema,
      nodeRadius: this.semanticSubstratesViewModel.nodeRadius,
      nodeColors: this.semanticSubstratesViewModel.nodeColors,
      styles: this.semanticSubstratesViewModel.styles,
    });
  }

  render(): JSX.Element {
    const {
      styles,
      plots,
      filteredRelations,
      allRelations,
      nodeLinkQueryResult,
      entitiesFromSchema,
      nodeColors,
      nodeRadius,
      plotSpecifications,
      visibleRelations,
    } = this.state;

    const plotElements: JSX.Element[] = [];
    for (let i = 0; i < plots.length; i++) {
      plotElements.push(
        <Plot
          key={plots[i].title + i}
          plotData={plots[i]}
          plotSpecification={plotSpecifications[i]}
          entitiesFromSchema={entitiesFromSchema}
          nodeColor={nodeColors[i]}
          nodeRadius={nodeRadius}
          width={plots[i].width}
          height={plots[i].height}
          selectedAttributeNumerical={plots[i].selectedAttributeNumerical}
          selectedAttributeCatecorigal={plots[i].selectedAttributeCatecorigal}
          scaleCalculation={plots[i].scaleCalculation}
          colourCalculation={plots[i].colourCalculation}
          onBrush={(xRange: MinMaxType, yRange: MinMaxType) =>
            // Call the onBrush method with the index of this plot
            this.semanticSubstratesViewModel.onBrush(i, xRange, yRange)
          }
          onDelete={() => this.semanticSubstratesViewModel.onDelete(i)}
          onAxisLabelChanged={(axis: 'x' | 'y', value: string) =>
            this.semanticSubstratesViewModel.onAxisLabelChanged(i, axis, value)
          }
          onTitleChanged={(entity: string, attrName: string, attrValue: string) =>
            this.semanticSubstratesViewModel.onPlotTitleChanged(i, entity, attrName, attrValue)
          }
        />,
      );
    }

    // Create the arrow-lines between the plots.
    const linesBetween: JSX.Element[] = [];
    for (let i = 0; i < filteredRelations.length; i++) {
      for (let j = 0; j < filteredRelations[i].length; j++) {
        let color = Color(nodeColors[i]);
        color = i == j ? color.lighten(0.3) : color.darken(0.3);
        if (visibleRelations[i][j]) {
          // Only create the lines if the checkbox is ticked
          linesBetween.push(
            <LinesBetweenPlots
              key={plots[i].title + i + '-' + plots[j].title + j}
              fromPlot={plots[i]}
              toPlot={plots[j]}
              relations={filteredRelations[i][j]}
              color={color.hex()}
              nodeRadius={nodeRadius}
            />,
          );
        }
      }
    }

    const heightOfAllPlots =
      plots.length > 0 ? plots[plots.length - 1].yOffset + plots[plots.length - 1].height + 50 : 0;
    return (
      <div className={styles.container}>
        <div style={{ width: '100%', height: '100%' }}>
          <svg style={{ width: '100%', height: heightOfAllPlots + 60 }}>
            <AddPlotButtonComponent
              x={750}
              onClick={(event: React.MouseEvent<SVGRectElement>) =>
                this.setState({ ...this.state, addPlotButtonAnchor: event.currentTarget })
              }
            />
            <g transform={'translate(60,60)'}>
              <SVGCheckboxesWithSemanticSubstrLabel
                plots={plots}
                relations={allRelations}
                visibleRelations={visibleRelations}
                nodeColors={nodeColors}
                onCheckboxChanged={(fromPlot: number, toPlot: number, value: boolean) =>
                  this.semanticSubstratesViewModel.onCheckboxChanged(fromPlot, toPlot, value)
                }
              />
              {plotElements}
              {linesBetween},
            </g>
          </svg>
        </div>
        <AddPlotPopup
          open={Boolean(this.state.addPlotButtonAnchor)}
          anchorEl={this.state.addPlotButtonAnchor}
          entitiesFromSchema={entitiesFromSchema}
          nodeLinkResultNodes={nodeLinkQueryResult.nodes}
          handleClose={() => this.setState({ ...this.state, addPlotButtonAnchor: null })}
          addPlot={(entity: string, attributeName: string, attributeValue: string) =>
            this.semanticSubstratesViewModel.addPlot(entity, attributeName, attributeValue)
          }
        />
        <VisConfigPanelComponent>
          <FSSConfigPanel
            fssViewModel={this.semanticSubstratesViewModel}
            graph={this.state.nodeLinkQueryResult}
            // currentColours={currentColours}
          />
        </VisConfigPanelComponent>
      </div>
    );
  }
}
