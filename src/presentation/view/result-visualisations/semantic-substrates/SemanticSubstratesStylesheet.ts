/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

/** CSS for semantic substrates view. */
export const useSemanticSubstratesStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      '@global': {
        '.selection': {
          fillOpacity: 0.1,
          stroke: 'lightgrey',
        },

        '.delete-plot-icon': {
          '& path': {
            opacity: 0.4,
            transition: '0.1s',

            transformBox: 'fill-box',
          },
          '&:hover': {
            '& path': {
              opacity: 0.8,
              fill: '#d00',
              transform: 'scale(1.1)',
            },
            '& .lid': {
              transform: 'rotate(-15deg) scale(1.1)',
            },
          },
        },
      },

      display: 'flex',
      flexDirection: 'row',

      width: '100%',

      marginTop: '10px',
      marginLeft: '30px',
      overflowY: 'auto',
    },

    checkboxGroup: {
      display: 'flex',
      flexDirection: 'column',
    },
  }),
);
