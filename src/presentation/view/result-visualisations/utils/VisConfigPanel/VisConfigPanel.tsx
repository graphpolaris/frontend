/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

/** A component for rendering a configuration panel to the right of the visualisation. */

import React, { useState } from 'react';
import styles from './VisConfigPanel.module.scss';
import { ReactComponent as ArrowRightSVG } from './ArrowRightIcon.svg';

export default function VisConfigPanelComponent({
  width = 250,
  children,
  isLeft = false,
}: {
  width?: number;
  children?: React.ReactNode;
  isLeft?: boolean;
}) {
  const [show, setShow] = useState(false);
  const containerStyle = isLeft
    ? { left: show ? 0 : -width + 13, width: width }
    : { right: show ? 0 : -width + 13, width: width };
  const arrowStyle = isLeft ? { right: 0 } : { left: 0 };

  return (
    <div className={styles.container} style={containerStyle} key="placeholderKey">
      <div
        className={styles.expandButton}
        key="placeholderKey2"
        onClick={() => (show ? setShow(false) : setShow(true))}
        style={arrowStyle}
      >
        <ArrowRightSVG className={show == isLeft ? '' : styles.arrowLeft} key="placeholderKey3" />
      </div>
      <div className={styles.childrenContainer} key="placeholderKey4">
        <div className={styles.children} key="placeholderKey5">
          {children}
        </div>
      </div>
    </div>
  );
}
