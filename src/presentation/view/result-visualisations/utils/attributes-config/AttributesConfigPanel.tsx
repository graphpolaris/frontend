/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
/** A component for rendering a configuration panel to the right of the visualisation. */

import React, { useRef, useState, Component } from 'react';
import BaseView from '../../../BaseView';
import styles from '../../node-link/NodeLinkConfigPanel.module.scss';
import attrStyles from './AttributesConfigPanel.module.scss';
import { GraphType, NodeType } from '../../../../../domain/entity/node-link/structures/Types';
import NodeLinkViewModel from '../../../../view-model/result-visualisations/node-link/NodeLinkViewModel';
import AttributesConfigPanelViewModel from '../../../../view-model/result-visualisations/utils/attributes-config/AttributesConfigPanelViewModel';
import AttributesConfigPanelViewModelImpl from '../../../../view-model/result-visualisations/utils/attributes-config/AttributesConfigPanelViewModelImpl';
import { Slider } from '@material-ui/core';

/** Props for this component */
type AttributesConfigPanelProps = {
  nodeLinkViewModel: NodeLinkViewModel;
};

type AttributesConfigPanelState = {
  currentNode: string | undefined;
};

export default class AttributesConfigPanel
  extends Component<AttributesConfigPanelProps, AttributesConfigPanelState>
  implements BaseView
{
  public attrViewModel: AttributesConfigPanelViewModelImpl;

  public constructor(props: AttributesConfigPanelProps) {
    super(props);

    const { nodeLinkViewModel } = props;
    this.attrViewModel = new AttributesConfigPanelViewModelImpl(nodeLinkViewModel);

    this.state = {
      currentNode: this.attrViewModel.currentNode,
    };
  }

  public getAttributes(): JSX.Element[] {
    let attributes = this.attrViewModel.getAttributes();
    let rows: JSX.Element[] = [];

    for (let attr in attributes) {
      rows.push(
        <div className={attrStyles.attribute} id={attr} key={attr}>
          <p>{attr}</p>
          <input
            type="checkbox"
            id={'attribute-' + attr}
            className={attrStyles.checkbox}
            onChange={() => this.attrViewModel.toggleAttribute(attr)}
            checked={attributes[attr]}
          />
        </div>,
      );
    }

    return rows;
  }

  /**
   * The componenDidMount function is called after the site is loaded and the component attached.
   * It calls upon initialising and starting functions. It is more or less the same as using useEffect in a functional component.
   */
  public componentDidMount() {
    // Attach Viewmodel Observer
    this.attrViewModel.initAttributes();
    this.attrViewModel.attachView(this);
  }

  /** The componentWillUnmount function is called when the site is closed to remove the components. */
  public componentWillUnmount() {
    // Detach Viewmodel Observer
    this.attrViewModel.detachView();
  }

  public onViewModelChanged() {
    this.setState({
      currentNode: this.attrViewModel.currentNode,
    });
  }

  public render() {
    this.attrViewModel.makeNodeTypes();

    return (
      <div className={styles.container}>
        <p className={styles.title}>Attributes Settings:</p>
        <p className={styles.subtitle}>- NodeLink</p>
        <div className={styles.subContainer}>
          <h4>Visible attributes:</h4>
          <div className={styles.subContainer}>
            <div className={attrStyles.nodeSelector}>
              <p>Node: </p>
              {this.attrViewModel.nodeSelect()}
            </div>
            <div className={attrStyles.attributes} id="attributes">
              {this.getAttributes()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
